CREATE TABLE `kemsa_issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `county_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `facility_code` int(11) DEFAULT NULL,
  `qty_issued` int(11) DEFAULT NULL,
  `qty_requested` int(11) DEFAULT NULL,
  `commodity_id` int(11) DEFAULT NULL,
  `issue_date` varchar(45) DEFAULT NULL,
  `delivery_note` varchar(45) DEFAULT NULL,
  `value_issued` int(11) DEFAULT NULL,
  `value_requested` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1