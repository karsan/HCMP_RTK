CREATE TABLE `facs_presto_fcdrr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facility_code` int(11) DEFAULT NULL,
  `fcdrr_date` varchar(45) DEFAULT NULL,
  `no_of_tests` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1