CREATE TABLE `kemsa_issue_discrepancies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kemsa_issue_id` int(11) NOT NULL,
  `county_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `facility_code` int(11) DEFAULT NULL,
  `qty_issued_current` int(11) DEFAULT NULL,
  `qty_issued_new` int(11) DEFAULT NULL,
  `qty_requested_current` int(11) DEFAULT NULL,
  `qty_requested_new` int(11) DEFAULT NULL,
  `commodity_id` int(11) DEFAULT NULL,
  `issue_date` varchar(45) DEFAULT NULL,
  `delivery_note` varchar(45) DEFAULT NULL,
  `value_issued` int(11) DEFAULT NULL,
  `value_requested` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1