CREATE TABLE `county_drawing_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `county_id` int(11) DEFAULT NULL,
  `zone` varchar(45) DEFAULT NULL,
  `duration` varchar(45) DEFAULT NULL,
  `quarter` int(11) DEFAULT NULL,
  `screening_total` int(11) DEFAULT '0',
  `screening_used` int(11) DEFAULT '0',
  `screening_distributed` int(11) DEFAULT NULL,
  `confirmatory_total` int(11) DEFAULT '0',
  `confirmatory_used` int(11) DEFAULT '0',
  `confirmatory_distributed` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `screening_allowed_quarter` int(11) DEFAULT NULL,
  `confirmatory_allowed_quarter` int(11) DEFAULT NULL,
  `screening_allowed_current` int(11) DEFAULT NULL,
  `confirmatory_allowed_current` int(11) DEFAULT NULL,
  `screening_bal_current` int(11) DEFAULT NULL,
  `confirmatory_bal_current` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1