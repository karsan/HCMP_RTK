CREATE TABLE `district_drawing_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `county_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `screening_allocated` int(11) NOT NULL,
  `screening_used` int(11) NOT NULL,
  `confirmatory_allocated` int(11) NOT NULL,
  `confirmatory_used` int(11) NOT NULL,
  `quarter` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `screening_distributed` int(11) DEFAULT NULL,
  `confirmatory_distributed` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1