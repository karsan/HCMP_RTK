CREATE TABLE `dhis_facilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facility_code` int(11) DEFAULT NULL,
  `dhis_facility_code` varchar(200) DEFAULT NULL,
  `facility_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1