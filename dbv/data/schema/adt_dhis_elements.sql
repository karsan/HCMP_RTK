CREATE TABLE `adt_dhis_elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dhis_code` varchar(50) NOT NULL,
  `dhis_name` varchar(150) NOT NULL,
  `dhis_report` varchar(20) NOT NULL,
  `target_report` varchar(20) NOT NULL,
  `target_name` varchar(100) NOT NULL,
  `target_category` varchar(10) NOT NULL,
  `target_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1