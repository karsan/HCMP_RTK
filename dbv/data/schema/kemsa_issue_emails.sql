CREATE TABLE `kemsa_issue_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `county_id` int(11) DEFAULT NULL,
  `email_status` int(11) DEFAULT NULL,
  `issue_date` varchar(45) DEFAULT NULL,
  `date_sent` varchar(45) DEFAULT NULL,
  `sent_to` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1