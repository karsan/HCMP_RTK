CREATE TABLE `facs_presto_fcdrr_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fcdrr_id` int(11) DEFAULT NULL,
  `run_id` varchar(45) DEFAULT NULL,
  `run_date_time` varchar(45) DEFAULT NULL,
  `operator` varchar(45) DEFAULT NULL,
  `reagent_lot_id` varchar(45) DEFAULT NULL,
  `reagent_lot_exp` varchar(45) DEFAULT NULL,
  `patient_id` varchar(45) DEFAULT NULL,
  `inst_qc_passed` varchar(45) DEFAULT NULL,
  `reagent_qc_passed` varchar(45) DEFAULT NULL,
  `cd4` varchar(45) DEFAULT NULL,
  `cd4_percent` varchar(45) DEFAULT NULL,
  `hb` varchar(45) DEFAULT NULL,
  `error_codes` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1