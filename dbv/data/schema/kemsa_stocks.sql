CREATE TABLE `kemsa_stocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commodity_id` int(11) DEFAULT NULL,
  `commodity_code` varchar(50) DEFAULT NULL,
  `mtd_receipts` int(11) DEFAULT NULL,
  `ytd_receipts` int(11) DEFAULT NULL,
  `mtd_issues` int(11) DEFAULT NULL,
  `ytd_issues` int(11) DEFAULT NULL,
  `at_hand` int(11) DEFAULT NULL,
  `stock_date` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1