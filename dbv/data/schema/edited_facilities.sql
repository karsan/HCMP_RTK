CREATE TABLE `edited_facilities` (
  `edited_facilities_id` int(11) NOT NULL AUTO_INCREMENT,
  `facility_id` int(11) DEFAULT NULL,
  `old_facility_code` varchar(45) DEFAULT NULL,
  `new_facility_code` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`edited_facilities_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1