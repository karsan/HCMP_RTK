CREATE TABLE `api_resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `resource_title` varchar(200) DEFAULT NULL,
  `resource_description` varchar(200) DEFAULT NULL,
  `resource_url` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1