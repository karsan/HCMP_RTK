CREATE TABLE `allocations_predefined` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `county` int(11) DEFAULT NULL,
  `district` int(11) DEFAULT NULL,
  `facility_code` int(11) DEFAULT NULL,
  `screening_allocated` int(11) DEFAULT NULL,
  `confirmatory_allocated` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1