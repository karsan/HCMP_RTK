UPDATE `facilities` SET `cd4_enabled` = '1' WHERE `facility_code` = '13847';
INSERT INTO `cd4_facility_device` (`facility_code`, `device`, `enabled`, `created_at`, `updated_at`) VALUES ('13847', '5', '1', NULL, NULL);

UPDATE `facilities` SET `cd4_enabled` = '1' WHERE `facility_code` = '13772';
INSERT INTO `cd4_facility_device` (`facility_code`, `device`, `enabled`, `created_at`, `updated_at`) VALUES ('13772', '4', '1', NULL, NULL);

UPDATE `facilities` SET `cd4_enabled` = '1' WHERE `facility_code` = '13741';
INSERT INTO `cd4_facility_device` (`facility_code`, `device`, `enabled`, `created_at`, `updated_at`) VALUES ('13741', '1', '1', NULL, NULL);

UPDATE `facilities` SET `cd4_enabled` = '1' WHERE `facility_code` = '13745';
INSERT INTO `cd4_facility_device` (`facility_code`, `device`, `enabled`, `created_at`, `updated_at`) VALUES ('13745', '5', '1', NULL, NULL);

UPDATE `facilities` SET `cd4_enabled` = '1' WHERE `facility_code` = '13656';
INSERT INTO `cd4_facility_device` (`facility_code`, `device`, `enabled`, `created_at`, `updated_at`) VALUES ('13656', '4', '1', NULL, NULL);

DELETE FROM `rtk`.`cd4_facility_device` WHERE facility_code='13703' AND device = 2;
