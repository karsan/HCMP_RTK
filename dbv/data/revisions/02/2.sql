DELETE r1 FROM rtk_county_percentage r1,
    rtk_county_percentage r2 
WHERE
    r1.id > r2.id AND r1.county_id = r2.county_id AND r1.month = r2.month;

ALTER TABLE `rtk_county_percentage` 
CHANGE COLUMN `month` `month` INT(11) NOT NULL ;


