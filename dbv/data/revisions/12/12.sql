INSERT INTO `rtk`.`dhis_commodity_category_combos` (`category`, `category_dhis_code`, `category_option`, `category_option_dhis_code`,`rtk_column`) VALUES ('HIV Commodities', 'wJe1BmdWrAM', 'Days out of stock', 'ohYiYpVD7p4','days_out_of_stock');
INSERT INTO `rtk`.`dhis_commodity_category_combos` (`category`, `category_dhis_code`, `category_option`, `category_option_dhis_code`,`rtk_column`) VALUES ('HIV Commodities', 'wJe1BmdWrAM', 'Quantity Used', 'R0j6FcLUYwb','q_used');
INSERT INTO `rtk`.`dhis_commodity_category_combos` (`category`, `category_dhis_code`, `category_option`, `category_option_dhis_code`,`rtk_column`) VALUES ('HIV Commodities', 'wJe1BmdWrAM', 'Quantity Requested for Re-supply', 'Ymv866YRJfo','q_requested');
INSERT INTO `rtk`.`dhis_commodity_category_combos` (`category`, `category_dhis_code`, `category_option`, `category_option_dhis_code`,`rtk_column`) VALUES ('HIV Commodities', 'wJe1BmdWrAM', 'Number of Tests done (include repeats, QA/QC)', 'KcLIg7zOYbn','no_of_tests_done');
INSERT INTO `rtk`.`dhis_commodity_category_combos` (`category`, `category_dhis_code`, `category_option`, `category_option_dhis_code`,`rtk_column`) VALUES ('HIV Commodities', 'wJe1BmdWrAM', 'Positive Adjustment (Receipt from other HF)', 'JuTumBOWYop','positive_adj');
INSERT INTO `rtk`.`dhis_commodity_category_combos` (`category`, `category_dhis_code`, `category_option`, `category_option_dhis_code`,`rtk_column`) VALUES ('HIV Commodities', 'wJe1BmdWrAM', 'Quantity expiring in less than 6 months', 'AfqKHRsbd1I','q_expiring');
INSERT INTO `rtk`.`dhis_commodity_category_combos` (`category`, `category_dhis_code`, `category_option`, `category_option_dhis_code`,`rtk_column`) VALUES ('HIV Commodities', 'wJe1BmdWrAM', 'Beginning Balance', 'YpTjRlaWgI7','beginning_bal');
INSERT INTO `rtk`.`dhis_commodity_category_combos` (`category`, `category_dhis_code`, `category_option`, `category_option_dhis_code`,`rtk_column`) VALUES ('HIV Commodities', 'wJe1BmdWrAM', 'End of Month Physical count', 'K8shOw0pNhd','physical_closing_stock');
INSERT INTO `rtk`.`dhis_commodity_category_combos` (`category`, `category_dhis_code`, `category_option`, `category_option_dhis_code`,`rtk_column`) VALUES ('HIV Commodities', 'wJe1BmdWrAM', 'Negative Adjustment (Issued to Other HF)', 'qX2fr9zJnwe','negative_adj');
INSERT INTO `rtk`.`dhis_commodity_category_combos` (`category`, `category_dhis_code`, `category_option`, `category_option_dhis_code`,`rtk_column`) VALUES ('HIV Commodities', 'wJe1BmdWrAM', 'Quantity Received this Month from Central Warehouses', 'OGUPof2zdrb','q_received');
INSERT INTO `rtk`.`dhis_commodity_category_combos` (`category`, `category_dhis_code`, `category_option`, `category_option_dhis_code`,`rtk_column`) VALUES ('HIV Commodities', 'wJe1BmdWrAM', 'Losses and Wastage', 'GWwYwY6i6U7','losses');
INSERT INTO `rtk`.`dhis_commodity_category_combos` (`category`, `category_dhis_code`, `category_option`, `category_option_dhis_code`,`rtk_column`) VALUES ('HIV Commodities', 'wJe1BmdWrAM', 'Quantity Received From Other Sources', 'hl7D5PiU3Gn','q_recieved_others');

INSERT INTO `rtk`.`dhis_data_elements` (`commodity_id`, `dhis_code`, `display_name`) VALUES ('4', 'CzrZlb2oGMv', 'MOH 643_Rapid HIV 1+2 Test 1 - Screening');
INSERT INTO `rtk`.`dhis_data_elements` (`commodity_id`, `dhis_code`, `display_name`) VALUES ('5', 'rTFvDwdftfT', 'MOH 643_Rapid HIV 1+2 Test 2 - Confirmatory');

INSERT INTO `rtk`.`api_resources` (`resource_title`, `resource_description`, `resource_url`) VALUES ('resources', 'API Resources', 'api/resources');
INSERT INTO `rtk`.`api_resources` (`resource_title`, `resource_description`, `resource_url`) VALUES ('dimensions', 'API Dimensions', 'api/dimensions');
INSERT INTO `rtk`.`api_resources` (`resource_title`, `resource_description`, `resource_url`) VALUES ('facilities', 'API Facility List', 'api/facilities');
INSERT INTO `rtk`.`api_resources` (`resource_title`, `resource_description`, `resource_url`) VALUES ('reports', 'Facility FCDRR Reports Resource', 'api/reports');
INSERT INTO `rtk`.`api_resources` (`resource_title`, `resource_description`, `resource_url`) VALUES ('data_elements', 'Data elements on both RTK and DHIS', 'api/data_elements');


INSERT INTO `rtk`.`api_dimensions` (`parameter`, `description`) VALUES ('period', 'Period required e.g 201711');
INSERT INTO `rtk`.`api_dimensions` (`parameter`, `description`) VALUES ('mfl', 'Facility code e.g 12909');
INSERT INTO `rtk`.`api_dimensions` (`parameter`, `description`) VALUES ('subcounty', 'Subcounty or District required in CAPS. e.g. DAGORETTI');
INSERT INTO `rtk`.`api_dimensions` (`parameter`, `description`) VALUES ('county', 'County required in CAPS. e.g NAIROBI');
INSERT INTO `rtk`.`api_dimensions` (`parameter`, `description`) VALUES ('format', 'Format of data e.g csv or json');
INSERT INTO `rtk`.`api_dimensions` (`parameter`, `description`) VALUES ('commodity_id', 'Commodity id for dataElement required. Either 4 for Screening or 5 for Confirmatory.');
