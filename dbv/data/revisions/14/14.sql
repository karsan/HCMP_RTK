INSERT INTO `rtk`.`api_dimensions` (`parameter`, `description`) VALUES ('dhis_encoding', 'Takes value of either TRUE|true or FALSE|false. Returns dhis reports with or without conversion to dhis coded data in either dhis_csv or dhis_json formats. True by default.');
UPDATE `rtk`.`api_dimensions` SET `description`='Format of data. Either: csv, json, dhis_csv, dhis_json' WHERE `id`='5';
