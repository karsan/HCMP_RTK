UPDATE `facilities` SET `cd4_enabled` = '1' WHERE `facility_code` = '13673';
INSERT INTO `cd4_facility_device` (`facility_code`, `device`, `enabled`, `created_at`, `updated_at`) VALUES ('13673', '4', '1', NULL, NULL);

UPDATE `facilities` SET `cd4_enabled` = '1' WHERE `facility_code` = '13680';
INSERT INTO `cd4_facility_device` (`facility_code`, `device`, `enabled`, `created_at`, `updated_at`) VALUES ('13680', '4', '1', NULL, NULL);

UPDATE `facilities` SET `cd4_enabled` = '1' WHERE `facility_code` = '14009';
INSERT INTO `cd4_facility_device` (`facility_code`, `device`, `enabled`, `created_at`, `updated_at`) VALUES ('14009', '4', '1', NULL, NULL);

UPDATE `facilities` SET `cd4_enabled` = '1' WHERE `facility_code` = '13969';
INSERT INTO `cd4_facility_device` (`facility_code`, `device`, `enabled`, `created_at`, `updated_at`) VALUES ('13969', '4', '1', NULL, NULL);

DELETE FROM `rtk`.`cd4_facility_device` WHERE facility_code='13745' AND device = 3;
DELETE FROM `rtk`.`cd4_facility_device` WHERE facility_code='14009' AND device = 2;
DELETE FROM `rtk`.`cd4_facility_device` WHERE facility_code='13805' AND device = 2;
