ALTER TABLE `rtk`.`lab_commodity_details` 
ADD INDEX `order_id` (`order_id` ASC);

ALTER TABLE `rtk`.`lab_commodity_orders` 
ADD INDEX `order_date` (`order_date` ASC);

