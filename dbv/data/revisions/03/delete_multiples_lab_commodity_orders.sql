DELETE l1 FROM lab_commodity_orders l1,
    lab_commodity_orders l2 
WHERE
    l1.id < l2.id
    AND l1.facility_code = l2.facility_code
	AND MONTH(l1.order_date) = MONTH(l2.order_date)
    AND YEAR(l1.order_date) = YEAR(l2.order_date)