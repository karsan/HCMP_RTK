ALTER TABLE `rtk`.`county_drawing_rights_details` 
ADD COLUMN `screening_total` INT NULL AFTER `year`,
ADD COLUMN `screening_used` INT NULL AFTER `screening_total`,
ADD COLUMN `screening_balance` INT NULL AFTER `screening_used`,
ADD COLUMN `confirmatory_total` INT NULL AFTER `screening_distributed`,
ADD COLUMN `confirmatory_used` INT NULL AFTER `confirmatory_total`,
ADD COLUMN `confirmatory_balance` INT NULL AFTER `confirmatory_used`;

UPDATE `rtk`.`district_drawing_rights` SET `quarter`='4';

