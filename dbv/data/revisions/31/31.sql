
INSERT INTO `lab_commodities` (`commodity_name`, `category`, `unit_of_issue`) VALUES ('Xpert HIV-1 Qualitative Catridge(EID)', '1', '1');
INSERT INTO `lab_commodities` (`commodity_name`, `category`, `unit_of_issue`) VALUES ('Xpert HIV-1 Quantitative Catridge(VL)', '1', '1');
INSERT INTO `lab_commodities` (`commodity_name`, `category`, `unit_of_issue`) VALUES ('Oraquick HIV Self Test', '1', '1');
INSERT INTO `lab_commodities` (`commodity_name`, `category`, `unit_of_issue`) VALUES ('HIV/Syphilis Duo RDT Kit', '1', '1');

UPDATE lab_commodity_orders 
SET 
    `order_date` = '2018-03-01',
    `report_for` = 'February'
WHERE
    district_id = 297 AND order_date='2018-04-02';
