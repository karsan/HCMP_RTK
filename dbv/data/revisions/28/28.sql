ALTER TABLE `dhis_fcdrr_data` 
ADD COLUMN `losses` INT NULL AFTER `no_of_tests_done`,
ADD COLUMN `positive_adj` INT NULL AFTER `losses`,
ADD COLUMN `negative_adj` INT NULL AFTER `positive_adj`,
ADD COLUMN `q_expiring` INT NULL AFTER `negative_adj`,
ADD COLUMN `days_out_of_stock` INT NULL AFTER `q_expiring`;
ADD COLUMN `import_status` INT NULL DEFAULT 0 AFTER `mfl_month_year`;



