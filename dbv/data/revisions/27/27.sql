ALTER TABLE `kemsa_issue_emails` 
ADD COLUMN `quarter` INT NULL AFTER `email_status`,
ADD COLUMN `year` INT NULL AFTER `quarter`;
