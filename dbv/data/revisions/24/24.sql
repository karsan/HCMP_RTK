ALTER TABLE `rtk`.`allocation_details` 
CHANGE COLUMN `amc_s` `amc_s` INT(11) NULL ,
CHANGE COLUMN `mmos_s` `mmos_s` INT(11) NULL ,
CHANGE COLUMN `amc_c` `amc_c` INT(11) NULL ,
CHANGE COLUMN `mmos_c` `mmos_c` INT(11) NULL ,
CHANGE COLUMN `amc_t` `amc_t` INT(11) NULL ;
