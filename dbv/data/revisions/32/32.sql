DELETE FROM `rtk`.`lab_commodities` WHERE `id`='24';
DELETE FROM `rtk`.`lab_commodities` WHERE `id`='25';
INSERT INTO `rtk`.`lab_commodities` (`id`, `commodity_name`, `category`, `unit_of_issue`) VALUES ('24', 'HIV/Syphilis Duo RDT Kit', '1', '1');
INSERT INTO `rtk`.`lab_commodities` (`id`, `commodity_name`, `category`, `unit_of_issue`) VALUES ('25', 'Oraquick HIV Self Test', '1', '1');

ALTER TABLE `lab_commodity_details` 
ADD COLUMN `self_anc` VARCHAR(45) NOT NULL DEFAULT 0,
ADD COLUMN `self_tb_sti` VARCHAR(45) NOT NULL DEFAULT 0,
ADD COLUMN `self_opd` VARCHAR(45) NOT NULL DEFAULT 0,
ADD COLUMN `self_outreach` VARCHAR(45) NOT NULL DEFAULT 0,
ADD COLUMN `self_ccc_pitc` VARCHAR(45) NOT NULL DEFAULT 0,
ADD COLUMN `self_vct` VARCHAR(45) NOT NULL DEFAULT 0;


ALTER TABLE `cd4_county_percentage` 
ADD COLUMN `created_at` TIMESTAMP(0) NULL DEFAULT CURRENT_TIMESTAMP AFTER `reported_month`,
ADD COLUMN `updated_at` TIMESTAMP(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `created_at`;

UPDATE `rtk`.`access_level` SET `user_indicator`='cd4_manager' WHERE `id`='16';

INSERT INTO `rtk`.`user` (`fname`, `lname`, `email`, `username`, `password`, `activation`, `usertype_id`, `telephone`, `district`, `partner`, `facility`, `created_at`, `updated_at`, `status`, `county_id`, `email_recieve`, `sms_recieve`) VALUES ('Tim', 'CD4', 'tngugicd4@gmail.com', 'tngugicd4@gmail.com', 'b56578e2f9d28c7497f42b32cbaf7d68', '', '16', '', '1', '0', '12905', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1', '1', '1', '1');

/*INSERT MENU ENTRY*/
INSERT INTO `menu` (`menu_describe`, `menu_text`, `menu_url`, `user_group`, `parent_status`) VALUES ('cd4_facility_management', 'CD4 Facility Management', 'cd4_management/cd4_facility_management', '8', '0');
INSERT INTO `menu` (`menu_describe`, `menu_text`, `menu_url`, `user_group`, `parent_status`) VALUES ('cd4_facility_management', 'CD4 Facility Management', 'cd4_management/cd4_facility_management', '16', '0');

-- INSERT INTO `menu` (`menu_describe`, `menu_text`, `menu_url`, `user_group`, `parent_status`) VALUES ('cd4_user_management', 'CD4 User Management', 'cd4_management/cd4_user_management', '8', '0');
INSERT INTO `menu` (`menu_describe`, `menu_text`, `menu_url`, `user_group`, `parent_status`) VALUES ('cd4_user_management', 'CD4 User Management', 'cd4_management/cd4_user_management', '16', '0');

