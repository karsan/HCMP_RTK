<?php
/*
Richard Karsan 2017
*/
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rtk_Admin extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        ini_set('memory_limit', '-1');
        ini_set('max_input_vars', 3000);
        ini_set('post_max_size', '64M'); 
        ini_set('upload_max_filesize', '64M');
        require_once(APPPATH.'controllers/rtk_management.php');
        // echo ini_get('upload_max_filesize');exit;

        /*error_reporting(1);
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        ini_set('display_errors', 1);*/

        $this -> load -> library(array('hcmp_functions', 'form_validation'));
    }

    public function index()
    {
        // echo "SUCCESS";exit;
        $data['title'] = "Admin Interface";
        // $data['banner_text'] = 'Admin';
        $data['content_view'] = 'rtk/admin/dashboard';
        // echo "<pre>";print_r($data);exit;

        $this->load->view('rtk/template', $data);
    }

    public function admin_allocations($success_status = NULL)
    {
        $success_status = (isset($success_status) && $success_status > 0)? $success_status:NULL;

        $month_rollback = 6;
        for ($i=0; $i < $month_rollback; $i++) { 
            $month = date('m', strtotime("-$i month"));
            $months_data[] = $month;
            $year = date('Y', strtotime("-$i month"));
            $year_data[] = $year;
            
            $month_year = date('F Y', strtotime("-$i month"));
            $month_year_data[] = $month_year;
        }

        // $data['months'] = $months_data;
        // $data['month_year_data'] = $month_year_data;
        // $sth = date('m-Y',strtotime($month_year_data[0]));
        // echo $sth;exit;
        // echo "<pre>";print_r($month_year_data);exit;
        // echo "<pre>";print_r($year_data);exit;
        // echo count($year_data);exit;
        $final_data = array();
        for ($i=0; $i < 6; $i++) { 
            $year = $year_data[$i];
            $month = $months_data[$i];
            $firstdate = $year . '-' . $month . '-01';
            $num_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $lastdate = $year . '-' . $month . '-' . $num_days;
            
            $query = "
                SELECT
                    c.zone,
                    c.county,
                    d.id as district_id,
                    d.district,
                    a.facility_code,
                    a.facility_name,
                    a.created_at,
                    a.month
                FROM
                    allocation_details a,
                    districts d,
                    counties c
                WHERE
                    d.id = a.district_id AND c.id = d.county 
                GROUP BY d.id,MONTH(month),YEAR(month) ORDER BY month DESC limit 500";

            // echo $query;exit;
            $result = $this->db->query($query)->result_array();

            array_push($final_data, $result);
        }

        // echo "<pre>";print_r($final_data);exit;
        $final_array = call_user_func_array('array_merge', $final_data);
        // $allocation_final = array_map("unserialize", array_unique(array_map("serialize", $allocation_final)));
        // $allocation_final = array_values($allocation_final);
        // echo "<pre>";print_r($final_array);exit;

        // echo $query;exit;
        // $order_data = $this->db->query($query)->result_array();
        // echo "<pre>";print_r($order_data);exit;

        $data['title'] = "Admin Allocations";
        $data['banner_text'] = 'Admin Allocations';
        $data['content_view'] = 'rtk/admin/allocations';
        $data['success_status'] = $success_status;
        $data['allocations'] = $final_array;

        // echo "<pre>";print_r($data);exit;

        $this->load->view('rtk/template', $data);
    }

    public function admin_delete_allocation($district_id,$date)
    {
        // echo $district_id.' '.$date;exit;
        $month = date('m',strtotime($date));
        $year = date('Y',strtotime($date));

        $allocation_details_sql = "DELETE FROM allocation_details WHERE district_id = $district_id AND month = '$date'";
        $allocation_details_deletion = $this->db->query($allocation_details_sql);

        $labs_orders_sql = "DELETE FROM lab_commodity_orders WHERE district_id = $district_id AND order_date = '$date'";
        $labs_orders_deletion = $this->db->query($labs_orders_sql);

        $labs_commodity_sql = "DELETE FROM lab_commodity_details WHERE district_id = $district_id AND DATE(created_at)= '$date'";
        $labs_commodity_deletion = $this->db->query($labs_commodity_sql);

        $allocations_sql = "DELETE FROM allocations WHERE district_id = $district_id AND MONTH(month) = '$month' AND YEAR(month) = '$year'";

        // echo "<pre>";print_r($allocations_sql);exit;
        $allocations_deletion = $this->db->query($allocations_sql);

        // echo "<pre>";print_r($labs_commodity_deletion);exit;

        redirect('rtk_admin/admin_allocations/1');
    }

    public function admin_sanitize_tables($district_id = NULL)
    {
        $query = "DELETE a FROM lab_commodity_details AS a,
                        lab_commodity_details AS b 
                    WHERE
                        (a.facility_code = b.facility_code
                        AND (MONTH(a.created_at) = MONTH(b.created_at))
                        AND a.id < b.id";

        $deletion = $this->db->query($query);

        echo "Successful cleaning of tables: lab_commodity_details";
    }

    public function admin_allocation_specific($district_id,$order_date)
    {
        // echo $district_id." ".$osrder_date;exit;
        /*$query = "
                SELECT 
                    c.zone as ,
                    c.county,
                    d.district,
                    a.*
                FROM
                    allocation_details a,
                    districts d,
                    counties c
                WHERE
                    d.id = a.district_id AND c.id = d.county
                    GROUP BY DAY(created_at),d.id;
            ";
            */
        $query = "
            SELECT
                d.id AS district_id,
                d.district,
                l.facility_code,
                f.facility_name,
                l.id AS order_id,
                l.order_date AS order_date
            FROM
                lab_commodity_orders l,districts d,facilities f
            WHERE l.district_id= d.id AND f.facility_code = l.facility_code AND l.district_id = $district_id AND order_date = '$order_date'
                ";
        $result = $this->db->query($query)->result_array();

        // echo "<pre>";print_r($result);exit;

        $data['title'] = "Allocations Specific";
        $data['banner_text'] = 'Admin Allocations Specific';
        $data['content_view'] = 'rtk/admin/allocations_specific';
        $data['order_details'] = $result;

        // echo "<pre>";print_r($data);exit;

        $this->load->view('rtk/template', $data);
    }

    public function drawing_rights_dashboard($success='')
    {
        $quarter_data = $this->get_year_quarter(NULL,NULL,NULL,1);
        // echo "<pre>";print_r($quarter_data);exit;

        $counties_query = "
            SELECT
                c.id AS county_id,
                c.county,
                c.zone,
                cdr.*
            FROM
                county_drawing_rights cdr, counties c
            WHERE c.id = cdr.county_id;
        ";
        $drawing_rights_counties = $this->db->query($counties_query)->result_array();

        $districts_query = "
            SELECT
                d.id as district_id,
                d.district,
                ddr.*
            FROM
                districts d,district_drawing_rights ddr
                WHERE d.id = ddr.district_id
        ";
        $drawing_rights_districts = $this->db->query($districts_query)->result_array();

        $queryy="SELECT * FROM counties";
        $counties = $this->db->query($queryy)->result_array();

        $queryyy="SELECT * FROM districts";
        $districts = $this->db->query($queryyy)->result_array();

        $data['drawing_rights_counties'] = $drawing_rights_counties;
        $data['drawing_rights_districts'] = $drawing_rights_districts;
        $data['districts'] = $districts;
        $data['quarter_data'] = $quarter_data;
        $data['counties'] = $counties;
        $data['title'] = "Allocation CSV";
        $data['banner_text'] = '';
        $data['success'] = $success;
        $data['months'] = $allowed_months;
        $data['content_view'] = 'rtk/admin/drawing_rights_dash';
        // $data['final_dets'] = $final_dets;
        // echo "<pre>";print_r($counties);exit;
        $this->load->view('rtk/template', $data);
    }

    public function drawing_rights_master($operation,$level)//matata
    {
        //adds or updates
        // echo $operation.' '.$level;exit;
        $data = $this->input->post();
        $entity_id = $data['entity_id'];
        $screening = $data['screening'];
        $confirmatory = $data['confirmatory'];

        $screening = ($screening>0)?$screening:'0';
        $confirmatory = ($confirmatory>0)?$confirmatory:'0';

        // echo $entity_id.' '.$screening.' '.$confirmatory;exit;
        // echo "<pre>";print_r($data);exit;
        switch ($level) {
            case 'county':
                // echo "County";
                $county_id = $entity_id;
                $data_array['county_id'] = $county_id;
                $data_array['screening_total'] = $screening;
                $data_array['confirmatory_total'] = $confirmatory;

                if ($county_id > 0) {
                    // $check = $this->db->get_where('county_drawing_rights', array('county_id' => $county_id));
                    $checker = $this->db->query("SELECT id FROM county_drawing_rights WHERE county_id = $county_id LIMIT 1")->result_array();
                    $check = count($checker);

                    // echo "<pre>";print_r($check);exit;
                    $operation = ($check>0)?"update":"addition";

                    if ($operation == "addition" ) {
                        $result = $this->db->insert('county_drawing_rights',$data_array);

                    }elseif($operation == "update"){
                        $this->db->where('county_id', $county_id);

                        // echo "<pre>";print_r($data_array);exit;

                        $result = $this->db->update('county_drawing_rights', $data_array);
                    }

                    $redirect = 'rtk_admin/drawing_rights_dashboard/1';
                }else{
                    $redirect = 'rtk_admin/drawing_rights_dashboard/2';
                }
                break;

            case 'district':
                echo "District";
                 // echo "County";
                $district_id = $entity_id;
                if ($district_id > 0) {
                    // $check = $this->db->get_where('county_drawing_rights', array('county_id' => $county_id));
                    $checker = $this->db->query("SELECT id FROM district_drawing_rights WHERE district_id = $district_id LIMIT 1")->result_array();
                    $check = count($checker);

                    // echo "<pre>";print_r($check);exit;
                    $county = $this->db->query("SELECT county FROM districts WHERE id = $district_id")->result_array();
                    $county = array_pop($county);
                    $county_id = $county['county'];
                    // echo "<pre>";print_r($county_id);exit;

                    $operation = ($check>0)?"update":"addition";
                    $data_array['county_id'] = $county_id;
                    $data_array['district_id'] = $district_id;
                    $data_array['screening_allocated'] = $screening;
                    $data_array['confirmatory_allocated'] = $confirmatory;
                    $data_array['user_id'] = 1;
                    if ($operation == "addition" ) {

                        $result = $this->db->insert('district_drawing_rights',$data_array);

                    }elseif($operation == "update"){
                        $this->db->where('district_id', $district_id);

                        // echo "<pre>";print_r($data_array);exit;

                        $result = $this->db->update('district_drawing_rights', $data_array);
                    }

                    $redirect = 'rtk_admin/drawing_rights_dashboard/1';
                }else{
                    $redirect = 'rtk_admin/drawing_rights_dashboard/2';
                }
                break;

            default:
                # code...
                break;
        }

        // echo $result;exit;
        redirect($redirect);

    }

    public function drawing_rights_csv($value = '')
    {
        // error_reporting(1);
        // echo "SUCCESS";exit;
        $data = $this->input->post();
        // $selected_month = $data['month'];
        // echo "<pre>";print_r($data);exit;
        $user_id = $this->session->userdata('user_id');


        $month = date('F', strtotime($selected_month));
        $year = date('Y', strtotime($selected_month));
        // echo "<pre>";print_r($year);exit;

        if (isset($_FILES['file']) && $_FILES['file']['size'] > 0) {
            $ext = pathinfo($_FILES["file"]['name'], PATHINFO_EXTENSION);
        // echo "THIS ".$ext;exit;
        //echo $_FILES["file"]["tmp_name"];exit;
            if ($ext == 'xls') {
                $excel2 = PHPExcel_IOFactory::createReader('Excel5');
            } else if ($ext == 'xlsx') {
                $excel2 = PHPExcel_IOFactory::createReader('Excel2007');
            } else if ($ext == 'csv') {
                $excel2 = PHPExcel_IOFactory::createReader('CSV');
            } else {
                die('Invalid file format given' . $_FILES['file']);
            }

            $excel2 = $objPHPExcel = $excel2->load($_FILES["file"]["tmp_name"]);
        // Empty Sheet

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();

            $highestColumn = $sheet->getHighestColumn();

        // echo $highestColumn.' '.$highestRow;exit;
            $rowData_temp = array();

            for ($row = 2; $row <= $highestRow; $row++) {
        //  Read a row of data into an array
                $rowData_temp = $objPHPExcel->getActiveSheet()->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $rowData_final[] = array_pop($rowData_temp);
            }

        // echo "<pre>";print_r($rowData_final);exit;

            $rowData_final_count = count($rowData_final);
            $blank_cells = 0;

            $data_array = $drawing_rights_final_array = array();
            foreach ($rowData_final as $row_data => $data) {
                // echo "<pre>";print_r($data);
                $county_name = $data[0];
                $zone = $data[1];

                // $duration = $data[2];
                // $screening_amt = $data[3];
                // $confirmatory_amt = $data[4];

                // $duration = $data[2];
                $screening_amt = $data[2];
                $confirmatory_amt = $data[3];

                if ($county_name !='') {
                    $county_name = trim($county_name,' ');
                    $county_name = str_replace("'", '',$county_name);
                    // echo "<pre>";print_r($county_name);exit;

                    $query = "SELECT id,zone FROM counties WHERE county = '$county_name'";
                    $result = $this->db->query($query)->result_array();
                    // echo "<pre>";print_r($result[0]['id']);
                    $county_id = $result[0]['id'];
                    // $zone = $result[0]['zone'];

                    $data_array['county_id'] = $county_id;
                    $data_array['zone'] = $zone;
                    $data_array['duration'] = $duration;
                    $data_array['screening_total'] = $screening_amt;
                    $data_array['confirmatory_total'] = $confirmatory_amt;

                    array_push($drawing_rights_final_array, $data_array);
                } else {
                    $blank_cells = $blank_cells + 1;
                }
            }

            // echo "<pre>";print_r($drawing_rights_final_array);exit;
            $insert = $this->db->insert_batch('county_drawing_rights',$drawing_rights_final_array);
            // echo "<pre>";print_r($insert);exit;

        }//end of file input if
        else {
            echo "NO FILE UPLOADED";
        }

        // echo "SUCCESSFUL UPLOAD";
        redirect('rtk_admin/allocation_csv_interface/1');
    }

    public function drawing_rights_reversal()
    {
        // echo "<pre>";print_r($posted_data);exit;
        $posted_data = $this->input->post();
        $county_id = $posted_data['entity_id'];

        $query = "
        UPDATE `county_drawing_rights` 
        SET 
        `screening_allowed_current` = screening_allowed_current + screening_distributed,
        `confirmatory_allowed_current` = confirmatory_allowed_current + confirmatory_distributed
        WHERE
        `county_id` = '$county_id';
        ";

        $result = $this->db->query($query);

        $query_2 = "
        UPDATE `county_drawing_rights` 
        SET 
        `screening_distributed` = 0,
        `confirmatory_distributed` = 0
        WHERE
        `county_id` = '$county_id';
        ";
        $result_2 = $this->db->query($query_2);

        $query_3 = "UPDATE `district_drawing_rights` SET `screening_allocated`='0',`confirmatory_allocated`='0' WHERE `county_id`='$county_id'";
        $result_3 = $this->db->query($query_3);
        
        $redirect = 'rtk_admin/drawing_rights_dashboard/1';
        redirect($redirect);
    }

    public function drawing_rights_quarterly_update($operation,$level)//matata
    {
        //adds or updates
        // echo $operation.' '.$level;exit;
        $data = $this->input->post();
        // echo "<pre>";print_r($data);exit;
        $entity_id = $data['entity_id'];
        $quarter_data = $data['quarter'];

        $q_data = explode('_', $quarter_data);
        // echo "<pre>";print_r($q_data);exit;
        $quarter = $q_data[0];
        $year = $q_data[1];
        $update = $this->update_county_drawing_rights($entity_id,$quarter,$year);
        // echo "<pre>";print_r($update);exit;
        $redirect = 'rtk_admin/drawing_rights_dashboard/1';
        // echo $result;exit;
        redirect($redirect);

    }

    public function admin_facilities_dashboard($success_status = '') {
        // ini_set('memory_limit','-1');
        $success_status = (isset($success_status) && $success_status > 0)? $success_status:NULL;
        $permissions='super_permissions';
        $data['title'] = "Facility Management";

        $facilities = Admin::get_facility_data();
        // echo "<pre>";print_r($facilities);exit;
        $data['facilities'] = $facilities;
        $data['title'] = "Admin facilities";
        $data['banner_text'] = '';
        $data['success_status'] = $success_status;
        $data['months'] = $allowed_months;
        $data['content_view'] = 'rtk/admin/facility_management';

        // echo "<pre>";print_r($data);exit;
        $this->load->view('rtk/template', $data);
    }

    public function admin_facilities($action,$facility_id)
    {
        switch ($action) {
            case 'edit':

                $facility_data = Admin::get_facility_data($facility_id);
                $facility_data = array_pop($facility_data);
                $district_data = Admin::get_district_data();

                // echo "<pre>";print_r($district_data);exit;
                $data['facility_data'] = $facility_data;
                $data['districts'] = $district_data;
                $data['content_view'] = 'rtk/admin/facility_edit';
               
                // echo "<pre>";print_r($data);exit;
                $this->load->view('rtk/template', $data);

                break;
            case 'view':
                $facility_data = Admin::get_facility_data($facility_id);
                $facility_data = array_pop($facility_data);
                $district_data = Admin::get_district_data();

                // echo "<pre>";print_r($facility_data);exit;
                $data['facility_data'] = $facility_data;
                $data['districts'] = $district_data;
                $data['content_view'] = 'rtk/admin/facility_specific_row';
               
                // echo "<pre>";print_r($data);exit;
                $this->load->view('rtk/template', $data);
                break;

            case 'delete':
                //echo "DELETE".$facility_id;
                $this->db->delete('facilities', array('id' => $facility_id)); 
                redirect('rtk_admin/admin_facilities_dashboard/2');
                break;
            
            default:
                # code...
                break;
        }
        
    }

    public function admin_facility_edit()
    {   
        $data = $this->input->post();

        // echo "<pre>";print_r($data);exit;
        $facility_id = $data['facility_id'];
        $old_facility_code = $data['old_facility_code'];
        $facility_code = $data['facility_code'];
        $facility_name = $data['facility_name'];
        $district = $data['district'];

        $facility_update_data = array(
               'facility_code' => $facility_code,
               'facility_name' => $facility_name,
               'district' => $district
            );

        $this->db->where('id', $facility_id);
        $facilities_update = $this->db->update('facilities', $facility_update_data); 

        $edit_data = array(
                'facility_id' => $facility_id,
                'old_facility_code' => $old_facility_code,
                'new_facility_code' => $facility_code
            );

        $edit_data_insert = $this->db->insert('edited_facilities',$edit_data);

        $lcd_update_query = "UPDATE `lab_commodity_details` SET `facility_code`='$facility_code' WHERE `facility_code`='$old_facility_code'";
        $lcd_update = $this->db->query($lcd_update_query);

        $lco_update_query = "UPDATE `lab_commodity_orders` SET `facility_code`='$facility_code' WHERE `facility_code`='$old_facility_code'";
        $lco_update = $this->db->query($lco_update_query);
        // echo "<pre>".$lcd_update_query;
        // echo "<pre>".$lco_update_query;
        // exit;
        // echo "<pre>";print_r($facilities_update);exit;
        redirect('rtk_admin/admin_facilities_dashboard/1');
    }

    public function national_stockcard_admin($value='')
    {
        rtk_management::national_stockcard_v2();
    }

    public function cd4_sites_interface($success=NULL)
    {
        // error_reporting(E_ALL);
        // echo $success;exit;
        $success = (isset($success) && $success > 0) ? $success : NULL;

        // echo "<pre>";print_r($months);exit;
        $data['title'] = "CD4 sites CSV";
        $data['banner_text'] = '';
        $data['success'] = $success;
        $data['content_view'] = 'rtk/admin/cd4_sites_upload';
        // $data['final_dets'] = $final_dets;
        $this->load->view('rtk/template', $data);
    
    }

    public function cd4_sites_upload()
    {
        // error_reporting(1);
        $data = $this->input->post();
        $user_id = $this->session->userdata('user_id');

        if (isset($_FILES['file']) && $_FILES['file']['size'] > 0) {
            $ext = pathinfo($_FILES["file"]['name'], PATHINFO_EXTENSION);
        // echo "THIS ".$ext;exit;
        //echo $_FILES["file"]["tmp_name"];exit;
            if ($ext == 'xls') {
                $excel2 = PHPExcel_IOFactory::createReader('Excel5');
            } else if ($ext == 'xlsx') {
                $excel2 = PHPExcel_IOFactory::createReader('Excel2007');
            } else if ($ext == 'csv') {
                $excel2 = PHPExcel_IOFactory::createReader('CSV');
            } else {
                die('Invalid file format given' . $_FILES['file']);
            }

            $excel2 = $objPHPExcel = $excel2->load($_FILES["file"]["tmp_name"]);
            // Empty Sheet

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();

            $highestColumn = $sheet->getHighestColumn();

            // echo $highestColumn.' '.$highestRow;exit;
            $rowData_temp = array();

            for ($row = 2; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
                $rowData_temp = $objPHPExcel->getActiveSheet()->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $rowData_final[] = array_pop($rowData_temp);
            }

            // echo "<pre>";print_r($rowData_final);exit;

            $rowData_final_count = count($rowData_final);
            $blank_cells = 0;

            $screening = $confirmatory = $allocation_data_array = array();
            foreach ($rowData_final as $row_data => $data) {
                // echo "<pre>";print_r($data);
                $facility_code = rtrim($data[0]);
                $facility_device = rtrim($data[4]);

                // echo "<pre>";print_r($facility_device);exit;
                // $facility_code = 15077;
                if (is_numeric($facility_code) && $facility_code > 0 && $facility_code != '') {
                    $update_facilities_query = "UPDATE `facilities` SET `cd4_enabled`='1' WHERE `facility_code`='$facility_code'";
                    // echo $update_facilities_query;exit;
                    $update_facilities = $this->db->query($update_facilities_query);

                    // echo "<pre>";print_r($equipment_check);exit;

                    $cd4_equipment_check_query = "SELECT * FROM cd4_facility_device WHERE facility_code = '$facility_code' LIMIT 1";
                    $cd4_equipment_check = $this->db->query($cd4_equipment_check_query);
                    // echo "<pre>";print_r($cd4_equipment_check);exit;

                    if (!is_null($cd4_equipment_check) && $cd4_equipment_check !='') {
                        $cd4_equipment_check = $cd4_equipment_check->result_array();
                        $cd4_equipment_check = array_pop($cd4_equipment_check);
                        // echo "<pre>";print_r($cd4_equipment_check);exit;

                        $equipment_check_query = "SELECT * FROM cd4_equipments WHERE description LIKE '%$facility_device%'";
                        $equipment_check = $this->db->query($equipment_check_query)->result_array();
                        // echo "<pre>";print_r($equipment_check);exit;
                        if (!is_null($equipment_check) && $equipment_check !='') {
                            $device_id = $equipment_check[0]['ID'];

                            $the_data = array(
                                'facility_code' => $facility_code, 
                                'device' => $device_id, 
                                'enabled' => 1
                                );

                            $insertion = $this->db->insert('cd4_facility_device',$the_data);

                            echo "<pre>".$facility_code." ".$device_id." ".$insertion;
                        } else {
                            echo "THERE IS NO EQUIPMENT MATCHING: ".$facility_device;
                            break;
                        }

                    } else {
                        $equipment_check_query = "SELECT * FROM cd4_equipments WHERE description LIKE '%$facility_device%'";
                        $equipment_check = $this->db->query($equipment_check_query)->result_array();
                        
                        $device_id = $equipment_check[0]['ID'];

                        $update_data = array(
                                'facility_code' => $facility_code, 
                                'device' => $device_id, 
                                'enabled' => 1
                                );

                        $update = $this->db->update('cd4_facility_device',$update_data);
                    }
                    
                } else {
                    $blank_cells = $blank_cells + 1;
                }

            }

        }//end of file input if
        else {
            echo "NO FILE UPLOADED";
        }

        redirect('rtk_management/cd4_sites_interface/1');
    
    }

    public function admin_move_allocation($district_id = NULL,$allocation_month = NULL,$destination_month = NULL)
    {
        // echo $district_id." ".$allocation_month." ".$destination_month;exit;

        // echo $district_id.' '.$date;exit;
        $aday = date('d',strtotime($allocation_month));
        $amonth_ = date('m',strtotime($allocation_month));
        $ayear = date('Y',strtotime($allocation_month));

        $dday = date('d',strtotime($destination_month));
        $dmonth_ = date('m',strtotime($destination_month));
        $dyear = date('Y',strtotime($destination_month));

        // 2013-10-20 15:00:32
        $dtimestamp = date('Y-m-d H:m:s',strtotime($destination_month));
        // echo "<pre>".$day;
        // echo "<pre>".$month_;
        // echo "<pre>".$year;exit;
        // echo $dtimestamp;exit;

        $stamp_ = sprintf('%s-%02s', $ayear, $amonth_);
        $old_date = $stamp_."-01";

        $stamp = sprintf('%s-%02s', $dyear, $dmonth_);
        $new_date = $stamp."-01";


        $allocation_details_sql = "
        UPDATE allocation_details 
        SET 
            month = '$new_date'
        WHERE
            district_id = '$district_id'
                AND MONTH(month) = MONTH('$old_date')
                AND YEAR(month) = YEAR('$old_date')";

        $allocations_update_sql = "
        UPDATE `allocations` 
        SET `month`='$new_date' 
        WHERE district_id = '$district_id'
        AND MONTH(month) = MONTH('$old_date')
        AND YEAR(month) = YEAR('$old_date')";

        // echo $allocation_details_sql;exit;
        $labs_orders_update_sql = "
        UPDATE lab_commodity_orders 
        SET created_at= $dtimestamp
        WHERE district_id = '$district_id'
        AND MONTH(created_at) = MONTH('$old_date')
        AND YEAR(created_at) = YEAR('$old_date')";

        $labs_commodity_update_sql = "
        UPDATE lab_commodity_details 
        SET created_at= $dtimestamp
        WHERE district_id = '$district_id'
        AND MONTH(created_at) = MONTH('$old_date')
        AND YEAR(created_at) = YEAR('$old_date')";



        $update_allocation_details = $this->db->query($allocation_details_sql);
        $update_allocations = $this->db->query($allocations_update_sql);
        $labs_orders_update = $this->db->query($labs_orders_update_sql);
        $labs_commodity_update = $this->db->query($labs_commodity_update_sql);

        // echo $update_allocations;exit;
        redirect('rtk_admin/admin_allocations/1');

    }

    public function admin_post_move_allocation()
    {
        $posted = $this->input->post();
        // echo "<pre>";print_r($posted);exit;
        $district_id = $posted['district_id'];
        $allocation_month = $posted['allocation_month'];
        $destination_month = $posted['destination_month'];

        $this->admin_move_allocation($district_id,$allocation_month,$destination_month);
    }

    public function admin_move_allocation_form($district_id = NULL,$date = NULL)
    {
        // error_reporting(E_ALL);
        // echo $success;exit;
        // echo $district_id." ".$month;exit;
        $allocation_month = date('F',strtotime($date));
        $allocation_year = date('Y',strtotime($date));

        $success = (isset($success) && $success > 0) ? $success : NULL;
        $month_rollback = 6;
        for ($i=0; $i < $month_rollback; $i++) { 
            $month = date('m', strtotime("-$i month"));
            $months_data[] = $month;
            $year = date('Y', strtotime("-$i month"));
            $year_data[] = $year;
            
            $month_year = date('F Y', strtotime("-$i month"));
            $month_year_data[] = $month_year;
        }

        // $data['months'] = $months_data;
        $district_name = districts::get_district_name_($district_id);
        // echo "<pre>";print_r($district_name);exit;
        // echo "<pre>";print_r($months);exit;
        $data['month_year_data'] = $month_year_data;
        $data['title'] = "Move allocation";
        $data['banner_text'] = '';
        $data['direction'] = $direction;
        $data['district_id'] = $district_id;
        $data['district_name'] = $district_name['district'];
        $data['allocation_month'] = $allocation_month;
        $data['allocation_year'] = $allocation_year;
        $data['content_view'] = 'rtk/admin/move_allocation';
        // echo "<pre>";print_r($data);exit;
        $this->load->view('rtk/template', $data);
        // redirect('rtk_management/cd4_sites_interface/1');
    }

    public function fcdrr_management($success_status=NULL)
    {
        $success_status = (isset($success_status) && $success_status > 0)? $success_status:NULL;

        $year = date('Y');
        $query = "
                SELECT 
                    c.id AS county_id,
                    c.county,
                    d.id AS district_id,
                    d.district,
                    f.facility_code,
                    f.facility_name,
                    l.id,
                    l.order_date,
                    l.district_id,
                    l.compiled_by,
                    l.facility_code
                FROM
                    lab_commodity_orders l,
                    facilities f,
                    districts d,
                    counties c
                WHERE
                    l.district_id = d.id
                    AND d.county = c.id
                    AND l.facility_code = f.facility_code
                    AND YEAR(l.order_date) = $year
                ORDER BY order_date DESC LIMIT 20000";

        // echo $query;exit;
        $result = $this->db->query($query)->result_array();
        // echo "<pre>";print_r($result);exit;
        $data['title'] = "Admin Allocations";
        $data['banner_text'] = 'Admin Allocations';
        $data['lab_order_list'] = $result;
        $data['content_view'] = 'rtk/admin/fcdrr_management';
        $data['success_status'] = $success_status;
        $data['allocations'] = $final_array;

        // echo "<pre>";print_r($data);exit;

        $this->load->view('rtk/template', $data);
    
    }

    public function admin_delete_fcdrr($order_id)
    {
        $q1 = "
        DELETE FROM lab_commodity_details WHERE order_id = $order_id;
        DELETE FROM lab_commodity_orders WHERE id = $order_id;
        ";
        $result = $this->db->query($q1);

        redirect('rtk_admin/fcdrr_management/1');
        
    }

    public function admin_delete_fcdrr_duplicates($order_id)
    {
        $q1 = "
        DELETE FROM lab_commodity_details WHERE order_id = $order_id;
        DELETE FROM lab_commodity_orders WHERE id = $order_id;
        ";

        $o_details_q = "SELECT * FROM lab_commodity_orders WHERE order_id = $order_id";
        $result = $this->db->query($o_details_q)->result_array();

        echo "<pre>";print_r($result);exit;
        $result = $this->db->query($q1);

        redirect('rtk_admin/fcdrr_management/1');
        
    }

    public function kemsa_issues_excel_interface($success=NULL)
    {
        // error_reporting(E_ALL);
        // echo $success;exit;
        $success = (isset($success) && $success > 0) ? $success : NULL;

        // echo "<pre>";print_r($months);exit;
        $data['title'] = "KEMSA issues excel CSV";
        $data['banner_text'] = '';
        $data['success'] = $success;
        $data['content_view'] = 'rtk/admin/kemsa_issues_upload';
        // $data['final_dets'] = $final_dets;
        $this->load->view('rtk/template', $data);
    
    }

    public function kemsa_issues_excel_upload()
    {
        // error_reporting(1);
        $data = $this->input->post();
        $user_id = $this->session->userdata('user_id');

        if (isset($_FILES['file']) && $_FILES['file']['size'] > 0) {
            $ext = pathinfo($_FILES["file"]['name'], PATHINFO_EXTENSION);
        // echo "THIS ".$ext;exit;
        //echo $_FILES["file"]["tmp_name"];exit;
            if ($ext == 'xls') {
                $excel2 = PHPExcel_IOFactory::createReader('Excel5');
            } else if ($ext == 'xlsx') {
                $excel2 = PHPExcel_IOFactory::createReader('Excel2007');
            } else if ($ext == 'csv') {
                $excel2 = PHPExcel_IOFactory::createReader('CSV');
            } else {
                die('Invalid file format given' . $_FILES['file']);
            }

            $excel2 = $objPHPExcel = $excel2->load($_FILES["file"]["tmp_name"]);
            // Empty Sheet

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();

            $highestColumn = $sheet->getHighestColumn();

            // echo $highestColumn.' '.$highestRow;exit;
            $rowData_temp = array();

            for ($row = 5; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
                $rowData_temp = $objPHPExcel->getActiveSheet()->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $rowData_final[] = array_pop($rowData_temp);
            }

            // echo "<pre>";print_r($rowData_final);exit;

            $rowData_final_count = count($rowData_final);
            $blank_cells = 0;

            $screening = $confirmatory = $allocation_data_array = array();
            $final_array = $final_array_ = array();

            foreach ($rowData_final as $row_data => $data) {
                // echo "<pre>";print_r($data);
                $county_name = rtrim($data[0]);
                $scr_kits = rtrim($data[3]);
                $scr_units = $scr_kits * 100;
                $conf_kits = rtrim($data[2]);
                $conf_units = $conf_kits * 30;

                $issue_date = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($data[1]));
                // echo "<pre>".$county_name;
                if (isset($county_name) && $county_name != '') {
                    $county_name = strtolower($county_name);
                    $county_name = ucfirst($county_name);
                    // echo "<pre>";print_r($county_name);exit;
                    $county_query = "SELECT * FROM counties WHERE county = '$county_name'";
                    // echo $update_facilities_query;exit;
                    $county_data = $this->db->query($county_query)->result_array();
                    // echo "<pre>";print_r($county_data);exit;
                    // echo "<pre>";print_r($issue_date);exit;
                    $county_id = $county_data[0]['id'];

                }

                if ($county_id > 0) {
                    $the_data = array(
                        'county_id' => $county_id, 
                        'screening_units' => $scr_units, 
                        'confirmatory_units' => $conf_units,
                        'issue_date' => $issue_date
                        );

                    $insertion = $this->db->insert('rtk_issued_county',$the_data);

                    echo "<pre>".$county_id." ".$county_name." ".$issue_date." ".$scr_units." ".$conf_units." ".$insertion;

                }else{
                    echo "<pre> NO COUNTY ID FOUND ".$county_name;
                }
            }

        }//end of file input if
        else {
            echo "NO FILE UPLOADED";
        }
        // exit;
        redirect('rtk_admin/kemsa_issues_excel_interface/1');
    
    }

    public function kemsa_allocations_excel_interface($success=NULL)
    {
        // error_reporting(E_ALL);
        // echo $success;exit;
        $success = (isset($success) && $success > 0) ? $success : NULL;

        // echo "<pre>";print_r($months);exit;
        $data['title'] = "KEMSA issues excel CSV";
        $data['banner_text'] = '';
        $data['success'] = $success;
        $data['content_view'] = 'rtk/admin/kemsa_excel_allocations_upload';
        // $data['final_dets'] = $final_dets;
        $this->load->view('rtk/template', $data);
    
    }

    public function kemsa_allocations_excel_upload()
    {
        // error_reporting(1);
        $data = $this->input->post();
        $user_id = $this->session->userdata('user_id');

        if (isset($_FILES['file']) && $_FILES['file']['size'] > 0) {
            $ext = pathinfo($_FILES["file"]['name'], PATHINFO_EXTENSION);
        // echo "THIS ".$ext;exit;
        //echo $_FILES["file"]["tmp_name"];exit;
            if ($ext == 'xls') {
                $excel2 = PHPExcel_IOFactory::createReader('Excel5');
            } else if ($ext == 'xlsx') {
                $excel2 = PHPExcel_IOFactory::createReader('Excel2007');
            } else if ($ext == 'csv') {
                $excel2 = PHPExcel_IOFactory::createReader('CSV');
            } else {
                die('Invalid file format given' . $_FILES['file']);
            }

            $excel2 = $objPHPExcel = $excel2->load($_FILES["file"]["tmp_name"]);
            // Empty Sheet

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();

            $highestColumn = $sheet->getHighestColumn();

            // echo $highestColumn.' '.$highestRow;exit;
            $rowData_temp = array();

            for ($row = 3; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
                $rowData_temp = $objPHPExcel->getActiveSheet()->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $rowData_final[] = array_pop($rowData_temp);
            }

            // echo "<pre>";print_r($rowData_final);exit;

            $rowData_final_count = count($rowData_final);
            $blank_cells = 0;

            $screening = $confirmatory = $allocation_data_array = array();
            $final_array = $final_allocations = array();

            foreach ($rowData_final as $row_data => $data) {
                $final_array_ = $final_allocations_ = array();
                // echo "<pre>";print_r($data);
                $county_name = rtrim($data[1]);
                $facility_code = rtrim($data[2]);

                $scr_kits = rtrim($data[7]);
                $scr_units = $scr_kits * 100;
                $conf_kits = rtrim($data[8]);
                $conf_units = $conf_kits * 30;

                if (isset($facility_code) && $facility_code > 0) {
                   
                    $facility_query = "SELECT * FROM facilities WHERE facility_code = '$facility_code'";
                    // echo $update_facilities_query;exit;
                    $facility_data = $this->db->query($facility_query)->result_array();
                    // echo "<pre>";print_r($facility_data);exit;

                    $facility_code = $facility_data[0]['facility_code'];
                    $facility_name = $facility_data[0]['facility_name'];
                    $district_id = $facility_data[0]['district'];
                    $district_query = "SELECT * FROM districts WHERE id = '$district_id'";
                    // echo $update_facilities_query;exit;
                    $district_data = $this->db->query($district_query)->result_array();
                    $county_id = $district_data[0]['county'];
                    // echo "<pre>";print_r($county_id);exit;
                    // echo "<pre>";print_r($district_data);exit;
                    $month = date('Y-m-d', strtotime('2017-01-25'));
                    // echo "<pre>";print_r($month);exit;
                    $final_array_['county_id'] = $county_id;
                    $final_array_['district_id'] = $district_id;
                    $final_array_['facility_code'] = $facility_code;
                    $final_array_['facility_name'] = $facility_name;
                    $final_array_['allocate_s'] = $scr_units;
                    $final_array_['allocate_c'] = $conf_units;
                    $final_array_['month'] = $month;
                    $final_array_['user_id'] = 0;

                    $final_allocations_['county_id'] = $county_id;
                    $final_allocations_['district_id'] = $district_id;
                    $final_allocations_['month'] = $month;
                    $final_allocations_['status'] = 'Approved';

                    array_push($final_allocations, $final_allocations_);
                    array_push($final_array, $final_array_);
                }
            }//end of rowDatafinal foreach

            // echo "<pre>";print_r($final_allocations);exit;
            $insert = $this->db->insert_batch('allocation_details',$final_array);
            $insert = $this->db->insert_batch('allocations',$final_allocations);

        }//end of file input if
        else {
            echo "NO FILE UPLOADED";
        }
        // exit;
        redirect('rtk_admin/kemsa_allocations_excel_interface/1');
    
    }

    public function kemsa_pod_upload_interface($success=NULL)
    {
        // error_reporting(E_ALL);
        // echo $success;exit;
        $success = (isset($success) && $success > 0) ? $success : NULL;

        // echo "<pre>";print_r($months);exit;
        $data['title'] = "KEMSA POD Upload";
        $data['banner_text'] = '';
        $data['success'] = $success;
        $data['content_view'] = 'rtk/admin/kemsa_pod_upload';
        // $data['final_dets'] = $final_dets;
        $this->load->view('rtk/template', $data);
    
    }

    public function kemsa_pod_upload()
    {
        // error_reporting(1);
        $data = $this->input->post();
        $user_id = $this->session->userdata('user_id');
        // echo "<pre>";print_r($_FILES);exit;
        if (isset($_FILES['file']) && $_FILES['file']['size'] > 0) {
            $ext = pathinfo($_FILES["file"]['name'], PATHINFO_EXTENSION);
        // echo "THIS ".$ext;exit;
        //echo $_FILES["file"]["tmp_name"];exit;
            if ($ext != 'pdf') {
                die('Invalid file format given' . $_FILES['file']);
            }else{
                $pod_data = $pod_data_ = array();

                $path = FCPATH.'uploads/pods';
                // echo "<pre>";print_r($path);exit;
                $config['file_name'] = $_FILES["file"]['name'];
                $config['file_type'] = $_FILES["file"]['type'];
                $config['file_ext'] = $_FILES["file"]['ext'];
                $config['file_size'] = $_FILES["file"]['size'];
                $config['upload_path'] = $path;
                $config['allowed_types']  = '*';

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                // echo "<pre>";print_r($this->upload->data());exit;
                if ( ! $this->upload->do_upload('file'))
                {
                        $error = array('error' => $this->upload->display_errors());

                        echo "<pre>";print_r($error);exit;
                }
                else
                {
                    $data = array('upload_data' => $this->upload->data());

                    $file_name = $data['upload_data']['file_name'];
                    $path_local = 'uploads/pods';
                    // $pod_data['county_id'] = $county_id;
                    $pod_data['county_id'] = 46;
                    // $pod_data['district_id'] = $district_id;
                    $pod_data['district_id'] = 0;
                    // $pod_data['facility_code'] = $facility_code;
                    $pod_data['facility_code'] = 0;

                    $file_path_local = $path_local."/".$file_name;
                    $pod_data['file_name'] = $file_name;
                    $pod_data['file_path'] = $file_path_local;

                    $insertion = $this->db->insert("pod_files",$pod_data);
                    //comment to make this commitable
                    // echo "<pre>";print_r($insertion);exit;

                }

            }

        }//end of file input if
        else {
            echo "NO FILE UPLOADED";
        }

        redirect('rtk_admin/kemsa_pod_upload_interface/1');
    }

    public function dhis_csv_interface($success = NULL)
    {
        // echo $facility_code;exit;

        // error_reporting(E_ALL);
        // echo $success;exit;
        $success = (isset($success) && $success > 0) ? $success : NULL;

        $county = (int)$this->session->userdata("county_id");

        $data['title'] = "DHIS CSV Upload";
        $data['banner_text'] = 'DHIS CSV Upload';
        $data['success'] = $success;
        $data['content_view'] = 'rtk/admin/dhis_csv';
        // $data['final_dets'] = $final_dets;
        $this->load->view('rtk/template', $data);

    }

    public function dhis_csv()
    {
        $data = $this->input->post();
        $user_id = $this->session->userdata('user_id');
        // echo "<pre>";print_r($data);exit;

        // echo ini_set('upload_max_filesize', '64M');
        // echo ini_get('upload_max_filesize');exit;

        // echo "<pre>";print_r($_FILES);exit;
        // echo "<pre>";print_r($_FILES['userfile']['error']);exit;
        if (isset($_FILES['userfile']) && $_FILES['userfile']['size'] > 0) {
            $ext = pathinfo($_FILES["userfile"]['name'], PATHINFO_EXTENSION);
        // echo "THIS ".$ext;exit;
        //echo $_FILES["file"]["tmp_name"];exit;
            if ($ext == 'xls') {
                $excel2 = PHPExcel_IOFactory::createReader('Excel5');
            } else if ($ext == 'xlsx') {
                $excel2 = PHPExcel_IOFactory::createReader('Excel2007');
            } else if ($ext == 'csv') {
                $excel2 = PHPExcel_IOFactory::createReader('CSV');
            } else {
                die('Invalid file format given' . $_FILES['userfile']);
            }

            $excel2 = $objPHPExcel = $excel2->load($_FILES["userfile"]["tmp_name"]);
            // Empty Sheet

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();

            $highestColumn = $sheet->getHighestColumn();


            // echo $highestColumn.' '.$highestRow;exit;
            $rowData_temp = array();

            for ($row = 0; $row <= $highestRow; $row++) {
                //  Read a row of data into an array
                $rowData_temp = $objPHPExcel->getActiveSheet()->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $rowData_final[] = array_pop($rowData_temp);
            }

            // echo "<pre>";print_r($rowData_final);exit;
            $no_of_tests = count($rowData_final);

            $rowData_final_count = count($rowData_final);
            $blank_cells = 0;

            $facility_details = $facility_details_final = array();

            $titles = $rowData_final[1];
            $years = array_slice($titles, 5);
            $title_count = count($titles);
            // echo "<pre>";print_r($titles);exit;
            $final_data = $final_data_ = array();
            $temp_array = array_fill_keys($titles, null);

            $scr_dimension = "MOH 643_Rapid HIV 1+2 Test 1 - Screening";
            $conf_dimension = "MOH 643_Rapid HIV 1+2 Test 2 - Confirmatory";
            // echo "<pre>";print_r($temp_array);

            for ($i=2; $i < $rowData_final_count; $i++) { 
                // echo "<pre>";print_r($rowData_final[$i]);
                for ($j=0; $j < $title_count; $j++) { 
                    $final_data_[$titles[$j]] = $rowData_final[$i][$j];
                }
                // echo "<pre>";print_r($final_data_);exit;
                array_push($final_data, $final_data_);
            }

            // echo "<pre>";print_r($final_data);exit;
            $insert_data = $insert_data_ = array();

            foreach ($final_data as $key => $value) {
                // echo "<pre>";print_r($value);exit;
                $mfl_check_q = "SELECT * FROM dhis_facilities d,facilities f WHERE f.facility_code = d.facility_code AND d.dhis_facility_code = '".$value['Code']."'";
                // echo $mfl_check_q;
                $check = $this->db->query($mfl_check_q);
                // echo "<pre>";print_r($check->num_rows());
                if ($check->num_rows() > 0) {
                    $check_res = $check->result_array();

                    // echo "<pre>";print_r($check_res);exit;
                    // $facility_code = rtrim($data[1]);
                    $facility_code = $check_res[0]['facility_code'];
                    if(is_numeric($facility_code)):
                        $dimension = $value['Dimension'];
                        $commodity_id = 0;
                        if ($dimension == $scr_dimension) {$commodity_id = 4;};
                        if ($dimension == $conf_dimension) {$commodity_id = 5;};

                        if ($commodity_id > 0) {
                            $facility_details['facility_code'] = $facility_code;
                            $facility_details['dhis_facility_code'] = $value['Code'];
                            $facility_details['commodity_id'] = $commodity_id;
                            $facility_details['dimension'] = $dimension;

                            foreach ($years as $key_yr => $value_yr) {
                                // echo "<pre>";print_r($value_yr);exit;    
                                $yr = substr($value_yr, 0, 4);
                                $mnth = substr($value_yr, 4, 6);

                                $tests_done = $value[$value_yr];
                                // echo $yr." ".$mnth;exit;
                                $facility_details['month_year'] = $mnth."_".$yr;
                                $facility_details['year'] = $yr;
                                $facility_details['month'] = $mnth;
                                $facility_details['tests_done'] = (isset($tests_done) && $tests_done > 0)? $tests_done:0;

                                // $result_facility_insert = $this->db->insert('dhis_data', $facility_details);
                            }

                            array_push($facility_details_final, $facility_details);
                        }

                    endif;
                }

            }
                // echo "<pre>";print_r($facility_details_final);exit;

        // echo "<pre>";print_r($facility_details_final);exit;

        //INSERT FOR FCDRR DATA
            $result_facility_details = $this->db->insert_batch('dhis_data', $facility_details_final);
            $query = $this->db->last_query();
            // echo "<pre>".$result_facility_details;
            // echo "<pre>";print_r($query);exit;

            $redirect_url = 'rtk_admin/dhis_csv_interface/1';
            redirect($redirect_url);
        }                //end of file input if
        else {
            echo "NO FILE UPLOADED";
        }
    }

    public function dhis_facilities_csv_interface($success = NULL)
    {
        // echo $facility_code;exit;

        // error_reporting(E_ALL);
        // echo $success;exit;
        $success = (isset($success) && $success > 0) ? $success : NULL;

        $county = (int)$this->session->userdata("county_id");

        $data['title'] = "DHIS CSV Upload";
        $data['banner_text'] = 'DHIS CSV Upload';
        $data['success'] = $success;
        $data['content_view'] = 'rtk/admin/dhis_facilities_csv';
        // $data['final_dets'] = $final_dets;
        $this->load->view('rtk/template', $data);

    }

    public function dhis_facilities_csv()
    {
        $data = $this->input->post();
        $user_id = $this->session->userdata('user_id');
        // echo "<pre>";print_r($data);exit;

        // echo ini_set('upload_max_filesize', '64M');
        // echo ini_get('upload_max_filesize');

        // echo "<pre>";print_r($_FILES);exit;
        // echo "<pre>";print_r($_FILES['userfile']['error']);exit;
        if (isset($_FILES['userfile']) && $_FILES['userfile']['size'] > 0) {
            $ext = pathinfo($_FILES["userfile"]['name'], PATHINFO_EXTENSION);
        // echo "THIS ".$ext;exit;
        //echo $_FILES["file"]["tmp_name"];exit;
            if ($ext == 'xls') {
                $excel2 = PHPExcel_IOFactory::createReader('Excel5');
            } else if ($ext == 'xlsx') {
                $excel2 = PHPExcel_IOFactory::createReader('Excel2007');
            } else if ($ext == 'csv') {
                $excel2 = PHPExcel_IOFactory::createReader('CSV');
            } else {
                die('Invalid file format given' . $_FILES['userfile']);
            }

            $excel2 = $objPHPExcel = $excel2->load($_FILES["userfile"]["tmp_name"]);
            // Empty Sheet

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();

            $highestColumn = $sheet->getHighestColumn();


            // echo $highestColumn.' '.$highestRow;exit;
            $rowData_temp = array();

            for ($row = 0; $row <= $highestRow; $row++) {
                //  Read a row of data into an array
                $rowData_temp = $objPHPExcel->getActiveSheet()->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $rowData_final[] = array_pop($rowData_temp);
            }

            // echo "<pre>";print_r($rowData_final);exit;
            $no_of_tests = count($rowData_final);

            $rowData_final_count = count($rowData_final);
            $blank_cells = 0;

            $facility_details = $facility_details_final = array();

            $final_data = $final_data_ = array();

            for ($i=2; $i < $rowData_final_count; $i++) { 
                // echo "<pre>";print_r($rowData_final[$i]);exit;
                // $facility_code = rtrim($rowData_final[$i][0]);
                $facility_code = rtrim($rowData_final[$i][1]);
                if(is_numeric($facility_code)):
                    $facility_details['facility_code'] = $facility_code;
                    // $facility_details['dhis_facility_code'] = $rowData_final[$i][1];
                    $facility_details['dhis_facility_code'] = $rowData_final[$i][2];
                    // $facility_details['facility_name'] = $rowData_final[$i][2];
                    $facility_details['facility_name'] = $rowData_final[$i][0];
                    array_push($facility_details_final, $facility_details);
                endif;
            }

            // echo "<pre>";print_r($facility_details_final);exit;

            //INSERT DHIS FACILITIES DATA
            $dhis_facility_insert = $this->db->insert_batch('dhis_facilities', $facility_details_final);
            // echo $dhis_facility_insert;
            $redirect_url = 'rtk_admin/dhis_facilities_csv_interface/1';
            redirect($redirect_url);
        }                //end of file input if
        else {
            echo "NO FILE UPLOADED";
        }
    }

    public function reporting_rates_recalculate_interface($success=NULL)
    {
        // error_reporting(E_ALL);
        // echo $success;exit;
        $success = (isset($success) && $success > 0) ? $success : NULL;

        // echo "<pre>";print_r($months);exit;
        $today = date('Y-m-d');
        $earliest_date = DATA_START_DATE;

        $data['months'] = $this->get_months_between_dates($earliest_date,$today);
        // echo "<pre>";print_r($months);exit;
        $queryy="SELECT * FROM counties";
        $data['counties'] = $this->db->query($queryy)->result_array();

        $queryyy="SELECT * FROM districts";
        $data['districts'] = $this->db->query($queryyy)->result_array();

        $data['title'] = "Recalculate reporting rates";
        $data['banner_text'] = '';
        $data['success'] = $success;
        $data['content_view'] = 'rtk/admin/reporting_rates_recalculate';
        // $data['final_dets'] = $final_dets;
        $this->load->view('rtk/template', $data);
    
    }

    public function reporting_rates_recalculate()
    {
        // error_reporting(1);
        $data = $this->input->post();
        // echo "<pre>";print_r($data);exit;
        $posted_county = $data['county'];
        $posted_subcounty = $data['subcounty'];
        $posted_date = $data['date'];

        $county_id = ($posted_county > 0)? $posted_county:NULL;
        $district_id = ($posted_subcounty > 0)? $posted_subcounty:NULL;

        $m = substr($posted_date, 4, 6);
        $y = substr($posted_date, 0,4);
        // echo $m.' '.$y;exit;
        $month = ($m > 0)? $m:NULL;
        $year = ($y > 0)? $y:NULL;

        // echo $county_id.' '.$district_id.' '.$facility_code.' '.$month.' '.$year;exit;
        $update = $this->update_reporting_percentages($county_id,$district_id,$facility_code,$month,$year);

        // echo "<pre>";print_r($update);exit;
        redirect('rtk_admin/reporting_rates_recalculate_interface/1');
    
    }

    public function admin_cd4_management($success_status = NULL)
    {
        $success_status = (isset($success_status) && $success_status > 0)? $success_status:NULL;

        $facilities = Facilities::get_total_facilities_cd4_in_district($district);       

        $final_data = array();
        
        $query = "
            SELECT DISTINCT
                cfd.facility_code,
                cfd.device,
                cfd.enabled,
                f.facility_code,
                f.facility_name,
                f.district as district_id,
                f.cd4_enabled,
                d.*,
                c.county AS county_name,
                cd.id AS device_id,
                cd.name AS device_name
            FROM
                facilities f,
                districts d,
                counties c,
                cd4_facility_device cfd,
                cd4_device cd
            WHERE
                cfd.facility_code = f.facility_code
                AND f.district = d.id
                AND d.county = c.id
                AND cfd.device = cd.id
                AND f.cd4_enabled = 1";

        // echo $query;exit;
        $cd4_sites = $this->db->query($query)->result_array();
        // echo "<pre>";print_r($result);exit;

        $query = "
            SELECT
                *
            FROM
                facilities";

        // echo $query;exit;
        $facilities = $this->db->query($query)->result_array();

        $query = "
            SELECT
                *
            FROM
                cd4_device cd";

        // echo $query;exit;
        $cd4_devices = $this->db->query($query)->result_array();

        // echo "<pre>";print_r($cd4_devices);exit;
        $data['devices'] = $cd4_devices;
        $data['facilities'] = $facilities;
        $data['cd4_sites'] = $cd4_sites;
        $data['title'] = "Admin Allocations";
        $data['banner_text'] = 'Admin Allocations';
        $data['content_view'] = 'rtk/admin/cd4_management';
        $data['success_status'] = $success_status;
        $data['allocations'] = $final_array;

        // echo "<pre>";print_r($data);exit;

        $this->load->view('rtk/template', $data);
    }

    public function admin_add_cd4_device()
    {   
        $post_data = $this->input->post();
        // echo "<pre>";print_r($post_data);exit;
        $mfl = $post_data['facility'];
        $device = $post_data['device'];

        if ($device > 0 && $mfl > 0) {
            $insert_data['facility_code'] = $mfl;    
            $insert_data['device'] = $device;    
            $insert_data['enabled'] = 1;   

            $ins = $this->db->insert('cd4_facility_device',$insert_data);

            $cd4_enable_q = "UPDATE facilities SET cd4_enabled='1' WHERE facility_code='$mfl'";
            // echo "<pre>";print_r($ins);exit;
            $result = $this->db->query($cd4_enable_q);
            
            if ($ins > 0) {
                redirect('rtk_admin/admin_cd4_management/1');
            }else{
                redirect('rtk_admin/admin_cd4_management/2');
            }
        }else{
                redirect('rtk_admin/admin_cd4_management/2');
            }
    }

    public function admin_delete_cd4_equipment($facility_code,$device_id)
    {
        // echo "<pre>";print_r($facility_code);
        // echo "<pre>";print_r($device_id);exit;
        // echo $district_id.' '.$date;exit;
        $deletion_sql = "DELETE FROM cd4_facility_device WHERE facility_code = $facility_code AND device = '$device_id'";
        $device_deletion = $this->db->query($deletion_sql);

        redirect('rtk_admin/admin_cd4_management/1');
    }

    public function send_issues_mail_interface($success=NULL)
    {
        // error_reporting(E_ALL);
        // echo $success;exit;
        $success = (isset($success) && $success > 0) ? $success : NULL;

        // echo "<pre>";print_r($months);exit;
        $today = date('Y-m-d');
        $earliest_date = DATA_START_DATE;

        $data['months'] = $this->get_months_between_dates($earliest_date,$today);
        // echo "<pre>";print_r($months);exit;
        $queryy="SELECT * FROM counties";
        $data['counties'] = $this->db->query($queryy)->result_array();

        $queryyy="SELECT * FROM districts";
        $data['districts'] = $this->db->query($queryyy)->result_array();

        $issues_q = "
        SELECT 
            c.county,
            k.*
        FROM
            kemsa_issue_emails k, counties c
        WHERE k.county_id = c.id
        ORDER BY date_sent DESC";
        $data['issue_emails'] = $this->db->query($issues_q)->result_array();
        // echo "<pre>";print_r($data['issue_emails']);exit;
        $data['title'] = "Recalculate reporting rates";
        $data['banner_text'] = '';
        $data['success'] = $success;
        $data['content_view'] = 'rtk/admin/send_issues_mail';
        // $data['final_dets'] = $final_dets;
        $this->load->view('rtk/template', $data);
    
    }

    public function send_issues_mail()
    {
        // error_reporting(1);
        $data = $this->input->post();
        // echo "<pre>";print_r($data);exit;
        $posted_county = $data['county'];
        $posted_testing = $data['testing'];
        $posted_date = $data['date'];
        $testing = NULL;

        if ($posted_testing == 'on') {
            $testing = 1;
        }

        $county_id = ($posted_county > 0)? $posted_county:NULL;
        $district_id = ($posted_subcounty > 0)? $posted_subcounty:NULL;

        $m = substr($posted_date, 4, 6);
        $y = substr($posted_date, 0,4);
        // echo $m.' '.$y;exit;
        $month = ($m > 0)? $m:NULL;
        $year = ($y > 0)? $y:NULL;

        // echo "<pre>";print_r($testing);exit;
        // echo 'COUNTY: '.$county_id.' DISTRICT: '.$district_id.' MFL: '.$facility_code.' MONTH: '.$month.' YEAR: '.$year;exit;
        $email_send = $this->send_issues_email($county_id,$district_id,$facility_code,$month,$year,$quarter,$testing);

        // echo "<pre>";print_r($email_send);exit;
        redirect('rtk_admin/send_issues_mail/1');
    }

    public function dhis_fcdrr_data_management_interface($success=NULL,$county_id = NULL)
    {
        // error_reporting(E_ALL);
        // echo $success;exit;
        $success = (isset($success) && $success > 0) ? $success : NULL;

        // echo "<pre>";print_r($months);exit;
        $today = date('Y-m-d');
        $earliest_date = DATA_START_DATE;

        $data['months'] = $this->get_months_between_dates($earliest_date,$today);
        // echo "<pre>";print_r($months);exit;
        $queryy="SELECT * FROM counties";
        $data['counties'] = $this->db->query($queryy)->result_array();

        $queryyy="SELECT * FROM districts";
        $all_districts = $this->db->query($queryyy)->result_array();

        $county_where = ($county_id > 0)? " AND c.id = $county_id":NULL;
        $join_query = "
        SELECT 
            c.id as county_id, c.county,d.id as district_id, d.district,dl.*,dl.status AS log_status
        FROM
            counties c,districts d
                LEFT JOIN
            dhis_fcdrr_pull_logs dl ON d.id = dl.level_id
        WHERE d.county = c.id $county_where GROUP BY c.id,d.id;
        ";
        // echo "<pre>";print_r($join_query);exit;
        $all_district_logs = $this->db->query($join_query)->result_array();
        // echo "<pre>";print_r($all_district_logs);exit;
        $data['all_district_logs'] = $all_district_logs;


        $data['districts'] = $all_districts;

        $data['title'] = "DHIS FCDRR Data Management";
        $data['banner_text'] = '';
        $data['success'] = $success;
        $data['content_view'] = 'rtk/admin/dhis_fcdrr_data_management';
        // $data['final_dets'] = $final_dets;
        $this->load->view('rtk/template', $data);
    
    }

    public function dhis_fcdrr_data_management()
    {
        // error_reporting(1);
        $data = $this->input->post();
        // echo "<pre>";print_r($data);exit;
        $posted_county = $data['county'];
        $posted_subcounty = $data['subcounty'];
        $posted_date = $data['date'];

        $county_id = ($posted_county > 0)? $posted_county:NULL;
        $district_id = ($posted_subcounty > 0)? $posted_subcounty:NULL;

        $m = substr($posted_date, 4, 6);
        $y = substr($posted_date, 0,4);
        // echo $m.' '.$y;exit;
        $month = ($m > 0)? $m:NULL;
        $year = ($y > 0)? $y:NULL;

        // echo $county_id.' '.$district_id.' '.$facility_code.' '.$month.' '.$year;exit;
        $update = $this->update_reporting_percentages($county_id,$district_id,$facility_code,$month,$year);

        // echo "<pre>";print_r($update);exit;
        redirect('rtk_admin/reporting_rates_recalculate_interface/1');
    
    }

    public function get_dhis_fcdrr_details($level = NULL, $level_id = NULL, $operation = NULL)
    {
        $county_id = $district_id = $facility_code = NULL;
        $commodity_id = ($commodity_id > 0)? $commodity_id:4;
        // echo "<pre>";print_r($level);exit;
        // echo "<pre>";print_r($order_id);exit;
        // echo "<pre>";print_r($operation);exit;

        if (isset($order_date) && $order_date !='') {
            $m = substr_replace($order_date, "", -4);
            $y = substr($order_date, -4);

            $year = $y;
            $month = $m;
        }

        // echo "<pre>";print_r($operation);exit;
        $back_button = $html = '';
        switch ($operation) {
            case 'pull_data':

                $data_pull = $this->get_dhis_fcdrr_data(NULL, $level_id,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'pull');
                // echo "<pre>";print_r($data_pull);exit;
                $sub_level = "subcounty";
                $html .= '<p class="alert alert-info col-md-12">Data retrieved <strong>SUCCESSFULLY</strong></p>';
                // $back_button = '<a class="btn btn-primary" style="width: 250px;margin: 5px 0px;" id="modal_balance_details" data-level = "subcounty" data-operation="view_data" data-level-id = "'.$level_id.'">View Data</a>';
                $back_button = '<a class="btn btn-primary" id="modal_fcdrr_details" style="width: 250px;margin: 5px 0px;" data-level = "subcounty" data-level-id = "'.$level_id.'" data-operation="view_data_by_facility" data-modal-level="facility">View FCDRRs</a>';
                break;
            case 'view_data_by_facility':
                $data_pull = $this->get_dhis_fcdrr_data(NULL, $level_id);
                // echo "<pre>";print_r($data_pull);exit;
                $data_array = $data_pull['result'];
                // echo "<pre>";print_r($data_array);exit;

                $table_heads = '
                <th>County</th>
                <th>Subcounty</th>
                <th>Facility Code</th>
                <th>Facility Name</th>
                <th>View</th>
                <th>Import</th>
                ';

                $html = '<table class="table table-bordered datatable display cell-border compact" cellspacing="0" width="100%" id="datatable">
                            <thead>
                                <tr>
                                    '.$table_heads.'
                                </tr>
                            </thead>
                            <tbody>';

                foreach ($data_array as $key => $value) { 
                    $order_date = $value['year'].'-'.$value['month'].'-01';

                    $html .= '
                    <tr>
                        <td>'.$value['county'].'</td>
                        <td>'.$value['district'].'</td>
                        <td>'.$value['facility_code'].'</td>
                        <td>'.$value['facility_name'].'</td>';
                    $html .= '<td><a class="btn btn-primary" id="modal_fcdrr_details" data-level = "facility" data-level-id = "'.$value['facility_code'].'" data-operation="view_data_by_month" data-modal-level="facility">View FCDRRs</a></td>';
                    // $html .= '<td><a class="btn btn-primary" id="modal_fcdrr_details" data-level = "facility" data-level-id = "'.$value['facility_code'].'" data-operation="import_data_by_facility" data-modal-level="facility">Import FCDRRs</a></td>';
                    $html .= '<td><a class="btn btn-primary" href="get_dhis_fcdrr_details/facility/'.$value['facility_code'].'/import_data_by_facility" data-level = "facility" target=\"_blank\"">Import FCDRRs</a></td>';
                    $html .= '</tr>';
                
                }
                    $html .='<tbody></table>';
                break;
            case 'view_data_by_month':
                /*AFTER FACILITY CODE HAS BEEN SELECTED. SHOWS ORDERS BY MONTH FOR INDIVIDUAL FACILITY*/
                // echo "<pre>";print_r($level_id);exit;

                
                $data_pull = $this->get_dhis_fcdrr_data(NULL, NULL, $level_id);
                // echo "<pre>";print_r($data_pull);exit;
                $data_array = $data_pull['result'];
                $dist_id = $data_array[0]['district_id'];

                $back_button = '<a class="btn btn-primary" id="modal_fcdrr_details" style="width: 250px;margin: 5px 0px;" data-level = "subcounty" data-level-id = "'.$dist_id.'" data-operation="view_data_by_facility" data-modal-level="facility">Back to Facility List</a>';

                $html .= '<div class="alert alert-info col-md-12">';
                $html .= '<p><strong>NB:</strong></p>';
                $html .= '<p><strong>Status: </strong>Present means that the report exists in HCMP already. </p>';
                $html .= '<p><strong>Status: </strong>Imported means that the report was imported in to HCMP already. </p>';
                $html .= '<p>Reports are not to be imported if they already exist on HCMP to avoid conflictings. </p>';
                $html .= '<p>All FCDRR data from this interface is DHIS data. </p>';
                $html .= '<p>Still working on enabling overwrites. </p>';
                $html .= '</div>';

                // echo "<pre>";print_r($data_array);exit;
                $table_heads = '
                <th>County</th>
                <th>Subcounty</th>
                <th>Facility Code</th>
                <th>Facility Name</th>
                <th>Report Date</th>
                <th>Status</th>
                <th>View</th>
                <th>Import</th>
                ';

                $html .= '<table class="table table-bordered datatable display cell-border compact" cellspacing="0" width="100%" id="datatable">
                            <thead>
                                <tr>
                                    '.$table_heads.'
                                </tr>
                            </thead>
                            <tbody>';

                foreach ($data_array as $key => $value) { 
                    $order_date = $value['year'].'-'.$value['month'].'-01';

                    $html .= '
                    <tr>
                        <td>'.$value['county'].'</td>
                        <td>'.$value['district'].'</td>
                        <td>'.$value['facility_code'].'</td>
                        <td>'.$value['facility_name'].'</td>
                        <td>'.date('F Y',strtotime($order_date)).'</td>';
                        
                        if ($value['order_id'] > 0 && $value['import_status'] > 0) {
                            $html .= '<td> <p class="label label-info">Imported from DHIS.</p> </td>';
                        }elseif ($value['order_id'] > 0) {
                            $html .= '<td> <p class="label label-success">Present in HCMP.</p> </td>';
                        }else{
                            $html .= '<td> <p class="label label-danger">Absent in HCMP.</p> </td>';
                        }

                    $html .= '<td><a class="btn btn-primary" id="modal_fcdrr_details" data-level = "dhis_order" data-level-id = "'.$value['facility_code'].'" data-operation="view_data_by_id" data-month-year = "'.$value['month_year'].'" data-modal-level="dhis_order">View FCDRR</a></td>';

                    // $html .= '<td><a class="btn btn-primary" id="modal_fcdrr_details" data-level = "dhis_order" data-level-id = "'.$value['facility_code'].'" data-operation="import_data_by_fcdrr" data-month-year = "'.$value['month_year'].'" data-modal-level="dhis_order">Import FCDRR</a></td>';
                    
                    if ($value['import_status'] > 0) {
                        // $html .= '<td><a class="btn btn-info" disabled="true">Already Imported</a></td>';
                        $html .= '<td><a class="btn btn-primary" id="modal_fcdrr_details" data-level = "facility" data-level-id = "'.$value['facility_code'].'" data-month-year = "'.$value['month_year'].'" data-operation="import_data_by_fcdrr" data-modal-level="facility">Repeat Import of FCDRR</a></td>';
                        
                    }elseif ($value['order_id'] > 0) {
                        $html .= '<td><a class="btn btn-warning" disabled="true">Available on HCMP</a></td>';
                        
                    }else{
                        $html .= '<td><a class="btn btn-primary" id="modal_fcdrr_details" data-level = "facility" data-level-id = "'.$value['facility_code'].'" data-month-year = "'.$value['month_year'].'" data-operation="import_data_by_fcdrr" data-modal-level="facility">Import FCDRR</a></td>';
                        
                    }


                    $html .= '</tr>';
                
                }
                    $html .='<tbody></table>';
            break;
            case 'import_data_by_facility':
                // echo "<pre>";print_r($level_id);exit;
                $import = $this->import_dhis_order_by_facility($level_id);
            break;

            default:
                
                break;

        }
        echo $back_button.$html;

    }

    public function get_dhis_order_details($month_year,$facility_code)
    {

        $query = "
            SELECT 
                c.county, d.district, f.facility_name, df.*,l.*
            FROM
                dhis_fcdrr_data df,
                facilities f,
                districts d,
                counties c,
                lab_commodities l
            WHERE
                df.facility_code = f.facility_code
                    AND f.district = d.id
                    AND d.county = c.id
                    AND df.commodity_id = l.id
                    AND df.facility_code = $facility_code
                    AND df.month_year = '$month_year'
            GROUP BY month_year,commodity_id
            ORDER BY commodity_id ASC";
        
        // echo $query;exit;
        $result = $this->db->query($query)->result_array();
        // echo "<pre>";print_r($result);exit;

        $dist_id = $result[0]['district_id'];

        $back_button = '<a class="btn btn-primary" id="modal_fcdrr_details" style="width: 250px;margin: 5px 0px;" data-level = "facility" data-level-id = "'.$facility_code.'" data-operation="view_data_by_month" data-modal-level="facility">Back to FCDRR List</a>';

        $table = '';
        $table .= '<table class="table table-bordered compact table-sm">';
        $table .= '<thead>';
        $table .= '
                <th>Commodity Name</th>
                <th>Beginning Balance</th>
                <th>Quantity Received</th>
                <th>Quantity Used</th>
                <th>Tests Done</th>
                <th>Losses</th>
                <th>Adj (+ve)</th>  
                <th>Adj (-ve)</th>  
                <th>Expiring in <u>less than</u> 6 Months</th>
                <th>End of Month Physical Count</th>
                <th>Days out of Stock</th>  
                <th>Requested for&nbsp;Re-Supply</th>';
        $table .= '</thead>';
        $table .= '<tbody>';
        //RANDOM COMMIT CODE
        foreach ($result as $detail) {
            $table .= '
                    <tr>
                        <td><b>'. $detail['commodity_name'].'</b></td>
                        <td>'.number_format($detail['beginning_bal']).'</td>
                        <td>'.number_format($detail['q_received']).'</td>
                        <td>'.number_format($detail['q_used']).'</td>
                        <td>'.number_format($detail['no_of_tests_done']).'</td>
                        <td>'.number_format($detail['losses']).'</td>
                        <td>'.number_format($detail['positive_adj']).'</td>
                        <td>'.number_format($detail['negative_adj']).'</td>
                        <td>'.number_format($detail['q_expiring']).'</td>
                        <td>'.number_format($detail['closing_stock']).'</td>
                        <td>'.number_format($detail['days_out_of_stock']).'</td>    
                        <td>'.number_format($detail['q_requested']).'</td>
                    </tr>';
        }

        $table .= '</tbody>';

        $table .= '</table>';
        echo $back_button.$table;
    }

    public function import_dhis_order($month_year,$facility_code)
    {

        $query = "
            SELECT 
                c.county, d.district, f.facility_name, df.id AS dhis_order_id, df.*,l.*
            FROM
                dhis_fcdrr_data df,
                facilities f,
                districts d,
                counties c,
                lab_commodities l
            WHERE
                df.facility_code = f.facility_code
                    AND f.district = d.id
                    AND d.county = c.id
                    AND df.commodity_id = l.id
                    AND df.facility_code = $facility_code
                    AND df.month_year = '$month_year'
            GROUP BY month_year,commodity_id
            ORDER BY commodity_id ASC";
        
        // echo $query;exit;
        $result = $this->db->query($query)->result_array();
        // echo "<pre>";print_r($result);exit;

        $m_data = explode('_', $month_year);
        // echo "<pre>";print_r($m_data);exit;
        $m = $m_data[0];
        $y = $m_data[1];

        $o_date = $y.'-'.$m.'-01';
        // echo "<pre>";print_r($o_date);exit;
        $beg_date = date('Y-m-d',strtotime($o_date));
        $end_date = date('Y-m-t',strtotime($o_date));
        $lastmonth = date('F', strtotime($o_date));

        $explanation = "Imported from DHIS";
        $compiled_by = "DHIS";
        $moh_642 = $_POST['moh_642'];
        $moh_643 = $_POST['moh_643'];

        $district_id = $result[0]['district_id'];
        
        $order_date_main = date('Y-m-d',strtotime('+1 MONTH',strtotime($beg_date)));
        // echo "<pre>";print_r($order_date_main);exit;

        $count = 1;
        $data = array('facility_code' => $facility_code, 'district_id' => $district_id, 'compiled_by' => $compiled_by, 'order_date' => $order_date_main, 'vct' => $vct, 'pitc' => $pitc, 'pmtct' => $pmtct, 'b_screening' => $b_screening, 'other' => $other, 'specification' => $specification, 'rdt_under_tests' => $rdt_under_tests, 'rdt_under_pos' => $rdt_under_pos, 'rdt_btwn_tests' => $rdt_btwn_tests, 'rdt_btwn_pos' => $rdt_btwn_pos, 'rdt_over_tests' => $rdt_over_tests, 'rdt_over_pos' => $rdt_over_pos, 'micro_under_tests' => $micro_under_tests, 'micro_under_pos' => $micro_under_pos, 'micro_btwn_tests' => $micro_btwn_tests, 'micro_btwn_pos' => $micro_btwn_pos, 'micro_over_tests' => $micro_over_tests, 'micro_over_pos' => $micro_over_pos, 'beg_date' => $beg_date, 'end_date' => $end_date, 'explanation' => $explanation, 'moh_642' => $moh_642, 'moh_643' => $moh_643, 'report_for' => $lastmonth, 'user_id' => $user_id);

        // echo "<pre>";print_r($data);exit;
        $u = new Lab_Commodity_Orders();
        $u->fromArray($data);
        $u->save();
        $object_id = $u->get('id');
        $rtk_management =  new rtk_management();//API INIT
        // echo "<pre>";print_r($rtk_management);exit;
        $rtk_management->logData('13', $object_id);
        $rtk_management->update_amc($facility_code);
        // echo "<pre>";print_r("HERE");exit;

        $lastId = Lab_Commodity_Orders::get_new_order($facility_code);
        $new_order_id = $lastId->maxId;
        $count++;

        $dhis_order_ids = array();
        foreach ($result as $key => $value) {

            date_default_timezone_set("EUROPE/Moscow");
            $firstday = date('D dS M Y', strtotime("first day of previous month"));
            $lastday = date('D dS M Y', strtotime("last day of previous month"));

            $m = $value['month'];
            $y = $value['year'];

            $dhis_order_id = $value['dhis_order_id'];

            $beg_date = date('Y-m-d',strtotime($o_date));
            $end_date = date('Y-m-t',strtotime($o_date));
            $lastmonth = date('F', strtotime($o_date));
            // echo "<pre>";print_r($end_date);exit;
            // echo "<pre>";print_r($o_date);exit;
            // echo "<pre>";print_r($lastmonth);exit;
            // echo "<pre>";print_r($this->input->post());exit;
            $district_id = $value['district_id'];
            $facility_code = $value['facility_code'];
            $drug_id = $value['commodity_id'];
            $unit_of_issue = $value['unit_of_issue'];
            $b_balance = $value['beginning_bal'];
            $q_received = $value['q_received'];
            $q_received_other = $value['q_received_other'];
            $q_used = $value['q_used'];
            $tests_done = $value['no_of_tests_done'];
            $losses = $value['losses'];
            $closing_stock = $value['closing_stock'];
            $pos_adj = $value['positive_adj'];
            $neg_adj = $value['negative_adj'];
            $physical_count = $value['physical_count'];
            $fcdrr_physical_count = $value['fcdrr_physical_count'];
            $q_expiring = $value['q_expiring'];
            $days_out_of_stock = $value['days_out_of_stock'];
            $q_requested = $value['q_requested'];
            $amc = $value['amc'];
            $commodity_count = count($drug_id);

            $order_date = $o_date;
            $mydata = array(
                'order_id' => $new_order_id, 
                'facility_code' => $facility_code, 
                'district_id' => $district_id, 
                'commodity_id' => $drug_id, 
                'unit_of_issue' => $unit_of_issue, 
                'beginning_bal' => $b_balance, 
                'physical_beginning_bal' => $physical_b_balance, 
                'q_received' => $q_received, 
                'q_received_others' => $q_received_other, 
                'q_used' => $q_used, 
                'no_of_tests_done' => $tests_done, 
                'losses' => $losses, 
                'positive_adj' => $pos_adj, 
                'negative_adj' => $neg_adj, 
                'closing_stock' => $closing_stock, 
                'q_expiring' => $q_expiring, 
                'days_out_of_stock' => $days_out_of_stock, 
                'q_requested' => $q_requested, 
                'amc' => $amc
            );

            Lab_Commodity_Details::save_lab_commodities($mydata);
            
            $user_id = $this->session->userdata('user_id');
            // echo "<pre>";print_r($user_id);exit;
            // $order_date = date('y-m-d');

            
            // echo "<pre>";print_r($mydata);exit;
            
            // die;
            $q = "select county from districts where id='$district_id'";
            $res = $this->db->query($q)->result_array();
            foreach ($res as $key => $value) {
                $county = $value['county'];
            }

            $r = "select partner from facilities where facility_code='$facility_code'";
            $resr = $this->db->query($r)->result_array();
            foreach ($resr as $key => $value) {
                $partner = $value['partner'];
            }
            if ($partner = 0) {
                $partner = null;
            }
            // echo "<pre>";print_r($data);exit;
            $rtk_management->_update_reports_count('add', $county, $district_id, $partner);

            $import_status = array(
               'import_status' => 1,
               'order_id' => $new_order_id
            );

            // echo "<pre>";print_r($import_status);exit;

            $search = $this->db->where('id', $dhis_order_id);
            // echo "<pre>";print_r($search);
            $update = $this->db->update('dhis_fcdrr_data', $import_status);
            // echo "<pre>";print_r($update);exit;
        }

        $html .= '<p class="alert alert-info col-md-12">Data imported <strong>SUCCESSFULLY</strong></p>';
        // $back_button = '<a class="btn btn-primary" style="width: 250px;margin: 5px 0px;" id="modal_balance_details" data-level = "subcounty" data-operation="view_data" data-level-id = "'.$level_id.'">View Data</a>';
        $back_button = '<a class="btn btn-primary" style="width: 250px;margin: 5px 0px;" id="modal_fcdrr_details" data-level = "facility" data-level-id = "'.$facility_code.'" data-operation="view_data_by_month" data-modal-level="facility">Back to FCDRRs</a>';

        echo $back_button.$html;
    }

    public function import_dhis_order_raw($month_year,$facility_code)
    {

        $query = "
            SELECT 
                c.county, d.district, f.facility_name, df.id AS dhis_order_id, df.*,l.*
            FROM
                dhis_fcdrr_data df,
                facilities f,
                districts d,
                counties c,
                lab_commodities l
            WHERE
                df.facility_code = f.facility_code
                    AND f.district = d.id
                    AND d.county = c.id
                    AND df.commodity_id = l.id
                    AND df.facility_code = $facility_code
                    AND df.month_year = '$month_year'
            GROUP BY month_year,commodity_id
            ORDER BY commodity_id ASC";
        
        // echo $query;exit;
        $result = $this->db->query($query)->result_array();
        // echo "<pre>";print_r($result);exit;

        $m_data = explode('_', $month_year);
        // echo "<pre>";print_r($m_data);exit;
        $m = $m_data[0];
        $y = $m_data[1];

        $o_date = $y.'-'.$m.'-01';
        // echo "<pre>";print_r($o_date);exit;
        $beg_date = date('Y-m-d',strtotime($o_date));
        $end_date = date('Y-m-t',strtotime($o_date));
        $lastmonth = date('F', strtotime($o_date));

        $explanation = "Imported from DHIS";
        $compiled_by = "DHIS";
        $moh_642 = $_POST['moh_642'];
        $moh_643 = $_POST['moh_643'];

        $district_id = $result[0]['district_id'];
        
        $order_date_main = date('Y-m-d',strtotime('+1 MONTH',strtotime($beg_date)));
        // echo "<pre>";print_r($order_date_main);exit;

        $count = 1;
        $data = array('facility_code' => $facility_code, 'district_id' => $district_id, 'compiled_by' => $compiled_by, 'order_date' => $order_date_main, 'vct' => $vct, 'pitc' => $pitc, 'pmtct' => $pmtct, 'b_screening' => $b_screening, 'other' => $other, 'specification' => $specification, 'rdt_under_tests' => $rdt_under_tests, 'rdt_under_pos' => $rdt_under_pos, 'rdt_btwn_tests' => $rdt_btwn_tests, 'rdt_btwn_pos' => $rdt_btwn_pos, 'rdt_over_tests' => $rdt_over_tests, 'rdt_over_pos' => $rdt_over_pos, 'micro_under_tests' => $micro_under_tests, 'micro_under_pos' => $micro_under_pos, 'micro_btwn_tests' => $micro_btwn_tests, 'micro_btwn_pos' => $micro_btwn_pos, 'micro_over_tests' => $micro_over_tests, 'micro_over_pos' => $micro_over_pos, 'beg_date' => $beg_date, 'end_date' => $end_date, 'explanation' => $explanation, 'moh_642' => $moh_642, 'moh_643' => $moh_643, 'report_for' => $lastmonth, 'user_id' => $user_id);

        // echo "<pre>";print_r($data);exit;
        $u = new Lab_Commodity_Orders();
        $u->fromArray($data);
        $u->save();
        $object_id = $u->get('id');
        $rtk_management =  new rtk_management();//API INIT
        // echo "<pre>";print_r($rtk_management);exit;
        $rtk_management->logData('13', $object_id);
        $rtk_management->update_amc($facility_code);
        // echo "<pre>";print_r("HERE");exit;

        $lastId = Lab_Commodity_Orders::get_new_order($facility_code);
        $new_order_id = $lastId->maxId;
        $count++;

        $dhis_order_ids = array();
        foreach ($result as $key => $value) {

            date_default_timezone_set("EUROPE/Moscow");
            $firstday = date('D dS M Y', strtotime("first day of previous month"));
            $lastday = date('D dS M Y', strtotime("last day of previous month"));

            $m = $value['month'];
            $y = $value['year'];

            $dhis_order_id = $value['dhis_order_id'];

            $beg_date = date('Y-m-d',strtotime($o_date));
            $end_date = date('Y-m-t',strtotime($o_date));
            $lastmonth = date('F', strtotime($o_date));
            // echo "<pre>";print_r($end_date);exit;
            // echo "<pre>";print_r($o_date);exit;
            // echo "<pre>";print_r($lastmonth);exit;
            // echo "<pre>";print_r($this->input->post());exit;
            $district_id = $value['district_id'];
            $facility_code = $value['facility_code'];
            $drug_id = $value['commodity_id'];
            $unit_of_issue = $value['unit_of_issue'];
            $b_balance = $value['beginning_bal'];
            $q_received = $value['q_received'];
            $q_received_other = $value['q_received_other'];
            $q_used = $value['q_used'];
            $tests_done = $value['no_of_tests_done'];
            $losses = $value['losses'];
            $closing_stock = $value['closing_stock'];
            $pos_adj = $value['positive_adj'];
            $neg_adj = $value['negative_adj'];
            $physical_count = $value['physical_count'];
            $fcdrr_physical_count = $value['fcdrr_physical_count'];
            $q_expiring = $value['q_expiring'];
            $days_out_of_stock = $value['days_out_of_stock'];
            $q_requested = $value['q_requested'];
            $amc = $value['amc'];
            $commodity_count = count($drug_id);

            $order_date = $o_date;
            $mydata = array(
                'order_id' => $new_order_id, 
                'facility_code' => $facility_code, 
                'district_id' => $district_id, 
                'commodity_id' => $drug_id, 
                'unit_of_issue' => $unit_of_issue, 
                'beginning_bal' => $b_balance, 
                'physical_beginning_bal' => $physical_b_balance, 
                'q_received' => $q_received, 
                'q_received_others' => $q_received_other, 
                'q_used' => $q_used, 
                'no_of_tests_done' => $tests_done, 
                'losses' => $losses, 
                'positive_adj' => $pos_adj, 
                'negative_adj' => $neg_adj, 
                'closing_stock' => $closing_stock, 
                'q_expiring' => $q_expiring, 
                'days_out_of_stock' => $days_out_of_stock, 
                'q_requested' => $q_requested, 
                'amc' => $amc
            );

            Lab_Commodity_Details::save_lab_commodities($mydata);
            
            $user_id = $this->session->userdata('user_id');
            // echo "<pre>";print_r($user_id);exit;
            // $order_date = date('y-m-d');

            
            // echo "<pre>";print_r($mydata);exit;
            
            // die;
            $q = "select county from districts where id='$district_id'";
            $res = $this->db->query($q)->result_array();
            foreach ($res as $key => $value) {
                $county = $value['county'];
            }

            $r = "select partner from facilities where facility_code='$facility_code'";
            $resr = $this->db->query($r)->result_array();
            foreach ($resr as $key => $value) {
                $partner = $value['partner'];
            }
            if ($partner = 0) {
                $partner = null;
            }
            // echo "<pre>";print_r($data);exit;
            $rtk_management->_update_reports_count('add', $county, $district_id, $partner);

            $import_status = array(
               'import_status' => 1,
               'order_id' => $new_order_id
            );

            // echo "<pre>";print_r($import_status);exit;

            $search = $this->db->where('id', $dhis_order_id);
            // echo "<pre>";print_r($search);
            $update = $this->db->update('dhis_fcdrr_data', $import_status);
            // echo "<pre>";print_r($update);exit;
        }

        return 1;
    }

    public function import_dhis_order_by_facility($facility_code)
    {
        // echo "<pre>";print_r($facility_code);exit;
        $data_pull = $this->get_dhis_fcdrr_data(NULL, NULL, $facility_code);
        // echo "<pre>";print_r($data_pull);exit;
        $data_array = $data_pull['result'];
        $dist_id = $data_array[0]['district_id'];

        $import_count = 0;
        $import_data = array();
        foreach ($data_array as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $mfl = $value['facility_code'];
            $m_y = $value['month_year'];

            $import_data_['facility_code'] = $mfl;
            $import_data_['month_year'] = $m_y;
            $import_data_['county'] = $value['county'];
            $import_data_['district'] = $value['district'];
            $import_data_['facility_name'] = $value['facility_name'];
            $import_data_['order_id'] = $value['order_id'];
            $import_data_['month'] = $value['month'];
            $import_data_['year'] = $value['year'];
            $import_data_['import_status'] = $value['import_status'];

            if ($value['order_id'] > 0) {
                $import_data_['order_id'] = $value['order_id'];
                $import_data_['status'] = 'FAIL';
                $import_data_['message'] = 'Report exists on HCMP';
                $import_data_['message_raw'] = 'report_exists';
                $import_data_['current_import_status'] = 0;

            }else{
                $import_data_['status'] = 'SUCCESS';
                $import_data_['message'] = 'Report imported successfully to HCMP';
                $import_data_['message_raw'] = 'report_imported';
                $import_data_['current_import_status'] = 1;
                $import_data_['status'] = $this->import_dhis_order_raw($m_y,$mfl);

            }
            $import_data_['dhis_fcdrr_data'] = $value;
            array_push($import_data, $import_data_);
            // echo "<pre>";print_r($import_data);exit;
        }//end of import foreach

        $back_button = '<a class="btn btn-primary" id="modal_fcdrr_details" style="width: 250px;margin: 5px 0px;" data-level = "subcounty" data-level-id = "'.$dist_id.'" data-operation="view_data_by_facility" data-modal-level="facility">Back to Facility List</a>';

        $table_heads = '
                <th>County</th>
                <th>Subcounty</th>
                <th>Facility Code</th>
                <th>Facility Name</th>
                <th>Report Date</th>
                <th>Status</th>
                <th>View</th>
                ';

        $html .= '<table class="table table-bordered datatable display cell-border compact" cellspacing="0" width="100%" id="datatable">
                            <thead>
                                <tr>
                                    '.$table_heads.'
                                </tr>
                            </thead>
                            <tbody>';

        // echo "<pre>";print_r($import_data);exit;
        foreach ($import_data as $key => $value) { 
            $order_date = $value['year'].'-'.$value['month'].'-01';

            $html .= '
            <tr>
                <td>'.$value['county'].'</td>
                <td>'.$value['district'].'</td>
                <td>'.$value['facility_code'].'</td>
                <td>'.$value['facility_name'].'</td>
                <td>'.date('F Y',strtotime($order_date)).'</td>';
                
                if ($value['order_id'] > 0 && $value['import_status'] > 0) {
                    $html .= '<td> <p class="label label-info">Order was already imported from DHIS.</p> </td>';
                }elseif ($value['order_id'] > 0) {
                    $html .= '<td> <p class="label label-warning">Order was present in HCMP.</p> </td>';
                }elseif ($value['current_import_status'] > 0) {
                    $html .= '<td> <p class="label label-success">Imported successfully.</p> </td>';
                }else{
                    $html .= '<td> <p class="label label-danger">Absent in HCMP.</p> </td>';
                }

            $html .= '<td><a class="btn btn-primary" id="modal_fcdrr_details" data-level = "dhis_order" data-level-id = "'.$value['facility_code'].'" data-operation="view_data_by_id" data-month-year = "'.$value['month_year'].'" data-modal-level="dhis_order">View FCDRR</a></td>';

            $html .= '</tr>';
        
        }
        
        $html .='<tbody></table>';

        echo $back_button.$html;
    }

} //END OF RTK_ADMIN CLASS
?>
