<?php

class  Allocations_api  extends  MY_Controller  {

    function __construct()
    {
        parent::__construct(); 
        // error_reporting(1);
        // error_reporting(E_ALL);
        // ini_set('display_errors', TRUE);
        // ini_set('display_startup_errors', TRUE);

    }

    public function index($month = NULL, $mfl = NULL)
    {
    	header("Content-Type: application/json; charset=UTF-8");

        $posted = $this->input->post();
        $allocation_month_ = $this->input->get('allocationmonth', TRUE);
        $mfl_code_ = $this->input->get('mflcode', TRUE);
        $api_token = $this->input->get('api_token', TRUE);
        $county = $this->input->get('county', TRUE);

        // echo json_encode($county_name);exit;

        $token_status = $this->verify_token($api_token);

        $data_final = $data_final = array();

        if ($token_status == "success") {
            // echo json_encode($token_status);exit;
            $allocation_month = (isset($allocation_month_) && $allocation_month_ !='')? $allocation_month_:NULL;
            $mfl_code = (isset($mfl_code_) && $mfl_code_ !='')? $mfl_code_:0;
            $final_allocation_month = "01-".$allocation_month;
            // print_r($mfl_code);exit;
            $county_id = 0;
            if ($county != NULL) {
                $county = rtrim($county);
                $county = strtolower($county);
                $county = ucfirst($county);

                $c_query = "SELECT * FROM counties WHERE county = '$county'";
                $c_data = $this->db->query($c_query)->result_array();
                // echo "<pre>";print_r($c_data);exit;
                // echo json_encode($c_data);exit;

                if (count($c_data) > 0) {
                    $county_id = $c_data[0]['id'];
                }
                // echo json_encode($county_id);exit;
                // echo "<pre>";print_r($c_data);exit;

            }
            
            if ($allocation_month != NULL) {
                $month = date('m',strtotime($final_allocation_month));
                $year = date('Y',strtotime($final_allocation_month));

            }else{
                $month = date('m');
                $year = date('Y');
            }
            
            // $county_id = $this->session->userdata('county_id');

            if ($mfl_code > 0) {
            	$f_query = "SELECT * FROM facilities WHERE facility_code = '$mfl_code'";
            	$f_data = $this->db->query($f_query)->result_array();
            	if (count($f_data) > 0) {
            		$f_data = array_pop($f_data);

            		$facility_code = $f_data['facility_code'];
            		$zone = $f_data['zone'];
            		$district_id = $f_data['district'];
            	}
            	// echo "<pre>";print_r($f_data);exit;
            }

            $county_id = (isset($county_id) && $county_id > 0) ? $county_id : NULL;
            $district_id = (isset($district_id) && $district_id > 0) ? $district_id : NULL;

            $month = (isset($month) && $month!='')? $month:date('m');
            $year = (isset($year) && $year!='')? $year:date('Y');
            // echo "<pre>".$user_type." ".$county_id." ".$district_id." ".$month;exit;
            // echo json_encode($district_id);exit;

            if (is_numeric($month)) { $month = $month; }
            else{
                $monthNum = $month;
                $dateObj = DateTime::createFromFormat('!F', $monthNum);
                $month = $dateObj->format('m');         // March
            }
            


            $criteria = '';
            $status_criteria .= ($county_id > 0)?" AND counties.id = $county_id":NULL;
            $criteria .= ($district_id > 0)?" AND districts.id = $district_id":NULL;

            $first_date = $year . '-' . $month . '-01';
            $last_date = $year . '-' . $month . '-31';

            // echo $first_date;exit;
            $sql2 = "select *, counties.county as county_name from counties, districts WHERE districts.county = counties.id $criteria ";

            // echo $sql2;exit;
            $result2 = $this->db->query($sql2)->result_array();

            // echo "<pre>";print_r($result2);exit;
            $final_dets = array();

            $status_criteria = '';
            // $status_criteria .= ($county_id > 0)?" AND county_id = $county_id":NULL;
            $status_criteria .= ($county_id > 0)?" AND county_id = $county_id":NULL;
            $status_criteria .= ($district_id > 0)?" AND district_id = $district_id":NULL;

            $status_query = "SELECT * FROM allocations WHERE MONTH(created_at) = MONTH('$first_date') $status_criteria LIMIT 1";

            // echo $status_query;exit;
            $allocation_status = $this->db->query($status_query)->result_array();

            // echo "<pre>";print_r($result);exit;

            $start_date = $year . '-' . $month . '-01';

            $end_date = $year . '-' . $month . '-31';

            $alloc_status_check = $this->get_allocation_status($district_id,$start_date,$county_id);

            // echo "<pre>";print_r($alloc_status_check);exit;
            if(count($alloc_status_check) > 0){
                foreach ($alloc_status_check as $key => $value) {
                    $data_final_ = array();
                    // echo "<pre>";print_r($result_new);exit;
                    $allocation_status = $value['status'];
                    // echo "<pre>";print_r($allocation_status);exit;
                    if ($allocation_status == "Approved") {
                        $screening_kits = $confirmatory_kits = 0;
                        $allocate_s = $allocate_c = 0;
                        $mflcode = $alloc_month = $c_id = $d_id = 0;

                        // echo "<pre>";print_r($value);exit;
                        $c_id = $value['county_id'];
                        $d_id = $value['district_id'];

                        $new_criteria = '';
                        $new_criteria .= ($d_id > 0)?" AND d.id = $d_id":NULL;
                        // $new_criteria .= ($c_id > 0)?" AND c.id = $county_id":NULL;
                        $new_criteria .= ($mfl_code > 0)?" AND f.facility_code = $facility_code":NULL;
                        
                        $new_query = "
                            SELECT 
                                c.id as   county_id,
                                c.zone as   zone,
                                c.county as county_name,
                                d.district,
                                f.facility_name,
                                f.facility_code,
                                a.allocate_s,
                                ROUND((a.allocate_s/100)) as screening_kits,
                                a.allocate_c,
                                ROUND((a.allocate_c/30)) as confirmatory_kits
                            FROM
                                allocation_details a,
                                districts d,
                                counties c,
                                facilities f
                            WHERE
                                d.id = a.district_id AND c.id = d.county
                                AND d.id = f.district
                                AND f.facility_code = a.facility_code
                                $new_criteria
                                AND MONTH(a.month) = $month AND YEAR(a.month) = $year
                                GROUP BY a.facility_code,d.id
                        ";

                        // echo $new_query;exit;
                        $result_new = $this->db->query($new_query)->result_array();


                        $result_count = count($result_new);

                        // echo json_encode($result_count);exit;

                        if ($result_count>0) {
                            // echo "<pre>";print_r($result_new);exit;
                            foreach ($result_new as $key => $value) {
                                // echo json_encode($value);exit;
                                // echo "<pre>";print_r($value);exit;
                                $data_final_['Allocationmonth'] = $allocation_month;
                                $data_final_['Mflcode'] = $value['facility_code'];
                                $data_final_['Facilityname'] = $value['facility_name'];

                                // $data_final['allocate_s'] = $value['allocate_s'];
                                // $data_final['allocate_c'] = $value['allocate_c'];
                                $data_final_['Zone'] = $value['zone'];
                                $data_final_['Determine'] = $value['screening_kits'];
                                $data_final_['FirstResponse'] = $value['confirmatory_kits'];

                                array_push($data_final, $data_final_);
                            }

                            // echo json_encode($data_final);

                        }else{
                            
                            
                        }//end of result count if
                    }else{
                        $data_final_['Message'] = "There are no approved allocations matching your query.";
                        array_push($data_final, $data_final_);
                    }//if not approved

                }

                // $allocation_status = $alloc_status_check['status'];
                // echo "<pre>";print_r($allocation_status);exit;
                
            }else{
                
            }//end of alloc_status_check if
        }else{

            $data_final_['Message'] = "API Token Invalid.";
            array_push($data_final, $data_final_);
        }

        echo json_encode($data_final);

    }

    public function under_maintenance()
    {
    	// echo "BRUH";exit;
    	// return $this->load->view('general/under_maintenance');
    	$data['message'] = "Sorry, we are under maintenance. Check in later.";
    	echo json_encode($data);
    }	

    public function api_token()
    {
        //build the headers
        $headers = ['alg'=>'HS256','typ'=>'JWT'];
        $headers_encoded = base64_encode(json_encode($headers));

        //build the payload
        // $payload = ['sub'=>'allocations_api','name'=>'RTK', 'admin'=>true];
        $payload = ['sub'=>'allocations_api','name'=>'RTK'];
        $payload_encoded = base64_encode(json_encode($payload));

        //build the signature
        $key = 'RTKsecretkey$#@';
        $signature = hash_hmac('SHA256',"$headers_encoded.$payload_encoded",$key,true);
        $signature_encoded = base64_encode($signature);

        //build and return the token
        $token = "$headers_encoded.$payload_encoded.$signature_encoded";
        // echo $token;
        echo json_encode("Hi. You are not authorized to retrieve this information.");
    }

    public function verify_token($api_token)
    {
        $token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhbGxvY2F0aW9uc19hcGkiLCJuYW1lIjoiUlRLIn0=.pSScZF/vcEJTuq6rNj9JO2MkAe2AtF/qMZxZ1oWbmGM=';

        $recievedJwt = $api_token;

        $secret_key = 'RTKsecretkey$#@';

        // Split a string by '.' 
        $jwt_values = explode('.', $recievedJwt);

        // extracting the signature from the original JWT 
        $recieved_signature = $jwt_values[2];

        // concatenating the first two arguments of the $jwt_values array, representing the header and the payload
        $recievedHeaderAndPayload = $jwt_values[0] . '.' . $jwt_values[1];

        // creating the Base 64 encoded new signature generated by applying the HMAC method to the concatenated header and payload values
        $resultedsignature = base64_encode(hash_hmac('sha256', $recievedHeaderAndPayload, $secret_key, true));

        // checking if the created signature is equal to the received signature
        if($resultedsignature == $recieved_signature) {
            $status = "success";
        }else{
            $status = "fail";
        }

        return $status;
    }

 }
 ?>