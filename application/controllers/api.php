<?php
/*
Richard Karsan 2017
*/
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        ini_set('memory_limit', '-1');
        ini_set('max_input_vars', 3000);
        ini_set('post_max_size', '64M'); 
        ini_set('upload_max_filesize', '64M');
        // echo ini_get('upload_max_filesize');exit;

        /*error_reporting(1);
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        ini_set('display_errors', 1);*/

        $this -> load -> library(array('hcmp_functions', 'form_validation'));
    }

    public function index()
    {
        $this->resources();
    }

/*API RESOURCE FUNCTIONS*/
    public function resources()
    {
        $query = "SELECT * FROM api_resources";
        $result = $this->db->query($query)->result_array();

        // echo "<pre>";print_r($result);exit;
        $res = $res_ = $data = array();

        foreach ($result as $key => $value) {
            $res_['resource_title'] = $value['resource_title'];
            $res_['resource_description'] = $value['resource_description'];
            // $res_['resource_url'] = base_url().$value['resource_url'];
            $res_['href'] = base_url().$value['resource_url'];

            array_push($res, $res_);
        }
        // echo "<pre>";print_r($res);exit;
        $data['results'] = $res;

        $export = $this->export('json',$data);
    }

    public function dimensions()
    {
        $query = "SELECT * FROM api_dimensions";
        $result = $this->db->query($query)->result_array();

        // echo "<pre>";print_r($result);exit;
        $res = $res_ = $data = array();

        foreach ($result as $key => $value) {
            $res_['dimension'] = $value['parameter'];
            $res_['description'] = $value['description'];

            array_push($res, $res_);
        }
        $data['results'] = $res;
        $export = $this->export('json',$data);
    }

    public function dhis_category_option_combos()
    {
        $query = "SELECT * FROM dhis_commodity_category_combos";
        $result = $this->db->query($query)->result_array();

        // echo "<pre>";print_r($result);exit;
        $res = $res_ = $data = array();

        foreach ($result as $key => $value) {
            $res_['category'] = $value['category'];
            $res_['category_dhis_code'] = $value['category_dhis_code'];
            $res_['category_option'] = $value['category_option'];
            $res_['category_option_dhis_code'] = $value['category_option_dhis_code'];

            array_push($res, $res_);
        }
        $data['results'] = $res;
        $export = $this->export('json',$data);
    }

    public function facilities()
    {
        $query = '
            SELECT 
                d.facility_code AS d_facility_code,
                d.dhis_facility_code,
                f.*,
                di.district AS district_name,
                c.county AS county_name
            FROM
                dhis_facilities d,facilities f,districts di,counties c
            WHERE d.facility_code = f.facility_code
            AND f.district = di.id
            AND di.county = c.id;
        ';
        $result = $this->db->query($query)->result_array();

        // echo "<pre>";print_r($result);exit;
        $res = $res_ = $data = array();

        foreach ($result as $key => $value) {
            $res_['facility_code'] = $value['facility_code'];
            $res_['dhis_code'] = $value['dhis_facility_code'];
            $res_['facility_name'] = $value['facility_name'];
            $res_['subcounty'] = $value['district_name'];
            $res_['county'] = $value['county_name'];
            array_push($res, $res_);
        }
        $data['results'] = $res;

        $export = $this->export('json',$data);
    }

    public function data_elements()
    {
        $query = '
            SELECT 
                d.*,l.*
            FROM
                lab_commodities l,dhis_data_elements d
            WHERE d.commodity_id = l.id;
        ';
        $result = $this->db->query($query)->result_array();

        // echo "<pre>";print_r($result);exit;
        $res = $res_ = $data = array();

        foreach ($result as $key => $value) {
            $res_['commodity_id'] = $value['commodity_id'];
            $res_['dhis_code'] = $value['dhis_code'];
            $res_['dhis_display_name'] = $value['display_name'];
            $res_['commodity_name'] = $value['commodity_name'];
            array_push($res, $res_);
        }
        $data['results'] = $res;

        $export = $this->export('json',$data);
    }

    public function reports()
    {
        $errors = $parameters = $final_res = array();
        $county_id = $district_id = $facility_code = $year = $month = $commodity_id = NULL;
        $dhis_encoding = 0;

        $get_data = $this->input->get();
        // echo "<pre>";print_r($get_data);exit;

        $headers = $this->input->request_headers();
        // echo "<pre>";print_r($headers);exit;
        $api_token = $this->input->get_request_header('Apitoken', TRUE);
        // echo "<pre>";print_r($api_token);exit;
        $token_status = $this->verify_api_token($api_token);
        // echo "<pre>";print_r($token_status);exit; 
        /*[period] => 201711
        [mfl] => 15674
        [format] => json*/
        $data_final = $data_final = array();

        if ($token_status == "success") {
            if (empty($get_data)) {
                $res['Status'] = "Error.";
                $res['Message'] = "Required dimension variable is empty. See dimensions resource for details.";

                $final_res['results'] = $res;

                // echo "<pre>";print_r($final_res);exit;
                $export = $this->export('json',$final_res);
            }else{
                // print_r($get_data);exit;
                /*DIMENSION VALIDATION AND PROCESSING*/
                    if (!empty($get_data['dimension:period']) && strlen($get_data['dimension:period']) > 3 && is_numeric($get_data['dimension:period'])) {
                        $year_month = $get_data['dimension:period'];

                        // echo "<pre>";print_r($get_data['dimension:period']);exit;
                        if (strlen($get_data['dimension:period']) == 4) {
                            $year = substr($year_month, 0, 4);
                            $month = NULL;
                        }elseif (strlen($get_data['dimension:period']) == 6) {
                            $year = substr($year_month, 0, 4);
                            $month = substr($year_month, -2);
                        }

                        $parameters['dimension:year']=$year;
                        $parameters['dimension:month']=$month;

                    }else{
                        // echo "dimension:period is empty";exit;
                        $parameters['dimension:period']="Period dimension is either empty or not numeric.";
                    }

                    if (!empty($get_data['dimension:mfl']) && is_numeric($get_data['dimension:mfl'])) {
                        $facility_code = $get_data['dimension:mfl'];
                        $parameters['dimension:mfl']=$facility_code;

                    }else{
                        // echo "dimension:period is empty";exit;
                        $parameters['dimension:mfl']="Facility code dimension is either empty or not numeric.";
                    }

                    if (!empty($get_data['dimension:county']) && is_string($get_data['dimension:county'])) {
                        $county_name = $get_data['dimension:county'];
                        $county_name = strtolower($county_name);
                        $county_name = ucfirst($county_name);
                        $county_name = rtrim($county_name);

                        $chk_query = "SELECT * FROM counties WHERE county = '$county_name'";
                        $chck_result = $this->db->query($chk_query);

                        if ($chck_result->num_rows() > 0) {
                        // echo "<pre>";print_r($chck_result->result_array());exit;
                            $chck_result = $chck_result->result_array();
                            $county_id = $chck_result[0]['id'];
                            $parameters['dimension:county_id']=$county_id;
                            $parameters['dimension:county']=$county_name;
                        }else{
                            $errors['dimension:county']="County does not exist in database. Check spelling and try again.";
                        }
                        
                    }else{
                        // echo "dimension:period is empty";exit;
                        $parameters['dimension:county']="County dimension is either empty or not string.";
                    }

                    if (!empty($get_data['dimension:subcounty']) && is_string($get_data['dimension:subcounty'])) {
                        $subcounty_name = $get_data['dimension:subcounty'];
                        $subcounty_name = strtolower($subcounty_name);
                        $subcounty_name = ucwords($subcounty_name);
                        $subcounty_name = rtrim($subcounty_name);

                        // echo "<pre>";print_r($subcounty_name);exit;
                        $chk_query = "SELECT * FROM districts WHERE district = '$subcounty_name'";
                        $chck_result = $this->db->query($chk_query);
                        // echo "<pre>";print_r($chck_result);exit;
                        if ($chck_result->num_rows() > 0) {
                        // echo "<pre>";print_r($chck_result->result_array());exit;
                            $chck_result = $chck_result->result_array();
                            $district_id = $chck_result[0]['id'];
                            $parameters['dimension:subcounty_id']=$district_id;
                            $parameters['dimension:subcounty']=$subcounty_name;
                        }else{
                            $errors['dimension:subcounty']="Subcounty does not exist in database. Check spelling and try again.";
                        }
                        
                    }else{
                        // echo "dimension:period is empty";exit;
                        $parameters['dimension:subcounty']="Subcounty dimension is either empty or not string.";
                    }

                    if (!empty($get_data['dimension:commodity_id']) && is_numeric($get_data['dimension:commodity_id']) && $get_data['dimension:commodity_id'] == 4 || $get_data['dimension:commodity_id'] == 5) {
                        $commodity_id = $get_data['dimension:commodity_id'];
                        $parameters['dimension:commodity_id']=$commodity_id;

                    }else{
                        // echo "dimension:period is empty";exit;
                        $parameters['dimension:commodity_id']="Commodity ID dimension is either empty or not either 4 or 5.";
                    }

                    if (!empty($get_data['dimension:format']) && is_string($get_data['dimension:format'])) {
                        $format = $get_data['dimension:format'];
                    }else{
                        // echo "dimension:period is empty";exit;
                        $parameters['dimension:format']="Format dimension is not set. Default of JSON will be used.";
                    }

                    if (!empty($get_data['dimension:dhis_encoding']) && $get_data['dimension:dhis_encoding'] == TRUE || $get_data['dimension:dhis_encoding'] == true || $get_data['dimension:dhis_encoding'] == FALSE || $get_data['dimension:dhis_encoding'] == false) {
                        $dhis_encoding = $get_data['dimension:dhis_encoding'];
                        // echo "<pre>";print_r($get_data['dimension:dhis_encoding']);exit;
                        // $dhis_encoding = 1;
                    }else{
                        // echo "dimension:period is empty";exit;
                        $parameters['dimension:dhis_encoding']="dhis_encoding dimension is not set. Default of TRUE will be used.";
                    }
                /*END OF DIMENSION VALIDATION AND PROCESSING*/

                // echo "<pre>";print_r($dhis_organisation_units);exit;
                // echo "COUNTY:".$county_id."DISTRICT: ".$district_id." MFL: ".$facility_code." COMMODITY_ID".$commodity_id." QUARTER: ".$quarter." MONTH: ".$month." YEAR: ".$year." BY: ".$by." DHIS_ENCODING: ".$dhis_encoding;exit;
                $report_data = $this->get_report_data($county_id, $district_id, $facility_code, $commodity_id, $quarter,$month,$year,$by,$dhis_encoding);
                // echo "<pre>";print_r($dhis_encoding);exit;
                // echo "<pre>";print_r($report_data);exit;

                /*$final_data = $final_data_ = array();
                foreach ($report_data as $key => $value) {
                    echo "<pre>";print_r($value);exit;
                }*/

                switch ($format) {
                    case 'csv':
                        $export = $this->export('csv',$report_data,$dhis_encoding);
                        break;

                    case 'json':
                        $export = $this->export('json',$report_data,$dhis_encoding);
                        break;

                    case 'dhis_json':
                        $export = $this->export('dhis_json',$report_data,$dhis_encoding);
                        break;

                    case 'dhis_csv':
                        $export = $this->export('dhis_csv',$report_data,$dhis_encoding);
                        break;
                    
                    default:
                        $export = $this->export('json',$report_data,$dhis_encoding);
                        break;
                }
            }//END OF IF GET HAS DATA
            
        }else{
            $data_final_['Message'] = "API Token Invalid.";
            array_push($data_final, $data_final_);
            echo json_encode($data_final);
        }

    }

    public function kemsa_issue_data_analysis($county_id = NULL,$district_id = NULL,$facility_code = NULL,$commodity_id = NULL,$quarter = NULL,$year = NULL,$month = NULL,$by = NULL,$format = NULL)
    {
        $get_data = $this->input->get();
        $errors = $parameters = $final_res = array();
        $county_id = $district_id = $facility_code = $year = $month = $commodity_id = NULL;
        $dhis_encoding = 0;
        
        // echo "<pre>";print_r($get_data);exit;
        if (!empty($get_data)) {
            $res['Status'] = "Error.";
            $res['Message'] = "Required dimension variable is empty. See dimensions resource for details.";

            $final_res['results'] = $res;

            /*DIMENSION CHECK*/
                // print_r($get_data);exit;
                if (!empty($get_data['dimension:period']) && strlen($get_data['dimension:period']) == 6 && is_numeric($get_data['dimension:period'])) {
                    $year_month = $get_data['dimension:period'];

                    // echo "<pre>";print_r($get_data['dimension:period']);exit;
                    $year = substr($year_month, 0, 4);
                    $month = substr($year_month, -2);
                    $parameters['dimension:year']=$year;
                    $parameters['dimension:month']=$month;

                }else{
                    // echo "dimension:period is empty";exit;
                    $parameters['dimension:period']="Period dimension is either empty or not numeric.";
                }

                if (!empty($get_data['dimension:mfl']) && is_numeric($get_data['dimension:mfl'])) {
                    $facility_code = $get_data['dimension:mfl'];
                    $parameters['dimension:mfl']=$facility_code;

                }else{
                    // echo "dimension:period is empty";exit;
                    $parameters['dimension:mfl']="Facility code dimension is either empty or not numeric.";
                }

                if (!empty($get_data['dimension:county']) && is_string($get_data['dimension:county'])) {
                    $county_name = $get_data['dimension:county'];
                    $county_name = strtolower($county_name);
                    $county_name = ucfirst($county_name);
                    $county_name = rtrim($county_name);

                    $chk_query = "SELECT * FROM counties WHERE county = '$county_name'";
                    $chck_result = $this->db->query($chk_query);

                    if ($chck_result->num_rows() > 0) {
                    // echo "<pre>";print_r($chck_result->result_array());exit;
                        $chck_result = $chck_result->result_array();
                        $county_id = $chck_result[0]['id'];
                        $parameters['dimension:county_id']=$county_id;
                        $parameters['dimension:county']=$county_name;
                    }else{
                        $errors['dimension:county']="County does not exist in database. Check spelling and try again.";
                    }
                    
                }else{
                    // echo "dimension:period is empty";exit;
                    $parameters['dimension:county']="County dimension is either empty or not string.";
                }

                if (!empty($get_data['dimension:subcounty']) && is_string($get_data['dimension:subcounty'])) {
                    $subcounty_name = $get_data['dimension:subcounty'];
                    $subcounty_name = strtolower($subcounty_name);
                    $subcounty_name = ucwords($subcounty_name);
                    $subcounty_name = rtrim($subcounty_name);

                    // echo "<pre>";print_r($subcounty_name);exit;
                    $chk_query = "SELECT * FROM districts WHERE district = '$subcounty_name'";
                    $chck_result = $this->db->query($chk_query);
                    // echo "<pre>";print_r($chck_result);exit;
                    if ($chck_result->num_rows() > 0) {
                    // echo "<pre>";print_r($chck_result->result_array());exit;
                        $chck_result = $chck_result->result_array();
                        $district_id = $chck_result[0]['id'];
                        $parameters['dimension:subcounty_id']=$district_id;
                        $parameters['dimension:subcounty']=$subcounty_name;
                    }else{
                        $errors['dimension:subcounty']="Subcounty does not exist in database. Check spelling and try again.";
                    }
                    
                }else{
                    // echo "dimension:period is empty";exit;
                    $parameters['dimension:subcounty']="Subcounty dimension is either empty or not string.";
                }

                if (!empty($get_data['dimension:commodity_id']) && is_numeric($get_data['dimension:commodity_id']) && $get_data['dimension:commodity_id'] == 4 || $get_data['dimension:commodity_id'] == 5) {
                    $commodity_id = $get_data['dimension:commodity_id'];
                    $parameters['dimension:commodity_id']=$commodity_id;

                }else{
                    // echo "dimension:period is empty";exit;
                    $parameters['dimension:commodity_id']="Commodity ID dimension is either empty or not either 4 or 5.";
                }

                if (!empty($get_data['dimension:format']) && is_string($get_data['dimension:format'])) {
                    $format = $get_data['dimension:format'];
                }else{
                    // echo "dimension:period is empty";exit;
                    $parameters['dimension:format']="Format dimension is not set. Default of JSON will be used.";
                }

                if (!empty($get_data['dimension:dhis_encoding']) && $get_data['dimension:dhis_encoding'] == TRUE || $get_data['dimension:dhis_encoding'] == true || $get_data['dimension:dhis_encoding'] == FALSE || $get_data['dimension:dhis_encoding'] == false) {
                    $dhis_encoding = $get_data['dimension:dhis_encoding'];
                    // echo "<pre>";print_r($get_data['dimension:dhis_encoding']);exit;
                    // $dhis_encoding = 1;
                }else{
                    // echo "dimension:period is empty";exit;
                    $parameters['dimension:dhis_encoding']="dhis_encoding dimension is not set. Default of TRUE will be used.";
                }
            /*END OF DIMENSION CHECK*/
        }

        $county_id = (isset($county_id) && $county_id > 0)? $county_id:NULL;
        $district_id = (isset($district_id) && $district_id > 0)? $district_id:NULL;
        $facility_code = (isset($facility_code) && $facility_code > 0)? $facility_code:NULL;
        $commodity_id = (isset($commodity_id) && $commodity_id > 0)? $commodity_id:NULL;
        $quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;
        $quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;

        $by = "facility";
        $local_data_json = $this->get_kemsa_issue_data($county_id,$district_id,$facility_code,$commodity_id,$quarter,$year,$month,$by,$format);
        $by = "facility_month_year";
        $local_data_mfl_m_yr_json = $this->get_kemsa_issue_data($county_id,$district_id,$facility_code,$commodity_id,$quarter,$year,$month,$by,$format);
        // echo "<pre>";print_r($local_data_mfl_m_yr_json);exit;
        if ($format == 'json') {
            $local_data = json_decode($local_data_json,true);
            $local_data_m_yr = json_decode($local_data_mfl_m_yr_json,true);
        }else{
            $local_data = $local_data_json;
            $local_data_m_yr = $local_data_mfl_m_yr_json;
        }
        // echo "<pre>";print_r($local_data);exit;
        // echo "<pre>";print_r($local_data_m_yr);exit;

        $local_by_month_year = array();
        $local_mfl_month_years = array();
        $local_month_years_mfl = array();
        $local_by_mfl = array();
        $local_by_delivery_note = array();

        // echo "<pre>THIS: ";print_r($local_data);exit;
        // foreach ($local_data as $key => $value) {
        
        foreach ($local_data_m_yr as $key => $value) {
            // echo "<pre>";print_r($key);exit;
            $month_year = $key;
            
            foreach ($value as $keyy => $valuee) {
                // echo "<pre>";print_r($valuee);exit;
                $mfl = $valuee['facility_code'];
                $month_year = date('mY',strtotime($valuee['issue_date']));
                $d_note = $valuee['delivery_note'];
                $commodity_id = $valuee['commodity_id'];

                $local_mfl_month_years[$mfl][$commodity_id][] = $month_year;
                $local_month_years_mfl[$month_year][$commodity_id][] = $mfl;

                $local_by_mfl[$mfl][$month_year][$commodity_id]['county_id'] = $valuee['county_id'];
                $local_by_mfl[$mfl][$month_year][$commodity_id]['county'] = $valuee['county'];
                $local_by_mfl[$mfl][$month_year][$commodity_id]['district_id'] = $valuee['district_id'];
                $local_by_mfl[$mfl][$month_year][$commodity_id]['district'] = $valuee['district'];
                $local_by_mfl[$mfl][$month_year][$commodity_id]['facility_code'] = $valuee['facility_code'];
                $local_by_mfl[$mfl][$month_year][$commodity_id]['facility_name'] = $valuee['facility_name'];
                $local_by_mfl[$mfl][$month_year][$commodity_id]['commodity_id'] = $valuee['commodity_id'];
                $local_by_mfl[$mfl][$month_year][$commodity_id]['qty_issued'] = $valuee['qty_issued'];
                $local_by_mfl[$mfl][$month_year][$commodity_id]['qty_requested'] = $valuee['qty_requested'];
                $local_by_mfl[$mfl][$month_year][$commodity_id]['value_issued'] = $valuee['value_issued'];
                $local_by_mfl[$mfl][$month_year][$commodity_id]['value_requested'] = $valuee['value_requested'];
                $local_by_mfl[$mfl][$month_year][$commodity_id]['delivery_note'] = $valuee['delivery_note'];
                $local_by_mfl[$mfl][$month_year][$commodity_id]['issue_date'] = $valuee['issue_date'];

                $local_by_month_year[$month_year][$mfl][$commodity_id]['county_id'] = $valuee['county_id'];
                $local_by_month_year[$month_year][$mfl][$commodity_id]['county'] = $valuee['county'];
                $local_by_month_year[$month_year][$mfl][$commodity_id]['district_id'] = $valuee['district_id'];
                $local_by_month_year[$month_year][$mfl][$commodity_id]['district'] = $valuee['district'];
                $local_by_month_year[$month_year][$mfl][$commodity_id]['facility_code'] = $valuee['facility_code'];
                $local_by_month_year[$month_year][$mfl][$commodity_id]['facility_name'] = $valuee['facility_name'];
                $local_by_month_year[$month_year][$mfl][$commodity_id]['commodity_id'] = $valuee['commodity_id'];
                $local_by_month_year[$month_year][$mfl][$commodity_id]['qty_issued'] = $valuee['qty_issued'];
                $local_by_month_year[$month_year][$mfl][$commodity_id]['qty_requested'] = $valuee['qty_requested'];
                $local_by_month_year[$month_year][$mfl][$commodity_id]['value_issued'] = $valuee['value_issued'];
                $local_by_month_year[$month_year][$mfl][$commodity_id]['value_requested'] = $valuee['value_requested'];
                $local_by_month_year[$month_year][$mfl][$commodity_id]['delivery_note'] = $valuee['delivery_note'];
                $local_by_month_year[$month_year][$mfl][$commodity_id]['issue_date'] = $valuee['issue_date'];

                $local_by_delivery_note[$d_note][$commodity_id]['county_id'] = $valuee['county_id'];
                $local_by_delivery_note[$d_note][$commodity_id]['county'] = $valuee['county'];
                $local_by_delivery_note[$d_note][$commodity_id]['district_id'] = $valuee['district_id'];
                $local_by_delivery_note[$d_note][$commodity_id]['district'] = $valuee['district'];
                $local_by_delivery_note[$d_note][$commodity_id]['facility_code'] = $valuee['facility_code'];
                $local_by_delivery_note[$d_note][$commodity_id]['facility_name'] = $valuee['facility_name'];
                $local_by_delivery_note[$d_note][$commodity_id]['commodity_id'] = $valuee['commodity_id'];
                $local_by_delivery_note[$d_note][$commodity_id]['qty_issued'] = $valuee['qty_issued'];
                $local_by_delivery_note[$d_note][$commodity_id]['qty_requested'] = $valuee['qty_requested'];
                $local_by_delivery_note[$d_note][$commodity_id]['value_issued'] = $valuee['value_issued'];
                $local_by_delivery_note[$d_note][$commodity_id]['value_requested'] = $valuee['value_requested'];
                $local_by_delivery_note[$d_note][$commodity_id]['delivery_note'] = $valuee['delivery_note'];
                $local_by_delivery_note[$d_note][$commodity_id]['issue_date'] = $valuee['issue_date'];
            }

            
        }

        /*foreach ($local_data as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $mfl = $value['facility_code'];
            $month_year = date('mY',strtotime($value['issue_date']));
            $d_note = $value['delivery_note'];
            $commodity_id = $value['commodity_id'];

            $local_mfl_month_years[$mfl][$commodity_id][] = $month_year;
            $local_month_years_mfl[$month_year][$commodity_id][] = $mfl;

            $local_by_mfl[$mfl][$month_year][$commodity_id]['county_id'] = $value['county_id'];
            $local_by_mfl[$mfl][$month_year][$commodity_id]['county'] = $value['county'];
            $local_by_mfl[$mfl][$month_year][$commodity_id]['district_id'] = $value['district_id'];
            $local_by_mfl[$mfl][$month_year][$commodity_id]['district'] = $value['district'];
            $local_by_mfl[$mfl][$month_year][$commodity_id]['facility_code'] = $value['facility_code'];
            $local_by_mfl[$mfl][$month_year][$commodity_id]['facility_name'] = $value['facility_name'];
            $local_by_mfl[$mfl][$month_year][$commodity_id]['commodity_id'] = $value['commodity_id'];
            $local_by_mfl[$mfl][$month_year][$commodity_id]['qty_issued'] = $local_data_m_yr[$month_year][$mfl]['qty_issued'];
            $local_by_mfl[$mfl][$month_year][$commodity_id]['qty_requested'] = $local_data_m_yr[$month_year][$mfl]['qty_requested'];
            $local_by_mfl[$mfl][$month_year][$commodity_id]['value_issued'] = $local_data_m_yr[$month_year][$mfl]['value_issued'];
            $local_by_mfl[$mfl][$month_year][$commodity_id]['value_requested'] = $local_data_m_yr[$month_year][$mfl]['value_requested'];
            $local_by_mfl[$mfl][$month_year][$commodity_id]['delivery_note'] = $local_data_m_yr[$month_year][$mfl]['delivery_note'];
            $local_by_mfl[$mfl][$month_year][$commodity_id]['issue_date'] = $local_data_m_yr[$month_year][$mfl]['issue_date'];

            $local_by_month_year[$month_year][$mfl][$commodity_id]['county_id'] = $value['county_id'];
            $local_by_month_year[$month_year][$mfl][$commodity_id]['county'] = $value['county'];
            $local_by_month_year[$month_year][$mfl][$commodity_id]['district_id'] = $value['district_id'];
            $local_by_month_year[$month_year][$mfl][$commodity_id]['district'] = $value['district'];
            $local_by_month_year[$month_year][$mfl][$commodity_id]['facility_code'] = $value['facility_code'];
            $local_by_month_year[$month_year][$mfl][$commodity_id]['facility_name'] = $value['facility_name'];
            $local_by_month_year[$month_year][$mfl][$commodity_id]['commodity_id'] = $local_data_m_yr[$month_year][$mfl]['commodity_id'];
            $local_by_month_year[$month_year][$mfl][$commodity_id]['qty_issued'] = $local_data_m_yr[$month_year][$mfl]['qty_issued'];
            $local_by_month_year[$month_year][$mfl][$commodity_id]['qty_requested'] = $local_data_m_yr[$month_year][$mfl]['qty_requested'];
            $local_by_month_year[$month_year][$mfl][$commodity_id]['value_issued'] = $local_data_m_yr[$month_year][$mfl]['value_issued'];
            $local_by_month_year[$month_year][$mfl][$commodity_id]['value_requested'] = $value['value_requested'];
            $local_by_month_year[$month_year][$mfl][$commodity_id]['delivery_note'] = $local_data_m_yr[$month_year][$mfl]['delivery_note'];
            $local_by_month_year[$month_year][$mfl][$commodity_id]['issue_date'] = $local_data_m_yr[$month_year][$mfl]['issue_date'];

            $local_by_delivery_note[$d_note][$commodity_id]['county_id'] = $value['county_id'];
            $local_by_delivery_note[$d_note][$commodity_id]['county'] = $value['county'];
            $local_by_delivery_note[$d_note][$commodity_id]['district_id'] = $value['district_id'];
            $local_by_delivery_note[$d_note][$commodity_id]['district'] = $value['district'];
            $local_by_delivery_note[$d_note][$commodity_id]['facility_code'] = $value['facility_code'];
            $local_by_delivery_note[$d_note][$commodity_id]['facility_name'] = $value['facility_name'];
            $local_by_delivery_note[$d_note][$commodity_id]['commodity_id'] = $value['commodity_id'];
            $local_by_delivery_note[$d_note][$commodity_id]['qty_issued'] = $local_data_m_yr[$month_year][$mfl]['qty_issued'];
            $local_by_delivery_note[$d_note][$commodity_id]['qty_requested'] = $local_data_m_yr[$month_year][$mfl]['qty_requested'];
            $local_by_delivery_note[$d_note][$commodity_id]['value_issued'] = $local_data_m_yr[$month_year][$mfl]['value_issued'];
            $local_by_delivery_note[$d_note][$commodity_id]['value_requested'] = $local_data_m_yr[$month_year][$mfl]['value_requested'];
            $local_by_delivery_note[$d_note][$commodity_id]['delivery_note'] = $local_data_m_yr[$month_year][$mfl]['delivery_note'];
            $local_by_delivery_note[$d_note][$commodity_id]['issue_date'] = $local_data_m_yr[$month_year][$mfl]['issue_date'];
        }*/
        
        // echo "<pre>";print_r($local_by_mfl);exit;
        // echo "<pre>";print_r($local_by_month_year);
        // echo "<pre>";print_r($local_by_delivery_note);exit;
        
        // echo "<pre>";print_r($cache_key);exit;
        
        $cache_key_plain_scr = 'kemsa_api_data_scr'.date('Y-m-d');
        $cache_key_scr = $this->generate_memcached_key($cache_key_plain_scr);

        $cache_key_plain_conf = 'kemsa_api_data_conf'.date('Y-m-d');
        $cache_key_conf = $this->generate_memcached_key($cache_key_plain_conf);

        /*
        echo "<pre>";print_r($cache_key_scr);
        echo "<pre>";print_r($cache_key_conf);exit;
        */

        $cache_check_scr = $this->memcached_get($cache_key_scr);
        // echo "<pre>";print_r($cache_check_scr);exit;
        if ($cache_check_scr['status'] == "FAILED") {
            $kemsa_api_data_scr = $this->get_issues_from_kemsa_api(4,NULL,"json");

             $cache_data['key'] = $cache_key_scr;
            $cache_data['data'] = $kemsa_api_data_scr;

            $cache_save = $this->memcached_save($cache_data);
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $kemsa_api_data_scr = $cache_check_scr['data'];
        }         

        $cache_check_conf = $this->memcached_get($cache_key_conf);
        // echo "<pre>CONF CACHE: ";print_r($cache_check_conf);exit;
        if ($cache_check_conf['status'] == "FAILED") {
            $kemsa_api_data_conf = $this->get_issues_from_kemsa_api(5,NULL,"json");
            // echo "<pre>CONF: ";print_r($kemsa_api_data_conf);exit;
            $cache_data['key'] = $cache_key_conf;
            $cache_data['data'] = $kemsa_api_data_conf;

            $cache_save = $this->memcached_save($cache_data);
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $kemsa_api_data_conf = $cache_check_conf['data'];
        }

        // echo "<pre>";print_r($kemsa_api_data_conf);exit;

        $scr_api_data = json_decode($kemsa_api_data_scr,true);
        $conf_api_data = json_decode($kemsa_api_data_conf,true);
        // echo "<pre>";print_r($scr_api_data);exit;

        $value_differences = $d_notes = $new_records = $valid = array();

        $api_data = array_merge($scr_api_data,$conf_api_data);
        // echo "<pre>";print_r($api_data);exit;
        foreach ($api_data as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $mfl = $value['facility_code'];
            $comm_id = $value['commodity_id'];
            $date = $value['issue_date'];
            $api_month_year = date('mY',strtotime($value['issue_date']));
            $d_note = $value['delivery_note'];
            $qty_issued = $value['qty_issued'];
            $qty_requested = $value['qty_requested'];
            $value_issued = $value['value_issued'];
            $value_requested = $value['value_requested'];

            // echo $mfl." MONTH_YEAR: ".$api_month_year." COMM_ID: ".$comm_id;exit;
            // echo "<pre>";print_r($local_month_years_mfl[$api_month_year][$comm_id]);exit;
            if(!empty($local_month_years_mfl[$api_month_year][$comm_id])){
                $mfl_search_by_month = array_search($mfl,$local_month_years_mfl[$api_month_year][$comm_id]);

                if (isset($mfl_search_by_month) && $mfl_search_by_month !='') {
                    // echo "PRESENT RECORD: ISSUE FOR MONTH: ".$api_month_year." FOR FACILITY: ".$mfl." FOUND.";
                    $d_note_data = $local_by_delivery_note[$d_note][$comm_id];
                    if (!empty($d_note_data)) {
                        $present_d_note_month_year = date('mY',strtotime($d_note_data['issue_date']));

                        if ($api_month_year == $present_d_note_month_year) {
                            // echo "PRESENT RECORD: D_NOTE: ".$d_note." FOR COMMODITY_ID: ".$comm_id." FOUND.";
                            // echo "<pre>";print_r($d_note_data);echo"</pre>";exit;
                            $present_qty_issued = $d_note_data['qty_issued'];
                            $present_qty_requested = $d_note_data['qty_requested'];
                            $present_value_issued = $d_note_data['value_issued'];
                            $present_value_requested = $d_note_data['value_requested'];

                            if ($present_qty_issued == $qty_issued) {
                                if ($present_qty_requested == $qty_requested) {
                                        //PENDING IF ELSES ON VALUE REQUESTED AND VALUE ISSUED.
                                        $valid[$comm_id][$d_note]['message'] = "PRESENT RECORD: QUANTITY REQUESTED && ISSUED VALID";
                                        $valid[$comm_id][$d_note]['current'] = $value;
                                }else{//else different quantity requested
                                    $value_differences[$comm_id]['quantity_requested'][$d_note]['message'] = "PRESENT RECORD: QUANTITY REQUESTED DIFFERENCE. PREVIOUS: ".$present_qty_requested." NEW: ".$qty_requested;
                                    $value_differences[$comm_id]['quantity_requested'][$d_note]['current'] = $d_note_data;
                                    $value_differences[$comm_id]['quantity_requested'][$d_note]['new'] = $value;
                                    // echo "<pre>";print_r($value_differences);exit;
                                }
                            }else{//else different quantity issued
                                $value_differences[$comm_id]['quantity_issued'][$d_note]['message'] = "PRESENT RECORD: QUANTITY ISSUED DIFFERENCE. PREVIOUS: ".$present_qty_issued." NEW: ".$qty_issued;
                                $value_differences[$comm_id]['quantity_issued'][$d_note]['current'] = $d_note_data;
                                    $value_differences[$comm_id]['quantity_issued'][$d_note]['new'] = $value;
                                }
                            }else{//else d_note dates don't match
                                $d_notes[$comm_id]['dates_altered'][$d_note]['message'] = "PRESENT RECORD: D_NOTE: ".$d_note." DATE ALTERED. INITIAL DATE: ".$present_d_note_month_year." NEW DATE:".$api_month_year;
                                $d_notes[$comm_id]['dates_altered'][$d_note]['current'] = $d_note_data;
                                $d_notes[$comm_id]['dates_altered'][$d_note]['new'] = $value;
                            }
                        }else{//else no delivery_note for facility that had issue
                            $d_notes[$comm_id]['absent'][$d_note]['message'] = "PRESENT RECORD: D_NOTE: ".$d_note." PREVIOUSLY PRESENT FOR COMMODITY_ID: ".$comm_id." NOT FOUND.";
                            $d_notes[$comm_id]['absent'][$d_note]['new'] = $value;
                            $d_notes[$comm_id]['absent'][$d_note]['current'] = $d_note_data;
                        }
                    }else{//else no issue for that facility for that month
                        // echo "NEW RECORD: ISSUE FOR MONTH: ".$month_year." FOR FACILITY: ".$mfl." NOT FOUND.";
                        $new_records[$api_month_year][$comm_id] = $value;
                    }//end of new record if
                }else{
                        $new_records[$api_month_year][$comm_id] = $value;
                }

        }//END OF API DATA FOREACH
        
        // echo "<pre>";print_r($d_notes);exit;
        $final_analysis['value_differences'] = $value_differences;
        $final_analysis['delivery_notes'] = $d_notes;
        $final_analysis['valid'] = $valid;
        $final_analysis['new_records'] = $new_records;
        $final_analysis['api_data'] = $api_data;
        $final_analysis['local_data'] = $local_data;

        // echo "<pre>";print_r($final_analysis);exit;
        $data['results'] = $final_analysis;

        // echo "<pre>";print_r($format);exit;

        if (isset($format) && $format !='') {
            $export = $this->export($format,$data);
        }else{
            return json_encode($final_analysis,JSON_UNESCAPED_SLASHES);
        }

    }
/*END OF API RESOURCE FUNCTIONS*/

/*DATA RETRIEVAL FUNCTIONS*/
    public function get_report_data($county_id = NULL, $district_id = NULL, $facility_code = NULL, $commodity_id = NULL, $quarter = NULL,$month = NULL, $year = NULL,$by = NULL,$dhis_encoding = NULL)
    {
        // echo "COUNTY: ".$county_id." DISTRICT: ".$district_id." MFL: ".$facility_code." QUARTER: ".$quarter." MONTH: ".$month." YEAR: ".$year." BY: ".$by;exit;
        // echo "<pre>MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;
        $county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
        $district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
        $facility_code = (isset($facility_code) && $facility_code!='')? $facility_code:NULL;
        $dhis_encoding = (isset($dhis_encoding) && $dhis_encoding!='')? $dhis_encoding:TRUE;
        // echo "<pre>";print_r($dhis_encoding);exit;

        // echo "<pre>";print_r($district_id);exit;
        $new_criteria = "";

        if ($quarter > 0 && $month < 1) {
            $year_ = date('Y');
            switch ($quarter) {
                case 1:
                    // echo "Quarter: One";exit;
                    $first_month = "01";
                    $last_month = "03";
                    $firstdate = $year_ . '-' . $first_month . '-01';
                    $lastdate = $year_ . '-' . $last_month . '-31';
                    break;
                case 2:
                    // echo "Quarter: Two";exit;
                    $first_month = "04";
                    $last_month = "06";
                    $firstdate = $year_ . '-' . $first_month . '-01';
                    $lastdate = $year_ . '-' . $last_month . '-31';
                    break;
                case 3:
                    // echo "Quarter: Three";exit;
                    $first_month = "07";
                    $last_month = "09";
                    $firstdate = $year_ . '-' . $first_month . '-01';
                    $lastdate = $year_ . '-' . $last_month . '-31';
                    break;
                case 4:
                    // echo "Quarter: Four";exit;
                    $first_month = "10";
                    $last_month = "12";
                    $firstdate = $year_ . '-' . $first_month . '-01';
                    $lastdate = $year_ . '-' . $last_month . '-31';
                    break;
                
                default:
                    // echo "Quarter: Invalid";exit;
                    break;
            }
            $data['date'] = "Quarter: ".$quarter;

            $new_criteria .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
        }elseif (isset($month) && $month > 0) {
            $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');

            $firstdate = $year . '-' . $month . '-01';
            // $lastdate = $year . '-' . $month . '-31';
            $data['date_text'] = date('F Y',strtotime($firstdate));
            $data['date'] = date('Ym',strtotime($firstdate));

            $firstdate = date('Y-m-d',strtotime("+1 months",strtotime($firstdate)));
            // echo "<pre>";print_r($firstdate);exit;
            $m = date('m',strtotime($firstdate));
            $lastdate = $year . '-' . $m . '-31';

            $new_criteria .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 

        }else{
            $year = (isset($year) && $year>0)? $year:date('Y');
            $new_criteria .= ($year > 0)?" AND YEAR(l.created_at) = $year":"AND YEAR(lco.order_date) = ".date('Y');
            $data['date'] = "Year: ".$year;
        }

        // $year = date('Y');
        if ($facility_code > 0) {
            $new_criteria .= " AND l.facility_code = $facility_code ";
        }else{
            if ($district_id > 0) {
                $new_criteria .= "  AND d.id = $district_id ";
            }else{
                if ($county_id > 0) {
                       $new_criteria .= " AND c.id = $county_id ";
                   }   
            }
        }

        if ($facility_code > 0) {
            $cols = "
                l.beginning_bal AS beginning_bal,
                l.q_received AS q_received,
                l.q_used AS q_used,
                l.no_of_tests_done AS no_of_tests_done,
                l.closing_stock AS closing_stock,
                l.q_expiring AS q_expiring,
                l.q_requested AS q_requested,
                l.positive_adj AS positive_adj,
                l.negative_adj AS negative_adj,
                l.losses AS losses,";
        }else{
            $cols = "
                SUM(l.beginning_bal) AS beginning_bal,
                SUM(l.q_received) AS q_received,
                SUM(l.q_used) AS q_used,
                SUM(l.no_of_tests_done) AS no_of_tests_done,
                SUM(l.closing_stock) AS closing_stock,
                SUM(l.q_expiring) AS q_expiring,
                SUM(l.q_requested) AS q_requested,
                SUM(l.positive_adj) AS positive_adj,
                SUM(l.negative_adj) AS negative_adj,
                SUM(l.losses) AS losses,";
        }

        $c_ids = (isset($commodity_id) && $commodity_id!='')? "'".$commodity_id."'" : "4,5";

        $group_by = "GROUP BY f.facility_code,commodity_id ";


        // echo $new_criteria;exit;
        $query="
            SELECT 
                c.id AS county_id,
                c.county,
                d.id AS district_id,
                d.district,
                f.facility_name,
                f.facility_code,
                l.district_id,
                l.commodity_id,
                lco.order_date,
                
                l.beginning_bal AS beginning_bal,
                l.q_received AS q_received,
                l.q_used AS q_used,
                l.no_of_tests_done AS no_of_tests_done,
                l.closing_stock AS closing_stock,
                l.physical_closing_stock AS physical_closing_stock,
                l.q_expiring AS q_expiring,
                l.q_requested AS q_requested,
                l.positive_adj AS positive_adj,
                l.negative_adj AS negative_adj,
                l.losses AS losses,

                l.created_at
            FROM
                lab_commodity_details l USE INDEX (order_id,facility_code,district),
                counties c,
                districts d,
                facilities f USE INDEX (facility_code),
                lab_commodity_orders lco USE INDEX (district)
            WHERE
                c.id = d.county
                    AND l.order_id = lco.id
                    AND l.facility_code > 0
                    AND l.facility_code = f.facility_code
                    AND lco.district_id = d.id
                    AND l.commodity_id IN ($c_ids)
                    $new_criteria
                    $group_by
                    ORDER BY lco.order_date DESC;        
            ";
        // echo "<pre>";print_r($query);exit;
        $res = $this->db->query($query);
        // $res = $res->result_array();
        // echo "<pre>";print_r($res);exit;
        if ($res->num_rows() > 0) {
            $results = $res->result_array();
            // echo "<pre>";print_r($results);exit;
            $data_ = array();
            foreach ($results as $key => $value) {
                $mfl = $value['facility_code'];
                $comm_id = $value['commodity_id'];

                // echo "<pre>";print_r($value);exit;
                $data_[$mfl][$comm_id]['facility_code'] = $value['facility_code'];
                $data_[$mfl][$comm_id]['facility_name'] = $value['facility_name'];
                $data_[$mfl][$comm_id]['county'] = $value['county'];
                $data_[$mfl][$comm_id]['county_id'] = $value['county_id'];
                $data_[$mfl][$comm_id]['district'] = $value['district'];
                $data_[$mfl][$comm_id]['district_id'] = $value['district_id'];
                $data_[$mfl][$comm_id]['commodity_id'] = $value['commodity_id'];
                $data_[$mfl][$comm_id]['order_date'] = $value['order_date'];

                $data_[$mfl][$comm_id]['beginning_bal'] = $value['beginning_bal'];
                $data_[$mfl][$comm_id]['order_date'] = $value['order_date'];
                $data_[$mfl][$comm_id]['q_received'] = $value['q_received'];
                $data_[$mfl][$comm_id]['q_requested'] = $value['q_requested'];
                $data_[$mfl][$comm_id]['q_used'] = $value['q_used'];
                $data_[$mfl][$comm_id]['q_expiring'] = $value['q_expiring'];
                $data_[$mfl][$comm_id]['no_of_tests_done'] = $value['no_of_tests_done'];
                $data_[$mfl][$comm_id]['losses'] = $value['losses'];
                $data_[$mfl][$comm_id]['positive_adj'] = $value['positive_adj'];
                $data_[$mfl][$comm_id]['negative_adj'] = $value['negative_adj'];
                $data_[$mfl][$comm_id]['closing_stock'] = $value['closing_stock'];
                $data_[$mfl][$comm_id]['physical_closing_stock'] = $value['physical_closing_stock'];
                    // array_push($data_, $data);
            }

            $data['data_by_commodity'] = $data_;
            // echo "<pre>";print_r($data_);exit;
            foreach ($data_ as $key => $value) {
                // echo "<pre>";print_r($value[4]);exit;
                $mfl = $value[4]['facility_code'];

                $data['results'][$mfl]['facility_code'] = $value[4]['facility_code'];
                $data['results'][$mfl]['facility_name'] = $value[4]['facility_name'];
                $data['results'][$mfl]['county'] = $value[4]['county'];
                // $data['results'][$mfl]['county_id'] = $value[4]['county_id'];
                $data['results'][$mfl]['district'] = $value[4]['district'];
                // $data['results'][$mfl]['district_id'] = $value[4]['district_id'];
                $data['results'][$mfl]['order_date'] = $value[4]['order_date'];

                // $data['results'][$mfl]['screening_order_date'] = $value[4]['order_date'];

                $data['results'][$mfl]['screening_beginning_balance'] = $value[4]['beginning_bal'];
                $data['results'][$mfl]['screening_quantity_received'] = $value[4]['q_received'];
                $data['results'][$mfl]['screening_quantity_used'] = $value[4]['q_used'];
                $data['results'][$mfl]['screening_quantity_expiring'] = $value[4]['q_expiring'];
                $data['results'][$mfl]['screening_quantity_requested'] = $value[4]['q_requested'];
                $data['results'][$mfl]['screening_tests_done'] = $value[4]['no_of_tests_done'];
                $data['results'][$mfl]['screening_losses'] = $value[4]['losses'];
                $data['results'][$mfl]['screening_positive_adjustments'] = $value[4]['positive_adj'];
                $data['results'][$mfl]['screening_negative_adjustments'] = $value[4]['negative_adj'];
                $data['results'][$mfl]['screening_closing_balance'] = $value[4]['closing_stock'];

                // $data['results'][$mfl]['confirmatory_order_date'] = $value[5]['order_date'];

                $data['results'][$mfl]['confirmatory_beginning_balance'] = $value[5]['beginning_bal'];
                $data['results'][$mfl]['confirmatory_quantity_received'] = $value[5]['q_received'];
                $data['results'][$mfl]['confirmatory_quantity_used'] = $value[5]['q_used'];
                $data['results'][$mfl]['confirmatory_quantity_expired'] = $value[5]['q_expiring'];
                $data['results'][$mfl]['confirmatory_quantity_requested'] = $value[4]['q_requested'];
                $data['results'][$mfl]['confirmatory_tests_done'] = $value[5]['no_of_tests_done'];
                $data['results'][$mfl]['confirmatory_losses'] = $value[5]['losses'];
                $data['results'][$mfl]['confirmatory_positive_adjustments'] = $value[5]['positive_adj'];
                $data['results'][$mfl]['confirmatory_negative_adjustments'] = $value[5]['negative_adj'];
                $data['results'][$mfl]['confirmatory_closing_balance'] = $value[5]['closing_stock'];
            }

            $data['titles'][] = 'facility_code';
            $data['titles'][] = 'facility_name';
            $data['titles'][] = 'county';
            // $data['titles'][] = 'county_id';
            $data['titles'][] = 'district';
            // $data['titles'][] = 'district_id';

            // $data['results'][$mfl]['screening_order_date'] = $value[4]['order_date'];

            $data['titles'][] = 'screening_beginning_balance';
            $data['titles'][] = 'screening_quantity_received';
            $data['titles'][] = 'screening_quantity_used';
            $data['titles'][] = 'screening_quantity_expiring';
            $data['titles'][] = 'screening_quantity_requested';
            $data['titles'][] = 'screening_tests_done';
            $data['titles'][] = 'screening_losses';
            $data['titles'][] = 'screening_positive_adjustments';
            $data['titles'][] = 'screening_negative_adjustments';
            $data['titles'][] = 'screening_closing_balance';
            $data['titles'][] = 'confirmatory_beginning_balance';
            $data['titles'][] = 'confirmatory_quantity_received';
            $data['titles'][] = 'confirmatory_quantity_used';
            $data['titles'][] = 'confirmatory_quantity_expiring';
            $data['titles'][] = 'confirmatory_quantity_requested';
            $data['titles'][] = 'confirmatory_tests_done';
            $data['titles'][] = 'confirmatory_losses';
            $data['titles'][] = 'confirmatory_positive_adjustments';
            $data['titles'][] = 'confirmatory_negative_adjustments';
            $data['titles'][] = 'confirmatory_closing_balance';

            $data['columns'][] = 'beginning_bal';
            $data['columns'][] = 'q_received';
            $data['columns'][] = 'q_used';
            $data['columns'][] = 'q_expiring';
            $data['columns'][] = 'q_requested';
            $data['columns'][] = 'no_of_tests_done';
            $data['columns'][] = 'losses';
            $data['columns'][] = 'positive_adj';
            $data['columns'][] = 'negative_adj';
            
            // $data['columns'][] = 'physical_closing_stock';

            $data['columns'][] = 'closing_stock';
            

            // echo "<pre>";print_r($data);exit;
            // $data['results'] = $results;
            
        }else{
            $data['results'] = NULL;
        }

        return $data;
    }

    public function get_dhis_facilities()
    {
        $query = "SELECT * FROM dhis_facilities";

        $result = $this->db->query($query)->result_array();

        $data = array();

        foreach ($result as $key => $value) {
            $mfl = $value['facility_code'];
            $data[$mfl]['facility_code'] = $value['facility_code'];    
            $data[$mfl]['dhis_facility_code'] = $value['dhis_facility_code'];    
            $data[$mfl]['facility_name'] = $value['facility_name'];    
        }

        return $data;
    }//END OF get_dhis_facilities FUNCTION

    public function get_dhis_data_elements()
    {
        $query = "SELECT * FROM dhis_data_elements";

        $result = $this->db->query($query)->result_array();

        $data = array();

        foreach ($result as $key => $value) {
            $c_id = $value['commodity_id'];
            $data[$c_id]['commodity_id'] = $value['commodity_id'];    
            $data[$c_id]['dhis_code'] = $value['dhis_code'];    
            $data[$c_id]['display_name'] = $value['display_name'];    
        }

        return $data;
    }//END OF get_dhis_data_elements FUNCTION

    public function get_dhis_commodity_category_combos()
    {
        $query = "SELECT * FROM dhis_commodity_category_combos";

        $result = $this->db->query($query)->result_array();

        $data = array();

        foreach ($result as $key => $value) {
            $rtk_col = $value['rtk_column'];
            $data[$rtk_col]['rtk_column'] = $value['rtk_column'];    
            $data[$rtk_col]['category_option'] = $value['category_option'];    
            $data[$rtk_col]['category_option_dhis_code'] = $value['category_option_dhis_code'];    
            $data[$rtk_col]['category'] = $value['category'];    
            $data[$rtk_col]['category_dhis_code'] = $value['category_dhis_code'];    
        }

        return $data;
    }//END OF get_dhis_commodity_category_combos

    public function get_issues_from_kemsa_api($commodity_id = NULL, $year = NULL,$format = NULL){
        /*DOC: Function inherited from update_issues_from_kemsa_api_v2 in MY_Controller*/
        // echo "<pre>";print_r($format);exit;
        // echo "<pre>";print_r($commodity_id);exit;
        $counties = $this->db->query("SELECT * FROM counties")->result_array();
        $districts = $this->db->query("SELECT * FROM districts")->result_array();
        $output = array();
        // $commodity_id = (isset($commodity_id) && $commodity_id > 0)? $commodity_id :NULL;
        $kemsa_scr_id = 'NL05TES003';//Determine
        $kemsa_conf_id = 'NL05TES069';//First response

        $yr = (isset($year) && $year > 0)? $year:date('Y');
        $year_beg = $yr."-01-01";
        $year_end = $yr."-12-31";

        switch ($commodity_id) {
            case 4:
            $kemsa_current_id = $kemsa_scr_id;
            $pack_size = 100;
            break;

            case 5:
            $kemsa_current_id = $kemsa_conf_id;
            $pack_size = 30;
            break;
            
            default:
            $commodity_id = 4;
            $kemsa_current_id = $kemsa_scr_id;
            $pack_size = 100;
            break;
        }

        // echo "<pre>";print_r($kemsa_current_id);exit;
        $kemsa_api_token = '$2a$06$BciA87SeFAPc4OT1HNLIYO5LxxWBq/2GlMVMT/K8AviAX1OUhrWsa';

        $final_issue_data = $final_issue_data_ = array();

        $start_date = $year . '-' . $month . '-01';
        $end_date = $year . '-' . $month . '-31';

        $data = array(
            'filter[where][product_code]'=>$kemsa_current_id,
            'filter[where][movementdate][between][0]'=>$year_beg,
            'filter[where][movementdate][between][1]'=>$year_end
        );

        // echo "<pre>";print_r($data);exit;
        $api_url = 'https://api.kemsa.co.ke/kem_issuesreports?';
        $url_data = http_build_query($data);
        $api_url = $api_url.$url_data;
        // echo "<pre>";print_r($api_url);exit;
        $curl = curl_init();
        $headr = array();
        $headr[] = 'apitoken: $2a$06$BciA87SeFAPc4OT1HNLIYO5LxxWBq/2GlMVMT/K8AviAX1OUhrWsa';

        curl_setopt_array($curl, array(
          CURLOPT_URL => $api_url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          // CURLOPT_HEADER => true,
          // CURLINFO_HEADER_OUT => true,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => $headr,
          CURLOPT_TIMEOUT => 0
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $information = curl_getinfo($curl);

        curl_close($curl);
        echo "<pre>";print_r($information);
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            echo "<pre>";print_r($response);exit;

            $issue_data = json_decode($response, true);
                // echo "<pre>";print_r($issue_data);exit;
            $issue_data_final_ = $issue_data_final = array();

            foreach ($issue_data as $keyy => $valuee) {
                $f_code = $valuee['mfl_code'];
                if (is_numeric($f_code)) {
                    $f_query = "SELECT * FROM facilities WHERE facility_code = $f_code";
                        // echo "<pre>";print_r($f_query);
                    $dist_data_ = $this->db->query($f_query);
                    // echo "<pre>NUM ROWS: ".$dist_data_->num_rows();
                        // echo "<pre>";print_r($dist_data);exit;
                    if ($dist_data_->num_rows() > 0) {
                        $dist_data = $dist_data_ ->result_array();
                        // echo "<pre>";print_r($dist_data);
                        $f_name = $dist_data[0]['facility_name'];
                        $d_id = $dist_data[0]['district'];

                        $d_query = "SELECT * FROM districts WHERE id = $d_id";
                        // echo "<pre>";print_r($d_query);
                        $c_data_ = $this->db->query($d_query);
                        if ($c_data_->num_rows() > 0){
                            $c_data = $c_data_->result_array();
                            // echo "<pre>";print_r($c_data);exit;
                            $c_id = $c_data[0]['county'];
                            $d_name = $c_data[0]['district'];
                            $com_id = $commodity_id;

                            $c_q = "SELECT * FROM counties WHERE id = $c_id";
                            $c_res = $this->db->query($c_q)->result_array();
                            // echo "<pre>";print_r($c_res);exit;
                            $c_name = $c_res[0]['county'];

                                // $q_issued = $valuee['movementqty'];
                                // $q_requested = $valuee['demandqty'];

                            $q_issued = $valuee['movementqty'] * $pack_size;
                            $q_requested = $valuee['demandqty'] * $pack_size;

                            $v_issued = $valuee['issued_value'];
                            $v_requested = $valuee['demand_value'];
                            $d_note = $valuee['dnote'];
                            $issue_date = $valuee['movementdate'];
                            $issue_date = date('Y-m-d',strtotime($issue_date));
                                // echo "<pre>";print_r($issue_date);exit;

                            
                            $issue_data_final_['county_id'] = $c_id;
                            $issue_data_final_['county'] = $c_name;
                            $issue_data_final_['district_id'] = $d_id;
                            $issue_data_final_['district'] = $d_name;
                            $issue_data_final_['facility_code'] = $f_code;
                            $issue_data_final_['facility_name'] = $f_name;
                            $issue_data_final_['commodity_id'] = $com_id;

                            $issue_data_final_['qty_issued'] = $q_issued;
                            $issue_data_final_['qty_requested'] = $q_requested;
                            $issue_data_final_['value_issued'] = $v_issued;
                            $issue_data_final_['value_requested'] = $v_requested;
                            $issue_data_final_['delivery_note'] = $d_note;
                            $issue_data_final_['issue_date'] = $issue_date;

                            array_push($issue_data_final, $issue_data_final_);
                        }//end of if county data is present
                    }//end of if district data is present
                }//end of if mfl is numeric
            }// end of issue_data foreach
        }//end of if curl success 

        // echo "<pre>";print_r($issue_data_final);exit;
        $data['results'] = $issue_data_final;
        switch ($format) {
                case 'csv':
                    $export = $this->export('csv',$data);
                    break;

                case 'json':
                    // $export = $this->export('json',$data);
                    header("Content-type:application/json");
                    $final = json_encode($issue_data_final,JSON_UNESCAPED_SLASHES);
                    break;

                default:
                    // $export = $this->export('json',$data);
                    $final = $issue_data_final;
                    break;
        }

        return $final;
        
    }//END OF get_issues_from_kemsa_api FUNCTION

/*END OF DATA RETRIEVAL FUNCTIONS*/

/*EXPORT FUNCTIONS*/
    public function export($type = NULL,$data = NULL,$dhis_encoding = NULL)
    {
        // echo "<pre>DATA: ";print_r($type);exit;
        // echo "<pre>DATA: ";print_r($data);exit;
        // echo "<pre>THIS: ";print_r($dhis_encoding);
        if ($dhis_encoding == false) {
            $dhis_encoding = 1;
        }else{
            $dhis_encoding = 0;
        }
        // echo "<pre>THIS ENCODING: ";print_r($dhis_encoding);exit;

        switch ($type) {
            case 'json':
                header("Content-type:application/json");
                echo json_encode($data['results'],JSON_UNESCAPED_SLASHES);
            break;

            case 'csv':
                $final_data = array();
                array_push($final_data, $data['titles']);
                array_push($final_data, $data['results']);

                // echo "<pre>";print_r($final_data);exit;
                $doc = new PHPExcel();
                $doc->setActiveSheetIndex(0);
                ob_end_clean();
                $filename = "rtk_data_".$data['date'].".csv";

                $doc->getActiveSheet()->fromArray($data['titles'], null, 'A1');
                $doc->getActiveSheet()->fromArray($data['results'], null, 'A2');
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="' . $filename . '"');                 //tell browser what's the file name
                // header('Content-Disposition: attachment;filename="Allocation list.xls"');
                header('Cache-Control: max-age=0');

                // echo "<pre>";print_r($doc);exit;
                // Do your stuff here
                $writer = PHPExcel_IOFactory::createWriter($doc, 'CSV');

                $writer->save('php://output');
            break;
            
            case 'dhis_json':
                $dhis_organisation_units = $this->get_dhis_facilities();
                $dhis_data_elements = $this->get_dhis_data_elements();
                $dhis_commodity_category_combos = $this->get_dhis_commodity_category_combos();
                // echo "<pre>";print_r($dhis_commodity_category_combos);exit;
                $report_data = $data['results'];
                $column_data = $data['columns'];
                $report_data_by_commodity = $data['data_by_commodity'];
                // echo "<pre>";print_r($report_data_by_commodity);exit;
                // echo "<pre>";print_r($report_data);exit;
                $final_data = $final_data_ = array();
                $final_data_original = $final_data_original_ = array();

                foreach ($report_data as $key => $value) {
                    //key is mfl
                    // echo "<pre>";print_r($value);exit;
                    $mfl = $key;
                    
                    $dhis_mfl = $dhis_organisation_units[$mfl]['dhis_facility_code'];
                    
                    // echo "<pre>";print_r($final_data_['organizationUnit']);exit;
                    
                    if (isset($dhis_mfl) && $dhis_mfl !='') {                        

                        foreach ($column_data as $keyy => $valuee) {
                            //DOC: Commented out closing_stock in the columns because DHIS happens to have no closing stock column
                            //only physical_closing_stock

                            // echo "<pre>";print_r($valuee);exit;
                            $column = $valuee;
                            // echo "<pre>";print_r($dhis_commodity_category_combos);exit;
                            $final_data_['dataElement'] = $dhis_data_elements[4]['dhis_code'];
                            $final_data_original_['dataElement'] = 'Screening';

                            $final_data_['period'] = $data['date'];
                            $final_data_original_['period'] = $data['date'];

                            $final_data_['organizationUnit'] = $dhis_organisation_units[$mfl]['dhis_facility_code'];
                            $final_data_original_['organizationUnit'] = $dhis_organisation_units[$mfl]['facility_code'];

                            $final_data_['categoryOptionCombo'] = $dhis_commodity_category_combos[$column]['category_option_dhis_code'];
                            $final_data_original_['categoryOptionCombo'] = $dhis_commodity_category_combos[$column]['category_option'];

                            $final_data_['attributeOptionCombo'] = '';
                            $final_data_original_['attributeOptionCombo'] = '';

                            $final_data_['value'] = $report_data_by_commodity[$mfl][4][$column];
                            $final_data_original_['value'] = $report_data_by_commodity[$mfl][4][$column];

                            /*echo "<pre>";print_r($final_data_);
                            echo "<pre>";print_r($final_data_original_);*/
                            array_push($final_data, $final_data_);
                            array_push($final_data_original, $final_data_original_);

                            // echo "<pre>";print_r($final_data_);exit;
                        }

                        foreach ($column_data as $keyy => $valuee) {
                            //DOC: Commented out closing_stock in the columns because DHIS happens to have no closing stock column
                            //only physical_closing_stock

                            // echo "<pre>";print_r($valuee);exit;
                            $column = $valuee;
                            // echo "<pre>";print_r($dhis_commodity_category_combos);exit;
                            $final_data_['dataElement'] = $dhis_data_elements[5]['dhis_code'];
                            $final_data_original_['dataElement'] = 'Confirmatory';

                            $final_data_['period'] = $data['date'];
                            $final_data_original_['period'] = $data['date'];

                            $final_data_['organizationUnit'] = $dhis_organisation_units[$mfl]['dhis_facility_code'];
                            $final_data_original_['organizationUnit'] = $dhis_organisation_units[$mfl]['facility_code'];

                            $final_data_['categoryOptionCombo'] = $dhis_commodity_category_combos[$column]['category_option_dhis_code'];
                            $final_data_original_['categoryOptionCombo'] = $dhis_commodity_category_combos[$column]['category_option'];
                            
                            $final_data_['attributeOptionCombo'] = '';
                            $final_data_original_['attributeOptionCombo'] = '';

                            $final_data_['value'] = $report_data_by_commodity[$mfl][5][$column];
                            $final_data_original_['value'] = $report_data_by_commodity[$mfl][5][$column];

                            /*echo "<pre>";print_r($final_data_);
                            echo "<pre>";print_r($final_data_original_);*/
                            array_push($final_data, $final_data_);
                            array_push($final_data_original, $final_data_original_);

                            // echo "<pre>";print_r($final_data_);exit;
                        }
                    }
                    // echo "<pre>";print_r($final_data);
                    // echo "<pre>";print_r($final_data_original);exit;
                }

                header("Content-type:application/json");
                    // echo "<pre>FALSE: ";print_r($dhis_encoding);exit;

                if ($dhis_encoding > 0) {
                    // echo "<pre>TRUE: ";print_r($dhis_encoding);exit;
                    echo json_encode($final_data);
                }else{
                    // echo "<pre>FALSE: ";print_r($dhis_encoding);exit;
                    echo json_encode($final_data_original);
                }

            break;

            case 'dhis_csv':
                $dhis_organisation_units = $this->get_dhis_facilities();
                $dhis_data_elements = $this->get_dhis_data_elements();
                $dhis_commodity_category_combos = $this->get_dhis_commodity_category_combos();
                // echo "<pre>";print_r($dhis_commodity_category_combos);exit;
                $report_data = $data['results'];
                $column_data = $data['columns'];
                $report_data_by_commodity = $data['data_by_commodity'];
                // echo "<pre>";print_r($report_data_by_commodity);exit;
                // echo "<pre>";print_r($report_data);exit;
                $final_data = $final_data_ = array();
                $final_data_original = $final_data_original_ = array();

                foreach ($report_data as $key => $value) {
                    //key is mfl
                    // echo "<pre>";print_r($value);exit;
                    $mfl = $key;

                    $dhis_mfl = $dhis_organisation_units[$mfl]['dhis_facility_code'];
                    
                    // echo "<pre>";print_r($final_data_['organizationUnit']);exit;
                    
                    if (isset($dhis_mfl) && $dhis_mfl !='') {
                        

                        foreach ($column_data as $keyy => $valuee) {
                            //DOC: Commented out closing_stock in the columns because DHIS happens to have no closing stock column
                            //only physical_closing_stock

                            // echo "<pre>";print_r($valuee);exit;
                            $column = $valuee;
                            // echo "<pre>";print_r($dhis_commodity_category_combos);exit;
                            $final_data_['dataElement'] = $dhis_data_elements[4]['dhis_code'];
                            $final_data_original_['dataElement'] = 'Screening';

                            $final_data_['period'] = $data['date'];
                            $final_data_original_['period'] = $data['date'];

                            $final_data_['organizationUnit'] = $dhis_organisation_units[$mfl]['dhis_facility_code'];
                            $final_data_original_['organizationUnit'] = $dhis_organisation_units[$mfl]['facility_code'];

                            $final_data_['categoryOptionCombo'] = $dhis_commodity_category_combos[$column]['category_option_dhis_code'];
                            $final_data_original_['categoryOptionCombo'] = $dhis_commodity_category_combos[$column]['category_option'];

                            $final_data_['attributeOptionCombo'] = '';
                            $final_data_original_['attributeOptionCombo'] = '';

                            $final_data_['value'] = $report_data_by_commodity[$mfl][4][$column];
                            $final_data_original_['value'] = $report_data_by_commodity[$mfl][4][$column];

                            /*echo "<pre>";print_r($final_data_);
                            echo "<pre>";print_r($final_data_original_);*/
                            array_push($final_data, $final_data_);
                            array_push($final_data_original, $final_data_original_);

                            // echo "<pre>";print_r($final_data_);exit;
                        }

                        foreach ($column_data as $keyy => $valuee) {
                            //DOC: Commented out closing_stock in the columns because DHIS happens to have no closing stock column
                            //only physical_closing_stock

                            // echo "<pre>";print_r($valuee);exit;
                            $column = $valuee;
                            // echo "<pre>";print_r($dhis_commodity_category_combos);exit;
                            $final_data_['dataElement'] = $dhis_data_elements[5]['dhis_code'];
                            $final_data_original_['dataElement'] = 'Confirmatory';

                            $final_data_['period'] = $data['date'];
                            $final_data_original_['period'] = $data['date'];

                            $final_data_['organizationUnit'] = $dhis_organisation_units[$mfl]['dhis_facility_code'];
                            $final_data_original_['organizationUnit'] = $dhis_organisation_units[$mfl]['facility_code'];

                            $final_data_['categoryOptionCombo'] = $dhis_commodity_category_combos[$column]['category_option_dhis_code'];
                            $final_data_original_['categoryOptionCombo'] = $dhis_commodity_category_combos[$column]['category_option'];
                            
                            $final_data_['attributeOptionCombo'] = '';
                            $final_data_original_['attributeOptionCombo'] = '';

                            $final_data_['value'] = $report_data_by_commodity[$mfl][5][$column];
                            $final_data_original_['value'] = $report_data_by_commodity[$mfl][5][$column];

                            /*echo "<pre>";print_r($final_data_);
                            echo "<pre>";print_r($final_data_original_);*/
                            array_push($final_data, $final_data_);
                            array_push($final_data_original, $final_data_original_);

                            // echo "<pre>";print_r($final_data_);exit;
                        }
                    }

                    // echo "<pre>";print_r($dhis_organisation_units[$mfl]);exit;
                    //SCREENING
                    // echo "<pre>";print_r($dhis_data_elements[4]);exit;

                    // exit;

                    // echo "<pre>";print_r($final_data);
                    // echo "<pre>";print_r($final_data_original);exit;
                }

                if ($dhis_encoding > 0) {
                    // echo "<pre>TRUE: ";print_r($dhis_encoding);exit;
                    $csv_data = $final_data;
                }else{
                    // echo "<pre>FALSE: ";print_r($dhis_encoding);exit;
                    $csv_data = $final_data_original;
                }

                // echo "<pre>";print_r($final_data);exit;
                $doc = new PHPExcel();
                $doc->setActiveSheetIndex(0);
                ob_end_clean();
                $filename = "dhis_data_".$data['date'].".csv";

                // $doc->getActiveSheet()->fromArray($data['titles'], null, 'A1');
                // $a_1 = array('dataelement', 'period', 'orgunit', 'categoryOptionCombo', 'attributeOptionCombo', 'value');
                $a_1 = array('dataelement', 'period', 'orgunit', 'catoptcombo', 'attroptcombo', 'value');

                $doc->getActiveSheet()->fromArray($a_1, null, 'A1');
                $doc->getActiveSheet()->fromArray($csv_data, null, 'A2');

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="' . $filename . '"');                 //tell browser what's the file name
                // header('Content-Disposition: attachment;filename="Allocation list.xls"');
                header('Cache-Control: max-age=0');

                // echo "<pre>";print_r($doc);exit;
                // Do your stuff here
                $writer = PHPExcel_IOFactory::createWriter($doc, 'CSV');

                $writer->save('php://output');;
            break;

            default:
                header("Content-type:application/json");
                echo json_encode($data);
                break;
        }
    }//END OF export FUNCTION
/*END OF EXPORT FUNCTIONS*/

/*START OF TOKEN FUNCTIONS*/
    public function generate_api_token()
    {
        //build the headers
        $headers = ['alg'=>'HS256','typ'=>'JWT'];
        $headers_encoded = base64_encode(json_encode($headers));

        //build the payload
        // $payload = ['sub'=>'allocations_api','name'=>'RTK', 'admin'=>true];
        $payload = ['sub'=>'allocations_api','name'=>'RTK'];
        $payload_encoded = base64_encode(json_encode($payload));

        //build the signature
        $key = 'RTKsecretkey$#@';
        $signature = hash_hmac('SHA256',"$headers_encoded.$payload_encoded",$key,true);
        $signature_encoded = base64_encode($signature);

        //build and return the token
        $token = "$headers_encoded.$payload_encoded.$signature_encoded";
        // echo $token;exit;
        echo json_encode("Hi. You are not authorized to retrieve this information.");
    }

    public function verify_api_token($api_token)
    {
        $token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhbGxvY2F0aW9uc19hcGkiLCJuYW1lIjoiUlRLIn0=.pSScZF/vcEJTuq6rNj9JO2MkAe2AtF/qMZxZ1oWbmGM=';

        $recievedJwt = $api_token;

        $secret_key = 'RTKsecretkey$#@';

        // Split a string by '.' 
        $jwt_values = explode('.', $recievedJwt);

        // extracting the signature from the original JWT 
        $recieved_signature = $jwt_values[2];

        // concatenating the first two arguments of the $jwt_values array, representing the header and the payload
        $recievedHeaderAndPayload = $jwt_values[0] . '.' . $jwt_values[1];

        // creating the Base 64 encoded new signature generated by applying the HMAC method to the concatenated header and payload values
        $resultedsignature = base64_encode(hash_hmac('sha256', $recievedHeaderAndPayload, $secret_key, true));

        // checking if the created signature is equal to the received signature
        if($resultedsignature == $recieved_signature) {
            $status = "success";
        }else{
            $status = "fail";
        }

        return $status;
    }
/*END OF TOKEN FUNCTIONS*/

/*ADT FUNCTIONS*/
    public function get_adt_dhis_data($fcdrr = NULL,$county_id = NULL,$district_id = NULL,$facility_code = NULL,$commodity_id = NULL,$quarter = NULL,$year = NULL,$month = NULL)
    {
        // echo "<pre>";print_r($district_id);exit;
        $param_string = $fcdrr.$county_id.$district_id.$facility_code.$commodity_id.$quarter.$year.$month;
        $dhis_data = array();

        $dhis_parameters = $this->get_adt_dhis_parameters($fcdrr);
        // echo "<pre>";print_r($dhis_parameters);exit;
        $dx_str = $dhis_parameters['dx'];
        $co_str = $dhis_parameters['co'];

        $dx = explode(";", $dx_str);
        $co = explode(";", $co_str);
        // echo "<pre>";print_r($co);exit;

        $dimensions['dx'] = $dx;
        $dimensions['co'] = $co;
        $dimensions['pe'][] = 'LAST_12_MONTHS';

        // echo "<pre>";print_r($dimensions);exit;
        // $org_units_q = "SELECT * FROM dhis_facilities;";
        $o_u_where = "";
        $o_u_where .= ($district_id > 0)? " AND d.id = $district_id":NULL;
        $o_u_where .= ($county_id > 0)? " AND c.id = $county_id":NULL;

        $org_units_q = "
            SELECT
                df.*,
                f.district AS district_id,
                d.district,
                d.county AS county_id,
                c.county
            FROM
                dhis_facilities df,
                adt_facilities af,
                districts d,
                counties c,
                facilities f
            WHERE
                df.facility_code = f.facility_code
                    AND df.facility_code = af.facility_code
                    AND f.district = d.id
                    AND d.county = c.id $o_u_where;
        ";
        // echo "<pre>";print_r($org_units_q);exit;
        $all_org_units = $this->db->query($org_units_q)->result_array(); 
        // echo "<pre>";print_r($all_org_units);exit;

        $count = 0;
        foreach ($all_org_units as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $org_units[] = $value['dhis_facility_code'];
            
            $count++;
        }

        // echo "<pre>";print_r($dimensions);exit;
        $dimensions_json = json_encode($dimensions);
        // echo "<pre>";print_r($dimensions_json);exit;

        // $org_units = array_chunk($org_units, 500);
        $org_units = array_chunk($org_units, 200);

        // echo "<pre>";print_r($org_units);exit;
        // echo "<pre>";print_r($param_string);exit;
        $cache_key = "pull_adt_dhis_fcdrr_data_final_data".$dimensions_json.$param_string.date('Y-m-d');
        $cache_check = $this->memcached_check($cache_key);
        // echo "<pre>";print_r($cache_check);exit;
        if ($cache_check['status'] == "FAILED") {
            /*INNER CACHE CHECK*/
            $cache_key_ = "pull_adt_dhis_data_".$dimensions_json.$param_string.date('Y-m-d');
            // echo "<pre>";print_r($cache_key_);exit;
            $cache_check_ = $this->memcached_check($cache_key_);
            // echo "<pre>";print_r($cache_check);exit;
            if ($cache_check_['status'] == "FAILED") {
                foreach ($org_units as $key => $value) {
                    $dimensions['ou'] = $value;
                    // echo "<pre>";print_r($dimensions);exit;
                    $dhis_data_ = $this->pull_adt_dhis_data_($dimensions);
                    // $dhis_data_ = $this->pull_adt_dhis_data_oauth_($dimensions);
                    // echo "<pre>";print_r($dhis_data_);exit;
                    // echo "<pre>";print_r(count($dhis_data_));exit;
                    if (count($dhis_data_) > 0) {
                        array_push($dhis_data, $dhis_data_);
                    }
                    // echo "<pre>";print_r($dhis_data);exit;
                    sleep(5);
                }
                // echo "<pre>";print_r($dhis_data);exit;

                $cache_data_['key'] = md5($cache_key_);
                $cache_data_['data'] = json_encode($dhis_data);

                if (count($dhis_data) > 0) {
                    // array_push($dhis_data, $dhis_data_);
                    // $cache_save_ = $this->memcached_save($cache_data_,1800);
                    //CURRENT DEBUG 29052018
                }
                // echo "CACHE SAVED.";exit;
            }else{
                // echo "<pre>";print_r($cache_check_);exit;
                $dhis_data = json_decode($cache_check_['data'],true);
            }
            /*END OF INNER CACHE CHECK*/
            // echo "<pre>dhis_data: ";print_r($dhis_data);exit;
            $final_array = $this->format_adt_dhis_data($dhis_data);

            // echo "<pre>final_array: ";print_r($final_array);exit;
            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($final_array);

            if (count($dhis_data) > 0) {
                $cache_save = $this->memcached_save($cache_data,1800);
            }
            // echo "<pre>Save: ";print_r($cache_save);exit;
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $final_array = json_decode($cache_check['data'],TRUE);
        }

        // echo "<pre>get_adt_dhis_data_: ";print_r($final_array);exit;

        $data['results'] = $final_array;
        // $data['final_array'] = $final_array;

        $export = $this->export('json',$data);
        // echo json_encode($data);
    }

    public function pull_adt_dhis_data_oauth_($dimensions = NULL)
    {
        // echo "<pre>pull_adt_dhis_data_: ";print_r($dimensions);exit;
        $ou = implode(";", $dimensions['ou']);
        $pe = implode(";", $dimensions['pe']);
        $dx = implode(";", $dimensions['dx']);
        $co = implode(";", $dimensions['co']);
        // echo "<pre>";print_r($dx);exit;
        $data = array();
        $data = array(
            'dx'=>$dx,
            'ou'=>$ou,
            'pe'=>$pe,
            'co'=>$co,
        );

        // echo "<pre>";print_r($data);exit;
        $url_data = '';

        foreach ($data as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $data_ = array(
            'dimension'=>$key.":".$value.';&',
            );
            // $d = 'dimension:'.$key.":".$value.';&';

            $url_data .= http_build_query($data_);
            // $url_data .= $d;
        }
        // echo "<pre>";print_r($url_data);exit;
        // $api_url = DHIS_ANALYTICS_API_URL;
        $api_url = DHIS_ANALYTICS_TEST_API_URL;
        // $url_data = urldecode($url_data);
        // echo "<pre>";print_r($url_data);exit;
        $api_url = $api_url.$url_data;
        $api_url = urldecode($api_url);
        echo "<pre>";print_r($api_url);exit;
        $curl = curl_init();
        $username = 'tngugi';
        $password = 'Ngugi1234';

        /*OAUTH2 IMPLEMENTATION*/

            $client_id = 'vl_eid';
            $client_secret = 'c79772344-efd9-da7b-b930-b2d931e7e87';
            $code = $_REQUEST['code'];

            $params = array(
              "client_id" => "vl_eid",
              "client_secret" => "c79772344-efd9-da7b-b930-b2d931e7e87",
              "grant_type" => "authorization_code");

            foreach($params as $k => $v)
            {
               $postData .= $k . '='.urlencode($v).'&';
            }

            $postData = rtrim($postData, '&');

        // echo "<pre>";print_r($postData);exit;
        /*END OF OAUTH2 IMPLEMENTATION*/

        $headr = array();
        // $header[] = "Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
        // $headr[] = 'apitoken: $2a$06$BciA87SeFAPc4OT1HNLIYO5LxxWBq/2GlMVMT/K8AviAX1OUhrWsa';

        curl_setopt_array($curl, array(
          CURLOPT_URL => $api_url,
          CURLOPT_RETURNTRANSFER => TRUE,
          // CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_0,
          CURLOPT_USERPWD, "$username" .":"."$password",
          // CURLOPT_VERBOSE => 1,
          // CURLOPT_HEADER => true,
          CURLINFO_HEADER_OUT => true,
          CURLOPT_CUSTOMREQUEST => "GET",
          // CURLOPT_FOLLOWLOCATION => TRUE,
          CURLOPT_POSTFIELDS => array(
                                        'code' => $code,
                                        'client_id' => $client_id,
                                        'client_secret' => $client_secret,
                                        'grant_type' => 'authorization_code'
                                    ),
          CURLOPT_HTTPHEADER => $headr
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $information = curl_getinfo($curl);

        curl_close($curl);
        echo "<pre>";print_r($information);    
        echo "<pre>";print_r($response);exit;  
        $dhis_data = NULL;  
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            if ($information['http_code'] == '200') {
                $dhis_data = json_decode($response, TRUE);
            }else if ($information['http_code'] == '502') {
                $error_data = array(
                        'message' => "DHIS HAS RETURNED A 502 ERROR. THE API IS UNAVAILABLE. KINDLY TRY AGAIN LATER.", 
                        'http_code' => "http_code: ".$information['http_code']
                    );

                    // $echo = $this->echo_to_console($error_data);
                    // echo "<pre>";print_r($error_data);
                    $output = json_encode($output,TRUE);
                    // echo $output;exit;
            }else{
                if (empty($response)) {
                    // echo "<pre>";print_r("FOLLOW THE LINK BELOW AND LOGIN TO DHIS TO CONTINUE.");
                    // echo "<pre>";print_r("<a href=".$api_url." target='blank_'>".$api_url."</a>");echo "</pre>";

                    // echo "<pre>";print_r($information['http_code']);
                    // echo "<pre>information: ";print_r($information);
                    // echo "<pre>response: ";print_r($response);

                    $error_data = array(
                        'status' => "AUTHENTICATION ERROR.", 
                        'message' => "FOLLOW THE LINK BELOW AND LOGIN TO DHIS TO CONTINUE. THIS MESSAGE WILL ONLY BE DISPLAYED WHEN VIEWED ON A BROWSER.", 
                        'http_code' => "http_code: ".$information['http_code'], 
                        'information' => $information, 
                        'response' => $response, 
                        'link' => "$api_url",
                    );

                    // $echo = $this->echo_to_console($error_data);
                    // echo "<pre>";print_r($error_data);
                    $output = json_encode($error_data,TRUE);
                    // echo $output;
                }
                
                // echo "<pre>";print_r("<a href=".$api_url." target='blank_'>".$api_url."</a>");echo "</pre>";
                // exit;
                // echo "<script>console.log(".$api_url.");</script>";
                // echo "<pre>response empty check: ";print_r(empty($response));
                // echo "<pre>response: ";print_r($response);
                // echo "<pre>";print_r($information);
            }

            // echo "<pre>";print_r($dhis_data);exit;
        }
        // echo "<pre>";print_r($dhis_data);exit;
        return $dhis_data;
    }

    public function pull_adt_dhis_data_($dimensions = NULL)
    {
        // echo "<pre>pull_adt_dhis_data_: ";print_r($dimensions);exit;
        $ou = implode(";", $dimensions['ou']);
        $pe = implode(";", $dimensions['pe']);
        $dx = implode(";", $dimensions['dx']);
        $co = implode(";", $dimensions['co']);
        // echo "<pre>";print_r($dx);exit;
        $data = array();
        $data = array(
            'dx'=>$dx,
            'ou'=>$ou,
            'pe'=>$pe,
            'co'=>$co,
        );

        // echo "<pre>";print_r($data);exit;
        $url_data = '';

        foreach ($data as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $data_ = array(
            'dimension'=>$key.":".$value.';&',
            );
            // $d = 'dimension:'.$key.":".$value.';&';

            $url_data .= http_build_query($data_);
            // $url_data .= $d;
        }
        // echo "<pre>";print_r($url_data);exit;
        $api_url = DHIS_ANALYTICS_API_URL;
        // $api_url = DHIS_ANALYTICS_TEST_API_URL;
        // $url_data = urldecode($url_data);
        // echo "<pre>";print_r($url_data);exit;
        $api_url = $api_url.$url_data;
        $api_url = urldecode($api_url);
        // echo "<pre>";print_r($api_url);exit;
        $curl = curl_init();
        $username = 'tngugi';
        $password = 'Ngugi1234';

        $headr = array();
        // $header[] = "Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
        // $headr[] = 'apitoken: $2a$06$BciA87SeFAPc4OT1HNLIYO5LxxWBq/2GlMVMT/K8AviAX1OUhrWsa';

        curl_setopt_array($curl, array(
          CURLOPT_URL => $api_url,
          CURLOPT_RETURNTRANSFER => TRUE,
          // CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_0,
          CURLOPT_USERPWD, "$username" .":"."$password",
          // CURLOPT_VERBOSE => 1,
          // CURLOPT_HEADER => true,
          CURLINFO_HEADER_OUT => true,
          CURLOPT_CUSTOMREQUEST => "GET",
          // CURLOPT_FOLLOWLOCATION => TRUE,
          CURLOPT_HTTPHEADER => $headr
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $information = curl_getinfo($curl);

        curl_close($curl);
        // echo "<pre>";print_r($information);    
        // echo "<pre>";print_r($response);  
        $dhis_data = NULL;  
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            if ($information['http_code'] == '200') {
                $dhis_data = json_decode($response, TRUE);
            }else if ($information['http_code'] == '502') {
                $error_data = array(
                        'message' => "DHIS HAS RETURNED A 502 ERROR. THE API IS UNAVAILABLE. KINDLY TRY AGAIN LATER.", 
                        'http_code' => "http_code: ".$information['http_code']
                    );

                    // $echo = $this->echo_to_console($error_data);
                    // echo "<pre>";print_r($error_data);
                    $output = json_encode($output,TRUE);
                    // echo $output;exit;
            }else{
                if (empty($response)) {
                    // echo "<pre>";print_r("FOLLOW THE LINK BELOW AND LOGIN TO DHIS TO CONTINUE.");
                    // echo "<pre>";print_r("<a href=".$api_url." target='blank_'>".$api_url."</a>");echo "</pre>";

                    // echo "<pre>";print_r($information['http_code']);
                    // echo "<pre>information: ";print_r($information);
                    // echo "<pre>response: ";print_r($response);

                    $error_data = array(
                        'status' => "AUTHENTICATION ERROR.", 
                        'message' => "FOLLOW THE LINK BELOW AND LOGIN TO DHIS TO CONTINUE. THIS MESSAGE WILL ONLY BE DISPLAYED WHEN VIEWED ON A BROWSER.", 
                        'http_code' => "http_code: ".$information['http_code'], 
                        'information' => $information, 
                        'response' => $response, 
                        'link' => "$api_url",
                    );

                    // $echo = $this->echo_to_console($error_data);
                    // echo "<pre>";print_r($error_data);
                    $output = json_encode($error_data,TRUE);
                    // echo $output;
                }
                
                // echo "<pre>";print_r("<a href=".$api_url." target='blank_'>".$api_url."</a>");echo "</pre>";
                // exit;
                // echo "<script>console.log(".$api_url.");</script>";
                // echo "<pre>response empty check: ";print_r(empty($response));
                // echo "<pre>response: ";print_r($response);
                // echo "<pre>";print_r($information);
            }

            // echo "<pre>";print_r($dhis_data);exit;
        }
        // echo "<pre>";print_r($dhis_data);exit;
        return $dhis_data;
    }

    public function format_adt_dhis_data($dhis_data = NULL)
    {
        // echo "<pre>";print_r($dhis_data);exit;
        $metadata['headers'] = $dhis_data[0]['headers'];
        $metadata['items'] = $dhis_data[0]['metaData']['items'];
        $metadata['dx'] = $dhis_data[0]['metaData']['dimensions']['dx'];
        $metadata['pe'] = $dhis_data[0]['metaData']['dimensions']['pe'];
        $metadata['co'] = $dhis_data[0]['metaData']['dimensions']['co'];

        // echo "<pre>";print_r($metadata);exit;
        $cat_opts = array();
        $cat_opt_arr = $org_unit_arr = $data_elem_arr = array();

        $counter = 0;
        foreach ($metadata['co'] as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $cat_opts[$counter]['code'] = $value;
            $cat_opts[$counter]['category_option'] = $metadata['items'][$value]['name'];

            $counter++;
        }

        foreach ($metadata['dx'] as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $data_elem_arr[$value]['dhis_code'] = $value;
        }

        foreach ($cat_opts as $key => $value) {
            $code = $value['code'];
            $cat_opt_arr[$code]['dhis_code'] = $code;
            $cat_opt_arr[$code]['category_option'] = $value['category_option'];
            // $cat_opt_arr[$code]['db_column'] = $value['db_column'];
        }

        // echo "<pre>";print_r($cat_opt_arr);exit;
        $org_units_q = "
            SELECT 
                df.*,
                c.id AS county_id,
                c.county,
                d.id AS district_id,
                d.district
            FROM
                dhis_facilities df,
                facilities f,
                districts d,
                counties c
            WHERE
                f.facility_code = df.facility_code
                    AND f.district = d.id
                    AND d.county = c.id;";
        $all_org_units = $this->db->query($org_units_q)->result_array(); 
        // echo "<pre>";print_r($all_org_units);exit;

        foreach ($all_org_units as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $code = $value['dhis_facility_code'];
            $org_unit_arr[$code]['dhis_code'] = $code;
            $org_unit_arr[$code]['county_id'] = $value['county_id'];
            $org_unit_arr[$code]['county'] = $value['county'];
            $org_unit_arr[$code]['district_id'] = $value['district_id'];
            $org_unit_arr[$code]['district'] = $value['district'];
            $org_unit_arr[$code]['facility_code'] = $value['facility_code'];
            $org_unit_arr[$code]['facility_name'] = $value['facility_name'];
        }

        // echo "<pre>";print_r($org_unit_arr);exit;

        $data_elem_q = "SELECT * FROM adt_dhis_elements;";
        $all_data_elem = $this->db->query($data_elem_q)->result_array(); 
        // echo "<pre>";print_r($all_data_elem);exit;

        foreach ($all_data_elem as $key => $value) {
            $code = $value['dhis_code'];
            $data_elem_arr[$code]['dhis_code'] = $code;
            $data_elem_arr[$code]['commodity_id'] = $value['target_id'];
        }

        // $data_elem_arr[0]['dhis_code'] = '';
        // $data_elem_arr[0]['commodity_id'] = '0';

        // echo "<pre>";print_r($data_elem_arr);exit;
        $dimensions = $metadata['dx'];
        // echo "<pre>";print_r($metadata['items']);exit;
        // echo "<pre>";print_r($dimensions);exit;
        $row_data = $meta_commodities = array();

        // echo "<pre>";print_r($dhis_data);exit;
        foreach ($dhis_data as $keys => $values) {
            // echo "<pre>";print_r($value);exit;
            $rows = $values['rows'];
            // echo "<pre>";print_r($rows);exit;
            foreach ($rows as $key => $value) {
                // echo "<pre>";print_r($value);exit;
                $comm_dhis_code = $value[0];
                $cat_opt_dhis_code = $value[1];
                $org_unit_dhis_code = $value[2];
                $date = $value[3];
                $val = $value[4];

                $month = substr($date, -2);
                $year = substr($date, 0, 4);

                // echo "<pre>";print_r($org_unit_arr);exit;
                // $o_d = $org_unit_dhis_code.'_'.$date;
                // $db_cat = $cat_opt_arr[$cat_opt_dhis_code]['db_column'];
                // echo "<pre>";print_r($db_cat);exit;
                // $db_cat = $cat_opt_arr[$cat_opt_dhis_code]['db_column'];
                $db_cat_long = $cat_opt_arr[$cat_opt_dhis_code]['category_option'];

                $mfl = $org_unit_arr[$org_unit_dhis_code]['facility_code'];
                $comm_name = $metadata['items'][$comm_dhis_code]['name'];
                // echo "<pre>";print_r($comm_name);exit;
                // echo "<pre>";print_r($metadata['items']);exit;
                // $comm_name = 'UNDEFINED';
                // echo "<pre>";print_r($comm_name);exit;
                $o_d = $mfl.'_'.$date;
                $o_d = md5($o_d);

                /*$comm_meta = array(
                        'commodity_name' => $comm_name, 
                        'commodity_dhis_code' => $comm_dhis_code,
                );*/

                $comm_adt_id = $data_elem_arr[$comm_dhis_code]['commodity_id'];
                $comm_meta[$comm_dhis_code]['commodity_name'] = $comm_name;
                $comm_meta[$comm_dhis_code]['commodity_adt_data'] = $data_elem_arr[$comm_dhis_code];
                $comm_meta[$comm_dhis_code]['commodity_dhis_code'] = $comm_dhis_code;
                // echo "<pre>";print_r($o_d_md5);exit;

                $row_data[$comm_adt_id][$o_d]['county_id'] = $org_unit_arr[$org_unit_dhis_code]['county_id'];
                $row_data[$comm_adt_id][$o_d]['county'] = $org_unit_arr[$org_unit_dhis_code]['county'];
                $row_data[$comm_adt_id][$o_d]['district_id'] = $org_unit_arr[$org_unit_dhis_code]['district_id'];
                $row_data[$comm_adt_id][$o_d]['district'] = $org_unit_arr[$org_unit_dhis_code]['district'];
                $row_data[$comm_adt_id][$o_d]['facility_name'] = $org_unit_arr[$org_unit_dhis_code]['facility_name'];
                $row_data[$comm_adt_id][$o_d]['facility_code'] = $mfl;
                $row_data[$comm_adt_id][$o_d]['dhis_code'] = $org_unit_dhis_code;
                $row_data[$comm_adt_id][$o_d]['value'] = $val;
                $row_data[$comm_adt_id][$o_d]['description'] = $db_cat_long;
                $row_data[$comm_adt_id][$o_d]['dhis_category_code'] = $cat_opt_dhis_code;
                $row_data[$comm_adt_id][$o_d]['month'] = $month;
                $row_data[$comm_adt_id][$o_d]['year'] = $year;

                array_push($meta_commodities, $comm_meta);
            }

        }

        // echo "<pre>";print_r($meta_commodities);exit;
        $meta = array(
            'commodities' => $meta_commodities,
            'category_options' => $cat_opts 
        );

        $data['metadata'] = $meta;
        $data['result'] = $row_data;
        // echo "<pre>";print_r($data);exit;
        return $data;    
    }

    public function get_adt_dhis_parameters($fcdrr=NULL)
    {
        switch ($fcdrr) {
            case 'MOH730A':
                $fcdrr = 'MOH730A';
                $dx_str = 'T8vIpv842eX;v70T2bzhYBH;kMTppRz9Dy6;OzHa7Wm9wWb;HKNzvgOEXdV;zX3RWIpPsU5;anJTqZAje6a;Ptvb1N5gQ54;ZcZwhTRfWkK;g2J7NpW2ZM8;onTjG9ODh1k;BDKBvFhIQJf;bZplX60YbPf;wxA11VC6wRx;U3PLl3ULQt8;piEFmBh6Uql;zbrSNo8bSST;UMqyIpzJRCJ;ugsQqSyyZy2;wFFhejJhg1B;LI1E9Xrvpwr;fWTJUSnntn6;HkZ39x8BCEK;siZ3FslqDHV;ymWk2uW4f1s;y8vOQhrlRLj;iKalfg97rtZ;r9fSbflgQgC;VJ9VJ6dxwVD;OnFFuii2sYi;JrfpoCr2vII;rNmSAnjCmWK;AUsGmP0j5eC;wWKgpcMkHnJ;pCPSqHPI4iN;kwhxIWspmVw;VfUQzXlpKQl;xUayEnX8Blt;NWL4ULZezMy;LK4JkwJDx2c;aYXQcIen2Wl;u66LdVsKCvV;ijmmJw97V6g;eb9fVoSbFIr;c4V5Fav5ovZ;in4iMk8Fjj1';

                $co_str = 'wckQxxQK4hY;zb988DEsTWI;Wo1PowlV2Nu;JGrvMIR9dNW;hC4WAv2wKep;QDGwAPfi7tu;c7ZwRxfFYyF;z80g7okL9TR;ESHbSUOcBfD;IYWzA8BiqBg';
            break;
            
            default:
                $fcdrr = 'MOH730BA';
                $dx_str = 'NQzz85r5JPD;ID6Aso4IXcZ;pZctFqRLi8W;F1W9V1jfIwf;q8sjZIlFs4J;RFWuUKeuciq;MNVBv62ijRX;MZgAjnphgsg;TW2swzvuk6K;gFNHAyxeLks;owvoPNua6Q1;jV9MuYZNb6l;vLBcMXH3ITC;VGti1UehUwX;wAkXhyTK5ug;EX8fzwmyfI7;I6KeyHpHPaI;Ao4QopHHGEk;LVNETa4jV2u;R3cZo5wqEJO;ARFq4oY3heY;PHm90uko7eq;CIl81MC1CxL;VgX1W96jMow;xOuDDwYj1Yw;jclt56IrD0J;LbpQwKhUFgf;KUVy8JTJHma;Iif81jdgAwp;bfehwWGgiWJ;lduCnun9ZJP;DLtximMysSs;MD5fubfNOMI;PQBxN4inyLo;aSaopRmPuaz;AZZqUosplNX;EBquyc7eibs;QjNUILIPIGL;uM70qGs87WM;AYlM9Gs9D6i;iObZlGvkJDy;AwEvXiS8TXC;nQlkXyft9pb;K4wC1yrizQD;cHlP8txIIr4;cv9SwH7B09P;CM4fRYs6wGn;PWd1NaYM55x;yYUBDfyp10Y;CQY0y7k4IQy;IEURUdOXwmV;wYblxwrkbww;Tn9w6C5ilIr';
                $co_str = 'uAB0YkTsscj;Vj6Cd9zuhti;d35XXJY6tC7;efzGRLKe3nk;CkQipmILsjN;AeoqJgAPkCJ;tKqMQQC3FSf;XCVq6ebb2vV;P4CduckLeJq;mSqguIpueMp;I7lrRhQUS9L';
            break;
        }

        $data['fcdrr'] = $fcdrr;
        $data['dx'] = $dx_str;
        $data['co'] = $co_str;
        
        return $data;
    }

/*END OF ADT FUNCTIONS*/

/*AUTOMATED SCRIPTS TO UPDATE EXISTING DATA IN DB*/
    public function update_kemsa_issue_quarters()
    {
        $query = "SELECT * FROM kemsa_issue_emails";
        $all = $this->db->query($query)->result_array();

        $final_data = array();

        foreach ($all as $all_key => $all_value) {
            $id = $all_value['id'];

            // echo "<pre>";print_r($all_value);exit;
            $issue_date = $all_value['issue_date'];

            $y = date('Y',strtotime($issue_date));
            $m = date('m',strtotime($issue_date));

            $q_data = $this->get_year_quarter($m,NULL,$y);
            // echo "<pre>";print_r($issue_date);
            // echo "<pre>";print_r($q_data);exit;
            $q = $q_data['quarter'];

            $data = array(
               'quarter' => $q,
               'year' => $y,
               'month' => $m,
            );

            $where = $this->db->where('id', $id);
            // echo "<pre>";print_r($where);exit;
            $update = $this->db->update('kemsa_issue_emails', $data);

            $data_['update'] = $update;
            $data_['data'] = $data;
            $data_['quarter_data'] = $q_data;
            $data_['value'] = $all_value;

            array_push($final_data, $data_);
            // echo "<pre>";print_r($final_data);exit;
        }

        echo "<pre>";print_r($final_data);exit;
        return $final_data;
    }//end of update_kemsa_issue_quarters()
/*END OF AUTOMATED SCRIPTS TO UPDATE EXISTING DATA IN DB*/
} //END OF API CLASS
?>
