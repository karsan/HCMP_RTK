<?php
/*
* @author Karsan
*/
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
class Dashboardv2 extends MY_Controller {
	function __construct() {
		parent::__construct();
		$this -> load -> helper(array('form', 'url', 'file'));
		// $this -> load -> library(array('form_validation','PHPExcel/PHPExcel'));
		$this -> load -> library(array('hcmp_functions', 'form_validation'));
		$this->load->model("dashboard_model");
		$cache = $this->db->cache_on();
		require_once(APPPATH.'controllers/api.php');
		// $clear = $this->clear_cache();
		// $page_caching = $this->output->cache(CACHE_REFRESH_INTERVAL);

        // error_reporting(E_ALL);
		
		/*error_reporting(1);
        error_reporting(E_ERROR | E_PARSE);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);*/
        
	}

	public function index($county_id = NULL, $district_id = NULL,$quarter = NULL,$month = NULL,$year = NULL) {
		$page_caching = $this->output->cache(CACHE_REFRESH_INTERVAL_LONG);
		$default = "tracer";//FYI
		// echo "<pre>";print_r($quarter);exit;
		// $map = $this->render_map();
		$county_id = (isset($county_id) && $county_id > 0)? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id > 0)? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code > 0)? $facility_code:NULL;
		$quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;

		$data['filter_years'] = $this->get_years_to_date();

		if (isset($month)) {
            // $year = substr($month, -4);
            $year = date('Y');
            $month = substr($month, 0, 2);
            $monthyear = $year . '-' . $month . '-01';

        } else {
            // $month = $this->session->userdata('Month');
            if ($month == '') {
                // $month = date('mY', time());
            }
            $year = date('Y');
            // $month = substr_replace($month, "", -4);
            $monthyear = $year . '-' . $month . '-01';
        }
    	// echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;

        $current_date = date('Y-m-d');

        $append_date = (isset($append_date) && $append_date !="")? $append_date : date('Y-m-d');
		
		$county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;

		$commodity_count = Dashboard_model::get_commodity_count();
		// $commodities = Dashboard_model::get_division_commodities($division);
		$page_title = "RTK Dashboard.";
		$data['page_title'] = $page_title;
		// echo "<pre>";print_r($commodities);exit;
		$commodity_divisions = Dashboard_model::get_division_details();
		$counties = $this->db->query("SELECT * FROM counties")->result_array();
		$districts = $this->db->query("SELECT * FROM districts")->result_array();

		$facility_count = Dashboard_model::get_online_offline_facility_count();
		$counties_using_CD4 = Counties::get_counties_all_using_CD4();

		$graph_type = $graph_data = $category_data = $series_data = array();
		$category_data[] = "Screening";
		$category_data[] = "Confirmatory";
		$screening = array("10","20");
		// echo "<pre>";print_r($screening);exit;
		$series_data['Screening'] = [10];
		$series_data['Confirmatory'] = [20];
		// $data['filter_months'] = $this->get_months_between_dates();

		$filtered_name = 	NULL;
		$district_data = (isset($district_id) && ($district_id > 0)) ? districts::get_district_name($district_id) -> toArray() : NULL;
		// echo "<pre>";print_r($district_data);exit;

		$county_search = counties::get_county_name($county_id);
		if (count($county_search) > 0) {
			// $filtered_name = $county_search['county']." County $append_date";
			$filtered_name = $county_search['county']." County";
		}
		
		if ($district_data != NULL) {
			// $filtered_name = $district_data[0]['district']." Sub-County $append_date";
			$filtered_name = $district_data[0]['district']." Sub-County";
		}

		// echo "<pre>";print_r($county_name);exit;
		
        $kemsa_issues = $this->get_kemsa_issue_data(NULL,NULL,NULL,NULL,$quarter,$year,$month,"commodity");
		
        // echo "<pre>";print_r($kemsa_issues);exit;
		
		$kemsa_issues_count = count($kemsa_issues);
		if ($kemsa_issues_count > 0) {
			if ($year > 0 && $month == 0 && $quarter == 0) {
				// echo "year";exit;
				$data['kemsa_issued_data_date'] = "Year: ".date('Y',strtotime($kemsa_issues[4]['issue_date']));
			}elseif(isset($month_year) && $month_year !='' && $month_year !=0){
				// echo "month_year";exit;
				$data['kemsa_issued_data_date'] = date('F Y',strtotime($filtered_date));
			}elseif(isset($quarter) && $quarter > 0){
				// echo "quarter";exit;
				$data['kemsa_issued_data_date'] = "Quarter: ".$quarter;
			}else{
				// echo "else";exit;
				$data['kemsa_issued_data_date'] = date('F Y',strtotime($kemsa_issues[4]['issue_date']));
			}
			$data['kemsa_issued_screening'] = $kemsa_issues[4]['qty_issued'];
			$data['kemsa_issued_confirmatory'] = $kemsa_issues[5]['qty_issued'];
		}else{
			if ($year > 0 && $month == 0 && $quarter == 0) {
				// echo "year";exit;
				$data['kemsa_issued_data_date'] = "Year: ".date('Y');
			}elseif(isset($month_year) && $month_year !='' && $month_year !=0){
				// echo "month_year";exit;
				$data['kemsa_issued_data_date'] = date('F Y');
			}elseif(isset($quarter) && $quarter > 0){
				// echo "quarter";exit;
				$data['kemsa_issued_data_date'] = "Quarter: ".$quarter;
			}else{
				// echo "else";exit;
				$data['kemsa_issued_data_date'] = date('F Y');
			}
			$data['kemsa_issued_screening'] = $kemsa_issues[4]['qty_issued'];
			$data['kemsa_issued_confirmatory'] = $kemsa_issues[5]['qty_issued'];
		}

		$county_name = "";

		$graph_type = 'bar';

		$graph_title = (isset($filtered_name) || ($filtered_name !=''))? "Drawing rights for $filtered_name" :NULL;

		$graph_data = array_merge($graph_data, array("graph_id" => 'drawing_rights_graph'));
		$graph_data = array_merge($graph_data, array("graph_title" => $graph_title));
		$graph_data = array_merge($graph_data, array("graph_type" => $graph_type));
		$graph_data = array_merge($graph_data, array("graph_yaxis_title" => "Stock levels"));
		$graph_data = array_merge($graph_data, array("graph_categories" => $category_data));
		$graph_data = array_merge($graph_data, array("series_data" => array('total' => $series_data)));

		// $data['drawing_rights_graph'] = $this -> hcmp_functions -> create_high_chart_graph($graph_data);

		// echo "<pre>";print_r($data['high_graph']);exit;
		$dhis_data = $this->get_dhis_data($county_id,$district_id,$facility_code,$quarter,$month,$year);//KARSAN
		// echo "<pre>";print_r($dhis_data);exit;

		$data['dhis_screening_tests_done'] = $dhis_data[4]['tests_done'];
		$data['dhis_confirmatory_tests_done'] = $dhis_data[5]['tests_done'];

		$national_aggregates = $this->national_aggregate($county_id,$district_id,$facility_code,$quarter,$month,$year);//KARSAN
		// echo "<pre>AGGR: ";print_r($national_aggregates);exit;

		$data['screening_tests_done'] = $national_aggregates['final_tests4'];
		$data['confirmatory_tests_done'] = $national_aggregates['final_tests5'];

		$data['screening_closing_balance'] = $national_aggregates['final_closing_bal4'];
		$data['confirmatory_closing_balance'] = $national_aggregates['final_closing_bal5'];

		$data['screening_requested'] = $national_aggregates['final_requested4'];
		$data['confirmatory_requested'] = $national_aggregates['final_requested5'];

		$data['screening_requested'] = $national_aggregates['final_requested4'];
		$data['confirmatory_requested'] = $national_aggregates['final_requested5'];

		$data['screening_days_out'] = $national_aggregates['final_days4'];
		$data['confirmatory_days_out'] = $national_aggregates['final_days5'];

		$data['screening_expiring'] = $national_aggregates['final_expiring4'];
		$data['confirmatory_expiring'] = $national_aggregates['final_expiring5'];

		$summary_and = "";
        $summary_and .= (isset($county_id) && $county_id!='')? " AND county_id = $county_id" :NULL; 
        
        // $c_drawing_query = "SELECT * FROM county_drawing_rights $summary_and";
        $c_drawing_query = "SELECT c.county, cd.* FROM county_drawing_rights cd,counties c WHERE c.id = cd.county_id; $summary_and";

		// echo $c_drawing_query;exit;
        $county_drawing_data = $this->db->query($c_drawing_query)->result_array();
        // echo "<pre>";print_r($county_drawing_data);exit;
        $data['county_drawing_data'] = $county_drawing_data;
        /*if ($quarter > 0) {
        	get_county_allocation_data
        }else{
        	$c_drawing_query = "SELECT * FROM county_drawing_rights $summary_and";
			// echo $c_drawing_query;exit;
        	$county_drawing_data = $this->db->query($c_drawing_query)->result_array();
        }*/

        // echo "<pre>";print_r($county_drawing_data);exit;
    	$drawing_total_confirmatory = $drawing_total_screening = 0;
    	$drawing_used_confirmatory = $drawing_used_screening = 0;
    	$drawing_balance_confirmatory = $drawing_balance_screening = 0;
    	$county_scr_distributed_aggr = $county_conf_distributed_aggr = 0;
    	$county_scr_total_drawing_rights = $county_conf_total_drawing_rights = 0;

        $current = date('m');
        $curMonth = $current;
        $curQuarter = ceil($curMonth/3);//KARSAN

        if ($quarter > 0) {
        	$curQuarter = $quarter;
        	
        }

        $data['cur_quarter'] = $curQuarter;
        // echo "<pre>";print_r($curQuarter);exit;

        // echo "<pre>";print_r($county_drawing_data);exit;
        foreach ($county_drawing_data as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
        	// $drawing_rights_data_['quarter'] = $curQuarter;
        	$screening_total = $confirmatory_total = 0;
        	$screening_total = $value['screening_total']*$curQuarter;
        	$confirmatory_total = $value['confirmatory_total']*$curQuarter;

        	$screening_used = $value['screening_used'];
        	$confirmatory_used = $value['confirmatory_used'];

        	$screening_distributed = $value['screening_distributed'];
        	$confirmatory_distributed = $value['confirmatory_distributed'];

        	// echo "<pre>";print_r($value['screening_used']);exit;
        	$screening_balance = $screening_total - $value['screening_distributed'];
        	$confirmatory_balance = $confirmatory_total - $value['confirmatory_distributed'];

			$screening_balance = (isset($screening_balance) && $screening_balance > 0)? $screening_balance:0;
			$confirmatory_balance = (isset($confirmatory_balance) && $confirmatory_balance > 0)? $confirmatory_balance:0;

			if (count($county_drawing_data) > 0) {
				$county_scr_total_drawing_rights += $screening_total;
				$county_conf_total_drawing_rights += $confirmatory_total;

				$county_scr_distributed_aggr += $screening_distributed;
				$county_conf_distributed_aggr += $confirmatory_distributed;

	        	$data['drawing_total_screening'] += $screening_total;
	        	$data['drawing_total_confirmatory'] += $confirmatory_total;

	        	$data['drawing_used_screening'] += $screening_used;
	        	$data['drawing_used_confirmatory'] += $confirmatory_used;

	        	$data['drawing_distributed_screening'] += $screening_distributed;
	        	$data['drawing_distributed_confirmatory'] += $confirmatory_distributed;
        	}else{
				$county_scr_total_drawing_rights = $screening_total;
				$county_conf_total_drawing_rights = $confirmatory_total;

				$county_scr_distributed_aggr = $screening_distributed;
				$county_conf_distributed_aggr = $confirmatory_distributed;

        		$data['drawing_total_screening'] = $screening_total;
	        	$data['drawing_total_confirmatory'] = $confirmatory_total;

	        	$data['drawing_used_screening'] = $screening_used;
	        	$data['drawing_used_confirmatory'] = $confirmatory_used;

	        	$data['drawing_distributed_screening'] = $screening_distributed;
	        	$data['drawing_distributed_confirmatory'] = $confirmatory_distributed;

        	}

        	// echo "<pre>";print_r($confirmatory_total);exit;
        	}

        if ($county_id > 0) {
	        /*DOC: START OF COUNTY ALLOCATION DATA FROM EXCEL SHEETS*/
	        $c_alloc_data = $this->get_county_allocation_data($county_id, $district_id, $month, $quarter,$year);
	        // echo "<pre>";print_r($c_alloc_data);exit;

	        $data['total_allocated_screening'] = $c_alloc_data[0]['allocate_s'];
	        $data['total_allocated_confirmatory'] = $c_alloc_data[0]['allocate_c'];

	        $county_scr_distributed_aggr = $c_alloc_data[0]['allocate_s'];
			$county_conf_distributed_aggr = $c_alloc_data[0]['allocate_c'];

			$distributed_county["Screening"][] = $county_scr_distributed_aggr;
			$distributed_county["Confirmatory"][] = $county_conf_distributed_aggr;

			$balance_county['Screening'][] = $county_scr_total_drawing_rights - $county_scr_distributed_aggr;
			$balance_county['Confirmatory'][] = $county_conf_total_drawing_rights - $county_conf_distributed_aggr;
	        /*DOC: END OF COUNTY ALLOCATION DATA FROM EXCEL SHEETS*/
        }else{
        	// $data['total_allocated_screening'] = $county_scr_distributed_aggr;
        	// $data['total_allocated_confirmatory'] = $county_conf_distributed_aggr;
        	$drawing_data_details = $this->get_drawing_rights_details($county_id,$district_id,$facility_code,$quarter,$month,$year);

	        // echo "<pre>";print_r($drawing_data_details);exit;
	        $data['national_drawing_totals_year'] = $drawing_data_details['data_year'];
	        $data['national_drawing_totals'] = $drawing_data_details['national_drawing_totals'];
	        $data['county_drawing_totals'] = $drawing_data_details['county_drawing_totals'];
	        $data['county_drawing_data'] = $county_drawing_data;
	        $data['county_drawing_data_details'] = $drawing_data_details;
	        $data['county_drawing_totals_details'] = $drawing_data_details['county_drawing_totals_details'];
	        $data['county_drawing_data_year'] = $year;

	        $c_alloc_data = $this->get_county_allocation_data($county_id, $district_id, $month, $quarter,$year);

	        $data['total_allocated_screening'] = $c_alloc_data[0]['allocate_s'];
	        $data['total_allocated_confirmatory'] = $c_alloc_data[0]['allocate_c'];

	        $county_scr_distributed_aggr = $c_alloc_data[0]['allocate_s'];
			$county_conf_distributed_aggr = $c_alloc_data[0]['allocate_c'];

			$distributed_county["Screening"][] = $county_scr_distributed_aggr;
			$distributed_county["Confirmatory"][] = $county_conf_distributed_aggr;

			$balance_county['Screening'][] = $county_scr_total_drawing_rights - $county_scr_distributed_aggr;
			$balance_county['Confirmatory'][] = $county_conf_total_drawing_rights - $county_conf_distributed_aggr;
	        
        	// echo "<pre>";print_r($c_alloc_data);exit;
        }


        $data['drawing_balance_screening'] = $county_scr_total_drawing_rights - $county_scr_distributed_aggr;
        $data['drawing_balance_confirmatory'] = $county_conf_total_drawing_rights - $county_conf_distributed_aggr;



		// echo "<pre>";print_r($data);exit;
        $summary_data_s = $this->get_county_order_data($county_id, $district_id, $facility_code, 4 , $quarter,$month,$year);
        // echo "<pre>";print_r($summary_data_s);exit;
        $summary_data_c = $this->get_county_order_data($county_id, $district_id, $facility_code, 5 , $quarter,$month,$year);

        // $kemsa_issued_screening = $kemsa_issues['kemsa_issue_data'][0]['screening_units'];
        // $kemsa_issued_confirmatory = $kemsa_issues['kemsa_issue_data'][0]['confirmatory_units'];

        $kemsa_issued_screening = $kemsa_issues[4]['qty_issued'];
        $kemsa_issued_confirmatory = $kemsa_issues[5]['qty_issued'];

        $received_site_scr = $summary_data_s['results'][0]['q_received'];
        $received_site_conf = $summary_data_c['results'][0]['q_received'];

        $data['received_site_screening'] = $received_site_scr;
        $data['received_site_confirmatory'] = $received_site_conf;
        $data['summary_data_date'] = $summary_data_s['date_text'];
        // echo "<pre>";print_r($data);exit;
        $consumed_site_scr = $summary_data_s['results'][0]['q_used'];
        $consumed_site_conf = $summary_data_c['results'][0]['q_used'];

        $data['consumed_site_screening'] = $consumed_site_scr;
        $data['consumed_site_confirmatory'] = $consumed_site_conf;

        $balances_scr = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 4 , $beg_date, $end_date, $quarter,NULL,$month,$year);
        $balances_conf = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 5 , $beg_date, $end_date, $quarter,NULL,$month,$year);
        // echo "<pre>";print_r($balances_scr);exit;
        // $closing_bal_scr = $summary_data_s['results'][0]['beginning_bal'];
        // $closing_bal_conf = $summary_data_c['results'][0]['beginning_bal'];

        $closing_bal_scr = $balances_scr['closing_balance']['closing_balance'];
        $closing_bal_conf = $balances_conf['closing_balance']['closing_balance'];
        $closing_bal_order_date = $balances_scr['closing_balance']['order_date'];

        $beginning_bal_scr = $balances_scr['beginning_balance']['beginning_balance'];
        $beginning_bal_conf = $balances_conf['beginning_balance']['beginning_balance'];
        $beginning_bal_order_date = $balances_scr['beginning_balance']['order_date'];

        $data['beginning_balance_screening'] = $beginning_bal_scr;
        $data['beginning_balance_confirmatory'] = $beginning_bal_conf;
        $data['beginning_balance_order_date'] = $beginning_bal_order_date;

        $data['closing_balance_screening'] = $closing_bal_scr;
        $data['closing_balance_confirmatory'] = $closing_bal_conf;
        $data['closing_balance_order_date'] = $closing_bal_order_date;

        // echo "<pre>";print_r($c_alloc_data);exit;
  		//$county_scr_closing_balance = $national_aggregates['final_closing_bal4'];
		//$county_conf_closing_balance = $national_aggregates['final_closing_bal5'];

		$county_scr_closing_balance = $closing_bal_scr;
		$county_conf_closing_balance = $closing_bal_conf;

		// echo "<pre>";print_r($county_scr_total_drawing_rights);exit;

		$used_series_scr = $used_series_conf = $allocated_series_scr = $allocated_series_conf = array();
		$received_series_scr = $received_series_conf = $used_category_scr = $used_category_conf = array();
		$received_category_scr = $received_category_conf = array();
		$total_rights_series_scr = $total_rights_series_conf = array();
		$closing_bal_series_scr = $closing_bal_series_conf = array();
		$kemsa_issued_series_scr = $kemsa_issued_series_conf = array();

		$total_rights_series_scr = array_merge($total_rights_series_scr, array("Total" => (int)$county_scr_total_drawing_rights));
		$total_rights_series_conf = array_merge($total_rights_series_conf, array("Total" => (int)$county_conf_total_drawing_rights));

		$kemsa_issued_series_scr = array_merge($kemsa_issued_series_scr, array("Issued from KEMSA" => (int)$kemsa_issued_screening));
		$kemsa_issued_series_conf = array_merge($kemsa_issued_series_conf, array("Issued from KEMSA" => (int)$kemsa_issued_confirmatory));

		$closing_bal_series_scr = array_merge($closing_bal_series_scr, array("Closing Balance" => (int)$county_scr_closing_balance));
		$closing_bal_series_conf = array_merge($closing_bal_series_conf, array("Closing Balance" => (int)$county_conf_closing_balance));

		$allocated_series_scr = array_merge($allocated_series_scr, array("Allocated" => (int)$county_scr_distributed_aggr));
		$allocated_series_conf = array_merge($allocated_series_conf, array("Allocated" => (int)$county_conf_distributed_aggr));

		$used_series_scr = array_merge($used_series_scr, array("Used" => (int)$consumed_site_scr));
		$used_series_conf = array_merge($used_series_scr, array("Used" => (int)$consumed_site_conf));

		$received_series_scr = array_merge($received_series_scr, array("Received" => (int)$received_site_scr));
		$received_series_conf = array_merge($received_series_conf, array("Received" => (int)$received_site_conf));

		// echo "<pre>";print_r($total_rights_series_scr);exit;
		// $utilization_category = array_merge($used_category, array("Screening","Confirmatory"));
		$utilization_category_scr[] = "Screening";
		$utilization_category_conf[] = "Confirmatory";
		$utilization_category_imploded_scr = "'" . implode ( "', '", $utilization_category_scr) . "'";
		$utilization_category_imploded_conf = "'" . implode ( "', '", $utilization_category_conf) . "'";

		$graph_stuff = array();

		// $beginning_imploded = "'" . implode ( "', '", $beginning_final_['Screening'] ) . "'";
		// $used_imploded = "'" . implode ( "', '", $used_final_['Screening'] ) . "'";
		$counties_imploded_c = "'" . implode ( "', '", $gcategories_final_c) . "'";

		$closing_bal_series_scr =  implode ( "', '", $closing_bal_series_scr);
		$total_rights_series_scr =  implode ( "', '", $total_rights_series_scr);
		$kemsa_issued_series_scr =  implode ( "', '", $kemsa_issued_series_scr);
		$allocated_series_scr = implode ( "', '", $allocated_series_scr);
		$used_series_scr = implode ( "', '", $used_series_scr);
		$received_series_scr = implode ( "', '", $received_series_scr);
		// echo "<pre>";print_r($closing_bal_series_scr);exit;

		// echo "<pre>";print_r($utilization_category_imploded_scr);exit;
		$graph_stuff['categories'] = $utilization_category_imploded_scr;

		// $graph_stuff['series_data']['Closing Balance'] = $closing_bal_series_scr;
		$graph_stuff['series_data']['Issued from KEMSA'] = $kemsa_issued_series_scr;
		// $graph_stuff['series_data']['Total'] = $total_rights_series_scr;
		$graph_stuff['series_data']['Allocated'] = $allocated_series_scr;
		// $graph_stuff['series_data']['Used'] = $used_series_scr;
		$graph_stuff['series_data']['Received'] = $received_series_scr;

		$graph_stuff['div'] = "utilization_graph_scr";
		// echo "<pre>";print_r($graph_stuff);exit;

		$utilization_graph_scr_custom = $this->allocationvsreceivedvsconsumed_highchart($graph_stuff);//Karsan
		// echo "<pre>";print_r($utilization_graph_scr_custom);exit;

		$data['utilization_graph_scr'] = $utilization_graph_scr_custom;

		$graph_stuff = array();

		// $beginning_imploded = "'" . implode ( "', '", $beginning_final_['Screening'] ) . "'";
		// $used_imploded = "'" . implode ( "', '", $used_final_['Screening'] ) . "'";
		$counties_imploded_c = "'" . implode ( "', '", $gcategories_final_c) . "'";
		$kemsa_issued_series_conf =  implode ( "', '", $kemsa_issued_series_conf);
		$closing_bal_series_conf =  implode ( "', '", $closing_bal_series_conf);
		$total_rights_series_conf =  implode ( "', '", $total_rights_series_conf);
		$allocated_series_conf = implode ( "', '", $allocated_series_conf);
		$used_series_conf = implode ( "', '", $used_series_conf);
		$received_series_conf = implode ( "', '", $received_series_conf);
		// echo "<pre>";print_r($counties_imploded);exit;

		// echo "<pre>";print_r($utilization_category_imploded_conf);exit;
		$graph_stuff['categories'] = $utilization_category_imploded_conf;


		// $graph_stuff['series_data']['Closing_Balance'] = $closing_bal_series_conf;
		$graph_stuff['series_data']['Issued from KEMSA'] = $kemsa_issued_series_conf;
		// $graph_stuff['series_data']['Total'] = $total_rights_series_conf;
		$graph_stuff['series_data']['Allocated'] = $allocated_series_conf;
		// $graph_stuff['series_data']['Used'] = $used_series_conf;
		$graph_stuff['series_data']['Received'] = $received_series_conf;

		$graph_stuff['div'] = "utilization_graph_conf";
		// echo "<pre>";print_r($graph_stuff);exit;
		$utilization_graph_conf_custom = $this->allocationvsreceivedvsconsumed_highchart($graph_stuff);//Karsan
		// echo "<pre>";print_r($utilization_graph_conf_custom);exit;

		$data['utilization_graph_conf'] = $utilization_graph_conf_custom;

		// echo "<pre>";print_r($data['utilization_graph']);exit;
		// $county_aggregates = $this->drawing_rights_data($county_id,$district_id);
		// echo $year." ".$month;exit;
		$county_aggregates = $this->national_stockcard_v2($county_id,$district_id);
        $drawing_aggregates = $this->drawing_rights_data($year, $month, $county_id, $district_id, $facility_code);

		// echo "<pre>";print_r($drawing_aggregates);exit;
		//sum_opening 
		//sum_used

		$beginning_series = $year_total = $distributed = $balance = $used_drawing_rights_series = $beginning_category = $used_drawing_rights_category = array();
		$gcategories_final_ = $gcategories_final_s = $gcategories_final_c = array();

        usort($drawing_aggregates, function($a, $b) {
		    // return $a['screening_balance'] - $b['screening_balance'];
		    return $a['screening_distributed'] - $b['screening_distributed'];
		});



        // echo "<pre>";print_r($drawing_aggregates);exit;

		foreach ($drawing_aggregates as $key => $value) {
				$bal_s = 0;
				$drawing_rights_quarter = $value['quarter'];
				$gcategories_final_s[] = $value['county'];

				$year_total["Screening"][] = $value['screening_year_total'];
				$distributed["Screening"][] = $value['screening_distributed'];
				$bal_s = $value['screening_year_total'] - $value['screening_distributed'];
				$bal_s = (isset($bal_s) && $bal_s > 0)? $bal_s:0;
				$balance["Screening"][] = $value['screening_balance'];
		}

		usort($drawing_aggregates, function($a, $b) {
		    // return $a['confirmatory_balance'] - $b['confirmatory_balance'];
		    return $a['confirmatory_distributed'] - $b['confirmatory_distributed'];
		});

		foreach ($drawing_aggregates as $key => $value) {
				$bal_c = 0;
				$gcategories_final_c[] = $value['county'];

				$year_total["Confirmatory"][] = $value['confirmatory_year_total'];
				$distributed["Confirmatory"][] = $value['confirmatory_distributed'];
				$bal_c = $value['confirmatory_year_total'] - $value['confirmatory_distributed'];
				$bal_c = (isset($bal_c) && $bal_c > 0)? $bal_c:0;
				$balance["Confirmatory"][] = $value['confirmatory_balance'];
		}

		$data['drawing_rights_quarter'] =$drawing_rights_quarter;

		// echo "<pre>";print_r($year_total);
		// echo "<pre>";print_r($distributed);
		// echo "<pre>";print_r($balance);
		// exit;

		$gcategories_final_s = array_unique($gcategories_final_s);
		$gcategories_final_c = array_unique($gcategories_final_c);
		// $utilization_category = array_merge($used_category, array("Screening","Confirmatory"));
		// $utilization_category = array_merge($used_category, $gcategories_final_);

		// echo "<pre>";print_r($gcategories_final_);exit;
		$counties = implode(",",$gcategories_final_s);
			// echo "<pre>";print_r($counties);exit;

		$beginning_series = array_merge($beginning_series, array("Screening beginning" => (int)$national_aggregates['final_closing_bal4']));
		$beginning_series = array_merge($beginning_series, array("Confirmatory beginning" => (int)$national_aggregates['final_closing_bal5']));

		$used_drawing_rights_series = array_merge($used_drawing_rights_series, array("Screening used" => (int)$national_aggregates['final_used4']));
		$used_drawing_rights_series = array_merge($used_drawing_rights_series, array("Confirmatory used" => (int)$national_aggregates['final_used5']));
		
		$year_total_imploded = implode(',', $year_total['Screening']);
		$balance_imploded = implode(',', $balance['Screening']);
		
		$distributed_imploded = implode(',', $distributed['Screening']);
		if ($county_id > 0) {
			$distributed_imploded = implode(',', $distributed_county['Screening']);
			$balance_imploded = implode(',', $balance_county['Screening']);
		}
		// $counties_imploded_s = implode(',', $gcategories_final_s);

		$graph_stuff = array();

		// $beginning_imploded = "'" . implode ( "', '", $beginning_final_['Screening'] ) . "'";
		// $used_imploded = "'" . implode ( "', '", $used_final_['Screening'] ) . "'";
		$counties_imploded_s = "'" . implode ( "', '", $gcategories_final_s) . "'";
		// echo "<pre>";print_r($counties_imploded);exit;

		// echo "<pre>";print_r($beginning_imploded);exit;
		$graph_stuff['categories'] = $counties_imploded_s;

		// $graph_stuff['series_data']['year_total'] = $year_total_imploded;
		$graph_stuff['series_data']['year_total'] = $balance_imploded;
		$graph_stuff['series_data']['distributed'] = $distributed_imploded;
		$graph_stuff['div'] = "utilization_bgg_screening_graph";

		$graph_custom = $this->drawing_rights_stacked_high_chart($graph_stuff);//Karsan
		$data['utilization_bgg_screening_graph'] = $graph_custom;

		$year_total_imploded = implode(',', $year_total['Confirmatory']);
		$balance_imploded = implode(',', $balance['Confirmatory']);
		$distributed_imploded = implode(',', $distributed['Confirmatory']);
		if ($county_id > 0) {
			$distributed_imploded = implode(',', $distributed_county['Confirmatory']);
			$balance_imploded = implode(',', $balance_county['Confirmatory']);
		}

		$counties_imploded = implode(',', $gcategories_final_);

		$graph_stuff = array();

		// $beginning_imploded = "'" . implode ( "', '", $beginning_final_['Screening'] ) . "'";
		// $used_imploded = "'" . implode ( "', '", $used_final_['Screening'] ) . "'";
		$counties_imploded_c = "'" . implode ( "', '", $gcategories_final_c) . "'";
		// echo "<pre>";print_r($counties_imploded);exit;

		// echo "<pre>";print_r($balance_imploded);exit;
		$graph_stuff['categories'] = $counties_imploded_c;

		// $graph_stuff['series_data']['year_total'] = $year_total_imploded;
		$graph_stuff['series_data']['year_total'] = $balance_imploded;
		$graph_stuff['series_data']['distributed'] = $distributed_imploded;
		$graph_stuff['div'] = "utilization_bgg_confirmatory_graph";

		$graph_custom_conf = $this->drawing_rights_stacked_high_chart($graph_stuff);//Karsan

		$data['utilization_bgg_confirmatory_graph'] = $graph_custom_conf;

		// echo "<pre>";print_r($used_imploded_conf);exit;

		// echo "<pre>";print_r($data['utilization_bgg_confirmatory_graph']);exit;

		$graph_type='column';
		$graph_data=array_merge($graph_data,array("graph_id"=>'test-graph'));
		$graph_data = array_merge($graph_data, array("color" => "['#4b0082', '#6AF9C4']"));
		$graph_data=array_merge($graph_data,array("graph_title"=>""));
		$graph_data=array_merge($graph_data,array("stacking"=>"normal"));
		$graph_data=array_merge($graph_data,array("data_labels"=>"white"));
		$graph_data=array_merge($graph_data,array("graph_type"=>$graph_type));
		$graph_data=array_merge($graph_data,array("graph_yaxis_title"=>""));
		$graph_data=array_merge($graph_data,array("graph_categories"=>$gcategories_final_ ));
		$graph_data=array_merge($graph_data,array("series_data"=>$graph_stuff_raw['series_data']));
		$util_conf_graph = $this->hcmp_functions->create_high_chart_graph($graph_data);

		// echo "<pre>";print_r($util_conf_graph);exit;
		$data['test_graph'] = $util_conf_graph;

		$graph_type='column';
		$graph_data=array_merge($graph_data,array("graph_id"=>'utilization_bg_graph'));
		$graph_data = array_merge($graph_data, array("color" => "['#4b0082', '#6AF9C4']"));
		$graph_data=array_merge($graph_data,array("graph_title"=>""));
		$graph_data=array_merge($graph_data,array("stacking"=>"normal"));
		$graph_data=array_merge($graph_data,array("data_labels"=>"white"));
		$graph_data=array_merge($graph_data,array("graph_type"=>$graph_type));
		$graph_data=array_merge($graph_data,array("graph_yaxis_title"=>""));
		$graph_data=array_merge($graph_data,array("graph_categories"=>$counties_imploded ));
		$graph_data=array_merge($graph_data,array("series_data"=>array('Balance'=>$beginning_final_['Screening'],'Used drawing rights'=>$used_final_['Screening'])));
		$data['utilization_bg_graph']= $this->hcmp_functions->create_high_chart_graph($graph_data);

        // echo "<pre>";print_r($data);exit;
        // echo "<pre>";print_r($drawing_balance_confirmatory);exit;
		$data['counties'] = $county_name;

		// echo "<pre>";print_r($data);exit;
		$counties = $this->db->query("SELECT * FROM counties")->result_array();
		$districts = $this->db->query("SELECT * FROM districts")->result_array();

		$data['tracer'] = $tracer;
		$data['commodity_division'] = isset($division)? $division :"NULL";
		// $title_append = "Sample filtered";

		$title_append = (isset($filtered_name) && $filtered_name !='')? $filtered_name:NULL;

		if ($county_id > 0) {
			$title_append .= " Report";
		}else{
			$title_append .= " as of ".date('F',strtotime("-1 MONTH"))." Report";
		}

		$title_prepend = (isset($county_id) && $county_id > 0)? 'County':'National';

		$title_append .= (isset($quarter) && $quarter > 0)? ' Quarter: '.$quarter:NULL;
		// echo "<pre>";print_r($title_append);exit;
		if ($quarter > 0) {
			$quartered = 1;
			$quarter_append = ' <span class="font-blue-steel">*</span>';
		}
		// $title_prepend = (isset($title_prepend) && $title_prepend !='')? $title_prepend:'National';

		$data['title_append'] = $title_append;
		$data['quartered'] = $quartered;
		$data['quarter_append'] = $quarter_append;
		// $data['tracer_commodities'] = $commodities;
		$data['facility_count'] = $facility_count;
		$data['commodity_count'] = $commodity_count;
		$data['content_view'] = 'dashboard/dashboard';
		$data['county_data'] = $counties;
		$data['title_prepend'] = $title_prepend;
		$data['county_count'] = count($counties_using_CD4);
		$data['district_data'] = $districts;
		$data['commodity_divisions'] = $commodity_divisions;
		$data['title'] = "National Dashboard";
		$data['county_data'] = $counties;
		$data['county_id'] = $county_id;
		$data['subcounty_id'] = $district_id;
		$data['district_data'] = $districts;
		$data['maps'] = $map;
		$data['counties'] = $county_name;
		$this -> load -> view("v2/dashboard/dashboard", $data);
	}

	public function national_dashboard($county_id = NULL, $district_id = NULL,$quarter = NULL,$month_year = NULL,$year = NULL) {
		$page_caching = $this->output->cache(CACHE_REFRESH_INTERVAL);
		$default = "tracer";//FYI
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter." MONTH_YEAR: ".$month_year;exit;
		// echo "<pre>";print_r($year);exit;
		// $map = $this->render_map();
		$county_id = (isset($county_id) && $county_id > 0)? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id > 0)? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code > 0)? $facility_code:NULL;
		$quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;
		$year = (isset($year) && $year > 0)? $year:NULL;
		$month = (isset($month) && $month > 0)? $month:NULL;
		$filtered = 0;
		$title_append = "";
		$data['filter_years'] = $this->get_years_to_date();
		// echo "<pre>";print_r($data);exit;
		if ($quarter > 0 && $month_year < 1) {
			$quartered = 1;
			$quarter_append = ' <span class="font-blue-steel">*</span>';
			$filtered = 1;
		}
		// echo "<pre>";print_r($quarter_append);exit;
		// if ($county_id > 0 || $quarter > 0) {
		// echo "<pre>";print_r($year);exit;
		if ($year > 0) {
			// echo "IF";exit;
			$filtered = 1;
			$quarter_append = ' <span class="font-blue-steel">*</span>';
			$data['filtered_year'] = $year;
			$filtered_year = $year;
			$title_append .= " Year: ".$year;

		}else{
			// echo "ELSE";exit;
			$year = date('Y');
			$filtered_year = NULL;
			$title_append .= " Year: ".date('Y',strtotime($year));
		}
        // echo "<pre>";print_r($title_append);exit;

		// echo "<pre>";print_r($month_year);exit;
		if (isset($month_year) && $month_year !='' && $month_year > 0) {
            // $year = substr($month, -4);
			$m_data = explode('_', $month_year);
			// echo "<pre>";print_r($m_data);exit;
            $month = $m_data[0];
            $year = $m_data[1];
            $monthyear = $year . '-' . $month . '-01';
			$filtered_date = $year."-".$month."-01";
			$filtered = 1;
			$quarter_append = ' <span class="font-blue-steel">*</span>';
			$title_append .= " as of ".date('F Y',strtotime($monthyear))." Report";
			$data['filtered_month'] = $month;
			$data['filtered_month_year'] = $year;
			$data['filtered_year'] = $year;
        } else {
			$filtered_date = "";
            /*$month = $this->session->userdata('Month');
            if ($month == '') {
                $month = date('mY', time());
            }
            $year = date('Y');
            $month = substr_replace($month, "", -4);
            $monthyear = $year . '-' . $month . '-01';*/
        }

        $current = date('m');
        $curMonth = $current;
        $curQuarter = ceil($curMonth/3);//KARSAN
        // echo "<pre>";print_r($month);exit;
        if ($quarter > 0 && $month < 1) {
        	$curQuarter = $quarter;
        	
        }else{
        	$quarter_data = $this->get_year_quarter($month,NULL,$year,NULL);
        	$curQuarter = $quarter_data['quarter'];
        }

        if ($year < date('Y')) {
			$year_quarters = 4;
		}else{
			$year_quarters = $curQuarter;
		}

		$data['filtered'] = $filtered;
		$data['quarter'] = $quarter;
    	// echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;
        // echo "<pre>";print_r($data);exit;
        $current_date = date('Y-m-d');

        $append_date = (isset($append_date) && $append_date !="")? $append_date : date('Y-m-d');
		
		$county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;

		$commodity_count = Dashboard_model::get_commodity_count();
		// $commodities = Dashboard_model::get_division_commodities($division);
		$page_title = "RTK Dashboard.";
		$data['page_title'] = $page_title;
		// echo "<pre>";print_r($commodities);exit;
		$commodity_divisions = Dashboard_model::get_division_details();
		$counties = $this->db->query("SELECT * FROM counties")->result_array();
		$districts = $this->db->query("SELECT * FROM districts")->result_array();

		$facility_count = Dashboard_model::get_online_offline_facility_count();
		$counties_using_CD4 = Counties::get_counties_all_using_CD4();

		$graph_type = $graph_data = $category_data = $series_data = array();
		$category_data[] = "Screening";
		$category_data[] = "Confirmatory";
		$screening = array("10","20");
		// echo "<pre>";print_r($screening);exit;
		$series_data['Screening'] = [10];
		$series_data['Confirmatory'] = [20];
		$data['filter_months'] = $this->get_months_between_dates();

		$filtered_name = 	NULL;
		$district_data = (isset($district_id) && ($district_id > 0)) ? districts::get_district_name($district_id) -> toArray() : NULL;
		// echo "<pre>";print_r($district_data);exit;

		$county_search = counties::get_county_name($county_id);
		if (count($county_search) > 0) {
			// $filtered_name = $county_search['county']." County $append_date";
			$filtered_name = $county_search['county']." County";
		}
		
		if ($district_data != NULL) {
			// $filtered_name = $district_data[0]['district']." Sub-County $append_date";
			$filtered_name = $district_data[0]['district']." Sub-County";
		}

		// echo "<pre>";print_r($month_year);exit;

		// $kemsa_issues = $this->kemsa_issue_data($year, $month, $county_id, $district_id, $facility_code,$quarter);
        // echo "<pre>";print_r($year);exit;
        // echo "<pre>".$quarter." ".$year." ".$month;exit;
        if ($year == 2017) {
        	// $kemsa_issues = $this->get_kemsa_issue_data(NULL,NULL,NULL,NULL,2017,$year,$month,"commodity");
        	$kemsa_issues = $this->get_kemsa_issue_data(NULL,NULL,NULL,NULL,$quarter,$year,$month,"commodity");
        }else{
        	$kemsa_issues = $this->get_kemsa_issue_data(NULL,NULL,NULL,NULL,$quarter,$year,$month,"commodity");
        }
        // echo "<pre>";print_r($kemsa_issues);exit;
		
		$kemsa_issues_count = count($kemsa_issues);
		if ($kemsa_issues_count > 0) {
			if ($year > 0 && $month == 0 && $quarter == 0) {
				// echo "year";exit;
				$data['kemsa_issued_data_date'] = "Year: ".date('Y',strtotime($kemsa_issues[4]['issue_date']));
			}elseif(isset($month_year) && $month_year !='' && $month_year !=0){
				// echo "month_year";exit;
				$data['kemsa_issued_data_date'] = date('F Y',strtotime($filtered_date));
			}elseif(isset($quarter) && $quarter > 0){
				// echo "quarter";exit;
				$data['kemsa_issued_data_date'] = "Quarter: ".$quarter;
			}else{
				// echo "else";exit;
				$data['kemsa_issued_data_date'] = date('F Y',strtotime($kemsa_issues[4]['issue_date']));
			}
			$data['kemsa_issued_screening'] = $kemsa_issues[4]['qty_issued'];
			$data['kemsa_issued_confirmatory'] = $kemsa_issues[5]['qty_issued'];
		}else{
			if ($year > 0 && $month == 0 && $quarter == 0) {
				// echo "year";exit;
				$data['kemsa_issued_data_date'] = "Year: ".date('Y');
			}elseif(isset($month_year) && $month_year !='' && $month_year !=0){
				// echo "month_year";exit;
				$data['kemsa_issued_data_date'] = date('F Y');
			}elseif(isset($quarter) && $quarter > 0){
				// echo "quarter";exit;
				$data['kemsa_issued_data_date'] = "Quarter: ".$quarter;
			}else{
				// echo "else";exit;
				$data['kemsa_issued_data_date'] = date('F Y');
			}
			$data['kemsa_issued_screening'] = $kemsa_issues[4]['qty_issued'];
			$data['kemsa_issued_confirmatory'] = $kemsa_issues[5]['qty_issued'];
		}
		// echo "<pre>";print_r($data);exit;
		if ($filter_year < 1) {
			$filter_year = $year;
		}
		
		// echo "<pre>";print_r($filter_year);exit;
	    /*
	    $summary_data_s = $this->get_order_data($county_id, $district_id, $facility_code, 4, $quarter, "national",$filter_year);
	    $summary_data_c = $this->get_order_data($county_id, $district_id, $facility_code, 5, $quarter, "national",$filter_year);
		$data['national_order_data']['screening'] = $summary_data_s[0];
	    $data['national_order_data']['confirmatory'] = $summary_data_c[0];
	    */

	    $summary_data_s = $this->get_order_data_formatted($county_id, $district_id, $facility_code, 4, $quarter, NULL,$filter_year);
	    // echo "<pre>";print_r($summary_data_s);exit;
	    $summary_data_c = $this->get_order_data_formatted($county_id, $district_id, $facility_code, 5, $quarter, NULL,$filter_year);
	    // echo "<pre>";print_r($summary_data_c);exit;


	    // echo "<pre>";print_r($summary_data_s);exit;
	    //IN PROGRESS
		// $summary_data_c = $this->get_order_data(NULL, $district_id, NULL, 5 , $quarter_,"facility",$year_);

	    $data['national_order_data']['screening'] = $summary_data_s['national_order_data_s'];
	    $data['national_order_data']['confirmatory'] = $summary_data_c['national_order_data_c'];

		$county_name = "";

		$graph_type = 'bar';

		$graph_title = (isset($filtered_name) || ($filtered_name !=''))? "Drawing rights for $filtered_name" :NULL;

		$graph_data = array_merge($graph_data, array("graph_id" => 'drawing_rights_graph'));
		$graph_data = array_merge($graph_data, array("graph_title" => $graph_title));
		$graph_data = array_merge($graph_data, array("graph_type" => $graph_type));
		$graph_data = array_merge($graph_data, array("graph_yaxis_title" => "Stock levels"));
		$graph_data = array_merge($graph_data, array("graph_categories" => $category_data));
		$graph_data = array_merge($graph_data, array("series_data" => array('total' => $series_data)));

		// $data['drawing_rights_graph'] = $this -> hcmp_functions -> create_high_chart_graph($graph_data);

		// echo "<pre>";print_r($data['high_graph']);exit;
		$dhis_data = $this->get_dhis_data($county_id,$district_id,$facility_code,$quarter,$month,$year);//KARSAN
		// echo "<pre>";print_r($dhis_data);exit;

		$data['dhis_screening_tests_done'] = $dhis_data[4]['tests_done'];
		$data['dhis_confirmatory_tests_done'] = $dhis_data[5]['tests_done'];


		$national_aggregates = $this->national_aggregate($county_id,$district_id,$facility_code,$quarter,$month,$year);//KARSAN

		// echo "<pre>AGGR: ";print_r($national_aggregates);exit;

		$data['screening_tests_done'] = $national_aggregates['final_tests4'];
		$data['confirmatory_tests_done'] = $national_aggregates['final_tests5'];

		$data['screening_closing_balance'] = $national_aggregates['final_closing_bal4'];
		$data['confirmatory_closing_balance'] = $national_aggregates['final_closing_bal5'];

		$data['screening_requested'] = $national_aggregates['final_requested4'];
		$data['confirmatory_requested'] = $national_aggregates['final_requested5'];

		$data['screening_requested'] = $national_aggregates['final_requested4'];
		$data['confirmatory_requested'] = $national_aggregates['final_requested5'];

		$data['screening_days_out'] = $national_aggregates['final_days4'];
		$data['confirmatory_days_out'] = $national_aggregates['final_days5'];

		$data['screening_expiring'] = $national_aggregates['final_expiring4'];
		$data['confirmatory_expiring'] = $national_aggregates['final_expiring5'];

		// $summary_and = "";
        // $summary_and .= (isset($county_id) && $county_id!='')? " AND county_id = $county_id" :NULL; 
        
        // $c_drawing_query = "SELECT * FROM county_drawing_rights $summary_and";
        // $c_drawing_query = "SELECT c.county, cd.* FROM county_drawing_rights cd,counties c WHERE c.id = cd.county_id; $summary_and";

        $summary_and = "";
        $summary_and .= (isset($county_id) && $county_id!='')? " WHERE county_id = $county_id" :NULL; 
        // $summary_and .= (isset($quarter) && $quarter>0)? " AND quarter = $quarter" :NULL; 
        
        $c_drawing_query = "SELECT * FROM county_drawing_rights $summary_and";
		// echo $c_drawing_query;exit;
        $county_drawing_data = $this->db->query($c_drawing_query)->result_array();
        
        // echo "<pre>";print_r($year);
        // echo "<pre>";print_r($curQuarter);exit;
        
        /*if ($quarter > 0) {
        	get_county_allocation_data
        }else{
        	$c_drawing_query = "SELECT * FROM county_drawing_rights $summary_and";
			// echo $c_drawing_query;exit;
        	$county_drawing_data = $this->db->query($c_drawing_query)->result_array();
        }*/

        // echo "<pre>";print_r($county_drawing_data);exit;
    	$drawing_total_confirmatory = $drawing_total_screening = 0;
    	$drawing_used_confirmatory = $drawing_used_screening = 0;
    	$drawing_balance_confirmatory = $drawing_balance_screening = 0;
    	$county_scr_distributed_aggr = $county_conf_distributed_aggr = 0;
    	$county_scr_total_drawing_rights = $county_conf_total_drawing_rights = 0;

        $current = date('m');
        $curMonth = $current;
        $curQuarter = ceil($curMonth/3);//KARSAN

        if ($quarter > 0) {
        	$curQuarter = $quarter;
        	
        }

        $data['cur_quarter'] = $curQuarter;
        $data['year_quarters'] = $year_quarters;
        // echo "<pre>";print_r($curQuarter);exit;

        // echo "<pre>";print_r($`);exit;
        $total_screening_used = $total_confirmatory_used = 0;
        $c_drawing_details = array();


        foreach ($county_drawing_data as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
        	// $drawing_rights_data_['quarter'] = $curQuarter;
        	// echo "<pre>";print_r($year_quarters);exit;
        	$screening_total = $confirmatory_total = 0;
        	// $screening_total = $value['screening_total']*$curQuarter;
        	// $confirmatory_total = $value['confirmatory_total']*$curQuarter;

        	$screening_total = $value['screening_total']*$year_quarters;
        	$confirmatory_total = $value['confirmatory_total']*$year_quarters;

        	$screening_total_raw = $value['screening_total'];
	        $confirmatory_total_raw = $value['confirmatory_total'];

        	$screening_used = $value['screening_used'];
        	$confirmatory_used = $value['confirmatory_used'];

        	$screening_distributed = $value['screening_distributed'];
        	$confirmatory_distributed = $value['confirmatory_distributed'];

        	// echo "<pre>";print_r($value['screening_used']);exit;
        	$screening_balance = $screening_total - $value['screening_distributed'];
        	$confirmatory_balance = $confirmatory_total - $value['confirmatory_distributed'];

			$screening_balance = (isset($screening_balance) && $screening_balance > 0)? $screening_balance:0;
			$confirmatory_balance = (isset($confirmatory_balance) && $confirmatory_balance > 0)? $confirmatory_balance:0;

			if (count($county_drawing_data) > 0) {
				// $county_scr_total_drawing_rights += $screening_total;
				// $county_conf_total_drawing_rights += $confirmatory_total;

				// $county_scr_distributed_aggr += $screening_distributed;
				// $county_conf_distributed_aggr += $confirmatory_distributed;

	        	// $data['drawing_total_screening'] += $screening_total;
	        	// $data['drawing_total_confirmatory'] += $confirmatory_total;

	        	$data['drawing_used_screening'] += $screening_used;
	        	$data['drawing_used_confirmatory'] += $confirmatory_used;

	        	// $data['drawing_distributed_screening'] += $screening_distributed;
	        	// $data['drawing_distributed_confirmatory'] += $confirmatory_distributed;
	        	
	        	if (!in_array($screening_total_raw, $qtt_data_arr['scr']['total'])) {
	        		$county_scr_total_drawing_rights += $screening_total;

					$data['drawing_total_screening'] += $screening_total;
					$data['drawing_total_screening_raw'] += $screening_total_raw;
				}else{
					// echo "<pre>";print_r($qtt_data_arr);
					// echo "<pre>";print_r($screening_total);exit;
				}

				if (!in_array($confirmatory_total_raw, $qtt_data_arr['conf']['total'])) {
					$county_conf_total_drawing_rights += $confirmatory_total;
	        		
	        		$data['drawing_total_confirmatory'] += $confirmatory_total;
	        		$data['drawing_total_confirmatory_raw'] += $confirmatory_total_raw;
				}else{
					// echo "<pre>";print_r($confirmatory_distributed);exit;
				}

				if (!in_array($screening_distributed, $qtt_data_arr['scr']['distributed'])) {
					$county_scr_distributed_aggr += $screening_distributed;
					$data['drawing_distributed_screening'] += $screening_distributed;
				}else{
					// echo "<pre>";print_r($screening_distributed);exit;
				}

				if (!in_array($confirmatory_distributed, $qtt_data_arr['conf']['distributed'])) {
					$county_conf_distributed_aggr += $confirmatory_distributed;
		        	$data['drawing_distributed_confirmatory'] += $confirmatory_distributed;
				}else{
					// echo "<pre>";print_r($confirmatory_distributed);exit;
				}

        	}else{
				$county_scr_total_drawing_rights = $screening_total;
				$county_conf_total_drawing_rights = $confirmatory_total;

				$county_scr_distributed_aggr = $screening_distributed;
				$county_conf_distributed_aggr = $confirmatory_distributed;

        		$data['drawing_total_screening'] = $screening_total;
	        	$data['drawing_total_confirmatory'] = $confirmatory_total;

	        	$data['drawing_used_screening'] = $screening_used;
	        	$data['drawing_used_confirmatory'] = $confirmatory_used;

	        	$data['drawing_distributed_screening'] = $screening_distributed;
	        	$data['drawing_distributed_confirmatory'] = $confirmatory_distributed;

        	}

        	// echo "<pre>";print_r($confirmatory_total);exit;
        }

        // echo "<pre>";print_r($data);exit;
        $drawing_data_details = $this->get_drawing_rights_details($county_id,$district_id,$facility_code,$quarter,$month,$year);

        // echo "<pre>drawing_data_details: ";print_r($drawing_data_details);exit;
        $data['national_drawing_totals_year'] = $drawing_data_details['data_year'];
        $data['national_drawing_totals'] = $drawing_data_details['national_drawing_totals'];
        $data['county_drawing_totals'] = $drawing_data_details['county_drawing_totals'];
        $data['county_drawing_data'] = $county_drawing_data;
        $data['county_drawing_data_details'] = $drawing_data_details;
        $data['county_drawing_totals_details'] = $drawing_data_details['county_drawing_totals_details'];
        $data['county_drawing_data_year'] = $year;
        // echo "<pre>";print_r($data);exit;
        if ($county_id > 0) {
	        /*DOC: START OF COUNTY ALLOCATION DATA FROM EXCEL SHEETS*/

	        $c_alloc_data = $this->get_county_allocation_data($county_id, $district_id, $month, $quarter,$year);
	        // echo "<pre>";print_r($c_alloc_data);exit;

	        $data['total_allocated_year'] = $year;

	        $data['total_allocated_screening'] = $c_alloc_data[0]['allocate_s'];
	        $data['total_allocated_confirmatory'] = $c_alloc_data[0]['allocate_c'];

	        $county_scr_distributed_aggr = $c_alloc_data[0]['allocate_s'];
			$county_conf_distributed_aggr = $c_alloc_data[0]['allocate_c'];

			$distributed_county["Screening"][] = $county_scr_distributed_aggr;
			$distributed_county["Confirmatory"][] = $county_conf_distributed_aggr;

			$balance_county['Screening'][] = $county_scr_total_drawing_rights - $county_scr_distributed_aggr;
			$balance_county['Confirmatory'][] = $county_conf_total_drawing_rights - $county_conf_distributed_aggr;
	        /*DOC: END OF COUNTY ALLOCATION DATA FROM EXCEL SHEETS*/
        }else{
        	$data['total_allocated_screening'] = $county_scr_distributed_aggr;
        	$data['total_allocated_confirmatory'] = $county_conf_distributed_aggr;
        }


        $data['drawing_balance_screening'] = $county_scr_total_drawing_rights - $county_scr_distributed_aggr;
        $data['drawing_balance_confirmatory'] = $county_conf_total_drawing_rights - $county_conf_distributed_aggr;

		// echo "<pre>";print_r($data);exit;
        $summary_data_s = $this->get_county_order_data($county_id, $district_id, $facility_code, 4 , $quarter,$month,$year);
        // echo "<pre>";print_r($summary_data_s);exit;
        $summary_data_c = $this->get_county_order_data($county_id, $district_id, $facility_code, 5 , $quarter,$month,$year);

        $kemsa_issued_screening = $kemsa_issues[4]['qty_issued'];
        $kemsa_issued_confirmatory = $kemsa_issues[5]['qty_issued'];

        // echo "<pre>";print_r($kemsa_issued_series_scr);exit;
        $received_site_scr = $summary_data_s['results'][0]['q_received'];
        $received_site_conf = $summary_data_c['results'][0]['q_received'];

        $data['received_site_screening'] = $received_site_scr;
        $data['received_site_confirmatory'] = $received_site_conf;

        $data['positive_adj_screening'] = $summary_data_s['results'][0]['positive_adj'];
        $data['positive_adj_confirmatory'] = $summary_data_c['results'][0]['positive_adj'];

        $data['negative_adj_screening'] = $summary_data_s['results'][0]['negative_adj'];
        $data['negative_adj_confirmatory'] = $summary_data_c['results'][0]['negative_adj'];

        $data['losses_screening'] = $summary_data_s['results'][0]['losses'];
        $data['losses_confirmatory'] = $summary_data_c['results'][0]['losses'];

        $data['quantity_used_screening'] = $summary_data_s['results'][0]['q_used'];
        $data['quantity_used_confirmatory'] = $summary_data_c['results'][0]['q_used'];

        $data['summary_data_date'] = $summary_data_s['date_text'];
        // echo "<pre>";print_r($data);exit;
        $consumed_site_scr = $summary_data_s['results'][0]['q_used'];
        $consumed_site_conf = $summary_data_c['results'][0]['q_used'];

        $data['consumed_site_screening'] = $consumed_site_scr;
        $data['consumed_site_confirmatory'] = $consumed_site_conf;

        $balances_scr = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 4 , $beg_date, $end_date, $quarter,NULL,$month,$year);
        $balances_conf = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 5 , $beg_date, $end_date, $quarter,NULL,$month,$year);
        // echo "<pre>";print_r($balances_scr);exit;
        // $closing_bal_scr = $summary_data_s['results'][0]['beginning_bal'];
        // $closing_bal_conf = $summary_data_c['results'][0]['beginning_bal'];

        $closing_bal_scr = $balances_scr['closing_balance']['closing_balance'];
        $closing_bal_conf = $balances_conf['closing_balance']['closing_balance'];
        $closing_bal_order_date = $balances_scr['closing_balance']['order_date'];

        $beginning_bal_scr = $balances_scr['beginning_balance']['beginning_balance'];
        $beginning_bal_conf = $balances_conf['beginning_balance']['beginning_balance'];
        $beginning_bal_order_date = $balances_scr['beginning_balance']['order_date'];

        $data['beginning_balance_screening'] = $beginning_bal_scr;
        $data['beginning_balance_confirmatory'] = $beginning_bal_conf;
        $data['beginning_balance_order_date'] = $beginning_bal_order_date;

        $data['closing_balance_screening'] = $closing_bal_scr;
        $data['closing_balance_confirmatory'] = $closing_bal_conf;
        $data['closing_balance_order_date'] = $closing_bal_order_date;

        $county_scr_distributed_aggr = $drawing_data_details['national_drawing_totals']['screening_distributed'];
		$county_conf_distributed_aggr = $drawing_data_details['national_drawing_totals']['confirmatory_distributed'];

        // echo "<pre>";print_r($c_alloc_data);exit;
  		//$county_scr_closing_balance = $national_aggregates['final_closing_bal4'];
		//$county_conf_closing_balance = $national_aggregates['final_closing_bal5'];

		$county_scr_closing_balance = $closing_bal_scr;
		$county_conf_closing_balance = $closing_bal_conf;

		// echo "<pre>";print_r($county_scr_total_drawing_rights);exit;

		$used_series_scr = $used_series_conf = $allocated_series_scr = $allocated_series_conf = array();
		$received_series_scr = $received_series_conf = $used_category_scr = $used_category_conf = array();
		$received_category_scr = $received_category_conf = array();
		$total_rights_series_scr = $total_rights_series_conf = array();
		$closing_bal_series_scr = $closing_bal_series_conf = array();
		$kemsa_issued_series_scr = $kemsa_issued_series_conf = array();

		$total_rights_series_scr = array_merge($total_rights_series_scr, array("Total" => (int)$county_scr_total_drawing_rights));
		$total_rights_series_conf = array_merge($total_rights_series_conf, array("Total" => (int)$county_conf_total_drawing_rights));

		$kemsa_issued_series_scr = array_merge($kemsa_issued_series_scr, array("Issued from KEMSA" => (int)$kemsa_issued_screening));
		$kemsa_issued_series_conf = array_merge($kemsa_issued_series_conf, array("Issued from KEMSA" => (int)$kemsa_issued_confirmatory));

		$closing_bal_series_scr = array_merge($closing_bal_series_scr, array("Closing Balance" => (int)$county_scr_closing_balance));
		$closing_bal_series_conf = array_merge($closing_bal_series_conf, array("Closing Balance" => (int)$county_conf_closing_balance));

		$allocated_series_scr = array_merge($allocated_series_scr, array("Allocated" => (int)$county_scr_distributed_aggr));
		$allocated_series_conf = array_merge($allocated_series_conf, array("Allocated" => (int)$county_conf_distributed_aggr));

		$used_series_scr = array_merge($used_series_scr, array("Used" => (int)$consumed_site_scr));
		$used_series_conf = array_merge($used_series_scr, array("Used" => (int)$consumed_site_conf));

		$received_series_scr = array_merge($received_series_scr, array("Received" => (int)$received_site_scr));
		$received_series_conf = array_merge($received_series_conf, array("Received" => (int)$received_site_conf));

		// echo "<pre>";print_r($total_rights_series_scr);exit;
		// $utilization_category = array_merge($used_category, array("Screening","Confirmatory"));
		$utilization_category_scr[] = "Screening";
		$utilization_category_conf[] = "Confirmatory";
		$utilization_category_imploded_scr = "'" . implode ( "', '", $utilization_category_scr) . "'";
		$utilization_category_imploded_conf = "'" . implode ( "', '", $utilization_category_conf) . "'";

		$graph_stuff = array();

		// $beginning_imploded = "'" . implode ( "', '", $beginning_final_['Screening'] ) . "'";
		// $used_imploded = "'" . implode ( "', '", $used_final_['Screening'] ) . "'";
		$counties_imploded_c = "'" . implode ( "', '", $gcategories_final_c) . "'";

		$closing_bal_series_scr =  implode ( "', '", $closing_bal_series_scr);
		$total_rights_series_scr =  implode ( "', '", $total_rights_series_scr);
		$kemsa_issued_series_scr =  implode ( "', '", $kemsa_issued_series_scr);
		$allocated_series_scr = implode ( "', '", $allocated_series_scr);
		$used_series_scr = implode ( "', '", $used_series_scr);
		$received_series_scr = implode ( "', '", $received_series_scr);
		// echo "<pre>";print_r($closing_bal_series_scr);exit;

		// echo "<pre>";print_r($utilization_category_imploded_scr);exit;
		$graph_stuff['categories'] = $utilization_category_imploded_scr;

		// $graph_stuff['series_data']['Closing Balance'] = $closing_bal_series_scr;
		$graph_stuff['series_data']['Issued from KEMSA'] = $kemsa_issued_series_scr;
		// $graph_stuff['series_data']['Total'] = $total_rights_series_scr;
		$graph_stuff['series_data']['Allocated'] = $allocated_series_scr;
		// $graph_stuff['series_data']['Used'] = $used_series_scr;
		$graph_stuff['series_data']['Received'] = $received_series_scr;

		$graph_stuff['div'] = "utilization_graph_scr";
		// echo "<pre>";print_r($graph_stuff);exit;

		$utilization_graph_scr_custom = $this->allocationvsreceivedvsconsumed_highchart($graph_stuff);//Karsan
		// echo "<pre>";print_r($utilization_graph_scr_custom);exit;

		$data['utilization_graph_scr'] = $utilization_graph_scr_custom;

		$graph_stuff = array();

		// $beginning_imploded = "'" . implode ( "', '", $beginning_final_['Screening'] ) . "'";
		// $used_imploded = "'" . implode ( "', '", $used_final_['Screening'] ) . "'";
		$counties_imploded_c = "'" . implode ( "', '", $gcategories_final_c) . "'";
		$kemsa_issued_series_conf =  implode ( "', '", $kemsa_issued_series_conf);
		$closing_bal_series_conf =  implode ( "', '", $closing_bal_series_conf);
		$total_rights_series_conf =  implode ( "', '", $total_rights_series_conf);
		$allocated_series_conf = implode ( "', '", $allocated_series_conf);
		$used_series_conf = implode ( "', '", $used_series_conf);
		$received_series_conf = implode ( "', '", $received_series_conf);
		// echo "<pre>";print_r($counties_imploded);exit;

		// echo "<pre>";print_r($utilization_category_imploded_conf);exit;
		$graph_stuff['categories'] = $utilization_category_imploded_conf;


		// $graph_stuff['series_data']['Closing_Balance'] = $closing_bal_series_conf;
		$graph_stuff['series_data']['Issued from KEMSA'] = $kemsa_issued_series_conf;
		// $graph_stuff['series_data']['Total'] = $total_rights_series_conf;
		$graph_stuff['series_data']['Allocated'] = $allocated_series_conf;
		// $graph_stuff['series_data']['Used'] = $used_series_conf;
		$graph_stuff['series_data']['Received'] = $received_series_conf;

		$graph_stuff['div'] = "utilization_graph_conf";
		// echo "<pre>";print_r($graph_stuff);exit;
		$utilization_graph_conf_custom = $this->allocationvsreceivedvsconsumed_highchart($graph_stuff);//Karsan
		// echo "<pre>";print_r($utilization_graph_conf_custom);exit;

		$data['utilization_graph_conf'] = $utilization_graph_conf_custom;

		// echo "<pre>";print_r($data['utilization_graph']);exit;
		// $county_aggregates = $this->drawing_rights_data($county_id,$district_id);
		// echo $year." ".$month;exit;
		$county_aggregates = $this->national_stockcard_v2($county_id,$district_id);
        $drawing_aggregates = $this->drawing_rights_data($year, $month, $county_id, $district_id, $facility_code);

		// echo "<pre>";print_r($drawing_aggregates);exit;
		//sum_opening 
		//sum_used

		$beginning_series = $year_total = $distributed = $balance = $used_drawing_rights_series = $beginning_category = $used_drawing_rights_category = array();
		$gcategories_final_ = $gcategories_final_s = $gcategories_final_c = array();

        usort($drawing_aggregates, function($a, $b) {
		    // return $a['screening_balance'] - $b['screening_balance'];
		    return $a['screening_distributed'] - $b['screening_distributed'];
		});



        // echo "<pre>";print_r($drawing_aggregates);exit;

		foreach ($drawing_aggregates as $key => $value) {
				$bal_s = 0;
				$drawing_rights_quarter = $value['quarter'];
				$gcategories_final_s_ids[] = $value['county_id'];
				$gcategories_final_s[] = $value['county'];

				$year_total["Screening"][] = $value['screening_year_total'];
				$distributed["Screening"][] = $value['screening_distributed'];
				$bal_s = $value['screening_year_total'] - $value['screening_distributed'];
				$bal_s = (isset($bal_s) && $bal_s > 0)? $bal_s:0;
				$balance["Screening"][] = $value['screening_balance'];
		}

		usort($drawing_aggregates, function($a, $b) {
		    // return $a['confirmatory_balance'] - $b['confirmatory_balance'];
		    return $a['confirmatory_distributed'] - $b['confirmatory_distributed'];
		});

		foreach ($drawing_aggregates as $key => $value) {
				$bal_c = 0;
				$gcategories_final_c_ids[] = $value['county_id'];
				$gcategories_final_c[] = $value['county'];

				$year_total["Confirmatory"][] = $value['confirmatory_year_total'];
				$distributed["Confirmatory"][] = $value['confirmatory_distributed'];
				$bal_c = $value['confirmatory_year_total'] - $value['confirmatory_distributed'];
				$bal_c = (isset($bal_c) && $bal_c > 0)? $bal_c:0;
				$balance["Confirmatory"][] = $value['confirmatory_balance'];
		}

		$data['drawing_rights_quarter'] =$drawing_rights_quarter;

		// echo "<pre>";print_r($year_total);
		// echo "<pre>";print_r($distributed);
		// echo "<pre>";print_r($balance);
		// exit;

		// echo "<pre>";print_r($gcategories_final_c_ids);exit;

		$gcategories_final_s = array_unique($gcategories_final_s);
		$gcategories_final_s_ids = array_unique($gcategories_final_s_ids);
		$gcategories_final_c = array_unique($gcategories_final_c);
		$gcategories_final_c_ids = array_unique($gcategories_final_c_ids);
		// $utilization_category = array_merge($used_category, array("Screening","Confirmatory"));
		// $utilization_category = array_merge($used_category, $gcategories_final_);

		// echo "<pre>";print_r($gcategories_final_);exit;
		$counties = implode(",",$gcategories_final_s);
			// echo "<pre>";print_r($counties);exit;

		$beginning_series = array_merge($beginning_series, array("Screening beginning" => (int)$national_aggregates['final_closing_bal4']));
		$beginning_series = array_merge($beginning_series, array("Confirmatory beginning" => (int)$national_aggregates['final_closing_bal5']));

		$used_drawing_rights_series = array_merge($used_drawing_rights_series, array("Screening used" => (int)$national_aggregates['final_used4']));
		$used_drawing_rights_series = array_merge($used_drawing_rights_series, array("Confirmatory used" => (int)$national_aggregates['final_used5']));
		
		$year_total_imploded = implode(',', $year_total['Screening']);
		$balance_imploded = implode(',', $balance['Screening']);
		
		$distributed_imploded = implode(',', $distributed['Screening']);
		if ($county_id > 0) {
			$distributed_imploded = implode(',', $distributed_county['Screening']);
			$balance_imploded = implode(',', $balance_county['Screening']);
		}

		// echo "<pre>";print_r($balance);exit;

		// $counties_imploded_s = implode(',', $gcategories_final_s);

		$graph_stuff_clickable = array();
		$graph_stuff_clickable['series_data']['year_total'] = $balance['Screening'];
		$graph_stuff_clickable['series_data']['distributed'] = $distributed['Screening'];
		$graph_stuff_clickable['categories'] = $gcategories_final_s;
		$graph_stuff_clickable['category_ids'] = $gcategories_final_s_ids;
		$graph_stuff_clickable['url'] = base_url().'dashboardv2/county_dashboard/';
		$graph_stuff_clickable['div'] = "utilization_bgg_screening_graph_clickable";
		$graph_custom_clickable = $this->drawing_rights_stacked_high_chart_clickable($graph_stuff_clickable);//Karsan
		// echo "<pre>";print_r($graph_custom_clickable);exit;
		$data['utilization_bgg_screening_graph_clickable'] = $graph_custom_clickable;

		$graph_stuff_clickable = array();
		$graph_stuff_clickable['series_data']['year_total'] = $balance['Confirmatory'];
		$graph_stuff_clickable['series_data']['distributed'] = $distributed['Confirmatory'];
		$graph_stuff_clickable['categories'] = $gcategories_final_c;
		$graph_stuff_clickable['category_ids'] = $gcategories_final_c_ids;
		$graph_stuff_clickable['url'] = base_url().'dashboardv2/county_dashboard/';
		$graph_stuff_clickable['div'] = "utilization_bgg_confirmatory_graph_clickable";
		$graph_custom_clickable = $this->drawing_rights_stacked_high_chart_clickable($graph_stuff_clickable);//Karsan
		// echo "<pre>";print_r($graph_custom_clickable);exit;
		$data['utilization_bgg_confirmatory_graph_clickable'] = $graph_custom_clickable;
		// echo "<pre>";print_r($graph_stuff_clickable);exit;
		// echo "<pre>";print_r($gcategories_final_s);exit;

		$graph_stuff = array();
		$counties_imploded_s = "'" . implode ( "', '", $gcategories_final_s) . "'";
		$graph_stuff['categories'] = $counties_imploded_s;

		// $graph_stuff['series_data']['year_total'] = $year_total_imploded;
		$graph_stuff['series_data']['year_total'] = $balance_imploded;
		$graph_stuff['series_data']['distributed'] = $distributed_imploded;
		$graph_stuff['div'] = "utilization_bgg_screening_graph";

		$graph_custom = $this->drawing_rights_stacked_high_chart($graph_stuff);//Karsan
		$data['utilization_bgg_screening_graph'] = $graph_custom;

		$year_total_imploded = implode(',', $year_total['Confirmatory']);
		$balance_imploded = implode(',', $balance['Confirmatory']);
		$distributed_imploded = implode(',', $distributed['Confirmatory']);
		if ($county_id > 0) {
			$distributed_imploded = implode(',', $distributed_county['Confirmatory']);
			$balance_imploded = implode(',', $balance_county['Confirmatory']);
		}

		$counties_imploded = implode(',', $gcategories_final_);

		$graph_stuff = array();

		// $beginning_imploded = "'" . implode ( "', '", $beginning_final_['Screening'] ) . "'";
		// $used_imploded = "'" . implode ( "', '", $used_final_['Screening'] ) . "'";
		$counties_imploded_c = "'" . implode ( "', '", $gcategories_final_c) . "'";
		// echo "<pre>";print_r($counties_imploded);exit;

		// echo "<pre>";print_r($balance_imploded);exit;
		$graph_stuff['categories'] = $counties_imploded_c;

		// $graph_stuff['series_data']['year_total'] = $year_total_imploded;
		$graph_stuff['series_data']['year_total'] = $balance_imploded;
		$graph_stuff['series_data']['distributed'] = $distributed_imploded;
		$graph_stuff['div'] = "utilization_bgg_confirmatory_graph";

		$graph_custom_conf = $this->drawing_rights_stacked_high_chart($graph_stuff);//Karsan

		$data['utilization_bgg_confirmatory_graph'] = $graph_custom_conf;

		// echo "<pre>";print_r($used_imploded_conf);exit;

		// echo "<pre>";print_r($data['utilization_bgg_confirmatory_graph']);exit;

		$graph_type='column';
		$graph_data=array_merge($graph_data,array("graph_id"=>'test-graph'));
		$graph_data = array_merge($graph_data, array("color" => "['#4b0082', '#6AF9C4']"));
		$graph_data=array_merge($graph_data,array("graph_title"=>""));
		$graph_data=array_merge($graph_data,array("stacking"=>"normal"));
		$graph_data=array_merge($graph_data,array("data_labels"=>"white"));
		$graph_data=array_merge($graph_data,array("graph_type"=>$graph_type));
		$graph_data=array_merge($graph_data,array("graph_yaxis_title"=>""));
		$graph_data=array_merge($graph_data,array("graph_categories"=>$gcategories_final_ ));
		$graph_data=array_merge($graph_data,array("series_data"=>$graph_stuff_raw['series_data']));
		$util_conf_graph = $this->hcmp_functions->create_high_chart_graph($graph_data);

		// echo "<pre>";print_r($util_conf_graph);exit;
		$data['test_graph'] = $util_conf_graph;

		$graph_type='column';
		$graph_data=array_merge($graph_data,array("graph_id"=>'utilization_bg_graph'));
		$graph_data = array_merge($graph_data, array("color" => "['#4b0082', '#6AF9C4']"));
		$graph_data=array_merge($graph_data,array("graph_title"=>""));
		$graph_data=array_merge($graph_data,array("stacking"=>"normal"));
		$graph_data=array_merge($graph_data,array("data_labels"=>"white"));
		$graph_data=array_merge($graph_data,array("graph_type"=>$graph_type));
		$graph_data=array_merge($graph_data,array("graph_yaxis_title"=>""));
		$graph_data=array_merge($graph_data,array("graph_categories"=>$counties_imploded ));
		$graph_data=array_merge($graph_data,array("series_data"=>array('Balance'=>$beginning_final_['Screening'],'Used drawing rights'=>$used_final_['Screening'])));
		$data['utilization_bg_graph']= $this->hcmp_functions->create_high_chart_graph($graph_data);

        // echo "<pre>";print_r($data);exit;
        // echo "<pre>";print_r($drawing_balance_confirmatory);exit;
		$data['counties'] = $county_name;

		// echo "<pre>";print_r($data);exit;
		$counties = $this->db->query("SELECT * FROM counties")->result_array();
		$districts = $this->db->query("SELECT * FROM districts")->result_array();

		$data['tracer'] = $tracer;
		$data['commodity_division'] = isset($division)? $division :"NULL";
		// $title_append = "Sample filtered";

		$title_append .= (isset($filtered_name) && $filtered_name !='')? $filtered_name:NULL;

		$title_prepend = (isset($county_id) && $county_id > 0)? 'County':'National';

		$title_append .= (isset($quarter) && $quarter > 0)? ' Quarter: '.$quarter:NULL;
		// echo "<pre>";print_r($title_append);exit;
		if ($quarter > 0) {
			$quartered = 1;
			$quarter_append = ' <span class="font-blue-steel">*</span>';
		}
		// $title_prepend = (isset($title_prepend) && $title_prepend !='')? $title_prepend:'National';

		$data['title_append'] = $title_append;
		$data['quartered'] = $quartered;
		$data['quarter_append'] = $quarter_append;
		// $data['tracer_commodities'] = $commodities;
		// $data['filtered_month'] = $month;
		$data['facility_count'] = $facility_count;
		$data['commodity_count'] = $commodity_count;
		$data['content_view'] = 'dashboard/dashboard';
		$data['county_data'] = $counties;
		$data['title_prepend'] = $title_prepend;
		$data['county_count'] = count($counties_using_CD4);
		$data['district_data'] = $districts;
		$data['commodity_divisions'] = $commodity_divisions;
		$data['title'] = "National Dashboard";
		$data['county_data'] = $counties;
		$data['county_id'] = $county_id;
		$data['subcounty_id'] = $district_id;
		$data['district_data'] = $districts;
		$data['maps'] = $map;
		$data['counties'] = $county_name;
		// echo "<pre>";print_r($data);exit;
		$this -> load -> view("v2/dashboard/dashboard_national", $data);
	}

	public function county_dashboard($county_id = NULL, $district_id = NULL,$quarter = NULL,$month_year = NULL,$year = NULL) 
	{
		$page_caching = $this->output->cache(CACHE_REFRESH_INTERVAL);
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;
		$default = "tracer";//FYI
		// echo "<pre>";print_r($quarter);exit;
		// $map = $this->render_map();
		$county_id = (isset($county_id) && $county_id > 0)? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id > 0)? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code > 0)? $facility_code:NULL;
		$quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;
		$year = (isset($year) && $year > 0)? $year:NULL;
		$month_year = (isset($month_year) && $month_year != 0 && $month_year !='')? $month_year:NULL;

		// echo "<pre>";print_r($month_year);exit;

		$filtered = 0;
		$title_append = "";
		$data['filter_years'] = $this->get_years_to_date();
		// echo "<pre>";print_r($data);exit;
		if ($quarter > 0 && $month_year < 1) {
			$quartered = 1;
			$quarter_append = ' <span class="font-blue-steel">*</span>';
			$filtered = 1;
		}
		// echo "<pre>";print_r($quarter_append);exit;
		// if ($county_id > 0 || $quarter > 0) {
		// echo "<pre>";print_r($year);exit;
		if ($year > 0) {
			// echo "IF";exit;
			$filtered = 1;
			// $quarter_append = ' <span class="font-blue-steel">*</span>';
			$data['filtered_year'] = $year;
			$title_append .= " Year: ".$year;
		}else{
			// echo "ELSE";exit;
			$year = date('Y');
			$title_append .= " Year: ".date('Y',strtotime($year));
		}

		// echo "<pre>";print_r($quarter_append);exit;
		// if ($county_id > 0 || $quarter > 0) {
		if ($county_id > 0) {
			$filtered = 1;
			// $quarter_append = ' <span class="font-blue-steel">*</span>';
		}
		// echo "<pre>";print_r($month_year);exit;
		if (isset($month_year) && $month_year !='') {
            // $year = substr($month, -4);
			$m_data = explode('_', $month_year);
			// echo "<pre>";print_r($m_data);exit;
            $month = $m_data[0];
            $year = $m_data[1];
            $monthyear = $year . '-' . $month . '-01';
			$filtered_date = $year."-".$month."-01";
			$filtered = 1;
			// $quarter_append = ' <span class="font-blue-steel">*</span>';
			$title_append .= " as of ".date('F Y',strtotime($monthyear))." Report";
        } else {
			$filtered_date = "";
            /*$month = $this->session->userdata('Month');
            if ($month == '') {
                $month = date('mY', time());
            }
            $year = date('Y');
            $month = substr_replace($month, "", -4);
            $monthyear = $year . '-' . $month . '-01';*/
        }

        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;
		$data['filtered'] = $filtered;

        $current_date = date('Y-m-d');

		$append_date = (isset($append_date) && $append_date !="")? $append_date : date('Y-m-d');
		
		$county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;

		$commodity_count = Dashboard_model::get_commodity_count();
		// $commodities = Dashboard_model::get_division_commodities($division);
		$page_title = "RTK Dashboard.";
		$data['page_title'] = $page_title;
		// echo "<pre>";print_r($commodities);exit;
		$commodity_divisions = Dashboard_model::get_division_details();
		$counties = $this->db->query("SELECT * FROM counties")->result_array();
		$districts = $this->db->query("SELECT * FROM districts")->result_array();

		$facility_count = Dashboard_model::get_online_offline_facility_count();
		$counties_using_CD4 = Counties::get_counties_all_using_CD4();

		$filtered_name = 	NULL;
		$district_data = (isset($district_id) && ($district_id > 0)) ? districts::get_district_name($district_id) -> toArray() : NULL;
		// echo "<pre>";print_r($district_data);exit;

		$county_search = counties::get_county_name($county_id);
		// echo "<pre>";print_r($county_search);exit;
		if (count($county_search) > 0) {
			// $filtered_name = $county_search['county']." County $append_date";
			$filtered_name = " ".$county_search['county']." County";
		}
		
		if ($district_data != NULL) {
			// $filtered_name = $district_data[0]['district']." Sub-County $append_date";
			$filtered_name = " ".$district_data[0]['district']." Sub-County";
		}

		// $data_start_date = DATA_START_DATE;
		$data['filter_months'] = $this->get_months_between_dates();

		// echo "<pre>";print_r($data_start_date);exit;

		$current = date('m');
        $curMonth = $current;
        $curQuarter = ceil($curMonth/3);//KARSAN
        // echo "<pre>";print_r($month);exit;
        if ($quarter > 0 && $month < 1) {
        	$curQuarter = $quarter;
        	
        }else{
        	$quarter_data = $this->get_year_quarter($month,NULL,$year,NULL);
        	$curQuarter = $quarter_data['quarter'];
        }
		$data['drawing_quarters'] = $curQuarter;

        // echo "<pre>";print_r($county_drawing_data);exit;
    	$drawing_total_confirmatory = $drawing_total_screening = 0;
    	$drawing_used_confirmatory = $drawing_used_screening = 0;
    	$drawing_balance_confirmatory = $drawing_balance_screening = 0;
    	$county_scr_distributed_aggr = $county_conf_distributed_aggr = 0;
    	$county_scr_total_drawing_rights = $county_conf_total_drawing_rights = 0;

        // echo "<pre>";print_r($quarter);exit;
    	
        if ($filtered == 0) {
	        /*if ($quarter > 0) {
	        	get_county_allocation_data
	        }else{
	        	$c_drawing_query = "SELECT * FROM county_drawing_rights $summary_and";
				// echo $c_drawing_query;exit;
	        	$county_drawing_data = $this->db->query($c_drawing_query)->result_array();
	        }*/

	        $counties_drawing_data_s = $counties_drawing_data_c = $counties_drawing_data_ss = $counties_drawing_data_cc = array();

	        // echo "<pre>";print_r($county_drawing_data);exit;
	        $drawing_data = $this->drawing_rights_data($year, $month, $county_id, $district_id, $facility_code);
	        // echo "<pre>";print_r($drawing_data);exit;
	    	$balances_scr = $this->get_opening_and_closing_balances(NULL, NULL,NULL, 4 , $beg_date, $end_date, $quarter,"county",$month,$year);
	    	// echo "<pre>";print_r($balances_scr);exit;
	    	$balances_conf = $this->get_opening_and_closing_balances(NULL, NULL,NULL, 5 , $beg_date, $end_date, $quarter,"county",$month,$year);

	    	$c_order_data_s = $this->get_county_order_data($c_id, NULL, NULL,4 , $quarter,$month,$year,"county");
	     	$c_order_data_c = $this->get_county_order_data($c_id, NULL, NULL,5 , $quarter,$month,$year,"county");

	     	// echo "<pre>";print_r($c_order_data_s);exit;
	        $kemsa_issue_data_s = $this->get_kemsa_issue_data(NULL, NULL, NULL, 4 , $quarter,$year,$month,"county");
	        // echo "<pre>";print_r($kemsa_issue_data_s);exit;
	        $kemsa_issue_data_c = $this->get_kemsa_issue_data(NULL, NULL, NULL, 5 , $quarter,$year,$month,"county");

	        // echo "<pre>";print_r($data);exit;
	        // $summary_data_s = $this->get_county_order_data($county_id, $district_id, $facility_code, 4 , $quarter,$month,$year,"county");
	        // // echo "<pre>";print_r($summary_data_s);exit;
	        // $summary_data_c = $this->get_county_order_data($county_id, $district_id, $facility_code, 5 , $quarter,$month,$year,"county");
	        
	        foreach ($drawing_data as $key => $value) {
	        	// echo "<pre>";print_r($value);
	        	$c_id = $value['county_id'];
	        	$c_name = $value['county'];
	        	
	        	$summary_and = '';
	        	$summary_and .= (isset($c_id) && $c_id!='')? " AND county_id = $c_id" :NULL; 
	        	
	        	$c_drawing_query = "SELECT * FROM county_drawing_rights_details WHERE quarter = $curQuarter AND year = $year $summary_and";
				// echo $c_drawing_query;
		        $county_drawing_data = $this->db->query($c_drawing_query)->result_array();
		        // echo "<pre>";print_r($county_drawing_data);exit;

	        	$scr_dist = $county_drawing_data[0]['screening_distributed'];
	        	$conf_dist = $county_drawing_data[0]['confirmatory_distributed'];
	        	$scr_yr_total = $county_drawing_data[0]['screening_total'];
	        	$conf_yr_total = $county_drawing_data[0]['confirmatory_total'];
	        
	        	/*$data['summary_data_date'] = $summary_data_s['results'][0]['order_date'];
		        // echo "<pre>";print_r($data);exit;

		        $received_site_scr = $summary_data_s['results'][0]['q_received'];
		        $received_site_conf = $summary_data_c['results'][0]['q_received'];

		        $data['received_site_screening'] = $received_site_scr;
		        $data['received_site_confirmatory'] = $received_site_conf;
	        	$data['summary_data_date'] = $summary_data_s['date_text'];

		        $consumed_site_scr = $summary_data_s['results'][0]['q_used'];
		        $consumed_site_conf = $summary_data_c['results'][0]['q_used'];

		        $data['consumed_site_screening'] = $consumed_site_scr;
		        $data['consumed_site_confirmatory'] = $consumed_site_conf;*/
		        
				$counties_drawing_data_s[$c_id]['issued_kemsa'] = $kemsa_issue_data_s[$c_id]['qty_issued'];
				$counties_drawing_data_c[$c_id]['issued_kemsa'] = $kemsa_issue_data_c[$c_id]['qty_issued'];

				$counties_drawing_data_s[$c_id]['beg_bal'] = $balances_scr[$c_id]['beginning_balance'];
				$counties_drawing_data_s[$c_id]['closing_bal'] = $balances_scr[$c_id]['closing_balance'];
				
				$counties_drawing_data_s[$c_id]['county_id'] = $c_id;
				$counties_drawing_data_s[$c_id]['county_name'] = $c_name;
				$counties_drawing_data_s[$c_id]['allocated'] = $scr_dist;

				$counties_drawing_data_s[$c_id]['q_rec'] = $c_order_data_s['results'][$c_id]['q_received'];
				$counties_drawing_data_s[$c_id]['q_used'] = $c_order_data_s['results'][$c_id]['q_used'];
				$counties_drawing_data_s[$c_id]['tests_done'] = $c_order_data_s['results'][$c_id]['no_of_tests_done'];
				$counties_drawing_data_s[$c_id]['losses'] = $c_order_data_s['results'][$c_id]['losses'];
				$counties_drawing_data_s[$c_id]['positive_adj'] = $c_order_data_s['results'][$c_id]['positive_adj'];
				$counties_drawing_data_s[$c_id]['negative_adj'] = $c_order_data_s['results'][$c_id]['negative_adj'];
				

				$counties_drawing_data_c[$c_id]['county_id'] = $c_id;
				$counties_drawing_data_c[$c_id]['county_name'] = $c_name;
				$counties_drawing_data_c[$c_id]['allocated'] = $conf_dist;
				$counties_drawing_data_c[$c_id]['beg_bal'] = $balances_conf[$c_id]['beginning_balance'];
				$counties_drawing_data_c[$c_id]['q_rec'] = $c_order_data_c['results'][$c_id]['q_received'];
				$counties_drawing_data_c[$c_id]['q_used'] = $c_order_data_c['results'][$c_id]['q_used'];
				$counties_drawing_data_c[$c_id]['tests_done'] = $c_order_data_c['results'][$c_id]['no_of_tests_done'];
				$counties_drawing_data_c[$c_id]['losses'] = $c_order_data_c['results'][$c_id]['losses'];
				$counties_drawing_data_c[$c_id]['positive_adj'] = $c_order_data_c['results'][$c_id]['positive_adj'];
				$counties_drawing_data_c[$c_id]['negative_adj'] = $c_order_data_c['results'][$c_id]['negative_adj'];
				$counties_drawing_data_c[$c_id]['closing_bal'] = $balances_conf[$c_id]['closing_balance'];

				// array_push($counties_drawing_data_s, $counties_drawing_data_ss);
				// array_push($counties_drawing_data_c, $counties_drawing_data_cc);

	        	// echo "<pre>";print_r($order_data_s);
	        	// echo "<pre>";print_r($kemsa_issues);exit;
	        }

	        // echo "<pre>";print_r($counties_drawing_data_s);exit;

	        $data['counties_drawing_data_s'] = $counties_drawing_data_s;
	        $data['counties_drawing_data_c'] = $counties_drawing_data_c;
        }else{
        	$summary_and = $limit = "";
	        // $summary_and .= (isset($county_id) && $county_id!='')? " WHERE county_id = $county_id" :NULL; 
	        $summary_and .= (isset($county_id) && $county_id!='')? " AND county_id = $county_id" :NULL; 
	        $summary_and .= (isset($quarter) && $quarter!='')? " AND quarter = $quarter " :NULL; 
	        $limit .= (isset($county_id) && $county_id!='')? " LIMIT 1 " :NULL; 
	        
	        // echo "<pre>";print_r($year);
	        // echo "<pre>";print_r($curQuarter);exit;
	        // $c_drawing_query = "SELECT * FROM county_drawing_rights $summary_and";
	        $c_drawing_query = "SELECT * FROM county_drawing_rights_details WHERE year = $year $summary_and ORDER BY created_at DESC";
			// echo $c_drawing_query;exit;
	        $county_drawing_data = $this->db->query($c_drawing_query)->result_array();
	        // echo "<pre>";print_r($county_drawing_data);exit;
			$qtt_data_arr = array();//array that holds the distributed values to ensure duplicates are not added again.
			foreach ($county_drawing_data as $key => $value) {
	        	// echo "<pre>";print_r($value);exit;
	        	// echo "<pre>";print_r($curQuarter);exit;
	        	// $drawing_rights_data_['quarter'] = $curQuarter;
				$q_ = $value['quarter'];
				$y_ = $value['year'];
				$m_ = $value['month'];

	        	$screening_total = $confirmatory_total = 0;
	        	// $c_id = $value['county_id'];
	        	$screening_total = $value['screening_total']*$curQuarter;
	        	$confirmatory_total = $value['confirmatory_total']*$curQuarter;

	        	$screening_total_raw = $value['screening_total'];
	        	$confirmatory_total_raw = $value['confirmatory_total'];
	        	// echo "<pre>";print_r($screening_total);exit;
	        	$screening_used = $value['screening_used'];
	        	$confirmatory_used = $value['confirmatory_used'];

	        	$screening_distributed = $value['screening_distributed'];
	        	$confirmatory_distributed = $value['confirmatory_distributed'];

	        	// echo "<pre>";print_r($value['screening_used']);exit;
	        	$screening_balance = $screening_total - $value['screening_distributed'];
	        	$confirmatory_balance = $confirmatory_total - $value['confirmatory_distributed'];

				$screening_balance = (isset($screening_balance) && $screening_balance > 0)? $screening_balance:0;
				$confirmatory_balance = (isset($confirmatory_balance) && $confirmatory_balance > 0)? $confirmatory_balance:0;

				if (count($county_drawing_data) > 0) {

					if (!in_array($screening_total_raw, $qtt_data_arr['scr']['total'])) {
						$county_scr_total_drawing_rights += $screening_total_raw;
					}else{
						// echo "<pre>";print_r($qtt_data_arr);
						// echo "<pre>";print_r($screening_total);exit;
					}

					if (!in_array($confirmatory_total_raw, $qtt_data_arr['conf']['total'])) {
						$county_conf_total_drawing_rights += $confirmatory_total_raw;
					}else{
						// echo "<pre>";print_r($confirmatory_distributed);exit;
					}

					if (!in_array($screening_distributed, $qtt_data_arr['scr']['distributed'])) {
						$county_scr_distributed_aggr += $screening_distributed;
						$data['drawing_distributed_screening'] += $screening_distributed;
					}else{
						// echo "<pre>";print_r($screening_distributed);exit;
					}

					if (!in_array($confirmatory_distributed, $qtt_data_arr['conf']['distributed'])) {
						$county_conf_distributed_aggr += $confirmatory_distributed;
			        	$data['drawing_distributed_confirmatory'] += $confirmatory_distributed;
					}else{
						// echo "<pre>";print_r($confirmatory_distributed);exit;
					}

		        	// $data['drawing_total_screening'] = $screening_total;
		        	// $data['drawing_total_confirmatory'] = $confirmatory_total;

		        	// $data['drawing_used_screening'] += $screening_used;
		        	// $data['drawing_used_confirmatory'] += $confirmatory_used;

		        	$data['drawing_used_screening'] = $screening_used;
		        	$data['drawing_used_confirmatory'] = $confirmatory_used;
		        	
		        	$qtt_data_arr['scr']['distributed'][] = $screening_distributed;
					$qtt_data_arr['conf']['distributed'][] = $confirmatory_distributed;

					$qtt_data_arr['scr']['total'][] = $screening_total_raw;
					$qtt_data_arr['conf']['total'][] = $confirmatory_total_raw;

	        	}else{
					$county_scr_total_drawing_rights = $screening_total;
					$county_conf_total_drawing_rights = $confirmatory_total;

					$county_scr_distributed_aggr = $screening_distributed;
					$county_conf_distributed_aggr = $confirmatory_distributed;

	        		$data['drawing_total_screening'] = $screening_total;
		        	$data['drawing_total_confirmatory'] = $confirmatory_total;

		        	$data['drawing_used_screening'] = $screening_used;
		        	$data['drawing_used_confirmatory'] = $confirmatory_used;

		        	$data['drawing_distributed_screening'] = $screening_distributed;
		        	$data['drawing_distributed_confirmatory'] = $confirmatory_distributed;

	        	}

	        	// echo "<pre>";print_r($county_scr_total_drawing_rights);exit;
	        }
	        
	        // echo "<pre>";print_r($county_scr_total_drawing_rights);exit;
	        // echo "<pre>";print_r($qtt_data_arr);exit;
	        // echo "<pre>";print_r($data);exit;
	        // echo "<pre>";print_r($quarter);exit;
			/*KEMSA DATA*/

				$kemsa_issue_data_s = $this->get_kemsa_issue_data($county_id, $district_id, $facility_code, 4 , $quarter,$year,$month,"county");
		        // echo "<pre>";print_r($kemsa_issue_data_s);exit;
		        $kemsa_issue_data_c = $this->get_kemsa_issue_data($county_id, $district_id, $facility_code, 5 , $quarter,$year,$month,"county");

		        if ($year > 0 && $month == 0) {
		        	$kemsa_issue_date_ = $kemsa_issue_data_s[$county_id]['issue_date'];
		        	// echo "<pre>";print_r($kemsa_issue_date_);exit;
		        	if (empty($kemsa_issue_date_)) {
		        		$kemsa_issue_date_ = date('Y-m-d');
		        	}

					$data['kemsa_issued_data_date'] = "Year: ".date('Y',strtotime($kemsa_issue_date_));
				}elseif(isset($month_year) && $month_year !=''){
					$data['kemsa_issued_data_date'] = date('F Y',strtotime($filtered_date));
				}else{
					$kemsa_issue_date_ = $kemsa_issue_data_s[$county_id]['issue_date'];
		        	// echo "<pre>";print_r($kemsa_issue_date_);exit;
		        	if (empty($kemsa_issue_date_)) {
		        		$kemsa_issue_date_ = date('Y-m-d');
		        	}

					$data['kemsa_issued_data_date'] = date('F Y',strtotime($kemsa_issue_date_));
				}

				// echo "<pre>";print_r($kemsa_issued_data_date);exit;
		        $kemsa_issued_screening = $kemsa_issue_data_s[$county_id]['qty_issued'];
		        $kemsa_issued_confirmatory = $kemsa_issue_data_c[$county_id]['qty_issued'];
				$data['kemsa_issue_date'] = $kemsa_issue_data_s['first_date'];
				$data['kemsa_issued_screening'] = $kemsa_issue_data_s[$county_id]['qty_issued'];
				$data['kemsa_issued_confirmatory'] = $kemsa_issue_data_c[$county_id]['qty_issued'];

			/*END OF KEMSA DATA*/
	        $data['total_allocated_screening'] = $county_scr_distributed_aggr;
	        $data['total_allocated_confirmatory'] = $county_conf_distributed_aggr;
			
			// echo "<pre>";print_r($screening_total);
			// echo "<pre>";print_r($county_scr_total_drawing_rights);
			// echo "<pre>";print_r($county_scr_distributed_aggr);exit;

	        $data['drawing_total_screening'] = $county_scr_total_drawing_rights;
		    $data['drawing_total_confirmatory'] = $county_conf_total_drawing_rights;

			$data['drawing_balance_screening'] = $county_scr_total_drawing_rights - $county_scr_distributed_aggr;
		    $data['drawing_balance_confirmatory'] = $county_conf_total_drawing_rights - $county_conf_distributed_aggr;

        	// echo "<pre>";print_r($data);exit;

		    // $county_scr_total_drawing_rights = $drawing_data_details['national_drawing_totals']['screening_total_annual'];
		    // $county_conf_total_drawing_rights = $drawing_data_details['national_drawing_totals']['confirmatory_total_annual'];

	        $district_drawing_data_s = $district_drawing_data_c = $district_drawing_data_ss = $district_drawing_data_cc = array();
        	$district_data = $this->get_district_data($county_id,$district_id);
	        // echo "<pre>";print_r($district_data);exit;
        	// echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;

	    	$balances_scr = $this->get_opening_and_closing_balances($county_id, NULL, NULL, 4 , $beg_date, $end_date, $quarter,"subcounty",$month,$year);
	    	$balances_conf = $this->get_opening_and_closing_balances($county_id, NULL, NULL, 5 , $beg_date, $end_date, $quarter,"subcounty",$month,$year);
	    	
	    	// echo "<pre>";print_r($quarter);exit;
	        $kemsa_issue_data_s = $this->get_kemsa_issue_data($county_id, NULL, NULL, 4 , $quarter,$year,$month,"subcounty");
	        // echo "<pre>";print_r($kemsa_issue_data_s);exit;
	        $kemsa_issue_data_c = $this->get_kemsa_issue_data($county_id, NULL, NULL, 5 , $quarter,$year,$month,"subcounty");

	        $quarter_data = $this->get_year_quarter(NULL,$set_quarter,$year);
	        $quarter_borders = $this->get_previous_and_next_quarter($set_quarter,$year);
	        // echo "<pre>";print_r($quarter_borders);exit;
	        // echo "<pre>";print_r($quarter_data);exit;
	        $curQuarter = $quarter_data['quarter'];
	        $curQuarterYr = $quarter_data['year'];

	        $curQuarterMonth = $quarter_data['months'][0];
	        $data['current_quarter_year'] = $curQuarterYr;
	        $data['current_quarter_month'] = $curQuarterMonth;
	        // echo "<pre>";print_r($data);exit;
	        $prevQuarter = $quarter_borders['previous_quarter'];
	        $nextQuarter = $quarter_borders['next_quarter'];
	        $nextQuarterYr = $quarter_borders['next_year'];
	        $prevQuarterYr = $quarter_borders['previous_year'];
	        $prevQuarterMonth = date('m',strtotime($quarter_borders['previous_last_date']));


	        $d_order_data_s = $this->get_county_order_data($county_id, NULL, NULL, 4 , $quarter,$month,$year,"subcounty");
	        $d_order_data_c = $this->get_county_order_data($county_id, NULL, NULL, 5 , $quarter,$month,$year,"subcounty");
	        //MONTHLY FILTER
	        // echo "<pre>";print_r($d_order_data_s);exit;
	    	// echo "<pre>";print_r($quarter);exit;
	    	// echo "<pre>";print_r($district_data);exit;
	        foreach ($district_data as $key => $value) {
	        	// echo "<pre>";print_r($value);exit;
	        	$c_id = $value['county_id'];
	        	$c_name = $value['county_name'];
	        	$d_id = $value['id'];
	        	$d_name = $value['district'];

	        	$query = "SELECT * FROM district_drawing_rights WHERE county_id = $county_id AND quarter = $curQuarter AND year = $curQuarterYr AND district_id = $d_id GROUP BY district_id";
		        // echo "<pre>";print_r($query);exit;
		        $dist_allocated = $this->db->query($query)->result_array();
		        // echo "<pre>";print_r($dist_allocated);

				// echo $balances_scr = $this->get_opening_and_closing_balances($c_id, NULL,NULL, 4 , $beg_date, $end_date, $quarter);
				// $counties_drawing_data_titles

				$district_drawing_data_ss['county_id'] = $c_id;
				$district_drawing_data_ss['county_name'] = $c_name;
				$district_drawing_data_ss['district_id'] = $d_id;
				$district_drawing_data_ss['district_name'] = $d_name;
				// $district_drawing_data_ss['allocated'] = $scr_dist;
				$district_drawing_data_ss['allocated'] = $dist_allocated[0]['screening_allocated'];
				$district_drawing_data_ss['issued_kemsa'] = $kemsa_issue_data_s[$d_id]['qty_issued'];
				// $district_drawing_data_ss['beg_bal'] = $d_order_data_s[0]['beginning_bal'];
				$district_drawing_data_ss['beg_bal'] = $balances_scr[$d_id]['beginning_balance'];

				$district_drawing_data_ss['q_rec'] = $d_order_data_s['results'][$d_id]['q_received'];
				$district_drawing_data_ss['q_used'] = $d_order_data_s['results'][$d_id]['q_used'];
				$district_drawing_data_ss['tests_done'] = $d_order_data_s['results'][$d_id]['no_of_tests_done'];
				$district_drawing_data_ss['losses'] = $d_order_data_s['results'][$d_id]['losses'];
				$district_drawing_data_ss['positive_adj'] = $d_order_data_s['results'][$d_id]['positive_adj'];
				$district_drawing_data_ss['negative_adj'] = $d_order_data_s['results'][$d_id]['negative_adj'];
				// $district_drawing_data_ss['closing_bal'] = $d_order_data_s[0]['closing_stock'];
				$district_drawing_data_ss['closing_bal'] = $balances_scr[$d_id]['closing_balance'];

				$district_drawing_data_cc['county_id'] = $c_id;
				$district_drawing_data_cc['county_name'] = $c_name;
				$district_drawing_data_cc['district_id'] = $d_id;
				$district_drawing_data_cc['district_name'] = $d_name;
				// $district_drawing_data_cc['allocated'] = $conf_dist;
				$district_drawing_data_cc['allocated'] = $dist_allocated[0]['confirmatory_allocated'];
				$district_drawing_data_cc['issued_kemsa'] = $kemsa_issue_data_c[$d_id]['qty_issued'];
				// $district_drawing_data_cc['beg_bal'] = $d_order_data_c[0]['beginning_bal'];
				$district_drawing_data_cc['beg_bal'] = $balances_conf[$d_id]['beginning_balance'];

				$district_drawing_data_cc['q_rec'] = $d_order_data_c['results'][$d_id]['q_received'];
				$district_drawing_data_cc['q_used'] = $d_order_data_c['results'][$d_id]['q_used'];
				$district_drawing_data_cc['tests_done'] = $d_order_data_c['results'][$d_id]['no_of_tests_done'];
				$district_drawing_data_cc['losses'] = $d_order_data_c['results'][$d_id]['losses'];
				$district_drawing_data_cc['positive_adj'] = $d_order_data_c['results'][$d_id]['positive_adj'];
				$district_drawing_data_cc['negative_adj'] = $d_order_data_c['results'][$d_id]['negative_adj'];
				// $district_drawing_data_cc['closing_bal'] = $d_order_data_c[0]['closing_stock'];
				$district_drawing_data_cc['closing_bal'] = $balances_conf[$d_id]['closing_balance'];

				array_push($district_drawing_data_s, $district_drawing_data_ss);
				array_push($district_drawing_data_c, $district_drawing_data_cc);

				// echo "<pre>";print_r($district_drawing_data_ss);exit;
	        	// echo "<pre>";print_r($order_data_s);
	        	// echo "<pre>";print_r($kemsa_issues);exit;
	        }

	        // echo "<pre>";print_r($district_drawing_data_s);exit;
	        $data['district_drawing_data_s'] = $district_drawing_data_s;
	        $data['district_drawing_data_c'] = $district_drawing_data_c;
	        // echo "<pre>";print_r($data);exit;
			$graph_type = $graph_data = $category_data = $series_data = array();
			$category_data[] = "Screening";
			$category_data[] = "Confirmatory";
			$screening = array("10","20");
			// echo "<pre>";print_r($screening);exit;
			$series_data['Screening'] = [10];
			$series_data['Confirmatory'] = [20];

			$county_name = "";

			$graph_type = 'bar';

			$graph_title = (isset($filtered_name) || ($filtered_name !=''))? "Drawing rights for $filtered_name" :NULL;

			$graph_data = array_merge($graph_data, array("graph_id" => 'drawing_rights_graph'));
			$graph_data = array_merge($graph_data, array("graph_title" => $graph_title));
			$graph_data = array_merge($graph_data, array("graph_type" => $graph_type));
			$graph_data = array_merge($graph_data, array("graph_yaxis_title" => "Stock levels"));
			$graph_data = array_merge($graph_data, array("graph_categories" => $category_data));
			$graph_data = array_merge($graph_data, array("series_data" => array('total' => $series_data)));

			// $data['drawing_rights_graph'] = $this -> hcmp_functions -> create_high_chart_graph($graph_data);

			// echo "<pre>";print_r($data['high_graph']);exit;

			$dhis_data = $this->get_dhis_data($county_id,$district_id,$facility_code,$quarter,$month,$year);//KARSAN
			// echo "<pre>";print_r($dhis_data);exit;

			$data['dhis_screening_tests_done'] = $dhis_data[4]['tests_done'];
			$data['dhis_confirmatory_tests_done'] = $dhis_data[5]['tests_done'];

			$national_aggregates = $this->national_aggregate($county_id,$district_id,$facility_code,$quarter,$month,$year);//KARSAN

			// echo "<pre>AGGR: ";print_r($national_aggregates);exit;

			$data['screening_tests_done'] = $national_aggregates['final_tests4'];
			$data['confirmatory_tests_done'] = $national_aggregates['final_tests5'];

			$data['screening_closing_balance'] = $national_aggregates['final_closing_bal4'];
			$data['confirmatory_closing_balance'] = $national_aggregates['final_closing_bal5'];

			$data['screening_requested'] = $national_aggregates['final_requested4'];
			$data['confirmatory_requested'] = $national_aggregates['final_requested5'];

			$data['screening_requested'] = $national_aggregates['final_requested4'];
			$data['confirmatory_requested'] = $national_aggregates['final_requested5'];

			$data['screening_days_out'] = $national_aggregates['final_days4'];
			$data['confirmatory_days_out'] = $national_aggregates['final_days5'];

			$data['screening_expiring'] = $national_aggregates['final_expiring4'];
			$data['confirmatory_expiring'] = $national_aggregates['final_expiring5'];

			// echo "<pre>";print_r($data);exit;
	        $summary_data_s = $this->get_county_order_data($county_id, $district_id, $facility_code, 4 , $quarter,$month,$year);
	        // echo "<pre>";print_r($summary_data_s);exit;
	        $summary_data_c = $this->get_county_order_data($county_id, $district_id, $facility_code, 5 , $quarter,$month,$year);
	        
	        $data['summary_data_date'] = $summary_data_s['results'][0]['order_date'];
	        // echo "<pre>";print_r($data);exit;

	        $received_site_scr = $summary_data_s['results'][0]['q_received'];
	        $received_site_conf = $summary_data_c['results'][0]['q_received'];

	        $data['received_site_screening'] = $received_site_scr;
	        $data['received_site_confirmatory'] = $received_site_conf;

	        $data['positive_adj_screening'] = $summary_data_s['results'][0]['positive_adj'];
	        $data['positive_adj_confirmatory'] = $summary_data_c['results'][0]['positive_adj'];

	        $data['negative_adj_screening'] = $summary_data_s['results'][0]['negative_adj'];
	        $data['negative_adj_confirmatory'] = $summary_data_c['results'][0]['negative_adj'];

	        $data['losses_screening'] = $summary_data_s['results'][0]['losses'];
	        $data['losses_confirmatory'] = $summary_data_c['results'][0]['losses'];

	        $data['quantity_used_screening'] = $summary_data_s['results'][0]['q_used'];
	        $data['quantity_used_confirmatory'] = $summary_data_c['results'][0]['q_used'];

        	$data['summary_data_date'] = $summary_data_s['date_text'];

	        $consumed_site_scr = $summary_data_s['results'][0]['q_used'];
	        $consumed_site_conf = $summary_data_c['results'][0]['q_used'];

	        $data['consumed_site_screening'] = $consumed_site_scr;
	        $data['consumed_site_confirmatory'] = $consumed_site_conf;

	        // echo "<pre>";print_r($data);exit;
	        // echo "<pre>";print_r($quarter);exit;
	        $balances_scr = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 4 , $beg_date, $end_date, $quarter,NULL,$month,$year);
	        $balances_conf = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 5 , $beg_date, $end_date, $quarter,NULL,$month,$year);
	        // echo "<pre>";print_r($balances_scr);exit;
	        // $closing_bal_scr = $summary_data_s['results'][0]['beginning_bal'];
	        // $closing_bal_conf = $summary_data_c['results'][0]['beginning_bal'];

	        $closing_bal_scr = $balances_scr['closing_balance']['closing_balance'];
	        $closing_bal_conf = $balances_conf['closing_balance']['closing_balance'];
	        $closing_bal_order_date = $balances_scr['closing_balance']['order_date'];

	        $beginning_bal_scr = $balances_scr['beginning_balance']['beginning_balance'];
	        $beginning_bal_conf = $balances_conf['beginning_balance']['beginning_balance'];
	        $beginning_bal_order_date = $balances_scr['beginning_balance']['order_date'];

	        $data['beginning_balance_screening'] = $beginning_bal_scr;
	        $data['beginning_balance_confirmatory'] = $beginning_bal_conf;
	        $data['beginning_balance_order_date'] = $beginning_bal_order_date;

	        $data['closing_balance_screening'] = $closing_bal_scr;
	        $data['closing_balance_confirmatory'] = $closing_bal_conf;
	        $data['closing_balance_order_date'] = $closing_bal_order_date;

			$county_scr_closing_balance = $closing_bal_scr;
			$county_conf_closing_balance = $closing_bal_conf;

			$used_series_scr = $used_series_conf = $allocated_series_scr = $allocated_series_conf = array();
			$received_series_scr = $received_series_conf = $used_category_scr = $used_category_conf = array();
			$received_category_scr = $received_category_conf = array();
			$total_rights_series_scr = $total_rights_series_conf = array();
			$closing_bal_series_scr = $closing_bal_series_conf = array();
			$kemsa_issued_series_scr = $kemsa_issued_series_conf = array();

			$total_rights_series_scr = array_merge($total_rights_series_scr, array("Total" => (int)$county_scr_total_drawing_rights));
			$total_rights_series_conf = array_merge($total_rights_series_conf, array("Total" => (int)$county_conf_total_drawing_rights));

			$kemsa_issued_series_scr = array_merge($kemsa_issued_series_scr, array("Issued from KEMSA" => (int)$kemsa_issued_screening));
			$kemsa_issued_series_conf = array_merge($kemsa_issued_series_conf, array("Issued from KEMSA" => (int)$kemsa_issued_confirmatory));

			$closing_bal_series_scr = array_merge($closing_bal_series_scr, array("Closing Balance" => (int)$county_scr_closing_balance));
			$closing_bal_series_conf = array_merge($closing_bal_series_conf, array("Closing Balance" => (int)$county_conf_closing_balance));

			$allocated_series_scr = array_merge($allocated_series_scr, array("Allocated" => (int)$county_scr_distributed_aggr));
			$allocated_series_conf = array_merge($allocated_series_conf, array("Allocated" => (int)$county_conf_distributed_aggr));

			$used_series_scr = array_merge($used_series_scr, array("Used" => (int)$consumed_site_scr));
			$used_series_conf = array_merge($used_series_scr, array("Used" => (int)$consumed_site_conf));

			$received_series_scr = array_merge($received_series_scr, array("Received" => (int)$received_site_scr));
			$received_series_conf = array_merge($received_series_conf, array("Received" => (int)$received_site_conf));

			// echo "<pre>";print_r($total_rights_series_scr);exit;
			// $utilization_category = array_merge($used_category, array("Screening","Confirmatory"));
			$utilization_category_scr[] = "Screening";
			$utilization_category_conf[] = "Confirmatory";
			$utilization_category_imploded_scr = "'" . implode ( "', '", $utilization_category_scr) . "'";
			$utilization_category_imploded_conf = "'" . implode ( "', '", $utilization_category_conf) . "'";

			$graph_stuff = array();

			// $beginning_imploded = "'" . implode ( "', '", $beginning_final_['Screening'] ) . "'";
			// $used_imploded = "'" . implode ( "', '", $used_final_['Screening'] ) . "'";
			$counties_imploded_c = "'" . implode ( "', '", $gcategories_final_c) . "'";

			$closing_bal_series_scr =  implode ( "', '", $closing_bal_series_scr);
			$total_rights_series_scr =  implode ( "', '", $total_rights_series_scr);
			$kemsa_issued_series_scr =  implode ( "', '", $kemsa_issued_series_scr);
			$allocated_series_scr = implode ( "', '", $allocated_series_scr);
			$used_series_scr = implode ( "', '", $used_series_scr);
			$received_series_scr = implode ( "', '", $received_series_scr);
			// echo "<pre>";print_r($closing_bal_series_scr);exit;

			// echo "<pre>";print_r($utilization_category_imploded_scr);exit;
			$graph_stuff['categories'] = $utilization_category_imploded_scr;

			// $graph_stuff['series_data']['Closing Balance'] = $closing_bal_series_scr;
			if ($month < 1) {
				// $graph_stuff['series_data']['Closing_Balance'] = $closing_bal_series_conf;
				$graph_stuff['series_data']['Issued from KEMSA'] = $kemsa_issued_series_scr;
				// $graph_stuff['series_data']['Total'] = $total_rights_series_conf;
				// $graph_stuff['series_data']['Allocated'] = $allocated_series_scr;
				// $graph_stuff['series_data']['Used'] = $used_series_conf;
				$graph_stuff['series_data']['Received'] = $received_series_scr;
			}else{
				$graph_stuff['series_data']['Issued from KEMSA'] = $kemsa_issued_series_scr;
				// $graph_stuff['series_data']['Allocated'] = $allocated_series_scr;
				$graph_stuff['series_data']['Beginning Balance'] = $beginning_bal_scr;
				$graph_stuff['series_data']['Closing Balance'] = $closing_bal_scr;
				$graph_stuff['series_data']['Used'] = $used_series_scr;
				$graph_stuff['series_data']['Received'] = $received_series_scr;
			}
			

			$graph_stuff['div'] = "utilization_graph_scr";
			// echo "<pre>";print_r($graph_stuff);exit;

			$utilization_graph_scr_custom = $this->allocationvsreceivedvsconsumed_highchart($graph_stuff);//Karsan
			// echo "<pre>";print_r($utilization_graph_scr_custom);exit;

			$data['utilization_graph_scr'] = $utilization_graph_scr_custom;

			$graph_stuff = array();

			// $beginning_imploded = "'" . implode ( "', '", $beginning_final_['Screening'] ) . "'";
			// $used_imploded = "'" . implode ( "', '", $used_final_['Screening'] ) . "'";
			$counties_imploded_c = "'" . implode ( "', '", $gcategories_final_c) . "'";
			$kemsa_issued_series_conf =  implode ( "', '", $kemsa_issued_series_conf);
			$closing_bal_series_conf =  implode ( "', '", $closing_bal_series_conf);
			$total_rights_series_conf =  implode ( "', '", $total_rights_series_conf);
			$allocated_series_conf = implode ( "', '", $allocated_series_conf);
			$used_series_conf = implode ( "', '", $used_series_conf);
			$received_series_conf = implode ( "', '", $received_series_conf);
			// echo "<pre>";print_r($counties_imploded);exit;

			// echo "<pre>";print_r($utilization_category_imploded_conf);exit;
			$graph_stuff['categories'] = $utilization_category_imploded_conf;


			if ($month < 1) {
				// $graph_stuff['series_data']['Closing_Balance'] = $closing_bal_series_conf;
				$graph_stuff['series_data']['Issued from KEMSA'] = $kemsa_issued_series_conf;
				// $graph_stuff['series_data']['Total'] = $total_rights_series_conf;
				// $graph_stuff['series_data']['Allocated'] = $allocated_series_conf;
				// $graph_stuff['series_data']['Used'] = $used_series_conf;
				$graph_stuff['series_data']['Received'] = $received_series_conf;
			}else{
				$graph_stuff['series_data']['Issued from KEMSA'] = $kemsa_issued_series_conf;
				// $graph_stuff['series_data']['Allocated'] = $allocated_series_conf;
				$graph_stuff['series_data']['Beginning Balance'] = $beginning_bal_conf;
				$graph_stuff['series_data']['Closing Balance'] = $closing_bal_conf;
				$graph_stuff['series_data']['Used'] = $used_series_conf;
				$graph_stuff['series_data']['Received'] = $received_series_conf;
			}

			$graph_stuff['div'] = "utilization_graph_conf";
			// echo "<pre>";print_r($graph_stuff);exit;
			$utilization_graph_conf_custom = $this->allocationvsreceivedvsconsumed_highchart($graph_stuff);//Karsan
			// echo "<pre>";print_r($utilization_graph_conf_custom);exit;

			$data['utilization_graph_conf'] = $utilization_graph_conf_custom;

			// echo "<pre>";print_r($data['utilization_graph']);exit;
	        $drawing_aggregates = $this->drawing_rights_data($year, $month, $county_id, $district_id, $facility_code);

			// echo "<pre>";print_r($drawing_aggregates);exit;
			//sum_opening 
			//sum_used

			$beginning_series = $year_total = $distributed = $balance = $used_drawing_rights_series = $beginning_category = $used_drawing_rights_category = array();
			$gcategories_final_ = $gcategories_final_s = $gcategories_final_c = array();

	        usort($drawing_aggregates, function($a, $b) {
			    // return $b['screening_balance'] - $a['screening_balance'];
			    return $b['screening_distributed'] - $a['screening_distributed'];
			});

			foreach ($drawing_aggregates as $key => $value) {
					$bal_s = 0;
					$drawing_rights_quarter = $value['quarter'];
					$gcategories_final_s[] = $value['county'];

					$year_total["Screening"][] = $value['screening_year_total'];
					$distributed["Screening"][] = $value['screening_distributed'];
					$bal_s = $value['screening_year_total'] - $value['screening_distributed'];
					$bal_s = (isset($bal_s) && $bal_s > 0)? $bal_s:0;
					$balance["Screening"][] = $value['screening_balance'];
			}

			usort($drawing_aggregates, function($a, $b) {
			    // return $b['confirmatory_balance'] - $a['confirmatory_balance'];
			    return $b['confirmatory_distributed'] - $a['confirmatory_distributed'];
			});

			foreach ($drawing_aggregates as $key => $value) {
					$bal_c = 0;
					$gcategories_final_c[] = $value['county'];

					$year_total["Confirmatory"][] = $value['confirmatory_year_total'];
					$distributed["Confirmatory"][] = $value['confirmatory_distributed'];
					$bal_c = $value['confirmatory_year_total'] - $value['confirmatory_distributed'];
					$bal_c = (isset($bal_c) && $bal_c > 0)? $bal_c:0;
					$balance["Confirmatory"][] = $value['confirmatory_balance'];
			}

			$data['drawing_rights_quarter'] =$drawing_rights_quarter;

			// echo "<pre>";print_r($year_total);
			// echo "<pre>";print_r($distributed);
			// echo "<pre>";print_r($balance);
			// exit;

			$gcategories_final_s = array_unique($gcategories_final_s);
			$gcategories_final_c = array_unique($gcategories_final_c);
			// $utilization_category = array_merge($used_category, array("Screening","Confirmatory"));
			// $utilization_category = array_merge($used_category, $gcategories_final_);

			// echo "<pre>";print_r($gcategories_final_);exit;
			$counties = implode(",",$gcategories_final_s);
				// echo "<pre>";print_r($counties);exit;

			$beginning_series = array_merge($beginning_series, array("Screening beginning" => (int)$national_aggregates['final_closing_bal4']));
			$beginning_series = array_merge($beginning_series, array("Confirmatory beginning" => (int)$national_aggregates['final_closing_bal5']));

			$used_drawing_rights_series = array_merge($used_drawing_rights_series, array("Screening used" => (int)$national_aggregates['final_used4']));
			$used_drawing_rights_series = array_merge($used_drawing_rights_series, array("Confirmatory used" => (int)$national_aggregates['final_used5']));
			
			$year_total_imploded = implode(',', $year_total['Screening']);
			$balance_imploded = implode(',', $balance['Screening']);
			
			$distributed_imploded = implode(',', $distributed['Screening']);
			if ($county_id > 0) {
				$distributed_imploded = implode(',', $distributed_county['Screening']);
				$balance_imploded = implode(',', $balance_county['Screening']);
			}
			// $counties_imploded_s = implode(',', $gcategories_final_s);

			$graph_stuff = array();

			// $beginning_imploded = "'" . implode ( "', '", $beginning_final_['Screening'] ) . "'";
			// $used_imploded = "'" . implode ( "', '", $used_final_['Screening'] ) . "'";
			$counties_imploded_s = "'" . implode ( "', '", $gcategories_final_s) . "'";
			// echo "<pre>";print_r($counties_imploded);exit;

			// echo "<pre>";print_r($beginning_imploded);exit;
			$graph_stuff['categories'] = $counties_imploded_s;

			// $graph_stuff['series_data']['year_total'] = $year_total_imploded;
			$graph_stuff['series_data']['year_total'] = $balance_imploded;
			$graph_stuff['series_data']['distributed'] = $distributed_imploded;
			$graph_stuff['div'] = "utilization_bgg_screening_graph";

			$graph_custom = $this->drawing_rights_stacked_high_chart($graph_stuff);//Karsan
			$data['utilization_bgg_screening_graph'] = $graph_custom;

			$year_total_imploded = implode(',', $year_total['Confirmatory']);
			$balance_imploded = implode(',', $balance['Confirmatory']);
			$distributed_imploded = implode(',', $distributed['Confirmatory']);
			if ($county_id > 0) {
				$distributed_imploded = implode(',', $distributed_county['Confirmatory']);
				$balance_imploded = implode(',', $balance_county['Confirmatory']);
			}

			$counties_imploded = implode(',', $gcategories_final_);

			$graph_stuff = array();

			// $beginning_imploded = "'" . implode ( "', '", $beginning_final_['Screening'] ) . "'";
			// $used_imploded = "'" . implode ( "', '", $used_final_['Screening'] ) . "'";
			$counties_imploded_c = "'" . implode ( "', '", $gcategories_final_c) . "'";
			// echo "<pre>";print_r($counties_imploded);exit;

			// echo "<pre>";print_r($balance_imploded);exit;
			$graph_stuff['categories'] = $counties_imploded_c;

			// $graph_stuff['series_data']['year_total'] = $year_total_imploded;
			$graph_stuff['series_data']['year_total'] = $balance_imploded;
			$graph_stuff['series_data']['distributed'] = $distributed_imploded;
			$graph_stuff['div'] = "utilization_bgg_confirmatory_graph";

			$graph_custom_conf = $this->drawing_rights_stacked_high_chart($graph_stuff);//Karsan

			$data['utilization_bgg_confirmatory_graph'] = $graph_custom_conf;

			// echo "<pre>";print_r($used_imploded_conf);exit;

			// echo "<pre>";print_r($data['utilization_bgg_confirmatory_graph']);exit;

			$graph_type='column';
			$graph_data=array_merge($graph_data,array("graph_id"=>'test-graph'));
			$graph_data = array_merge($graph_data, array("color" => "['#4b0082', '#6AF9C4']"));
			$graph_data=array_merge($graph_data,array("graph_title"=>""));
			$graph_data=array_merge($graph_data,array("stacking"=>"normal"));
			$graph_data=array_merge($graph_data,array("data_labels"=>"white"));
			$graph_data=array_merge($graph_data,array("graph_type"=>$graph_type));
			$graph_data=array_merge($graph_data,array("graph_yaxis_title"=>""));
			$graph_data=array_merge($graph_data,array("graph_categories"=>$gcategories_final_ ));
			$graph_data=array_merge($graph_data,array("series_data"=>$graph_stuff_raw['series_data']));
			$util_conf_graph = $this->hcmp_functions->create_high_chart_graph($graph_data);

			// echo "<pre>";print_r($util_conf_graph);exit;
			$data['test_graph'] = $util_conf_graph;

			$graph_type='column';
			$graph_data=array_merge($graph_data,array("graph_id"=>'utilization_bg_graph'));
			$graph_data = array_merge($graph_data, array("color" => "['#4b0082', '#6AF9C4']"));
			$graph_data=array_merge($graph_data,array("graph_title"=>""));
			$graph_data=array_merge($graph_data,array("stacking"=>"normal"));
			$graph_data=array_merge($graph_data,array("data_labels"=>"white"));
			$graph_data=array_merge($graph_data,array("graph_type"=>$graph_type));
			$graph_data=array_merge($graph_data,array("graph_yaxis_title"=>""));
			$graph_data=array_merge($graph_data,array("graph_categories"=>$counties_imploded ));
			$graph_data=array_merge($graph_data,array("series_data"=>array('Balance'=>$beginning_final_['Screening'],'Used drawing rights'=>$used_final_['Screening'])));
			$data['utilization_bg_graph']= $this->hcmp_functions->create_high_chart_graph($graph_data);

        }
        // echo "<pre>";print_r($data);exit;
        // echo "<pre>";print_r($drawing_balance_confirmatory);exit;
		$data['counties'] = $county_name;
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;

		// echo "<pre>";print_r($data);exit;
		$counties = $this->db->query("SELECT * FROM counties")->result_array();
		$districts = $this->db->query("SELECT * FROM districts")->result_array();

		$data['tracer'] = $tracer;
		$data['commodity_division'] = isset($division)? $division :"NULL";
		// $title_append = "Sample filtered";

		$title_append .= (isset($filtered_name) && $filtered_name !='')? $filtered_name:NULL;
		$title_append .= (isset($filtered_date) && $filtered_date !="")? " ".date('F Y',strtotime($filtered_date)):NULL;

		if ($county_id > 0 || $month > 0) {
			$title_append .= " Report";
		}else{
			$title_append .= " as of ".date('F',strtotime("-1 MONTH"))." Report";
		}

		$title_prepend = (isset($county_id) && $county_id > 0)? 'County':'County';

		$title_append .= (isset($quarter) && $quarter > 0 && $month < 1)? ' Quarter: '.$quarter:NULL;

		// $title_append .= (isset($filtered_date) && $filtered_date !="")? ' Month: '.date('F Y',strtotime($filtered_date)):NULL;
		// echo "<pre>";print_r($title_append);exit;
		
		// echo "<pre>";print_r($filtered);exit;

		// $title_prepend = (isset($title_prepend) && $title_prepend !='')? $title_prepend:'National';

		$data['title_append'] = $title_append;
		$data['quartered'] = $quartered;
		$data['quarter'] = $quarter;
		$data['filtered'] = $filtered;
		$data['filtered_month'] = $month;
		$data['filtered_year'] = $year;
		$data['quarter_append'] = $quarter_append;
		// $data['tracer_commodities'] = $commodities;
		$data['facility_count'] = $facility_count;
		$data['commodity_count'] = $commodity_count;
		$data['content_view'] = 'dashboard/dashboard';
		$data['county_data'] = $counties;
		$data['title_prepend'] = $title_prepend;
		$data['county_count'] = count($counties_using_CD4);
		$data['district_data'] = $districts;
		$data['commodity_divisions'] = $commodity_divisions;
		$data['title'] = "County Dashboard";
		$data['county_data'] = $counties;
		$data['county_id'] = $county_id;
		$data['subcounty_id'] = $district_id;
		$data['district_data'] = $districts;
		$data['maps'] = $map;
		$data['counties'] = $county_name;

		// echo "<pre>";print_r($data);exit;
		$this -> load -> view("v2/dashboard/dashboard_county", $data);
	}

	public function subcounty_dashboard($county_id = NULL, $district_id = NULL,$quarter = NULL,$month_year = NULL,$year = NULL)
	{
		$page_caching = $this->output->cache(CACHE_REFRESH_INTERVAL);
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter." BY: ".$by;exit;
		$default = "tracer";//FYI
		// echo "<pre>";print_r($district_id);exit;
		// echo "<pre>";print_r($quarter);exit;
		// $map = $this->render_map();
		$county_id = (isset($county_id) && $county_id > 0)? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id > 0)? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code > 0)? $facility_code:NULL;
		$quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;
		$filtered = 0;
		//IMPLEMENTING: YEARLY FILTERS 
		$data['filter_years'] = $this->get_years_to_date();
		$month_year = (isset($month_year) && $month_year != 0 && $month_year !='')? $month_year:NULL;

		if (isset($month_year)) {
            // $year = substr($month, -4);
			$m_data = explode('_', $month_year);
			// echo "<pre>";print_r($m_data);exit;
            $month = $m_data[0];
            $year = $m_data[1];
            $monthyear = $year . '-' . $month . '-01';
			$filtered_date = $year."-".$month."-01";
        } else {
            /*$month = $this->session->userdata('Month');
            if ($month == '') {
                $month = date('mY', time());
            }
            $year = date('Y');
            $month = substr_replace($month, "", -4);
            $monthyear = $year . '-' . $month . '-01';*/
        }

        if ($year > 0) {
			$filtered = 1;
			$quarter_append = ' <span class="font-blue-steel">*</span>';
			$data['filtered_year'] = $year;
		}else{
			$year = date('Y');
		}

        // if ($county_id > 0 || $quarter > 0) {
		if ($district_id > 0) {
			$filtered = 1;
			// $quarter_append = ' <span class="font-blue-steel">*</span>';
		}else{
			// $filtered = 0;
		}
		$data['filtered'] = $filtered;

		if ($quarter > 0 && $month < 1) {
        	$curQuarter = $quarter;
        	
        }else{
        	$quarter_data = $this->get_year_quarter($month,NULL,$year,NULL);
        	$curQuarter = $quarter_data['quarter'];
        }
        
		$data['drawing_quarters'] = $curQuarter;
		// echo "<pre>";print_r($data);exit;
        $current_date = date('Y-m-d');
		$data['filter_months'] = $this->get_months_between_dates();

        $append_date = (isset($append_date) && $append_date !="")? $append_date : date('Y-m-d');
		
		$commodity_count = Dashboard_model::get_commodity_count();
		// $commodities = Dashboard_model::get_division_commodities($division);
		$page_title = "RTK Dashboard.";
		$data['page_title'] = $page_title;
		// echo "<pre>";print_r($commodities);exit;
		$commodity_divisions = Dashboard_model::get_division_details();
		$counties = $this->db->query("SELECT * FROM counties")->result_array();
		$districts = $this->db->query("SELECT * FROM districts")->result_array();

		$facility_count = Dashboard_model::get_online_offline_facility_count();
		$counties_using_CD4 = Counties::get_counties_all_using_CD4();

		if ($filtered == 0) {

			$district_drawing_data_s = $district_drawing_data_c = $district_drawing_data_ss = $district_drawing_data_cc = array();

	        $balances_scr = $this->get_opening_and_closing_balances(NULL,NULL ,NULL, 4 , $beg_date, $end_date, $quarter,"subcounty",$month,$year);
	        $balances_conf = $this->get_opening_and_closing_balances(NULL,NULL ,NULL, 5 , $beg_date, $end_date, $quarter,"subcounty",$month,$year);
	        // echo "<pre>";print_r($balances_scr);exit;
	        $d_order_data_s = $this->get_county_order_data(NULL, $d_id, NULL, 4 , $quarter,$month,$year,"subcounty");
	        // echo "<pre>";print_r($d_order_data_s);exit;
	        $d_order_data_c = $this->get_county_order_data(NULL, $d_id, NULL, 5 , $quarter,$month,$year,"subcounty");


	        $kemsa_issue_data_s = $this->get_kemsa_issue_data($county_id, $district_id, $facility_code, 4 , $quarter,$year,$month,"subcounty");
	        // echo "<pre>";print_r($kemsa_issue_data_s);exit;
	        $kemsa_issue_data_c = $this->get_kemsa_issue_data($county_id, $district_id, $facility_code, 5 , $quarter,$year,$month,"subcounty");

	        $district_data = $this->get_district_data($county_id,$district_id);
	        // echo "<pre>";print_r($district_data);exit;
	        foreach ($district_data as $key => $value) {
	        	// echo "<pre>";print_r($value);exit;
	        	$c_id = $value['county_id'];
	        	$c_name = $value['county_name'];
	        	$d_id = $value['id'];
	        	$d_name = $value['district'];

	        	
				// $d_kemsa_issues = $this->kemsa_issue_data($year, $month, NULL, $d_id, NULL, $quarter);
				// echo "<pre>";print_r($d_order_data_s);exit;

				$district_drawing_data_ss['county_id'] = $c_id;
				$district_drawing_data_ss['county_name'] = $c_name;
				$district_drawing_data_ss['district_id'] = $d_id;
				$district_drawing_data_ss['district_name'] = $d_name;
				$district_drawing_data_ss['allocated'] = $scr_dist;
				$district_drawing_data_ss['issued_kemsa'] = $kemsa_issue_data_s['kemsa_issue_data'][$d_id]['qty_issued'];
				// $district_drawing_data_ss['beg_bal'] = $d_order_data_s['results'][$d_id]['beginning_bal'];
				$district_drawing_data_ss['beg_bal'] = $balances_scr[$d_id]['beginning_balance'];

				$district_drawing_data_ss['q_rec'] = $d_order_data_s['results'][$d_id]['q_received'];
				$district_drawing_data_ss['q_used'] = $d_order_data_s['results'][$d_id]['q_used'];
				$district_drawing_data_ss['tests_done'] = $d_order_data_s['results'][$d_id]['no_of_tests_done'];
				$district_drawing_data_ss['losses'] = $d_order_data_s['results'][$d_id]['losses'];
				$district_drawing_data_ss['positive_adj'] = $d_order_data_s['results'][$d_id]['positive_adj'];
				$district_drawing_data_ss['negative_adj'] = $d_order_data_s['results'][$d_id]['negative_adj'];
				// $district_drawing_data_ss['closing_bal'] = $d_order_data_s['results'][$d_id]['closing_stock'];
				$district_drawing_data_ss['closing_bal'] = $balances_scr[$d_id]['closing_balance'];

				$district_drawing_data_cc['county_id'] = $c_id;
				$district_drawing_data_cc['county_name'] = $c_name;
				$district_drawing_data_cc['district_id'] = $d_id;
				$district_drawing_data_cc['district_name'] = $d_name;
				$district_drawing_data_cc['allocated'] = $conf_dist;
				$district_drawing_data_cc['issued_kemsa'] = $kemsa_issue_data_c['kemsa_issue_data'][$d_id]['qty_issued'];
				// $district_drawing_data_cc['beg_bal'] = $d_order_data_c['results'][$d_id]['beginning_bal'];
				$district_drawing_data_cc['beg_bal'] = $balances_conf[$d_id]['beginning_balance'];

				$district_drawing_data_cc['q_rec'] = $d_order_data_c['results'][$d_id]['q_received'];
				$district_drawing_data_cc['q_used'] = $d_order_data_c['results'][$d_id]['q_used'];
				$district_drawing_data_cc['tests_done'] = $d_order_data_c['results'][$d_id]['no_of_tests_done'];
				$district_drawing_data_cc['losses'] = $d_order_data_c['results'][$d_id]['losses'];
				$district_drawing_data_cc['positive_adj'] = $d_order_data_c['results'][$d_id]['positive_adj'];
				$district_drawing_data_cc['negative_adj'] = $d_order_data_c['results'][$d_id]['negative_adj'];
				// $district_drawing_data_cc['closing_bal'] = $d_order_data_c['results'][$d_id]['closing_stock'];
				$district_drawing_data_cc['closing_bal'] = $balances_conf[$d_id]['closing_balance'];

				array_push($district_drawing_data_s, $district_drawing_data_ss);
				array_push($district_drawing_data_c, $district_drawing_data_cc);

	        	// echo "<pre>";print_r($order_data_s);
	        	// echo "<pre>";print_r($kemsa_issues);exit;
	        }

	        // echo "<pre>";print_r($district_drawing_data_s);exit;
	        $data['district_drawing_data_s'] = $district_drawing_data_s;
	        $data['district_drawing_data_c'] = $district_drawing_data_c;

		}else{

			$dhis_data = $this->get_dhis_data($county_id,$district_id,$facility_code,$quarter,$month,$year);//KARSAN
			// echo "<pre>";print_r($dhis_data);exit;

			$data['dhis_screening_tests_done'] = $dhis_data[4]['tests_done'];
			$data['dhis_confirmatory_tests_done'] = $dhis_data[5]['tests_done'];

			$national_aggregates = $this->national_aggregate($county_id,$district_id,$facility_code,$quarter,$month,$year);//KARSAN

			// echo "<pre>AGGR: ";print_r($national_aggregates);exit;

			$data['screening_tests_done'] = $national_aggregates['final_tests4'];
			$data['confirmatory_tests_done'] = $national_aggregates['final_tests5'];
			// echo "<pre>";print_r($data);exit;
			$summary_data_s = $this->get_county_order_data(NULL, $district_id, NULL, 4 , $quarter,$month,$year);
	        // echo "<pre>";print_r($summary_data_s);exit;
	        $summary_data_c = $this->get_county_order_data(NULL, $district_id, NULL, 5 , $quarter,$month,$year);

	        $data['summary_data_date'] = $summary_data_s['results'][0]['order_date'];

	        $received_site_scr = $summary_data_s['results'][0]['q_received'];
	        $received_site_conf = $summary_data_c['results'][0]['q_received'];

	        $data['received_site_screening'] = $received_site_scr;
	        $data['received_site_confirmatory'] = $received_site_conf;

	        $consumed_site_scr = $summary_data_s['results'][0]['q_used'];
	        $consumed_site_conf = $summary_data_c['results'][0]['q_used'];

	        $data['consumed_site_screening'] = $consumed_site_scr;
	        $data['consumed_site_confirmatory'] = $consumed_site_conf;

			$graph_type = $graph_data = $category_data = $series_data = array();
			$category_data[] = "Screening";
			$category_data[] = "Confirmatory";
			$screening = array("10","20");
			// echo "<pre>";print_r($screening);exit;
			$series_data['Screening'] = [10];
			$series_data['Confirmatory'] = [20];

			$filtered_name = 	NULL;
			$district_data = (isset($district_id) && ($district_id > 0)) ? districts::get_district_name($district_id) -> toArray() : NULL;
			// echo "<pre>";print_r($district_data);exit;

			$county_search = counties::get_county_name($county_id);
			if (count($county_search) > 0) {
				// $filtered_name = $county_search['county']." County $append_date";
				$filtered_name = $county_search['county']." County";
			}
			
			if ($district_data != NULL) {
				// $filtered_name = $district_data[0]['district']." Sub-County $append_date";
				$filtered_name = $district_data[0]['district']." Sub-County";
			}

			// echo "<pre>";print_r($county_name);exit;

			$kemsa_issue_data_s = $this->get_kemsa_issue_data($county_id, $district_id, $facility_code, 4 , $quarter,$year,$month,"subcounty");
	        // echo "<pre>";print_r($kemsa_issue_data_s);exit;
	        $kemsa_issue_data_c = $this->get_kemsa_issue_data($county_id, $district_id, $facility_code, 5 , $quarter,$year,$month,"subcounty");

	        $kemsa_issued_screening = $kemsa_issue_data_s[$district_id]['qty_issued'];
	        $kemsa_issued_confirmatory = $kemsa_issue_data_c[$district_id]['qty_issued'];
			$data['kemsa_issue_date'] = $kemsa_issue_data_s[$district_id]['issue_date'];
			$data['kemsa_issued_screening'] = $kemsa_issue_data_s[$district_id]['qty_issued'];
			$data['kemsa_issued_confirmatory'] = $kemsa_issue_data_c[$district_id]['qty_issued'];
			// echo "<pre>";print_r($data);exit;

			$county_name = "";

			$summary_and = "";
	        $summary_and .= (isset($district_id) && $district_id!='')? " WHERE district_id = $district_id" :NULL; 
	        
	        $d_drawing_query = "SELECT * FROM district_drawing_rights $summary_and";
			// echo $c_drawing_query;exit;
	        $district_drawing_data = $this->db->query($d_drawing_query)->result_array();

	        // echo "<pre>";print_r($district_drawing_data);exit;
	    	$drawing_total_confirmatory = $drawing_total_screening = 0;
	    	$drawing_used_confirmatory = $drawing_used_screening = 0;
	    	$drawing_balance_confirmatory = $drawing_balance_screening = 0;
	    	$county_scr_distributed_aggr = $county_conf_distributed_aggr = 0;
	    	$county_scr_total_drawing_rights = $county_conf_total_drawing_rights = 0;
	        // echo "<pre>";print_r($curQuarter);exit;

	        // echo "<pre>";print_r($district_drawing_data);exit;
	        
	        // echo "<pre>";print_r($sth);exit;
	        foreach ($district_drawing_data as $key => $value) {
	        	// echo "<pre>";print_r($value);exit;
	        	// $drawing_rights_data_['quarter'] = $curQuarter;
	        	$screening_total = $confirmatory_total = 0;
	        	// $c_id = $value['county_id'];
	        	// $screening_total = $value['screening_total']*$curQuarter;
	        	// $confirmatory_total = $value['confirmatory_total']*$curQuarter;
	        	$screening_total = $value['screening_allocated'];
	        	$confirmatory_total = $value['confirmatory_allocated'];

	        	$screening_used = $value['screening_used'];
	        	$confirmatory_used = $value['confirmatory_used'];

	        	// $screening_distributed = $value['screening_distributed'];
	        	// $confirmatory_distributed = $value['confirmatory_distributed'];

	        	// echo "<pre>";print_r($value['screening_used']);exit;
	        	$screening_balance = $screening_total - $value['screening_used'];
	        	$confirmatory_balance = $confirmatory_total - $value['confirmatory_used'];

				$screening_balance = (isset($screening_balance) && $screening_balance > 0)? $screening_balance:0;
				$confirmatory_balance = (isset($confirmatory_balance) && $confirmatory_balance > 0)? $confirmatory_balance:0;


				if (count($district_drawing_data) > 0) {
					$district_scr_total_drawing_rights += $screening_total;
					$district_conf_total_drawing_rights += $confirmatory_total;

					$district_scr_used += $screening_used;
					$district_conf_used += $confirmatory_used;

					$district_scr_distributed_aggr += $screening_distributed;
					$district_conf_distributed_aggr += $confirmatory_distributed;

		        	$data['drawing_total_screening'] += $screening_total;
		        	$data['drawing_total_confirmatory'] += $confirmatory_total;

		        	$data['drawing_used_screening'] += $screening_used;
		        	$data['drawing_used_confirmatory'] += $confirmatory_used;

	        	}else{
					$district_scr_total_drawing_rights = $screening_total;
					$district_conf_total_drawing_rights = $confirmatory_total;

					$district_scr_distributed_aggr = $screening_distributed;
					$district_conf_distributed_aggr = $confirmatory_distributed;

	        		$data['drawing_total_screening'] = $screening_total;
		        	$data['drawing_total_confirmatory'] = $confirmatory_total;

		        	$data['drawing_used_screening'] = $screening_used;
		        	$data['drawing_used_confirmatory'] = $confirmatory_used;

		        	$data['drawing_distributed_screening'] = $screening_distributed;
		        	$data['drawing_distributed_confirmatory'] = $confirmatory_distributed;

	        	}

	        	// echo "<pre>";print_r($confirmatory_total);exit;
	        }

	        if ($district_id > 0) {
		        /*DOC: START OF COUNTY ALLOCATION DATA FROM EXCEL SHEETS*/
		        $d_alloc_data = $this->get_county_allocation_data($county_id, $district_id, $month, $quarter,$year);
		        // echo "<pre>";print_r($d_alloc_data);exit;

		        $data['total_allocated_screening'] = $d_alloc_data[0]['allocate_s'];
		        $data['total_allocated_confirmatory'] = $d_alloc_data[0]['allocate_c'];

		        $district_scr_distributed_aggr = $d_alloc_data[0]['allocate_s'];
				$district_conf_distributed_aggr = $d_alloc_data[0]['allocate_c'];

				$distributed_district["Screening"][] = $district_scr_distributed_aggr;
				$distributed_district["Confirmatory"][] = $district_conf_distributed_aggr;

				$balance_district['Screening'][] = $district_scr_total_drawing_rights - $district_scr_distributed_aggr;
				$balance_district['Confirmatory'][] = $district_conf_total_drawing_rights - $district_conf_distributed_aggr;
		        /*DOC: END OF COUNTY ALLOCATION DATA FROM EXCEL SHEETS*/
	        }else{
	        	$data['total_allocated_screening'] = $district_scr_distributed_aggr;
	        	$data['total_allocated_confirmatory'] = $district_conf_distributed_aggr;
	        }

	        $data['drawing_balance_screening'] = $district_scr_total_drawing_rights - $district_scr_distributed_aggr;
	        $data['drawing_balance_confirmatory'] = $district_conf_total_drawing_rights - $district_conf_distributed_aggr;

	        // echo "<pre>";print_r($result);exit;
	        $facility_drawing_data_s = $facility_drawing_data_c = $facility_drawing_data_ss = $facility_drawing_data_cc = array();
	        // $districts = $result = $this->db->query($)->result_array();
	        $facility_data = $this->get_facility_data($county_id,$district_id,$facility_code);

	        $f_o_data_s = $this->get_facility_order_data(NULL, $district_id,NULL, 4 , $quarter,$month,$year);
	        $f_o_data_c = $this->get_facility_order_data(NULL, $district_id,NULL, 5 , $quarter,$month,$year);
	        // echo "<pre>";print_r($f_o_data_s);exit;
	        $balances_scr = $this->get_opening_and_closing_balances(NULL,$district_id ,NULL, 4 , $beg_date, $end_date, $quarter,"facility",$month,$year);
	        $balances_conf = $this->get_opening_and_closing_balances(NULL,$district_id ,NULL, 5 , $beg_date, $end_date, $quarter,"facility",$month,$year);

	        // echo "<pre>";print_r($balances_scr);exit;
	        // echo "<pre>";print_r($f_o_data_s);exit;
	        foreach ($f_o_data_s as $key => $value) {
	        	// echo "<pre>";print_r($value);exit;
	        	$c_id = $value['county_id'];
	        	$c_name = $value['county'];
	        	$d_id = $value['district_id'];
	        	$d_name = $value['district'];
	        	$f_code = $value['facility_code'];
	        	$f_name = $value['facility_name'];

	        	// $balances_scr = $this->get_opening_and_closing_balances(NULL, NULL,$f_code, 4 , $beg_date, $end_date, $quarter);
	        	// $balances_conf = $this->get_opening_and_closing_balances(NULL, NULL,$f_code, 5 , $beg_date, $end_date, $quarter);

				$facility_drawing_data_ss['county_id'] = $c_id;
				$facility_drawing_data_ss['county_name'] = $c_name;
				$facility_drawing_data_ss['district_id'] = $d_id;
				$facility_drawing_data_ss['district_name'] = $d_name;
				$facility_drawing_data_ss['facility_code'] = $f_code;
				$facility_drawing_data_ss['facility_name'] = $f_name;
				$facility_drawing_data_ss['allocated'] = $scr_dist;
				// $facility_drawing_data_ss['issued_kemsa'] = $f_kemsa_issues[0]['screening_units'];
				// $facility_drawing_data_ss['beg_bal'] = $value['beginning_bal'];
				$facility_drawing_data_ss['beg_bal'] = $balances_scr[$f_code]['beginning_balance'];

				$facility_drawing_data_ss['q_rec'] = $value['q_received'];
				$facility_drawing_data_ss['q_used'] = $value['q_used'];
				$facility_drawing_data_ss['tests_done'] = $value['no_of_tests_done'];
				$facility_drawing_data_ss['losses'] = $value['losses'];
				$facility_drawing_data_ss['positive_adj'] = $value['positive_adj'];
				$facility_drawing_data_ss['negative_adj'] = $value['negative_adj'];
				// $facility_drawing_data_ss['closing_bal'] = $value['closing_stock'];
				$facility_drawing_data_ss['closing_bal'] = $balances_scr[$f_code]['closing_balance'];

	        	// echo "<pre>";print_r($facility_drawing_data_ss);exit;
				array_push($facility_drawing_data_s, $facility_drawing_data_ss);
	        }
	        // echo "<pre>";print_r($facility_drawing_data_s);exit;

	        foreach ($f_o_data_c as $key => $value) {
	        	// echo "<pre>";print_r($value);exit;
	        	$c_id = $value['county_id'];
	        	$c_name = $value['county'];
	        	$d_id = $value['district_id'];
	        	$d_name = $value['district'];
	        	$f_code = $value['facility_code'];
	        	$f_name = $value['facility_name'];

	        	// $balances_scr = $this->get_opening_and_closing_balances(NULL, NULL,$f_code, 4 , $beg_date, $end_date, $quarter);
	        	// $balances_conf = $this->get_opening_and_closing_balances(NULL, NULL,$f_code, 5 , $beg_date, $end_date, $quarter);

				$facility_drawing_data_cc['county_id'] = $c_id;
				$facility_drawing_data_cc['county_name'] = $c_name;
				$facility_drawing_data_cc['district_id'] = $d_id;
				$facility_drawing_data_cc['district_name'] = $d_name;
				$facility_drawing_data_cc['facility_code'] = $f_code;
				$facility_drawing_data_cc['facility_name'] = $f_name;
				$facility_drawing_data_cc['allocated'] = $conf_dist;
				// $facility_drawing_data_cc['issued_kemsa'] = $f_kemsa_issues[0]['confirmatory_units'];
				// $facility_drawing_data_cc['beg_bal'] = $value['beginning_bal'];
				$facility_drawing_data_cc['beg_bal'] = $balances_conf[$f_code]['beginning_balance'];

				$facility_drawing_data_cc['q_rec'] = $value['q_received'];
				$facility_drawing_data_cc['q_used'] = $value['q_used'];
				$facility_drawing_data_cc['tests_done'] = $value['no_of_tests_done'];
				$facility_drawing_data_cc['losses'] = $value['losses'];
				$facility_drawing_data_cc['positive_adj'] = $value['positive_adj'];
				$facility_drawing_data_cc['negative_adj'] = $value['negative_adj'];
				// $facility_drawing_data_cc['closing_bal'] = $value['closing_stock'];
				$facility_drawing_data_cc['closing_bal'] = $balances_conf[$f_code]['closing_balance'];


				array_push($facility_drawing_data_c, $facility_drawing_data_cc);
	        }

	        $data['facility_drawing_data_s'] = $facility_drawing_data_s;
	        $data['facility_drawing_data_c'] = $facility_drawing_data_c;
	        // echo "<pre>";print_r($facility_drawing_data_c);exit;

	        $balances_scr = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 4 , $beg_date, $end_date, $quarter,NULL,$month,$year);
	        $balances_conf = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 5 , $beg_date, $end_date, $quarter,NULL,$month,$year);
	        // echo "<pre>";print_r($balances_scr);exit;
	        // $closing_bal_scr = $summary_data_s['results'][0]['beginning_bal'];
	        // $closing_bal_conf = $summary_data_c['results'][0]['beginning_bal'];

	        $closing_bal_scr = $balances_scr['closing_balance']['closing_balance'];
	        $closing_bal_conf = $balances_conf['closing_balance']['closing_balance'];
	        $closing_bal_order_date = $balances_scr['closing_balance']['order_date'];

	        $beginning_bal_scr = $balances_scr['beginning_balance']['beginning_balance'];
	        $beginning_bal_conf = $balances_conf['beginning_balance']['beginning_balance'];
	        $beginning_bal_order_date = $balances_scr['beginning_balance']['order_date'];

	        $data['beginning_balance_screening'] = $beginning_bal_scr;
	        $data['beginning_balance_confirmatory'] = $beginning_bal_conf;
	        $data['beginning_balance_order_date'] = $beginning_bal_order_date;

	        $data['closing_balance_screening'] = $closing_bal_scr;
	        $data['closing_balance_confirmatory'] = $closing_bal_conf;
	        $data['closing_balance_order_date'] = $closing_bal_order_date;

	        // echo "<pre>";print_r($data);exit;
	        // echo "<pre>";print_r($c_alloc_data);exit;
	  		//$county_scr_closing_balance = $national_aggregates['final_closing_bal4'];
			//$county_conf_closing_balance = $national_aggregates['final_closing_bal5'];

			$county_scr_closing_balance = $closing_bal_scr;
			$county_conf_closing_balance = $closing_bal_conf;

			// echo "<pre>";print_r($county_scr_total_drawing_rights);exit;

			$used_series_scr = $used_series_conf = $allocated_series_scr = $allocated_series_conf = array();
			$received_series_scr = $received_series_conf = $used_category_scr = $used_category_conf = array();
			$received_category_scr = $received_category_conf = array();
			$total_rights_series_scr = $total_rights_series_conf = array();
			$closing_bal_series_scr = $closing_bal_series_conf = array();
			$kemsa_issued_series_scr = $kemsa_issued_series_conf = array();

			$total_rights_series_scr = array_merge($total_rights_series_scr, array("Total" => (int)$county_scr_total_drawing_rights));
			$total_rights_series_conf = array_merge($total_rights_series_conf, array("Total" => (int)$county_conf_total_drawing_rights));

			$kemsa_issued_series_scr = array_merge($kemsa_issued_series_scr, array("Issued from KEMSA" => (int)$kemsa_issued_screening));
			$kemsa_issued_series_conf = array_merge($kemsa_issued_series_conf, array("Issued from KEMSA" => (int)$kemsa_issued_confirmatory));

			$closing_bal_series_scr = array_merge($closing_bal_series_scr, array("Closing Balance" => (int)$county_scr_closing_balance));
			$closing_bal_series_conf = array_merge($closing_bal_series_conf, array("Closing Balance" => (int)$county_conf_closing_balance));

			$allocated_series_scr = array_merge($allocated_series_scr, array("Allocated" => (int)$county_scr_distributed_aggr));
			$allocated_series_conf = array_merge($allocated_series_conf, array("Allocated" => (int)$county_conf_distributed_aggr));

			$used_series_scr = array_merge($used_series_scr, array("Used" => (int)$consumed_site_scr));
			$used_series_conf = array_merge($used_series_scr, array("Used" => (int)$consumed_site_conf));

			$received_series_scr = array_merge($received_series_scr, array("Received" => (int)$received_site_scr));
			$received_series_conf = array_merge($received_series_conf, array("Received" => (int)$received_site_conf));

			// echo "<pre>";print_r($total_rights_series_scr);exit;
			// $utilization_category = array_merge($used_category, array("Screening","Confirmatory"));
			$utilization_category_scr[] = "Screening";
			$utilization_category_conf[] = "Confirmatory";
			$utilization_category_imploded_scr = "'" . implode ( "', '", $utilization_category_scr) . "'";
			$utilization_category_imploded_conf = "'" . implode ( "', '", $utilization_category_conf) . "'";

			$graph_stuff = array();

			// $beginning_imploded = "'" . implode ( "', '", $beginning_final_['Screening'] ) . "'";
			// $used_imploded = "'" . implode ( "', '", $used_final_['Screening'] ) . "'";
			$counties_imploded_c = "'" . implode ( "', '", $gcategories_final_c) . "'";

			$closing_bal_series_scr =  implode ( "', '", $closing_bal_series_scr);
			$total_rights_series_scr =  implode ( "', '", $total_rights_series_scr);
			$kemsa_issued_series_scr =  implode ( "', '", $kemsa_issued_series_scr);
			$allocated_series_scr = implode ( "', '", $allocated_series_scr);
			$used_series_scr = implode ( "', '", $used_series_scr);
			$received_series_scr = implode ( "', '", $received_series_scr);
			// echo "<pre>";print_r($closing_bal_series_scr);exit;

			// echo "<pre>";print_r($utilization_category_imploded_scr);exit;
			$graph_stuff['categories'] = $utilization_category_imploded_scr;

			if ($month < 1) {
				// $graph_stuff['series_data']['Closing_Balance'] = $closing_bal_series_conf;
				$graph_stuff['series_data']['Issued from KEMSA'] = $kemsa_issued_series_scr;
				// $graph_stuff['series_data']['Total'] = $total_rights_series_conf;
				// $graph_stuff['series_data']['Allocated'] = $allocated_series_scr;
				// $graph_stuff['series_data']['Used'] = $used_series_conf;
				$graph_stuff['series_data']['Received'] = $received_series_scr;
			}else{
				$graph_stuff['series_data']['Issued from KEMSA'] = $kemsa_issued_series_scr;
				// $graph_stuff['series_data']['Allocated'] = $allocated_series_scr;
				$graph_stuff['series_data']['Beginning Balance'] = $beginning_bal_scr;
				$graph_stuff['series_data']['Closing Balance'] = $closing_bal_scr;
				$graph_stuff['series_data']['Used'] = $used_series_scr;
				$graph_stuff['series_data']['Received'] = $received_series_scr;
			}


			$graph_stuff['div'] = "utilization_graph_scr";
			// echo "<pre>";print_r($graph_stuff);exit;

			$utilization_graph_scr_custom = $this->allocationvsreceivedvsconsumed_highchart($graph_stuff);//Karsan
			// echo "<pre>";print_r($utilization_graph_scr_custom);exit;

			$data['utilization_graph_scr'] = $utilization_graph_scr_custom;

			$graph_stuff = array();

			// $beginning_imploded = "'" . implode ( "', '", $beginning_final_['Screening'] ) . "'";
			// $used_imploded = "'" . implode ( "', '", $used_final_['Screening'] ) . "'";
			$counties_imploded_c = "'" . implode ( "', '", $gcategories_final_c) . "'";
			$kemsa_issued_series_conf =  implode ( "', '", $kemsa_issued_series_conf);
			$closing_bal_series_conf =  implode ( "', '", $closing_bal_series_conf);
			$total_rights_series_conf =  implode ( "', '", $total_rights_series_conf);
			$allocated_series_conf = implode ( "', '", $allocated_series_conf);
			$used_series_conf = implode ( "', '", $used_series_conf);
			$received_series_conf = implode ( "', '", $received_series_conf);
			// echo "<pre>";print_r($counties_imploded);exit;

			// echo "<pre>";print_r($utilization_category_imploded_conf);exit;
			$graph_stuff['categories'] = $utilization_category_imploded_conf;


			if ($month < 1) {
				// $graph_stuff['series_data']['Closing_Balance'] = $closing_bal_series_conf;
				$graph_stuff['series_data']['Issued from KEMSA'] = $kemsa_issued_series_conf;
				// $graph_stuff['series_data']['Total'] = $total_rights_series_conf;
				// $graph_stuff['series_data']['Allocated'] = $allocated_series_conf;
				// $graph_stuff['series_data']['Used'] = $used_series_conf;
				$graph_stuff['series_data']['Received'] = $received_series_conf;
			}else{
				$graph_stuff['series_data']['Issued from KEMSA'] = $kemsa_issued_series_conf;
				// $graph_stuff['series_data']['Allocated'] = $allocated_series_conf;
				$graph_stuff['series_data']['Beginning Balance'] = $beginning_bal_conf;
				$graph_stuff['series_data']['Closing Balance'] = $closing_bal_conf;
				$graph_stuff['series_data']['Used'] = $used_series_conf;
				$graph_stuff['series_data']['Received'] = $received_series_conf;
			}

			$graph_stuff['div'] = "utilization_graph_conf";
			// echo "<pre>";print_r($graph_stuff);exit;
			$utilization_graph_conf_custom = $this->allocationvsreceivedvsconsumed_highchart($graph_stuff);//Karsan
			// echo "<pre>";print_r($utilization_graph_conf_custom);exit;

			$data['utilization_graph_conf'] = $utilization_graph_conf_custom;

			// echo "<pre>";print_r($data['utilization_graph']);exit;
			// $county_aggregates = $this->drawing_rights_data($county_id,$district_id);
			// echo $year." ".$month;exit;
			$county_aggregates = $this->national_stockcard_v2($county_id,$district_id);
	        $drawing_aggregates = $this->drawing_rights_data($year, $month, $county_id, $district_id, $facility_code);

			// echo "<pre>";print_r($drawing_aggregates);exit;
			//sum_opening 
			//sum_used

			$beginning_series = $year_total = $distributed = $balance = $used_drawing_rights_series = $beginning_category = $used_drawing_rights_category = array();
			$gcategories_final_ = $gcategories_final_s = $gcategories_final_c = array();

	        usort($drawing_aggregates, function($a, $b) {
			    // return $b['screening_balance'] - $a['screening_balance'];
			    return $b['screening_distributed'] - $a['screening_distributed'];
			});

			foreach ($drawing_aggregates as $key => $value) {
					$bal_s = 0;
					$drawing_rights_quarter = $value['quarter'];
					$gcategories_final_s[] = $value['county'];

					$year_total["Screening"][] = $value['screening_year_total'];
					$distributed["Screening"][] = $value['screening_distributed'];
					$bal_s = $value['screening_year_total'] - $value['screening_distributed'];
					$bal_s = (isset($bal_s) && $bal_s > 0)? $bal_s:0;
					$balance["Screening"][] = $value['screening_balance'];
			}

			usort($drawing_aggregates, function($a, $b) {
			    // return $b['confirmatory_balance'] - $a['confirmatory_balance'];
			    return $b['confirmatory_distributed'] - $a['confirmatory_distributed'];
			});

			foreach ($drawing_aggregates as $key => $value) {
					$bal_c = 0;
					$gcategories_final_c[] = $value['county'];

					$year_total["Confirmatory"][] = $value['confirmatory_year_total'];
					$distributed["Confirmatory"][] = $value['confirmatory_distributed'];
					$bal_c = $value['confirmatory_year_total'] - $value['confirmatory_distributed'];
					$bal_c = (isset($bal_c) && $bal_c > 0)? $bal_c:0;
					$balance["Confirmatory"][] = $value['confirmatory_balance'];
			}

			$data['drawing_rights_quarter'] =$drawing_rights_quarter;

			// echo "<pre>";print_r($year_total);
			// echo "<pre>";print_r($distributed);
			// echo "<pre>";print_r($balance);
			// exit;

			$gcategories_final_s = array_unique($gcategories_final_s);
			$gcategories_final_c = array_unique($gcategories_final_c);
			// $utilization_category = array_merge($used_category, array("Screening","Confirmatory"));
			// $utilization_category = array_merge($used_category, $gcategories_final_);

			// echo "<pre>";print_r($gcategories_final_);exit;
			$counties = implode(",",$gcategories_final_s);
				// echo "<pre>";print_r($counties);exit;

			$beginning_series = array_merge($beginning_series, array("Screening beginning" => (int)$national_aggregates['final_closing_bal4']));
			$beginning_series = array_merge($beginning_series, array("Confirmatory beginning" => (int)$national_aggregates['final_closing_bal5']));

			$used_drawing_rights_series = array_merge($used_drawing_rights_series, array("Screening used" => (int)$national_aggregates['final_used4']));
			$used_drawing_rights_series = array_merge($used_drawing_rights_series, array("Confirmatory used" => (int)$national_aggregates['final_used5']));
			
			$year_total_imploded = implode(',', $year_total['Screening']);
			$balance_imploded = implode(',', $balance['Screening']);
			
			$distributed_imploded = implode(',', $distributed['Screening']);
			if ($county_id > 0) {
				$distributed_imploded = implode(',', $distributed_county['Screening']);
				$balance_imploded = implode(',', $balance_county['Screening']);
			}
			// $counties_imploded_s = implode(',', $gcategories_final_s);

			$graph_stuff = array();

			// $beginning_imploded = "'" . implode ( "', '", $beginning_final_['Screening'] ) . "'";
			// $used_imploded = "'" . implode ( "', '", $used_final_['Screening'] ) . "'";
			$counties_imploded_s = "'" . implode ( "', '", $gcategories_final_s) . "'";
			// echo "<pre>";print_r($counties_imploded);exit;

			// echo "<pre>";print_r($beginning_imploded);exit;
			$graph_stuff['categories'] = $counties_imploded_s;

			// $graph_stuff['series_data']['year_total'] = $year_total_imploded;
			$graph_stuff['series_data']['year_total'] = $balance_imploded;
			$graph_stuff['series_data']['distributed'] = $distributed_imploded;
			$graph_stuff['div'] = "utilization_bgg_screening_graph";

			$graph_custom = $this->drawing_rights_stacked_high_chart($graph_stuff);//Karsan
			$data['utilization_bgg_screening_graph'] = $graph_custom;

			$year_total_imploded = implode(',', $year_total['Confirmatory']);
			$balance_imploded = implode(',', $balance['Confirmatory']);
			$distributed_imploded = implode(',', $distributed['Confirmatory']);
			if ($county_id > 0) {
				$distributed_imploded = implode(',', $distributed_county['Confirmatory']);
				$balance_imploded = implode(',', $balance_county['Confirmatory']);
			}

			$counties_imploded = implode(',', $gcategories_final_);

			$graph_stuff = array();

			// $beginning_imploded = "'" . implode ( "', '", $beginning_final_['Screening'] ) . "'";
			// $used_imploded = "'" . implode ( "', '", $used_final_['Screening'] ) . "'";
			$counties_imploded_c = "'" . implode ( "', '", $gcategories_final_c) . "'";
			// echo "<pre>";print_r($counties_imploded);exit;

			// echo "<pre>";print_r($balance_imploded);exit;
			$graph_stuff['categories'] = $counties_imploded_c;

			// $graph_stuff['series_data']['year_total'] = $year_total_imploded;
			$graph_stuff['series_data']['year_total'] = $balance_imploded;
			$graph_stuff['series_data']['distributed'] = $distributed_imploded;
			$graph_stuff['div'] = "utilization_bgg_confirmatory_graph";

			$graph_custom_conf = $this->drawing_rights_stacked_high_chart($graph_stuff);//Karsan

			$data['utilization_bgg_confirmatory_graph'] = $graph_custom_conf;

			// echo "<pre>";print_r($used_imploded_conf);exit;

			// echo "<pre>";print_r($data['utilization_bgg_confirmatory_graph']);exit;

			$graph_type='column';
			$graph_data=array_merge($graph_data,array("graph_id"=>'test-graph'));
			$graph_data = array_merge($graph_data, array("color" => "['#4b0082', '#6AF9C4']"));
			$graph_data=array_merge($graph_data,array("graph_title"=>""));
			$graph_data=array_merge($graph_data,array("stacking"=>"normal"));
			$graph_data=array_merge($graph_data,array("data_labels"=>"white"));
			$graph_data=array_merge($graph_data,array("graph_type"=>$graph_type));
			$graph_data=array_merge($graph_data,array("graph_yaxis_title"=>""));
			$graph_data=array_merge($graph_data,array("graph_categories"=>$gcategories_final_ ));
			$graph_data=array_merge($graph_data,array("series_data"=>$graph_stuff_raw['series_data']));
			$util_conf_graph = $this->hcmp_functions->create_high_chart_graph($graph_data);

			// echo "<pre>";print_r($util_conf_graph);exit;
			$data['test_graph'] = $util_conf_graph;

			$graph_type='column';
			$graph_data=array_merge($graph_data,array("graph_id"=>'utilization_bg_graph'));
			$graph_data = array_merge($graph_data, array("color" => "['#4b0082', '#6AF9C4']"));
			$graph_data=array_merge($graph_data,array("graph_title"=>""));
			$graph_data=array_merge($graph_data,array("stacking"=>"normal"));
			$graph_data=array_merge($graph_data,array("data_labels"=>"white"));
			$graph_data=array_merge($graph_data,array("graph_type"=>$graph_type));
			$graph_data=array_merge($graph_data,array("graph_yaxis_title"=>""));
			$graph_data=array_merge($graph_data,array("graph_categories"=>$counties_imploded ));
			$graph_data=array_merge($graph_data,array("series_data"=>array('Balance'=>$beginning_final_['Screening'],'Used drawing rights'=>$used_final_['Screening'])));
			$data['utilization_bg_graph']= $this->hcmp_functions->create_high_chart_graph($graph_data);

		}
        // echo "<pre>";print_r($data);exit;
        // echo "<pre>";print_r($drawing_balance_confirmatory);exit;
		$data['counties'] = $county_name;

		// echo "<pre>";print_r($data);exit;
		$counties = $this->db->query("SELECT * FROM counties")->result_array();
		$districts = $this->db->query("SELECT * FROM districts")->result_array();

		$data['tracer'] = $tracer;
		$data['commodity_division'] = isset($division)? $division :"NULL";
		// $title_append = "Sample filtered";

		$title_append = (isset($filtered_name) && $filtered_name !='')? $filtered_name:NULL;

		if ($district_id > 0 || $month > 0) {
			$title_append .= " Report";
		}else{
			$title_append .= " as of ".date('F',strtotime("-1 MONTH"))." Report";
		}

		$title_prepend = (isset($county_id) && $county_id > 0)? 'County':'Sub-County';

		$title_append .= (isset($quarter) && $quarter > 0 && $month < 1)? ' Quarter: '.$quarter:NULL;
		// echo "<pre>";print_r($title_append);exit;
		if ($quarter > 0 && $month_year < 1) {
			$quartered = 1;
			$quarter_append = ' <span class="font-blue-steel">*</span>';
		}
		$title_append .= (isset($filtered_date) && $filtered_date !="")? " ".date('F Y',strtotime($filtered_date)):NULL;
		// $title_prepend = (isset($title_prepend) && $title_prepend !='')? $title_prepend:'National';
		// echo "<pre>";print_r($title_append);exit;
		$data['title_append'] = $title_append;
		$data['quartered'] = $quartered;
		$data['quarter'] = $quarter;
		$data['quarter_append'] = $quarter_append;
		$data['filtered_month'] = $month;
		$data['filtered_year'] = $year;
		// $data['tracer_commodities'] = $commodities;
		$data['facility_count'] = $facility_count;
		$data['commodity_count'] = $commodity_count;
		$data['content_view'] = 'dashboard/dashboard';
		$data['county_data'] = $counties;
		$data['title_prepend'] = $title_prepend;
		$data['county_count'] = count($counties_using_CD4);
		$data['district_data'] = $districts;
		$data['commodity_divisions'] = $commodity_divisions;
		$data['title'] = "National Dashboard";
		$data['county_data'] = $counties;
		$data['county_id'] = $county_id;
		$data['subcounty_id'] = $district_id;
		$data['facility_code'] = $facility_code;
		$data['district_data'] = $districts;
		$data['maps'] = $map;
		$data['counties'] = $county_name;
		// echo "<pre>";print_r($data);exit;
		$this -> load -> view("v2/dashboard/dashboard_subcounty", $data);
	
	}

	public function facility_dashboard($county_id = NULL, $district_id = NULL, $facility_code = NULL,$quarter = NULL,$month_year = NULL)
	{
		$page_caching = $this->output->cache(CACHE_REFRESH_INTERVAL);
		$default = "tracer";//FYI
		// echo "<pre>";print_r($quarter);exit;
		// $map = $this->render_map();
		$county_id = (isset($county_id) && $county_id > 0)? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id > 0)? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code > 0)? $facility_code:NULL;
		$quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;

		$data['filter_years'] = $this->get_years_to_date();
		$month_year = (isset($month_year) && $month_year != 0 && $month_year !='')? $month_year:NULL;

		if (isset($month_year)) {
            // $year = substr($month, -4);
			$m_data = explode('_', $month_year);
			// echo "<pre>";print_r($m_data);exit;
            $month = $m_data[0];
            $year = $m_data[1];
            $monthyear = $year . '-' . $month . '-01';
			$filtered_date = $year."-".$month."-01";

        } else {
            /*$month = $this->session->userdata('Month');
            if ($month == '') {
                $month = date('mY', time());
            }
            $year = date('Y');
            $month = substr_replace($month, "", -4);
            $monthyear = $year . '-' . $month . '-01';*/
        }

        if ($facility_code > 0) {
			$filtered = 1;
			// $quarter_append = ' <span class="font-blue-steel">*</span>';
		}else{
			$filtered = 0;
		}

		$data['filtered'] = $filtered;
		$data['filter_months'] = $this->get_months_between_dates();

		$current = date('m');
        $curMonth = $current;
        $curQuarter = ceil($curMonth/3);//KARSAN
        // echo "<pre>";print_r($month);exit;
        if ($quarter > 0 && $month < 1) {
        	$curQuarter = $quarter;
        	
        }else{
        	$quarter_data = $this->get_year_quarter($month,NULL,$year,NULL);
        	$curQuarter = $quarter_data['quarter'];
        }
		$data['drawing_quarters'] = $curQuarter;

        $current_date = date('Y-m-d');
        $append_date = (isset($append_date) && $append_date !="")? $append_date : date('Y-m-d');
		
		$county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;

		$commodity_count = Dashboard_model::get_commodity_count();
		// $commodities = Dashboard_model::get_division_commodities($division);
		$page_title = "RTK Dashboard.";
		$data['page_title'] = $page_title;
		// echo "<pre>";print_r($commodities);exit;
		$commodity_divisions = Dashboard_model::get_division_details();
		$counties = $this->db->query("SELECT * FROM counties")->result_array();
		$districts = $this->db->query("SELECT * FROM districts")->result_array();
		$facilities = $this->db->query("SELECT * FROM facilities WHERE rtk_enabled = 1")->result_array();

		$facility_count = Dashboard_model::get_online_offline_facility_count();
		$counties_using_CD4 = Counties::get_counties_all_using_CD4();

		$graph_type = $graph_data = $category_data = $series_data = array();
		$category_data[] = "Screening";
		$category_data[] = "Confirmatory";
		$screening = array("10","20");
		// echo "<pre>";print_r($screening);exit;
		$series_data['Screening'] = [10];
		$series_data['Confirmatory'] = [20];

        
		$filtered_name = 	NULL;
		// $district_data = (isset($district_id) && ($district_id > 0)) ? districts::get_district_name($district_id) -> toArray() : NULL;
		// $county_search = counties::get_county_name($county_id);
		$facility_data = (isset($facility_code) && ($facility_code > 0)) ? facilities::get_facility_name($facility_code) -> toArray() : NULL;
		// echo "<pre>";print_r($facility_data);exit;

		if (count($facility_data) > 0) {
			// $filtered_name = $county_search['county']." County $append_date";
			$filtered_name = $facility_data[0]['facility_name'];
		}

        $facility_data = $this->get_facility_data($county_id,$district_id,$facility_code);
        
        if ($filtered > 0) {
        	// $kemsa_issues = $this->kemsa_issue_data($year, $month, $county_id, $district_id,$facility_code, $quarter);
			
			$facility_orders = $this->get_facility_orders(NULL,NULL,$facility_code,$quarter,$year,$month);
			// echo "<pre>";print_r($facility_orders);exit;
			$data['facility_orders'] = $facility_orders;

			$kemsa_issue_data_s = $this->get_kemsa_issue_data(NULL, NULL, $facility_code, 4 , $quarter,$year,$month,"facility");
	        // echo "<pre>";print_r($kemsa_issue_data_s);exit;
	        $kemsa_issue_data_c = $this->get_kemsa_issue_data(NULL, NULL, $facility_code, 5 , $quarter,$year,$month,"facility");

	        $kemsa_issued_screening = $kemsa_issue_data_s[$facility_code]['qty_issued'];
	        $kemsa_issued_confirmatory = $kemsa_issue_data_c[$facility_code]['qty_issued'];
			$data['kemsa_issue_date'] = $kemsa_issue_data_s[$facility_code]['issue_date'];
			$data['kemsa_issued_screening'] = $kemsa_issue_data_s[$facility_code]['qty_issued'];
			$data['kemsa_issued_confirmatory'] = $kemsa_issue_data_c[$facility_code]['qty_issued'];

			// echo "<pre>";print_r($data);exit;
	        // $summary_data_s = $summary_data_c = array();
	        $summary_data_s = $this->get_county_order_data($county_id, $district_id, $facility_code, 4 , $quarter,$month,$year);
	        // echo "<pre>";print_r($summary_data_s);exit;
	        $summary_data_c = $this->get_county_order_data($county_id, $district_id, $facility_code, 5 , $quarter,$month,$year);

	        $data['summary_data_date_text'] = $summary_data_s['date_text'];
	        $data['summary_data_date'] = $summary_data_s['results'][0]['order_date'];

	        // echo "<pre>";print_r($summary_data_s);exit;
	        $received_site_scr = $summary_data_s['results'][0]['q_received'];
	        $received_site_conf = $summary_data_c['results'][0]['q_received'];
	        // echo "REACHED HERE";exit;

	        $data['received_site_screening'] = $received_site_scr;
	        $data['received_site_confirmatory'] = $received_site_conf;

	        $consumed_site_scr = $summary_data_s['results'][0]['q_used'];
	        $consumed_site_conf = $summary_data_c['results'][0]['q_used'];

	        $data['consumed_site_screening'] = $consumed_site_scr;
	        $data['consumed_site_confirmatory'] = $consumed_site_conf;

	        $balances_scr = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 4 , $beg_date, $end_date, $quarter,NULL,$month,$year);
	        $balances_conf = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 5 , $beg_date, $end_date, $quarter,NULL,$month,$year);
	        // echo "<pre>";print_r($balances_scr);exit;
	        // $closing_bal_scr = $summary_data_s['results'][0]['beginning_bal'];
	        // $closing_bal_conf = $summary_data_c['results'][0]['beginning_bal'];

	        $closing_bal_scr = $balances_scr['closing_balance']['closing_balance'];
	        $closing_bal_conf = $balances_conf['closing_balance']['closing_balance'];
	        $closing_bal_order_date = $balances_scr['closing_balance']['order_date'];

	        $beginning_bal_scr = $balances_scr['beginning_balance']['beginning_balance'];
	        $beginning_bal_conf = $balances_conf['beginning_balance']['beginning_balance'];
	        $beginning_bal_order_date = $balances_scr['beginning_balance']['order_date'];

	        $data['beginning_balance_screening'] = $beginning_bal_scr;
	        $data['beginning_balance_confirmatory'] = $beginning_bal_conf;
	        $data['beginning_balance_order_date'] = $beginning_bal_order_date;

	        $data['closing_balance_screening'] = $closing_bal_scr;
	        $data['closing_balance_confirmatory'] = $closing_bal_conf;
	        $data['closing_balance_order_date'] = $closing_bal_order_date;


	        // echo "<pre>";print_r($c_alloc_data);exit;
	  		//$county_scr_closing_balance = $national_aggregates['final_closing_bal4'];
			//$county_conf_closing_balance = $national_aggregates['final_closing_bal5'];

			$county_scr_closing_balance = $closing_bal_scr;
			$county_conf_closing_balance = $closing_bal_conf;

			// echo "<pre>";print_r($county_scr_total_drawing_rights);exit;

			$used_series_scr = $used_series_conf = $allocated_series_scr = $allocated_series_conf = array();
			$received_series_scr = $received_series_conf = $used_category_scr = $used_category_conf = array();
			$received_category_scr = $received_category_conf = array();
			$total_rights_series_scr = $total_rights_series_conf = array();
			$closing_bal_series_scr = $closing_bal_series_conf = array();
			$kemsa_issued_series_scr = $kemsa_issued_series_conf = array();

			$total_rights_series_scr = array_merge($total_rights_series_scr, array("Total" => (int)$county_scr_total_drawing_rights));
			$total_rights_series_conf = array_merge($total_rights_series_conf, array("Total" => (int)$county_conf_total_drawing_rights));

			$kemsa_issued_series_scr = array_merge($kemsa_issued_series_scr, array("Issued from KEMSA" => (int)$kemsa_issued_screening));
			$kemsa_issued_series_conf = array_merge($kemsa_issued_series_conf, array("Issued from KEMSA" => (int)$kemsa_issued_confirmatory));

			$closing_bal_series_scr = array_merge($closing_bal_series_scr, array("Closing Balance" => (int)$county_scr_closing_balance));
			$closing_bal_series_conf = array_merge($closing_bal_series_conf, array("Closing Balance" => (int)$county_conf_closing_balance));

			$allocated_series_scr = array_merge($allocated_series_scr, array("Allocated" => (int)$county_scr_distributed_aggr));
			$allocated_series_conf = array_merge($allocated_series_conf, array("Allocated" => (int)$county_conf_distributed_aggr));

			$used_series_scr = array_merge($used_series_scr, array("Used" => (int)$consumed_site_scr));
			$used_series_conf = array_merge($used_series_scr, array("Used" => (int)$consumed_site_conf));

			$received_series_scr = array_merge($received_series_scr, array("Received" => (int)$received_site_scr));
			$received_series_conf = array_merge($received_series_conf, array("Received" => (int)$received_site_conf));

			// echo "<pre>";print_r($total_rights_series_scr);exit;
			// $utilization_category = array_merge($used_category, array("Screening","Confirmatory"));
			$utilization_category_scr[] = "Screening";
			$utilization_category_conf[] = "Confirmatory";
			$utilization_category_imploded_scr = "'" . implode ( "', '", $utilization_category_scr) . "'";
			$utilization_category_imploded_conf = "'" . implode ( "', '", $utilization_category_conf) . "'";

			$graph_stuff = array();

			// $beginning_imploded = "'" . implode ( "', '", $beginning_final_['Screening'] ) . "'";
			// $used_imploded = "'" . implode ( "', '", $used_final_['Screening'] ) . "'";
			$counties_imploded_c = "'" . implode ( "', '", $gcategories_final_c) . "'";

			$closing_bal_series_scr =  implode ( "', '", $closing_bal_series_scr);
			$total_rights_series_scr =  implode ( "', '", $total_rights_series_scr);
			$kemsa_issued_series_scr =  implode ( "', '", $kemsa_issued_series_scr);
			$allocated_series_scr = implode ( "', '", $allocated_series_scr);
			$used_series_scr = implode ( "', '", $used_series_scr);
			$received_series_scr = implode ( "', '", $received_series_scr);
			// echo "<pre>";print_r($closing_bal_series_scr);exit;

			// echo "<pre>";print_r($utilization_category_imploded_scr);exit;
			$graph_stuff['categories'] = $utilization_category_imploded_scr;

			if ($month < 1) {
				// $graph_stuff['series_data']['Closing_Balance'] = $closing_bal_series_conf;
				$graph_stuff['series_data']['Issued from KEMSA'] = $kemsa_issued_series_scr;
				// $graph_stuff['series_data']['Total'] = $total_rights_series_conf;
				// $graph_stuff['series_data']['Allocated'] = $allocated_series_scr;
				// $graph_stuff['series_data']['Used'] = $used_series_conf;
				$graph_stuff['series_data']['Received'] = $received_series_scr;
			}else{
				$graph_stuff['series_data']['Issued from KEMSA'] = $kemsa_issued_series_scr;
				// $graph_stuff['series_data']['Allocated'] = $allocated_series_scr;
				$graph_stuff['series_data']['Beginning Balance'] = $beginning_bal_scr;
				$graph_stuff['series_data']['Closing Balance'] = $closing_bal_scr;
				$graph_stuff['series_data']['Used'] = $used_series_scr;
				$graph_stuff['series_data']['Received'] = $received_series_scr;
			}

			$graph_stuff['div'] = "utilization_graph_scr";
			// echo "<pre>";print_r($graph_stuff);exit;

			$utilization_graph_scr_custom = $this->allocationvsreceivedvsconsumed_highchart($graph_stuff);//Karsan
			// echo "<pre>";print_r($utilization_graph_scr_custom);exit;

			$data['utilization_graph_scr'] = $utilization_graph_scr_custom;

			$graph_stuff = array();

			// $beginning_imploded = "'" . implode ( "', '", $beginning_final_['Screening'] ) . "'";
			// $used_imploded = "'" . implode ( "', '", $used_final_['Screening'] ) . "'";
			$counties_imploded_c = "'" . implode ( "', '", $gcategories_final_c) . "'";
			$kemsa_issued_series_conf =  implode ( "', '", $kemsa_issued_series_conf);
			$closing_bal_series_conf =  implode ( "', '", $closing_bal_series_conf);
			$total_rights_series_conf =  implode ( "', '", $total_rights_series_conf);
			$allocated_series_conf = implode ( "', '", $allocated_series_conf);
			$used_series_conf = implode ( "', '", $used_series_conf);
			$received_series_conf = implode ( "', '", $received_series_conf);
			// echo "<pre>";print_r($counties_imploded);exit;

			// echo "<pre>";print_r($utilization_category_imploded_conf);exit;
			$graph_stuff['categories'] = $utilization_category_imploded_conf;


			if ($month < 1) {
				// $graph_stuff['series_data']['Closing_Balance'] = $closing_bal_series_conf;
				$graph_stuff['series_data']['Issued from KEMSA'] = $kemsa_issued_series_conf;
				// $graph_stuff['series_data']['Total'] = $total_rights_series_conf;
				// $graph_stuff['series_data']['Allocated'] = $allocated_series_conf;
				// $graph_stuff['series_data']['Used'] = $used_series_conf;
				$graph_stuff['series_data']['Received'] = $received_series_conf;
			}else{
				$graph_stuff['series_data']['Issued from KEMSA'] = $kemsa_issued_series_conf;
				// $graph_stuff['series_data']['Allocated'] = $allocated_series_conf;
				$graph_stuff['series_data']['Beginning Balance'] = $beginning_bal_conf;
				$graph_stuff['series_data']['Closing Balance'] = $closing_bal_conf;
				$graph_stuff['series_data']['Used'] = $used_series_conf;
				$graph_stuff['series_data']['Received'] = $received_series_conf;
			}

			$graph_stuff['div'] = "utilization_graph_conf";
			// echo "<pre>";print_r($graph_stuff);exit;
			$utilization_graph_conf_custom = $this->allocationvsreceivedvsconsumed_highchart($graph_stuff);//Karsan
			// echo "<pre>";print_r($utilization_graph_conf_custom);exit;

			$data['utilization_graph_conf'] = $utilization_graph_conf_custom;

			// echo "<pre>";print_r($data['utilization_graph']);exit;
			// $county_aggregates = $this->drawing_rights_data($county_id,$district_id);
			// echo $year." ".$month;exit;
			$county_aggregates = $this->national_stockcard_v2($county_id,$district_id);
	        $drawing_aggregates = $this->drawing_rights_data($year, $month, $county_id, $district_id, $facility_code);

			// echo "<pre>";print_r($drawing_aggregates);exit;
			//sum_opening 
			//sum_used
			// echo "<pre>";print_r($data);exit;
			$beginning_series = $year_total = $distributed = $balance = $used_drawing_rights_series = $beginning_category = $used_drawing_rights_category = array();
			$gcategories_final_ = $gcategories_final_s = $gcategories_final_c = array();

	        usort($drawing_aggregates, function($a, $b) {
			    // return $b['screening_balance'] - $a['screening_balance'];
			    return $b['screening_distributed'] - $a['screening_distributed'];
			});

			foreach ($drawing_aggregates as $key => $value) {
					$bal_s = 0;
					$drawing_rights_quarter = $value['quarter'];
					$gcategories_final_s[] = $value['county'];

					$year_total["Screening"][] = $value['screening_year_total'];
					$distributed["Screening"][] = $value['screening_distributed'];
					$bal_s = $value['screening_year_total'] - $value['screening_distributed'];
					$bal_s = (isset($bal_s) && $bal_s > 0)? $bal_s:0;
					$balance["Screening"][] = $value['screening_balance'];
			}

			usort($drawing_aggregates, function($a, $b) {
			    // return $b['confirmatory_balance'] - $a['confirmatory_balance'];
			    return $b['confirmatory_distributed'] - $a['confirmatory_distributed'];
			});

			foreach ($drawing_aggregates as $key => $value) {
					$bal_c = 0;
					$gcategories_final_c[] = $value['county'];

					$year_total["Confirmatory"][] = $value['confirmatory_year_total'];
					$distributed["Confirmatory"][] = $value['confirmatory_distributed'];
					$bal_c = $value['confirmatory_year_total'] - $value['confirmatory_distributed'];
					$bal_c = (isset($bal_c) && $bal_c > 0)? $bal_c:0;
					$balance["Confirmatory"][] = $value['confirmatory_balance'];
			}

			$data['drawing_rights_quarter'] =$drawing_rights_quarter;

			// echo "<pre>";print_r($year_total);
			// echo "<pre>";print_r($distributed);
			// echo "<pre>";print_r($balance);
			// exit;

			$gcategories_final_s = array_unique($gcategories_final_s);
			$gcategories_final_c = array_unique($gcategories_final_c);
			// $utilization_category = array_merge($used_category, array("Screening","Confirmatory"));
			// $utilization_category = array_merge($used_category, $gcategories_final_);

			// echo "<pre>";print_r($gcategories_final_);exit;
			$counties = implode(",",$gcategories_final_s);
				// echo "<pre>";print_r($counties);exit;

			$beginning_series = array_merge($beginning_series, array("Screening beginning" => (int)$national_aggregates['final_closing_bal4']));
			$beginning_series = array_merge($beginning_series, array("Confirmatory beginning" => (int)$national_aggregates['final_closing_bal5']));

			$used_drawing_rights_series = array_merge($used_drawing_rights_series, array("Screening used" => (int)$national_aggregates['final_used4']));
			$used_drawing_rights_series = array_merge($used_drawing_rights_series, array("Confirmatory used" => (int)$national_aggregates['final_used5']));
			
			$year_total_imploded = implode(',', $year_total['Screening']);
			$balance_imploded = implode(',', $balance['Screening']);
			
			$distributed_imploded = implode(',', $distributed['Screening']);
			if ($county_id > 0) {
				$distributed_imploded = implode(',', $distributed_county['Screening']);
				$balance_imploded = implode(',', $balance_county['Screening']);
			}
			// $counties_imploded_s = implode(',', $gcategories_final_s);

			$graph_stuff = array();

			// $beginning_imploded = "'" . implode ( "', '", $beginning_final_['Screening'] ) . "'";
			// $used_imploded = "'" . implode ( "', '", $used_final_['Screening'] ) . "'";
			$counties_imploded_s = "'" . implode ( "', '", $gcategories_final_s) . "'";
			// echo "<pre>";print_r($counties_imploded);exit;

			// echo "<pre>";print_r($beginning_imploded);exit;
			$graph_stuff['categories'] = $counties_imploded_s;

			// $graph_stuff['series_data']['year_total'] = $year_total_imploded;
			$graph_stuff['series_data']['year_total'] = $balance_imploded;
			$graph_stuff['series_data']['distributed'] = $distributed_imploded;
			$graph_stuff['div'] = "utilization_bgg_screening_graph";

			$graph_custom = $this->drawing_rights_stacked_high_chart($graph_stuff);//Karsan
			$data['utilization_bgg_screening_graph'] = $graph_custom;

			$year_total_imploded = implode(',', $year_total['Confirmatory']);
			$balance_imploded = implode(',', $balance['Confirmatory']);
			$distributed_imploded = implode(',', $distributed['Confirmatory']);
			if ($county_id > 0) {
				$distributed_imploded = implode(',', $distributed_county['Confirmatory']);
				$balance_imploded = implode(',', $balance_county['Confirmatory']);
			}

			$counties_imploded = implode(',', $gcategories_final_);

			$graph_stuff = array();

			// $beginning_imploded = "'" . implode ( "', '", $beginning_final_['Screening'] ) . "'";
			// $used_imploded = "'" . implode ( "', '", $used_final_['Screening'] ) . "'";
			$counties_imploded_c = "'" . implode ( "', '", $gcategories_final_c) . "'";
			// echo "<pre>";print_r($counties_imploded);exit;

			// echo "<pre>";print_r($balance_imploded);exit;
			$graph_stuff['categories'] = $counties_imploded_c;

			// $graph_stuff['series_data']['year_total'] = $year_total_imploded;
			$graph_stuff['series_data']['year_total'] = $balance_imploded;
			$graph_stuff['series_data']['distributed'] = $distributed_imploded;
			$graph_stuff['div'] = "utilization_bgg_confirmatory_graph";

			$graph_custom_conf = $this->drawing_rights_stacked_high_chart($graph_stuff);//Karsan

			$data['utilization_bgg_confirmatory_graph'] = $graph_custom_conf;

			// echo "<pre>";print_r($used_imploded_conf);exit;

			// echo "<pre>";print_r($data['utilization_bgg_confirmatory_graph']);exit;

			$graph_type='column';
			$graph_data=array_merge($graph_data,array("graph_id"=>'test-graph'));
			$graph_data = array_merge($graph_data, array("color" => "['#4b0082', '#6AF9C4']"));
			$graph_data=array_merge($graph_data,array("graph_title"=>""));
			$graph_data=array_merge($graph_data,array("stacking"=>"normal"));
			$graph_data=array_merge($graph_data,array("data_labels"=>"white"));
			$graph_data=array_merge($graph_data,array("graph_type"=>$graph_type));
			$graph_data=array_merge($graph_data,array("graph_yaxis_title"=>""));
			$graph_data=array_merge($graph_data,array("graph_categories"=>$gcategories_final_ ));
			$graph_data=array_merge($graph_data,array("series_data"=>$graph_stuff_raw['series_data']));
			$util_conf_graph = $this->hcmp_functions->create_high_chart_graph($graph_data);

			// echo "<pre>";print_r($util_conf_graph);exit;
			$data['test_graph'] = $util_conf_graph;

			$graph_type='column';
			$graph_data=array_merge($graph_data,array("graph_id"=>'utilization_bg_graph'));
			$graph_data = array_merge($graph_data, array("color" => "['#4b0082', '#6AF9C4']"));
			$graph_data=array_merge($graph_data,array("graph_title"=>""));
			$graph_data=array_merge($graph_data,array("stacking"=>"normal"));
			$graph_data=array_merge($graph_data,array("data_labels"=>"white"));
			$graph_data=array_merge($graph_data,array("graph_type"=>$graph_type));
			$graph_data=array_merge($graph_data,array("graph_yaxis_title"=>""));
			$graph_data=array_merge($graph_data,array("graph_categories"=>$counties_imploded ));
			$graph_data=array_merge($graph_data,array("series_data"=>array('Balance'=>$beginning_final_['Screening'],'Used drawing rights'=>$used_final_['Screening'])));
			$data['utilization_bg_graph']= $this->hcmp_functions->create_high_chart_graph($graph_data);
        }else{
        	$facility_drawing_data_s = $facility_drawing_data_c = $facility_drawing_data_ss = $facility_drawing_data_cc = array();
	        // $districts = $result = $this->db->query($)->result_array();

	        // echo "<pre>";print_r($balances_scr);exit;

	        // $f_o_data_s = $this->get_facility_order_data(NULL, $district_id,NULL, 4 , $quarter,$month,$year);
	        
	        /*
	        $balances_scr = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 4 , $beg_date, $end_date, $quarter,"facility",$month,$year);
	        $balances_conf = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 5 , $beg_date, $end_date, $quarter,"facility",$month,$year);

	        $f_o_data_s = $this->get_facility_order_data(NULL, NULL,NULL, 4 , $quarter,$month,$year);
	        $f_o_data_c = $this->get_facility_order_data(NULL, NULL,NULL, 5 , $quarter,$month,$year);
	        */
	        
	        // echo "<pre>";print_r($f_o_data_s);exit;
	        foreach ($f_o_data_s as $key => $value) {
	        	// echo "<pre>";print_r($value);exit;
	        	$c_id = $value['county_id'];
	        	$c_name = $value['county'];
	        	$d_id = $value['district_id'];
	        	$d_name = $value['district'];
	        	$f_code = $value['facility_code'];
	        	$f_name = $value['facility_name'];

				$facility_drawing_data_ss['county_id'] = $c_id;
				$facility_drawing_data_ss['county_name'] = $c_name;
				$facility_drawing_data_ss['district_id'] = $d_id;
				$facility_drawing_data_ss['district_name'] = $d_name;
				$facility_drawing_data_ss['facility_code'] = $f_code;
				$facility_drawing_data_ss['facility_name'] = $f_name;
				$facility_drawing_data_ss['allocated'] = $scr_dist;
				// $facility_drawing_data_ss['issued_kemsa'] = $f_kemsa_issues[0]['screening_units'];
				// $facility_drawing_data_ss['beg_bal'] = $value['beginning_bal'];
				$facility_drawing_data_ss['beg_bal'] = $balances_scr[$f_code]['beginning_balance'];

				$facility_drawing_data_ss['q_rec'] = $value['q_received'];
				$facility_drawing_data_ss['q_used'] = $value['q_used'];
				$facility_drawing_data_ss['tests_done'] = $value['no_of_tests_done'];
				$facility_drawing_data_ss['losses'] = $value['losses'];
				$facility_drawing_data_ss['positive_adj'] = $value['positive_adj'];
				$facility_drawing_data_ss['negative_adj'] = $value['negative_adj'];
				// $facility_drawing_data_ss['closing_bal'] = $value['closing_stock'];
				$facility_drawing_data_ss['closing_bal'] = $balances_scr[$f_code]['closing_balance'];

	        	// echo "<pre>";print_r($facility_drawing_data_ss);exit;
				array_push($facility_drawing_data_s, $facility_drawing_data_ss);
	        }
	        // echo "<pre>";print_r($facility_drawing_data_s);exit;

	        foreach ($f_o_data_c as $key => $value) {
	        	// echo "<pre>";print_r($value);exit;
	        	$c_id = $value['county_id'];
	        	$c_name = $value['county'];
	        	$d_id = $value['district_id'];
	        	$d_name = $value['district'];
	        	$f_code = $value['facility_code'];
	        	$f_name = $value['facility_name'];

				$facility_drawing_data_cc['county_id'] = $c_id;
				$facility_drawing_data_cc['county_name'] = $c_name;
				$facility_drawing_data_cc['district_id'] = $d_id;
				$facility_drawing_data_cc['district_name'] = $d_name;
				$facility_drawing_data_cc['facility_code'] = $f_code;
				$facility_drawing_data_cc['facility_name'] = $f_name;
				$facility_drawing_data_cc['allocated'] = $conf_dist;
				// $facility_drawing_data_cc['issued_kemsa'] = $f_kemsa_issues[0]['confirmatory_units'];
				// $facility_drawing_data_cc['beg_bal'] = $value['beginning_bal'];
				$facility_drawing_data_cc['beg_bal'] = $balances_conf[$f_code]['beginning_balance'];

				$facility_drawing_data_cc['q_rec'] = $value['q_received'];
				$facility_drawing_data_cc['q_used'] = $value['q_used'];
				$facility_drawing_data_cc['tests_done'] = $value['no_of_tests_done'];
				$facility_drawing_data_cc['losses'] = $value['losses'];
				$facility_drawing_data_cc['positive_adj'] = $value['positive_adj'];
				$facility_drawing_data_cc['negative_adj'] = $value['negative_adj'];
				// $facility_drawing_data_cc['closing_bal'] = $value['closing_stock'];
				$facility_drawing_data_cc['closing_bal'] = $balances_conf[$f_code]['closing_balance'];

				array_push($facility_drawing_data_c, $facility_drawing_data_cc);
	        }

	        // echo "<pre>";print_r($facility_drawing_data_c);exit;
	        $data['facility_drawing_data_s'] = $facility_drawing_data_s;
	        $data['facility_drawing_data_c'] = $facility_drawing_data_c;
        }

        // echo "<pre>";print_r($data);exit;
        // echo "<pre>";print_r($drawing_balance_confirmatory);exit;
		$data['counties'] = $county_name;

		// echo "<pre>";print_r($data);exit;
		$counties = $this->db->query("SELECT * FROM counties")->result_array();
		$districts = $this->db->query("SELECT * FROM districts")->result_array();

		$data['tracer'] = $tracer;
		$data['commodity_division'] = isset($division)? $division :"NULL";
		// $title_append = "Sample filtered";

		$title_append .= (isset($filtered_name) && $filtered_name !='')? $filtered_name:NULL;
		// $title_append .= (isset($filtered_date) && $filtered_date !="")? " ".date('F Y',strtotime($filtered_date)):NULL;
		// echo "<pre>";print_r($title_append);exit;

		$title_append .= (isset($filtered_date) && $filtered_date !="")? " ".date('F Y',strtotime($filtered_date)):NULL;
		if ($facility_code > 0 || $month > 0) {
			$title_append .= " Report";
		}else{
			$title_append .= " as of ".date('F',strtotime("-1 MONTH"))." Report";
		}

		$title_prepend = (isset($county_id) && $county_id > 0)? 'County':'Facility';

		$title_append .= (isset($quarter) && $quarter > 0)? ' Quarter: '.$quarter:NULL;
		// echo "<pre>";print_r($title_append);exit;
		if ($quarter > 0) {
			$quartered = 1;
			$quarter_append = ' <span class="font-blue-steel">*</span>';
		}
		// $title_prepend = (isset($title_prepend) && $title_prepend !='')? $title_prepend:'National';
		// echo "<pre>";print_r($facilities);exit;
		$data['title_append'] = $title_append;
		$data['quarter'] = $quarter;
		$data['quartered'] = $quartered;
		$data['quarter_append'] = $quarter_append;
		$data['filtered_month'] = $month;
		$data['filtered_year'] = $year;
		// $data['tracer_commodities'] = $commodities;
		$data['facility_count'] = $facility_count;
		$data['facility_data'] = $facilities;
		$data['commodity_count'] = $commodity_count;
		$data['content_view'] = 'dashboard/dashboard';
		$data['county_data'] = $counties;
		$data['title_prepend'] = $title_prepend;
		$data['county_count'] = count($counties_using_CD4);
		$data['district_data'] = $districts;
		$data['commodity_divisions'] = $commodity_divisions;
		$data['title'] = "National Dashboard";
		$data['county_data'] = $counties;
		$data['county_id'] = $county_id;
		$data['subcounty_id'] = $district_id;
		$data['facility_code'] = $facility_code;
		$data['district_data'] = $districts;
		$data['maps'] = $map;
		$data['counties'] = $county_name;
		// echo "<pre>";print_r($data);exit;
		// echo "<pre>";print_r($data);exit;
		$this -> load -> view("v2/dashboard/dashboard_facility", $data);
	
	}

	public function get_facility_summary_table($by = NULL,$county_id = NULL,$district_id = NULL,$facility_code = NULL,$commodity_id = NULL, $month = NULL, $quarter = NULL, $graph_div = NULL)
	{
		// echo "<pre>THIS: ";print_r($county_id);exit;
		$commodity_id = (isset($commodity_id) && $commodity_id > 0)? $commodity_id:"4,5";
        $months_texts = array();
        $percentages = array();

        /*switch ($by) {
        	case 'county':
        	// $group_by = "GROUP BY commodity_id , c.id";
        	if ($county_id > 0) {
        		$group_by = "GROUP BY commodity_id , c.id, MONTH(lco.order_date)";
        	}else{
        		$group_by = "GROUP BY commodity_id , c.id";
        	}
        		
        		break;

        	case 'subcounty':

        	if ($district_id > 0) {
        		$group_by = "GROUP BY commodity_id , d.id, MONTH(lco.order_date)";
	        	// $limit = "  LIMIT 50 ";
        	}else{
        		$group_by = "GROUP BY commodity_id , d.id";
	        	$limit = "  LIMIT 50 ";
        	}
        		
        		break;

        	case 'facility':
	        	$group_by = "GROUP BY commodity_id , f.facility_code ";
	        	$limit = "  LIMIT 50 ";

        	// if ($facility_code > 0) {
        	// 	$group_by = "GROUP BY commodity_id , d.id, MONTH(lco.order_date)";
	        // 	// $limit = "  LIMIT 50 ";
        	// }else{
        	// }
        		
        		break;
        	
        	default:
			$group_by = "GROUP BY commodity_id , c.id";
        		break;
        }*/

        $facility_drawing_data_s = $facility_drawing_data_c = $facility_drawing_data_ss = $facility_drawing_data_cc = array();

        $f_o_data_s = $this->get_facility_order_data(NULL, NULL,NULL, $commodity_id , $quarter);
        // $f_o_data_c = $this->get_facility_order_data(NULL, NULL,NULL, 5 , $quarter);
        // echo "<pre>";print_r($f_o_data_s);exit;
        foreach ($f_o_data_s as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
        	$c_id = $value['county_id'];
        	$c_name = $value['county'];
        	$d_id = $value['district_id'];
        	$d_name = $value['district'];
        	$f_code = $value['facility_code'];
        	$f_name = $value['facility_name'];

			$facility_drawing_data_ss['county_id'] = $c_id;
			$facility_drawing_data_ss['county_name'] = $c_name;
			$facility_drawing_data_ss['district_id'] = $d_id;
			$facility_drawing_data_ss['district_name'] = $d_name;
			$facility_drawing_data_ss['facility_code'] = $f_code;
			$facility_drawing_data_ss['facility_name'] = $f_name;
			$facility_drawing_data_ss['allocated'] = $scr_dist;
			// $facility_drawing_data_ss['issued_kemsa'] = $f_kemsa_issues[0]['screening_units'];
			$facility_drawing_data_ss['beg_bal'] = $value['beginning_bal'];
			$facility_drawing_data_ss['q_rec'] = $value['q_received'];
			$facility_drawing_data_ss['q_used'] = $value['q_used'];
			$facility_drawing_data_ss['tests_done'] = $value['no_of_tests_done'];
			$facility_drawing_data_ss['losses'] = $value['losses'];
			$facility_drawing_data_ss['positive_adj'] = $value['positive_adj'];
			$facility_drawing_data_ss['negative_adj'] = $value['negative_adj'];
			$facility_drawing_data_ss['closing_bal'] = $value['closing_stock'];

        	// echo "<pre>";print_r($facility_drawing_data_ss);exit;
			array_push($facility_drawing_data_s, $facility_drawing_data_ss);
        }
        // echo "<pre>";print_r($facility_drawing_data_s);exit;

        /*foreach ($f_o_data_c as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
        	$c_id = $value['county_id'];
        	$c_name = $value['county'];
        	$d_id = $value['district_id'];
        	$d_name = $value['district'];
        	$f_code = $value['facility_code'];
        	$f_name = $value['facility_name'];

			$facility_drawing_data_cc['county_id'] = $c_id;
			$facility_drawing_data_cc['county_name'] = $c_name;
			$facility_drawing_data_cc['district_id'] = $d_id;
			$facility_drawing_data_cc['district_name'] = $d_name;
			$facility_drawing_data_cc['facility_code'] = $f_code;
			$facility_drawing_data_cc['facility_name'] = $f_name;
			$facility_drawing_data_cc['allocated'] = $conf_dist;
			// $facility_drawing_data_cc['issued_kemsa'] = $f_kemsa_issues[0]['confirmatory_units'];
			$facility_drawing_data_cc['beg_bal'] = $value['beginning_bal'];
			$facility_drawing_data_cc['q_rec'] = $value['q_received'];
			$facility_drawing_data_cc['q_used'] = $value['q_used'];
			$facility_drawing_data_cc['tests_done'] = $value['no_of_tests_done'];
			$facility_drawing_data_cc['losses'] = $value['losses'];
			$facility_drawing_data_cc['positive_adj'] = $value['positive_adj'];
			$facility_drawing_data_cc['negative_adj'] = $value['negative_adj'];
			$facility_drawing_data_cc['closing_bal'] = $value['closing_stock'];

			array_push($facility_drawing_data_c, $facility_drawing_data_cc);
        }
		*/
        // echo "<pre>";print_r($facility_drawing_data_c);exit;
        // $data['facility_drawing_data_s'] = $facility_drawing_data_s;
        // $data['facility_drawing_data_c'] = $facility_drawing_data_c;

        $table_data = '
        <table class="table datatable display cell-border compact" id="datatable" style="table-layout:fixed;" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th rowspan="2">#</th>
					<th rowspan="2">Facility Code</th>
					<th rowspan="2">Facility Name</th>
					<th rowspan="2">Sub-County</th>
					<th rowspan="2">County</th>
					<!-- <th rowspan="2">Allocated</th> -->
					<!-- <th rowspan="2">Issued from KEMSA</th> -->
					<th rowspan="2">Beginning Balance</th>
					<th rowspan="2">Quantity Received</th>
					<th rowspan="2">Quantity Used</th>
					<th rowspan="2">Tests Done</th>
					<th rowspan="2">Losses</th>
					<th colspan="2">Adjustments</th>
					<!-- <th rowspan="2">Adjustments(-)</th> -->
					<th rowspan="2">Closing Balance</th>
				</tr>
				<tr>
					<th>Positive</th>
					<th>Negative</th>
				</tr>
			</thead>
			<tbody>';
		foreach ($f_o_data_s as $key => $value) {
		    $table_data .='
		    	<tr>
					<td></td>
					<td>'.$value['facility_code'].'</td>
					<td>'.$value['facility_name'].'</td>
					<td>'.$value['district_name'].'</td>
					<td>'.$value['county_name'].'</td>
					<td>'.number_format($value['beg_bal']).'</td>
					<td>'.number_format($value['q_rec']).'</td>
					<td>'.number_format($value['q_used']).'</td>
					<td>'.number_format($value['tests_done']).'</td>
					<td>'.number_format($value['losses']).'</td>
					<td>'.number_format($value['positive_adj']).'</td>
					<td>'.number_format($value['negative_adj']).'</td>
					<td>'.number_format($value['closing_bal']).'</td>
				</tr>';
		} 
		
		$table_data.='</tbody></table>';

		
	    echo $table_data;

	}

	public function balance_discrepancy_dashboard($county_id = NULL, $district_id = NULL,$year = NULL,$quarter = NULL,$month = NULL)
	{
		// $page_caching = $this->output->cache(CACHE_REFRESH_INTERVAL);
		$default = "tracer";//FYI
		// echo "<pre>";print_r($district_id);exit;
		// echo "<pre>";print_r($year);exit;
		// $map = $this->render_map();
		$county_id = (isset($county_id) && $county_id > 0)? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id > 0)? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code > 0)? $facility_code:NULL;
		$year = (isset($year) && $year > 0)? $year:NULL;
		$quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;

		$data['filter_years'] = $this->get_years_to_date();

		if (isset($month)) {
            // $year = substr($month, -4);
            $year = date('Y');
            $month = substr($month, 0, 2);
            $monthyear = $year . '-' . $month . '-01';
            $set_month = $month;
        } else {
            $month = $this->session->userdata('Month');
            if ($month == '') {
                $month = date('mY', time());
            }
            // $year = date('Y');
            $month = substr_replace($month, "", -4);
            $monthyear = $year . '-' . $month . '-01';
        }

        // if ($county_id > 0 || $quarter > 0) {
		if ($district_id > 0) {
			$filtered = 1;
			// $quarter_append = ' <span class="font-blue-steel">*</span>';
		}else{
			$filtered = 0;
		}

		$data['filtered'] = $filtered;
		// echo "<pre>";print_r($data);exit;
        $current_date = date('Y-m-d');

        $append_date = (isset($append_date) && $append_date !="")? $append_date : date('Y-m-d');
		
		$county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;

		$page_title = "RTK Dashboard.";
		$data['page_title'] = $page_title;

		$commodity_divisions = Dashboard_model::get_division_details();

		$counties = $this->db->query("SELECT * FROM counties")->result_array();
		$districts = $this->db->query("SELECT * FROM districts")->result_array();
		$facilities = $this->db->query("SELECT * FROM facilities WHERE cd4_enabled = 1")->result_array();

		$filtered_name = 	NULL;
		$district_data = (isset($district_id) && ($district_id > 0)) ? districts::get_district_name($district_id) -> toArray() : NULL;
		// echo "<pre>";print_r($district_data);exit;

        $current = date('m');
        $curMonth = $current;
        $curQuarter = ceil($curMonth/3);//KARSAN

        if ($quarter > 0) {
        	$curQuarter = $quarter;
        	
        }

        $district_drawing_data_s = $district_drawing_data_c = $district_drawing_data_ss = $district_drawing_data_cc = array();
        // echo "<pre>";print_r($year);exit;

        $cache_key = "get_opening_and_closing_balances".$county_id.$district_id.$facility_code.'4'.$beg_date.$end_date.$quarter.'county'.$set_month.$year."county".date('Y-m-d');
        // echo "<pre>";print_r($cache_key);exit;
        $cache_check = $this->memcached_check($cache_key);
        // echo "<pre>";print_r($cache_check);exit;
        if ($cache_check['status'] == "FAILED") {
        	$county_balances_scr = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 4 , $beg_date, $end_date, $quarter,'county',$set_month,$year);
        	// echo "<pre>";print_r($balances_scr);exit;

            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($county_balances_scr);

            $cache_save = $this->memcached_save($cache_data);
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $county_balances_scr = json_decode($cache_check['data'],true);
        }
        // echo "<pre>";print_r($county_balances_scr);exit;

        $cache_key = "get_opening_and_closing_balances".$county_id.$district_id.$facility_code.'5'.$beg_date.$end_date.$quarter.'county'.$set_month.$year."county".date('Y-m-d');
        $cache_check = $this->memcached_check($cache_key);
        if ($cache_check['status'] == "FAILED") {
        	$county_balances_conf = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 5 , $beg_date, $end_date, $quarter,'county',$set_month,$year);

            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($county_balances_conf,true);

            $cache_save = $this->memcached_save($cache_data);
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $county_balances_conf = json_decode($cache_check['data'],true);
        }

        $cache_key = "get_opening_and_closing_balances".$county_id.$district_id.$facility_code.'4'.$beg_date.$end_date.$quarter.'facility'.$set_month.$year."facility".date('Y-m-d');
        // echo "<pre>";print_r($cache_key);exit;
        $cache_check = $this->memcached_check($cache_key);
        // echo "<pre>";print_r($cache_check);exit;
        if ($cache_check['status'] == "FAILED") {
        	$balances_scr = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 4 , $beg_date, $end_date, $quarter,'facility',$set_month,$year);
        	// echo "<pre>";print_r($balances_scr);exit;

            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($balances_scr);

            $cache_save = $this->memcached_save($cache_data);
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $balances_scr = json_decode($cache_check['data'],true);
        }
        // echo "<pre>";print_r($balances_scr);exit;

        $cache_key = "get_opening_and_closing_balances".$county_id.$district_id.$facility_code.'5'.$beg_date.$end_date.$quarter.'facility'.$set_month.$year."facility".date('Y-m-d');
        $cache_check = $this->memcached_check($cache_key);
        if ($cache_check['status'] == "FAILED") {
        	$balances_conf = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 5 , $beg_date, $end_date, $quarter,'facility',$set_month,$year);

            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($balances_conf,true);

            $cache_save = $this->memcached_save($cache_data);
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $balances_conf = json_decode($cache_check['data'],true);
        }

        $cache_key = "get_opening_and_closing_balances".$county_id.$district_id.$facility_code.'4'.$beg_date.$end_date.$quarter.'NULL'.$set_month.$year.date('Y-m-d');
        $cache_check = $this->memcached_check($cache_key);
        if ($cache_check['status'] == "FAILED") {
        	$nat_balances_scr = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 4 , $beg_date, $end_date, $quarter,NULL,$set_month,$year);
        	// echo "<pre>HERE: ";print_r($nat_balances_scr);exit;
            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($nat_balances_scr,true);

            $cache_save = $this->memcached_save($cache_data);
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $nat_balances_scr = json_decode($cache_check['data'],true);
        }

        // echo "<pre>";print_r($nat_balances_scr);exit;

        $cache_key = "get_opening_and_closing_balances".$county_id.$district_id.$facility_code.'5'.$beg_date.$end_date.$quarter.'NULL'.$set_month.$year.date('Y-m-d');
        $cache_check = $this->memcached_check($cache_key);
        if ($cache_check['status'] == "FAILED") {
        	$nat_balances_conf = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 5 , $beg_date, $end_date, $quarter,NULL,$set_month,$year);

            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($nat_balances_conf,true);

            $cache_save = $this->memcached_save($cache_data);
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $nat_balances_conf = json_decode($cache_check['data'],true);
        }

        $year_ = (isset($year) && $year > 0)?$year:date('Y');

        $cache_key = "summary_data_s".$district_id.$quarter_."facility".$year_.date('Y-m-d');
        // echo "<pre>";print_r($cache_key);exit;
        // echo "<pre>";print_r(md5($cache_key));exit;
        $cache_check = $this->memcached_check($cache_key);
        // echo "<pre>";print_r($cache_check);exit;
        if ($cache_check['status'] == "FAILED") {
	        $summary_data_s = $this->get_order_data(NULL, $district_id, NULL, 4 , $quarter_,"facility",$year_);
	        // echo "<pre>summary_data_s: ";print_r($summary_data_s);exit;

            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($summary_data_s);

            $cache_save = $this->memcached_save($cache_data);
            // echo "<pre>Save: ";print_r($cache_save);exit;
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $summary_data_s = json_decode($cache_check['data'],TRUE);
        }
        // echo "<pre>DECODED: ";print_r($summary_data_s);exit;	

        $cache_key = "summary_data_c".$district_id.$quarter_."facility".$year_.date('Y-m-d');
        $cache_check = $this->memcached_check($cache_key);
        if ($cache_check['status'] == "FAILED") {
			$summary_data_c = $this->get_order_data(NULL, $district_id, NULL, 5 , $quarter_,"facility",$year_);
	        // echo "<pre>";print_r($summary_data_s);exit;

            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($summary_data_c);

            $cache_save = $this->memcached_save($cache_data);
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $summary_data_c = json_decode($cache_check['data'],TRUE);
        }


        // echo "<pre>THIS: ";print_r($nat_balances_scr);exit;

        


        $national_bal_s = $county_bal_s = $county_bal_s_ = $dist_bal_s = $dist_bal_s_ = $fac_bal_s = $fac_bal_s_ = array();
        $national_bal_c = $county_bal_c = $county_bal_c_ = $dist_bal_c = $dist_bal_c_ = $fac_bal_c = $fac_bal_c_ = array();

        // echo "<pre>";print_r($counties);exit;
        // echo "<pre>";print_r($balances_scr);exit;
        foreach ($balances_scr as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
            $c_id = $value['county_id'];
            $d_id = $value['district_id'];
            $f_code = $value['facility_code'];
            $comm_id = $value['commodity_id'];

        	if (count($county_bal[$c_id][$comm_id]) > 0) {

        		if (date('Y-m-d',strtotime($value['beginning_order_date'])) > $county_bal[$c_id][$comm_id]['beginning_order_date']) {
        			$county_bal[$c_id][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		}

        		$county_bal[$c_id][$comm_id]['beginning_balance'] += $value['beginning_balance'];

        		if (date('Y-m-d',strtotime($value['closing_order_date'])) > $county_bal[$c_id][$comm_id]['closing_order_date']) {
        			$county_bal[$c_id][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		}

        		$county_bal[$c_id][$comm_id]['closing_balance'] += $value['closing_balance'];

        	}else{
        		$county_bal[$c_id][$comm_id]['county_id'] = $value['county_id'];
        		$county_bal[$c_id][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$county_bal[$c_id][$comm_id]['beginning_balance'] = $value['beginning_balance'];
        		$county_bal[$c_id][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$county_bal[$c_id][$comm_id]['closing_balance'] = $value['closing_balance'];
        	}

        	if (count($dist_bal[$d_id]) > 0) {
        		$dist_bal[$d_id][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$dist_bal[$d_id][$comm_id]['beginning_balance'] += $value['beginning_balance'];
        		$dist_bal[$d_id][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$dist_bal[$d_id][$comm_id]['closing_balance'] += $value['closing_balance'];
        	}else{
        		$dist_bal[$d_id][$comm_id]['county_id'] = $value['county_id'];
        		$dist_bal[$d_id][$comm_id]['district_id'] = $value['district_id'];
        		$dist_bal[$d_id][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$dist_bal[$d_id][$comm_id]['beginning_balance'] = $value['beginning_balance'];
        		$dist_bal[$d_id][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$dist_bal[$d_id][$comm_id]['closing_balance'] = $value['closing_balance'];
        	}

        	if (count($fac_bal[$f_code]) > 0) {
        		$fac_bal[$f_code][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$fac_bal[$f_code][$comm_id]['beginning_balance'] += $value['beginning_balance'];
        		$fac_bal[$f_code][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$fac_bal[$f_code][$comm_id]['closing_balance'] += $value['closing_balance'];
        	}else{
        		$fac_bal[$f_code][$comm_id]['facility_code'] = $value['facility_code'];
        		$fac_bal[$f_code][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$fac_bal[$f_code][$comm_id]['beginning_balance'] = $value['beginning_balance'];
        		$fac_bal[$f_code][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$fac_bal[$f_code][$comm_id]['closing_balance'] = $value['closing_balance'];
        	}
        	
        }//end of balance data foreach

        // echo "<pre>";print_r($county_bal);exit;
         // echo "<pre>";print_r($counties);exit;
        foreach ($balances_conf as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
        	$c_id = $value['county_id'];
            $d_id = $value['district_id'];
            $f_code = $value['facility_code'];
            $comm_id = $value['commodity_id'];

        	if (count($county_bal[$c_id]) > 0) {
        		$county_bal[$c_id][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$county_bal[$c_id][$comm_id]['beginning_balance'] += $value['beginning_balance'];
        		$county_bal[$c_id][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$county_bal[$c_id][$comm_id]['closing_balance'] += $value['closing_balance'];
        	}else{
        		$county_bal[$c_id][$comm_id]['county_id'] = $value['county_id'];
        		$county_bal[$c_id][$comm_id]['facility_code'] = $value['facility_code'];
        		$county_bal[$c_id][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$county_bal[$c_id][$comm_id]['beginning_balance'] = $value['beginning_balance'];
        		$county_bal[$c_id][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$county_bal[$c_id][$comm_id]['closing_balance'] = $value['closing_balance'];

        	}

        	if (count($dist_bal[$d_id]) > 0) {
        		$dist_bal[$d_id][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$dist_bal[$d_id][$comm_id]['beginning_balance'] += $value['beginning_balance'];
        		$dist_bal[$d_id][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$dist_bal[$d_id][$comm_id]['closing_balance'] += $value['closing_balance'];
        	}else{
        		$dist_bal[$d_id][$comm_id]['county_id'] = $value['county_id'];
        		$dist_bal[$d_id][$comm_id]['district_id'] = $value['district_id'];
        		$dist_bal[$d_id][$comm_id]['facility_code'] = $value['facility_code'];
                $dist_bal[$d_id][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$dist_bal[$d_id][$comm_id]['beginning_balance'] = $value['beginning_balance'];
        		$dist_bal[$d_id][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$dist_bal[$d_id][$comm_id]['closing_balance'] = $value['closing_balance'];
        	}

        	if (count($fac_bal[$f_code]) > 0) {
        		$fac_bal[$f_code][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$fac_bal[$f_code][$comm_id]['beginning_balance'] += $value['beginning_balance'];
        		$fac_bal[$f_code][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$fac_bal[$f_code][$comm_id]['closing_balance'] += $value['closing_balance'];
        	}else{
        		$fac_bal[$f_code][$comm_id]['county_id'] = $value['county_id'];
        		$fac_bal[$f_code][$comm_id]['district_id'] = $value['district_id'];
        		$fac_bal[$f_code][$comm_id]['facility_code'] = $value['facility_code'];
        		$fac_bal[$f_code][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$fac_bal[$f_code][$comm_id]['beginning_balance'] = $value['beginning_balance'];
        		$fac_bal[$f_code][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$fac_bal[$f_code][$comm_id]['closing_balance'] = $value['closing_balance'];
        	}
        	
        }//end of balance data foreach

        // echo "<pre>";print_r($county_bal);exit;
        // echo "<pre>";print_r($national_bal);exit;

        // echo "<pre>";print_r($summary_data_s);exit;
        $national_arr_s = $county_arr_s = $county_arr_s_ = $dist_arr_s = $dist_arr_s_ = $fac_arr_s = $fac_arr_s_ = array();
        $national_arr_c = $county_arr_c = $county_arr_c_ = $dist_arr_c = $dist_arr_c_ = $fac_arr_c = $fac_arr_c_ = array();

        // echo "<pre>";print_r($county_balances_scr);exit;
    	$national_arr_s['beginning_bal'] = $nat_balances_scr['beginning_balance']['beginning_balance'];
    	$national_arr_s['beginning_bal_order_date'] = $nat_balances_scr['beginning_balance']['order_date'];
		$national_arr_s['closing_stock'] = $nat_balances_scr['closing_balance']['closing_balance'];
    	$national_arr_s['closing_bal_order_date'] = $nat_balances_scr['closing_balance']['order_date'];

    	$national_arr_c['beginning_bal'] = $nat_balances_conf['beginning_balance']['beginning_balance'];
    	$national_arr_c['beginning_bal_order_date'] = $nat_balances_conf['beginning_balance']['order_date'];
		$national_arr_c['closing_stock'] = $nat_balances_conf['closing_balance']['closing_balance'];
    	$national_arr_c['closing_bal_order_date'] = $nat_balances_conf['closing_balance']['order_date'];

        foreach ($summary_data_s as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
        	$c_id = $value['county_id'];
        	$c_name = $value['county'];
        	$d_id = $value['district_id'];
        	$d_name = $value['district'];
        	$f_code = $value['facility_code'];
        	$f_name = $value['facility_name'];

	    	// $national_arr_s['beginning_bal'] += $value['beginning_bal'];
			// $national_arr_s['closing_stock'] += $value['closing_stock'];
        	
    		$national_arr_s['q_received'] += $value['q_received'];
    		$national_arr_s['q_used'] += $value['q_used'];
    		$national_arr_s['no_of_tests_done'] += $value['no_of_tests_done'];
    		$national_arr_s['positive_adj'] += $value['positive_adj'];
    		$national_arr_s['negative_adj'] += $value['negative_adj'];
    		$national_arr_s['losses'] += $value['losses'];

    		$national_arr_s['level'] = "national";
    		$national_arr_s['level_id'] = 0;
    		$national_arr_s['commodity_id'] = 4;

        	
        	if (count($county_arr_s[$c_id]) > 0) {
        		// $county_arr_s[$c_id]['beginning_bal'] += $value['beginning_bal'];
        		// $county_arr_s[$c_id]['closing_stock'] += $value['closing_stock'];

        		$county_arr_s[$c_id]['q_received'] += $value['q_received'];
        		$county_arr_s[$c_id]['q_used'] += $value['q_used'];
        		$county_arr_s[$c_id]['no_of_tests_done'] += $value['no_of_tests_done'];
        		$county_arr_s[$c_id]['positive_adj'] += $value['positive_adj'];
        		$county_arr_s[$c_id]['negative_adj'] += $value['negative_adj'];
        		$county_arr_s[$c_id]['losses'] += $value['losses'];

        	}else{
        		$county_arr_s[$c_id]['county_id'] = $value['county_id'];
        		$county_arr_s[$c_id]['county_name'] = $value['county'];
        		$county_arr_s[$c_id]['district_id'] = $value['district_id'];
        		$county_arr_s[$c_id]['district_name'] = $value['district'];
        		$county_arr_s[$c_id]['facility_code'] = $value['facility_code'];
        		$county_arr_s[$c_id]['facility_name'] = $value['facility_name'];
        		$county_arr_s[$c_id]['order_date'] = $value['order_date'];
        		
        		// $county_arr_s[$c_id]['beginning_bal'] = $value['beginning_bal'];
        		// $county_arr_s[$c_id]['closing_stock'] = $value['closing_stock'];

        		$county_arr_s[$c_id]['beginning_bal'] = $county_balances_scr[$c_id]['beginning_balance'];
        		$county_arr_s[$c_id]['beginning_order_date'] = $county_balances_scr[$c_id]['beginning_order_date'];
        		$county_arr_s[$c_id]['closing_stock'] = $county_balances_scr[$c_id]['closing_balance'];
        		$county_arr_s[$c_id]['closing_order_date'] = $county_balances_scr[$c_id]['closing_order_date'];
        		
        		$county_arr_s[$c_id]['q_received'] = $value['q_received'];
        		$county_arr_s[$c_id]['q_used'] = $value['q_used'];
        		$county_arr_s[$c_id]['no_of_tests_done'] = $value['no_of_tests_done'];
        		$county_arr_s[$c_id]['positive_adj'] = $value['positive_adj'];
        		$county_arr_s[$c_id]['negative_adj'] = $value['negative_adj'];
        		$county_arr_s[$c_id]['losses'] = $value['losses'];

        		$county_arr_s[$c_id]['level'] = "county";
        		$county_arr_s[$c_id]['level_id'] = $value['county_id'];
        		$county_arr_s[$c_id]['commodity_id'] = 4;

        	}

        	if (count($dist_arr_s[$d_id]) > 0) {
        		// $dist_arr_s[$d_id]['beginning_bal'] += $value['beginning_bal'];
        		// $dist_arr_s[$d_id]['closing_stock'] += $value['closing_stock'];
        		
        		$dist_arr_s[$d_id]['q_received'] += $value['q_received'];
        		$dist_arr_s[$d_id]['q_used'] += $value['q_used'];
        		$dist_arr_s[$d_id]['no_of_tests_done'] += $value['no_of_tests_done'];
        		$dist_arr_s[$d_id]['positive_adj'] += $value['positive_adj'];
        		$dist_arr_s[$d_id]['negative_adj'] += $value['negative_adj'];
        		$dist_arr_s[$d_id]['losses'] += $value['losses'];
        	}else{
        		$dist_arr_s[$d_id]['county_id'] = $value['county_id'];
        		$dist_arr_s[$d_id]['county_name'] = $value['county'];
        		$dist_arr_s[$d_id]['district_id'] = $value['district_id'];
        		$dist_arr_s[$d_id]['district_name'] = $value['district'];
        		$dist_arr_s[$d_id]['facility_code'] = $value['facility_code'];
        		$dist_arr_s[$d_id]['facility_name'] = $value['facility_name'];
        		$dist_arr_s[$d_id]['order_date'] = $value['order_date'];

        		// $dist_arr_s[$d_id]['beginning_bal'] = $value['beginning_bal'];
        		// $dist_arr_s[$d_id]['closing_stock'] = $value['closing_stock'];

        		$dist_arr_s[$d_id]['beginning_bal'] = $dist_bal[$d_id][4]['beginning_balance'];
        		$dist_arr_s[$d_id]['beginning_order_date'] = $dist_bal[$d_id][4]['beginning_order_date'];
        		$dist_arr_s[$d_id]['closing_stock'] = $dist_bal[$d_id][4]['closing_balance'];
        		$dist_arr_s[$d_id]['closing_order_date'] = $dist_bal[$d_id][4]['closing_order_date'];
        		
        		$dist_arr_s[$d_id]['q_received'] = $value['q_received'];
        		$dist_arr_s[$d_id]['q_used'] = $value['q_used'];
        		$dist_arr_s[$d_id]['no_of_tests_done'] = $value['no_of_tests_done'];
        		$dist_arr_s[$d_id]['positive_adj'] = $value['positive_adj'];
        		$dist_arr_s[$d_id]['negative_adj'] = $value['negative_adj'];
        		$dist_arr_s[$d_id]['losses'] = $value['losses'];

        		$dist_arr_s[$d_id]['level'] = "subcounty";
        		$dist_arr_s[$d_id]['level_id'] = $value['district_id'];
        		$dist_arr_s[$d_id]['commodity_id'] = 4;
        	}

        	if (count($fac_arr_s[$f_code]) > 0) {
        		// $fac_arr_s[$f_code]['beginning_bal'] += $value['beginning_bal'];
        		// $fac_arr_s[$f_code]['closing_stock'] += $value['closing_stock'];

        		$fac_arr_s[$f_code]['q_received'] += $value['q_received'];
        		$fac_arr_s[$f_code]['q_used'] += $value['q_used'];
        		$fac_arr_s[$f_code]['no_of_tests_done'] += $value['no_of_tests_done'];
        		$fac_arr_s[$f_code]['positive_adj'] += $value['positive_adj'];
        		$fac_arr_s[$f_code]['negative_adj'] += $value['negative_adj'];
        		$fac_arr_s[$f_code]['losses'] += $value['losses'];
        	}else{
        		$fac_arr_s[$f_code]['county_id'] = $value['county_id'];
        		$fac_arr_s[$f_code]['county_name'] = $value['county'];
        		$fac_arr_s[$f_code]['district_id'] = $value['district_id'];
        		$fac_arr_s[$f_code]['district_name'] = $value['district'];
        		$fac_arr_s[$f_code]['facility_code'] = $value['facility_code'];
        		$fac_arr_s[$f_code]['facility_name'] = $value['facility_name'];
        		$fac_arr_s[$f_code]['order_date'] = $value['order_date'];
        		
        		// $fac_arr_s[$f_code]['beginning_bal'] = $value['beginning_bal'];
        		// $fac_arr_s[$f_code]['closing_stock'] = $value['closing_stock'];
        		
        		$fac_arr_s[$f_code]['beginning_bal'] = $fac_bal[$f_code][4]['beginning_balance'];
        		$fac_arr_s[$f_code]['beginning_order_date'] = $fac_bal[$f_code][4]['beginning_order_date'];
        		$fac_arr_s[$f_code]['closing_stock'] = $fac_bal[$f_code][4]['closing_balance'];
        		$fac_arr_s[$f_code]['closing_order_date'] = $fac_bal[$f_code][4]['closing_order_date'];

        		$fac_arr_s[$f_code]['q_received'] = $value['q_received'];
        		$fac_arr_s[$f_code]['q_used'] = $value['q_used'];
        		$fac_arr_s[$f_code]['no_of_tests_done'] = $value['no_of_tests_done'];
        		$fac_arr_s[$f_code]['positive_adj'] = $value['positive_adj'];
        		$fac_arr_s[$f_code]['negative_adj'] = $value['negative_adj'];
        		$fac_arr_s[$f_code]['losses'] = $value['losses'];

        		$fac_arr_s[$f_code]['level'] = "facility";
        		$fac_arr_s[$f_code]['level_id'] = $value['facility_code'];
        		$fac_arr_s[$f_code]['commodity_id'] = 4;

        	}
        	
        }//end of summary data foreach

         // echo "<pre>";print_r($counties);exit;
        foreach ($summary_data_c as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
        	$c_id = $value['county_id'];
        	$c_name = $value['county'];
        	$d_id = $value['district_id'];
        	$d_name = $value['district'];
        	$f_code = $value['facility_code'];
        	$f_name = $value['facility_name'];

        	// $national_arr_c['beginning_bal'] += $value['beginning_bal'];
    		// $national_arr_c['closing_stock'] += $value['closing_stock'];

    		$national_arr_c['q_received'] += $value['q_received'];
    		$national_arr_c['q_used'] += $value['q_used'];
    		$national_arr_c['no_of_tests_done'] += $value['no_of_tests_done'];
    		$national_arr_c['positive_adj'] += $value['positive_adj'];
    		$national_arr_c['negative_adj'] += $value['negative_adj'];
    		$national_arr_c['losses'] += $value['losses'];

    		$national_arr_c['level'] = "national";
    		$national_arr_c['level_id'] = 0;
    		$national_arr_c['commodity_id'] = 5;
        	
        	if (count($county_arr_c[$c_id]) > 0) {
        		// $county_arr_c[$c_id]['beginning_bal'] += $value['beginning_bal'];
        		// $county_arr_c[$c_id]['closing_stock'] += $value['closing_stock'];
        		
        		$county_arr_c[$c_id]['q_received'] += $value['q_received'];
        		$county_arr_c[$c_id]['q_used'] += $value['q_used'];
        		$county_arr_c[$c_id]['no_of_tests_done'] += $value['no_of_tests_done'];
        		$county_arr_c[$c_id]['positive_adj'] += $value['positive_adj'];
        		$county_arr_c[$c_id]['negative_adj'] += $value['negative_adj'];
        		$county_arr_c[$c_id]['losses'] += $value['losses'];
        	}else{
        		$county_arr_c[$c_id]['county_id'] = $value['county_id'];
        		$county_arr_c[$c_id]['county_name'] = $value['county'];
        		$county_arr_c[$c_id]['district_id'] = $value['district_id'];
        		$county_arr_c[$c_id]['district_name'] = $value['district'];
        		$county_arr_c[$c_id]['facility_code'] = $value['facility_code'];
        		$county_arr_c[$c_id]['facility_name'] = $value['facility_name'];
        		$county_arr_c[$c_id]['order_date'] = $value['order_date'];

        		// $county_arr_c[$c_id]['beginning_bal'] = $value['beginning_bal'];
        		// $county_arr_c[$c_id]['closing_stock'] = $value['closing_stock'];
        			
        		$county_arr_c[$c_id]['beginning_bal'] = $county_balances_conf[$c_id]['beginning_balance'];
        		$county_arr_c[$c_id]['beginning_order_date'] = $county_balances_conf[$c_id]['beginning_order_date'];
        		$county_arr_c[$c_id]['closing_stock'] = $county_balances_conf[$c_id]['closing_balance'];
        		$county_arr_c[$c_id]['closing_order_date'] = $county_balances_conf[$c_id]['closing_order_date'];

        		$county_arr_c[$c_id]['q_received'] = $value['q_received'];
        		$county_arr_c[$c_id]['q_used'] = $value['q_used'];
        		$county_arr_c[$c_id]['no_of_tests_done'] = $value['no_of_tests_done'];
        		$county_arr_c[$c_id]['positive_adj'] = $value['positive_adj'];
        		$county_arr_c[$c_id]['negative_adj'] = $value['negative_adj'];
        		$county_arr_c[$c_id]['losses'] = $value['losses'];

        		$county_arr_c[$c_id]['level'] = "county";
        		$county_arr_c[$c_id]['level_id'] = $value['county_id'];
        		$county_arr_c[$c_id]['commodity_id'] = 5;
        	}

        	if (count($dist_arr_c[$d_id]) > 0) {
        		// $dist_arr_c[$d_id]['beginning_bal'] += $value['beginning_bal'];
        		// $dist_arr_c[$d_id]['closing_stock'] += $value['closing_stock'];
        		$dist_arr_c[$d_id]['q_received'] += $value['q_received'];
        		$dist_arr_c[$d_id]['q_used'] += $value['q_used'];
        		$dist_arr_c[$d_id]['no_of_tests_done'] += $value['no_of_tests_done'];
        		$dist_arr_c[$d_id]['positive_adj'] += $value['positive_adj'];
        		$dist_arr_c[$d_id]['negative_adj'] += $value['negative_adj'];
        		$dist_arr_c[$d_id]['losses'] += $value['losses'];
        	}else{
        		$dist_arr_c[$d_id]['county_id'] = $value['county_id'];
        		$dist_arr_c[$d_id]['county_name'] = $value['county'];
        		$dist_arr_c[$d_id]['district_id'] = $value['district_id'];
        		$dist_arr_c[$d_id]['district_name'] = $value['district'];
        		$dist_arr_c[$d_id]['facility_code'] = $value['facility_code'];
        		$dist_arr_c[$d_id]['facility_name'] = $value['facility_name'];
        		$dist_arr_c[$d_id]['order_date'] = $value['order_date'];
        		
        		// $dist_arr_c[$d_id]['beginning_bal'] = $value['beginning_bal'];
        		// $dist_arr_c[$d_id]['closing_stock'] = $value['closing_stock'];

        		$dist_arr_c[$d_id]['beginning_bal'] = $dist_bal[$d_id][5]['beginning_balance'];
        		$dist_arr_c[$d_id]['beginning_order_date'] = $dist_bal[$d_id][5]['beginning_order_date'];
        		$dist_arr_c[$d_id]['closing_stock'] = $dist_bal[$d_id][5]['closing_balance'];
        		$dist_arr_c[$d_id]['closing_order_date'] = $dist_bal[$d_id][5]['closing_order_date'];

        		$dist_arr_c[$d_id]['q_received'] = $value['q_received'];
        		$dist_arr_c[$d_id]['q_used'] = $value['q_used'];
        		$dist_arr_c[$d_id]['no_of_tests_done'] = $value['no_of_tests_done'];
        		$dist_arr_c[$d_id]['positive_adj'] = $value['positive_adj'];
        		$dist_arr_c[$d_id]['negative_adj'] = $value['negative_adj'];
        		$dist_arr_c[$d_id]['losses'] = $value['losses'];

        		$dist_arr_c[$d_id]['level'] = "subcounty";
        		$dist_arr_c[$d_id]['level_id'] = $value['district_id'];
        		$dist_arr_c[$d_id]['commodity_id'] = 5;
        	}

        	if (count($fac_arr_c[$f_code]) > 0) {
        		// $fac_arr_c[$f_code]['beginning_bal'] += $value['beginning_bal'];
        		// $fac_arr_c[$f_code]['closing_stock'] += $value['closing_stock'];

        		$fac_arr_c[$f_code]['q_received'] += $value['q_received'];
        		$fac_arr_c[$f_code]['q_used'] += $value['q_used'];
        		$fac_arr_c[$f_code]['no_of_tests_done'] += $value['no_of_tests_done'];
        		$fac_arr_c[$f_code]['positive_adj'] += $value['positive_adj'];
        		$fac_arr_c[$f_code]['negative_adj'] += $value['negative_adj'];
        		$fac_arr_c[$f_code]['losses'] += $value['losses'];
        	}else{
        		$fac_arr_c[$f_code]['county_id'] = $value['county_id'];
        		$fac_arr_c[$f_code]['county_name'] = $value['county'];
        		$fac_arr_c[$f_code]['district_id'] = $value['district_id'];
        		$fac_arr_c[$f_code]['district_name'] = $value['district'];
        		$fac_arr_c[$f_code]['facility_code'] = $value['facility_code'];
        		$fac_arr_c[$f_code]['facility_name'] = $value['facility_name'];
        		$fac_arr_c[$f_code]['order_date'] = $value['order_date'];

        		// $fac_arr_c[$f_code]['beginning_bal'] = $value['beginning_bal'];
        		// $fac_arr_c[$f_code]['closing_stock'] = $value['closing_stock'];
        		$fac_arr_c[$f_code]['beginning_bal'] = $fac_bal[$f_code][5]['beginning_balance'];
        		$fac_arr_c[$f_code]['beginning_order_date'] = $fac_bal[$f_code][5]['beginning_order_date'];
        		$fac_arr_c[$f_code]['closing_stock'] = $fac_bal[$f_code][5]['closing_balance'];
        		$fac_arr_c[$f_code]['closing_order_date'] = $fac_bal[$f_code][5]['closing_order_date'];

        		$fac_arr_c[$f_code]['q_received'] = $value['q_received'];
        		$fac_arr_c[$f_code]['q_used'] = $value['q_used'];
        		$fac_arr_c[$f_code]['no_of_tests_done'] = $value['no_of_tests_done'];
        		$fac_arr_c[$f_code]['positive_adj'] = $value['positive_adj'];
        		$fac_arr_c[$f_code]['negative_adj'] = $value['negative_adj'];
        		$fac_arr_c[$f_code]['losses'] = $value['losses'];

        		$fac_arr_c[$f_code]['level'] = "facility";
        		$fac_arr_c[$f_code]['level_id'] = $value['facility_code'];
        		$fac_arr_c[$f_code]['commodity_id'] = 5;
        	}
        	
        }//end of summary data foreach

        // echo "<pre>";print_r($county_arr_s);exit;

        // echo "<pre>";print_r($county_arr_s);exit;

        // echo "<pre>";print_r($national_arr_s);exit;

        $data['national_order_data_s'] = $national_arr_s;
        $data['county_data_s'] = $county_arr_s;
        $data['district_data_s'] = $dist_arr_s;
        $data['facility_data_s'] = $fac_arr_s;

        $data['national_order_data_c'] = $national_arr_c;
        $data['county_data_c'] = $county_arr_c;
        $data['district_data_c'] = $dist_arr_c;
        $data['facility_data_c'] = $fac_arr_c;

        // echo "<pre>";print_r($data);exit;
        	// echo "<pre>";print_r($ids);exit;
        // exit;
        // echo "<pre>";print_r($fac_arr_s);exit;
        $kemsa_issued_screening = $kemsa_issues['kemsa_issue_data'][0]['screening_units'];
        $kemsa_issued_confirmatory = $kemsa_issues['kemsa_issue_data'][0]['confirmatory_units'];

        $received_site_scr = $summary_data_s['results'][0]['q_received'];
        $received_site_conf = $summary_data_c['results'][0]['q_received'];

        $data['received_site_screening'] = $received_site_scr;
        $data['received_site_confirmatory'] = $received_site_conf;

        $consumed_site_scr = $summary_data_s['results'][0]['q_used'];
        $consumed_site_conf = $summary_data_c['results'][0]['q_used'];

        $data['consumed_site_screening'] = $consumed_site_scr;
        $data['consumed_site_confirmatory'] = $consumed_site_conf;

        $beginning_bal_scr = $summary_data_s['results'][0]['beginning_bal'];
        $beginning_bal_conf = $summary_data_c['results'][0]['beginning_bal'];

        $data['beginning_balance_screening'] = $beginning_bal_scr;
        $data['beginning_balance_confirmatory'] = $beginning_bal_conf;

        // echo "<pre>";print_r($year);exit;
        $balances_scr = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 4 , $beg_date, $end_date, $quarter,NULL,$month,$year);
        $balances_conf = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 5 , $beg_date, $end_date, $quarter,NULL,$month,$year);

		// $expected = ($beg_bal + $rec + $pos_adj)-($used + $losses + $neg_adj); 

        // echo "<pre>";print_r($balances_scr);exit;
        // $closing_bal_scr = $summary_data_s['results'][0]['beginning_bal'];
        // $closing_bal_conf = $summary_data_c['results'][0]['beginning_bal'];

        $closing_bal_scr = $balances_scr['closing_balance']['closing_balance'];
        $closing_bal_conf = $balances_conf['closing_balance']['closing_balance'];
        $closing_bal_order_date = $balances_scr['closing_balance']['order_date'];

        $beginning_bal_scr = $balances_scr['beginning_balance']['beginning_balance'];
        $beginning_bal_conf = $balances_conf['beginning_balance']['beginning_balance'];
        $beginning_bal_order_date = $balances_scr['beginning_balance']['order_date'];

        $data['beginning_balance_screening'] = $beginning_bal_scr;
        $data['beginning_balance_confirmatory'] = $beginning_bal_conf;
        $data['beginning_balance_order_date'] = $beginning_bal_order_date;

        $data['closing_balance_screening'] = $closing_bal_scr;
        $data['closing_balance_confirmatory'] = $closing_bal_conf;
        $data['closing_balance_order_date'] = $closing_bal_order_date;

        

		$title_append = (isset($filtered_name) && $filtered_name !='')? $filtered_name:NULL;

		if ($county_id > 0) {
			$title_append .= " Report";
		}else{
			$title_append .= " as of ".date('F',strtotime("-1 MONTH"))." Report";
		}

		$title_prepend = (isset($county_id) && $county_id > 0)? 'County':'Sub-County';

		$title_append .= (isset($quarter) && $quarter > 0)? ' Quarter: '.$quarter:NULL;
		// echo "<pre>";print_r($title_append);exit;
		if ($quarter > 0) {
			$quartered = 1;
			$quarter_append = ' <span class="font-blue-steel">*</span>';
		}
		// $title_prepend = (isset($title_prepend) && $title_prepend !='')? $title_prepend:'National';
		// echo "<pre>";print_r($data);exit;
		// echo "<pre>";print_r($county_data_s);exit;
		$data['title_append'] = $title_append;
		$data['quartered'] = $quartered;
		$data['quarter_append'] = $quarter_append;
		// $data['tracer_commodities'] = $commodities;
		$data['facility_count'] = $facility_count;
		$data['commodity_count'] = $commodity_count;
		$data['content_view'] = 'dashboard/dashboard';
		$data['county_data'] = $counties;
		$data['title_prepend'] = $title_prepend;
		$data['county_count'] = count($counties_using_CD4);
		$data['district_data'] = $districts;
		$data['commodity_divisions'] = $commodity_divisions;
		$data['title'] = "National Dashboard";
		$data['county_data'] = $counties;
		$data['county_id'] = $county_id;
		$data['subcounty_id'] = $district_id;
		$data['facility_code'] = $facility_code;
		$data['district_data'] = $districts;
		$data['maps'] = $map;
		$data['counties'] = $county_name;
		$this -> load -> view("v2/dashboard/dashboard_discrepancies", $data);
	
	}
	
	public function get_discrepancy_details($level = NULL, $level_id = NULL, $commodity_id = NULL,$order_date = NULL)
	{
		$county_id = $district_id = $facility_code = NULL;
		$commodity_id = ($commodity_id > 0)? $commodity_id:4;
		// echo "<pre>";print_r($level);exit;
		// echo "<pre>";print_r($order_id);exit;
		// echo "<pre>";print_r($order_date);exit;

		if (isset($order_date) && $order_date !='') {
			$m = substr_replace($order_date, "", -4);
			$y = substr($order_date, -4);

			$year = $y;
			$month = $m;
		}

		switch ($level) {
			case 'county':
				$county_id = $level_id;
				$table_heads = "
				<th>County</th>
				<th>Subcounty</th>
				<th>Order Date</th>
				<th>Beginning Balance</th>
				<th>Expected Balance</th>
				<th>Reported</th>
				<th>Difference</th>
				<th>Action</th>
				";
				$sub_level = "subcounty";
				break;
			case 'subcounty':
				$district_id = $level_id;
				$table_heads = "
				<th>Facility Name</th>
				<th>MFL Code</th>
				<th>Sub-County</th>
				<th>County</th>
				<th>Order Date</th>
				<th>Beginning Balance</th>
				<th>Expected Balance</th>
				<th>Reported</th>
				<th>Difference</th>
				<th>Action</th>
				";
				$sub_level = "facility";
				
				break;
			case 'facility':
				$facility_code = $level_id;
				$table_heads = "
				<th>Facility Name</th>
				<th>MFL Code</th>
				<th>Sub-County</th>
				<th>County</th>
				<th>Order Date</th>
				<th>Beginning Balance</th>
				<th>Expected Balance</th>
				<th>Reported</th>
				<th>Difference</th>
				<th>Action</th>
				";
				$sub_level = "facility_orders";
				break;

			case 'order':
				$order_id = $level_id;
				
				$sub_level = 'order';
				break;
			
			default:
				$table_heads = "
				<th>Facility Name</th>
				<th>MFL Code</th>
				<th>Sub-County</th>
				<th>County</th>
				<th>Order Date</th>
				<th>Beginning Balance</th>
				<th>Expected Balance</th>
				<th>Reported</th>
				<th>Difference</th>
				<th>Action</th>
				";
				break;
		}

		// echo "<pre>";print_r($lvl);
		// echo "<pre>";print_r($level);exit;

		// echo "<pre>";print_r($table_heads);exit;

		// $summary_data = $this->get_order_data($county_id, $district_id, $facility_code, $commodity_id , $quarter_,"facility");	
		// $level = "facility";
		// $level = "subcounty";
		// echo "<pre>";print_r($level);
		// echo "<pre>";print_r($lvl);exit;
		$back_button = '';

		if ($level == 'order'){
			$summary_data = $this->get_order_data_by_id($order_id);
			// echo "<pre>";print_r($summary_data);exit;

			$table = '';
			$table .= '<table class="table table-bordered compact table-sm">';
			$table .= '<thead>';
			$table .= '
	                <th>Commodity Name</th>
	                <th>Beginning Balance</th>
	                <th>Quantity Received</th>
	                <th>Quantity Used</th>
	                <th>Tests Done</th>
	                <th>Losses</th>
	                <th>Adj (+ve)</th>	
	                <th>Adj (-ve)</th>	
	                <th>End of Month Physical Count</th>
	                <th>Expected Closing Balance</th>
	                <th>Expiring in <u>less than</u> 6 Months</th>
	                <th>Days out of Stock</th>	
	                <th>Requested for&nbsp;Re-Supply</th>';
			$table .= '</thead>';
			$table .= '<tbody>';

			foreach ($summary_data as $detail) {
				// echo "<pre>";print_r($detail);exit;
				$mfl = $detail['facility_code'];
				$d_id = $detail['district_id'];
				$f_name = $detail['facility_name'];
				$d_name = $detail['district'];
				$report_for = $detail['report_for'];
				$comm_id = $detail['commodity_id'];

				$beg_bal = $detail['beginning_bal'];
				$rec = $detail['q_received'];
				$pos_adj = $detail['positive_adj'];
				$used = $detail['q_used'];
				$losses = $detail['losses'];
				$neg_adj = $detail['negative_adj'];
				$clos_bal = $detail['closing_stock'];

				$expected = ($beg_bal + $rec + $pos_adj)-($used + $losses + $neg_adj); 

				$table .= '
	                    <tr>
	                        <td><b>'. $detail['commodity_name'].'</b></td>
	                        <td>'.number_format($detail['beginning_bal']).'</td>
	                        <td>'.number_format($detail['q_received']).'</td>
	                        <td>'.number_format($detail['q_used']).'</td>
	                        <td>'.number_format($detail['no_of_tests_done']).'</td>
	                        <td>'.number_format($detail['losses']).'</td>
	                        <td>'.number_format($detail['positive_adj']).'</td>
	                        <td>'.number_format($detail['negative_adj']).'</td>
	                        <td><strong>'.number_format($detail['closing_stock']).'</strong></td>
	                        <td><strong>'.number_format($expected).'</strong></td>
	                        <td>'.number_format($detail['q_expiring']).'</td>
	                        <td>'.number_format($detail['days_out_of_stock']).'</td>	
	                        <td>'.number_format($detail['q_requested']).'</td>
	                    </tr>';
	        }

	        $back_button = '<a class="btn btn-primary" id="modal_balance_details" data-level = "subcounty" data-level-id = "'.$d_id.'" data-commodity-id = "'.$commodity_id.'" data-modal-level="subcounty" style="width: 250px;margin: 5px 0px;">Back to Facility List</a>';
	        $title_data = '
	        	<div class="col-md-12">
	        		<h5 class="pull-left">MFL: <strong>'.$mfl.'</strong> </br>Facility Name: <strong>'.$f_name.'</strong> </br>District: <strong>'.$d_name.'</strong></h5>
	        		<h5 class="pull-right">Report for: <strong>'.$report_for.'</strong></h5>
	        		<h5 class="pull-left">Formula Used: <strong>(Beginning Balance + Quantity Received + Positive Adjustments) - (Quantity Used + Losses + Negative Adjustments)</strong></h5>
	        	</div>
	        ';

			$table .= '</tbody>';

			$table .= '</table>';

			$html .= $title_data.$table;

		}else {
			$summary_data = $this->get_order_data($county_id, $district_id, $facility_code, $commodity_id , $quarter_,$sub_level,$year,$order_id,$month);	
			// echo "<pre>";print_r($summary_data);exit;
			// echo "<pre>";print_r($order_date);exit;
			
			// echo "<pre>";print_r($m);exit;
			$balances = $this->get_opening_and_closing_balances_dash_v2($county_id, $district_id, $facility_code, $commodity_id , $quarter_,$sub_level,$m,$y);

			// echo "<pre>";print_r($balances);exit;        
			// echo "<pre>";print_r($summary_data);exit;
			// echo "<pre>";print_r($level);exit;
	        $county_arr_s = $county_arr_s_ = $dist_arr_s = $dist_arr_s_ = $fac_arr_s = $fac_arr_s_ = array();
	        $county_arr_c = $county_arr_c_ = $dist_arr_c = $dist_arr_c_ = $fac_arr_c = $fac_arr_c_ = array();

	        // echo "<pre>";print_r($counties);exit;

			foreach ($summary_data as $key => $value) {
	        	// echo "<pre>";print_r($value);exit;
	        	$c_id = $value['county_id'];
	        	$c_name = $value['county'];
	        	$d_id = $value['district_id'];
	        	$d_name = $value['district'];
	        	$f_code = $value['facility_code'];
	        	$f_name = $value['facility_name'];
	        	$order_id = $value['order_id'];
	        	$od_my = date('mY',strtotime($value['order_date']));

	        	
	        	if (count($county_arr[$c_id]) > 0) {
	        		// $county_arr[$c_id]['beginning_bal'] += $value['beginning_bal'];
	        		// $county_arr[$c_id]['closing_stock'] += $value['closing_stock'];
	        		$county_arr[$c_id]['q_received'] += $value['q_received'];
	        		$county_arr[$c_id]['q_used'] += $value['q_used'];
	        		$county_arr[$c_id]['no_of_tests_done'] += $value['no_of_tests_done'];
	        		$county_arr[$c_id]['positive_adj'] += $value['positive_adj'];
	        		$county_arr[$c_id]['negative_adj'] += $value['negative_adj'];
	        		$county_arr[$c_id]['losses'] += $value['losses'];

	        	}else{
	        		$county_arr[$c_id]['county_id'] = $value['county_id'];
	        		$county_arr[$c_id]['county_name'] = $value['county'];
	        		$county_arr[$c_id]['district_id'] = $value['district_id'];
	        		$county_arr[$c_id]['district_name'] = $value['district'];
	        		$county_arr[$c_id]['facility_code'] = $value['facility_code'];
	        		$county_arr[$c_id]['facility_name'] = $value['facility_name'];
	        		$county_arr[$c_id]['order_id'] = $value['order_id'];
	        		$county_arr[$c_id]['order_date'] = $value['order_date'];

	        		// $county_arr[$c_id]['beginning_bal'] = $value['beginning_bal'];
	        		// $county_arr[$c_id]['closing_stock'] = $value['closing_stock'];

	        		$county_arr[$c_id]['beginning_bal'] = $balances['county_balances'][$c_id][$commodity_id]['beginning_balance'];
	        		$county_arr[$c_id]['beginning_order_date'] = $balances['county_balances'][$c_id][$commodity_id]['beginning_order_date'];
	        		$county_arr[$c_id]['closing_stock'] = $balances['county_balances'][$c_id][$commodity_id]['closing_balance'];
	        		$county_arr[$c_id]['closing_order_date'] = $balances['county_balances'][$c_id][$commodity_id]['closing_order_date'];


	        		$county_arr[$c_id]['q_received'] = $value['q_received'];
	        		$county_arr[$c_id]['q_used'] = $value['q_used'];
	        		$county_arr[$c_id]['no_of_tests_done'] = $value['no_of_tests_done'];
	        		$county_arr[$c_id]['positive_adj'] = $value['positive_adj'];
	        		$county_arr[$c_id]['negative_adj'] = $value['negative_adj'];
	        		$county_arr[$c_id]['losses'] = $value['losses'];

	        		$county_arr[$c_id]['level'] = "county";
	        		$county_arr[$c_id]['level_id'] = $value['county_id'];
	        		$county_arr[$c_id]['commodity_id'] = 4;

	        	}

	        	if (count($dist_arr[$d_id.$od_my]) > 0) {
	        		$dist_arr[$d_id.$od_my]['beginning_bal'] += $value['beginning_bal'];
	        		$dist_arr[$d_id.$od_my]['closing_stock'] += $value['closing_stock'];
	        		$dist_arr[$d_id.$od_my]['q_received'] += $value['q_received'];
	        		$dist_arr[$d_id.$od_my]['q_used'] += $value['q_used'];
	        		$dist_arr[$d_id.$od_my]['no_of_tests_done'] += $value['no_of_tests_done'];
	        		$dist_arr[$d_id.$od_my]['positive_adj'] += $value['positive_adj'];
	        		$dist_arr[$d_id.$od_my]['negative_adj'] += $value['negative_adj'];
	        		$dist_arr[$d_id.$od_my]['losses'] += $value['losses'];
	        	}else{
	        		$dist_arr[$d_id.$od_my]['county_id'] = $value['county_id'];
	        		$dist_arr[$d_id.$od_my]['county_name'] = $value['county'];
	        		$dist_arr[$d_id.$od_my]['district_id'] = $value['district_id'];
	        		$dist_arr[$d_id.$od_my]['district_name'] = $value['district'];
	        		$dist_arr[$d_id.$od_my]['facility_code'] = $value['facility_code'];
	        		$dist_arr[$d_id.$od_my]['facility_name'] = $value['facility_name'];
	        		$dist_arr[$d_id.$od_my]['order_id'] = $value['order_id'];
	        		$dist_arr[$d_id.$od_my]['order_date'] = $value['order_date'];
	        		
	        		$dist_arr[$d_id.$od_my]['beginning_bal'] = $value['beginning_bal'];
	        		$dist_arr[$d_id.$od_my]['closing_stock'] = $value['closing_stock'];

	        		// $dist_arr[$d_id.$od_my]['beginning_bal'] = $balances['district_balances'][$d_id][$commodity_id]['beginning_balance'];
	        		// $dist_arr[$d_id.$od_my]['beginning_order_date'] = $balances['district_balances'][$d_id][$commodity_id]['beginning_order_date'];
	        		// $dist_arr[$d_id.$od_my]['closing_stock'] = $balances['district_balances'][$d_id][$commodity_id]['closing_balance'];
	        		// $dist_arr[$d_id.$od_my]['closing_order_date'] = $balances['district_balances'][$d_id][$commodity_id]['closing_order_date'];

	        		$dist_arr[$d_id.$od_my]['q_received'] = $value['q_received'];
	        		$dist_arr[$d_id.$od_my]['q_used'] = $value['q_used'];
	        		$dist_arr[$d_id.$od_my]['no_of_tests_done'] = $value['no_of_tests_done'];
	        		$dist_arr[$d_id.$od_my]['positive_adj'] = $value['positive_adj'];
	        		$dist_arr[$d_id.$od_my]['negative_adj'] = $value['negative_adj'];
	        		$dist_arr[$d_id.$od_my]['losses'] = $value['losses'];

	        		$dist_arr[$d_id.$od_my]['level'] = "subcounty";
	        		$dist_arr[$d_id.$od_my]['level_id'] = $value['district_id'];
	        		$dist_arr[$d_id.$od_my]['commodity_id'] = 4;
	        	}

	        	if (count($fac_arr[$order_id]) > 0) {
	        		$fac_arr[$order_id]['beginning_bal'] += $value['beginning_bal'];
	        		$fac_arr[$order_id]['q_received'] += $value['q_received'];
	        		$fac_arr[$order_id]['q_used'] += $value['q_used'];
	        		$fac_arr[$order_id]['no_of_tests_done'] += $value['no_of_tests_done'];
	        		$fac_arr[$order_id]['closing_stock'] += $value['closing_stock'];
	        		$fac_arr[$order_id]['positive_adj'] += $value['positive_adj'];
	        		$fac_arr[$order_id]['negative_adj'] += $value['negative_adj'];
	        		$fac_arr[$order_id]['losses'] += $value['losses'];
	        	}else{
	        		$fac_arr[$order_id]['county_id'] = $value['county_id'];
	        		$fac_arr[$order_id]['county_name'] = $value['county'];
	        		$fac_arr[$order_id]['district_id'] = $value['district_id'];
	        		$fac_arr[$order_id]['district_name'] = $value['district'];
	        		$fac_arr[$order_id]['facility_code'] = $value['facility_code'];
	        		$fac_arr[$order_id]['facility_name'] = $value['facility_name'];
	        		$fac_arr[$order_id]['order_id'] = $value['order_id'];
	        		$fac_arr[$order_id]['order_date'] = $value['order_date'];
	        		$fac_arr[$order_id]['beginning_bal'] = $value['beginning_bal'];
	        		$fac_arr[$order_id]['q_received'] = $value['q_received'];
	        		$fac_arr[$order_id]['q_used'] = $value['q_used'];
	        		$fac_arr[$order_id]['no_of_tests_done'] = $value['no_of_tests_done'];
	        		$fac_arr[$order_id]['closing_stock'] = $value['closing_stock'];
	        		$fac_arr[$order_id]['positive_adj'] = $value['positive_adj'];
	        		$fac_arr[$order_id]['negative_adj'] = $value['negative_adj'];
	        		$fac_arr[$order_id]['losses'] = $value['losses'];

	        		$fac_arr[$order_id]['level'] = "facility";
	        		$fac_arr[$order_id]['level_id'] = $value['facility_code'];
	        		$fac_arr[$order_id]['commodity_id'] = 4;

	        	}
	        	
	        }//end of summary data foreach

	        // echo "<pre>";print_r($sub_level);exit;
	        // echo "<pre>";print_r($level);exit;
	        // echo "<pre>";print_r($table_heads);exit;
	        // echo "<pre>";print_r($county_arr);exit;
	        // echo "<pre>";print_r($dist_arr);exit;
	        // echo "<pre>";print_r($fac_arr);exit;

	        switch ($sub_level) {
					case 'county':
						$data_arr = $county_arr;
						break;
					case 'subcounty':
						$data_arr = $dist_arr;
						break;
					case 'facility':
						$data_arr = $fac_arr;
						break;
					case 'facility-orders':
						$data_arr = $fac_arr;
						break;
					default:
						$data_arr = $fac_arr;
						break;
			}
			// echo "<pre>";print_r($sub_level);exit;

			// echo "<pre>";print_r($data_arr);exit;

	        $html = '<table class="table table-bordered datatable display cell-border compact" cellspacing="0" width="100%" id="datatable">
						<thead>
							<tr>
								'.$table_heads.'
							</tr>
						</thead>
						<tbody>';

			foreach ($data_arr as $key => $value) { 
				// echo "<pre>";print_r($value);exit;
				$beg_bal = $value['beginning_bal'];
				$rec = $value['q_received'];
				$pos_adj = $value['positive_adj'];
				$used = $value['q_used'];
				$losses = $value['losses'];
				$neg_adj = $value['negative_adj'];

				$clos_bal = $value['closing_stock'];

				$expected = $value['beginning_bal'] - $value['q_used'];
				$reported = $value['closing_stock'];

				// echo "<pre>";print_r($used + $losses + $neg_adj);exit;
				$expected = ($beg_bal + $rec + $pos_adj)-($used + $losses + $neg_adj); 

				// $difference = $expected - $reported;
				$difference = $reported - $expected;
				// echo "<pre>";print_r($difference);exit;
				$class = ($difference>0)? " green ":" red ";
				$class = ($difference==0)? "":$class;
				
				if ($difference != 0 || $sub_level = 'facility'):

				// if ($difference != 0):
					// echo "<pre>";print_r($level);exit;
					// echo "<pre>";print_r($level);
					// echo "<pre>";print_r($lvl);exit;

				switch ($level) {
					case 'county':
						$county_id = $level_id;
						$html .= '
						<tr>
							<td>'.$value['county_name'].'</td>
							<td>'.$value['district_name'].'</td>
							<td>'.date('F Y',strtotime("-1 MONTH",strtotime($value['order_date']))).'</td>
							<td>'.number_format($beg_bal).'</td>
							<td>'.number_format($expected).'</td>
							<td>'.number_format($reported).'</td>
							<td class="'.$class.'"><strong>'.number_format($difference).'</strong></td>';
						break;
					case 'subcounty':
						$district_id = $level_id;
						$html .= '
						<tr>	
							<td>'.$value['facility_name'].'</td>
							<td>'.$value['facility_code'].'</td>
							<td>'.$value['district_name'].'</td>
							<td>'.$value['county_name'].'</td>';
						// $html .= '<td>'.date('F Y',strtotime("-1 MONTH",strtotime($value['order_date']))).'</td>';
						$html .= '<td>'.date('F Y',strtotime("-1 MONTH",strtotime($value['order_date']))).'</td>';
						$html .= '
							<td>'.number_format($beg_bal).'</td>
							<td>'.number_format($expected).'</td>
							<td>'.number_format($reported).'</td>
							<td class="'.$class.'"><strong>'.number_format($difference).'</strong></td>';
						break;
					case 'facility':
						$facility_code = $level_id;
						$html .= '
						<tr>
							<td>'.$value['facility_name'].'</td>
							<td>'.$value['facility_code'].'</td>
							<td>'.$value['district_name'].'</td>
							<td>'.$value['county_name'].'</td>
							<td>'.date('F Y',strtotime("-1 MONTH",strtotime($value['order_date']))).'</td>
							<td>'.number_format($beg_bal).'</td>
							<td>'.number_format($expected).'</td>
							<td>'.number_format($reported).'</td>
							<td class="'.$class.'"><strong>'.number_format($difference).'</strong></td>';
						break;
					
					default:
						$html .= '
						<tr>
							<td>'.$value['facility_name'].'</td>
							<td>'.$value['facility_code'].'</td>
							<td>'.$value['district_name'].'</td>
							<td>'.$value['county_name'].'</td>
							<td>'.date('F Y',strtotime("-1 MONTH",strtotime($value['order_date']))).'</td>
							<td>'.number_format($beg_bal).'</td>
							<td>'.number_format($expected).'</td>
							<td>'.number_format($reported).'</td>
							<td class="'.$class.'"><strong>'.number_format($difference).'</strong></td>';
						break;
				}

				// echo "<pre>";print_r($level);exit;

				// $level = $value['level'];

				// echo "<pre>";print_r($level);exit;
				// echo "<pre>";print_r($lvl);exit;
				// echo "<pre>";print_r($value);exit;
				// echo "<pre>";print_r($sub_level);exit;

				switch ($level) {
					case 'county':
						/*$html .= '<td><a class="btn btn-primary" id="modal_balance_details" data-level = "'.$value['level'].'" data-level-id = "'.$value['level_id'].'" data-commodity-id = "'.$commodity_id.'" data-modal-level="county">View Details</a></td>';*/
						// echo "BUTTON LEVEL: COUNTY";exit;
						$html .= '<td><a class="btn btn-primary" id="modal_balance_details" data-level = "'.$value['level'].'" data-level-id = "'.$value['district_id'].'" data-commodity-id = "'.$commodity_id.'" data-order-date="'.date('mY',strtotime($value['order_date'])).'" data-modal-level="subcounty">View Details</a></td>';
						break;

					case 'subcounty':
						/*$html .= '<td><a class="btn btn-primary" id="modal_balance_details" data-level = "'.$value['level'].'" data-level-id = "'.$value['level_id'].'" data-commodity-id = "'.$commodity_id.'" data-modal-level="subcounty">View Details</a></td>';*/

						// echo "BUTTON LEVEL: SUBCOUNTY";exit;
						// $html .= '<td><a class="btn btn-primary" id="modal_balance_details" data-level = "'.$value['level'].'" data-level-id = "'.$value['level_id'].'" data-commodity-id = "'.$commodity_id.'" data-modal-level="facility">View FCDRR</a></td>';
						
						// $html .= '<td><a class="btn btn-primary" id="modal_balance_details" data-level = "'.$value['level'].'" data-level-id = "'.$value['facility_code'].'" data-commodity-id = "'.$commodity_id.'" data-order-id = "'.$value['order_id'].'"  data-modal-level="facility">View FCDRR</a></td>';

						// $html .= '<td><a class="btn btn-primary" data-level = "order" data-level-id = "'.$value['order_id'].'" data-commodity-id = "'.$commodity_id.'" data-order-id = "'.$value['order_id'].'"  data-modal-level="facility">View FCDRR</a></td>';

						$html .= '<td><a class="btn btn-primary" id="modal_balance_details" data-level = "order" data-level-id = "'.$value['order_id'].'" data-commodity-id = "'.$commodity_id.'" data-modal-level="order">View FCDRR</a></td>';

						$back_button = '<a class="btn btn-primary col-md-3 pull-left" id="modal_balance_details" data-level = "county" data-level-id = "'.$value['county_id'].'" data-commodity-id = "'.$commodity_id.'" data-modal-level="county" style="width: 250px;margin: 5px 0px;">Back to Subounty List</a>';

						break;

					case 'facility':
						// echo "BUTTON LEVEL: FACILITY";exit;
						$html .= '<td><a class="btn btn-primary" id="modal_balance_details" data-level = "'.$value['level'].'" data-level-id = "'.$value['level_id'].'" data-commodity-id = "'.$commodity_id.'" data-order-id = "'.$value['order_id'].'" data-modal-level="facility">View Orders</a></td>';
						$back_button = '<td><a class="btn btn-primary" id="modal_balance_details" data-level = "subcounty" data-level-id = "'.$value['district_id'].'" data-commodity-id = "'.$commodity_id.'" data-modal-level="subcounty" style="width: 250px;margin: 5px 0px;">Back to Subcounty List</a></td>';
						break;
					
					default:
						// echo "BUTTON LEVEL: COUNTY";exit;
						$html .= '<td><a class="btn btn-primary" id="modal_balance_details" data-level = "'.$value['level'].'" data-level-id = "'.$value['level_id'].'" data-commodity-id = "'.$commodity_id.'" data-order-id = "'.$value['order_id'].'" data-modal-level="county">View Details</a></td>';
						break;
				}

				$html .= '</tr>';

				endif;
			}
			
			$html .='<tbody></table>';
		}
		
		echo $back_button.$html;
	}

	public function issues_discrepancy_dashboard($county_id = NULL, $district_id = NULL,$quarter = NULL,$month = NULL,$year = NULL)
	{
		// $page_caching = $this->output->cache(CACHE_REFRESH_INTERVAL);
		$default = "tracer";//FYI
		// echo "<pre>";print_r($district_id);exit;
		// echo "<pre>";print_r($year);exit;
		// $map = $this->render_map();
		$county_id = (isset($county_id) && $county_id > 0)? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id > 0)? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code > 0)? $facility_code:NULL;
		$quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;
		$year = (isset($year) && $year > 0)? $year:date('Y');

		$data['filter_years'] = $this->get_years_to_date();
		$filtered_name = NULL;

		// $quarters = array();
		$prev_yr = date("Y",strtotime('-1 YEAR'));
		$quarters_prev_yr = $this->get_year_quarter(NULL,NULL,$prev_yr,1);
		$quarters_curr_yr = $this->get_year_quarter(NULL,NULL,NULL,1);
		// echo "<pre>";print_r($quarters_prev_yr);exit;
		$data['quarters'] = array_merge_recursive($quarters_curr_yr, $quarters_prev_yr);
		// echo "<pre>";print_r($quarters);exit;
		// echo "<pre>";print_r($quarter);exit;
		if (isset($month)) {
            // $year = substr($month, -4);
            // $year = date('Y');
            $month = substr($month, 0, 2);
            $monthyear = $year . '-' . $month . '-01';

        } else {
            $month = $this->session->userdata('Month');
            if ($month == '') {
                $month = date('mY', time());
            }
            // $year = date('Y');
            $month = substr_replace($month, "", -4);
            $monthyear = $year . '-' . $month . '-01';
        }

        // if ($county_id > 0 || $quarter > 0) {
		if ($quarter > 0) {
			$filtered = 1;
			// $quarter_append = ' <span class="font-blue-steel">*</span>';
		}else{
			$filtered = 0;
		}

		if (isset($quarter) && $quarter > 0) {
			$q_data = explode('_', $quarter);
			// echo "<pre>";print_r($q_data);exit;
			$quarter = $q_data[0];
			$year = $q_data[1];
			$filtered_name = "Quarter ".$quarter." ".$year;
		}else{}

		$data['filtered'] = $filtered;
		// echo "<pre>";print_r($data);exit;
        $current_date = date('Y-m-d');

        $append_date = (isset($append_date) && $append_date !="")? $append_date : date('Y-m-d');
		
		$county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;

		$page_title = "RTK Issues Discrepancy Dashboard.";
		$data['page_title'] = $page_title;

		$commodity_divisions = Dashboard_model::get_division_details();

		$counties = $this->db->query("SELECT * FROM counties")->result_array();
		$districts = $this->db->query("SELECT * FROM districts")->result_array();
		$facilities = $this->db->query("SELECT * FROM facilities WHERE cd4_enabled = 1")->result_array();

		$facility_count = Dashboard_model::get_online_offline_facility_count();
		$counties_using_CD4 = Counties::get_counties_all_using_CD4();

		$district_data = (isset($district_id) && ($district_id > 0)) ? districts::get_district_name($district_id) -> toArray() : NULL;
		// echo "<pre>";print_r($district_data);exit;

		$county_search = counties::get_county_name($county_id);
		if (count($county_search) > 0) {
			// $filtered_name = $county_search['county']." County $append_date";
			$filtered_name = $county_search['county']." County";
		}
		
		if ($district_data != NULL) {
			// $filtered_name = $district_data[0]['district']." Sub-County $append_date";
			$filtered_name = $district_data[0]['district']." Sub-County";
		}

		// echo "<pre>";print_r($county_name);exit;
		$county_name = "";

		$summary_and = "";
        $summary_and .= (isset($district_id) && $district_id!='')? " WHERE district_id = $district_id" :NULL; 
        
        $d_drawing_query = "SELECT * FROM district_drawing_rights $summary_and";
		// echo $c_drawing_query;exit;
        $district_drawing_data = $this->db->query($d_drawing_query)->result_array();

        // echo "<pre>";print_r($district_drawing_data);exit;
    	$drawing_total_confirmatory = $drawing_total_screening = 0;
    	$drawing_used_confirmatory = $drawing_used_screening = 0;
    	$drawing_balance_confirmatory = $drawing_balance_screening = 0;
    	$county_scr_distributed_aggr = $county_conf_distributed_aggr = 0;
    	$county_scr_total_drawing_rights = $county_conf_total_drawing_rights = 0;

        $current = date('m');
        $curMonth = $current;
        $curQuarter = ceil($curMonth/3);//KARSAN

        if ($quarter > 0) {
        	$curQuarter = $quarter;
        }
        // echo "<pre>";print_r($curQuarter);exit;

        // echo "<pre>";print_r($district_drawing_data);exit;
        // echo "<pre>";print_r($quarter);
        // echo "<pre>";print_r($year);exit;
        $kemsa_issue_data_s = $this->get_kemsa_issue_data($county_id, $district_id, NULL, 4 , $quarter,$year);
        // echo "<pre>";print_r($kemsa_issue_data_s);exit;
        $kemsa_issue_data_c = $this->get_kemsa_issue_data($county_id, $district_id, NULL, 5 , $quarter,$year);
        // echo "<pre>";print_r($kemsa_issue_data_c);exit;	

        // echo "<pre>";print_r($summary_data_s);exit;

        $kemsa_county_arr_s = $kemsa_county_arr_s_ = $kemsa_dist_arr_s = $kemsa_dist_arr_s_ = $kemsa_fac_arr_s = $kemsa_fac_arr_s_ = array();
        $kemsa_county_arr_c = $kemsa_county_arr_c_ = $kemsa_dist_arr_c = $kemsa_dist_arr_c_ = $kemsa_fac_arr_c = $kemsa_fac_arr_c_ = array();

        // echo "<pre>";print_r($counties);exit;
        foreach ($kemsa_issue_data_s as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
        	$c_id = $value['county_id'];
        	$c_name = $value['county'];
        	$d_id = $value['district_id'];
        	$d_name = $value['district'];
        	$f_code = $value['facility_code'];
        	$f_name = $value['facility_name'];

        	
        	if (count($kemsa_county_arr_s[$c_id]) > 0) {
        		$kemsa_county_arr_s[$c_id]['qty_issued'] += $value['qty_issued'];
        		$kemsa_county_arr_s[$c_id]['qty_requested'] += $value['qty_requested'];

        	}else{

        		$kemsa_county_arr_s[$c_id]['county_id'] = $value['county_id'];
        		$kemsa_county_arr_s[$c_id]['county_name'] = $value['county'];
        		$kemsa_county_arr_s[$c_id]['district_id'] = $value['district_id'];
        		$kemsa_county_arr_s[$c_id]['district_name'] = $value['district'];
        		$kemsa_county_arr_s[$c_id]['facility_code'] = $value['facility_code'];
        		$kemsa_county_arr_s[$c_id]['facility_name'] = $value['facility_name'];

        		$kemsa_county_arr_s[$c_id]['issue_date'] = $value['issue_date'];
        		$kemsa_county_arr_s[$c_id]['qty_issued'] = $value['qty_issued'];
        		$kemsa_county_arr_s[$c_id]['qty_requested'] = $value['qty_requested'];

        		$kemsa_county_arr_s[$c_id]['level'] = "county";
        		$kemsa_county_arr_s[$c_id]['level_id'] = $value['county_id'];
        		$kemsa_county_arr_s[$c_id]['commodity_id'] = 4;
        		$kemsa_county_arr_s[$c_id]['quarter'] = $quarter;
        		$kemsa_county_arr_s[$c_id]['year'] = $year;

        	}

        	if (count($kemsa_dist_arr_s[$d_id]) > 0) {
        		$kemsa_dist_arr_s[$d_id]['qty_issued'] += $value['qty_issued'];
        		$kemsa_dist_arr_s[$d_id]['qty_requested'] += $value['qty_requested'];
        	}else{
        		$kemsa_dist_arr_s[$d_id]['county_id'] = $value['county_id'];
        		$kemsa_dist_arr_s[$d_id]['county_name'] = $value['county'];
        		$kemsa_dist_arr_s[$d_id]['district_id'] = $value['district_id'];
        		$kemsa_dist_arr_s[$d_id]['district_name'] = $value['district'];
        		$kemsa_dist_arr_s[$d_id]['facility_code'] = $value['facility_code'];
        		$kemsa_dist_arr_s[$d_id]['facility_name'] = $value['facility_name'];
        		$kemsa_dist_arr_s[$d_id]['issue_date'] = $value['issue_date'];

        		$kemsa_dist_arr_s[$d_id]['qty_issued'] = $value['qty_issued'];
        		$kemsa_dist_arr_s[$d_id]['qty_requested'] = $value['qty_requested'];

        		$kemsa_dist_arr_s[$d_id]['level'] = "subcounty";
        		$kemsa_dist_arr_s[$d_id]['level_id'] = $value['district_id'];
        		$kemsa_dist_arr_s[$d_id]['commodity_id'] = 4;
        		$kemsa_dist_arr_s[$d_id]['quarter'] = $quarter;
        		$kemsa_dist_arr_s[$d_id]['year'] = $year;
        	}

        	if (count($kemsa_fac_arr_s[$f_code]) > 0) {
        		$kemsa_fac_arr_s[$f_code]['qty_issued'] += $value['qty_issued'];
        		$kemsa_fac_arr_s[$f_code]['qty_requested'] += $value['qty_requested'];
        	}else{
        		$kemsa_fac_arr_s[$f_code]['county_id'] = $value['county_id'];
        		$kemsa_fac_arr_s[$f_code]['county_name'] = $value['county'];
        		$kemsa_fac_arr_s[$f_code]['district_id'] = $value['district_id'];
        		$kemsa_fac_arr_s[$f_code]['district_name'] = $value['district'];
        		$kemsa_fac_arr_s[$f_code]['facility_code'] = $value['facility_code'];
        		$kemsa_fac_arr_s[$f_code]['facility_name'] = $value['facility_name'];
        		$kemsa_fac_arr_s[$f_code]['issue_date'] = $value['issue_date'];

        		$kemsa_fac_arr_s[$f_code]['qty_issued'] = $value['qty_issued'];
        		$kemsa_fac_arr_s[$f_code]['qty_requested'] = $value['qty_requested'];

        		$kemsa_fac_arr_s[$f_code]['level'] = "facility";
        		$kemsa_fac_arr_s[$f_code]['level_id'] = $value['facility_code'];
        		$kemsa_fac_arr_s[$f_code]['commodity_id'] = 4;
        		$kemsa_fac_arr_s[$f_code]['quarter'] = $quarter;
        		$kemsa_fac_arr_s[$f_code]['year'] = $year;
        	}
        	
        }//end of summary data foreach

        // echo "<pre>";print_r($kemsa_county_arr_s);exit;
         // echo "<pre>";print_r($counties);exit;
        foreach ($kemsa_issue_data_c as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
        	$c_id = $value['county_id'];
        	$c_name = $value['county'];
        	$d_id = $value['district_id'];
        	$d_name = $value['district'];
        	$f_code = $value['facility_code'];
        	$f_name = $value['facility_name'];

        	
        	if (count($kemsa_county_arr_c[$c_id]) > 0) {
        		$kemsa_county_arr_c[$c_id]['qty_issued'] += $value['qty_issued'];
        		$kemsa_county_arr_c[$c_id]['qty_requested'] += $value['qty_requested'];

        	}else{

        		$kemsa_county_arr_c[$c_id]['county_id'] = $value['county_id'];
        		$kemsa_county_arr_c[$c_id]['county_name'] = $value['county'];
        		$kemsa_county_arr_c[$c_id]['district_id'] = $value['district_id'];
        		$kemsa_county_arr_c[$c_id]['district_name'] = $value['district'];
        		$kemsa_county_arr_c[$c_id]['facility_code'] = $value['facility_code'];
        		$kemsa_county_arr_c[$c_id]['facility_name'] = $value['facility_name'];

        		$kemsa_county_arr_c[$c_id]['issue_date'] = $value['issue_date'];
        		$kemsa_county_arr_c[$c_id]['qty_issued'] = $value['qty_issued'];
        		$kemsa_county_arr_c[$c_id]['qty_requested'] = $value['qty_requested'];

        		$kemsa_county_arr_c[$c_id]['level'] = "county";
        		$kemsa_county_arr_c[$c_id]['level_id'] = $value['county_id'];
        		$kemsa_county_arr_c[$c_id]['commodity_id'] = 5;
        		$kemsa_county_arr_c[$c_id]['quarter'] = $quarter;
        		$kemsa_county_arr_c[$c_id]['year'] = $year;
        	}

        	if (count($kemsa_dist_arr_c[$d_id]) > 0) {
        		$kemsa_dist_arr_c[$d_id]['qty_issued'] += $value['qty_issued'];
        		$kemsa_dist_arr_c[$d_id]['qty_requested'] += $value['qty_requested'];
        	}else{
        		$kemsa_dist_arr_c[$d_id]['county_id'] = $value['county_id'];
        		$kemsa_dist_arr_c[$d_id]['county_name'] = $value['county'];
        		$kemsa_dist_arr_c[$d_id]['district_id'] = $value['district_id'];
        		$kemsa_dist_arr_c[$d_id]['district_name'] = $value['district'];
        		$kemsa_dist_arr_c[$d_id]['facility_code'] = $value['facility_code'];
        		$kemsa_dist_arr_c[$d_id]['facility_name'] = $value['facility_name'];
        		$kemsa_dist_arr_c[$d_id]['issue_date'] = $value['issue_date'];

        		$kemsa_dist_arr_c[$d_id]['qty_issued'] = $value['qty_issued'];
        		$kemsa_dist_arr_c[$d_id]['qty_requested'] = $value['qty_requested'];

        		$kemsa_dist_arr_c[$d_id]['level'] = "subcounty";
        		$kemsa_dist_arr_c[$d_id]['level_id'] = $value['district_id'];
        		$kemsa_dist_arr_c[$d_id]['commodity_id'] = 5;
        		$kemsa_dist_arr_c[$d_id]['quarter'] = $quarter;
        		$kemsa_dist_arr_c[$d_id]['year'] = $year;
        	}

        	if (count($kemsa_fac_arr_c[$f_code]) > 0) {
        		$kemsa_fac_arr_c[$f_code]['qty_issued'] += $value['qty_issued'];
        		$kemsa_fac_arr_c[$f_code]['qty_requested'] += $value['qty_requested'];
        	}else{
        		$kemsa_fac_arr_c[$f_code]['county_id'] = $value['county_id'];
        		$kemsa_fac_arr_c[$f_code]['county_name'] = $value['county'];
        		$kemsa_fac_arr_c[$f_code]['district_id'] = $value['district_id'];
        		$kemsa_fac_arr_c[$f_code]['district_name'] = $value['district'];
        		$kemsa_fac_arr_c[$f_code]['facility_code'] = $value['facility_code'];
        		$kemsa_fac_arr_c[$f_code]['facility_name'] = $value['facility_name'];
        		$kemsa_fac_arr_c[$f_code]['issue_date'] = $value['issue_date'];

        		$kemsa_fac_arr_c[$f_code]['qty_issued'] = $value['qty_issued'];
        		$kemsa_fac_arr_c[$f_code]['qty_requested'] = $value['qty_requested'];

        		$kemsa_fac_arr_c[$f_code]['level'] = "facility";
        		$kemsa_fac_arr_c[$f_code]['level_id'] = $value['facility_code'];
        		$kemsa_fac_arr_c[$f_code]['commodity_id'] = 5;
        		$kemsa_fac_arr_c[$f_code]['quarter'] = $quarter;
        		$kemsa_fac_arr_c[$f_code]['year'] = $year;
        	}
        	
        }//end of summary data foreach

        
        $data['issue_county_data_s'] = $kemsa_county_arr_s;
        $data['issue_district_data_s'] = $kemsa_dist_arr_s;
        $data['issue_facility_data_s'] = $kemsa_fac_arr_s;

        $data['issue_county_data_c'] = $kemsa_county_arr_c;
        $data['issue_district_data_c'] = $kemsa_dist_arr_c;
        $data['issue_facility_data_c'] = $kemsa_fac_arr_c;
        // echo "<pre>";print_r($data);exit;
        $district_drawing_data_s = $district_drawing_data_c = $district_drawing_data_ss = $district_drawing_data_cc = array();

        $summary_data_s = $this->get_order_data(NULL, $district_id, NULL, 4 , $quarter,"facility",$year);
        // echo "<pre>";print_r($summary_data_s);exit;
        $summary_data_c = $this->get_order_data(NULL, $district_id, NULL, 5 , $quarter,"facility",$year);
        // echo "<pre>";print_r($summary_data_s);e3xit;	
        // echo "HERE";exit;

        // echo "<pre>";print_r($summary_data_s);exit;

        $county_arr_s = $county_arr_s_ = $dist_arr_s = $dist_arr_s_ = $fac_arr_s = $fac_arr_s_ = array();
        $county_arr_c = $county_arr_c_ = $dist_arr_c = $dist_arr_c_ = $fac_arr_c = $fac_arr_c_ = array();
    	
    	$balances_scr = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 4 , $beg_date, $end_date, $quarter,"county");
    	$balances_conf = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 5 , $beg_date, $end_date, $quarter,"county");
    	// echo "<pre>";print_r($balances_scr);exit;

    	$balances_scr_d = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 4 , $beg_date, $end_date, $quarter,"subcounty");
	    $balances_conf_d = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 5 , $beg_date, $end_date, $quarter,"subcounty");

	    $balances_scr_f = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 4 , $beg_date, $end_date, $quarter,"facility");
	    $balances_conf_f = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 5 , $beg_date, $end_date, $quarter,"facility");
	    	// echo "<pre>";print_r($balances_scr);exit;

	    // echo "<pre>";print_r($balances_scr);exit;
        // echo "<pre>";print_r($counties);exit;
        foreach ($summary_data_s as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
        	$c_id = $value['county_id'];
        	$c_name = $value['county'];
        	$d_id = $value['district_id'];
        	$d_name = $value['district'];
        	$f_code = $value['facility_code'];
        	$f_name = $value['facility_name'];
        	$m_y = date('FY',strtotime($value['order_date']));
        	// echo "<pre>";print_r($m_y);exit;

        	if (count($county_arr_s[$c_id]) > 0) {
        		// $county_arr_s[$c_id]['beginning_bal'] += $value['beginning_bal'];
        		$county_arr_s[$c_id]['q_received'] += $value['q_received'];
        		$county_arr_s[$c_id]['q_used'] += $value['q_used'];
        		$county_arr_s[$c_id]['no_of_tests_done'] += $value['no_of_tests_done'];
        		// $county_arr_s[$c_id]['closing_stock'] += $value['closing_stock'];
        		$county_arr_s[$c_id]['positive_adj'] += $value['positive_adj'];
        		$county_arr_s[$c_id]['negative_adj'] += $value['negative_adj'];
        		$county_arr_s[$c_id]['losses'] += $value['losses'];

        	}else{
        		$county_arr_s[$c_id]['county_id'] = $value['county_id'];
        		$county_arr_s[$c_id]['county_name'] = $value['county'];
        		$county_arr_s[$c_id]['district_id'] = $value['district_id'];
        		$county_arr_s[$c_id]['district_name'] = $value['district'];
        		$county_arr_s[$c_id]['facility_code'] = $value['facility_code'];
        		$county_arr_s[$c_id]['facility_name'] = $value['facility_name'];
        		$county_arr_s[$c_id]['order_date'] = $value['order_date'];
        		// $county_arr_s[$c_id]['beginning_bal'] = $value['beginning_bal'];
        		$county_arr_s[$c_id]['beginning_bal'] = $balances_scr[$c_id]['beginning_balance'];

        		$county_arr_s[$c_id]['q_received'] = $value['q_received'];
        		$county_arr_s[$c_id]['q_used'] = $value['q_used'];
        		$county_arr_s[$c_id]['no_of_tests_done'] = $value['no_of_tests_done'];
        		// $county_arr_s[$c_id]['closing_stock'] = $value['closing_stock'];
        		$county_arr_s[$c_id]['closing_stock'] = $balances_scr[$c_id]['closing_balance'];

        		$county_arr_s[$c_id]['positive_adj'] = $value['positive_adj'];
        		$county_arr_s[$c_id]['negative_adj'] = $value['negative_adj'];
        		$county_arr_s[$c_id]['losses'] = $value['losses'];

        		$county_arr_s[$c_id]['level'] = "county";
        		$county_arr_s[$c_id]['level_id'] = $value['county_id'];
        		$county_arr_s[$c_id]['commodity_id'] = 4;
        		$county_arr_s[$c_id]['quarter'] = $quarter;
        		$county_arr_s[$c_id]['year'] = $year;

        	}

        	if (count($dist_arr_s[$d_id]) > 0) {
        		// $dist_arr_s[$d_id]['beginning_bal'] += $value['beginning_bal'];
        		$dist_arr_s[$d_id]['q_received'] += $value['q_received'];
        		$dist_arr_s[$d_id]['q_used'] += $value['q_used'];
        		$dist_arr_s[$d_id]['no_of_tests_done'] += $value['no_of_tests_done'];
        		// $dist_arr_s[$d_id]['closing_stock'] += $value['closing_stock'];
        		$dist_arr_s[$d_id]['positive_adj'] += $value['positive_adj'];
        		$dist_arr_s[$d_id]['negative_adj'] += $value['negative_adj'];
        		$dist_arr_s[$d_id]['losses'] += $value['losses'];
        	}else{
        		$dist_arr_s[$d_id]['county_id'] = $value['county_id'];
        		$dist_arr_s[$d_id]['county_name'] = $value['county'];
        		$dist_arr_s[$d_id]['district_id'] = $value['district_id'];
        		$dist_arr_s[$d_id]['district_name'] = $value['district'];
        		$dist_arr_s[$d_id]['facility_code'] = $value['facility_code'];
        		$dist_arr_s[$d_id]['facility_name'] = $value['facility_name'];
        		$dist_arr_s[$d_id]['order_date'] = $value['order_date'];
        		// $dist_arr_s[$d_id]['beginning_bal'] = $value['beginning_bal'];
        		$dist_arr_s[$d_id]['beginning_bal'] = $balances_scr_d[$d_id]['beginning_balance'];

        		$dist_arr_s[$d_id]['q_received'] = $value['q_received'];
        		$dist_arr_s[$d_id]['q_used'] = $value['q_used'];
        		$dist_arr_s[$d_id]['no_of_tests_done'] = $value['no_of_tests_done'];
        		// $dist_arr_s[$d_id]['closing_stock'] = $value['closing_stock'];
        		$dist_arr_s[$d_id]['closing_stock'] = $balances_scr_d[$d_id]['closing_balance'];

        		$dist_arr_s[$d_id]['positive_adj'] = $value['positive_adj'];
        		$dist_arr_s[$d_id]['negative_adj'] = $value['negative_adj'];
        		$dist_arr_s[$d_id]['losses'] = $value['losses'];

        		$dist_arr_s[$d_id]['level'] = "subcounty";
        		$dist_arr_s[$d_id]['level_id'] = $value['district_id'];
        		$dist_arr_s[$d_id]['commodity_id'] = 4;
        		$dist_arr_s[$d_id]['quarter'] = $quarter;
        		$dist_arr_s[$d_id]['year'] = $year;
        	}

        	if (count($fac_arr_s[$m_y][$f_code]) > 0) {
        		// $fac_arr_s[$f_code]['beginning_bal'] += $value['beginning_bal'];
        		$fac_arr_s[$m_y][$f_code]['order_date'] = $value['order_date'];
        		$fac_arr_s[$m_y][$f_code]['q_received'] += $value['q_received'];
        		$fac_arr_s[$m_y][$f_code]['q_used'] += $value['q_used'];
        		$fac_arr_s[$m_y][$f_code]['no_of_tests_done'] += $value['no_of_tests_done'];
        		// $fac_arr_s[$f_code]['closing_stock'] += $value['closing_stock'];
        		$fac_arr_s[$m_y][$f_code]['positive_adj'] += $value['positive_adj'];
        		$fac_arr_s[$m_y][$f_code]['negative_adj'] += $value['negative_adj'];
        		$fac_arr_s[$m_y][$f_code]['losses'] += $value['losses'];
        	}else{
        		$fac_arr_s[$m_y][$f_code]['county_id'] = $value['county_id'];
        		$fac_arr_s[$m_y][$f_code]['county_name'] = $value['county'];
        		$fac_arr_s[$m_y][$f_code]['district_id'] = $value['district_id'];
        		$fac_arr_s[$m_y][$f_code]['district_name'] = $value['district'];
        		$fac_arr_s[$m_y][$f_code]['facility_code'] = $value['facility_code'];
        		$fac_arr_s[$m_y][$f_code]['facility_name'] = $value['facility_name'];
        		$fac_arr_s[$m_y][$f_code]['order_date'] = $value['order_date'];
        		$fac_arr_s[$m_y][$f_code]['report_for'] = $value['report_for'];
        		// $fac_arr_s[$f_code]['beginning_bal'] = $value['beginning_bal'];
        		$fac_arr_s[$m_y][$f_code]['beginning_bal'] = $balances_scr_f[$f_code]['beginning_balance'];

        		$fac_arr_s[$m_y][$f_code]['q_received'] = $value['q_received'];
        		$fac_arr_s[$m_y][$f_code]['q_used'] = $value['q_used'];
        		$fac_arr_s[$m_y][$f_code]['no_of_tests_done'] = $value['no_of_tests_done'];
        		// $fac_arr_s[$f_code]['closing_stock'] = $value['closing_stock'];
        		$fac_arr_s[$f_code]['closing_stock'] = $balances_scr_f[$f_code]['closing_balance'];

        		$fac_arr_s[$m_y][$f_code]['positive_adj'] = $value['positive_adj'];
        		$fac_arr_s[$m_y][$f_code]['negative_adj'] = $value['negative_adj'];
        		$fac_arr_s[$m_y][$f_code]['losses'] = $value['losses'];

        		$fac_arr_s[$m_y][$f_code]['level'] = "facility";
        		$fac_arr_s[$m_y][$f_code]['level_id'] = $value['facility_code'];
        		$fac_arr_s[$m_y][$f_code]['commodity_id'] = 4;
        		$fac_arr_s[$m_y][$f_code]['quarter'] = $quarter;
        		$fac_arr_s[$m_y][$f_code]['year'] = $year;

        	}
        	
        }//end of summary data foreach

        // echo "<pre>";print_r($fac_arr_s);exit;

         // echo "<pre>";print_r($counties);exit;
        foreach ($summary_data_c as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
        	$c_id = $value['county_id'];
        	$c_name = $value['county'];
        	$d_id = $value['district_id'];
        	$d_name = $value['district'];
        	$f_code = $value['facility_code'];
        	$f_name = $value['facility_name'];
        	$m_y = date('FY',strtotime($value['order_date']));

        	
        	if (count($county_arr_c[$c_id]) > 0) {
        		// $county_arr_c[$c_id]['beginning_bal'] += $value['beginning_bal'];
        		$county_arr_c[$c_id]['q_received'] += $value['q_received'];
        		$county_arr_c[$c_id]['q_used'] += $value['q_used'];
        		$county_arr_c[$c_id]['no_of_tests_done'] += $value['no_of_tests_done'];
        		// $county_arr_c[$c_id]['closing_stock'] += $value['closing_stock'];
        		$county_arr_c[$c_id]['positive_adj'] += $value['positive_adj'];
        		$county_arr_c[$c_id]['negative_adj'] += $value['negative_adj'];
        		$county_arr_c[$c_id]['losses'] += $value['losses'];
        	}else{
        		$county_arr_c[$c_id]['county_id'] = $value['county_id'];
        		$county_arr_c[$c_id]['county_name'] = $value['county'];
        		$county_arr_c[$c_id]['district_id'] = $value['district_id'];
        		$county_arr_c[$c_id]['district_name'] = $value['district'];
        		$county_arr_c[$c_id]['facility_code'] = $value['facility_code'];
        		$county_arr_c[$c_id]['facility_name'] = $value['facility_name'];
        		$county_arr_c[$c_id]['order_date'] = $value['order_date'];
        		// $county_arr_c[$c_id]['beginning_bal'] = $value['beginning_bal'];
        		$county_arr_c[$c_id]['beginning_bal'] = $balances_conf[$c_id]['beginning_balance'];


        		$county_arr_c[$c_id]['q_received'] = $value['q_received'];
        		$county_arr_c[$c_id]['q_used'] = $value['q_used'];
        		$county_arr_c[$c_id]['no_of_tests_done'] = $value['no_of_tests_done'];
        		// $county_arr_c[$c_id]['closing_stock'] = $value['closing_stock'];
        		$county_arr_c[$c_id]['closing_stock'] = $balances_conf[$c_id]['closing_balance'];

        		$county_arr_c[$c_id]['positive_adj'] = $value['positive_adj'];
        		$county_arr_c[$c_id]['negative_adj'] = $value['negative_adj'];
        		$county_arr_c[$c_id]['losses'] = $value['losses'];

        		$county_arr_c[$c_id]['level'] = "county";
        		$county_arr_c[$c_id]['level_id'] = $value['county_id'];
        		$county_arr_c[$c_id]['commodity_id'] = 5;
        		$county_arr_c[$c_id]['quarter'] = $quarter;
        		$county_arr_c[$c_id]['year'] = $year;

        	}

        	if (count($dist_arr_c[$d_id]) > 0) {
        		// $dist_arr_c[$d_id]['beginning_bal'] += $value['beginning_bal'];
        		$dist_arr_c[$d_id]['q_received'] += $value['q_received'];
        		$dist_arr_c[$d_id]['q_used'] += $value['q_used'];
        		$dist_arr_c[$d_id]['no_of_tests_done'] += $value['no_of_tests_done'];
        		// $dist_arr_c[$d_id]['closing_stock'] += $value['closing_stock'];
        		$dist_arr_c[$d_id]['positive_adj'] += $value['positive_adj'];
        		$dist_arr_c[$d_id]['negative_adj'] += $value['negative_adj'];
        		$dist_arr_c[$d_id]['losses'] += $value['losses'];
        	}else{
        		$dist_arr_c[$d_id]['county_id'] = $value['county_id'];
        		$dist_arr_c[$d_id]['county_name'] = $value['county'];
        		$dist_arr_c[$d_id]['district_id'] = $value['district_id'];
        		$dist_arr_c[$d_id]['district_name'] = $value['district'];
        		$dist_arr_c[$d_id]['facility_code'] = $value['facility_code'];
        		$dist_arr_c[$d_id]['facility_name'] = $value['facility_name'];
        		$dist_arr_c[$d_id]['order_date'] = $value['order_date'];
        		// $dist_arr_c[$d_id]['beginning_bal'] = $value['beginning_bal'];
        		$dist_arr_c[$d_id]['beginning_bal'] = $balances_conf_d[$d_id]['beginning_balance'];


        		$dist_arr_c[$d_id]['q_received'] = $value['q_received'];
        		$dist_arr_c[$d_id]['q_used'] = $value['q_used'];
        		$dist_arr_c[$d_id]['no_of_tests_done'] = $value['no_of_tests_done'];
        		// $dist_arr_c[$d_id]['closing_stock'] = $value['closing_stock'];
        		$dist_arr_c[$d_id]['closing_stock'] = $balances_conf_d[$d_id]['closing_balance'];
        		
        		$dist_arr_c[$d_id]['positive_adj'] = $value['positive_adj'];
        		$dist_arr_c[$d_id]['negative_adj'] = $value['negative_adj'];
        		$dist_arr_c[$d_id]['losses'] = $value['losses'];

        		$dist_arr_c[$d_id]['level'] = "subcounty";
        		$dist_arr_c[$d_id]['level_id'] = $value['district_id'];
        		$dist_arr_c[$d_id]['commodity_id'] = 5;
        		$dist_arr_c[$d_id]['quarter'] = $quarter;
        		$dist_arr_c[$d_id]['year'] = $year;
        	}

        	if (count($fac_arr_c[$f_code]) > 0) {
        		// $fac_arr_c[$f_code]['beginning_bal'] += $value['beginning_bal'];
        		$fac_arr_c[$m_y][$f_code]['order_date'] = $value['order_date'];
        		$fac_arr_c[$m_y][$f_code]['q_received'] += $value['q_received'];
        		$fac_arr_c[$m_y][$f_code]['q_used'] += $value['q_used'];
        		$fac_arr_c[$m_y][$f_code]['no_of_tests_done'] += $value['no_of_tests_done'];
        		// $fac_arr_c[$f_code]['closing_stock'] += $value['closing_stock'];
        		$fac_arr_c[$m_y][$f_code]['positive_adj'] += $value['positive_adj'];
        		$fac_arr_c[$m_y][$f_code]['negative_adj'] += $value['negative_adj'];
        		$fac_arr_c[$m_y][$f_code]['losses'] += $value['losses'];
        	}else{
        		$fac_arr_c[$m_y][$f_code]['county_id'] = $value['county_id'];
        		$fac_arr_c[$m_y][$f_code]['county_name'] = $value['county'];
        		$fac_arr_c[$m_y][$f_code]['district_id'] = $value['district_id'];
        		$fac_arr_c[$m_y][$f_code]['district_name'] = $value['district'];
        		$fac_arr_c[$f_code]['facility_code'] = $value['facility_code'];
        		$fac_arr_c[$f_code]['facility_name'] = $value['facility_name'];
        		$fac_arr_c[$m_y][$f_code]['order_date'] = $value['order_date'];
        		// $fac_ar[$m_y]r_c[$f_code]['beginning_bal'] = $value['beginning_bal'];
        		$fac_arr_c[$m_y][$f_code]['beginning_bal'] = $balances_conf_f[$f_code]['beginning_balance'];


        		$fac_arr_c[$m_y][$f_code]['q_received'] = $value['q_received'];
        		$fac_arr_c[$m_y][$f_code]['q_used'] = $value['q_used'];
        		$fac_arr_c[$m_y][$f_code]['no_of_tests_done'] = $value['no_of_tests_done'];
        		// $fac_arr_c[$f_code]['closing_stock'] = $value['closing_stock'];
        		$fac_arr_c[$m_y][$f_code]['closing_stock'] = $balances_conf_f[$f_code]['closing_balance'];
        		
        		$fac_arr_c[$m_y][$f_code]['positive_adj'] = $value['positive_adj'];
        		$fac_arr_c[$m_y][$f_code]['negative_adj'] = $value['negative_adj'];
        		$fac_arr_c[$m_y][$f_code]['losses'] = $value['losses'];

        		$fac_arr_c[$m_y][$f_code]['level'] = "facility";
        		$fac_arr_c[$m_y][$f_code]['level_id'] = $value['facility_code'];
        		$fac_arr_c[$m_y][$f_code]['commodity_id'] = 5;
        		$fac_arr_c[$m_y][$f_code]['quarter'] = $quarter;
        		$fac_arr_c[$m_y][$f_code]['year'] = $year;
        	}
        	
        }//end of summary data foreach

        $data['county_data_s'] = $county_arr_s;
        $data['district_data_s'] = $dist_arr_s;
        $data['facility_data_s'] = $fac_arr_s;

        $data['county_data_c'] = $county_arr_c;
        $data['district_data_c'] = $dist_arr_c;
        $data['facility_data_c'] = $fac_arr_c;
        // echo "<pre>";print_r($data);exit;
		$title_append = (isset($filtered_name) && $filtered_name !='')? $filtered_name:NULL;

		if ($quarter > 0) {
			// $title_append .= " Report";
		}else{
			// $title_append .= " as of ".date('F',strtotime("-1 MONTH"))." Report";
			$title_append .= " as of ".$year.".";
		}

		$title_prepend = (isset($county_id) && $county_id > 0)? 'County':'Sub-County';

		// $title_append .= (isset($quarter) && $quarter > 0)? ' Quarter: '.$quarter:NULL;
		// echo "<pre>";print_r($title_append);exit;
		if ($quarter > 0) {
			$quartered = 1;
			// $quarter_append = ' <span class="font-blue-steel">*</span>';
		}
		// $title_prepend = (isset($title_prepend) && $title_prepend !='')? $title_prepend:'National';
		// echo "<pre>";print_r($data);exit;
		// echo "<pre>";print_r($county_data_s);exit;

		$data['title_append'] = $title_append;
		$data['quartered'] = $quartered;
		$data['quarter_append'] = $quarter_append;
		// $data['tracer_commodities'] = $commodities;
		$data['facility_count'] = $facility_count;
		$data['commodity_count'] = $commodity_count;
		$data['content_view'] = 'dashboard/dashboard';
		$data['county_data'] = $counties;
		$data['title_prepend'] = $title_prepend;
		$data['county_count'] = count($counties_using_CD4);
		$data['district_data'] = $districts;
		$data['commodity_divisions'] = $commodity_divisions;
		$data['title'] = "National Dashboard";
		$data['county_data'] = $counties;
		$data['county_id'] = $county_id;
		$data['subcounty_id'] = $district_id;
		$data['facility_code'] = $facility_code;
		$data['district_data'] = $districts;
		$data['maps'] = $map;
		$data['counties'] = $county_name;
		$this -> load -> view("v2/dashboard/dashboard_discrepancies_issues", $data);
	
	}
	public function get_issue_discrepancy_details($level = NULL, $level_id = NULL, $commodity_id = NULL,$quarter = NULL,$year = NULL)
	{
		$county_id = $district_id = $facility_code = NULL;
		$commodity_id = ($commodity_id > 0)? $commodity_id:4;
        $quarter = (isset($quarter) && $quarter>0)? $quarter:NULL;
        $year = (isset($year) && $year>0)? $year:date('Y');

        // echo "<pre>";print_r($year);exit;
		switch ($level) {
			case 'county':
				$county_id = $level_id;
				break;
			case 'subcounty':
				$district_id = $level_id;
				break;
			case 'facility':
				$facility_code = $level_id;
				break;
			
			default:
				
				break;
		}

		$summary_data = $this->get_order_data($county_id, $district_id, $facility_code, $commodity_id , $quarter,"facility",$year);	
		// echo "<pre>";print_r($summary_data);exit;
		foreach ($summary_data as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
        	$c_id = $value['county_id'];
        	$c_name = $value['county'];
        	$d_id = $value['district_id'];
        	$d_name = $value['district'];
        	$f_code = $value['facility_code'];
        	$f_name = $value['facility_name'];
        	$o_date = $value['order_date'];
        	$order_date = date('Y-m-d',strtotime("-1 MONTH",strtotime($value['order_date'])));
        	$o_d = date('mY',strtotime("-1 MONTH",strtotime($o_date)));
        	// echo "<pre>";print_r($o_d);exit;

        	if (count($fac_arr[$f_code]) > 0) {
        		$fac_arr[$o_d.'_'.$f_code]['order_date'] = $order_date;
        		$fac_arr[$o_d.'_'.$f_code]['beginning_bal'] += $value['beginning_bal'];
        		$fac_arr[$o_d.'_'.$f_code]['q_received'] += $value['q_received'];
        		$fac_arr[$o_d.'_'.$f_code]['q_used'] += $value['q_used'];
        		$fac_arr[$o_d.'_'.$f_code]['no_of_tests_done'] += $value['no_of_tests_done'];
        		$fac_arr[$o_d.'_'.$f_code]['closing_stock'] += $value['closing_stock'];
        		$fac_arr[$o_d.'_'.$f_code]['positive_adj'] += $value['positive_adj'];
        		$fac_arr[$o_d.'_'.$f_code]['negative_adj'] += $value['negative_adj'];
        		$fac_arr[$o_d.'_'.$f_code]['losses'] += $value['losses'];
        	}else{
        		$fac_arr[$o_d.'_'.$f_code]['county_id'] = $value['county_id'];
        		$fac_arr[$o_d.'_'.$f_code]['county_name'] = $value['county'];
        		$fac_arr[$o_d.'_'.$f_code]['district_id'] = $value['district_id'];
        		$fac_arr[$o_d.'_'.$f_code]['district_name'] = $value['district'];
        		$fac_arr[$o_d.'_'.$f_code]['facility_code'] = $value['facility_code'];
        		$fac_arr[$o_d.'_'.$f_code]['facility_name'] = $value['facility_name'];
        		// $fac_arr[$o_d.'_'.$f_code]['order_date'] = $value['order_date'];
        		$fac_arr[$o_d.'_'.$f_code]['order_date'] = $order_date;
        		$fac_arr[$o_d.'_'.$f_code]['report_for'] = $value['report_for'];
        		$fac_arr[$o_d.'_'.$f_code]['beginning_bal'] = $value['beginning_bal'];
        		$fac_arr[$o_d.'_'.$f_code]['q_received'] = $value['q_received'];
        		$fac_arr[$o_d.'_'.$f_code]['q_used'] = $value['q_used'];
        		$fac_arr[$o_d.'_'.$f_code]['no_of_tests_done'] = $value['no_of_tests_done'];
        		$fac_arr[$o_d.'_'.$f_code]['closing_stock'] = $value['closing_stock'];
        		$fac_arr[$o_d.'_'.$f_code]['positive_adj'] = $value['positive_adj'];
        		$fac_arr[$o_d.'_'.$f_code]['negative_adj'] = $value['negative_adj'];
        		$fac_arr[$o_d.'_'.$f_code]['losses'] = $value['losses'];

        		$fac_arr[$o_d.'_'.$f_code]['level'] = "facility";
        		$fac_arr[$o_d.'_'.$f_code]['level_id'] = $value['facility_code'];
        	}
        	
        }//end of summary data foreach

        // echo "<pre>";print_r($fac_arr);exit;
        // echo $county_id.' '.$district_id.' '.$facility_code.' '.$commodity_id.' '.$quarter.' '.$year;exit;
		$issue_data = $this->get_kemsa_issue_data($county_id, $district_id, $facility_code, $commodity_id , $quarter, $year);	
		// echo "<pre>";print_r($issue_data);exit;
        $county_arr_s = $county_arr_s_ = $dist_arr_s = $dist_arr_s_ = $fac_arr_s = $fac_arr_s_ = array();
        $county_arr_c = $county_arr_c_ = $dist_arr_c = $dist_arr_c_ = $fac_arr_c = $fac_arr_c_ = array();

        // echo "<pre>";print_r($counties);exit;
        foreach ($issue_data as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
        	$c_id = $value['county_id'];
        	$c_name = $value['county'];
        	$d_id = $value['district_id'];
        	$d_name = $value['district'];
        	$f_code = $value['facility_code'];
        	$f_name = $value['facility_name'];
        	$i_date = $value['issue_date'];
        	// $i_d = date('mY',strtotime("-1 MONTH",strtotime($i_date)));
        	$i_d = date('mY',strtotime("-0 MONTH",strtotime($i_date)));
        	// echo "<pre>";print_r($i_d);exit;

        	if (count($fac_arr[$f_code]) > 0) {
        		$fac_arr[$i_d.'_'.$f_code]['qty_issued'] += $value['qty_issued'];
        		$fac_arr[$i_d.'_'.$f_code]['qty_requested'] += $value['qty_requested'];

        	}else{
        		$fac_arr[$i_d.'_'.$f_code]['county_id'] = $value['county_id'];
        		$fac_arr[$i_d.'_'.$f_code]['county_name'] = $value['county'];
        		$fac_arr[$i_d.'_'.$f_code]['district_id'] = $value['district_id'];
        		$fac_arr[$i_d.'_'.$f_code]['district_name'] = $value['district'];
        		$fac_arr[$i_d.'_'.$f_code]['facility_code'] = $value['facility_code'];
        		$fac_arr[$i_d.'_'.$f_code]['facility_name'] = $value['facility_name'];
        		$fac_arr[$i_d.'_'.$f_code]['issue_date'] = $value['issue_date'];

        		$fac_arr[$i_d.'_'.$f_code]['qty_issued'] = $value['qty_issued'];
        		$fac_arr[$i_d.'_'.$f_code]['qty_requested'] = $value['qty_requested'];

        		$fac_arr[$i_d.'_'.$f_code]['level'] = "facility";
        		$fac_arr[$i_d.'_'.$f_code]['level_id'] = $value['facility_code'];
        	}
        	
        }//end of summary data foreach

        // echo "<pre>";print_r($fac_arr);exit;
        $html = '<table class="table table-bordered datatable display cell-border compact" cellspacing="0" width="100%" id="datatable">
					<thead>
						<tr>
							<th>Facility Name</th>
							<th>MFL Code</th>
							<th>Sub-County</th>
							<th>County</th>
							<th>Issue Date</th>
							<th>Order Date</th>
							<th>Issued from KEMSA</th>
							<th>Received at site</th>
							<th>Difference</th>
						</tr>
					</thead>
					<tbody>';

		foreach ($fac_arr as $key => $value) { 
			// echo "<pre>";print_r($value);exit;
			$kemsa_issued = $value['qty_issued'];
			$rec = $value['q_received'];
			
			$o_date = $value['order_date'];
			$i_date = $value['issue_date'];

			$i_date = (isset($i_date) && $i_date !='')?date('F Y',strtotime($i_date)):'-';
			$o_date = (isset($o_date) && $o_date !='')?date('F Y',strtotime($o_date)):'-';


			$difference = $kemsa_issued - $rec;
			// echo "<pre>";print_r($difference);exit;
			$class = ($difference>0)? " green ":" red ";
			$class = ($difference==0)? "":$class;

			if ($difference != 0):
			$html .= '
			<tr>
				<td>'.$value['facility_name'].'</td>
				<td>'.$value['facility_code'].'</td>
				<td>'.$value['district_name'].'</td>
				<td>'.$value['county_name'].'</td>
				<td>'.$i_date.'</td>
				<td>'.$o_date.'</td>
				<td>'.number_format($kemsa_issued).'</td>
				<td>'.number_format($rec).'</td>
				<td class="'.$class.'"><strong>'.number_format($difference).'</strong></td>
			</tr>';

			endif;
		}
		
		$html .='<tbody></table>';

		// echo "<pre>";print_r($html);exit;
		echo $html;
	}

	public function kemsa_issues_dashboard($county_id = NULL, $district_id = NULL,$quarter = NULL,$month = NULL,$year = NULL)
	{
		// $page_caching = $this->output->cache(CACHE_REFRESH_INTERVAL);
		$default = "tracer";//FYI
		// echo "<pre>";print_r($district_id);exit;
		// echo "<pre>";print_r($quarter);exit;
		// $map = $this->render_map();
		$county_id = (isset($county_id) && $county_id > 0)? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id > 0)? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code > 0)? $facility_code:NULL;
		$quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;
		$year = (isset($year) && $year > 0)? $year:date('Y');

		$data['filter_years'] = $this->get_years_to_date();
		$filtered_name = NULL;

		// $quarters = array();
		$prev_yr = date("Y",strtotime('-1 YEAR'));
		$quarters_prev_yr = $this->get_year_quarter(NULL,NULL,$prev_yr,1);
		$quarters_curr_yr = $this->get_year_quarter(NULL,NULL,NULL,1);
		// echo "<pre>";print_r($quarters_prev_yr);exit;
		$data['quarters'] = array_merge_recursive($quarters_curr_yr, $quarters_prev_yr);

		$data['filtered'] = $filtered;
		// echo "<pre>";print_r($data);exit;
        $current_date = date('Y-m-d');

		$page_title = "RTK KEMSA Issues Dashboard.";
		$data['page_title'] = $page_title;

        // echo "<pre>";print_r($district_drawing_data);exit;
    	$drawing_total_confirmatory = $drawing_total_screening = 0;
    	$drawing_used_confirmatory = $drawing_used_screening = 0;
    	$drawing_balance_confirmatory = $drawing_balance_screening = 0;
    	$county_scr_distributed_aggr = $county_conf_distributed_aggr = 0;
    	$county_scr_total_drawing_rights = $county_conf_total_drawing_rights = 0;

        $current = date('m');
        $curMonth = $current;
        $curQuarter = ceil($curMonth/3);//KARSAN

        if ($quarter > 0) {
        	$curQuarter = $quarter;
        }

        // echo "<pre>";print_r($month);exit;
        $kemsa_issue_data_s = $this->get_kemsa_issue_data(NULL, NULL, NULL, 4 , $quarter,$year,$month,"issue_date");
        // echo "<pre>";print_r($kemsa_issue_data_s);exit;
        $kemsa_issue_data_c = $this->get_kemsa_issue_data(NULL, NULL, NULL, 5 , $quarter,$year,$month,"issue_date");
        // echo "<pre>";print_r($kemsa_issue_data_c);exit;	

        $kemsa_county_arr_s = $kemsa_county_arr_s_ = $kemsa_dist_arr_s = $kemsa_dist_arr_s_ = $kemsa_fac_arr_s = $kemsa_fac_arr_s_ = array();
        $kemsa_county_arr_c = $kemsa_county_arr_c_ = $kemsa_dist_arr_c = $kemsa_dist_arr_c_ = $kemsa_fac_arr_c = $kemsa_fac_arr_c_ = array();

        // echo "<pre>";print_r($counties);exit;
        foreach ($kemsa_issue_data_s as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
        	$c_id = $value['county_id'];
        	$c_name = $value['county'];
        	$d_id = $value['district_id'];
        	$d_name = $value['district'];
        	$f_code = $value['facility_code'];
        	$f_name = $value['facility_name'];

        	
        	if (count($kemsa_county_arr_s[$c_id]) > 0) {
        		$kemsa_county_arr_s[$c_id]['qty_issued'] += $value['qty_issued'];
        		$kemsa_county_arr_s[$c_id]['qty_requested'] += $value['qty_requested'];

        	}else{

        		$kemsa_county_arr_s[$c_id]['county_id'] = $value['county_id'];
        		$kemsa_county_arr_s[$c_id]['county_name'] = $value['county'];
        		$kemsa_county_arr_s[$c_id]['district_id'] = $value['district_id'];
        		$kemsa_county_arr_s[$c_id]['district_name'] = $value['district'];
        		$kemsa_county_arr_s[$c_id]['facility_code'] = $value['facility_code'];
        		$kemsa_county_arr_s[$c_id]['facility_name'] = $value['facility_name'];

        		$kemsa_county_arr_s[$c_id]['issue_date'] = $value['issue_date'];
        		$kemsa_county_arr_s[$c_id]['qty_issued'] = $value['qty_issued'];
        		$kemsa_county_arr_s[$c_id]['qty_requested'] = $value['qty_requested'];
        		$kemsa_county_arr_s[$c_id]['delivery_note'] = $value['delivery_note'];

        		$kemsa_county_arr_s[$c_id]['level'] = "county";
        		$kemsa_county_arr_s[$c_id]['level_id'] = $value['county_id'];
        		$kemsa_county_arr_s[$c_id]['commodity_id'] = 4;
        		$kemsa_county_arr_s[$c_id]['quarter'] = $quarter;
        		$kemsa_county_arr_s[$c_id]['year'] = $year;

        	}

        	if (count($kemsa_dist_arr_s[$d_id]) > 0) {
        		$kemsa_dist_arr_s[$d_id]['qty_issued'] += $value['qty_issued'];
        		$kemsa_dist_arr_s[$d_id]['qty_requested'] += $value['qty_requested'];
        	}else{
        		$kemsa_dist_arr_s[$d_id]['county_id'] = $value['county_id'];
        		$kemsa_dist_arr_s[$d_id]['county_name'] = $value['county'];
        		$kemsa_dist_arr_s[$d_id]['district_id'] = $value['district_id'];
        		$kemsa_dist_arr_s[$d_id]['district_name'] = $value['district'];
        		$kemsa_dist_arr_s[$d_id]['facility_code'] = $value['facility_code'];
        		$kemsa_dist_arr_s[$d_id]['facility_name'] = $value['facility_name'];
        		$kemsa_dist_arr_s[$d_id]['issue_date'] = $value['issue_date'];
        		$kemsa_dist_arr_s[$d_id]['delivery_note'] = $value['delivery_note'];

        		$kemsa_dist_arr_s[$d_id]['qty_issued'] = $value['qty_issued'];
        		$kemsa_dist_arr_s[$d_id]['qty_requested'] = $value['qty_requested'];

        		$kemsa_dist_arr_s[$d_id]['level'] = "subcounty";
        		$kemsa_dist_arr_s[$d_id]['level_id'] = $value['district_id'];
        		$kemsa_dist_arr_s[$d_id]['commodity_id'] = 4;
        		$kemsa_dist_arr_s[$d_id]['quarter'] = $quarter;
        		$kemsa_dist_arr_s[$d_id]['year'] = $year;
        	}

    		$kemsa_fac_arr_s_['county_id'] = $value['county_id'];
    		$kemsa_fac_arr_s_['county_name'] = $value['county'];
    		$kemsa_fac_arr_s_['district_id'] = $value['district_id'];
    		$kemsa_fac_arr_s_['district_name'] = $value['district'];
    		$kemsa_fac_arr_s_['facility_code'] = $value['facility_code'];
    		$kemsa_fac_arr_s_['facility_name'] = $value['facility_name'];
    		$kemsa_fac_arr_s_['issue_date'] = $value['issue_date'];
    		$kemsa_fac_arr_s_['delivery_note'] = $value['delivery_note'];

    		$kemsa_fac_arr_s_['qty_issued'] = $value['qty_issued'];
    		$kemsa_fac_arr_s_['qty_requested'] = $value['qty_requested'];

    		$kemsa_fac_arr_s_['level'] = "facility";
    		$kemsa_fac_arr_s_['level_id'] = $value['facility_code'];
    		$kemsa_fac_arr_s_['commodity_id'] = 4;
    		$kemsa_fac_arr_s_['quarter'] = $quarter;
    		$kemsa_fac_arr_s_['year'] = $year;

    		array_push($kemsa_fac_arr_s, $kemsa_fac_arr_s_);
        	
        }//end of summary data foreach

        // echo "<pre>";print_r($kemsa_fac_arr_s);exit;
         // echo "<pre>";print_r($counties);exit;
        foreach ($kemsa_issue_data_c as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
        	$c_id = $value['county_id'];
        	$c_name = $value['county'];
        	$d_id = $value['district_id'];
        	$d_name = $value['district'];
        	$f_code = $value['facility_code'];
        	$f_name = $value['facility_name'];

        	
        	if (count($kemsa_county_arr_c[$c_id]) > 0) {
        		$kemsa_county_arr_c[$c_id]['qty_issued'] += $value['qty_issued'];
        		$kemsa_county_arr_c[$c_id]['qty_requested'] += $value['qty_requested'];

        	}else{

        		$kemsa_county_arr_c[$c_id]['county_id'] = $value['county_id'];
        		$kemsa_county_arr_c[$c_id]['county_name'] = $value['county'];
        		$kemsa_county_arr_c[$c_id]['district_id'] = $value['district_id'];
        		$kemsa_county_arr_c[$c_id]['district_name'] = $value['district'];
        		$kemsa_county_arr_c[$c_id]['facility_code'] = $value['facility_code'];
        		$kemsa_county_arr_c[$c_id]['facility_name'] = $value['facility_name'];

        		$kemsa_county_arr_c[$c_id]['issue_date'] = $value['issue_date'];
        		$kemsa_county_arr_c[$c_id]['qty_issued'] = $value['qty_issued'];
        		$kemsa_county_arr_c[$c_id]['qty_requested'] = $value['qty_requested'];

        		$kemsa_county_arr_c[$c_id]['level'] = "county";
        		$kemsa_county_arr_c[$c_id]['level_id'] = $value['county_id'];
        		$kemsa_county_arr_c[$c_id]['commodity_id'] = 5;
        		$kemsa_county_arr_c[$c_id]['quarter'] = $quarter;
        		$kemsa_county_arr_c[$c_id]['year'] = $year;
        	}

        	if (count($kemsa_dist_arr_c[$d_id]) > 0) {
        		$kemsa_dist_arr_c[$d_id]['qty_issued'] += $value['qty_issued'];
        		$kemsa_dist_arr_c[$d_id]['qty_requested'] += $value['qty_requested'];
        	}else{
        		$kemsa_dist_arr_c[$d_id]['county_id'] = $value['county_id'];
        		$kemsa_dist_arr_c[$d_id]['county_name'] = $value['county'];
        		$kemsa_dist_arr_c[$d_id]['district_id'] = $value['district_id'];
        		$kemsa_dist_arr_c[$d_id]['district_name'] = $value['district'];
        		$kemsa_dist_arr_c[$d_id]['facility_code'] = $value['facility_code'];
        		$kemsa_dist_arr_c[$d_id]['facility_name'] = $value['facility_name'];
        		$kemsa_dist_arr_c[$d_id]['issue_date'] = $value['issue_date'];

        		$kemsa_dist_arr_c[$d_id]['qty_issued'] = $value['qty_issued'];
        		$kemsa_dist_arr_c[$d_id]['qty_requested'] = $value['qty_requested'];

        		$kemsa_dist_arr_c[$d_id]['level'] = "subcounty";
        		$kemsa_dist_arr_c[$d_id]['level_id'] = $value['district_id'];
        		$kemsa_dist_arr_c[$d_id]['commodity_id'] = 5;
        		$kemsa_dist_arr_c[$d_id]['quarter'] = $quarter;
        		$kemsa_dist_arr_c[$d_id]['year'] = $year;
        	}

        	$kemsa_fac_arr_c_['county_id'] = $value['county_id'];
    		$kemsa_fac_arr_c_['county_name'] = $value['county'];
    		$kemsa_fac_arr_c_['district_id'] = $value['district_id'];
    		$kemsa_fac_arr_c_['district_name'] = $value['district'];
    		$kemsa_fac_arr_c_['facility_code'] = $value['facility_code'];
    		$kemsa_fac_arr_c_['facility_name'] = $value['facility_name'];
    		$kemsa_fac_arr_c_['issue_date'] = $value['issue_date'];
    		$kemsa_fac_arr_c_['delivery_note'] = $value['delivery_note'];

    		$kemsa_fac_arr_c_['qty_issued'] = $value['qty_issued'];
    		$kemsa_fac_arr_c_['qty_requested'] = $value['qty_requested'];

    		$kemsa_fac_arr_c_['level'] = "facility";
    		$kemsa_fac_arr_c_['level_id'] = $value['facility_code'];
    		$kemsa_fac_arr_c_['commodity_id'] = 4;
    		$kemsa_fac_arr_c_['quarter'] = $quarter;
    		$kemsa_fac_arr_c_['year'] = $year;

    		array_push($kemsa_fac_arr_c, $kemsa_fac_arr_c_);
        	
        }//end of summary data foreach

        
        $data['issue_county_data_s'] = $kemsa_county_arr_s;
        $data['issue_district_data_s'] = $kemsa_dist_arr_s;
        $data['issue_facility_data_s'] = $kemsa_fac_arr_s;

        $data['issue_county_data_c'] = $kemsa_county_arr_c;
        $data['issue_district_data_c'] = $kemsa_dist_arr_c;
        $data['issue_facility_data_c'] = $kemsa_fac_arr_c;
        // echo "<pre>";print_r($data);exit;
        $district_drawing_data_s = $district_drawing_data_c = $district_drawing_data_ss = $district_drawing_data_cc = array();

		$title_append = (isset($filtered_name) && $filtered_name !='')? $filtered_name:NULL;

		if ($quarter > 0) {
			// $title_append .= " Report";
		}else{
			// $title_append .= " as of ".date('F',strtotime("-1 MONTH"))." Report";
			$title_append .= " as of ".$year.".";
		}

		$title_prepend = (isset($county_id) && $county_id > 0)? 'County':'Sub-County';

		// $title_append .= (isset($quarter) && $quarter > 0)? ' Quarter: '.$quarter:NULL;
		// echo "<pre>";print_r($title_append);exit;
		if ($quarter > 0) {
			$quartered = 1;
			// $quarter_append = ' <span class="font-blue-steel">*</span>';
		}
		// $title_prepend = (isset($title_prepend) && $title_prepend !='')? $title_prepend:'National';
		// echo "<pre>";print_r($data);exit;
		// echo "<pre>";print_r($county_data_s);exit;

		$data['title_append'] = $title_append;
		$data['quartered'] = $quartered;
		$data['quarter_append'] = $quarter_append;
		// $data['tracer_commodities'] = $commodities;
		$data['facility_count'] = $facility_count;
		$data['commodity_count'] = $commodity_count;
		$data['content_view'] = 'dashboard/dashboard';
		$data['county_data'] = $counties;
		$data['title_prepend'] = $title_prepend;
		$data['county_count'] = count($counties_using_CD4);
		$data['district_data'] = $districts;
		$data['commodity_divisions'] = $commodity_divisions;
		$data['title'] = "National Dashboard";
		$data['county_data'] = $counties;
		$data['county_id'] = $county_id;
		$data['subcounty_id'] = $district_id;
		$data['facility_code'] = $facility_code;
		$data['district_data'] = $districts;
		$data['maps'] = $map;
		$data['counties'] = $county_name;
		$this -> load -> view("v2/dashboard/dashboard_kemsa_issues", $data);
	
	}

	public function kemsa_pod_dashboard($county_id = NULL, $district_id = NULL,$quarter = NULL,$month = NULL)
	{
		// $page_caching = $this->output->cache(CACHE_REFRESH_INTERVAL);
		/*PARAMETER CHECKS AND DEFAULT VALUES*/
			$default = "tracer";//FYI
			$county_id = (isset($county_id) && $county_id > 0)? $county_id:NULL;
			$district_id = (isset($district_id) && $district_id > 0)? $district_id:NULL;
			$facility_code = (isset($facility_code) && $facility_code > 0)? $facility_code:NULL;
			$quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;

			$data['filter_years'] = $this->get_years_to_date();
			$filtered_name = NULL;

			$prev_yr = date("Y",strtotime('-1 YEAR'));
			$quarters_prev_yr = $this->get_year_quarter(NULL,NULL,$prev_yr,1);
			$quarters_curr_yr = $this->get_year_quarter(NULL,NULL,NULL,1);
			// echo "<pre>";print_r($quarters_prev_yr);exit;
			$data['quarters'] = array_merge_recursive($quarters_curr_yr, $quarters_prev_yr);

			// $data['filtered'] = $filtered;
			// echo "<pre>";print_r($data);exit;
	        $current_date = date('Y-m-d');

			$page_title = "RTK KEMSA Issues Dashboard.";
			$data['page_title'] = $page_title;
	        
	        $current = date('m');

	        $curMonth = $current;
	        $curQuarter = ceil($curMonth/3);//KARSAN

	        if ($quarter > 0) {
	        	$curQuarter = $quarter;
	        }

			$title_append = (isset($filtered_name) && $filtered_name !='')? $filtered_name:NULL;

			if ($quarter > 0) {
				// $title_append .= " Report";
			}else{
				// $title_append .= " as of ".date('F',strtotime("-1 MONTH"))." Report";
				$title_append .= " as of ".date('Y').".";
			}

			$title_prepend = (isset($county_id) && $county_id > 0)? 'County':'Sub-County';

			// $title_append .= (isset($quarter) && $quarter > 0)? ' Quarter: '.$quarter:NULL;
			// echo "<pre>";print_r($title_append);exit;
			if ($quarter > 0) {
				$quartered = 1;
				// $quarter_append = ' <span class="font-blue-steel">*</span>';
			}
		/*END OF PARAMETER CHECKS AND DEFAULT VALUES*/
		// echo "<pre>";print_r(FTP_PATH);exit;
		$ftp_path    = FCPATH.'ftp';
		$files = scandir($ftp_path);
		$files = array_diff(scandir($ftp_path), array('.', '..','.gitkeep'));
		$document_months = $files;
		// echo "<pre>";print_r($files);exit;

		$pod_data = array();
		foreach ($files as $key => $value) {
			// echo "<pre>";print_r($value);exit;
			$month_yr = $county_name = NULL;
			$m_yr = $value;
			$month_yr = strtoupper($value);

			$dir_path = $ftp_path.'/'.$value;
			$dir_files = scandir($dir_path);
			$dir_files = array_diff(scandir($dir_path), array('.', '..'));
			$documents_by_month = $files;

			// echo "<pre>";print_r($dir_files);exit;
			$counter = 0;
			foreach ($dir_files as $keyy => $valuee) {
				$counter ++;
				// echo "<pre>";print_r($counter);exit;
				$c_name = $valuee;
				$county_name = strtoupper($valuee);
				// echo "<pre>";print_r($valuee);exit;
				$file_path = $dir_path.'/'.$valuee;
				$county_files = scandir($file_path);
				$county_files = array_diff(scandir($file_path), array('.', '..'));
				// echo "<pre>";print_r($county_files);exit;
				$all_documents[] = $files;
				$pod_data[$month_yr][$county_name]['all_files'] = $county_files;

				$file_counter = 0;
				foreach ($county_files as $county_file => $county_file_data) {
					$file_counter ++;
					// echo "<pre>";print_r($county_file_data);exit;
					$pod_data[$month_yr][$county_name]['file_data'][$file_counter]['file_name'] = $county_file_data;
					$pod_data[$month_yr][$county_name]['file_data'][$file_counter]['file_url'] = $file_path.'/'.$county_file_data;
					$pod_data[$month_yr][$county_name]['file_data'][$file_counter]['file_url_local'] = $m_yr.'_'.$c_name.'_'.$county_file_data;
				}
			}
			// echo "<pre>";print_r($dir_files);exit;
		}

		// echo "<pre>";print_r($pod_data);exit;

		// $title_prepend = (isset($title_prepend) && $title_prepend !='')? $title_prepend:'National';
		// echo "<pre>";print_r($data);exit;
		// echo "<pre>";print_r($county_data_s);exit;

		$data['title_append'] = $title_append;
		$data['quartered'] = $quartered;
		$data['quarter_append'] = $quarter_append;
		$data['pod_data'] = $pod_data;
		// $data['tracer_commodities'] = $commodities;
		$data['facility_count'] = $facility_count;
		$data['commodity_count'] = $commodity_count;
		$data['content_view'] = 'dashboard/dashboard';
		$data['county_data'] = $counties;
		$data['title_prepend'] = $title_prepend;
		$data['county_count'] = count($counties_using_CD4);
		$data['district_data'] = $districts;
		$data['commodity_divisions'] = $commodity_divisions;
		$data['title'] = "National Dashboard";
		$data['county_data'] = $counties;
		$data['county_id'] = $county_id;
		$data['subcounty_id'] = $district_id;
		$data['facility_code'] = $facility_code;
		$data['district_data'] = $districts;
		$data['maps'] = $map;
		$data['counties'] = $county_name;
		$this -> load -> view("v2/dashboard/dashboard_kemsa_pods", $data);
	
	}

	public function kemsa_issue_data_analysis_dashboard($county_id = NULL,$district_id = NULL,$facility_code = NULL,$commodity_id = NULL,$quarter = NULL,$year = NULL,$month = NULL,$by = NULL,$format = NULL)
	{
		// echo "HERE";exit;
		// $page_caching = $this->output->cache(CACHE_REFRESH_INTERVAL);
        $api =  new Api();//API INIT
		$default = "tracer";
		$county_id = (isset($county_id) && $county_id > 0)? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id > 0)? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code > 0)? $facility_code:NULL;
		$quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;

		$data['filter_years'] = $this->get_years_to_date();
		$filtered_name = NULL;

		$prev_yr = date("Y",strtotime('-1 YEAR'));
		$quarters_prev_yr = $this->get_year_quarter(NULL,NULL,$prev_yr,1);
		$quarters_curr_yr = $this->get_year_quarter(NULL,NULL,NULL,1);
		$data['quarters'] = array_merge_recursive($quarters_curr_yr, $quarters_prev_yr);
		$data['filtered'] = $filtered;
		// echo "<pre>";print_r($data);exit;
        $current_date = date('Y-m-d');

        $query = "
        SELECT 
			c.id as county_id,
			c.county,
		    d.id as district_id,
		    d.district,
		    f.facility_name,
		    k.*,
		    ki.created_at as old_retrieval_date
		FROM
		    kemsa_issue_discrepancies k,
		    kemsa_issues ki,
		    counties c,
		    districts d,
		    facilities f
		WHERE
		 k.facility_code = f.facility_code
		 AND f.district = d.id
		 AND d.county = c.id
		 AND k.kemsa_issue_id = ki.id;
		 ";
		 // echo "<pre>";print_r($query);exit;
		 $result = $this->db->query($query)->result_array();

		 // echo "<pre>";print_r($result);exit;
		$data['data_analysis'] = $result;

		$page_title = "RTK KEMSA Issue Data Analysis Dashboard.";
		$data['page_title'] = $page_title;

        // echo "<pre>";print_r($district_drawing_data);exit;
        $current = date('m');
        $curMonth = $current;
        $curQuarter = ceil($curMonth/3);//KARSAN

        if ($quarter > 0) {
        	$curQuarter = $quarter;
        }

		$title_append = (isset($filtered_name) && $filtered_name !='')? $filtered_name:NULL;

		if ($quarter > 0) {
			// $title_append .= " Report";
		}else{
			// $title_append .= " as of ".date('F',strtotime("-1 MONTH"))." Report";
			$title_append .= " as of ".date('jS F Y').".";
		}

		$title_prepend = (isset($county_id) && $county_id > 0)? 'County':'Sub-County';

		if ($quarter > 0) {
			$quartered = 1;
			// $quarter_append = ' <span class="font-blue-steel">*</span>';
		}


		// echo "<pre>";print_r($kemsa_issue_data_analysis_arr);exit;
		$data['title_append'] = $title_append;
		$data['quartered'] = $quartered;
		$data['quarter_append'] = $quarter_append;
		// $data['tracer_commodities'] = $commodities;
		$data['facility_count'] = $facility_count;
		$data['commodity_count'] = $commodity_count;
		$data['content_view'] = 'dashboard/dashboard';
		$data['county_data'] = $counties;
		$data['title_prepend'] = $title_prepend;
		$data['county_count'] = count($counties_using_CD4);
		$data['district_data'] = $districts;
		$data['commodity_divisions'] = $commodity_divisions;
		$data['title'] = "National Dashboard";
		$data['county_data'] = $counties;
		$data['county_id'] = $county_id;
		$data['subcounty_id'] = $district_id;
		$data['facility_code'] = $facility_code;
		$data['district_data'] = $districts;
		$data['maps'] = $map;
		$data['counties'] = $county_name;
		$this -> load -> view("v2/dashboard/dashboard_kemsa_issue_data_analysis", $data);
	
	}

	public function get_kemsa_issue_data_analysis_modal($first_key,$second_key,$commodity_id,$delivery_note)
	{
        $api =  new Api();//API INIT
        // echo "<pre>";print_r(urldecode($delivery_note));exit;
        $delivery_note = urldecode($delivery_note);
        $cache_key = "kemsa_issue_data_analysis".date('Y-m-d');
        $cache_check = $this->memcached_check($cache_key);
        // echo "<pre>";print_r($api);exit;

        // echo "<pre>";print_r($cache_check);exit;
        if ($cache_check['status'] == "FAILED") {

            $kemsa_issue_data_analysis = $api->kemsa_issue_data_analysis();
            // echo "<pre>";print_r($kemsa_issue_data_analysis);exit;
            $cache_data['key'] = $cache_key;
            $cache_data['data'] = $kemsa_issue_data_analysis;

            $cache_save = $this->memcached_save($cache_data);
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $kemsa_issue_data_analysis = $cache_check['data'];
        } 

        // echo "<pre>";print_r($kemsa_issue_data_analysis);exit;
        $kemsa_issue_data_analysis_arr = json_decode($kemsa_issue_data_analysis,TRUE);
        /*echo "<pre>";print_r($first_key);
        echo "<pre>";print_r($second_key);
        echo "<pre>";print_r($delivery_note);
        echo "<pre>";print_r($commodity_id);exit;*/
        // echo "<pre>kemsa_issue_data_analysis_arr: ";print_r($kemsa_issue_data_analysis_arr[$first_key][$commodity_id][$second_key][$delivery_note]);exit;

        $data = $kemsa_issue_data_analysis_arr[$first_key][$commodity_id][$second_key][$delivery_note];
        // echo "<pre>";print_r($data);exit;
		$html = '';
		$html .='
			<div class="clearfix col-md-6">
			<h4>Current</h4>
			<table class="table table-bordered display cell-border compact col-md-6" cellspacing="0" >';
		$html .='<tr>';
		$html .='<td><strong>County: </strong></td>';
		$html .='<td>'.$data['current']['county'].'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Subcounty: </strong></td>';
		$html .='<td>'.$data['current']['district'].'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Facility: </strong></td>';
		$html .='<td>'.$data['current']['facility_name'].'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Facility Code: </strong></td>';
		$html .='<td>'.$data['current']['facility_code'].'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Commodity: </strong></td>';
		if ($data['current']['commodity_id'] == 4) {
			$comm = "Screening";
		}else{
			$comm = "Confirmatory";
		}
		$html .='<td>'.$comm.'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Quantity Issued: </strong></td>';
		$html .='<td>'.number_format($data['current']['qty_issued']).'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Quantity Requested: </strong></td>';
		$html .='<td>'.number_format($data['current']['qty_requested']).'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Value Issued: </strong></td>';
		$html .='<td>'.number_format($data['current']['value_issued']).'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Value Requested: </strong></td>';
		$html .='<td>'.number_format($data['current']['value_requested']).'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Issue Date: </strong></td>';
		$html .='<td>'.date('d F Y',strtotime($data['current']['issue_date'])).'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Delivery Note: </strong></td>';
		$html .='<td>'.$data['current']['delivery_note'].'</td>';
		$html .='</tr>';
		$html.='
			</table>
			</div>
		';

		$html .='
			<div class="clearfix col-md-6">
			<h4>New</h4>
			<table class="table table-bordered display cell-border compact col-md-6" cellspacing="0" >';
		$html .='<tr>';
		$html .='<td><strong>County: </strong></td>';
		$html .='<td>'.$data['new']['county'].'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Subcounty: </strong></td>';
		$html .='<td>'.$data['new']['district'].'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Facility: </strong></td>';
		$html .='<td>'.$data['new']['facility_name'].'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Facility Code: </strong></td>';
		$html .='<td>'.$data['new']['facility_code'].'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Commodity: </strong></td>';
		if ($data['new']['commodity_id'] == 4) {
			$comm = "Screening";
		}else{
			$comm = "Confirmatory";
		}
		$html .='<td>'.$comm.'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Quantity Issued: </strong></td>';
		$html .='<td>'.number_format($data['new']['qty_issued']).'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Quantity Requested: </strong></td>';
		$html .='<td>'.number_format($data['new']['qty_requested']).'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Value Issued: </strong></td>';
		$html .='<td>'.number_format($data['new']['value_issued']).'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Value Requested: </strong></td>';
		$html .='<td>'.number_format($data['new']['value_requested']).'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Issue Date: </strong></td>';
		$html .='<td>'.date('d F Y',strtotime($data['current']['issue_date'])).'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Delivery Note: </strong></td>';
		$html .='<td>'.$data['new']['delivery_note'].'</td>';
		$html .='</tr>';
		$html.='
			</table>
			</div>
		';

		$html .='
			<div class="clearfix col-md-12">
			<h4>Differences</h4>
			<table class="table table-bordered display cell-border compact col-md-6" cellspacing="0" >';

		if ($data['new']['commodity_id'] == 4) {
			$comm = "Screening";
		}else{
			$comm = "Confirmatory";
		}

		$html .='<tr>';
		$html .='<td><strong>Commodity: </strong></td>';
		$html .='<td>'.$comm.'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Quantity Issued Difference: </strong></td>';
		$q_iss_diff = $data['current']['qty_issued'] - $data['new']['qty_issued'];
		$html .='<td>'.number_format($q_iss_diff).'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Value Issued Difference: </strong></td>';
		$v_iss_diff = $data['current']['value_issued'] - $data['new']['value_issued'];
		$html .='<td>'.number_format($v_iss_diff).'</td>';
		$html .='</tr>';

		$html .='<tr>';
		$html .='<td><strong>Quantity Requested Difference: </strong></td>';
		$q_req_diff = $data['current']['qty_requested'] - $data['new']['qty_requested'];
		$html .='<td>'.number_format($q_req_diff).'</td>';
		$html .='</tr>';


		$html .='<tr>';
		$html .='<td><strong>Value Requested Difference: </strong></td>';
		$v_req_diff = $data['current']['value_requested'] - $data['new']['value_requested'];
		$html .='<td>'.number_format($v_req_diff).'</td>';
		$html .='</tr>';


		
		$html.='
			</table>
			</div>
		';
		echo $html;
	
	}

	public function get_kemsa_issue_details($level = NULL, $level_id = NULL, $commodity_id = NULL,$quarter = NULL,$year = NULL)
	{
		$county_id = $district_id = $facility_code = NULL;
		$commodity_id = ($commodity_id > 0)? $commodity_id:4;
        $quarter = (isset($quarter) && $quarter>0 && $quarter !='')? $quarter:NULL;
        $year = (isset($year) && $year>0)? $year:date('Y');

        // echo "<pre>";print_r($year);exit;
		switch ($level) {
			case 'county':
				$county_id = $level_id;
				break;
			case 'subcounty':
				$district_id = $level_id;
				break;
			case 'facility':
				$facility_code = $level_id;
				break;
			
			default:
				
				break;
		}

		$issue_data = $this->get_kemsa_issue_data($county_id, $district_id, $facility_code, $commodity_id , $quarter, $year,$month,"issue_date");	
		// echo "<pre>";print_r($issue_data);exit;
        $county_arr_s = $county_arr_s_ = $dist_arr_s = $dist_arr_s_ = $fac_arr_s = $fac_arr_s_ = array();
        $county_arr_c = $county_arr_c_ = $dist_arr_c = $dist_arr_c_ = $fac_arr_c = $fac_arr_c_ = array();

        $fac_array = array();
        // echo "<pre>";print_r($counties);exit;
        foreach ($issue_data as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
        	$c_id = $value['county_id'];
        	$c_name = $value['county'];
        	$d_id = $value['district_id'];
        	$d_name = $value['district'];
        	$f_code = $value['facility_code'];
        	$f_name = $value['facility_name'];
        	$com_id = $value['commodity_id'];

        	$fac_arr['county_id'] = $value['county_id'];
    		$fac_arr['county_name'] = $value['county'];
    		$fac_arr['district_id'] = $value['district_id'];
    		$fac_arr['district_name'] = $value['district'];
    		$fac_arr['facility_code'] = $value['facility_code'];
    		$fac_arr['facility_name'] = $value['facility_name'];
    		$fac_arr['commodity_id'] = $value['commodity_id'];
    		$fac_arr['issue_date'] = $value['issue_date'];
    		$fac_arr['delivery_note'] = $value['delivery_note'];

    		$fac_arr['qty_issued'] = $value['qty_issued'];
    		$fac_arr['qty_requested'] = $value['qty_requested'];

    		$fac_arr['level'] = "facility";
    		$fac_arr['level_id'] = $value['facility_code'];

    		array_push($fac_array, $fac_arr);

        	/*if (count($fac_arr[$f_code]) > 0) {
        		$fac_arr[$f_code]['qty_issued'] += $value['qty_issued'];
        		$fac_arr[$f_code]['qty_requested'] += $value['qty_requested'];

        	}else{
        		$fac_arr[$f_code]['county_id'] = $value['county_id'];
        		$fac_arr[$f_code]['county_name'] = $value['county'];
        		$fac_arr[$f_code]['district_id'] = $value['district_id'];
        		$fac_arr[$f_code]['district_name'] = $value['district'];
        		$fac_arr[$f_code]['facility_code'] = $value['facility_code'];
        		$fac_arr[$f_code]['facility_name'] = $value['facility_name'];
        		$fac_arr[$f_code]['commodity_id'] = $value['commodity_id'];
        		$fac_arr[$f_code]['issue_date'] = $value['issue_date'];
        		$fac_arr[$f_code]['delivery_note'] = $value['delivery_note'];

        		$fac_arr[$f_code]['qty_issued'] = $value['qty_issued'];
        		$fac_arr[$f_code]['qty_requested'] = $value['qty_requested'];

        		$fac_arr[$f_code]['level'] = "facility";
        		$fac_arr[$f_code]['level_id'] = $value['facility_code'];
        	}*/
        	
        }//end of summary data foreach

        // echo "<pre>";print_r($fac_array);exit;
        $html = '<table class="table table-bordered datatable display cell-border compact" cellspacing="0" width="100%" id="datatable">
					<thead>
						<tr>
							<th rowspan = "2">Facility Name</th>
							<th rowspan = "2">MFL Code</th>
							<th rowspan = "2">Sub-County</th>
							<th rowspan = "2">County</th>
							<th colspan = "2">Issued from KEMSA</th>
							<th rowspan = "2">Date of Issue</th>
							<th rowspan = "2">Delivery Note</th>
						</tr>
						<tr>
							<th>Units</th>
							<th>Packs</th>
						</tr>
					</thead>
					<tbody>';

		foreach ($fac_array as $key => $value) { 
			$divisor = ($commodity_id == 4)? 100:30;
			$kemsa_issued = $value['qty_issued'];
			$kemsa_issued_packs = round($value['qty_issued']/$divisor);
			$kemsa_issue_date = $value['issue_date'];
			$commodity_id = $value['commodity_id'];
			$rec = $value['q_received'];

			$difference = $kemsa_issued - $rec;
			// echo "<pre>";print_r($difference);exit;
			$class = ($difference>0)? " green ":" red ";
			$class = ($difference==0)? "":$class;

			if ($difference != 0):
			$html .= '
			<tr>
				<td>'.$value['facility_name'].'</td>
				<td>'.$value['facility_code'].'</td>
				<td>'.$value['district_name'].'</td>
				<td>'.$value['county_name'].'</td>
				<td>'.number_format($kemsa_issued).'</td>
				<td>'.number_format($kemsa_issued_packs).'</td>
				<td>'.date('d F Y',strtotime($kemsa_issue_date)).'</td>
				<td>'.$value['delivery_note'].'</td>
			</tr>';

			endif;
		}
		
		$html .='<tbody></table>';

		// echo "<pre>";print_r($html);exit;
		echo $html;
	}


	public function get_county_order_data($county_id = NULL, $district_id = NULL, $facility_code = NULL, $commodity_id = NULL, $quarter = NULL,$month = NULL, $year = NULL,$by = NULL)
	{
		// echo $county_id." ".$district_id." ".$facility_code." ".$quarter." ".$month." ".$year." ".$by;exit;
		// echo $by;exit;
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;
		$county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code!='')? $facility_code:NULL;
		// echo "<pre>";print_r($district_id);exit;
		$new_criteria = "";

		if ($quarter > 0 && $month < 1) {
			$year_ = date('Y');
        	switch ($quarter) {
        		case 1:
        			// echo "Quarter: One";exit;
        			$first_month = "01";
        			$last_month = "03";
        			$firstdate = $year_ . '-' . $first_month . '-01';
	        		$lastdate = $year_ . '-' . $last_month . '-31';
        			break;
        		case 2:
        			// echo "Quarter: Two";exit;
        			$first_month = "04";
        			$last_month = "06";
        			$firstdate = $year_ . '-' . $first_month . '-01';
	        		$lastdate = $year_ . '-' . $last_month . '-31';
        			break;
        		case 3:
        			// echo "Quarter: Three";exit;
        			$first_month = "07";
        			$last_month = "09";
        			$firstdate = $year_ . '-' . $first_month . '-01';
	        		$lastdate = $year_ . '-' . $last_month . '-31';
        			break;
        		case 4:
        			// echo "Quarter: Four";exit;
        			$first_month = "10";
        			$last_month = "12";
        			$firstdate = $year_ . '-' . $first_month . '-01';
	        		$lastdate = $year_ . '-' . $last_month . '-31';
        			break;
        		
        		default:
        			// echo "Quarter: Invalid";exit;
        			break;
        	}
 			$data['date_text'] = "Quarter: ".$quarter;

        	$new_criteria .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
        }elseif (isset($month) && $month > 0) {
            $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');

			$firstdate = $year . '-' . $month . '-01';
    		$lastdate = $year . '-' . $month . '-31';

    		// $firstdate = date('Y-m-d',strtotime($firstdate,"-1 MONTH"));
    		// $lastdate = date('Y-m-d',strtotime($lastdate,"-1 MONTH"));

        	$new_criteria .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
 			$data['date_text'] = date('F Y',strtotime($lastdate));

        }else{
			$year = (isset($year) && $year>0)? $year:date('Y');
        	$new_criteria .= ($year > 0)?" AND YEAR(l.created_at) = $year":"AND YEAR(lco.order_date) = ".date('Y');
 			$data['date_text'] = "Year: ".$year;
        }

		// $year = date('Y');

		$new_criteria .= ($district_id > 0)?" AND d.id = $district_id":NULL;
        $new_criteria .= ($county_id > 0)?" AND c.id = $county_id":NULL;
        $new_criteria .= ($facility_code > 0)?" AND f.facility_code = $facility_code":NULL;

		$c_ids = (isset($commodity_id) && $commodity_id!='')? "'".$commodity_id."'" : "4,5";

		switch ($by) {
            case 'county':
            $group_by = "GROUP BY c.id,commodity_id ";
            break;
            case 'subcounty':
            $group_by = "GROUP BY d.id,commodity_id ";
            break;
            case 'facility':
            $group_by = "GROUP BY f.facility_code,commodity_id ";
            break;   
            default:
            $group_by = "GROUP BY commodity_id ";
            break;
        }

		// echo $new_criteria;exit;
		$query="
			SELECT 
			    c.id AS county_id,
			    c.county,
			    d.id AS district_id,
				d.district,
				f.facility_name,
				f.facility_code,
			    l.district_id,
			    l.commodity_id,
			    lco.order_date,
			    SUM(l.beginning_bal) AS beginning_bal,
			    SUM(l.q_received) AS q_received,
			    SUM(l.q_used) AS q_used,
			    SUM(l.no_of_tests_done) AS no_of_tests_done,
			    SUM(l.closing_stock) AS closing_stock,
			    SUM(l.q_expiring) AS q_expiring,
			    SUM(l.q_requested) AS q_requested,
			    SUM(l.positive_adj) AS positive_adj,
			    SUM(l.negative_adj) AS negative_adj,
			    SUM(l.losses) AS losses,
			    l.created_at
			FROM
			    lab_commodity_details l USE INDEX (order_id,facility_code,district),
			    counties c,
			    districts d,
			    facilities f USE INDEX (facility_code),
			    lab_commodity_orders lco USE INDEX (district)
			WHERE
			    c.id = d.county
			        AND l.order_id = lco.id
			        AND l.facility_code > 0
			        AND l.facility_code = f.facility_code
			        AND lco.district_id = d.id
			        AND l.commodity_id IN ($c_ids)
			        $new_criteria
					$group_by
					ORDER BY lco.order_date DESC;        
			";
		// echo "<pre>";print_r($query);exit;
		$res = $this->db->query($query);
		// $res = $res->result_array();
		// echo "<pre>";print_r($res);exit;
		
		switch ($by) {
            case 'county':
            if ($res) {
                $res = $res->result_array();
                    // echo "<pre>";print_r($beg_data);exit;
                foreach ($res as $key => $value) {
                    $county_id = $value['county_id'];
                    $data['results'][$county_id]['facility_code'] = $value['facility_code'];
                    $data['results'][$county_id]['county'] = $value['county'];
                    $data['results'][$county_id]['county_id'] = $value['county_id'];
                    $data['results'][$county_id]['district'] = $value['district'];
                    $data['results'][$county_id]['district_id'] = $value['district_id'];
                    $data['results'][$county_id]['commodity_id'] = $value['commodity_id'];

                    $data['results'][$county_id]['order_date'] = $value['order_date'];
                    $data['results'][$county_id]['q_received'] = $value['q_received'];
                    $data['results'][$county_id]['q_used'] = $value['q_used'];
                    $data['results'][$county_id]['no_of_tests_done'] = $value['no_of_tests_done'];
                    $data['results'][$county_id]['losses'] = $value['losses'];
                    $data['results'][$county_id]['positive_adj'] = $value['positive_adj'];
                    $data['results'][$county_id]['negative_adj'] = $value['negative_adj'];
                    $data['results'][$county_id]['closing_balance'] = $value['closing_balance'];
                        // array_push($data_, $data);
                }

            }


            break;
            case 'subcounty':
            if ($res) {
                $res = $res->result_array();
                    // echo "<pre>";print_r($res);exit;
                foreach ($res as $key => $value) {
                    $district_id = $value['district_id'];
                        // $qty_issued = $value['qty_issued'];

                    $data['results'][$district_id]['facility_code'] = $value['facility_code'];
                    $data['results'][$district_id]['county'] = $value['county'];
                    $data['results'][$district_id]['county_id'] = $value['county_id'];
                    $data['results'][$district_id]['district'] = $value['district'];
                    $data['results'][$district_id]['district_id'] = $value['district_id'];
                    $data['results'][$district_id]['commodity_id'] = $value['commodity_id'];

                    $data['results'][$district_id]['order_date'] = $value['order_date'];
                    $data['results'][$district_id]['q_received'] = $value['q_received'];
                    $data['results'][$district_id]['q_used'] = $value['q_used'];
                    $data['results'][$district_id]['no_of_tests_done'] = $value['no_of_tests_done'];
                    $data['results'][$district_id]['losses'] = $value['losses'];
                    $data['results'][$district_id]['positive_adj'] = $value['positive_adj'];
                    $data['results'][$district_id]['negative_adj'] = $value['negative_adj'];
                    $data['results'][$district_id]['closing_balance'] = $value['closing_balance'];

                        // array_push($data_, $data);
                }

            }

            break;
            case 'facility':
            if ($res) {
                $res = $res->result_array();
                    // echo "<pre>";print_r($beg_data);exit;
                foreach ($res as $key => $value) {
                    $mfl = $value['facility_code'];

                    $data['results'][$mfl]['facility_code'] = $value['facility_code'];
                    $data['results'][$mfl]['county'] = $value['county'];
                    $data['results'][$mfl]['county_id'] = $value['county_id'];
                    $data['results'][$mfl]['district'] = $value['district'];
                    $data['results'][$mfl]['district_id'] = $value['district_id'];
                    $data['results'][$mfl]['commodity_id'] = $value['commodity_id'];

                    $data['results'][$mfl]['order_date'] = $value['order_date'];
                    $data['results'][$mfl]['q_received'] = $value['q_received'];
                    $data['results'][$mfl]['q_used'] = $value['q_used'];
                    $data['results'][$mfl]['no_of_tests_done'] = $value['no_of_tests_done'];
                    $data['results'][$mfl]['losses'] = $value['losses'];
                    $data['results'][$mfl]['positive_adj'] = $value['positive_adj'];
                    $data['results'][$mfl]['negative_adj'] = $value['negative_adj'];
                    $data['results'][$mfl]['closing_balance'] = $value['closing_balance'];
                        // array_push($data_, $data);
                }

            }

            break;   
            default:
	            if ($res) {
	                $res = $res->result_array();
	            }else{
	                $res = NULL;
	            }
	            $data['results'] = $res;

            break;
        }

		return $data;
	}

	public function get_facility_order_data($county_id = NULL, $district_id = NULL, $facility_code = NULL, $commodity_id = NULL, $quarter = NULL,$month = NULL, $year = NULL)
	{
		// echo "QUARTER: ".$quarter;exit;
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;
		$county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code!='')? $facility_code:NULL;
		$year = (isset($year) && is_numeric($year) && $year>0)? $year:NULL;
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:NULL;

		$new_criteria = "";

		

		if ($quarter > 0 && $month < 1) {
			$year = date('Y');
        	switch ($quarter) {
        		case 1:
        			// echo "Quarter: One";exit;
        			$first_month = "01";
        			$last_month = "03";
        			$firstdate = $year . '-' . $first_month . '-01';
	        		$lastdate = $year . '-' . $last_month . '-31';
        			break;
        		case 2:
        			// echo "Quarter: Two";exit;
        			$first_month = "04";
        			$last_month = "06";
        			$firstdate = $year . '-' . $first_month . '-01';
	        		$lastdate = $year . '-' . $last_month . '-31';
        			break;
        		case 3:
        			// echo "Quarter: Three";exit;
        			$first_month = "07";
        			$last_month = "09";
        			$firstdate = $year . '-' . $first_month . '-01';
	        		$lastdate = $year . '-' . $last_month . '-31';
        			break;
        		case 4:
        			// echo "Quarter: Four";exit;
        			$first_month = "10";
        			$last_month = "12";
        			$firstdate = $year . '-' . $first_month . '-01';
	        		$lastdate = $year . '-' . $last_month . '-31';
        			break;
        		
        		default:
        			// echo "Quarter: Invalid";exit;
        			break;
        	}
        	$new_criteria .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
        }elseif (isset($month) && $month > 0) {
            $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');

            $firstdate = $year . '-' . $month . '-01';
	       	$lastdate = $year . '-' . $month . '-31';
        	$new_criteria .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
        }else{
        	$new_criteria .= ($year > 0)?" AND YEAR(l.created_at) = $year":"AND YEAR(lco.order_date) = ".date('Y');
        }

		$year_ = date('Y');

		$new_criteria .= ($district_id > 0)?" AND d.id = $district_id":NULL;
        $new_criteria .= ($county_id > 0)?" AND c.id = $county_id":NULL;
        $new_criteria .= ($facility_code > 0)?" AND f.facility_code = $facility_code":NULL;

		$c_ids = (isset($commodity_id) && $commodity_id!='')? "'".$commodity_id."'" : "4,5";


		$group_by = " GROUP BY commodity_id,f.facility_code";
		// $limit = "  LIMIT 1000";

		// echo $new_criteria;exit;
		$query="
			SELECT 
			    c.id AS county_id,
			    c.county,
			    d.id AS district_id,
				d.district,
				f.facility_code,
				f.facility_name,
			    l.district_id,
			    l.commodity_id,
			    lco.order_date,
			    SUM(l.beginning_bal) AS beginning_bal,
			    SUM(l.q_received) AS q_received,
			    SUM(l.q_used) AS q_used,
			    SUM(l.no_of_tests_done) AS no_of_tests_done,
			    SUM(l.closing_stock) AS closing_stock,
			    SUM(l.q_expiring) AS q_expiring,
			    SUM(l.q_requested) AS q_requested,
			    SUM(l.positive_adj) AS positive_adj,
			    SUM(l.negative_adj) AS negative_adj,
			    SUM(l.losses) AS losses,
			    l.created_at
			FROM
			    lab_commodity_details l USE INDEX (order_id,facility_code,district),
			    counties c,
			    districts d,
			    facilities f,
			    lab_commodity_orders lco USE INDEX (district)
			WHERE
			    c.id = d.county
			        AND l.order_id = lco.id
			        AND l.facility_code > 0
			        AND l.facility_code = f.facility_code
			        AND lco.district_id = d.id
			        AND l.commodity_id IN ($c_ids)
			        
			        $new_criteria
					$group_by $limit;        
			";
		// echo "<pre>";print_r($query);exit;

		$result = $this->db->query($query)->result_array();
		// echo "<pre>";print_r($result);exit;
		return $result;
	
	}

	public function ajax_fac_summary()
	{
		$data = $this->input->post();
		// echo "<pre>";print_r($data);exit;
		echo json_encode($data);
	}
	public function get_county_allocation_data($county_id = NULL, $district_id = NULL, $month = NULL, $quarter = NULL,$year = NULL)
	{
        $county_id = (isset($county_id) && $county_id > 0) ? $county_id : NULL;
		$month = (isset($month) && $month!='')? $month:date('m');
        $year = (isset($year) && $year!='')? $year:date('Y');
        // echo "<pre>".$county_id." ".$district_id." ".$month." ".$year;exit;
        $month_criteria = "";

        if ($quarter > 0) {
			$year = date('Y');
        	switch ($quarter) {
        		case 1:
        			// echo "Quarter: One";exit;
        			$first_month = "01";
        			$last_month = "03";
        			$firstdate = $year . '-' . $first_month . '-01';
	        		$lastdate = $year . '-' . $last_month . '-31';
        			break;
        		case 2:
        			// echo "Quarter: Two";exit;
        			$first_month = "04";
        			$last_month = "06";
        			$firstdate = $year . '-' . $first_month . '-01';
	        		$lastdate = $year . '-' . $last_month . '-31';
        			break;
        		case 3:
        			// echo "Quarter: Three";exit;
        			$first_month = "07";
        			$last_month = "09";
        			$firstdate = $year . '-' . $first_month . '-01';
	        		$lastdate = $year . '-' . $last_month . '-31';
        			break;
        		case 4:
        			// echo "Quarter: Four";exit;
        			$first_month = "10";
        			$last_month = "12";
        			$firstdate = $year . '-' . $first_month . '-01';
	        		$lastdate = $year . '-' . $last_month . '-31';
        			break;
        		
        		default:
        			// echo "Quarter: Invalid";exit;
        			break;
        	}
        // $month_criteria .= " AND DATE(a.month) BETWEEN DATE('$firstdate') AND DATE('$lastdate')"; 
	    $month_criteria .= " AND a.month BETWEEN '$firstdate' AND '$lastdate'";
        }else{
        	if (is_numeric($month)) { 
    			$first_month = $month;
    			$last_month = $month;
    			$firstdate = $year . '-' . $first_month . '-01';
        		$lastdate = $year . '-' . $last_month . '-31';

	        	// $month_criteria .= " AND a.month BETWEEN '$firstdate' AND '$lastdate'";
        	}else{
				// $month_criteria .= " AND YEAR(a.month) = $year";
        	}

				$month_criteria .= " AND YEAR(a.month) = $year";
        	
        }



        // $month_criteria = "AND MONTH(a.month) = $month AND YEAR(a.month) = $year";

        $new_criteria = ($district_id > 0)?" AND d.id = $district_id":NULL;
        $county_criteria = (isset($county_id) && $county_id > 0) ? " AND c.id = $county_id" : NULL;


		$sql = "
			SELECT 
			    c.zone AS zone,
			    c.county AS county_name,
			    d.district,
			    f.facility_name,
			    f.facility_code,
			    SUM(a.allocate_s) AS allocate_s,
			    ROUND((SUM(a.allocate_s) / 100)) AS screening_kits,
			    SUM(a.allocate_c) AS allocate_c,
			    ROUND((SUM(a.allocate_c) / 30)) AS confirmatory_kits
			FROM
			    allocation_details a,
			    districts d,
			    counties c,
			    facilities f
			WHERE
	            d.id = a.district_id AND c.id = d.county
	            AND d.id = f.district
	            AND f.facility_code = a.facility_code
	            $month_criteria
	            $county_criteria
	            $new_criteria
	            
		";
		// echo "<pre>";print_r($sql);exit;
		$result = $this->db->query($sql)->result_array();
		// echo "<pre>";print_r($result);exit;

		return $result;

	}
	public function drawing_rights_stacked_high_chart($graph_stuff=NULL)
	{
		// echo "<pre>";print_r($graph_stuff);exit;
		$categories = (isset($graph_stuff['categories']) && $graph_stuff['categories']!='')? $graph_stuff['categories'] : "'County One', 'County Two'";
		$graph_variable = "
		Highcharts.chart('".$graph_stuff["div"]."', {
			    chart: {
			        type: 'column'
			    },
			    title: {
			        text: ''
			    },
			    xAxis: {
			        categories: [".$categories."]
			    },
			    yAxis: {
			        min: 0,
			        title: {
			            text: 'Units'
			        },
			        stackLabels: {
			            enabled: false,
			            style: {
			                fontWeight: 'bold',
			                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
			            }
			        }
			    },
			     credits: {
				      enabled: false
				  },
			    legend: {
			        align: 'right',
			        x: -30,
			        verticalAlign: 'top',
			        y: 25,
			        floating: true,
			        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
			        borderColor: '#CCC',
			        borderWidth: 1,
			        shadow: false
			    },
			    tooltip: { crosshairs: [true,true] },
			    plotOptions: {
			        column: {
			            stacking: 'normal',
			            dataLabels: {
			                enabled: false,
			                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
			            }
			        },
			        series: {
		                cursor: 'pointer',
		                point: {
		                    events: {
		                        click: function () {
		                            // window.open(this.options.url, '_blank');
		                            console.log(this.options);
		                        }
		                    }
		                }
		            }
			    },
			    series: [{
			    	name: 'Balance',
			        data: [".$graph_stuff['series_data']['year_total']."],
			        color: '#FF7E1C'
			    }, {
			        name: 'Allocated',
			        data: [".$graph_stuff['series_data']['distributed']."],
			        color: '#69D1C5'
			    }]
			});";
		// echo $graph_variable;exit;	
		// return $variable;		   
		return $graph_variable;		   
	}

	public function drawing_rights_stacked_high_chart_clickable($graph_stuff)
	{
		// echo "<pre>";print_r($graph_stuff);exit;
		$count = count($graph_stuff['categories']);
		$categories_imploded = "'" . implode ( "', '", $graph_stuff['categories']) . "'";
		$series_data_1 = $series_data_2 = '';
		for ($i=0; $i < $count; $i++) { 
			$series_data_1 .= '{
				"y":'.$graph_stuff['series_data']['year_total'][$i].',
				"url":"'.$graph_stuff['url'].$graph_stuff['category_ids'][$i].'",
			},';

			$series_data_2 .= '{
				"y":'.$graph_stuff['series_data']['distributed'][$i].',
				"url":"'.$graph_stuff['url'].$graph_stuff['category_ids'][$i].'",
			},';
		}
		// echo "<pre>";print_r($series_data_1);exit;
		$categories = (isset($graph_stuff['categories']) && $graph_stuff['categories']!='')? $graph_stuff['categories'] : "'County One', 'County Two'";
		$graph_variable = "
		Highcharts.chart('".$graph_stuff["div"]."', {
			    chart: {
			        type: 'column'
			    },
			    title: {
			        text: ''
			    },
			    xAxis: {
			        categories: [".$categories_imploded."]
			    },
			    yAxis: {
			        min: 0,
			        title: {
			            text: 'Units'
			        },
			        stackLabels: {
			            enabled: false,
			            style: {
			                fontWeight: 'bold',
			                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
			            }
			        }
			    },
			     credits: {
				      enabled: false
				  },
			    legend: {
			        align: 'right',
			        x: -30,
			        verticalAlign: 'top',
			        y: 25,
			        floating: true,
			        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
			        borderColor: '#CCC',
			        borderWidth: 1,
			        shadow: false
			    },
			    tooltip: { crosshairs: [true,true] },
			    plotOptions: {
			        column: {
			            stacking: 'normal',
			            dataLabels: {
			                enabled: false,
			                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
			            }
			        },
			        series: {
		                cursor: 'pointer',
		                point: {
		                    events: {
		                        click: function () {
		                            window.open(this.options.url, '_blank');
		                            console.log(this.options);
		                        }
		                    }
		                }
		            }
			    },
			    series: [{
			    	name: 'Balance',
			        data: [".$series_data_1."],
			        color: '#FF7E1C'
			    }, {
			        name: 'Allocated',
			        data: [".$series_data_2."],
			        color: '#69D1C5'
			    }]
			});";
		// echo $graph_variable;exit;	
		// return $variable;		   
		return $graph_variable;		   
	}

	public function allocationvsreceivedvsconsumed_highchart($graph_stuff = NULL)
	{
		
		// echo "<pre>";print_r($graph_stuff);exit;
		$categories = (isset($graph_stuff['categories']) && $graph_stuff['categories']!='')? $graph_stuff['categories'] : "'County One', 'County Two'";

			$variable = "
			Highcharts.chart('".$graph_stuff["div"]."', {
			    chart: {
			        type: 'column'
			    },
			    title: {
			        text: ''
			    },
			    subtitle: {
			        text: 'Source: RTK'
			    },
			    xAxis: {
			        categories: [".$categories."],
			        crosshair: true
			    },
			    yAxis: {
			        min: 0,
			        title: {
			            text: 'Units'
			        }
			    },
			     credits: {
				      enabled: false
				  },
			    tooltip: {
			        headerFormat: '<span style=\"font-size:10px\">{point.key}</span><table>',
			        pointFormat: '<tr><td style=\"color:{series.color};padding:3px\">{series.name}: </td>' +
			            '<td style=\"padding:3px\"><b>{point.y:,.0f}</b></td></tr>',
			        footerFormat: '</table>',
			        shared: true,
			        useHTML: true
			    },
			    plotOptions: {
			        column: {
			            pointPadding: 0.2,
			            borderWidth: 0
			        }
			    },series: [
			    
			    ";
			foreach ($graph_stuff['series_data'] as $key => $value) {
				// echo "<pre>";print_r($value);exit;
				$variable .= "
				{
				    	name: '".$key."',
				        data: [".$value."]
				    },";
			}

			$variable .= "]});";
		// echo $variable_2;exit;	
		// return $variable;		   
		return $variable;		   
	
	}

	public function national_stockcard_v2($county_id = NULL, $district_id = NULL)
    {
    	$county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;

        if (isset($month)) {
            $year = substr($month, -4);
            $month = substr($month, 0, 2);
            $monthyear = $year . '-' . $month . '-01';

        } else {
            $month = $this->session->userdata('Month');
            if ($month == '') {
                $month = date('mY', time());
            }
            $year = substr($month, -4);
            $month = substr_replace($month, "", -4);
            $monthyear = $year . '-' . $month . '-01';
        }
        $englishdate = date('F-Y', strtotime('-1 Month', time()));
        // $month = 04;

        $stock_status = $this->_national_reports_sum($year, $month, $county_id, $district_id);
        // $stock_status = $this->drawing_rights_data($year, $month, $county_id, $district_id, $facility_code);
        // echo "<pre>";print_r($stock_status);exit;
        // echo "<pre>";print_r($year." ".$month);
        // echo "<pre>";print_r($stock_status);exit;
        // echo "<pre>";
        // print_r($stock_status);die();
        $screening_values = $confirmatory_values = $screening_ = array();

        foreach ($stock_status as $keyy => $valuee) {
        	// echo "<pre>";print_r($value);exit;
        	$val = $screening_ = array();
        	foreach ($valuee as $key => $value) {

        		$val['county_id'] = $value['id'];
        		$val['county'] = $value['county'];
		        $val['commodity_name'] = $value['commodity_name'];
		        $val['sum_opening'] = $value['sum_opening'];
		        $val['sum_received'] = $value['sum_received'];
		        $val['sum_used'] = $value['sum_used'];
		        $val['sum_tests'] = $value['sum_tests'];
		        $val['sum_closing_bal'] = $value['sum_closing_bal'];
		        $val['sum_requested'] = $value['sum_requested'];
		        $val['sum_days'] = $value['sum_days'];
		        $val['sum_expiring'] = $value['sum_expiring'];
		        $val['sum_allocated'] = $value['sum_allocated'];

        		// echo "<pre>";print_r($val);exit;
        		$commodity_name = rtrim($value['commodity_name']);

        		array_push($screening_, $val);
        		$screening_values[$commodity_name] = $screening_;
        		// echo "<pre>";print_r($screening_);exit;
        	}
        }

        // echo "<pre>";print_r($screening_values);exit;

       return $screening_values;
    }

    public function national_stockcard_v2_aggr($month = NULL)
    {
    	$county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;

        if (isset($month)) {
            $year = substr($month, -4);
            $month = substr($month, 0, 2);
            $monthyear = $year . '-' . $month . '-01';

        } else {
            $month = $this->session->userdata('Month');
            if ($month == '') {
                $month = date('mY', time());
            }
            $year = substr($month, -4);
            $month = substr_replace($month, "", -4);
            $monthyear = $year . '-' . $month . '-01';
        }
        $englishdate = date('F-Y', strtotime('-1 Month', time()));
        // $month = 04;

        $stock_status = $this->_national_reports_sum($year, $month, $county_id, $district_id);
        // echo "THIS: <pre>";print_r($stock_status);exit;

        $screening_values = $confirmatory_values = $screening_ = array();

        foreach ($stock_status as $keyy => $valuee) {
        	// echo "<pre>";print_r($value);exit;
        	$val = $screening_ = array();
        	foreach ($valuee as $key => $value) {

        		$val['county_id'] = $value['id'];
        		$val['county'] = $value['county'];
		        $val['commodity_name'] = $value['commodity_name'];
		        $val['sum_opening'] = $value['sum_opening'];
		        $val['sum_received'] = $value['sum_received'];
		        $val['sum_used'] = $value['sum_used'];
		        $val['sum_tests'] = $value['sum_tests'];
		        $val['sum_closing_bal'] = $value['sum_closing_bal'];
		        $val['sum_requested'] = $value['sum_requested'];
		        $val['sum_days'] = $value['sum_days'];
		        $val['sum_expiring'] = $value['sum_expiring'];
		        $val['sum_allocated'] = $value['sum_allocated'];

        		// echo "<pre>";print_r($val);exit;
        		$commodity_name = rtrim($value['commodity_name']);

        		array_push($screening_, $val);
        		$screening_values[$commodity_name] = $screening_;
        		echo "<pre>";print_r($screening_);exit;
        	}
        }

        // echo "<pre>";print_r($screening_values);exit;

       return $screening_values;
    }

    public function drawing_rights_data($year, $month, $county_id = NULL,$district_id = NULL,$facility_code = NULL, $by = NULL)
    {
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;
    	$county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code!='')? $facility_code:NULL;
		$month = (isset($month) && $month>0)? $month:date('m');
		$year = (isset($year) && $year>0)? $year:date('Y');
		// echo "YEAR: ".$year." MONTH:".$month;exit;
        
        $monthyear = $year . '-' . $month . '-01';

		// echo "YEAR: ".$year." MONTH:".$month;exit;

        $drawing_rights = $this->get_drawing_right_data($year, $month, $county_id, $district_id, $facility_code, $by);
        // echo "<pre>";print_r($drawing_rights);exit;
        $current = date('Y-m-d');
        $curMonth = $month;
        $curQuarter = ceil($curMonth/3);//KARSAN

        // echo "<pre>";print_r($curQuarter);exit;
        $drawing_rights_data = $drawing_rights_data_ = array();

        foreach ($drawing_rights as $key => $value) {
        	$year_total = $bal_s = $bal_c= 0;
        	$drawing_rights_data_['quarter'] = $curQuarter;
        	$drawing_rights_data_['county_id'] = $value['county_id_'];
        	$drawing_rights_data_['county'] = $value['county'];

        	$drawing_rights_data_['screening_total'] = (isset($value['screening_total']) && $value['screening_total'] > 0)? $value['screening_total']:0;
        	$year_total_s = $value['screening_total']*$curQuarter;

			$bal_s = $year_total_s - $value['screening_distributed'];
			$bal_s = (isset($bal_s) && $bal_s > 0)? $bal_s:0;
			$drawing_rights_data_["screening_balance"] = $bal_s;

        	$drawing_rights_data_['screening_year_total'] = $year_total_s;
        	$drawing_rights_data_['screening_used'] = (isset($value['screening_used']) && $value['screening_used'] > 0)? $value['screening_used']:0;
        	$drawing_rights_data_['screening_distributed'] = (isset($value['screening_distributed']) && $value['screening_distributed'] > 0)? $value['screening_distributed']:0;

        	$year_total_c = $value['confirmatory_total']*$curQuarter;
			$bal_c = $year_total_c - $value['confirmatory_distributed'];
			$bal_c = (isset($bal_c) && $bal_c > 0)? $bal_c:0;
			$drawing_rights_data_["confirmatory_balance"] = $bal_c;
			        	
        	$drawing_rights_data_['confirmatory_total'] = (isset($value['confirmatory_total']) && $value['confirmatory_total'] > 0)? $value['confirmatory_total']:0;
        	$drawing_rights_data_['confirmatory_year_total'] = $value['confirmatory_total']*$curQuarter;
        	$drawing_rights_data_['confirmatory_distributed'] = $value['confirmatory_distributed'];
        	$drawing_rights_data_['confirmatory_distributed'] = (isset($value['confirmatory_distributed']) && $value['confirmatory_distributed'] > 0)? $value['confirmatory_distributed']:0;
        	$drawing_rights_data_['confirmatory_used'] = (isset($value['confirmatory_used']) && $value['confirmatory_used'] > 0)? $value['confirmatory_used']:0;
        	array_push($drawing_rights_data, $drawing_rights_data_);

        	// echo "<pre>";print_r($drawing_rights_data_);exit;
        }
        // echo "<pre>";print_r($drawing_rights_data);exit;
        return $drawing_rights_data;
    }

    public function get_drawing_right_data($year, $month, $county_id = NULL,$district_id = NULL,$facility_code = NULL,$by = NULL)
	{
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;
		// echo "YEAR: ".$year." MONTH:".$month;exit;
        $returnable = array();
        // echo "<pre>";print_r($by);exit;
        $and = $where = $group_by = "";

        switch ($by) {
        	case 'county':
        	$group_by .= " GROUP BY c.id";
        		break;
        	case 'subcounty':
        	// $group_by .= " GROUP BY d.id";
        		break;
        	
        	default:
        	$group_by .= " GROUP BY c.id";
        		break;
        }
        $firstdate = $year . '-' . $month . '-01';
        $firstday = date("Y-m-d", strtotime("$firstdate Month "));

        // $month = date("m", strtotime("$firstdate  Month "));
        // $year = date("Y", strtotime("$firstdate  Month "));
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $lastdate = $year . '-' . $month . '-' . $num_days;

        $and .= (isset($county_id) && $county_id!='')? " AND c.id = $county_id" :NULL; 
        $where .= (isset($county_id) && $county_id!='')? " WHERE c.id = $county_id" :NULL; 
        $where .= (isset($district_id) && $district_id!='')? " WHERE d.id = $district_id" :NULL; 
        // $and .= (isset($district_id) && $district_id!='')? " AND districts.id = $district_id" :NULL;

        $sql = "SELECT 
				    c.id AS county_id_,
				    c.county,
				    cdr.*
				FROM
				    counties c,county_drawing_rights cdr
				WHERE 
				cdr.county_id = c.id $and
				GROUP BY c.id ORDER BY c.id DESC";

		$sql = "
			SELECT 
				c.id AS county_id_,
			    c.county, cdr.*
			FROM
			    counties c
			        LEFT JOIN
			    county_drawing_rights cdr ON c.id = cdr.county_id
			    $where
				$group_by
			ORDER BY c.id DESC;
		";
 		// echo $sql;exit;
 		$result = $this->db->query($sql);
 		// echo "<pre>";print_r($result);exit;
 		// echo "<pre>";print_r($result->result_array());exit;
 		// echo count($result);exit;
 		// $c = count($result);
 		// echo "<pre>";print_r($c);exit;
 		if (!empty($result)) {
 			$res = $result->result_array();
 		}else{
 			$res = NULL;
 		}

        return $res;
    
	}

    public function kemsa_issue_data($year = NULL, $month = NULL, $county_id = NULL,$district_id = NULL,$facility_code = NULL,$quarter = NULL)
    {
    	// echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;
    	// echo "<pre>";print_r($quarter);exit;
    	$county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code!='')? $facility_code:NULL;
		$quarter = (isset($quarter) && $quarter!='')? $quarter:NULL;
 		
 		$year = (isset($year) && $year!='')?$year:NULL;
 		// echo "<pre>";print_r($quarter);
 		// echo "<pre>";print_r($year);
   //      echo "<pre>";print_r($month);exit;
        if (isset($month) && $month > 0) {
        	$year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');
            $monthyear = $year . '-' . $month . '-01';

        } else {

        	/*$year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');
            $month = $this->session->userdata('Month');
            if ($month == '') {
                $month = date('mY', time());
            }
            // $year = substr($month, -4);
            $month = substr_replace($month, "", -4);
            $monthyear = $year . '-' . $month . '-01';*/
        }

        /*echo "<pre>";print_r($monthyear);
        echo "<pre>";print_r($year);
        echo "<pre>";print_r($quarter);
        echo "<pre>";print_r($month);exit;*/
        $kemsa_issues = $this->get_kemsa_county_issue_data($year, $month, $county_id, $district_id,$facility_code,$quarter);
        // echo "<pre>";print_r($kemsa_issues);exit;
        $current = date('Y-m-d');
        $curMonth = $month;
        $curQuarter = ceil($curMonth/3);//KARSAN

        // echo "<pre>";print_r($curQuarter);exit;
        $kemsa_issue_data = $kemsa_issue_data_ = array();
        // echo "<pre>";print_r($kemsa_issues);exit;
        foreach ($kemsa_issues['result'] as $key => $value) {

        	$county = $scr_units = $conf_units= 0;

        	$kemsa_issue_data_['county_id'] = $value['county_id'];
        	$kemsa_issue_data_['county'] = $value['county'];

        	$kemsa_issue_data_['screening_units'] += $value['screening_units'];
        	$kemsa_issue_data_['confirmatory_units'] += $value['confirmatory_units'];


        	// echo "<pre>";print_r($kemsa_issues);exit;
        }
        
        array_push($kemsa_issue_data, $kemsa_issue_data_);
        // echo "<pre>";print_r($kemsa_issue_data);exit;
        $data['kemsa_issue_data'] = $kemsa_issue_data;
        $data['date_text'] = $kemsa_issues['date_text'];
        $data['first_date'] = $kemsa_issues['first_date'];
        $data['last_date'] = $kemsa_issues['last_date'];
        return $data;
    }

    public function get_kemsa_county_issue_data($year = NULL, $month = NULL, $county_id = NULL,$district_id = NULL,$facility_code = NULL,$quarter = NULL)
	{
    	// echo "MONTH: ".$month." YEAR: ".$year;exit;
		/*echo "<pre>";print_r($monthyear);
        echo "<pre>";print_r($year);
        echo "<pre>";print_r($quarter);
        echo "<pre>";print_r($month);exit;*/

        $returnable = array();
        $and = "";
        $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');
		// $year = (isset($year) && is_numeric($year) && $year>0)? $year:NULL;
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:NULL;

        if ($quarter > 0 && $month < 1) {
        	switch ($quarter) {
        		case 1:
        			// echo "Quarter: One";exit;
        			$first_month = "01";
        			$last_month = "03";
        			$firstdate = $year . '-' . $first_month . '-01';
	        		$lastdate = $year . '-' . $last_month . '-31';
        			break;
        		case 2:
        			// echo "Quarter: Two";exit;
        			$first_month = "04";
        			$last_month = "06";
        			$firstdate = $year . '-' . $first_month . '-01';
	        		$lastdate = $year . '-' . $last_month . '-31';
        			break;
        		case 3:
        			// echo "Quarter: Three";exit;
        			$first_month = "07";
        			$last_month = "09";
        			$firstdate = $year . '-' . $first_month . '-01';
	        		$lastdate = $year . '-' . $last_month . '-31';
        			break;
        		case 4:
        			// echo "Quarter: Four";exit;
        			$first_month = "10";
        			$last_month = "12";
        			$firstdate = $year . '-' . $first_month . '-01';
	        		$lastdate = $year . '-' . $last_month . '-31';
        			break;
        		
        		default:
        			// echo "Quarter: Invalid";exit;
        			break;
        	}
        	$and .= " AND issue_date BETWEEN '$firstdate' AND '$lastdate'"; 
 			$data['date_text'] = "Quarter: ".$quarter;

        }elseif (isset($month) && $month > 0) {
            $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');

            $firstdate = $year . '-' . $month . '-01';
            $lastdate = $year . '-' . $month . '-31';
        	$and .= " AND issue_date BETWEEN '$firstdate' AND '$lastdate'"; 
 			$data['date_text'] = date('F Y',strtotime($lastdate));
        }else{
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;
        	
        	$firstdate = $year . '-01-01';
	        // $firstday = date("Y-m-d", strtotime("$firstdate Month "));

	        // $num_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
	        $lastdate = $year . '-12-31';
        	$and .= " AND issue_date BETWEEN '$firstdate' AND '$lastdate'"; 
 			$data['date_text'] = "Year: ".date('Y',strtotime($lastdate));
        }

		if (isset($month) && $month > 0) {
            $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');

			$firstdate = $year . '-' . $month . '-01';
    		$lastdate = $year . '-' . $month . '-31';

        	$and .= " AND issue_date BETWEEN '$firstdate' AND '$lastdate'"; 
 			$data['date_text'] = date('F Y',strtotime($lastdate));
        }
        /*echo "<pre>";print_r($firstdate);
        echo "<pre>";print_r($lastdate);exit;*/
		$data['first_date'] = date('F Y',strtotime($firstdate));
		$data['last_date'] = date('F Y',strtotime($lastdate));
    
        $and .= (isset($county_id) && $county_id!='')? " AND c.id = $county_id" :NULL; 

        // $and .= (isset($district_id) && $district_id!='')? " AND districts.id = $district_id" :NULL;

        $sql = "SELECT 
				    c.county,
				    r_c.*
				FROM
				    counties c,rtk_issued_county r_c
				WHERE 
				c.id = r_c.county_id $and";

 		// echo $sql;exit;
 		$result = $this->db->query($sql);
 		// echo "<pre>";print_r($result->num_rows());exit;
 		if ($result->num_rows()>0) {
 			$data['result'] = $result->result_array();
 		}else{
 			$data['result'] = NULL;
 		}
        return $data;
    
	}

	public function national_aggregate($county_id = NULL, $district_id = NULL,$facility_code = NULL, $quarter = NULL,$month = NULL,$year = NULL)
	{	
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;
		$county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code>0)? $facility_code:NULL;

        $pdf_htm = '';
        if (isset($month) && $month > 0) {
            // $year = substr($month, -4);
            /*$year = date('Y');
            $month = substr($month, 0, 2);
            $monthyear = $year . '-' . $month . '-01';*/

        } else {
            /*$month = date('mY', strtotime("-1 MONTH"));
            $year = date('Y', strtotime("-1 MONTH"));
            $month = substr_replace($month, "", -4);
            $monthyear = $year . '-' . $month . '-01';*/
        }

        // $englishdate = date('F-Y', strtotime('-1 Month', time()));
        $englishdate = date('F-Y', strtotime($monthyear));
        // $month = 04;
        // echo "<pre>";print_r($quarter);exit;
        // echo "<pre>";print_r($year." ".$month);exit;
        $stock_status = $this->_national_reports_sum($year, $month, $county_id,$district_id,$facility_code,$quarter);
        // echo "<pre>";print_r($stock_status);exit;
        // echo "<pre>";
        // print_r($stock_status);die();


        $count = count($stock_status);
        $final_opening4 = $final_received4 = $final_used4 = $final_tests4 = $final_negative4 = $final_losses4 = $final_closing_bal4 = $final_requested4 = $final_expiring4 = $final_days4 = $final_allocated4 = 0;
        $final_opening5 = $final_received5 = $final_used5 = $final_tests5 = $final_negative5 = $final_losses5 = $final_closing_bal5 = $final_requested5 = $final_expiring5 = $final_days5 = $final_allocated5 = 0;
        $final_opening6 = $final_received6 = $final_used6 = $final_tests6 = $final_negative6 = $final_losses6 = $final_closing_bal6 = $final_requested6 = $final_expiring6 = $final_days6 = $final_allocated6 = 0;
        // $final_opening = $final_received = $final_used = $final_tests = $final_negative = $final_losses = $final_closing_bal = $final_requested = $final_expiring = $final_days = $final_allocated = 0;
        for ($i = 0; $i < $count; $i++) {
            foreach ($stock_status[$i] as $key => $value) {
                $commodity_name = "";
                $commodity_name = $value['commodity_name'];
                $sum_opening = $value['sum_opening'];
                $sum_received = $value['sum_received'];
                $sum_used = $value['sum_used'];
                $sum_tests = $value['sum_tests'];
                $sum_closing_bal = $value['sum_closing_bal'];
                $sum_requested = $value['sum_requested'];
                $sum_days = $value['sum_days'];
                $sum_expiring = $value['sum_expiring'];
                $sum_allocated = $value['sum_allocated'];
                $data['created_at'] = $value['created_at'];

                $commodity_name = rtrim($commodity_name);
                // echo $commodity_name;
                if ($commodity_name == "Screening") {
                    $data['final_opening4'] += $sum_opening;
                    $data['final_received4'] += $sum_received;
                    $data['final_used4'] += $sum_used;
                    $data['final_tests4'] += $sum_tests;
                    $data['final_closing_bal4'] += $sum_closing_bal;
                    $data['final_requested4'] += $sum_requested;
                    $data['final_days4'] += $sum_days;
                    $data['final_expiring4'] += $sum_expiring;
                    $data['final_allocated4'] += $sum_allocated;
                } elseif ($commodity_name == "Confirmatory") {
                    $data['final_opening5'] += $sum_opening;
                    $data['final_received5'] += $sum_received;
                    $data['final_used5'] += $sum_used;
                    $data['final_tests5'] += $sum_tests;
                    $data['final_closing_bal5'] += $sum_closing_bal;
                    $data['final_requested5'] += $sum_requested;
                    $data['final_days5'] += $sum_days;
                    $data['final_expiring5'] += $sum_expiring;
                    $data['final_allocated5'] += $sum_allocated;
                }elseif ($commodity_name == "Tie Breaker") {
                    $data['final_opening6'] += $sum_opening;
                    $data['final_received6'] += $sum_received;
                    $data['final_used6'] += $sum_used;
                    $data['final_tests6'] += $sum_tests;
                    $data['final_closing_bal6'] += $sum_closing_bal;
                    $data['final_requested6'] += $sum_requested;
                    $data['final_days6'] += $sum_days;
                    $data['final_expiring6'] += $sum_expiring;
                    $data['final_allocated6'] += $sum_allocated;
                }
            }
        }

        /*echo "<pre>".$final_opening4." ".$final_received4." ".$final_used4." ".$final_tests4." ".$final_closing_bal4." ".$final_requested4." ".$final_days4." ".$final_expiring4." ".$final_allocated4;
        echo "<pre>".$final_opening5." ".$final_received5." ".$final_used5." ".$final_tests5." ".$final_closing_bal5." ".$final_requested5." ".$final_days5." ".$final_expiring5." ".$final_allocated5;
        echo "<pre>".$final_opening6." ".$final_received6." ".$final_used6." ".$final_tests6." ".$final_closing_bal6." ".$final_requested6." ".$final_days6." ".$final_expiring6." ".$final_allocated6;exit;*/
        
    	return $data;
	}

	public function _national_reports_sum($year, $month, $county_id = NULL,$district_id = NULL,$facility_code = NULL,$quarter = NULL)
	{
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;
        $returnable = array();
        $county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
        $district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
        $facility_code = (isset($facility_code) && $facility_code!='')? $facility_code:NULL;
        $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:NULL;
        $by = (isset($by) && $by!='')? $by:NULL;
        
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;
        if ($quarter > 0 && $month < 1) {
			$year = date('Y');
        	switch ($quarter) {
        		case 1:
        			// echo "Quarter: One";exit;
        			$first_month = "01";
        			$last_month = "03";
        			$firstdate = $year . '-' . $first_month . '-01';
	        		$lastdate = $year . '-' . $last_month . '-31';
        			break;
        		case 2:
        			// echo "Quarter: Two";exit;
        			$first_month = "04";
        			$last_month = "06";
        			$firstdate = $year . '-' . $first_month . '-01';
	        		$lastdate = $year . '-' . $last_month . '-31';
        			break;
        		case 3:
        			// echo "Quarter: Three";exit;
        			$first_month = "07";
        			$last_month = "09";
        			$firstdate = $year . '-' . $first_month . '-01';
	        		$lastdate = $year . '-' . $last_month . '-31';
        			break;
        		case 4:
        			// echo "Quarter: Four";exit;
        			$first_month = "10";
        			$last_month = "12";
        			$firstdate = $year . '-' . $first_month . '-01';
	        		$lastdate = $year . '-' . $last_month . '-31';
        			break;
        		
        		default:
        			// echo "Quarter: Invalid";exit;
        			break;
        	}
        }elseif (isset($month) && $month > 0) {
            $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');

            $firstdate = $year . '-' . $month . '-01';
            $lastdate = $year . '-' . $month . '-31';
        }else{
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;

        	$firstdate = $year . '-01-01';
	        // $firstday = date("Y-m-d", strtotime("$firstdate Month "));

	        // $num_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
	        $lastdate = $year . '-12-31';
        }

        // echo "<pre>". $firstdate." ".$lastdate;exit;

        $and = "";
        $and .= (isset($county_id) && $county_id!='')? " AND counties.id = $county_id" :NULL; 
        $and .= (isset($district_id) && $district_id!='')? " AND districts.id = $district_id" :NULL;
        $and .= (isset($facility_code) && $facility_code!='')? " AND facilities.facility_code = $facility_code" :NULL;

        $sql = "SELECT
                SUM(CASE
                WHEN lab_commodity_details.days_out_of_stock > 0 THEN 1
                WHEN lab_commodity_details.days_out_of_stock <= 0 THEN 0
                END) AS sum_days,
                counties.county,
                counties.id,
                lab_commodities.commodity_name,
                SUM(lab_commodity_details.beginning_bal) AS sum_opening,
                SUM(lab_commodity_details.q_received) AS sum_received,
                SUM(lab_commodity_details.q_used) AS sum_used,
                SUM(lab_commodity_details.no_of_tests_done) AS sum_tests,
                SUM(lab_commodity_details.positive_adj) AS sum_positive,
                SUM(lab_commodity_details.negative_adj) AS sum_negative,
                SUM(lab_commodity_details.losses) AS sum_losses,
                SUM(lab_commodity_details.closing_stock) AS sum_closing_bal,
                SUM(lab_commodity_details.q_requested) AS sum_requested,
                SUM(lab_commodity_details.q_expiring) AS sum_expiring,
                lab_commodity_details.created_at
                FROM
                lab_commodity_details  USE INDEX (order_id,facility_code,district),
                facilities USE INDEX (facility_code),
                districts,
                counties,
                lab_commodities
                WHERE
                lab_commodity_details.created_at BETWEEN '$firstdate' AND '$lastdate'
                AND lab_commodity_details.facility_code = facilities.facility_code
                AND facilities.district = districts.id
                AND districts.county = counties.id
                and lab_commodity_details.commodity_id = lab_commodities.id
                $and";
        //         and lab_commodities.id between 0 and 6
        // group by counties.id,lab_commodities.id";
        //$returnable = $this->db->query($sql)->result_array();

        $sql4 = $sql . " AND lab_commodities.id = 4 Group By counties.county ORDER BY lab_commodity_details.created_at ASC";
        // echo "<pre>";print_r($sql4);exit;

        $cache_key = "_national_reports_sum_".$year.$month.$county_id.$district_id.$facility_code.$quarter.date('Y-m-d');
        // echo "<pre>";print_r(md5($cache_key));

        $cache_check = $this->memcached_check($cache_key);

        // echo "<pre>";print_r($cache_check);
        if ($cache_check['status'] == "FAILED") {

            $res3 = $this->db->query($sql4)->result_array();
	        array_push($returnable, $res3);

	        $sql5 = $sql . " AND lab_commodities.id = 5 Group By counties.county ORDER BY lab_commodity_details.created_at ASC";
	        $res4 = $this->db->query($sql5)->result_array();
	        array_push($returnable, $res4);

	        $sql6 = $sql . " AND lab_commodities.id = 6 Group By counties.county ORDER BY lab_commodity_details.created_at ASC";
	        $res5 = $this->db->query($sql6)->result_array();
	        array_push($returnable, $res5);

            // echo "<pre>RETURNABLE: ";print_r($returnable);exit;
            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = $returnable;
            // echo "<pre>";print_r($cache_data);exit;
            $cache_save = $this->memcached_save($cache_data);
            // echo "<pre>";print_r($cache_save);exit;
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $returnable = $cache_check['data'];
        }

        // echo $sql4; die;
        // echo "<pre>";print_r($returnable);die;
        return $returnable;
    
	}

	public function get_drawing_rights_graph($county_id = NULL,$commodity = NULL)
	{
        $months_texts = array();
        $percentages = array();
       // echo "$sql";die();

        for ($i=11; $i >=0; $i--) { 
            $month =  date("mY", strtotime( date( 'Y-m-01' )." -$i months"));
            $j = $i+1;            
            $month_text =  date("M Y", strtotime( date( 'Y-m-01' )." -$j months")); 
            array_push($months_texts,$month_text);            
             if(isset($county_id)){
                     $sql = "select sum(reported) as reported, sum(facilities) as total, month from rtk_county_percentage 
                        where month ='$month' and county_id='$county_id'";
                }else{
                     $sql = "select sum(reported) as reported, sum(facilities) as total, month from rtk_county_percentage 
                        where month ='$month'";
                }

            // echo "$sql";die();
            $res_trend = $this->db->query($sql)->result_array();            
            foreach ($res_trend as $key => $value) {
                $reported = $value['reported'];
                $total = $value['total'];
                $percentage = round(($reported/$total)*100);
                if($percentage>100){
                    $percentage = 100;
                }
                array_push($percentages, $percentage);
                $trend_details[$month] = array('reported'=>$reported,'total'=>$total,'percentage'=>$percentage);
            }
        }   
        $trend_details = json_encode($trend_details);        
        $months_texts = str_replace('"',"'",json_encode($months_texts));        
        $percentages = str_replace('"',"'",json_encode($percentages));                
        $first_month = date("M Y", strtotime( date( 'Y-m-01' )." -12 months")); 
        $last_month = date("M Y", strtotime( date( 'Y-m-01' )." -1 months")); 
        // $percentages = $percentages;
        // $months_texts= $months_texts;
        // $trend_details = $trend_details; 
        $trend_chart = "<script type=\"text/javascript\">
                  $('#trend-chart').highcharts({
                chart: {
                    type: 'line'
                },
                title: {
                    text: '',
                    x: -20 //center
                },
                subtitle: {
                    text: 'Live data from RTK',
                    x: -20
                },
                xAxis: {
                    categories: ".$months_texts."
                },
                  yAxis: {
                      title: {
                          text: 'Reports Submission (%)'
                      },
                      plotLines: [{
                          value: 0,
                          width: 1,
                          color: '#44b6ae'
                      }]
                  },
                  tooltip: {
                      valueSuffix: '%'
                  },
                  legend: {
		            enabled: false
		        },
                  plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                credits: false,
                series: [{
                    color: '#44b6ae',
                    name: 'Reporting Rates',
                    data: ".$percentages."
                }],
                colors: ['#0000FF', '#0066FF', '#00CCFF'],          
          
   
            });
            </script>";
        echo $trend_chart;
    
	}

	public function get_national_trend($county_id = NULL,$district_id = NULL,$facility_code = NULL,$month = NULL,$year = NULL)
	{
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;
		// echo "<pre>";print_r($facility_code);exit;
		$county_id = (isset($county_id) && $county_id > 0)? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id > 0)? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code > 0)? $facility_code:NULL;
		$quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;
		$year = (isset($year) && $year > 0)? $year:date('Y');
		$month = (isset($month) && $month > 0)? $month:date('m');
        $cur_year = date('Y');

        if ($year < $cur_year) {
        	$month = '12';
        	$k = 11;
        }else{
        	$k = 12;
        }

        $i_ = 1;
        $date = $year.'-'.$month.'-01';

        // echo "<pre>";print_r($date);exit;
        $months_texts = array();
        $percentages = array();
       // echo "$sql";die();
        for ($i=$k; $i >=$i_; $i--) { 
            $month_date =  date("Y-m-d", strtotime( date($date)." -$i months"));
            $month =  date("mY", strtotime( date($date)." -$i months"));
            $month_num =  date("m", strtotime( date($date)." -$i months"));
            $year =  date("Y", strtotime( date($date)." -$i months"));
            // echo "<pre>";print_r($month);
            $j = $i+1;             

            if(isset($county_id) && $county_id > 0){
                    $sql = "select sum(reported) as reported, sum(facilities) as total, sum(percentage) as percentage, month from rtk_county_percentage where month ='$month' and county_id='$county_id'";
                    $sql_new = "select percentage from rtk_county_percentage where month='$month' and county_id='$county_id'";


                    $from = ',districts,counties';
	            	$conditions .= "and lab_commodity_orders.district_id= districts.id and districts.county = counties.id and counties.id = $county_id";
	            	$total_fac_append = " AND county_id = $county_id";

	            	$total_fac_q = "
			    	SELECT sum(facilities) as facilities, 
			    	sum(reported) as reported 
			    	FROM `rtk_county_percentage` 
			    	WHERE month='$month' $total_fac_append";

            }elseif(isset($district_id) && $district_id > 0){
				$sql = "select sum(reported) as reported, sum(facilities) as total, sum(percentage) as percentage, month from rtk_district_percentage where month ='$month' and district_id='$district_id'";
	            $total_fac_append = " AND district_id = $district_id";

				$total_fac_q = "
		    	SELECT sum(facilities) as facilities, 
		    	sum(reported) as reported 
		    	FROM `rtk_district_percentage` 
		    	WHERE month='$month' $total_fac_append";

            }elseif(isset($facility_code) && $facility_code > 0){
            	// $reported = lab_commodity_orders::get_recent_lab_orders($facility_code);
            	$rates = $this->get_facility_reporting_rates($facility_code,$month_date);
            	// echo "<pre>";print_r($rates);exit;
            }
            else{
                    $sql = "select sum(reported) as reported, sum(facilities) as total, sum(percentage) as percentage, month from rtk_county_percentage where month ='$month'";

            		$sql_new = "select percentage from rtk_county_percentage where month='$month'";

	            	// $total_fac_append = " AND district_id = $district_id";
	            	$total_fac_append = "";
            		$total_fac_q = "
			    	SELECT sum(facilities) as facilities, 
			    	sum(reported) as reported 
			    	FROM `rtk_county_percentage` 
			    	WHERE month='$month' $total_fac_append";
            }


            if ($facility_code > 0) {

		        $xArr = $yArr = $xArr1 = array();
		        $cumulative_result = 0;

	            $count = $rates['count'];
	            $order_date = substr($rates['order_date'], -2);
	            $order_date = date('jS', strtotime($rates['order_date']));
	            $cumulative_result += $count;
	            array_push($xArr1, $cumulative_result);
	            array_push($yArr, $order_date);
	            array_push($xArr, $count);

		        $facility_count = $rates['total'];
		        // echo "<pre>";print_r($facility_count);exit;
		        // echo "<pre>";print_r($total_facilities);exit;
		        // echo "$cumulative_result";die();
		        $data['cumulative_result'] = $cumulative_result;
		        $data['reported'] = $rates['reported'];
		        $data['jsony'] = json_encode($yArr);
		        $data['jsonx'] = str_replace('"', "", json_encode($xArr));
		        $data['jsonx1'] = str_replace('"', "", json_encode($xArr1));

		        // echo $cumulative_result." ".$facility_count." ".$month;
		        $reporting_percentage = ($cumulative_result/$facility_count)*100;
	            // echo "<pre>";print_r($reporting_percentage);exit;   
	            $percentage = $percentage_tmp = $counter = 0;         

                $reported = $rates['reported'];
                $total = $rates['total'];
                $percentage = $rates['percentage'];
	            
                $percentage = $percentage + 0;

                if($rates['percentage']>100){
	                $percentage = 100;
	            }
	            
		        $percentage = round($reporting_percentage);

	            if(!is_numeric($percentage)){
	                $percentage = 0;
	            }

	            if($percentage>100){
	                $percentage = 100;
	            }

	            // echo "<pre>".$month_text." ".$percentage_tmp." ".$percentage;


	            // echo "<pre>";print_r($counter);exit;
	            $trend_details[$month] = array('reported'=>$reported,'total'=>$total,'percentage'=>$percentage);
				array_push($percentages, $percentage);

            }
            else{
            	// echo "<pre>";print_r($sql);exit;
	            // echo "<pre>";print_r($total_fac_q);exit;
	            $res_trend = $this->db->query($sql)->result_array();
	            // echo "<pre>";print_r($res_trend);
	            // $facility_code = NULL;

	            $reporting_rates = $this->dash_reporting_rates($county_id, $district_id, $facility_code, $year, $month_num);
	            // echo "<pre>";print_r($reporting_rates);exit;
		        $xArr = $yArr = $xArr1 = array();
		        $cumulative_result = 0;

		        foreach ($reporting_rates as $value) {
		            $count = $value['count'];
		            $order_date = substr($value['order_date'], -2);
		            $order_date = date('jS', strtotime($value['order_date']));
		            $cumulative_result += $count;
		            array_push($xArr1, $cumulative_result);
		            array_push($yArr, $order_date);
		            array_push($xArr, $count);
		        }

			    // echo "<pre>";print_r($total_fac_q);exit;
			    $total_fac = $this->db->query($total_fac_q);
		        $total_facilities = $total_fac->result_array();
		        $facility_count = $total_facilities[0]['facilities'];
		        // echo "<pre>";print_r($facility_count);exit;
		        // echo "<pre>";print_r($total_facilities);exit;
		        // echo "$cumulative_result";die();
		        $data['cumulative_result'] = $cumulative_result;
		        $data['reported'] = $facility_count[0]['reported'];
		        $data['jsony'] = json_encode($yArr);
		        $data['jsonx'] = str_replace('"', "", json_encode($xArr));
		        $data['jsonx1'] = str_replace('"', "", json_encode($xArr1));

		        // echo $cumulative_result." ".$facility_count." ".$month;
		        $reporting_percentage = ($cumulative_result/$facility_count)*100;
	            // echo "<pre>";print_r($reporting_percentage);exit; 
	            // echo "<pre>"	;print_r($res_trend);exit;  
	            $percentage = $percentage_tmp = $counter = 0;         
	            foreach ($res_trend as $key => $value) {
	            	// echo "<pre>";print_r($value);
	                $reported = $value['reported'];
	                $total = $value['total'];
	                $month_yr = $value['month'];

	                /*if($value['percentage']>100){
		                $percentage = 100;
		            }*/

	                if (isset($county_id)) {
	                	$percentage = $value['percentage'];
	                	// array_push($percentages, $percentage);
	                }else{
	                	// $percentage = $value['percentage'];
	                	// $percentage = $value['percentage'];

						$percentage_tmp = $value['percentage'];
						// $percentage_tmp += $value['percentage'];
						$counter += 1;
			        	// $percentage_final = round($percentage_tmp/47);

			        	$percentage_final = round(($reported/$total) * 100);

			        	// echo "<pre>";print_r($percentage_final);
			        	// echo "<pre>";print_r($reported);
			        	// echo "<pre>";print_r($total);
			        	// echo "<pre>";print_r($reported/$total);
			        	// echo "<pre>";print_r($percentage_final);
		                $percentage = $percentage_final + 0;
	                }
	                // echo "<pre>PERCENTAGE: ".$percentage;
	                $percentage = $percentage + 0;

			        // $percentage = round($reporting_percentage);

		            if(!is_numeric($percentage)){
		                $percentage = 0;
		            }

		            if($percentage>100){
		                $percentage = 100;
		            }

		            // echo "<pre>".$month_text." ".$percentage_tmp." ".$percentage;


		            // echo "<pre>";print_r($counter);exit;
		            // $trend_details[$month] = array('reported'=>$reported,'total'=>$total,'percentage'=>$percentage);
		            if (isset($reported) && $reported !='') {
		            	$mnth = substr($month_yr, 0, 2);
	                	$yr = substr($month_yr, -4);
	                	$yr_mnth_date = $yr.'-'.$mnth.'-01';
	                	// echo "<pre>";print_r($yr_mnth_date);exit;
	                	// $month_text =  date("M Y", strtotime( date($date)." -$i months")); 
	                	// $month_text = date('M Y',strtotime("-1 MONTH",strtotime($yr_mnth_date)));
	                	$month_text = date('M Y',strtotime("-0 MONTH",strtotime($yr_mnth_date)));
	            		array_push($months_texts,$month_text); 

			            $trend_details[$mnth] = array('reported'=>$reported,'total'=>$total,'percentage'=>$percentage,'month_year'=>$month_yr);
						array_push($percentages, $percentage);
		            }

	            }//end of for

	            
        }

        }   

        // echo "<pre>";print_r($trend_details);
        // echo "<pre>";print_r($percentages);exit;
        $months_texts = array_unique($months_texts);
        // echo "<pre>";print_r($months_texts);exit;
        $trend_details = json_encode($trend_details);        
        $months_texts = str_replace('"',"'",json_encode($months_texts));        
        $percentages = json_encode($percentages);                
        $first_month = date("M Y", strtotime( date( 'Y-m-01' )." -12 months")); 
        $last_month = date("M Y", strtotime( date( 'Y-m-01' )." -1 months")); 
        // $percentages = $percentages;
        // $months_texts= $months_texts;
        // $trend_details = $trend_details; 

        // echo "<pre>";print_r($months_texts);
        // echo "<pre>";print_r($percentages);exit;

        $trend_chart = "";
        // $trend_chart = "<script type=\"text/javascript\">";

        $trend_chart_ .= "
                  $('#trend-chart').highcharts({
                chart: {
                    type: 'line'
                },
                title: {
                    text: '',
                    x: -20 //center
                },
                subtitle: {
                    text: 'Live data from RTK',
                    x: -20
                },
                xAxis: {
                    categories: ".$months_texts."
                },
                  yAxis: {
                      title: {
                          text: 'Reports Submission (%)'
                      },
                      plotLines: [{
                          value: 0,
                          width: 1,
                          color: '#44b6ae'
                      }]
                  },
                  tooltip: {
                      valueSuffix: '%'
                  },
                  plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                credits: false,
                series: [{
                    color: '#44b6ae',
                    name: 'Reporting Rates',
                    data: ".$percentages."
                }],
                colors: ['#0000FF', '#0066FF', '#00CCFF'],          
        });";

        $trend_chart = "<script type=\"text/javascript\">";
        $trend_chart .= "
			Highcharts.chart('trend-chart', {
				    chart: {
				        type: 'line'
				    },
				    title: {
				        text: ''
				    },
				    xAxis: {
				        categories: ".$months_texts."
				    },
				    yAxis: {
				        min: 0,
				        title: {
				            text: 'Percentage (%)'
				        },
				        stackLabels: {
				            enabled: false,
				            style: {
				                fontWeight: 'bold',
				                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
				            }
				        }
				    },
				     credits: {
					      enabled: false
					  },
				    legend: {
				        align: 'right',
				        x: -30,
				        verticalAlign: 'right',
				        y: 0,
				        floating: true,
				        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
				        borderColor: '#CCC',
				        borderWidth: 1,
				        shadow: false
				    },
				    tooltip: { crosshairs: [true,true] },
				    plotOptions: {
				        column: {
				            stacking: 'normal',
				            dataLabels: {
				                enabled: false,
				                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
				            }
				        }
				    },
				    series: [{
	                    color: '#44b6ae',
	                    name: 'Reporting Rates',
	                    data: ".$percentages."
	                }]
				});
			";

        $trend_chart .= " </script>";

        // echo "<pre>";print_r($trend_chart);exit;
        echo $trend_chart;
    
	}

	public function get_consumption_trend($county_id = NULL,$district_id = NULL, $facility_code = NULL, $month = NULL, $year = NULL,$quarter = NULL)
	{
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter." BY: ".$by;exit;
        $months_texts = array();
        $percentages = array();
       // echo "$sql";die();

        $trend_results = array();
        // for ($i=24; $i >=1; $i--) {

        $county_id = (isset($county_id) && $county_id > 0)? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id > 0)? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code > 0)? $facility_code:NULL;
		$quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;
		$year = (isset($year) && $year > 0)? $year:date('Y');
		$month = (isset($month) && $month > 0)? $month:NULL;
        $cur_year = date('Y');
        $n = 0;

        if ($year < $cur_year) {
        	// $month = '12';
        	$prev_yr_months = 12;
        	$k = 11;
        }else{
        	// $k = 24;
        	$prev_yr_months = 0;
        	$k = 24;
        }
        // echo "<pre>";print_r($quarter);exit;
        switch ($quarter) {
                case 1:
                    // echo "Quarter: One";exit;
                $first_month = "01";
                $last_month = "03";
        		$date = $year.'-'.$last_month.'-01';
        		$k = 3;
        		$m = 1;
                break;
                case 2:
                    // echo "Quarter: Two";exit;
                $first_month = "04";
                $last_month = "06";
        		$date = $year.'-'.$last_month.'-01';
        		$k = 3;
        		$m = 1;
                break;
                case 3:
                    // echo "Quarter: Three";exit;
                $first_month = "07";
                $last_month = "09";
        		$date = $year.'-'.$last_month.'-01';
        		$k = 3;
        		$m = 1;
                break;
                case 4:
                    // echo "Quarter: Four";exit;
                $first_month = "10";
                $last_month = "12";
        		$date = $year.'-'.$last_month.'-01';
        		$k = 3;
        		$m = 1;
                break;
                
                default:
        		$date = $year.'-'.$month.'-01';
        		// $k = 3;
                break;
            }

        if (isset($month) && $month > 0) {
        	$date = $year.'-'.$month.'-01';
        	$k = 1;
        	$n = 1;
        }else{
        	// echo "<pre>";print_r($prev_yr_months);exit;
        	if ($prev_yr_months > 0) {
        		$date = $year.'-'.$prev_yr_months.'-01';
        	}else{
        		$month = date('m');
        		$date = $year.'-'.$month.'-01';
        	}
        }

        // echo $k." ".$m;exit;
        // echo "<pre>";print_r($date);exit;
        for ($i=$k; $i >=$n; $i--) { 
        	// echo "<pre>";print_r($i);
            $m =  date("m", strtotime( date($date)." -$i months"));
            $y = date('Y',strtotime( date($date)." -$i months"));
            // echo "<pre>".$i." ".$m." ".$y;
            // exit;

            $h = $i-1;            
            $j = $i+1;  

            if (isset($m) && $m > 0) {
            	$month_text =  date("M Y", strtotime(date($date))); 
	        }else{
            	$month_text =  date("M Y", strtotime(date($date)." -$i months")); 
	        }

            $month_text =  date("M Y", strtotime(date($date)." -$i months")); 
            $month_text_ =  date("M_Y", strtotime( date($date)." -$h months")); 
            $month_text_a =  date("M Y", strtotime( date($date)." -$j months")); 

            // array_push($months_texts,$month_text_);   

             if(isset($county_id)){
                $append = " and c.id ='$county_id'";

            }else{
                 $append = "";
            }

            $sql = "
	            SELECT 
				    c.zone,
				    c.county,
				    c.id as county_id,
				    sum(a.allocate_s) as total_allocate_s,
				    sum(a.allocate_c) as total_allocate_c,
				    d.id AS district_id,
				    d.district,
				    a.created_at,
				    a.month
				FROM
				    allocation_details a,
				    districts d,
				    counties c
				WHERE
				    d.id = a.district_id AND c.id = d.county 
				    AND MONTH(month) ='$m' 
				    AND YEAR(month) ='$y' 
				    $append
				GROUP BY MONTH(month) , YEAR(month)";
				// echo "<pre>";print_r($sql);exit;

			// $group_by = "GROUP BY commodity_id , c.id";
			$group_by = "GROUP BY commodity_id";

			$sql2="
			SELECT 
			    c.id AS county_id,
			    c.county,
			    d.id AS district_id,
				d.district,
			    l.facility_code,
			    l.district_id,
			    l.commodity_id,
			    lco.order_date,
			    SUM(l.beginning_bal) AS beginning_bal,
			    SUM(l.q_received) AS q_received,
			    SUM(l.q_used) AS q_used,
			    SUM(l.no_of_tests_done) AS no_of_tests_done,
			    SUM(l.closing_stock) AS closing_stock,
			    SUM(l.q_expiring) AS q_expiring,
			    SUM(l.q_requested) AS q_requested,
			    l.created_at
			FROM
			    lab_commodity_details l,
			    counties c,
			    districts d,
			    lab_commodity_orders lco
			WHERE
			    c.id = d.county
			        AND l.order_id = lco.id
			        AND lco.district_id = d.id
			        AND MONTH(lco.order_date) = '$m'
			        AND YEAR(lco.order_date) = '$y'
			        $append
			        AND l.commodity_id IN (4,5)
			$group_by;        
			";
			//Above query gives data of one month ahead, thus backtracking by a month below

            // echo "<pre>".$sql2;
            $result = $this->db->query($sql2);
            // echo "<pre>";print_r($result);exit;
            $row_count = $result->num_rows();
            // echo "<pre>num_rows(): ";print_r($row_count);

            if ($result->num_rows() > 0) {
            	$result = $result->result_array(); 
            }

            $trend_result = $trend_result_ = array();
            /*echo "<pre>";print_r($m);
            echo "<pre>";print_r($y);
            echo "<pre>";print_r($result);*/

            foreach ($result as $key => $value) {
				$trend_result_ = array();
                $county_id_ = $value['county_id'];
                $district_id_ = $value['district_id'];
                $commodity_id = $value['commodity_id'];
                $month_ = date('Y-m-d',strtotime($value['order_date']));

                //SQL 1
                // $total_allocate_s = $value['total_allocate_s'];
                // $total_allocate_c = $value['total_allocate_c'];

                if ($commodity_id == '4') {
                	$total_allocate_s = $value['q_used'];
                }elseif ($commodity_id == '5') {
                	$total_allocate_c = $value['q_used'];
                }else{
                	$total_allocate_s = 0;
                	$total_allocate_c = 0;
                }

                $trend_result_['county_id'] = $county_id_;
                $trend_result_['commodity_id'] = $commodity_id;
                $trend_result_['district_id'] = $district_id_;
                $trend_result_['month'] = $month_;

                $trend_result_['total_allocate_s'] = $total_allocate_s;
                $trend_result_['total_allocate_c'] = $total_allocate_c;

                array_push($trend_result, $trend_result_);
        		// echo "<pre>";print_r($trend_result);exit;
		        /*if (count($trend_result)>1) {
		                array_push($trend_results, $trend_result);
		        }*/

		        // $month_string = date('M_Y',strtotime($month));

				$month_minus_one = date("M Y",strtotime("-1 MONTH", strtotime($month_)));

				$newdate = date('M_Y', strtotime('-1 months', strtotime($month))); 
            	array_push($months_texts,$month_minus_one);   

		        $trend_results[$month_minus_one] = $trend_result;

        	}   
		}
    	// echo "<pre>";print_r($trend_results);exit;
    	$months_texts = array_unique($months_texts);
    	// echo "<pre>";print_r($months_texts);exit;
        $scr_distibuted = $conf_distibuted = array();

        foreach ($trend_results as $key => $value) {
        	// $used_s = $value
        	// echo "<pre>";print_r($value[0]);
        	$total_allocate_s = $value[0]['total_allocate_s'];
        	$total_allocate_c = $value[0]['total_allocate_c'];

        	$total_allocate_s = (isset($total_allocate_s) && $total_allocate_s > 0)?$total_allocate_s:0;
        	$total_allocate_c = (isset($total_allocate_c) && $total_allocate_c > 0)?$total_allocate_c:0;

        	array_push($scr_distibuted, $total_allocate_s);
        	array_push($conf_distibuted, $total_allocate_c);

        	// echo "<pre>";print_r($total_allocate_s);

        }

		$scr_distibuted_imploded = implode(',', $scr_distibuted);
		$conf_distibuted_imploded = implode(',', $conf_distibuted);

        // echo "<pre>";print_r($scr_distibuted_imploded);
        // echo "<pre>";print_r($conf_distibuted_imploded);exit;

        $trend_details = json_encode($trend_details);        
        $months_texts = str_replace('"',"'",json_encode(array_values($months_texts)));        
        $percentages = str_replace('"',"'",json_encode($percentages));                
        $first_month = date("M Y", strtotime( date( 'Y-m-01' )." -12 months")); 
        $last_month = date("M Y", strtotime( date( 'Y-m-01' )." -1 months")); 

        // echo "<pre>";print_r($months_texts);exit;
        // $percentages = $percentages;
        // $months_texts= $months_texts;
        // $trend_details = $trend_details; 

        $trend_chart = "
        <script type=\"text/javascript\">
		Highcharts.chart('consumption-trend-chart', {
			    chart: {
			        type: 'column'
			    },
			    title: {
			        text: ''
			    },
			    xAxis: {
			        categories: ".$months_texts."
			    },
			    yAxis: {
			        min: 0,
			        title: {
			            text: ''
			        },
			        stackLabels: {
			            enabled: false,
			            style: {
			                fontWeight: 'bold',
			                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
			            }
			        }
			    },
			     credits: {
				      enabled: false
				  },
			    legend: {
			        align: 'right',
			        x: -30,
			        verticalAlign: 'top',
			        y: 25,
			        floating: true,
			        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
			        borderColor: '#CCC',
			        borderWidth: 1,
			        shadow: false
			    },
			    tooltip: { crosshairs: [true,true] },
			    plotOptions: {
			        column: {
			            stacking: 'normal',
			            dataLabels: {
			                enabled: false,
			                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
			            }
			        }
			    },
			    series: [{
			        name: 'Confirmatory',
			        data: [".$conf_distibuted_imploded."],
			        color: '#FF7E1C'
			    }, {
			    	name: 'Screening',
			        data: [".$scr_distibuted_imploded."],
			        color: '#69D1C5'
			    }]
			});
			</script>";
        echo $trend_chart;

	}

	public function get_consumption_trend_by($by = NULL,$county_id = NULL,$district_id = NULL,$facility_code = NULL,$commodity_id = NULL, $month = NULL, $quarter = NULL, $graph_div = NULL,$year = NULL,$ordered = NULL)
	{
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;
		// echo "<pre>THIS: ";print_r($quarter);exit;

		$county_id = (isset($county_id) && $county_id > 0)? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id > 0)? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code > 0)? $facility_code:NULL;
		$quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;
		$year = (isset($year) && $year > 0)? $year:date('Y');
		$month = (isset($month) && $month > 0)? $month:NULL;
        $cur_year = date('Y');

        if ($year < $cur_year) {
        	// $month = '12';
        	$k = 11;
        }else{
        	// $k = 12;
        	$k = 12;
        }

        $date = $year.'-'.$month.'-01';

		$commodity_id = (isset($commodity_id) && $commodity_id > 0)? $commodity_id:"4,5";
        $months_texts = array();
        $percentages = array();
        // echo "$sql";die();
        switch ($by) {
        	case 'county':
        	// $group_by = "GROUP BY commodity_id , c.id";
        	if ($county_id > 0) {
        		// $group_by = "GROUP BY commodity_id , c.id, MONTH(lco.order_date)";
        		$group_by = "GROUP BY commodity_id , c.id, MONTH(lco.order_date),YEAR(lco.order_date)";
        	}else{
        		$group_by = "GROUP BY commodity_id , c.id";
        	}
        		break;

        	case 'subcounty':

        	if ($district_id > 0) {
        		$group_by = "GROUP BY commodity_id , d.id, MONTH(lco.order_date),YEAR(lco.order_date)";
	        	// $limit = "  LIMIT 50 ";
        	}else{
        		$group_by = "GROUP BY commodity_id , d.id";
	        	$limit = "  LIMIT 50 ";
        	}
        		
        		break;

        	case 'facility':
	        	$group_by = "GROUP BY commodity_id , f.facility_code ";
	        	$limit = "  LIMIT 50 ";

        	// if ($facility_code > 0) {
        	// 	$group_by = "GROUP BY commodity_id , d.id, MONTH(lco.order_date)";
	        // 	// $limit = "  LIMIT 50 ";
        	// }else{
        	// }
        		
        		break;
        	
        	default:
			$group_by = "GROUP BY commodity_id , c.id";
        		break;
        }

        $trend_results = array();
        if(isset($county_id) && $county_id > 0){
            $append .= " AND c.id ='$county_id'";
        }

        if(isset($district_id) && $district_id > 0){
            $append .= " AND d.id ='$district_id'";
        }

        if(isset($facility_code) && $facility_code > 0){
            $append .= " AND f.facility_code ='$facility_code'";
        }

        /*if(isset($month) && $month > 0){
            $append .= " AND MONTH(lco.order_date) = '$month'";
        }*/

        $month_text =  date("M Y", strtotime($monthyear)); 
        $month_text_ =  date("M_Y", strtotime($monthyear)); 
        // echo "<pre>";print_r($month_text);exit;
        array_push($months_texts,$month_text);   

        $new_criteria = "";

        if ($quarter > 0 && $month < 1) {
        	// echo "if";exit;
            switch ($quarter) {
                case 1:
                    // echo "Quarter: One";exit;
                    $first_month = "01";
                    $last_month = "03";
                    $firstdate = $year . '-' . $first_month . '-01';
                    $lastdate = $year . '-' . $last_month . '-31';
                    $append .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
                    break;
                case 2:
                    // echo "Quarter: Two";exit;
                    $first_month = "04";
                    $last_month = "06";
                    $firstdate = $year . '-' . $first_month . '-01';
                    $lastdate = $year . '-' . $last_month . '-31';
                    $append .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
                    break;
                case 3:
                    // echo "Quarter: Three";exit;
                    $first_month = "07";
                    $last_month = "09";
                    $firstdate = $year . '-' . $first_month . '-01';
                    $lastdate = $year . '-' . $last_month . '-31';
                    $append .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
                    break;
                case 4:
                    // echo "Quarter: Four";exit;
                    $first_month = "10";
                    $last_month = "12";
                    $firstdate = $year . '-' . $first_month . '-01';
                    $lastdate = $year . '-' . $last_month . '-31';
                    $append .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
                    break;
                
                default:
                    // echo "Quarter: Invalid";exit;
                    break;
            }
        }elseif (isset($month) && $month > 0) {
        	// echo "elseif";exit;
            $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');

			$firstdate = $year . '-' . $month . '-01';
    		$lastdate = $year . '-' . $month . '-31';

			$append .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
        }else{
        	// echo "else";exit;
            $append .= ($year > 0)?" AND YEAR(lco.order_date) = $year ":" AND YEAR(lco.order_date) = ".date('Y');
 			// $data['date_text'] = "Year: ".$year;

        }

        // $year = date('Y');

        // $new_criteria .= ($district_id > 0)?" AND d.id = $district_id":NULL;
        // $new_criteria .= ($county_id > 0)?" AND c.id = $county_id":NULL;
        // $new_criteria .= ($facility_code > 0)?" AND f.facility_code = $facility_code":NULL;
        // echo "<pre>";print_r($append);exit;
			$sql="
				SELECT 
					MONTH(lco.order_date) AS month,
					YEAR(lco.order_date) AS year,
    				lco.order_date,
				    c.id AS county_id,
				    c.county,
				    d.id AS district_id,
					d.district,
				    f.facility_code,
				    f.facility_name,
				    l.facility_code,
				    l.district_id,
				    l.commodity_id,
				    SUM(l.beginning_bal) AS beginning_bal,
				    SUM(l.q_received) AS q_received,
				    SUM(l.q_used) AS q_used,
				    SUM(l.no_of_tests_done) AS no_of_tests_done,
				    SUM(l.closing_stock) AS closing_stock,
				    SUM(l.q_expiring) AS q_expiring,
				    SUM(l.q_requested) AS q_requested,
				    l.created_at
				FROM
				    lab_commodity_details l  USE INDEX (order_id,facility_code,district),
				    counties c,
				    districts d,
					facilities f,
				    lab_commodity_orders lco  USE INDEX (district)
				WHERE
				    c.id = d.county
				        AND l.order_id = lco.id
				        AND lco.district_id = d.id
        				AND l.facility_code > 0
        				AND l.facility_code = f.facility_code
				        
				        $append
				        AND l.commodity_id IN ($commodity_id)
				$group_by
				ORDER BY DATE(lco.order_date) ASC
				$limit;        
			";
			//Above query gives data of one month ahead, thus backtracking by a month below
			// echo "<pre>";print_r($sql);exit;
			// echo "<pre>";print_r($result);exit;
            $result = $this->db->query($sql)->result_array(); 
            // echo "<pre>";print_r($result);exit;
            $trend_result = $trend_result_ = array();
            // echo "<pre>";print_r($month);
            // echo "<pre>";print_r($result);exit;
            foreach ($result as $key => $value) {
				// $trend_result_scr_ = $trend_result_conf = array();
				// echo "<pre>";print_r($value);exit;
                $county_id_ = $value['county_id'];
                $county_name = $value['county'];
                $district_id_ = $value['district_id'];
                $district_name  = $value['district'];
                $facility_code_ = $value['facility_code'];
                $facility_name  = $value['facility_name'];
                $commodity_id = $value['commodity_id'];
                $total_used_ = $value['q_used'];
                $month_ = date('Y-m-d',strtotime($value['order_date']));
                $month_name = date('F',strtotime($value['order_date']));
                $month_year = date('F Y',strtotime($value['order_date']));

                if ($commodity_id == 4) {
                	$commodity_name = "Screening";
                	$category_color = "#69D1C5";
                }elseif ($commodity_id == 5) {
                	$commodity_name = "Confirmatory";
                	$category_color = "#FF7E1C";
                }
                $trend_result_['county_id'] = $county_id_;
                $trend_result_['county_name'] = $county_name;
                $trend_result_['district_id'] = $district_id_;
                $trend_result_['district_name'] = $district_name;
                $trend_result_['facility_code'] = $facility_code_;
                $trend_result_['facility_name'] = $facility_name;
                $trend_result_['commodity_id'] = $commodity_id;
                $trend_result_['commodity_name'] = $commodity_name;
                $trend_result_['total_used'] = $total_used_;
                $trend_result_['month'] = $month_;
                $trend_result_['month_name'] = $month_name;
                $trend_result_['month_year'] = $month_year;

                array_push($trend_result, $trend_result_);
        	}   
        		// echo "<pre>";print_r($trend_result);exit;
        		/*echo "<pre>";print_r($county_id);
        		echo "<pre>";print_r($district_id);
        		echo "<pre>";print_r($quarter);exit;*/
        	if(isset($county_id) && $county_id > 0 && $quarter == 0){
        		/*usort($trend_result, function($a, $b) {
				    // return $a['screening_balance'] - $b['screening_balance'];
				    return $b['total_used'] - $a['total_used'];
				});*/
	        }elseif(isset($county_id) && $county_id > 0 && $quarter > 0){
	        	/*usort($trend_result, function($a, $b) {
				    // return $a['screening_balance'] - $b['screening_balance'];
				    return $b['total_used'] - $a['total_used'];
				});*/
	        }elseif(isset($district_id) && $district_id > 0 && $quarter == 0){
	        	/*usort($trend_result, function($a, $b) {
				    // return $a['screening_balance'] - $b['screening_balance'];
				    return $b['total_used'] - $a['total_used'];
				});*/
	        }elseif(isset($district_id) && $district_id > 0 && $quarter > 0){
	        	/*usort($trend_result, function($a, $b) {
				    // return $a['screening_balance'] - $b['screening_balance'];
				    return $b['total_used'] - $a['total_used'];
				});*/
	        }else{
				usort($trend_result, function($a, $b) {
				    // return $a['screening_balance'] - $b['screening_balance'];
				    return $b['total_used'] - $a['total_used'];
				});
	        }

	        if ($ordered > 0) {
	        	usort($trend_result, function($a, $b) {
				    // return $a['screening_balance'] - $b['screening_balance'];
				    return $b['total_used'] - $a['total_used'];
				});
	        }
    	// echo "<pre>";print_r($trend_result);exit;
    	// echo "<pre>";print_r($months_texts);exit;
        $total_used_arr = $county_arr = $district_arr = $facilities_arr = $months_arr = array();

        foreach ($trend_result as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
        	// echo "<pre>";print_r($value[0]);
        	$total_used = $value['total_used'];
        	$counties = $value['county_name'];
        	$districts = $value['district_name'];
        	$facilities = $value['facility_name'];
        	$month_names = $value['month_name'];
        	$month_years = $value['month_year'];

        	$districts = (isset($districts) && $districts !='')?$districts:0;
        	$counties = (isset($counties) && $counties !='')?$counties:0;
        	$facilities = (isset($facilities) && $facilities !='')?$facilities:0;
        	$total_used = (isset($total_used) && $total_used > 0)?$total_used:0;
        	$month_names = (isset($month_names) && $month_names != "")?$month_names:NULL;
        	$month_years = (isset($month_years) && $month_years != "")?$month_years:NULL;

        	array_push($county_arr, $counties);
        	array_push($district_arr, $districts);
        	array_push($facilities_arr, $facilities);
        	array_push($total_used_arr, $total_used);
        	// array_push($months_arr, $month_names);
        	array_push($months_arr, $month_years);
        }

		$total_used_imploded = implode(',', $total_used_arr);
		$districts_imploded = implode(',', $district_arr);
		$facilities_imploded = '"'.implode('","', $facilities_arr).'"';
		$months_imploded = '"'.implode('","', $months_arr).'"';
		// $counties_imploded = implode(',', $county_arr);
		$counties_imploded = sprintf("'%s'", implode("','", $county_arr ) );
		// $districts_imploded = sprintf("'%s'", implode("','", $district_arr ) );

		$districts_imploded = '"'.implode('","', $district_arr).'"';

		// echo "<pre>";print_r($total_used_imploded);exit;
		switch ($by) {
        	case 'county':
        		if(isset($county_id) && $county_id > 0){
	        		$category_text = $months_imploded;
	        	}else{
        			$category_text = $counties_imploded;
	        	}

        		break;

        	case 'subcounty':
        		// echo "SUBCOUNTY";
        		if(isset($district_id) && $district_id > 0){
	        		$category_text = $months_imploded;
	        	}else{
        			$category_text = $districts_imploded;
	        	}

        		break;

        	case 'facility':
        		// echo "SUBCOUNTY";
        		$category_text = $facilities_imploded;
        		break;
        	
        	default:
        		$category_text = $counties_imploded;
        		break;
        }

        $category_name = $commodity_name;
        $category_data_imploded = (isset($total_used_imploded) && $total_used_imploded > 0)?$total_used_imploded:0;

        // $facilities_imploded = str_replace('"','"',json_encode($facilities_imploded));
        // echo "<pre>";print_r($facilities_imploded);exit;                
        // echo "<pre>";print_r($category_text);
        // echo "<pre>";print_r($total_used_imploded);exit;
        // echo "<pre>";print_r($districts_imploded);exit;
        // echo <pre>";print_r($conf_distibuted_imploded);exit;

        $trend_details = json_encode($trend_details);        
        // $category_text = str_replace('"',"'",json_encode($category_text));        
        $percentages = str_replace('"',"'",json_encode($percentages));                
        $first_month = date("M Y", strtotime( date( 'Y-m-01' )." -12 months")); 
        $last_month = date("M Y", strtotime( date( 'Y-m-01' )." -1 months")); 

        // echo "<pre>";print_r($category_text);exit;
        // $percentages = $percentages;
        // $months_texts= $months_texts;
        // $trend_details = $trend_details; 

        $trend_chart = "
        <script>
		Highcharts.chart('".$graph_div."', {
			    chart: {
			        type: 'column'
			    },
			    title: {
			        text: ''
			    },
			    xAxis: {
			        categories: [".$category_text."]
			    },
			    yAxis: {
			        min: 0,
			        title: {
			            text: 'Units'
			        },
			        stackLabels: {
			            enabled: false,
			            style: {
			                fontWeight: 'bold',
			                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
			            }
			        }
			    },
			     credits: {
				      enabled: false
				  },
			    legend: {
			        align: 'right',
			        x: -30,
			        verticalAlign: 'top',
			        y: 25,
			        floating: true,
			        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
			        borderColor: '#CCC',
			        borderWidth: 1,
			        shadow: false
			    },
			    tooltip: { crosshairs: [true,true] },
			    plotOptions: {
			        column: {
			            stacking: 'normal',
			            dataLabels: {
			                enabled: false,
			                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
			            }
			        }
			    },
			    series: [{
			        name: '".$category_name."',
			        data: [".$category_data_imploded."],
			        color: '".$category_color."'
			    }]
			});
			</script>
			";
			// return $trend_chart;
			// echo "<pre>";print_r($trend_chart);exit;
        echo $trend_chart;
	}

	public function get_consumption_trend_clickable_by($by = NULL,$county_id = NULL,$district_id = NULL,$facility_code = NULL,$commodity_id = NULL, $month = NULL, $quarter = NULL, $graph_div = NULL,$year = NULL,$ordered = NULL)
	{
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;
		// echo "<pre>THIS: ";print_r($quarter);exit;
		// echo "<pre>";print_r($by);exit;
		$county_id = (isset($county_id) && $county_id > 0)? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id > 0)? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code > 0)? $facility_code:NULL;
		$quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;
		$year = (isset($year) && $year > 0)? $year:date('Y');
		$month = (isset($month) && $month > 0)? $month:NULL;
        $cur_year = date('Y');

        if ($year < $cur_year) {
        	// $month = '12';
        	$k = 11;
        }else{
        	// $k = 12;
        	$k = 12;
        }

        $date = $year.'-'.$month.'-01';

		$commodity_id = (isset($commodity_id) && $commodity_id > 0)? $commodity_id:"4,5";
        $months_texts = array();
        $percentages = array();
        // echo "$sql";die();
        switch ($by) {
        	case 'county':
        	// $group_by = "GROUP BY commodity_id , c.id";
        	if ($county_id > 0) {
        		// $group_by = "GROUP BY commodity_id , c.id, MONTH(lco.order_date)";
        		$group_by = "GROUP BY commodity_id , c.id, MONTH(lco.order_date),YEAR(lco.order_date)";
        	}else{
        		$group_by = "GROUP BY commodity_id , c.id";
        	}
        		break;

        	case 'subcounty':

        	if ($district_id > 0) {
        		$group_by = "GROUP BY commodity_id , d.id, MONTH(lco.order_date),YEAR(lco.order_date)";
	        	// $limit = "  LIMIT 50 ";
        	}else{
        		$group_by = "GROUP BY commodity_id , d.id";
	        	$limit = "  LIMIT 50 ";
        	}
        		
        		break;

        	case 'facility':
	        	$group_by = "GROUP BY commodity_id , f.facility_code ";
	        	$limit = "  LIMIT 50 ";

        	// if ($facility_code > 0) {
        	// 	$group_by = "GROUP BY commodity_id , d.id, MONTH(lco.order_date)";
	        // 	// $limit = "  LIMIT 50 ";
        	// }else{
        	// }
        		
        		break;
        	
        	default:
			$group_by = "GROUP BY commodity_id , c.id";
        		break;
        }

        $trend_results = array();
        if(isset($county_id) && $county_id > 0){
            $append .= " AND c.id ='$county_id'";
        }

        if(isset($district_id) && $district_id > 0){
            $append .= " AND d.id ='$district_id'";
        }

        if(isset($facility_code) && $facility_code > 0){
            $append .= " AND f.facility_code ='$facility_code'";
        }

        /*if(isset($month) && $month > 0){
            $append .= " AND MONTH(lco.order_date) = '$month'";
        }*/

        $month_text =  date("M Y", strtotime($monthyear)); 
        $month_text_ =  date("M_Y", strtotime($monthyear)); 
        // echo "<pre>";print_r($month_text);exit;
        array_push($months_texts,$month_text);   

        $new_criteria = "";

        if ($quarter > 0 && $month < 1) {
        	// echo "if";exit;
            switch ($quarter) {
                case 1:
                    // echo "Quarter: One";exit;
                    $first_month = "01";
                    $last_month = "03";
                    $firstdate = $year . '-' . $first_month . '-01';
                    $lastdate = $year . '-' . $last_month . '-31';
                    $append .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
                    break;
                case 2:
                    // echo "Quarter: Two";exit;
                    $first_month = "04";
                    $last_month = "06";
                    $firstdate = $year . '-' . $first_month . '-01';
                    $lastdate = $year . '-' . $last_month . '-31';
                    $append .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
                    break;
                case 3:
                    // echo "Quarter: Three";exit;
                    $first_month = "07";
                    $last_month = "09";
                    $firstdate = $year . '-' . $first_month . '-01';
                    $lastdate = $year . '-' . $last_month . '-31';
                    $append .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
                    break;
                case 4:
                    // echo "Quarter: Four";exit;
                    $first_month = "10";
                    $last_month = "12";
                    $firstdate = $year . '-' . $first_month . '-01';
                    $lastdate = $year . '-' . $last_month . '-31';
                    $append .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
                    break;
                
                default:
                    // echo "Quarter: Invalid";exit;
                    break;
            }
        }elseif (isset($month) && $month > 0) {
        	// echo "elseif";exit;
            $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');

			$firstdate = $year . '-' . $month . '-01';
    		$lastdate = $year . '-' . $month . '-31';

			$append .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
        }else{
        	// echo "else";exit;
            $append .= ($year > 0)?" AND YEAR(lco.order_date) = $year ":" AND YEAR(lco.order_date) = ".date('Y');
 			// $data['date_text'] = "Year: ".$year;

        }

        // $year = date('Y');

        // $new_criteria .= ($district_id > 0)?" AND d.id = $district_id":NULL;
        // $new_criteria .= ($county_id > 0)?" AND c.id = $county_id":NULL;
        // $new_criteria .= ($facility_code > 0)?" AND f.facility_code = $facility_code":NULL;
        // echo "<pre>";print_r($append);exit;
			$sql="
				SELECT 
					MONTH(lco.order_date) AS month,
					YEAR(lco.order_date) AS year,
    				lco.order_date,
				    c.id AS county_id,
				    c.county,
				    d.id AS district_id,
					d.district,
				    f.facility_code,
				    f.facility_name,
				    l.facility_code,
				    l.district_id,
				    l.commodity_id,
				    SUM(l.beginning_bal) AS beginning_bal,
				    SUM(l.q_received) AS q_received,
				    SUM(l.q_used) AS q_used,
				    SUM(l.no_of_tests_done) AS no_of_tests_done,
				    SUM(l.closing_stock) AS closing_stock,
				    SUM(l.q_expiring) AS q_expiring,
				    SUM(l.q_requested) AS q_requested,
				    l.created_at
				FROM
				    lab_commodity_details l  USE INDEX (order_id,facility_code,district),
				    counties c,
				    districts d,
					facilities f,
				    lab_commodity_orders lco  USE INDEX (district)
				WHERE
				    c.id = d.county
				        AND l.order_id = lco.id
				        AND lco.district_id = d.id
        				AND l.facility_code > 0
        				AND l.facility_code = f.facility_code
				        
				        $append
				        AND l.commodity_id IN ($commodity_id)
				$group_by
				ORDER BY DATE(lco.order_date) ASC
				$limit;        
			";
			//Above query gives data of one month ahead, thus backtracking by a month below
			// echo "<pre>";print_r($sql);exit;
			// echo "<pre>";print_r($result);exit;
            $result = $this->db->query($sql)->result_array(); 
            // echo "<pre>";print_r($result);exit;
            $trend_result = $trend_result_ = array();
            // echo "<pre>";print_r($month);
            // echo "<pre>";print_r($result);exit;
            foreach ($result as $key => $value) {
				// $trend_result_scr_ = $trend_result_conf = array();
				// echo "<pre>";print_r($value);exit;
                $county_id_ = $value['county_id'];
                $county_name = $value['county'];
                $district_id_ = $value['district_id'];
                $district_name  = $value['district'];
                $facility_code_ = $value['facility_code'];
                $facility_name  = $value['facility_name'];
                $commodity_id = $value['commodity_id'];
                $total_used_ = $value['q_used'];
                $month_ = date('Y-m-d',strtotime($value['order_date']));
                $month_name = date('F',strtotime($value['order_date']));
                $month_year = date('F Y',strtotime($value['order_date']));

                if ($commodity_id == 4) {
                	$commodity_name = "Screening";
                	$category_color = "#69D1C5";
                }elseif ($commodity_id == 5) {
                	$commodity_name = "Confirmatory";
                	$category_color = "#FF7E1C";
                }
                $trend_result_['county_id'] = $county_id_;
                $trend_result_['county_name'] = $county_name;
                $trend_result_['district_id'] = $district_id_;
                $trend_result_['district_name'] = $district_name;
                $trend_result_['facility_code'] = $facility_code_;
                $trend_result_['facility_name'] = $facility_name;
                $trend_result_['commodity_id'] = $commodity_id;
                $trend_result_['commodity_name'] = $commodity_name;
                $trend_result_['total_used'] = $total_used_;
                $trend_result_['month'] = $month_;
                $trend_result_['month_name'] = $month_name;
                $trend_result_['month_year'] = $month_year;

                array_push($trend_result, $trend_result_);
        	}   
        		// echo "<pre>";print_r($trend_result);exit;
        		/*echo "<pre>";print_r($county_id);
        		echo "<pre>";print_r($district_id);
        		echo "<pre>";print_r($quarter);exit;*/
        	if(isset($county_id) && $county_id > 0 && $quarter == 0){
        		/*usort($trend_result, function($a, $b) {
				    // return $a['screening_balance'] - $b['screening_balance'];
				    return $b['total_used'] - $a['total_used'];
				});*/
	        }elseif(isset($county_id) && $county_id > 0 && $quarter > 0){
	        	/*usort($trend_result, function($a, $b) {
				    // return $a['screening_balance'] - $b['screening_balance'];
				    return $b['total_used'] - $a['total_used'];
				});*/
	        }elseif(isset($district_id) && $district_id > 0 && $quarter == 0){
	        	/*usort($trend_result, function($a, $b) {
				    // return $a['screening_balance'] - $b['screening_balance'];
				    return $b['total_used'] - $a['total_used'];
				});*/
	        }elseif(isset($district_id) && $district_id > 0 && $quarter > 0){
	        	/*usort($trend_result, function($a, $b) {
				    // return $a['screening_balance'] - $b['screening_balance'];
				    return $b['total_used'] - $a['total_used'];
				});*/
	        }else{
				usort($trend_result, function($a, $b) {
				    // return $a['screening_balance'] - $b['screening_balance'];
				    return $b['total_used'] - $a['total_used'];
				});
	        }

	        if ($ordered > 0) {
	        	usort($trend_result, function($a, $b) {
				    // return $a['screening_balance'] - $b['screening_balance'];
				    return $b['total_used'] - $a['total_used'];
				});
	        }
    	// echo "<pre>";print_r($trend_result);exit;
    	// echo "<pre>";print_r($months_texts);exit;
        $total_used_arr = $county_arr = $district_arr = $facilities_arr = $months_arr = array();
		$series_data_1 = $series_data_2 = '';

        switch ($by) {
        	case 'county':
        		$col_url = base_url().'dashboardv2/county_dashboard/';
        		break;
        	case 'subcounty':
        		$col_url = base_url().'dashboardv2/subcounty_dashboard/';
        		break;

        	case 'facility':
        		$col_url = base_url().'dashboardv2/facility_dashboard/';
        		break;
        	default:
        		break;
        }

        foreach ($trend_result as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
        	// echo "<pre>";print_r($value[0]);
        	$total_used = $value['total_used'];
        	$counties = $value['county_name'];
        	$c_id = $value['county_id'];
        	$d_id = $value['district_id'];
        	$f_code = $value['facility_code'];
        	$districts = $value['district_name'];
        	$facilities = $value['facility_name'];
        	$month_names = $value['month_name'];
        	$month_year = date('m_Y',strtotime($value['month']));
        	$month_years = $value['month_year'];

        	$districts = (isset($districts) && $districts !='')?$districts:0;
        	$counties = (isset($counties) && $counties !='')?$counties:0;
        	$facilities = (isset($facilities) && $facilities !='')?$facilities:0;
        	$total_used = (isset($total_used) && $total_used > 0)?$total_used:0;
        	$month_names = (isset($month_names) && $month_names != "")?$month_names:NULL;
        	$month_years = (isset($month_years) && $month_years != "")?$month_years:NULL;

        	array_push($county_arr, $counties);
        	array_push($district_arr, $districts);
        	array_push($facilities_arr, $facilities);
        	array_push($total_used_arr, $total_used);
        	// array_push($months_arr, $month_names);
        	array_push($months_arr, $month_years);

        	if(isset($county_id) && $county_id > 0){
    			switch ($by) {
		        	case 'county':
		        		// $col_url = base_url().'dashboardv2/county_dashboard/';
    					$column_url = $col_url.$c_id.'/NULL/NULL/'.$month_year;
    					// $column_url = $col_url.$c_id.'/NULL/NULL/NULL';
		        		break;
		        	case 'subcounty':
    					// $column_url = $col_url.'NULL/'.$d_id.'/NULL/'.$month_year;
    					$column_url = $col_url.'NULL/'.$d_id;
		        		break;

		        	case 'facility':
    					// $column_url = $col_url.'NULL/NULL/'.$f_code.'/'.$month_year;
    					$column_url = $col_url.'NULL/NULL/'.$f_code;
		        		break;
		        	default:
		        		break;
		        }

        	}elseif(isset($district_id) && $district_id > 0){
    			switch ($by) {
		        	case 'county':
		        		// $col_url = base_url().'dashboardv2/county_dashboard/';
    					$column_url = $col_url.$c_id.'/NULL/NULL/';
    					// $column_url = $col_url.$c_id.'/NULL/NULL/NULL';
		        		break;
		        	case 'subcounty':
    					$column_url = $col_url.'/NULL/'.$d_id.'/NULL/'.$month_year;
		        		break;

		        	case 'facility':
    					// $column_url = $col_url.'NULL/NULL/'.$f_code.'/'.$month_year;
    					$column_url = $col_url.'NULL/NULL/'.$f_code;
		        		break;
		        	default:
		        		break;
		        }

        	}else{
        		switch ($by) {
		        	case 'county':
		        		// $col_url = base_url().'dashboardv2/county_dashboard/';
    					$column_url = $col_url.$c_id;
		        		break;
		        	case 'subcounty':
    					$column_url = $col_url.'NULL/'.$d_id;
		        		break;

		        	case 'facility':
    					$column_url = $col_url.'NULL/NULL/'.$f_code;
		        		break;
		        	default:
		        		break;
		        }

        	}

        	// echo "<pre>";print_r($column_url);exit;
        	$series_data_1 .= '{
				"y":'.$total_used.',
				"url":"'.$column_url.'",
			},';

        }

        // echo "<pre>";print_r($series_data_1);exit;
        // echo "<pre>";print_r($total_used_arr);exit;
		$total_used_imploded = implode(',', $total_used_arr);
		$districts_imploded = implode(',', $district_arr);
		$facilities_imploded = '"'.implode('","', $facilities_arr).'"';
		$months_imploded = '"'.implode('","', $months_arr).'"';
		// $counties_imploded = implode(',', $county_arr);
		$counties_imploded = sprintf("'%s'", implode("','", $county_arr ) );
		// $districts_imploded = sprintf("'%s'", implode("','", $district_arr ) );

		$districts_imploded = '"'.implode('","', $district_arr).'"';

		// echo "<pre>";print_r($total_used_imploded);exit;
		// echo "<pre>";print_r($by);exit;
		switch ($by) {
        	case 'county':
        		if(isset($county_id) && $county_id > 0){
	        		$category_text = $months_imploded;
        			// echo "HERE";
	        	}else{
        			$category_text = $counties_imploded;
	        	}

        		break;

        	case 'subcounty':
        		// echo "SUBCOUNTY";
        		if(isset($district_id) && $district_id > 0){
	        		$category_text = $months_imploded;
	        	}else{
        			$category_text = $districts_imploded;
	        	}

        		break;

        	case 'facility':
        		// echo "SUBCOUNTY";
        		$category_text = $facilities_imploded;
        		break;
        	
        	default:
        		$category_text = $counties_imploded;
        		break;
        }
        // echo "<pre>";print_r($category_text);exit;
        $category_name = $commodity_name;
        $category_data_imploded = (isset($total_used_imploded) && $total_used_imploded > 0)?$total_used_imploded:0;

        $trend_details = json_encode($trend_details);        
        // $category_text = str_replace('"',"'",json_encode($category_text));        
        $percentages = str_replace('"',"'",json_encode($percentages));                
        $first_month = date("M Y", strtotime( date( 'Y-m-01' )." -12 months")); 
        $last_month = date("M Y", strtotime( date( 'Y-m-01' )." -1 months")); 

        // echo "<pre>";print_r($category_text);exit;
        // $percentages = $percentages;
        // $months_texts= $months_texts;
        // $trend_details = $trend_details; 

        $scripted = 1;
        // $scripted = 0;
        $trend_chart = '';
        if ($scripted == 1) {
            $trend_chart .= "<script>";
        }

        $trend_chart .= "
		Highcharts.chart('".$graph_div."', {
			    chart: {
			        type: 'column'
			    },
			    title: {
			        text: ''
			    },
			    xAxis: {
			        categories: [".$category_text."]
			    },
			    yAxis: {
			        min: 0,
			        title: {
			            text: 'Units'
			        },
			        stackLabels: {
			            enabled: false,
			            style: {
			                fontWeight: 'bold',
			                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
			            }
			        }
			    },
			     credits: {
				      enabled: false
				  },
			    legend: {
			        align: 'right',
			        x: -30,
			        verticalAlign: 'top',
			        y: 25,
			        floating: true,
			        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
			        borderColor: '#CCC',
			        borderWidth: 1,
			        shadow: false
			    },
			    tooltip: { crosshairs: [true,true] },
			    plotOptions: {
			        column: {
			            stacking: 'normal',
			            dataLabels: {
			                enabled: false,
			                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
			            }
			        },
			        series: {
		                cursor: 'pointer',
		                point: {
		                    events: {
		                        click: function () {
		                            window.open(this.options.url, '_blank');
		                            console.log(this.options);
		                        }
		                    }
		                }
		            }
			    },
			    series: [{
			        name: '".$category_name."',
			        data: [".$series_data_1."],
			        color: '".$category_color."'
			    }]
		});";

		if ($scripted == 1) {
            $trend_chart .= "</script>";
        }
            // return $trend_chart;
        if ($scripted == 0) {
            echo "<pre>";print_r($trend_chart);exit;
        }
		// return $trend_chart;
		// echo "<pre>";print_r($trend_chart);exit;
        echo $trend_chart;
	}

	public function download_excels($type=null){			
		if($type=='consumption'){			
			$this->consumption(null,null,null,null,'excel',null,null,null);
		}elseif ($type=='expiries') {
			$this->expiry(null,null,null,'excel',null,null,null);
		}elseif($type=='stock'){
		}
	}
	public function divisions($division = NULL) {//for the sake of beauty
		$_GET['division'] = $division;
		$default = "tracer";//FYI
		$map = $this->render_map();
		$commodity_count = Dashboard_model::get_commodity_count();
		$commodities = Dashboard_model::get_division_commodities($division);
		$division_details = Dashboard_model::get_division_details($division);
		// echo "<pre>";print_r($tracer_commodities);exit;
		if (isset($division) && $division>0) {
			$page_title = $division_details[0]['division_name'];
			if ($division==5) {
				$tracer = 1;
			}
			
		}else{
			$tracer = 1;
			$page_title = "Tracer Items";
		}
		$data['page_title'] = $page_title;
		// echo "<pre>";print_r($commodities);exit;
		$commodity_divisions = Dashboard_model::get_division_details();
		$counties = $this->db->query("SELECT * FROM counties")->result_array();
		$districts = $this->db->query("SELECT * FROM districts")->result_array();
		$facility_count = Dashboard_model::get_online_offline_facility_count();
		$counties_using_HCMP = Counties::get_counties_all_using_HCMP();
		// echo "<pre>";print_r($counties_using);exit;
		$data['tracer'] = $tracer;
		$data['commodity_division'] = isset($division)? $division :"NULL";
		$data['tracer_commodities'] = $commodities;
		$data['facility_count'] = $facility_count;
		$data['commodity_count'] = $commodity_count;
		$data['content_view'] = 'dashboard/dashboard';
		$data['county_data'] = $counties;
		$data['county_count'] = count($counties_using_HCMP);
		$data['district_data'] = $districts;
		$data['commodity_divisions'] = $commodity_divisions;
		$data['title'] = "National Dashboard";
		$data['maps'] = $map;
		$data['counties'] = $county_name;
		$this->load->view('dashboard/dashboard_template',$data); 
	}
	public function render_map()
	{
		$counties = $q = Doctrine_Manager::getInstance()
		->getCurrentConnection()
		->fetchAll("SELECT distinct c.id,c.kenya_map_id as county_fusion_map_id,c.county,count(c.county) as facility_no FROM facilities f 
			INNER JOIN districts d ON f.district=d.id
			INNER JOIN counties c ON d.county=c.id
		where cd4_enabled =1 group by c.county");// change  !!!!!!!!!!!!!
		// change  !!!!!!!!!!!!!
		$county_name = array();
		$map = array();
		$datas = array();
		$status = '';
		foreach ($counties as $county) {
			$countyMap = (int)$county['county_fusion_map_id'];
			$countyName = $county['county_name'];
			$facility_No = $county['facility_no'];
			array_push($county_name,array($county['id']=>$countyName));
			$datas[] = array('id' => $countyMap, 
				'value' => $countyName, 
				'value' => $facility_No,
				'color' => 'A8E3D7', 
				'tooltext' => $countyName,
				"baseFontColor" => "000000", 
				"link" => "Javascript:run('" .$county['id']. "^" .$countyName. "')");
		}
		$map = array( "showlabels"=>'0' , "baseFontColor" => "000000", "canvasBorderColor" => "ffffff", 
			"hoverColor" => "79A4AD", "fillcolor" => "F8F8FF", "numbersuffix" => " Facilities", 
			"includevalueinlabels" => "1", "labelsepchar" => ":", "baseFontSize" => "9",
			"borderColor" => "333333 ","showBevel" => "0", 'showShadow' => "0",'showTooltip'=>"1");
		$styles = array("showBorder" => 0 , 'animation'=>"_xScale");
		$finalMap = array('map' => $map, 'data' => $datas, 'styles' => $styles);
		return json_encode($finalMap);
	}
	public function search() {
		$data['title'] = "National Dashboard";
		$data['c_data'] = Commodities::get_all_2();
		$this -> load -> view("national/national_search_v", $data);
	}
	public function create_json() {
		$facility = $this -> db -> get('counties');
		$facility = $facility -> result_array();
		foreach ($facility as $fac) {
			$facArray[] = array('county' => $fac['county'], 'id' => $fac['id'], 'name' => 'county');
		}
		$data = json_encode($facArray);
		write_file('assets/scripts/typehead/json/counties.json', $data);
		$facility = $this -> db -> get('districts');
		$facility = $facility -> result_array();
		foreach ($facility as $fac) {
			$facArray1[] = array('districts' => $fac['district'], 'name' => 'district', 'county' => $fac['county'], 'id' => $fac['id']);
		}
		$data = json_encode($facArray1);
		write_file('assets/scripts/typehead/json/districts.json', $data);
		$facility = $this -> db -> get('facilities');
		$facility = $facility -> result_array();
		foreach ($facility as $fac) {
			$facArray2[] = array('facilities' => $fac['facility_name'], 'name' => 'facility', 'subcounty' => $fac['district'], 'id' => $fac['facility_code']);
		}
		$data = json_encode($facArray2);
		write_file('assets/scripts/typehead/json/facilities.json', $data);
	}
	public function facility_over_view($county_id = null, $district_id = null, $facility_code = null, $graph_type = null,$offline=null) {
		$district_id = ($district_id == "NULL") ? null : $district_id;
		$graph_type = ($graph_type == "NULL") ? null : $graph_type;
		$facility_code = ($facility_code == "NULL") ? null : $facility_code;
		$county_id = ($county_id == "NULL") ? null : $county_id;
		$offline = ($offline == "NULL") ? null : $offline;
		$and = ($district_id > 0) ? " AND d.id = '$district_id'" : null;
		$and .= ($facility_code > 0) ? " AND f.facility_code = '$facility_code'" : null;
		$and .= ($county_id > 0) ? " AND c.id='$county_id'" : null;
		$and .= ((isset($offline)&& $offline == 2))?" AND f.using_hcmp=2":NULL;
		$and .= ((isset($offline)&& $offline == 1))?" AND f.using_hcmp=1":NULL;
		$and .= ((isset($offline)&& $offline == 1))?" AND f.using_hcmp=1":NULL;
		$and .= ((isset($offline)&& $offline == 0))?" AND f.using_hcmp in (1,2)":NULL;
		$last_sync_column = ((isset($offline)&& $offline == 2))?",ftp.date_added AS last_sync":NULL;
		$join_data = ((isset($offline)&& $offline == 2))?",facilities f LEFT JOIN ftp_uploads ftp ON ftp.facility_code = f.facility_code":",facilities f";
		$title_main = ((isset($offline)&& $offline == 1)) ? " Online" : 'Offline';
		if($offline==0){
			$title_main = "OFFLINE AND ONLINE";	
		}
		

		$and = isset($and) ? $and : null;	
		// echo $and;exit;	
		if (isset($county_id)) :
			$county_name = counties::get_county_name($county_id);
		$name = $county_name['county'];
		$title = "$name County";
		elseif (isset($district_id)) :
			$district_data = (isset($district_id) && ($district_id > 0)) ? districts::get_district_name($district_id) -> toArray() : null;
		$district_name_ = (isset($district_data)) ? " :" . $district_data[0]['district'] . " Subcounty" : null;
		$title = isset($facility_code) && isset($district_id) ? "$district_name_ : $facility_name" : (isset($district_id) && !isset($facility_code) ? "$district_name_" : "$name County");
		elseif (isset($facility_code)) :
			$facility_code_ = isset($facility_code) ? facilities::get_facility_name_($facility_code) : null;
		$title = $facility_code_['facility_name'];
		else :
			$title = "National";
		endif;
		if ($graph_type != "excel") :
			$q = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("SELECT f.`using_hcmp`
				from facilities f, districts d, 
				counties c where f.district=d.id 
				and d.county=c.id 
				$and
				group by f.facility_name
				");
		echo count($q);
		else :
		$facility_data = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("
			SELECT 
			    c.county,
			    d.district AS subcounty,
			    f.facility_name,
			    f.facility_code,
			    f.`level`,
			    f.type,
			    f.date_of_activation $last_sync_column
			FROM
			    districts d,
			    counties c
			    $join_data
			WHERE
			    f.district = d.id 
			    AND d.county = c.id
				$and
			GROUP BY f.facility_name
			ORDER BY c.county , d.district , f.facility_name
			");	
		$row_data = array();
		if((isset($offline)&& $offline == 1)){
			$column_data = array("County", "Sub-County", "Facility Name", "Facility Code", "Facility Level","Type", "Date of Activation");
			foreach ($facility_data as $facility_data_item) :
			array_push($row_data, array(
				$facility_data_item["county"], 
				$facility_data_item["subcounty"], 
				$facility_data_item["facility_name"], 
				$facility_data_item["facility_code"], 
				$facility_data_item["level"], 
				$facility_data_item["type"],
				$facility_data_item["date_of_activation"])
			);
			endforeach;
		}else if((isset($offline)&& $offline == 2)){
			$column_data = array("County", "Sub-County", "Facility Name", "Facility Code", "Facility Level","Type", "Date of Activation","Date of Last Synchronization");
			foreach ($facility_data as $facility_data_item) :
				$last_sync = ((isset($facility_data_item["last_sync"]) && $facility_data_item["last_sync"] > 0))? $facility_data_item["last_sync"]:"No Data Available";
			array_push($row_data, array(
				$facility_data_item["county"], 
				$facility_data_item["subcounty"], 
				$facility_data_item["facility_name"], 
				$facility_data_item["facility_code"], 
				$facility_data_item["level"], 
				$facility_data_item["type"],
				$facility_data_item["date_of_activation"],
				$last_sync
				));
			endforeach;
		}else if((isset($offline)&& $offline == 0)){
			$column_data = array("County", "Sub-County", "Facility Name", "Facility Code", "Facility Level","Type", "Date of Activation");
			foreach ($facility_data as $facility_data_item) :
			array_push($row_data, array(
				$facility_data_item["county"], 
				$facility_data_item["subcounty"], 
				$facility_data_item["facility_name"], 
				$facility_data_item["facility_code"], 
				$facility_data_item["level"], 
				$facility_data_item["type"],
				$facility_data_item["date_of_activation"])
			);
			endforeach;
		}
		// echo "<pre>";print_r($row_data);exit;
		$excel_data = array('doc_creator' => "HCMP", 'doc_title' => "facilities rolled out $title", 'file_name' => "Facilities Rolled Out $title_main");
		$excel_data['column_data'] = $column_data;
		$excel_data['row_data'] = $row_data;
		// $this -> hcmp_functions -> create_excel($excel_data);
		$this -> hcmp_functions -> create_new_excel($excel_data);
		endif;
	}
	public function hcw($county_id = null, $district_id = null, $facility_code = null, $graph_type = null) {
		$district_id = ($district_id == "NULL") ? null : $district_id;
		$graph_type = ($graph_type == "NULL") ? null : $graph_type;
		$facility_code = ($facility_code == "NULL") ? null : $facility_code;
		$county_id = ($county_id == "NULL") ? null : $county_id;
		$and = ($district_id > 0) ? " AND d.id = '$district_id'" : null;
		$and .= ($facility_code > 0) ? " AND f.facility_code = '$facility_code'" : null;
		$and .= ($county_id > 0) ? " AND c.id='$county_id'" : null;
		$and = isset($and) ? $and : null;
		if (isset($county_id)) :
			$county_name = counties::get_county_name($county_id);
		$name = $county_name['county'];
		$title = "$name County";
		elseif (isset($district_id)) :
			$district_data = (isset($district_id) && ($district_id > 0)) ? districts::get_district_name($district_id) -> toArray() : null;
		$district_name_ = (isset($district_data)) ? " :" . $district_data[0]['district'] . " Subcounty" : null;
		$title = isset($facility_code) && isset($district_id) ? "$district_name_ : $facility_name" : (isset($district_id) && !isset($facility_code) ? "$district_name_" : "$name County");
		elseif (isset($facility_code)) :
			$facility_code_ = isset($facility_code) ? facilities::get_facility_name_($facility_code) : null;
		$title = $facility_code_['facility_name'];
		else :
			$title = "National";
		endif;
		if ($graph_type != "excel") :
			$q = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll(" SELECT  
				distinct u.`id`
				from facilities f, 
				districts d, counties c, user u 
				where f.district=d.id and 
				d.county=c.id and 
				f.facility_code=u.facility 
				$and 
				");
		echo count($q);
		else :
			$excel_data = array('doc_creator' => "HCMP", 'doc_title' => "hcw trained $title", 'file_name' => 'hcw trained');
		$row_data = array();
		$column_data = array("County", "Sub-County", "Total");
		$excel_data['column_data'] = $column_data;
		$facility_stock_data = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("SELECT  c.county, d.district as subcounty, count(u.id) as total from 
			facilities f, districts d, counties c, user u 
			where f.district=d.id and d.county=c.id 
			and f.facility_code=u.facility 
			and (u.usertype_id=2 or u.usertype_id=5 or u.usertype_id=3) 
			$and
			group by c.id,d.`id` 
			order by c.county asc,d.district asc
			");
		foreach ($facility_stock_data as $facility_stock_data_item) :
			array_push($row_data, array($facility_stock_data_item["county"], $facility_stock_data_item["subcounty"], $facility_stock_data_item["total"]));
		endforeach;
		$excel_data['row_data'] = $row_data;
		$this -> hcmp_functions -> create_excel($excel_data);
		endif;
	}
	public function order_approval($county_id = null) {
		$and = isset($county_id) ? "and c.id=$county_id" : null;
	}
	public function approval_delivery($county_id = null) {
		$and = isset($county_id) ? "and c.id=$county_id" : null;
	}
	public function get_facility_infor($county_id = null, $district_id = null, $facility_code = null, $graph_type = null) {
		$district_id = ($district_id == "NULL") ? null : $district_id;
		$graph_type = ($graph_type == "NULL") ? null : $graph_type;
		$facility_code = ($facility_code == "NULL") ? null : $facility_code;
		$county_id = ($county_id == "NULL") ? null : $county_id;
		$and = ($district_id > 0) ? " AND d.id = '$district_id'" : null;
		$and .= ($facility_code > 0) ? " AND f.facility_code = '$facility_code'" : null;
		$and .= ($county_id > 0) ? " AND c.id='$county_id'" : null;
		$and = isset($and) ? $and : null;
		$fbo = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll(" SELECT count(*) as total
			FROM  `facilities` f, districts d, counties c  
			WHERE  f.district=d.id and d.county=c.id   $and  and (f.`owner` LIKE  '%fbo%' or f.`owner` LIKE  '%faith%' 
			or f.`owner` LIKE  '%christian%' or f.`owner` LIKE  '%catholic%' or f.`owner` LIKE  '%muslim%' 
			or f.`owner` LIKE  '%episcopal%' or f.`owner` LIKE  '%cbo%') ");
		$private = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll(" SELECT count(*) as total 
			FROM  `facilities` f, districts d, counties c  
			WHERE  f.district=d.id and d.county=c.id   $and  and (f.`owner` LIKE  '%private%' or f.`owner` LIKE  '%non%' or  f.`owner` LIKE  '%ngo%'
			or  f.`owner` LIKE  '%company%' or f.`owner` LIKE  '%armed%') ");
		$public = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("SELECT count(*) as total
			FROM  `facilities` f, districts d, counties c
			WHERE  f.district=d.id and d.county=c.id   $and  and (f.`owner` LIKE  '%gok%' or f.`owner` LIKE  '%moh%' or f.`owner` LIKE  '%ministry%'
			or f.`owner` LIKE  '%community%' or f.`owner` LIKE  '%public%' or f.`owner` LIKE  '%local%' or f.`owner` LIKE  '%g.o.k%' ) ");
		$using_hcmp = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll(" SELECT count(f.id) as total, sum(f.`using_hcmp`) as using_hcmp
			from facilities f, districts d, 
			counties c where f.district=d.id 
			and d.county=c.id 
			$and
			");
		$other = $using_hcmp[0]['total'] - $public[0]['total'] - $private[0]['total'] - $fbo[0]['total'];
		echo "
		<table>
			<tr><td># of facilities </td>     <td>" . $using_hcmp[0]['total'] . "</td></tr>
			<tr><td># of public health facilities</td> <td>" . $public[0]['total'] . "</td></tr>
			<tr><td># of private facilities</td>       <td>" . $private[0]['total'] . "</td></tr>
			<tr><td># of faith based facilities</td>   <td>" . $fbo[0]['total'] . "</td></tr>
			<tr><td># of other facilities</td>   <td>" . $other . "</td></tr>
			<tr><td># of facilities using HCMP</td>    <td>" . $using_hcmp[0]['using_hcmp'] . "</td></tr>
		</table>
		";
	}
	public function expiry($year = NULL, $county_id = NULL, $district_id = NULL, $facility_code = NULL, $graph_type = NULL,$commodity_id=NULL,$commodity_division = NULL,$tracer = NULL) {//8 parameters
		// return $tracer;die;
		$year = ($year>0) ? $year : date('Y');
		$commodity_id = (isset($commodity_id) && $commodity_id > 0)? $commodity_id:$this->division_preferred_commodity_check($commodity_division);
		// echo $commodity_id;exit;
		// echo $year;exit;
		/*//Get the current month
		$datetime1 = new DateTime('Y-10');
		$datetime2 = new DateTime('Y-12');
		$interval = $datetime2->diff($datetime1);
		echo $interval->format('%R%a days');exit;
		$current_month = date("Y-m");
		$end = date('Y-12');
		$interval = $current_month->diff($end);
		echo $interval;exit;*/
		// echo $year;exit;
		//check if the district is set
		$district_id = ($district_id == "NULL") ? null : $district_id;
		// $option=($optionr=="NULL") ? null :$option;
		$facility_code = ($facility_code == "NULL") ? null : $facility_code;
		$county_id = ($county_id == "NULL") ? null : $county_id;
		// $months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
		$month_ = isset($month) ? $months[(int)$month - 1] : null;
		$category_data = array();
		$series_data = $series_data2 = $series_data_ = $series_data_2 = array();
		$temp_array = $temp_array2 = $temp_array_ = array();
		$graph_data = array();
		$title = '';
		if ($commodity_id > 0) {
			$commodity_details = Dashboard_model::get_commodity_details($commodity_id);
			// echo "<pre>";print_r($commodity_details);exit;
			$commodity_name = $commodity_details[0]['commodity_name'];
			$title .= $commodity_name;
		}else{
			$title .= ($tracer > 0)? "Tracer Commodity " :NULL;
		}
		if (isset($county_id)) :
			$county_name = Counties::get_county_name($county_id);
		$name = $county_name['county'];
		$title .= " $name County";
		elseif (isset($district_id)) :
			$district_data = (isset($district_id) && ($district_id > 0)) ? districts::get_district_name($district_id) -> toArray() : null;
		$district_name_ = (isset($district_data)) ? " :" . $district_data[0]['district'] . " Sub-County" : null;
		$title .= isset($facility_code) && isset($district_id) ? "$district_name_ : $facility_name" : (isset($district_id) && !isset($facility_code) ? "$district_name_" : " $name County");
		elseif (isset($facility_code)) :
			$facility_code_ = isset($facility_code) ? facilities::get_facility_name2($facility_code) : null;
			$title .= " ".$facility_code_['facility_name'];
		else :
			$title .= "";
		endif;
		$title = trim($title);
		$graph_title = $title." Expiries in ".$year;
		$months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
		$category_data = array_merge($category_data, $months);
		$and_data = ($district_id > 0) ? " AND d1.id = '$district_id'" : null;
		$and_data .= ($facility_code > 0) ? " AND f.facility_code = '$facility_code'" : null;
		$and_data .= ($county_id > 0) ? " AND d1.county='$county_id'" : null;
		$and_data = isset($and_data) ? $and_data : null;
		$and_data1 =($commodity_id>0) ?" AND d.id='$commodity_id'" : null;
		$nested_and_data = ($commodity_division>0)? " AND d.commodity_division = $commodity_division":null;
		$nested_and_data .= ($commodity_id>0)? " AND d.id = $commodity_id":null;
		$nested_and_data .= ($tracer>0)? " AND d.tracer_item = 1":null;
		$group_by = ($district_id > 0 && isset($county_id) && !isset($facility_code)) ? " ,d1.id" : null;
		$group_by .= ($facility_code > 0 && isset($district_id)) ? "  ,f.facility_code" : null;
		$group_by .= ($county_id > 0 && !isset($district_id)) ? " ,c.id" : null;
		$group_by = isset($group_by) ? $group_by : " ,c.id";
		// echo $and_data;exit;
		if ($graph_type != "excel") :
			// changed this: (select ROUND(SUM(f_s.current_balance / d.total_commodity_units) * d.unit_cost, 1) AS total,
			// to this: (select ROUND(SUM(f_s.current_balance / d.total_commodity_units), 1) AS total,
			
			$commodity_array = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("select DATE_FORMAT( temp.expiry_date,  '%b' ) AS cal_month,
				sum(temp.total) as total
				from
				districts d1,
				facilities f
				left join
				(select ROUND(SUM(f_s.current_balance / d.total_commodity_units), 1) AS total,
				f_s.facility_code,f_s.expiry_date
				from
				facility_stocks f_s, commodities d
				where
				f_s.expiry_date < NOW()
				and d.id = f_s.commodity_id
				$nested_and_data
				and year(f_s.expiry_date) = $year
				AND (f_s.status =1 or f_s.status =2 )
				GROUP BY d.id , f_s.facility_code having total > 1) 
				temp ON temp.facility_code = f.facility_code
				where
				f.district = d1.id
				$and_data
				and temp.total > 0
				group by month(temp.expiry_date)");
		$commodity_array2 = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("
			select 
			DATE_FORMAT( temp.expiry_date,  '%b' ) AS cal_month,
			sum(temp.total) as total
			from
			districts d1,
			facilities f
			left join
			(select 
			ROUND(SUM(f_s.current_balance / d.total_commodity_units), 1) AS total,
			f_s.facility_code,f_s.expiry_date
			from
			facility_stocks f_s, commodities d
			where
			f_s.expiry_date >= NOW()
			and d.id = f_s.commodity_id
			$nested_and_data
			AND f_s.status = (1 or 2)
			AND year(f_s.expiry_date) = $year
			GROUP BY d.id , f_s.facility_code
			having total > 1) temp ON temp.facility_code = f.facility_code
			where
			f.district = d1.id
			$and_data
			and temp.total > 0
			group by month(temp.expiry_date)
			");
		foreach ($commodity_array as $data) :
			$temp_array = array_merge($temp_array, array($data["cal_month"] => $data['total']));
		endforeach;
		foreach ($commodity_array2 as $data2) :
			$temp_array2 = array_merge($temp_array2, array($data2["cal_month"] => $data2['total']));
		//$series_data2 = array_merge($series_data2, array($data2["cal_month"] => (int)$data2['total']));
		//$category_data = array_merge($category_data, array($data2["cal_month"]));
		endforeach;
		 // echo "<pre>";print_r($temp_array2);echo "</pre>";exit;
		foreach ($months as $key => $data) :
		//for expiries
		$val = (array_key_exists($data, $temp_array)) ? (int)$temp_array[$data] : (int)0;
		$series_data = array_merge($series_data, array($val));
		array_push($series_data_, array($data, $val));
		//for potential expiries
		$val2 = (array_key_exists($data, $temp_array2)) ? (int)$temp_array2[$data] : (int)0;
		$series_data2 = array_merge($series_data2, array($val2));
		array_push($series_data_2, array($data, $val2));
		endforeach;
		$graph_type = 'column';
		$graph_data = array_merge($graph_data, array("graph_id" => 'dem_graph_'));
		$graph_data = array_merge($graph_data, array("graph_title" => "$graph_title"));
		// echo "<pre>";print_r($graph_data);exit;
		$graph_data = array_merge($graph_data, array("color" => "['#7CB5EC', '#434348']"));
		$graph_data = array_merge($graph_data, array("graph_type" => $graph_type));
		$graph_data = array_merge($graph_data, array("graph_yaxis_title" => "Packs"));
		$graph_data = array_merge($graph_data, array("graph_categories" => $category_data));
		$graph_data = array_merge($graph_data, array("series_data" => array()));
		//$default_expiries=array_merge($default_expiries,array("series_data"=>array()));
		$graph_data['series_data'] = array_merge($graph_data['series_data'], array("Potential Expiries" => $series_data2, "Actual Expiries" => $series_data));
		// echo "<pre>";print_r($graph_data['series_data']);echo "</pre>";exit;
		$data = array();
		$data['graph_id'] = 'dem_graph_';
		$data['high_graph'] = $this -> hcmp_functions -> create_high_chart_graph($graph_data);
		// print_r($data['high_graph']);exit;
		//exit;
		return $this -> load -> view("shared_files/report_templates/high_charts_template_v_national", $data);
		else :
			$excel_data = array('doc_creator' => "HCMP", 'doc_title' => "Expiry  $title", 'file_name' => "Stock Expired in $title  $year");
		$row_data = array();
		// $column_data = array("Commodity", "Unit Size", "Quantity (Packs)", "Quantity (Units)", "Unit Cost (Ksh)", "Total Cost Expired (Ksh)", "Date of Expiry", "Supplier", "Manufacturer", "Facility Name", "Facility Code", "Sub-County", "County");
		$column_data = array("Commodity", "Unit Size", "Quantity (Packs)", "Quantity (Units)","Date of Expiry", "Supplier", "Manufacturer", "Facility Name", "Facility Code", "Sub-County", "County");
		$excel_data['column_data'] = $column_data;
		//echo  ; exit;		
		// $facility_stock_data = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("select  c.county, d1.district as subcounty ,temp.drug_name,
		// 	f.facility_code, f.facility_name,temp.manufacture, sum(temp.total) as total_ksh,temp.units,
		// 	temp.unit_cost,temp.expiry_date,temp.unit_size,
		// 	temp.packs
		// 	from districts d1, counties c, facilities f left join
		// 	(SELECT 
		// 		ROUND(SUM(f_s.current_balance / d.total_commodity_units) * d.unit_cost, 1) AS total,
		//         ROUND(SUM(f_s.current_balance / d.total_commodity_units), 1) AS packs,
		//         ROUND(SUM(f_s.current_balance), 1) AS units,
		//         f_s.facility_code,
		// 		d.id,
		// 		d.commodity_name AS drug_name,
		// 		f_s.manufacture,
		// 		f_s.expiry_date,
		// 		d.unit_size,
		// 		d.unit_cost
		//     FROM
		//         facility_stocks f_s, commodities d
		// 	where f_s.expiry_date < NOW( ) 
		// 	and d.id=f_s.commodity_id
		// 	$and_data1
		// 	and year(f_s.expiry_date) !=1970
		// 	and year(f_s.expiry_date) = $year
		// 	AND (f_s.status =1 or f_s.status =2)
		// 	GROUP BY d.id,f_s.facility_code having total >1
		// 	) temp
		// 	on temp.facility_code = f.facility_code
		// 	where  f.district = d1.id
		// 	and c.id=d1.county
		// 	and temp.total>0
		// 	$and_data
		// 	group by temp.id,f.facility_code
		// 	order by temp.drug_name asc,temp.total asc, temp.expiry_date desc
		// 	");
		$facility_stock_data = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("SELECT c.county,d1.district AS subcounty,temp.drug_name,f.facility_code,
    		f.facility_name,temp.manufacture,SUM(temp.total) AS total_ksh,temp.units,temp.unit_cost,temp.expiry_date,temp.unit_size,temp.packs 
    		FROM  districts d1,counties c,facilities f LEFT JOIN
    			(SELECT 
    			ROUND(SUM(f_s.current_balance / d.total_commodity_units), 1) AS total,
		        ROUND(SUM(f_s.current_balance / d.total_commodity_units), 1) AS packs,
		        ROUND(SUM(f_s.current_balance), 1) AS units, f_s.facility_code,
				d.id,
				d.commodity_name AS drug_name,
				f_s.manufacture,
				f_s.expiry_date,
				d.unit_size,
				d.unit_cost
		    FROM
		        facility_stocks f_s, commodities d
			where f_s.expiry_date < NOW( ) 
			and d.id=f_s.commodity_id
			$and_data1
			and year(f_s.expiry_date) !=1970
			and year(f_s.expiry_date) = $year
			AND (f_s.status =1 or f_s.status =2)
			GROUP BY d.id,f_s.facility_code having total >1
			) temp
			on temp.facility_code = f.facility_code
			where  f.district = d1.id
			and c.id=d1.county
			and temp.total>0
			$and_data
			group by temp.id,f.facility_code
			order by temp.drug_name asc,temp.total asc, temp.expiry_date desc
			");
		array_push($row_data, array("The below commodities have expired $title  $year"));
		if (count($facility_stock_data)<=0) {
			array_push($row_data, array("There are not expired commmodities for the selected commodity"));	
		}else{
			foreach ($facility_stock_data as $facility_stock_data_item) :
				// array_push($row_data, array($facility_stock_data_item["drug_name"], $facility_stock_data_item["unit_size"], $facility_stock_data_item["packs"], $facility_stock_data_item["units"], $facility_stock_data_item["unit_cost"], $facility_stock_data_item["total_ksh"], $facility_stock_data_item["expiry_date"], "KEMSA", $facility_stock_data_item["manufacture"], $facility_stock_data_item["facility_name"], $facility_stock_data_item["facility_code"], $facility_stock_data_item["subcounty"], $facility_stock_data_item["county"]));
				array_push($row_data, array($facility_stock_data_item["drug_name"], $facility_stock_data_item["unit_size"], $facility_stock_data_item["packs"], $facility_stock_data_item["units"],$facility_stock_data_item["expiry_date"], "KEMSA", $facility_stock_data_item["manufacture"], $facility_stock_data_item["facility_name"], $facility_stock_data_item["facility_code"], $facility_stock_data_item["subcounty"], $facility_stock_data_item["county"]));
			endforeach;
		}
		$excel_data['row_data'] = $row_data;
		$this -> hcmp_functions -> create_excel($excel_data);
		endif;
	}
	public function generate_commodities_excel(){
		$commodities = Commodities::get_all_with_suppliers();
		$column_data = array("Commodity Code", "Commodity Name","Commodity Source","Unit Size");
		$row_data = array();
		foreach ($commodities as $key =>$facility_data_item) :			
			array_push($row_data, array(
				$facility_data_item["commodity_code"], 
				$facility_data_item["commodity_name"], 
				$facility_data_item["commodity_source"], 				
				$facility_data_item["unit_size"]));
		endforeach;		
		// echo "<pre>";print_r($row_data);exit;
		$excel_data = array('doc_creator' => "HCMP", 'doc_title' => "HCMP Commodities", 'file_name' => "HCMP Commodities");
		$excel_data['column_data'] = $column_data;
		$excel_data['row_data'] = $row_data;
		$this -> hcmp_functions -> create_new_excel($excel_data);		
	}
	
	public function potential($county_id=null, $district_id=null,$facility_code=null,$graph_type=null,$interval=null,$commodity_id=null)
	{
		$interval = ((isset($interval))&& ($interval > 0))? $interval:12;//default to select annual
		$and_data =($district_id>0) ?" AND d1.id = '$district_id'" : null;
		$and_data .=($facility_code>0) ?" AND f.facility_code = '$facility_code'" : null;
		$and_data .=($county_id>0) ?" AND d1.county='$county_id'" : null;
		$and_data1 =($commodity_id>0) ?" AND d.id='$commodity_id'" : null;
		$and_data =isset( $and_data) ?  $and_data:null;
		if( $graph_type!="excel"):
			$commodity_array = Doctrine_Manager::getInstance()
		->getCurrentConnection()
		->fetchAll("
			select
			DATE_FORMAT( temp.expiry_date,  '%b-%Y' ) AS cal_month,
			sum(temp.total) as total
			from
			districts d1,
			facilities f
			left join
			(select
			ROUND(SUM(f_s.current_balance / d.total_commodity_units) * d.unit_cost, 1) AS total,
			f_s.facility_code,f_s.expiry_date
			from
			facility_stocks f_s, commodities d
			where
			f_s.expiry_date between DATE_ADD(CURDATE(), INTERVAL 1 day) and  DATE_ADD(CURDATE(), INTERVAL $interval MONTH)
			and d.id = f_s.commodity_id
			AND f_s.status = (1 or 2)
			GROUP BY d.id , f_s.facility_code
			having total > 1) temp ON temp.facility_code = f.facility_code
			where
			f.district = d1.id
			$and_data
			and temp.total > 0
			group by month(temp.expiry_date)
			");
		foreach ($commodity_array as $data) :
			$series_data = array_merge($series_data, array($data["cal_month"] => (int)$data['total']));
		$category_data = array_merge($category_data, array($data["cal_month"]));
		endforeach;
		$graph_type='spline';
		$graph_data=array_merge($graph_data,array("graph_id"=>'dem_graph_1'));
		$graph_data = array_merge($graph_data, array("color" => "['#4b0082', '#6AF9C4']"));
		$graph_data=array_merge($graph_data,array("graph_title"=>"Stock Expiring $title in the Next $interval Months"));
		$graph_data=array_merge($graph_data,array("graph_type"=>$graph_type));
		$graph_data=array_merge($graph_data,array("graph_yaxis_title"=>"stock expiring in KSH"));
		$graph_data=array_merge($graph_data,array("graph_categories"=>$category_data ));
		$graph_data=array_merge($graph_data,array("series_data"=>array('total'=>$series_data)));
		$data = array();
		$data['high_graph']= $this->hcmp_functions->create_high_chart_graph($graph_data);
		$data['graph_id']='dem_graph_1';
		return $this -> load -> view("shared_files/report_templates/high_charts_template_v_national", $data);
		else:
			$excel_data = array('doc_creator' => "HCMP", 'doc_title' => "Potential Expiry  $title",
				'file_name' => "Stock Expiring $title in the Next $interval Months");
		$row_data = array();
		$column_data = array("Commodity","Date of Expiry","Unit Size","Quantity (Packs)","Quantity (Units)",
			"Unit Cost (Ksh)","Total Cost Expired (Ksh)"
			,"Supplier","Manufacturer","Facility Name","Facility Code","Sub-County","County");
		$excel_data['column_data'] = $column_data;
		// echo  $and_data; exit;
		$facility_stock_data = Doctrine_Manager::getInstance()
		->getCurrentConnection()
		->fetchAll("select  c.county, d1.district as subcounty ,temp.drug_name,
			f.facility_code, f.facility_name,temp.manufacture, sum(temp.total) as total_ksh,
			temp.unit_cost,temp.expiry_date,temp.unit_size,temp.units,
			temp.packs
			from districts d1, counties c, facilities f left join
			(
			select  ROUND( SUM(
			f_s.current_balance  / d.total_commodity_units ) * d.unit_cost, 1) AS total,
			ROUND( SUM( f_s.current_balance  / d.total_commodity_units  ), 1) as packs,
			SUM( f_s.current_balance) as units,
			f_s.facility_code,d.id,d.commodity_name as drug_name, f_s.manufacture,
			f_s.expiry_date,d.unit_size,d.unit_cost
			from facility_stocks f_s, commodities d
			where f_s.expiry_date between DATE_ADD(CURDATE(), INTERVAL 1 day) and  DATE_ADD(CURDATE(), INTERVAL $interval MONTH)
			and d.id=f_s.commodity_id
			$and_data1
			and year(f_s.expiry_date) !=1970
			AND (f_s.status =1 or f_s.status =2)
			GROUP BY d.id,f_s.facility_code having total >1
			) temp
			on temp.facility_code = f.facility_code
			where  f.district = d1.id
			and c.id=d1.county
			and temp.total>0
			$and_data
			group by temp.id,f.facility_code
			order by temp.drug_name asc,temp.total asc, temp.expiry_date desc
			");
		$date=date( "d M y");
		array_push($row_data, array("The below commodities will expire in the next $interval months from $date $title  "));
		if (count($facility_stock_data)<=0) {
			array_push($row_data, array("There are no expiries for the selected commodity"));	
		}else{
			foreach ($facility_stock_data as $facility_stock_data_item) :
				array_push($row_data, array($facility_stock_data_item["drug_name"],
					$facility_stock_data_item["expiry_date"],
					$facility_stock_data_item["unit_size"],
					$facility_stock_data_item["packs"],
					$facility_stock_data_item["units"],
					$facility_stock_data_item["unit_cost"],
					$facility_stock_data_item["total_ksh"],
					"KEMSA",
					$facility_stock_data_item["manufacture"],
					$facility_stock_data_item["facility_name"],
					$facility_stock_data_item["facility_code"],
					$facility_stock_data_item["subcounty"],
					$facility_stock_data_item["county"]
					));
			endforeach;
		}
		// echo "<pre>";print_r($facility_stock_data);exit;
		$excel_data['row_data'] = $row_data;
		$this->hcmp_functions->create_excel($excel_data);
		endif;
	}
	public function stock_level_mos($county_id = null, $district_id = null, $facility_code = null, $commodity_id = null, $graph_type = null) {
		$district_id = ($district_id == "NULL") ? null : $district_id;
		$graph_type = ($graph_type == "NULL") ? null : $graph_type;
		$facility_code = ($facility_code == "NULL") ? null : $facility_code;
		$county_id = ($county_id == "NULL") ? null : $county_id;
		$commodity_id = ($commodity_id == "ALL" || $commodity_id == "NULL") ? null : $commodity_id;
			// echo $commodity_id;die;
		$and_data = ($district_id > 0) ? " AND d.id = '$district_id'" : null;
		$and_data .= ($facility_code > 0) ? " AND f.facility_code = '$facility_code'" : null;
		$and_data .= ($county_id > 0) ? " AND ct.id='$county_id'" : null;
		// $and_data .= ($county_id > 0) ? " AND counties.id='$county_id'" : null;
		$and_data = isset($and_data) ? $and_data : null;
		$from_others = null;
		$from_others .= ($district_id > 0) ? "districts d," : null;
		$from_others .= ($county_id > 0) ? "counties ct," : null;
		if ($graph_type=='excel') {
			$and_data .= isset($commodity_id) ? "AND d.id =$commodity_id" : "AND d.tracer_item =1";			
			// $and_data .= isset($commodity_id) ? "AND commodities.id =$commodity_id" : "AND d.tracer_item =1";			
		}else{
			$and_data .= isset($commodity_id) ? "AND commodities.id =$commodity_id" : "AND commodities.tracer_item =1";
		}
		$group_by = ($district_id > 0 && isset($county_id) && !isset($facility_code)) ? " ,d.id" : null;
		$group_by .= ($facility_code > 0 && isset($district_id)) ? "  ,f.facility_code" : null;
		$group_by .= ($county_id > 0 && !isset($district_id)) ? " ,c_.id" : null;
		$group_by = isset($group_by) ? $group_by : " ,c_.id";
		$title = '';
		if (isset($county_id)) :
			$county_name = counties::get_county_name($county_id);
			$name = $county_name['county'];
			$title = "$name County";
		elseif (isset($district_id)) :
			$district_data = (isset($district_id) && ($district_id > 0)) ? districts::get_district_name($district_id) -> toArray() : null;
			$district_name_ = (isset($district_data)) ? " :" . $district_data[0]['district'] . " Subcounty" : null;
			$title = isset($facility_code) && isset($district_id) ? "$district_name_ : $facility_name" : (isset($district_id) && !isset($facility_code) ? "$district_name_" : "$name County");
		elseif (isset($facility_code)) :
			$facility_code_ = isset($facility_code) ? facilities::get_facility_name_($facility_code) : null;
			$title = $facility_code_['facility_name'];
		else :
			$title = "National";
		endif;
		if ($graph_type != "excel") :
			$getdates = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAll("SELECT MIN(created_at) as EarliestDate,MAX(created_at) as LatestDate
				FROM facility_issues");
				//echo '<pre>'; print_r($getdates);echo '<pre>'; exit;
			$early=$getdates[0]['EarliestDate'];
			$late=$getdates[0]['LatestDate'];
			$now = time(); 
			$my_date = strtotime($early);
				$datediff = ($now - $my_date)/(60*60*24);//in days
				$datediff= round($datediff,1);
				if (isset($county_id)) {
					$getdates = Doctrine_Manager::getInstance()->getCurrentConnection()
					->fetchAll("SELECT MIN(created_at) as EarliestDate,MAX(created_at) as LatestDate
						FROM facility_issues 
						inner join facilities on facility_issues.facility_code=facilities.facility_code
						inner join districts on facilities.district=districts.id
						inner join counties on districts.county=counties.id
						inner join commodities on facility_issues.commodity_id=commodities.id where counties.id=$county_id");
				//echo '<pre>'; print_r($getdates);echo '<pre>'; exit;
					$early=$getdates[0]['EarliestDate'];
					$late=$getdates[0]['LatestDate'];
					$now = time(); 
					$my_date = strtotime($early);
				$datediff = ($now - $my_date)/(60*60*24);//in days
				$datediff= round($datediff,1);
				$get_amc = Doctrine_Manager::getInstance()->getCurrentConnection()
				->fetchAll("SELECT commodities.id,commodities.commodity_name,avg(facility_issues.qty_issued) as total_units_consumed,
					(sum(facility_issues.qty_issued)*30/$datediff)/commodities.total_commodity_units as amc_packs,commodities.total_commodity_units FROM hcmp_rtk.facility_issues 
					inner join facilities on facility_issues.facility_code=facilities.facility_code
					inner join districts on facilities.district=districts.id
					inner join counties on districts.county=counties.id inner join commodities on facility_issues.commodity_id=commodities.id where s11_No IN('internal issue','(-ve Adj) Stock Deduction')
					$and_data group by commodities.id");
				
				$get_amc = Doctrine_Manager::getInstance()->getCurrentConnection()
				->fetchAll($new_sql_amc);
					//return $get_amc ;	
				$get_totals = Doctrine_Manager::getInstance()->getCurrentConnection()
				->fetchAll("SELECT commodities.id,commodities.commodity_name,sum(facility_stocks.current_balance) 
					as total_bal_units, sum(facility_stocks.current_balance)/commodities.total_commodity_units as cur_bal_packs,commodities.total_commodity_units FROM hcmp_rtk.facility_stocks 
					inner join facilities on facility_stocks.facility_code=facilities.facility_code
					inner join districts on facilities.district=districts.id
					inner join counties on districts.county=counties.id inner join commodities on facility_stocks.commodity_id=commodities.id 
					where commodities.status=1 $and_data group by commodities.id order by commodities.commodity_name");
			}else {
				$new_sql_amc = "SELECT commodities.id,commodities.commodity_name,CEIL(AVG(facility_issues.qty_issued)) AS total_units_consumed, 
								CEIL((SUM(facility_issues.qty_issued) / 3)) AS amc_units,
								CEIL((SUM(facility_issues.qty_issued) / 3) / commodities.total_commodity_units) AS amc_packs,commodities.total_commodity_units
								FROM   facility_issues $from_others INNER JOIN  commodities  ON facility_issues.commodity_id = commodities.id 
								WHERE s11_No IN ('internal issue' , '(-ve Adj) Stock Deduction') $and_data 
								AND facility_issues.expiry_date > '2016-04-21' 
								AND facility_issues.date_issued  between '2016-02-31' and '2016-04-31'
								GROUP BY commodities.id order by commodities.commodity_name asc";
				// echo $new_sql_amc;die;
				$get_amc = Doctrine_Manager::getInstance()->getCurrentConnection()
				->fetchAll($new_sql_amc);
				
				$new_sql_totals = "SELECT commodities.id,commodities.commodity_name,CEIL(sum(facility_stocks.current_balance))
				 	as total_bal_units, CEIL(sum(facility_stocks.current_balance)/commodities.total_commodity_units) as cur_bal_packs,commodities.total_commodity_units FROM facility_stocks $from_others inner join commodities on facility_stocks.commodity_id=commodities.id 
				 	where commodities.status=1 $and_data AND facility_stocks.expiry_date > '2016-04-21' group by commodities.id order by commodities.commodity_name asc";
				$get_totals = Doctrine_Manager::getInstance()->getCurrentConnection()
				->fetchAll($new_sql_totals);
			}
					//return $get_totals ;	
			$combine=array ();
			for ($i=0; $i < sizeof($get_totals) ; $i++) { 
						//array_push($combine,$get_totals[$i ],$get_amc[$i ]);
				$combine[]=array_merge($get_totals[$i ],$get_amc[$i ]);
			}
					// echo '<pre>'; print_r($combine);echo '<pre>'; exit;
			$category_data = array();
			$series_data = $series_data_ = array();
			$temp_array = $temp_array_ = array();
			$graph_data = array();
			$graph_type = '';
			foreach ($combine as $data) :
				$series_data = array_merge($series_data, array($data["commodity_name"] => (int)$data['cur_bal_packs']/(int)$data['amc_packs']));
			$category_data = array_merge($category_data, array($data["commodity_name"]));
			endforeach;
					// echo "<pre>";print_r($series_data);echo "</pre>";exit;
			$graph_type = 'bar';
			$graph_data = array_merge($graph_data, array("graph_id" => 'dem_graph_mos'));
			$graph_data = array_merge($graph_data, array("graph_title" => "$title Stock Level in Months of Stock (MOS)"));
			$graph_data = array_merge($graph_data, array("graph_type" => $graph_type));
			$graph_data = array_merge($graph_data, array("color" => "['#7CB5EC', '#434348']"));
			$graph_data = array_merge($graph_data, array("graph_yaxis_title" => "MOS"));
			$graph_data = array_merge($graph_data, array("graph_categories" => $category_data));
			// $graph_data = array_merge($graph_data, array("series_data" => array('total' => $series_data)));
			$graph_data=array_merge($graph_data,array("series_data"=>array("Unit Balance"=>array(),"Pack Balance"=>array())));
			$data = array();
			$data['high_graph'] = $this -> hcmp_functions -> create_high_chart_graph($graph_data);
			
			// echo "<pre>";print_r($data['high_graph']);exit;
			$data['graph_id'] = 'dem_graph_mos';
			return $this -> load -> view("shared_files/report_templates/high_charts_template_v_national", $data);
				//
		else :
			$excel_data = array('doc_creator' => "HCMP", 'doc_title' => "Stock Level in Months of Stock $title", 'file_name' => $title . ' MOS');
			$row_data = array();
			$column_data = array("County", "Sub-County", "Facility Name", "Facility Code", "Item Name", "MOS(packs)");
			$excel_data['column_data'] = $column_data;
					 //echo '' ; exit;
			$commodity_array = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("SELECT d.id,ct.county,sc.district,f.facility_code,
				f.facility_name,sum(fs.current_balance) as bal
				,sum(fs.current_balance)/d.total_commodity_units as packs,d.total_commodity_units,fs.batch_no,fs.expiry_date,d.commodity_name
				FROM hcmp_rtk.facility_stocks fs
				INNER JOIN facilities f ON  fs.facility_code=f.facility_code
				INNER JOIN commodities d ON  fs.commodity_id=d.id
				INNER JOIN districts sc ON  f.district=sc.id
				INNER JOIN counties ct ON  sc.county=ct.id
				$and_data AND fs.status=1 group by fs.batch_no order by ct.id asc
				
				");
						$r_data = array();
						$counter = 0;
				foreach ($commodity_array as $key) {
					$commodity=$key['id'];
					$f_code=$key['facility_code'];
					$batch_n=$key['batch_no'];
					$amc = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("
						SELECT sum(qty_issued) as amc FROM hcmp_rtk.facility_issues where commodity_id=$commodity and facility_code=$f_code AND batch_no='$batch_n' AND
						date_issued	> DATE_SUB(CURDATE(), INTERVAL 31 DAY)
						");
					foreach ($amc as $val ) {
						$amc= $val['amc'];
						if ($amc<0) {
							$amc=$amc*-1;
						}
						$amc_packs=round(($amc/$key['total_commodity_units']));
						if ($amc_packs<0) {
							$amc_packs=$amc_packs*-1;
						}
						if ($key['bal']<0 ) {
							$bal=$key['bal']*-1;
						}else{
							$bal=$key['bal'];
						}
						if ($key['packs']<0 ) {
							$packs=$key['packs']*-1;
						}else{
							$packs=$key['packs'];
						}
					}
					$r_data[$counter]["county"] = $key['county'];
					$r_data[$counter]["district"] = $key['district'];
					$r_data[$counter]["facility_code"] = $key['facility_code'];
					$r_data[$counter]["facility_name"] = $key['facility_name'];
					$r_data[$counter]["commodity_name"] = $key['commodity_name'];
					$r_data[$counter]["amc_packs"] = $amc_packs;
					$counter = $counter + 1;
				}
				array_push($row_data,$r_data);
				$excel_data['row_data'] = $r_data;
					// echo "<pre>";print_r($row_data);echo "</pre>";exit;
				$this -> hcmp_functions -> create_excel($excel_data);
		endif;
	}
	//for getting the stock level in units for the national dashboard
	public function stock_level_units($county_id = null, $district_id = null, $facility_code = null, $commodity_id = null, $graph_type = null) {
		$district_id = ($district_id == "NULL") ? null : $district_id;
		$graph_type = ($graph_type == "NULL") ? null : $graph_type;
		$facility_code = ($facility_code == "NULL") ? null : $facility_code;
		$county_id = ($county_id == "NULL") ? null : $county_id;
		$commodity_id = ($commodity_id == "ALL" || $commodity_id == "NULL") ? null : $commodity_id;
		$and_data = ($district_id > 0) ? " AND d1.id = '$district_id'" : null;
		$and_data .= ($facility_code > 0) ? " AND f.facility_code = '$facility_code'" : null;
		$and_data .= ($county_id > 0) ? " AND c.id='$county_id'" : null;
		$and_data .= isset($commodity_id) ? "AND d.id =$commodity_id" : "AND d.tracer_item =1";
		$and_data = isset($and_data) ? $and_data : null;
		$group_by = ($district_id > 0 && isset($county_id) && !isset($facility_code)) ? " ,d.id" : null;
		$group_by .= ($facility_code > 0 && isset($district_id)) ? "  ,f.facility_code" : null;
		$group_by .= ($county_id > 0 && !isset($district_id)) ? " ,c.id" : null;
		$group_by = isset($group_by) ? $group_by : " ,c.id";
		$title = '';
		if (isset($county_id)) :
			$county_name = counties::get_county_name($county_id);
		$name = $county_name['county'];
		$title = "$name County";
		elseif (isset($district_id) && ($district_id > 0)) :
			$district_data = districts::get_district_name($district_id);
		//echo $district_data;exit;
		$district_name_ = (isset($district_data)) ? " :" . $district_data[0]['district'] . " Subcounty" : null;
		$title = isset($facility_code) && isset($district_id) ? "$district_name_ : $facility_name" : (isset($district_id) && !isset($facility_code) ? "$district_name_" : "$name County");
		elseif (isset($facility_code)) :
			$facility_code_ = isset($facility_code) ? facilities::get_facility_name_($facility_code) : null;
		$title = $facility_code_['facility_name'];
		else :
			$title = "National";
		endif;
		if ($graph_type != "excel") :
			$commodity_array = Doctrine_Manager::getInstance() -> getCurrentConnection() -> 
		fetchAll("select 
			d.commodity_name as drug_name,
			f_s.current_balance as total
			from
			facilities f,
			districts d1,
			counties c,
			facility_stocks f_s,
			commodities d
			left join
			facility_monthly_stock f_m_s ON f_m_s.`commodity_id` = d.id
			where
			f_s.facility_code = f.facility_code
			and f.district = d1.id
			and d1.county = c.id
			and f_s.commodity_id = d.id
			and f_m_s.facility_code = f.facility_code
			$and_data
			group by d.id $group_by
			");
		$category_data = array();
		$series_data = $series_data_ = array();
		$temp_array = $temp_array_ = array();
		$graph_data = array();
		$graph_type = '';
		foreach ($commodity_array as $data) :
			$series_data = array_merge($series_data, array($data["drug_name"] => (int)$data['total']));
		$category_data = array_merge($category_data, array($data["drug_name"]));
		endforeach;
		$graph_type = 'bar';
		$graph_data = array_merge($graph_data, array("graph_id" => 'dem_graph_mos'));
		$graph_data = array_merge($graph_data, array("graph_title" => "$title Stock Level"));
		$graph_data = array_merge($graph_data, array("graph_type" => $graph_type));
		$graph_data = array_merge($graph_data, array("color" => "['#4b0082','#FFF263', '#6AF9C4']"));
		$graph_data = array_merge($graph_data, array("graph_yaxis_title" => "units"));
		$graph_data = array_merge($graph_data, array("graph_categories" => $category_data));
		$graph_data = array_merge($graph_data, array("series_data" => array('total' => $series_data)));
		$data = array();
		$data['high_graph'] = $this -> hcmp_functions -> create_high_chart_graph($graph_data);
		//
		$data['graph_id'] = 'dem_graph_mos';
		return $this -> load -> view("shared_files/report_templates/high_charts_template_v_national", $data);
		//
		else :
			$excel_data = array('doc_creator' => "HCMP", 'doc_title' => "Stock Level in units $title", 'file_name' => $title);
		$row_data = array();
		$column_data = array("County", "Sub-County", "Facility Name", "Facility Code", "Item Name", "Units");
		$excel_data['column_data'] = $column_data;
		$facility_stock_data = Doctrine_Manager::getInstance() -> getCurrentConnection() -> 
		fetchAll("select 
			c.county,d1.district as subcounty, 
			f.facility_name,
			f.facility_code, 
			d.commodity_name as drug_name,
			f_s.current_balance
			from
			facilities f,
			districts d1,
			counties c,
			facility_stocks f_s,
			commodities d
			where
			f_s.facility_code = f.facility_code
			and f.district = d1.id
			and d1.county = c.id
			and f_s.commodity_id = d.id
			$and_data
			group by d.id,f.facility_code
			order by c.county asc,d1.district asc
			");
		//echo "<pre>";print_r($facility_stock_data);exit;
		foreach ($facility_stock_data as $facility_stock_data_item) :
			array_push($row_data, array($facility_stock_data_item["county"], $facility_stock_data_item["subcounty"], $facility_stock_data_item["facility_name"], $facility_stock_data_item["facility_code"], $facility_stock_data_item["drug_name"], $facility_stock_data_item["current_balance"]));
		endforeach;
		$excel_data['row_data'] = $row_data;
		$this -> hcmp_functions -> create_excel($excel_data);
		endif;
	}
	public function consumption($county_id = NULL, $district_id = NULL, $facility_code = NULL, $commodity_id = NULL, $graph_type = NULL, $from = NULL, $to = NULL,$division = NULL) {
		// http://localhost/HCMP/dashboard/consumption/NULL/NULL/NULL/12/excel/NULL

		$title = '';
		$district_id = ($district_id == "NULL") ? null : $district_id;
		$graph_type = ($graph_type == "NULL") ? null : $graph_type;
		$facility_code = ($facility_code == "NULL") ? null : $facility_code;
		$county_id = ($county_id == "NULL") ? null : $county_id;
		$commodity_id = ($commodity_id > 0) ? $commodity_id : NULL;//by default set to ORS Co Pack
		$commodity_id = (isset($commodity_id) && $commodity_id > 0)? $commodity_id:$this->division_preferred_commodity_check($division);
		// echo $commodity_id;exit;
		$division = ($division == "NULL") ? null : $division;
		$year = date('Y');
		$commodity_array = explode(',', $commodity_id);
		$count_commodities = count($commodity_array);
		$year = ($year == "NULL" || !isset($year)) ? date('Y') : $year;
		$to = ($to == "NULL" || !isset($to)) ? date('Y-m-d') : date('Y-m-d', strtotime(urldecode($to)));
		$from = ($from == "NULL" || !isset($from)) ? date('Y-01-01') : date('Y-m-d', strtotime(urldecode($from)));
		$category_data = array();
		$series_data = $series_data_ = array();
		$temp_array = $temp_array_ = array();
		$graph_data = array();
		// $graph_type = '';
		$and_data = ($district_id > 0) ? " AND d1.id = '$district_id'" : null;
		$and_data .= ($facility_code > 0) ? " AND f.facility_code = '$facility_code'" : null;
		$and_data .= ($county_id > 0) ? " AND c.id='$county_id'" : null;
		$and_data = isset($and_data) ? $and_data : null;
		if($count_commodities>1){
		}else{
			$and_data .= ($commodity_id > 0) ? "AND d.id =$commodity_id" : "AND d.tracer_item =1";
		}
		// echo $graph_type;exit;
		/*$group_by =($district_id>0 && isset($county_id) && !isset($facility_code)) ?" ,d.id" : null;
		$group_by .=($facility_code>0 && isset($district_id)) ?"  ,f.facility_code" : null;
		$group_by .=($county_id>0 && !isset($district_id)) ?" ,c_.id" : null;
		$group_by =isset( $group_by) ?  $group_by: " ,c_.id";*/
		$time = "Between " . date('j M y', strtotime(urldecode($from))) . " and " . date('j M y', strtotime(urldecode($to)));
		if ($commodity_id > 0) {
			$commodity_details = Dashboard_model::get_commodity_details($commodity_id);
			// echo "<pre>";print_r($commodity_details);exit;
			$commodity_name = $commodity_details[0]['commodity_name'];
			$title .= $commodity_name;
		}else{
			// $title .= ($tracer > 0)? "Tracer Commodity " :NULL;
		}
		// echo htmlspecialchars($title,ENT_QUOTES);exit;
		if (isset($county_id)) :
			$county_name = counties::get_county_name($county_id);
			$name = $county_name['county'];
			$title .= " $name County";
			//print_r($name);exit;
		elseif (isset($district_id)) :
			$district_data = (isset($district_id) && ($district_id > 0)) ? districts::get_district_name($district_id) -> toArray() : null;
			$district_name_ = (isset($district_data)) ? " :" . $district_data[0]['district'] . " Sub-County" : null;
			$title .= isset($facility_code) && isset($district_id) ? "$district_name_ : $facility_name" : (isset($district_id) && !isset($facility_code) ? "$district_name_" : " $name County");
		elseif (isset($facility_code)) :
				$facility_code_ = isset($facility_code) ? facilities::get_facility_name2($facility_code) : null;
				$title .=" ".$facility_code_['facility_name'];
		else :
				// echo "I work here";exit;
			$title .= "";
		endif;//county id isset
		if ($graph_type != "excel") ://if you need change it,refer to national controller,function naming are similar
			// echo "This ".$graph_type;exit;
			$filter = ($commodity_id > 0)? "AND d.id = $commodity_id" : NULL;//default to ORS Co pack
			$commodity_array = $this->db->query("
				SELECT 
					d.id,
				    d.commodity_name AS drug_name, 
				    ROUND(( IFNULL(SUM(f_i.qty_issued),0) / IFNULL(d.total_commodity_units, 0) ),0) as total,
    				DATE_FORMAT(f_i.created_at, '%b') AS cal_month
				FROM
				    facilities f,
				    districts d1,
				    counties c,
				    commodities d
				        LEFT JOIN
				    facility_issues f_i ON f_i.commodity_id = d.id
				WHERE
				    f_i.facility_code = f.facility_code
				        AND f.district = d1.id
				        AND d1.county = c.id
				        AND f_i.`qty_issued` > 0
				        AND YEAR(f_i.created_at) = $year
				        $and_data
				        GROUP BY MONTH(f_i.created_at)
				        ")->result_array();
			// echo "<pre>";print_r($commodity_array);exit;
			$months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
			$category_data = array_merge($category_data, $months);
			$temp_array = array();
			foreach ($commodity_array as $data) :
				// echo "<pre>";print_r($data);
			$temp_array = array_merge($temp_array, array($data["cal_month"] => $data['total']));
			endforeach;
			// echo "<pre>";print_r($temp_array);exit;
			foreach ($months as $key => $data) :
				$val = (array_key_exists($data, $temp_array)) ? (int)$temp_array[$data] : (int)0;
				$series_data = array_merge($series_data, array($val));
				// array_push($series_data_, array($data, $val));
			endforeach;
		$title = trim($title);
		$graph_type = 'spline';
		$graph_data = array_merge($graph_data, array("graph_id" => 'dem_graph_consuption'));
		$graph_data = array_merge($graph_data, array("graph_title" =>" ".$title." Consumption (Packs) for $year"));
		$graph_data = array_merge($graph_data, array("graph_type" => $graph_type));
		$graph_data = array_merge($graph_data, array("color" => "['#7CB5EC', '#434348']"));
		$graph_data = array_merge($graph_data, array("graph_yaxis_title" => "Packs"));
		$graph_data = array_merge($graph_data, array("graph_categories" => $category_data));
		$graph_data = array_merge($graph_data, array("series_data" => array('total' => $series_data)));
		$data = array();
		// echo "<pre>";print_r($graph_data['graph_title']);exit;
		$data['high_graph'] = $this -> hcmp_functions -> create_high_chart_graph($graph_data);
		$data['graph_id'] = 'dem_graph_consuption';
		return $this -> load -> view("shared_files/report_templates/high_charts_template_v_national", $data);
			// echo "Graph data";exit;
		else :
			// echo "Excel data";exit;
			$excel_data = array('doc_creator' => "HCMP", 'doc_title' => " $title Consumption (Packs) $time", 'file_name' => $title . ' Consumption');
			$row_data = array();			
			$column_data = array("County", "Sub-County", "Facility Name", "Facility Code", "Item Name", "Consumption (Packs)");
		for ($i=1; $i < $count_commodities; $i++) { 
			$item_name = "Item Name";
			$consumption = "Consumption (Packs)";
			array_push($column_data, $item_name);
			array_push($column_data, $consumption);
		}
		// echo "<pre>";
		// print_r($column_data);die;
		$excel_data['column_data'] = $column_data;
		// echo ; exit;
		if($count_commodities>1){
			$sql = "SELECT c.county,d1.district AS subcounty,f.facility_name,f.facility_code FROM
			facilities f,districts d1,counties c WHERE f.district = d1.id AND d1.county = c.id $and_data					    
			ORDER BY c.county ASC , d1.district ASC";
			// echo "$sql";die;
			$facility_data = $this->db->query($sql)->result_array();
			// array_push($final_array[0], "The below commodities were consumed $time");
			$final_array[] = array("The below commodities were consumed $time",null,null,null);					
			foreach ($facility_data as $key => $value) {
				$county = $value['county'];
				$subcounty = $value['subcounty'];
				$facility_name = $value['facility_name'];
				$facility_code = $value['facility_code'];
				$final_array[$facility_code] = array($county,$subcounty,$facility_name,$facility_code);					
				for ($i=0; $i <$count_commodities ; $i++) { 
					$commodity_id =  $commodity_array[$i];
					$commodity_details = Commodities::get_commodity_name($commodity_id);
					$drug_name = $commodity_details[0]['commodity_name'];
					array_push($final_array[$facility_code],$drug_name);
					$sql_commodity_details = "SELECT ROUND(AVG(IFNULL(ABS(f_i.`qty_issued`), 0) / IFNULL(d.total_commodity_units, 0)),1) AS total
					FROM commodities d LEFT JOIN  facility_issues f_i ON f_i.`commodity_id` = d.id
					WHERE f_i.facility_code = '$facility_code'
					AND f_i.`qty_issued` > 0
					and f_i.created_at between '$from' and '$to'
					AND d.id = '$commodity_id'
					GROUP BY d.id , f_i.facility_code";
					echo $sql_commodity_details;die;
					$consuption_details = $this->db->query($sql_commodity_details)->result_array();
					if(count($consuption_details)==0){
						$total = 'No Data Available';		 				
						array_push($final_array[$facility_code],$total);
					}else{
						foreach ($consuption_details as $keys => $values) {												
							$total = $values['total'];	
							$total = ($total=='') ? 'No Data Available' : $total ;																	
							array_push($final_array[$facility_code],$total);
						}
					}
				}
			}
			$row_data = $final_array;
		}else{
			/*echo "select 
				c.county,d1.district as subcounty, f.facility_name,f.facility_code, d.commodity_name as drug_name,
				round(avg(IFNULL(ABS(f_i.`qty_issued`), 0) / IFNULL(d.total_commodity_units, 0)),
				1) as total
				from
				facilities f,
				districts d1,
				counties c,
				commodities d
				left join facility_issues f_i on f_i.`commodity_id`=d.id 
				where f_i.facility_code = f.facility_code 
				and f.district=d1.id 
				and d1.county=c.id 
				and f_i.`qty_issued`>0
				and f_i.created_at between '$from' and '$to'
				$and_data
				group by d.id , f.facility_code
				order by c.county asc , d1.district asc";exit;*/
			array_push($row_data, array("The below commodities were consumed $time"));
			$commodity_id = $commodity_array[0];
			$facility_stock_data = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("
				select 
				c.county,d1.district as subcounty, f.facility_name,f.facility_code, d.commodity_name as drug_name,
				round((IFNULL(SUM(f_i.`qty_issued`), 0) / IFNULL(d.total_commodity_units, 0)),
				1) as total
				from
				facilities f,
				districts d1,
				counties c,
				commodities d
				left join facility_issues f_i on f_i.`commodity_id`=d.id 
				where f_i.facility_code = f.facility_code 
				and f.district=d1.id 
				and d1.county=c.id 
				and f_i.`qty_issued`>0
				and f_i.created_at between '$from' and '$to'
				$and_data
				group by d.id , f.facility_code
				order by c.county asc , d1.district asc
				");
			foreach ($facility_stock_data as $facility_stock_data_item) :
				array_push($row_data, array($facility_stock_data_item["county"], $facility_stock_data_item["subcounty"], $facility_stock_data_item["facility_name"], $facility_stock_data_item["facility_code"], $facility_stock_data_item["drug_name"], $facility_stock_data_item["total"]));
			endforeach;
		}
		// echo "<pre>";print_r($row_data);exit;
		$excel_data['row_data'] = $row_data;
		$this -> hcmp_functions -> create_excel($excel_data);
		endif;
	}
	public function division_preferred_commodity_check($division = NULL)
	{
		if (isset($division) && $division > 0) {
			$commodity = $this->db->query("
				SELECT 
				    *
				FROM
				    commodity_division_details WHERE id = $division")->result_array();
			// echo "<pre>";print_r($commodity);exit;
			return $commodity[0]['preferred_commodity'];
		}else{
			$division = 5;//tracer commodities by default
			$commodity = $this->db->query("
				SELECT 
				    *
				FROM
				    commodity_division_details WHERE id = $division")->result_array();
			// echo "<pre>";print_r($commodity);exit;
			return $commodity[0]['preferred_commodity'];
		}
	}
	public function old_order($year = null, $county_id = null, $district_id = null, $facility_code = null, $graph_type = null) {
		$district_id = ($district_id == "NULL") ? null : $district_id;
		$graph_type = ($graph_type == "NULL") ? null : $graph_type;
		$facility_code = ($facility_code == "NULL") ? null : $facility_code;
		$county_id = ($county_id == "NULL") ? null : $county_id;
		$year = ($year == "NULL") ? date('Y') : $year;
		$array_months = array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
		// echo "<pre>";
		// print_r($array_months);die;
		$and_data = ($district_id > 0) ? " AND d.id = '$district_id'" : null;
		$and_data .= ($facility_code > 0) ? " AND f.facility_code = '$facility_code'" : null;
		$and_data .= ($county_id > 0) ? " AND c.id='$county_id'" : null;
		$and_data .= ($year > 0) ? " and year(o.`order_date`) =$year" : null;
		$and_data = isset($year) ? $and_data : null;
		//echo  ; exit;
		$commodity_array = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("SELECT 
			sum(o.`order_total`) as total,DATE_FORMAT( o.`order_date`,  '%b' ) AS cal_month
			FROM
			facilities f, districts d, counties c,`facility_orders` o
			WHERE
			o.facility_code=f.facility_code
			and f.district=d.id and d.county=c.id
			$and_data
			group by month(o.`order_date`)
			");
		$commodity_array_2 = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("SELECT 
			sum(o.`order_total`) as total,DATE_FORMAT( o.`order_date`,  '%b' ) AS cal_month
			FROM
			facilities f, districts d, counties c,`facility_orders` o
			WHERE
			o.facility_code=f.facility_code
			and f.district=d.id and d.county=c.id and o.status = 4
			$and_data
			group by month(o.`order_date`)
			");
		// var_dump($commodity_array_2);
		// exit;
		$category_data = array();
		$series_data = $series_data_ = array();
		$temp_array = $temp_array_ = array();
		$graph_data = array();
		$title = '';
		if ($graph_type != "excel") :
			if (isset($county_id)) :
				$county_name = counties::get_county_name($county_id);
			$name = $county_name['county'];
			$title = "$name county";
			elseif (isset($district_id)) :
				$district_data = (isset($district_id) && ($district_id > 0)) ? districts::get_district_name($district_id) -> toArray() : null;
			$district_name_ = (isset($district_data)) ? " :" . $district_data[0]['district'] . " Subcounty" : null;
			$title = isset($facility_code) && isset($district_id) ? "$district_name_ : $facility_name" : (isset($district_id) && !isset($facility_code) ? "$district_name_" : "$name County");
			elseif (isset($facility_code)) :
				$facility_code_ = isset($facility_code) ? facilities::get_facility_name_($facility_code) : null;
			$title = $facility_code_['facility_name'];
			else :
				$title = "National";
			endif;
			foreach ($commodity_array as $data) :
				$series_data = array_merge($series_data, array($data["cal_month"] => (int)$data['total']));
			$category_data = array_merge($category_data, array($data["cal_month"]));
			endforeach;
			$series_data2 = $series_data_2 = $category_data_2 = array();
			foreach ($commodity_array_2 as $data_2) :
				$series_data_2 = array_merge($series_data_2, array($data_2["cal_month"] => (int)$data_2['total']));
			$category_data_2 = array_merge($category_data_2, array($data_2["cal_month"]));
			endforeach;
			//$graph_details = array('' => , );;
			// array_merge($series_data,$series_data_2);
			// echo "<pre>";print_r($series_data_2);echo "</pre>";exit;
					$graph_type = 'column';
					$graph_data = array_merge($graph_data, array("graph_id" => 'dem_graph_order'));
					$graph_data = array_merge($graph_data, array("graph_title" => "$year $title Order Cost"));
					$graph_data = array_merge($graph_data, array("graph_type" => $graph_type));
					$graph_data = array_merge($graph_data, array("color" => "['#92A8CD', '#A47D7C']"));
					$graph_data = array_merge($graph_data, array("graph_yaxis_title" => "Cost in KSH"));
					$graph_data = array_merge($graph_data, array("graph_categories" => $category_data));
					$graph_data = array_merge($graph_data, array("series_data" => array('Cost of Orders Made' => $series_data, 'Cost of Orders delivered' => $series_data_2)));
					$data = array();
			//seth
					$data['high_graph'] = $this -> hcmp_functions -> create_high_chart_graph($graph_data);
			// echo $data['high_graph'];exit;
					$data['graph_id'] = 'dem_graph_order';
					return $this -> load -> view("shared_files/report_templates/high_charts_template_v_national", $data);
					else :
						$excel_data = array('doc_creator' => "HCMP", 'doc_title' => "$year $title Order Cost", 'file_name' => "$year $title Order Cost (KSH)");
					$row_data = array();
					$column_data = array("Date of Order Placement", "Date of Order Approval", "Total Order Cost (Ksh)", "Date of Delivery", "Lead Time (Order Placement to Delivery)", "Supplier", "Facility Name", "Facility Code", "Sub-County", "County");
					$excel_data['column_data'] = $column_data;
			//echo  ; exit;
					$facility_stock_data = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("SELECT c.county,d.district as sub_county, f.facility_name, f.facility_code, 
						DATE_FORMAT(`order_date`,'%d %b %y') as order_date, 
						DATE_FORMAT(`approval_date`,'%d %b %y')  as approval_date,
						DATE_FORMAT(`deliver_date`,'%d %b %y')  as delivery_date, 
						DATEDIFF(`approval_date`,`order_date`) as tat_order_approval,
						DATEDIFF(`deliver_date`,`approval_date`) as tat_approval_deliver,
						DATEDIFF(`deliver_date`,`order_date`) as tat_order_delivery
						, sum(o.`order_total`) as total 
						from facility_orders o, facilities f, districts d, counties c 
						where f.facility_code=o.facility_code and f.district=d.id 
						and c.id=d.county $and_data
						group by o.id order by c.county asc ,d.district asc , 
						f.facility_name asc 
						");
					array_push($row_data, array("The orders below were placed $year $title"));
					foreach ($facility_stock_data as $facility_stock_data_item) :
						array_push($row_data, array($facility_stock_data_item["order_date"], $facility_stock_data_item["approval_date"], $facility_stock_data_item["total"], $facility_stock_data_item["delivery_date"], $facility_stock_data_item["tat_order_delivery"], "KEMSA", $facility_stock_data_item["facility_name"], $facility_stock_data_item["facility_code"], $facility_stock_data_item["sub_county"], $facility_stock_data_item["county"]));
					endforeach;
					$excel_data['row_data'] = $row_data;
					$this -> hcmp_functions -> create_excel($excel_data);
					endif;
	}
	public function order($year = null, $county_id = null, $district_id = null, $facility_code = null, $graph_type = null) {
		$district_id = ($district_id == "NULL") ? null : $district_id;
		$graph_type = ($graph_type == "NULL") ? null : $graph_type;
		$facility_code = ($facility_code == "NULL") ? null : $facility_code;
		$county_id = ($county_id == "NULL") ? null : $county_id;
		$year = ($year == "NULL") ? date('Y') : $year;			
		$array_months_texts = array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
		$array_months = array(1,2,3,4,5,6,7,8,9,10,11,12);
		$and_data = ($district_id > 0) ? " AND d.id = '$district_id'" : null;
		$and_data .= ($facility_code > 0) ? " AND f.facility_code = '$facility_code'" : null;
		$and_data .= ($county_id > 0) ? " AND c.id='$county_id'" : null;
		$and_data .= ($year > 0) ? " and year(o.`order_date`) =$year" : null;
		$new_commodity_array = array();
		foreach ($array_months as $key => $value) {
			$month_name = $array_months_texts[$key];								
			$month_and = ($year > 0) ? " and MONTH(o.`order_date`) ='$value'" : null;				
			$and_data = isset($year) ? $and_data : null;	
			$commodity_array = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("SELECT 
				sum(o.`order_total`) as total,DATE_FORMAT( o.`order_date`,  '%b' ) AS cal_month
				FROM
				facilities f, districts d, counties c,`facility_orders` o
				WHERE
				o.facility_code=f.facility_code
				and f.district=d.id and d.county=c.id
				$and_data $month_and
				group by month(o.`order_date`)");	
			$commodity_array_2 = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("SELECT 
				sum(o.`order_total`) as total,DATE_FORMAT( o.`order_date`,  '%b' ) AS cal_month
				FROM
				facilities f, districts d, counties c,`facility_orders` o
				WHERE
				o.facility_code=f.facility_code
				and f.district=d.id and d.county=c.id and o.status = 4
				$and_data $month_and
				group by month(o.`order_date`)
				");
			$count_array1 = count($commodity_array);			
			if($count_array1>0){
				foreach ($commodity_array as $key => $value) {
					$new_commodity_array[] = array('cal_month'=>$month_name,'total'=>$value['total']);
				}
			}else{
				$new_commodity_array[] = array('cal_month'=>$month_name,'total'=>0);
			}
			$count_array2 = count($commodity_array_2);			
			if($count_array2>0){
				foreach ($commodity_array_2 as $key => $value) {
					$new_commodity_array_2[] = array('cal_month'=>$month_name,'total'=>$value['total']);
				}
			}else{
				$new_commodity_array_2[] = array('cal_month'=>$month_name,'total'=>0);
			}
		}
		// $commodity_array = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("SELECT 
		// 	sum(o.`order_total`) as total,DATE_FORMAT( o.`order_date`,  '%b' ) AS cal_month
		// 	FROM
		// 	facilities f, districts d, counties c,`facility_orders` o
		// 	WHERE
		// 	o.facility_code=f.facility_code
		// 	and f.district=d.id and d.county=c.id
		// 	$and_data
		// 	group by month(o.`order_date`)
		// 	");
		// $commodity_array_2 = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("SELECT 
		// 	sum(o.`order_total`) as total,DATE_FORMAT( o.`order_date`,  '%b' ) AS cal_month
		// 	FROM
		// 	facilities f, districts d, counties c,`facility_orders` o
		// 	WHERE
		// 	o.facility_code=f.facility_code
		// 	and f.district=d.id and d.county=c.id and o.status = 4
		// 	$and_data
		// 	group by month(o.`order_date`)
		// 	");
				$category_data = array();
				$series_data = $series_data_ = array();
				$temp_array = $temp_array_ = array();
				$graph_data = array();
				$title = '';
				if ($graph_type != "excel") :
					if (isset($county_id)) :
						$county_name = counties::get_county_name($county_id);
					$name = $county_name['county'];
					$title = "$name county";
					elseif (isset($district_id)) :
						$district_data = (isset($district_id) && ($district_id > 0)) ? districts::get_district_name($district_id) -> toArray() : null;
					$district_name_ = (isset($district_data)) ? " :" . $district_data[0]['district'] . " Subcounty" : null;
					$title = isset($facility_code) && isset($district_id) ? "$district_name_ : $facility_name" : (isset($district_id) && !isset($facility_code) ? "$district_name_" : "$name County");
					elseif (isset($facility_code)) :
						$facility_code_ = isset($facility_code) ? facilities::get_facility_name_($facility_code) : null;
					$title = $facility_code_['facility_name'];
					else :
						$title = "National";
					endif;
					foreach ($new_commodity_array as $keyz=>$data) :
						$cal_month = $data["cal_month"];
					$total = $data["total"];					
					$series_data = array_merge($series_data, array($cal_month => (int)$total));
					$category_data = array_merge($category_data, array($data["cal_month"]));
					endforeach;
		// die;
		// foreach ($commodity_array as $data) :
		// 	$series_data = array_merge($series_data, array($data["cal_month"] => (int)$data['total']));
		// $category_data = array_merge($category_data, array($data["cal_month"]));
		// endforeach;
		$series_data2 = $series_data_2 = $category_data_2 = array();
		foreach ($new_commodity_array_2 as $keyz=>$data) :
			$cal_month = $data["cal_month"];
		$total = $data["total"];					
		$series_data_2 = array_merge($series_data_2, array($cal_month => (int)$total));
		$category_data_2 = array_merge($category_data_2, array($data["cal_month"]));
		endforeach;
		// foreach ($commodity_array_2 as $data_2) :
		// 	// echo "<pre>";
		// 	// print_r($data_2);die;
		// 	$series_data_2 = array_merge($series_data_2, array($data_2["cal_month"] => (int)$data_2['total']));
		// $category_data_2 = array_merge($category_data_2, array($data_2["cal_month"]));
		// endforeach;
		// 
		//$graph_details = array('' => , );;
		// array_merge($series_data,$series_data_2);
		// echo "<pre>";print_r($series_data_2);echo "</pre>";exit;
		$graph_type = 'column';
		$graph_data = array_merge($graph_data, array("graph_id" => 'dem_graph_order'));
		$graph_data = array_merge($graph_data, array("graph_title" => "$year $title Order Cost"));
		$graph_data = array_merge($graph_data, array("graph_type" => $graph_type));
		$graph_data = array_merge($graph_data, array("color" => "['#92A8CD', '#A47D7C']"));
		$graph_data = array_merge($graph_data, array("graph_yaxis_title" => "Cost in KSH"));
		$graph_data = array_merge($graph_data, array("graph_categories" => $category_data));
		$graph_data = array_merge($graph_data, array("series_data" => array('Cost of Orders Made' => $series_data, 'Cost of Orders delivered' => $series_data_2)));
		$data = array();
		//seth
		$data['high_graph'] = $this -> hcmp_functions -> create_high_chart_graph($graph_data);
		// echo $data['high_graph'];exit;
		$data['graph_id'] = 'dem_graph_order';
		return $this -> load -> view("shared_files/report_templates/high_charts_template_v_national", $data);
		else :
			$excel_data = array('doc_creator' => "HCMP", 'doc_title' => "$year $title Order Cost", 'file_name' => "$year $title Order Cost (KSH)");
		$row_data = array();
		$column_data = array("Date of Order Placement", "Date of Order Approval", "Total Order Cost (Ksh)", "Date of Delivery", "Lead Time (Order Placement to Delivery)", "Supplier", "Facility Name", "Facility Code", "Sub-County", "County");
		$excel_data['column_data'] = $column_data;
		//echo  ; exit;
		$facility_stock_data = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("SELECT c.county,d.district as sub_county, f.facility_name, f.facility_code, 
			DATE_FORMAT(`order_date`,'%d %b %y') as order_date, 
			DATE_FORMAT(`approval_date`,'%d %b %y')  as approval_date,
			DATE_FORMAT(`deliver_date`,'%d %b %y')  as delivery_date, 
			DATEDIFF(`approval_date`,`order_date`) as tat_order_approval,
			DATEDIFF(`deliver_date`,`approval_date`) as tat_approval_deliver,
			DATEDIFF(`deliver_date`,`order_date`) as tat_order_delivery
			, sum(o.`order_total`) as total 
			from facility_orders o, facilities f, districts d, counties c 
			where f.facility_code=o.facility_code and f.district=d.id 
			and c.id=d.county $and_data
			group by o.id order by c.county asc ,d.district asc , 
			f.facility_name asc 
			");
		array_push($row_data, array("The orders below were placed $year $title"));
		foreach ($facility_stock_data as $facility_stock_data_item) :
			array_push($row_data, array($facility_stock_data_item["order_date"], $facility_stock_data_item["approval_date"], $facility_stock_data_item["total"], $facility_stock_data_item["delivery_date"], $facility_stock_data_item["tat_order_delivery"], "KEMSA", $facility_stock_data_item["facility_name"], $facility_stock_data_item["facility_code"], $facility_stock_data_item["sub_county"], $facility_stock_data_item["county"]));
		endforeach;
		$excel_data['row_data'] = $row_data;
		$this -> hcmp_functions -> create_excel($excel_data);
		endif;
	}
	public function get_lead_infor($year = null, $county_id = null, $district_id = null, $facility_code = null, $graph_type = null) {
		$district_id = ($district_id == "NULL") ? null : $district_id;
		$graph_type = ($graph_type == "NULL") ? null : $graph_type;
		$facility_code = ($facility_code == "NULL") ? null : $facility_code;
		$county_id = ($county_id == "NULL") ? null : $county_id;
		$year = ($year == "NULL") ? date('Y') : $year;
		$and = ($district_id > 0) ? " AND d.id = '$district_id'" : null;
		$and .= ($facility_code > 0) ? " AND f.facility_code = '$facility_code'" : null;
		$and .= ($county_id > 0) ? " AND c.id='$county_id'" : null;
		// $and =isset( $and) ?  $and:null;
		$and .= ($year > 0) ? " and year(o.`order_date`) =$year" : null;
		if ($graph_type != "excel") :
			$using_hcmp = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll(" SELECT 
				avg( ifnull(DATEDIFF(`approval_date`, `order_date`),0)) as tat_order_approval,
				avg( ifnull(DATEDIFF(`deliver_date`,`approval_date`),0)) as tat_approval_delivery,
				avg( ifnull(DATEDIFF( `deliver_date`,`order_date`),0)) as tat_order_delivery
				from
				facility_orders o,
				facilities f,
				districts d,
				counties c
				where
				f.facility_code = o.facility_code
				and f.district = d.id
				and c.id = d.county
				$and
				");
		$one = $this -> get_time($using_hcmp[0]['tat_order_approval']);
		$two = $this -> get_time($using_hcmp[0]['tat_approval_delivery']);
		$three = $this -> get_time($using_hcmp[0]['tat_order_delivery']);
		$table = "
		<table class='table table-bordered'>
			<tr><td>Order Placement - Order Approval</td><td>$one</td></tr>
			<tr><td>Order Approval - Order Delivery</td><td>$two</td></tr>
			<tr><td>Order Placement - Order Delivery</td><td>$three</td></tr>
		</table> 
		";
		echo $table;
		else :
			$excel_data = array('doc_creator' => "HCMP", 'doc_title' => "Order Lead Time", 'file_name' => "Order Lead Time");
		$row_data = array();
		$column_data = array("Date of Order Placement", "Date of Order Approval", "Total Order Cost (Ksh)", "Date of Delivery", "Lead Time (Order Placement to Delivery)", "Supplier", "Facility Name", "Facility Code", "Sub-County", "County");
		$excel_data['column_data'] = $column_data;
		$facility_stock_data = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("SELECT c.county,d.district as sub_county, f.facility_name, f.facility_code, 
			DATE_FORMAT(`order_date`,'%d %b %y') as order_date, 
			DATE_FORMAT(`approval_date`,'%d %b %y')  as approval_date,
			DATE_FORMAT(`deliver_date`,'%d %b %y')  as delivery_date, 
			DATEDIFF(`approval_date`,`order_date`) as tat_order_approval,
			DATEDIFF(`deliver_date`,`approval_date`) as tat_approval_deliver,
			DATEDIFF(`deliver_date`,`order_date`) as tat_order_delivery
			, sum(o.`order_total`) as total 
			from facility_orders o, facilities f, districts d, counties c 
			where f.facility_code=o.facility_code and f.district=d.id 
			and c.id=d.county $and
			group by o.id order by c.county asc ,d.district asc , 
			f.facility_name asc 
			");
		array_push($row_data, array("The orders below were placed $year"));
		foreach ($facility_stock_data as $facility_stock_data_item) :
			array_push($row_data, array($facility_stock_data_item["order_date"], $facility_stock_data_item["approval_date"], $facility_stock_data_item["total"], $facility_stock_data_item["delivery_date"], $facility_stock_data_item["tat_order_delivery"], "KEMSA", $facility_stock_data_item["facility_name"], $facility_stock_data_item["facility_code"], $facility_stock_data_item["sub_county"], $facility_stock_data_item["county"]));
		endforeach;
		$excel_data['row_data'] = $row_data;
		$this -> hcmp_functions -> create_excel($excel_data);
		endif;
	}
	public function get_time($days) {
		switch (true) {
			case $days==1 :
			$time = "$days day";
			break;
			case $days>1 && $days<=7 :
			$days = ceil($days);
			$time = "$days days";
			break;
			case $days>7 && $days<=30 :
			$new = ceil($days / 7);
			if ($new === 1) {
				$time = "$new week";
			} else {
				$extra = $days%=7;
				if ($extra <= 1) {
					$time = "$new weeks  $extra day";
				} else {
					$time = "$new weeks  $extra days";
				}
			}
			break;
			case $days>30 && $days<=365 :
			$new = ceil($days / 30);
			if ($new == 1) {
				$time = "$new month";
			} else {
				$time = "$new months";
			}
			break;
			default :
			$time = "N/A";
			break;
		}
		return $time;
	}
	public function demo_accounts() {
		$facility_user_data = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("select distinct
			user.id,
			user.username,
			user.usertype_id,
			ifnull(log.`action`, 0) as login
			from
			user
			left join
			log ON log.user_id = user.id and log.start_time_of_event =(select max(start_time_of_event) from log where log.user_id=user.id)
			where
			user.username like '%@hcmp.com'
			group by user.id
			order by user.id asc
			");
		$graph_data = $series_data = array();
		$total_active_users = $total_inactive_users = 0;
		$status = '';
		$total = count($facility_user_data);
		foreach ($facility_user_data as $facility_user_data) :
			if ($facility_user_data['login'] == "Logged In") {
				$total_active_users++;
				$status = '<button type="button" class="btn btn-xs btn btn-danger">In Use</button>';
			} else {
				$total_inactive_users++;
				$status = '<button type="button" class="btn btn-xs btn-success">Available</button>';
			}
			array_push($series_data, array($facility_user_data['username'], 123456, $status));
			endforeach;
			array_push($series_data, array('', 'Total Available Accounts:', $total_inactive_users));
			array_push($series_data, array('', 'Total Accounts In Use :', $total_active_users));
			array_push($series_data, array('', 'Total Demo Accounts:', $total));
			$category_data = array( array("User Name", " Password ", "Status"));
			$graph_data = array_merge($graph_data, array("table_id" => 'dem_graph_1'));
			$graph_data = array_merge($graph_data, array("table_header" => $category_data));
			$graph_data = array_merge($graph_data, array("table_body" => $series_data));
			$data['content_view'] = $this -> hcmp_functions -> create_data_table($graph_data);
			$data['title'] = "Demo User Accounts";
			$data['banner_text'] = "Demo User Accounts";
			$this -> load -> view('shared_files/template/plain_template_v', $data);
	}
	public function reports() {
		//$data['county'] = Counties::getAll();
		//Added function to display oonly the counties that are currently using HCMP
		$counties = Counties::get_counties_all_using_CD4();
		$data['county'] = $counties;
		$data['commodities'] = Commodities::get_all();		
		$data['sub_county'] = Districts::getAll();
		$this -> load -> view('national/reports_home', $data);
	}
	public function facilities_json() {
		echo json_encode(facilities::getAll_json());
	}
	public function commodities_json() {
		echo json_encode(Commodities::getAll_json());
	}
	public function consumption_report() {
		$county = $_POST['county'];
		$sub_county = $_POST['sub_county'];
		$facility_id = $_POST['facility_id'];
		$email_address = $_POST[''];
		$username = $_POST['username'];
		$facility_id = $_POST['facility_id'];
	}
	public function tracer_item_stock_level($county_id = null, $district_id = null, $facility_code = null, $commodity_id = null, $graph_type = null)
	{
		$district_id = ($district_id == "NULL") ? null : $district_id;
		$graph_type = ($graph_type == "NULL") ? null : $graph_type;
		$facility_code = ($facility_code == "NULL") ? null : $facility_code;
		$county_id = ($county_id == "NULL") ? null : $county_id;
		$commodity_id = ($commodity_id == "ALL" || $commodity_id == "NULL") ? null : $commodity_id;
		// echo $commodity_id;die;
		$and_data = ($district_id > 0) ? " AND d.id = '$district_id'" : null;
		$and_data .= ($facility_code > 0) ? " AND f.facility_code = '$facility_code'" : null;
		$and_data .= ($county_id > 0) ? " AND ct.id='$county_id'" : null;
		// $and_data .= ($county_id > 0) ? " AND counties.id='$county_id'" : null;
		$and_data = isset($and_data) ? $and_data : null;
		$from_others = null;
		$from_others .= ($district_id > 0) ? "districts d," : null;
		$from_others .= ($county_id > 0) ? "counties ct," : null;
		if ($graph_type=='excel') {
			$and_data .= isset($commodity_id) ? "AND d.id =$commodity_id" : "AND d.tracer_item =1";			
		// $and_data .= isset($commodity_id) ? "AND commodities.id =$commodity_id" : "AND d.tracer_item =1";			
		}else{
			$and_data .= isset($commodity_id) ? "AND commodities.id =$commodity_id" : "AND commodities.tracer_item =1";
		}
		$group_by = ($district_id > 0 && isset($county_id) && !isset($facility_code)) ? " ,d.id" : null;
		$group_by .= ($facility_code > 0 && isset($district_id)) ? "  ,f.facility_code" : null;
		$group_by .= ($county_id > 0 && !isset($district_id)) ? " ,c_.id" : null;
		$group_by = isset($group_by) ? $group_by : " ,c_.id";
		$title = '';
		if (isset($county_id)) :
			$county_name = counties::get_county_name($county_id);
		$name = $county_name['county'];
		$title = "$name County";
		elseif (isset($district_id)) :
			$district_data = (isset($district_id) && ($district_id > 0)) ? districts::get_district_name($district_id) -> toArray() : null;
		$district_name_ = (isset($district_data)) ? " :" . $district_data[0]['district'] . " Subcounty" : null;
		$title = isset($facility_code) && isset($district_id) ? "$district_name_ : $facility_name" : (isset($district_id) && !isset($facility_code) ? "$district_name_" : "$name County");
		elseif (isset($facility_code)) :
			$facility_code_ = isset($facility_code) ? facilities::get_facility_name_($facility_code) : null;
		$title = $facility_code_['facility_name'];
		else :
			$title = "National";
		endif;
		if ($graph_type != "excel") :
			$commodity_array ;
		$getdates = Doctrine_Manager::getInstance()->getCurrentConnection()
		->fetchAll("SELECT MIN(created_at) as EarliestDate,MAX(created_at) as LatestDate
			FROM facility_issues");
		//echo '<pre>'; print_r($getdates);echo '<pre>'; exit;
		$early=$getdates[0]['EarliestDate'];
		$late=$getdates[0]['LatestDate'];
		$now = time(); 
		$my_date = strtotime($early);
		$datediff = ($now - $my_date)/(60*60*24);//in days
		$datediff= round($datediff,1);
			if (isset($county_id)) {
				$getdates = Doctrine_Manager::getInstance()->getCurrentConnection()
				->fetchAll("SELECT MIN(created_at) as EarliestDate,MAX(created_at) as LatestDate
					FROM facility_issues 
					inner join facilities on facility_issues.facility_code=facilities.facility_code
					inner join districts on facilities.district=districts.id
					inner join counties on districts.county=counties.id
					inner join commodities on facility_issues.commodity_id=commodities.id where counties.id=$county_id");
				//echo '<pre>'; print_r($getdates);echo '<pre>'; exit;
				$early=$getdates[0]['EarliestDate'];
				$late=$getdates[0]['LatestDate'];
				$now = time(); 
				$my_date = strtotime($early);
			$datediff = ($now - $my_date)/(60*60*24);//in days
			$datediff= round($datediff,1);
			$get_amc = Doctrine_Manager::getInstance()->getCurrentConnection()
			->fetchAll("SELECT commodities.id,commodities.commodity_name,avg(facility_issues.qty_issued) as total_units_consumed,
				(sum(facility_issues.qty_issued)*30/$datediff)/commodities.total_commodity_units as amc_packs,commodities.total_commodity_units FROM hcmp_rtk.facility_issues 
				inner join facilities on facility_issues.facility_code=facilities.facility_code
				inner join districts on facilities.district=districts.id
				inner join counties on districts.county=counties.id inner join commodities on facility_issues.commodity_id=commodities.id where s11_No IN('internal issue','(-ve Adj) Stock Deduction')
				$and_data group by commodities.id");
			$get_amc = Doctrine_Manager::getInstance()->getCurrentConnection()
			->fetchAll($new_sql_amc);
			//return $get_amc ;	
			$get_totals = Doctrine_Manager::getInstance()->getCurrentConnection()
			->fetchAll("SELECT commodities.id,commodities.commodity_name,sum(facility_stocks.current_balance) 
				as total_bal_units, sum(facility_stocks.current_balance)/commodities.total_commodity_units as cur_bal_packs,commodities.total_commodity_units FROM hcmp_rtk.facility_stocks 
				inner join facilities on facility_stocks.facility_code=facilities.facility_code
				inner join districts on facilities.district=districts.id
				inner join counties on districts.county=counties.id inner join commodities on facility_stocks.commodity_id=commodities.id 
				where commodities.status=1 $and_data group by commodities.id");
			}else {
				$new_sql_amc = "SELECT commodities.id,commodities.commodity_name,CEIL(AVG(facility_issues.qty_issued)) AS total_units_consumed, 
				CEIL((SUM(facility_issues.qty_issued) / 3)) AS amc_units,
				CEIL((SUM(facility_issues.qty_issued) / 3) / commodities.total_commodity_units) AS amc_packs,commodities.total_commodity_units
				FROM   facility_issues $from_others INNER JOIN  commodities  ON facility_issues.commodity_id = commodities.id 
				WHERE s11_No IN ('internal issue' , '(-ve Adj) Stock Deduction') $and_data 
				AND facility_issues.expiry_date > '2016-04-21' 
				AND facility_issues.date_issued  between '2016-02-31' and '2016-04-31'
				GROUP BY commodities.id order by commodities.id asc";
			// echo $new_sql_amc;die;
				$get_amc = Doctrine_Manager::getInstance()->getCurrentConnection()
				->fetchAll($new_sql_amc);
				//return $get_amc ;	
				echo $and_data;exit;
				$new_sql_totals = "SELECT commodities.id,commodities.commodity_name,CEIL(sum(facility_stocks.current_balance))
				as total_bal_units, CEIL(sum(facility_stocks.current_balance)/commodities.total_commodity_units) as cur_bal_packs,commodities.total_commodity_units FROM facility_stocks $from_others inner join commodities on facility_stocks.commodity_id=commodities.id 
				where commodities.status=1 $and_data AND facility_stocks.expiry_date > '2016-04-21' group by commodities.id order by commodities.id asc";
				$get_totals = Doctrine_Manager::getInstance()->getCurrentConnection()
				->fetchAll($new_sql_totals);
			}
			//return $get_totals ;	
			$combine=array ();
			for ($i=0; $i < sizeof($get_totals) ; $i++) { 
			//array_push($combine,$get_totals[$i ],$get_amc[$i ]);
				$combine[]=array_merge($get_totals[$i ],$get_amc[$i ]);
			}
			// echo '<pre>'; print_r($combine);echo '<pre>'; exit;
			$category_data = array();
			$series_data = $series_data_ = array();
			$temp_array = $temp_array_ = array();
			$graph_data = array();
			$graph_type = '';
			foreach ($combine as $data) :
				$series_data = array_merge($series_data, array($data["commodity_name"] => (int)$data['cur_bal_packs']/(int)$data['amc_packs']));
			$category_data = array_merge($category_data, array($data["commodity_name"]));
			endforeach;
			// echo "<pre>";print_r($series_data);echo "</pre>";exit;
			$graph_type = 'bar';
			$graph_data = array_merge($graph_data, array("graph_id" => 'dem_graph_mos'));
			$graph_data = array_merge($graph_data, array("graph_title" => "$title Stock Level in Months of Stock (MOS)"));
			$graph_data = array_merge($graph_data, array("graph_type" => $graph_type));
			$graph_data = array_merge($graph_data, array("color" => "['#4572A7','#FFF263', '#6AF9C4']"));
			$graph_data = array_merge($graph_data, array("graph_yaxis_title" => "MOS"));
			$graph_data = array_merge($graph_data, array("graph_categories" => $category_data));
			$graph_data = array_merge($graph_data, array("series_data" => array('total' => $series_data)));
			$data = array();
			$data['high_graph'] = $this -> hcmp_functions -> create_high_chart_graph($graph_data);
			//
			$data['graph_id'] = 'dem_graph_mos';
			return $this -> load -> view("shared_files/report_templates/high_charts_template_v_national", $data);
			//
			else :
				$excel_data = array('doc_creator' => "HCMP", 'doc_title' => "Stock Level in Months of Stock $title", 'file_name' => $title . ' MOS');
			$row_data = array();
			$column_data = array("County", "Sub-County", "Facility Name", "Facility Code", "Item Name", "MOS(packs)");
			$excel_data['column_data'] = $column_data;
			//echo '' ; exit;
			$commodity_array = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("SELECT d.id,ct.county,sc.district,f.facility_code,
				f.facility_name,sum(fs.current_balance) as bal
				,sum(fs.current_balance)/d.total_commodity_units as packs,d.total_commodity_units,fs.batch_no,fs.expiry_date,d.commodity_name
				FROM hcmp_rtk.facility_stocks fs
				INNER JOIN facilities f ON  fs.facility_code=f.facility_code
				INNER JOIN commodities d ON  fs.commodity_id=d.id
				INNER JOIN districts sc ON  f.district=sc.id
				INNER JOIN counties ct ON  sc.county=ct.id
				$and_data AND fs.status=1 group by fs.batch_no order by ct.id asc
				");
			$r_data = array();
			$counter = 0;
			foreach ($commodity_array as $key) {
				$commodity=$key['id'];
				$f_code=$key['facility_code'];
				$batch_n=$key['batch_no'];
				$amc = Doctrine_Manager::getInstance() -> getCurrentConnection() -> fetchAll("
					SELECT sum(qty_issued) as amc FROM hcmp_rtk.facility_issues where commodity_id=$commodity and facility_code=$f_code AND batch_no='$batch_n' AND
					date_issued	> DATE_SUB(CURDATE(), INTERVAL 31 DAY)
					");
				foreach ($amc as $val ) {
					$amc= $val['amc'];
					if ($amc<0) {
						$amc=$amc*-1;
					}
					$amc_packs=round(($amc/$key['total_commodity_units']));
					if ($amc_packs<0) {
						$amc_packs=$amc_packs*-1;
					}
					if ($key['bal']<0 ) {
						$bal=$key['bal']*-1;
					}else{
						$bal=$key['bal'];
					}
					if ($key['packs']<0 ) {
						$packs=$key['packs']*-1;
					}else{
						$packs=$key['packs'];
					}
				}
			$r_data[$counter]["county"] = $key['county'];
			$r_data[$counter]["district"] = $key['district'];
			$r_data[$counter]["facility_code"] = $key['facility_code'];
			$r_data[$counter]["facility_name"] = $key['facility_name'];
			$r_data[$counter]["commodity_name"] = $key['commodity_name'];
			$r_data[$counter]["amc_packs"] = $amc_packs;
			// $r_data[$counter]["amc_units"] = $key['bal'];
			$counter = $counter + 1;
			}
			
			array_push($row_data,$r_data);
			$excel_data['row_data'] = $r_data;
			// echo "<pre>";print_r($row_data);echo "</pre>";exit;
			$this -> hcmp_functions -> create_excel($excel_data);
			endif;
	}
	public function stocking_levels($county_id = NULL, $district_id = NULL, $facility_code = NULL, $commodity_id = NULL,$tracer_item = NULL,$division = NULL,$graph_type = NULL){
		/*function does not take county if you give district,neither does it take district if you give facility. purpose: query optimisation*/
		// $commodity_id = 456;
		// echo $tracer_item;exit;
		$county_id = ($county_id == "NULL") ? null : $county_id;
		$district_id = ($district_id == "NULL") ? null : $district_id;
		$graph_type = ($graph_type == "NULL") ? null : $graph_type;
		$facility_code = ($facility_code == "NULL") ? null : $facility_code;
		$tracer_item = ($tracer_item == "NULL") ? null : $tracer_item;
		$commodity_id = ($commodity_id == "ALL" || $commodity_id == "NULL") ? null : $commodity_id;
		$division_details = Dashboard_model::get_division_details($division);
		// echo "<pre>";print_r($tracer_commodities);exit;
		if (isset($division) && $division>0) {
			$page_title = $division_details[0]['division_name'];
			$tracer = "NULL";
		}else{
			$tracer = 1;
			$page_title = "Tracer Item";
		}
		$page_title = trim($page_title);		
		$graph_title = $page_title.' National Stock Level';
		if($county_id>0){
			$county_name = Counties::get_county_name($county_id);			
			$graph_title = $page_title.' '.$county_name['county'].' County Stock Level';
		}
		if($district_id>0){
			$district_name = Districts::get_district_name_($district_id);
			$graph_title = $page_title.' '.$district_name['district'].' Sub-County Stock Level';
		}
		if($facility_code>0){
			$facility_name = Facilities::get_facility_name2($facility_code);			
			$graph_title = $page_title.' '.$facility_name['facility_name'].' Stock Level';
		}
		
		// echo $tracer_item;exit;
		// echo is_null($district_id);
		$filter = '';
		$filter .= ($county_id > 0 && is_null($district_id))? " AND counties.id = $county_id":NULL;
		$filter .= ($district_id > 0 && is_null($county_id))? " AND districts.id = $district_id":NULL;
		$filter .= ($facility_code > 0 && is_null($county_id) && is_null($district_id))? " AND facilities.facility_code = $facility_code":NULL;
		$filter .= ($commodity_id > 0)? " AND commodities.id = $commodity_id ":NULL;
		$filter .= ($tracer_item > 0)? " AND commodities.tracer_item = 1 " : NULL;
		if($division!=5){
			$filter .= ($division > 0)? " AND commodities.commodity_division = $division" : NULL;			
		}
		// echo $filter;exit;
		/*echo "SELECT 
			    commodities.id,
			    commodities.commodity_name,
			    SUM(facility_stocks.current_balance) AS unit_balance,
			    SUM(facility_stocks.current_balance) / commodities.total_commodity_units AS pack_balance,
			    commodities.total_commodity_units
			FROM
			    hcmp_rtk.facility_stocks
			        INNER JOIN
			    facilities ON facility_stocks.facility_code = facilities.facility_code
			        INNER JOIN
			    districts ON facilities.district = districts.id
			        INNER JOIN
			    counties ON districts.county = counties.id
			        INNER JOIN
			    commodities ON facility_stocks.commodity_id = commodities.id
			WHERE
			    commodities.status = 1
			         $filter
			GROUP BY commodities.id ORDER BY commodities.commodity_name ASC";exit;*/
		$stocking_levels = $this->db->query("
			SELECT 
			    commodities.id,
			    commodities.commodity_name,
			    CEIL(SUM(facility_stocks.current_balance)) AS unit_balance,
			    CEIL(SUM(facility_stocks.current_balance)) / commodities.total_commodity_units AS pack_balance,
			    commodities.total_commodity_units
			FROM
			    hcmp_rtk.facility_stocks
			        INNER JOIN
			    facilities ON facility_stocks.facility_code = facilities.facility_code
			        INNER JOIN
			    districts ON facilities.district = districts.id
			        INNER JOIN
			    counties ON districts.county = counties.id
			        INNER JOIN
			    commodities ON facility_stocks.commodity_id = commodities.id
			WHERE
			    commodities.status = 1
			    and facility_stocks.expiry_date > NOW()
			         $filter
			GROUP BY commodities.id ORDER BY commodities.commodity_name ASC
		")->result_array();
		// echo "<pre>"; print_r($stocking_levels); exit;
		/*			
		$category_data = array();
		$series_data = $series_data_ = array();
		$temp_array = $temp_array_ = array();
		$graph_data = array();
		$graph_type = '';
		foreach ($stocking_levels as $data) :
			$series_data = array_merge($series_data, array($data["commodity_name"] => (int)$data['cur_bal_packs']/(int)$data['amc_packs']));
			$category_data = array_merge($category_data, array($data["commodity_name"]));
		endforeach;
		*/
		/*CODE FOR MULTI BAR COLUMN*/
		$graph_data=array();
		$graph_data=array_merge($graph_data,array("graph_id"=>'dem_graph_mos'));
		$graph_data=array_merge($graph_data,array("graph_title"=>$graph_title));
		$graph_data = array_merge($graph_data, array("color" => "['#7CB5EC', '#434348']"));
		$graph_data=array_merge($graph_data,array("graph_type"=>'bar'));
		// $graph_data=array_merge($graph_data,array("graph_yaxis_title"=>'National Stock Level (Units and Packs)'));
		$graph_data=array_merge($graph_data,array("graph_categories"=>array()));
		// $graph_data=array_merge($graph_data,array("series_data"=>array("Pack Balance"=>array(),"Unit Balance"=>array())));
		$graph_data=array_merge($graph_data,array("series_data"=>array("Pack Balance"=>array())));
		$graph_data['stacking']='normal';
		foreach($stocking_levels as $stock_level):
			// $category_name = $stock_level['commodity_name'].' ('.$facility_stock_['source_name'].')';
			$category_name = $stock_level['commodity_name'];
			$graph_data['graph_categories']=array_merge($graph_data['graph_categories'],array($category_name));	
			// $graph_data['series_data']['Unit Balance']=array_merge($graph_data['series_data']['Unit Balance'],array((float) $stock_level['unit_balance']));
			$graph_data['series_data']['Pack Balance']=array_merge($graph_data['series_data']['Pack Balance'],array((float) $stock_level['pack_balance']));	
		endforeach;
		// echo "<pre>";print_r($graph_data);exit;
		/*END OF THAT TITLE OVER THERE*/
		// echo "<pre>";print_r($graph_data);echo "</pre>";exit;
		
		/*
		$graph_type = 'bar';
		$graph_data = array_merge($graph_data, array("graph_id" => 'dem_graph_mos'));
		$graph_data = array_merge($graph_data, array("graph_title" => "$title Stock Level in Months of Stock (MOS)"));
		$graph_data = array_merge($graph_data, array("graph_type" => $graph_type));
		$graph_data = array_merge($graph_data, array("color" => "['#4572A7','#FFF263', '#6AF9C4']"));
		$graph_data = array_merge($graph_data, array("graph_yaxis_title" => "MOS"));
		$graph_data = array_merge($graph_data, array("graph_categories" => $category_data));
		// $graph_data = array_merge($graph_data, array("series_data" => array('total' => $series_data)));
		$graph_data=array_merge($graph_data,array("series_data"=>array("Unit Balance"=>array(),"Pack Balance"=>array())));
		$data = array();
		*/
		$data['high_graph'] = $this -> hcmp_functions -> create_high_chart_graph($graph_data);
		
		// echo "<pre>";print_r($data['high_graph']);exit;
		$data['graph_id'] = 'dem_graph_mos';
		if ($graph_type == "excel") {
			// print_r($_GET); exit;
			// echo "<pre>"; print_r($stocking_levels); exit;
			$excel_data = array('doc_creator' => "HCMP", 'doc_title' => $page_title, 'file_name' => $page_title);
			$row_data = array();
			$column_data = array("Commoidity Name", "Unit Balance", "Pack Balance", "Total Commodity Units");
			foreach($stocking_levels as $stocking_levels){
				array_push($row_data, array($stocking_levels['commodity_name'], $stocking_levels['unit_balance'],$stocking_levels['pack_balance'],$stocking_levels['total_commodity_units']));
			}
			$excel_data['column_data'] = $column_data;
			$excel_data['row_data'] = $row_data;
			$this -> hcmp_functions -> create_excel($excel_data);
		}
		return $this -> load -> view("shared_files/report_templates/high_charts_template_v_national", $data);
		/*END OF THIS OLD SH!T*/
	}
	public function report_problems()
	{
		$commodities = Dashboard_model::get_commodity_count();
		$facility_count = Dashboard_model::get_online_offline_facility_count();
		
		$commodity_divisions = Dashboard_model::get_division_details();
		$data['commodity_divisions'] = $commodity_divisions;
		$data['facility_count'] = $facility_count;
		$data['commodity_count'] = $commodities;
		$data['content_view'] = 'dashboard/dashboard_report_problems';
		return $this->load->view('dashboard/dashboard_template',$data); 
	}
	public function work_in_progress()
	{
		$commodities = Dashboard_model::get_commodity_count();
		$facility_count = Dashboard_model::get_online_offline_facility_count();
		
		$commodity_divisions = Dashboard_model::get_commodity_divisions();
		$data['commodity_divisions'] = $commodity_divisions;
		$data['facility_count'] = $facility_count;
		$data['commodity_count'] = $commodities;
		$data['content_view'] = 'dashboard/dashboard_work_in_progress';
		return $this->load->view('dashboard/dashboard_template',$data); 
	}

	public function kemsa_reports()
	{
		// echo "kemsa_reports function.";exit;
		$pod_query = "SELECT * FROM pod_files pf, counties c WHERE pf.county_id = c.id";
		$pod_result = $this->db->query($pod_query)->result_array();
		// echo "<pre>";print_r($pod_result);exit;

		$commodities = Dashboard_model::get_commodity_count();
		$facility_count = Dashboard_model::get_online_offline_facility_count();
		
		$commodity_divisions = Dashboard_model::get_commodity_divisions();
		$data['pods'] = $pod_result;

		$data['commodity_divisions'] = $commodity_divisions;
		$data['facility_count'] = $facility_count;
		$data['commodity_count'] = $commodities;
		$data['content_view'] = 'dashboard/dashboard_work_in_progress';

		$data['county_count'] = count($counties_using_CD4);
		$data['district_data'] = $districts;
		$data['commodity_divisions'] = $commodity_divisions;
		$data['title'] = "National Dashboard";
		$data['county_data'] = $counties;
		$data['county_id'] = $county_id;
		$data['subcounty_id'] = $district_id;
		$data['district_data'] = $districts;
		$data['maps'] = $map;
		$data['counties'] = $county_name;
		$this -> load -> view("v2/dashboard/reports/dashboard_pod_reports", $data);
	}

	public function download_pod($file_name)
	{
        // echo "<pre>";print_r($file_name);exit;
        // echo urldecode($file_name);exit;
        $file_name = urldecode($file_name);
        $file_data = explode("_", $file_name);

        // echo "<pre>";print_r($file_data);exit;
        $path = FCPATH.'/ftp/'.$file_data[0].'/'.$file_data[1].'/'.$file_data[2];
        // echo "<pre>";print_r($path);exit;
        ob_clean();
		$data = file_get_contents($path);
		//comment to make this commitable
		// echo "<pre>";print_r($path);exit;
		// Read the file's contents
		force_download(basename($path), $data);
		// echo "<pre>";print_r($path);exit;
	}

	public function file_download($path)
	{
		$download = $this->hcmp_functions->download_file($path);
	}

	public function dash_reporting_rates($county_id = NULL, $district_id = NULL, $facility_id = NULL, $year = NULL, $month = NULL)
	{
		// echo "<pre>";print_r($district_id);exit;
        if ($year == NULL) {
            $year = date('Y', time());
            $month = date('m', time());
        }

        if ($county_id > 0) {
            $from = ',districts,counties';
            $conditions .= "and lab_commodity_orders.district_id= districts.id and districts.county = counties.id and counties.id = $county_id";
        }

        if ($district_id > 0) {
        	$from = ',districts,counties';
            $conditions .= "and lab_commodity_orders.district_id= districts.id and districts.county = counties.id and districts.id = $district_id";
        }
        $firstdate = $year . '-' . $month . '-01';
        $month = date("m", strtotime("$firstdate"));
        $year = date("Y", strtotime("$firstdate"));
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $lastdate = $year . '-' . $month . '-' . $num_days;
        $firstdate = $year . '-' . $month . '-01';

        $sql = "select
                lab_commodity_orders.order_date as order_date,
                count(distinct lab_commodity_orders.facility_code) as count
                from
                lab_commodity_orders $from
                WHERE
                lab_commodity_orders.order_date between '$firstdate' and '$lastdate' $conditions
                Group BY lab_commodity_orders.order_date";
		// echo "<pre>";print_r($sql);exit;
        $res = $this->db->query($sql);
        // echo "<pre>";print_r($res->result_array());exit;
        return ($res->result_array());
    
	}

	public function get_kemsa_stock_status($year = NULL,$county_id = NULL,$district_id = NULL,$facility_code = NULL)
	{
		// echo "<pre>";print_r($facility_code);exit;
        $months_texts = array();
        $percentages = array();
       // echo "$sql";die();
        $cur_date = date('Y-m-d');
		$kemsa_stock_data = $this->get_kemsa_stock_data(NULL,NULL,NULL,$year);
		// echo "<pre>";print_r($kemsa_stock_data);exit;
        
        $stock_status_table = "";
        // $trend_chart = "<script type=\"text/javascript\">";

        $stock_status_table .= '
        	<div class="table-scrollable">
				<table class="table table-bordered table-hover">
				<thead>
					<th></th>
					<th>Screening</th>
					<th>Confirmatory</th>
				</thead>
				
				<tbody>
				
				<tr>
					<td>
						 <strong>Receipts from suppliers (Current year to date)</strong>
					</td>
					<td>
						 '.number_format($kemsa_stock_data[4]['ytd_receipts']).'
					</td>
					<td>
						 '.number_format($kemsa_stock_data[5]['ytd_receipts']).'
					</td>
				</tr>
				<tr>
					<td>
						 <strong>Receipts from suppliers (Current month to date)</strong>
					</td>
					<td>
						 '.number_format($kemsa_stock_data[4]['mtd_receipts']).'
					</td>
					<td>
						 '.number_format($kemsa_stock_data[5]['mtd_receipts']).'
					</td>
				</tr>
				<tr>
					<td>
						 <strong>Issues to sites (Current year to date)</strong>
					</td>
					<td>
						 '.number_format($kemsa_stock_data[4]['ytd_issues']).'
					</td>
					<td>
						 '.number_format($kemsa_stock_data[5]['ytd_issues']).'
					</td>
				</tr>
				<tr>
					<td>
						 <strong>Issues to sites (Current month to date)</strong>
					</td>
					<td>
						 '.number_format($kemsa_stock_data[4]['mtd_issues']).'
					</td>
					<td>
						 '.number_format($kemsa_stock_data[5]['mtd_issues']).'
					</td>
				</tr>
				<tr>
					<td>
						 <strong>Stock at hand (As at '.date('Y-F-d',strtotime($kemsa_stock_data[4]['stock_date'])).')</strong>
					</td>
					<td>
						 '.number_format($kemsa_stock_data[4]['at_hand']).'
					</td>
					<td>
						 '.number_format($kemsa_stock_data[5]['at_hand']).'
					</td>
				</tr>

				<tr>
					<td colspan="3"><span class="pull-right bold"><small>Date retrieved: '.date('Y-F-d',strtotime($kemsa_stock_data[4]['created_at'])).'</small></span></td>
				</tr>

				</tbody>
				</table>
			</div>
        ';

        echo $stock_status_table;
	}

	public function get_reporting_rates($county_id = NULL,$district_id = NULL,$facility_code = NULL,$month = NULL,$year = NULL)
	{
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;
		// echo "<pre>";print_r($facility_code);exit;
		$county_id = (isset($county_id) && $county_id > 0)? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id > 0)? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code > 0)? $facility_code:NULL;
		$quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;
		$year = (isset($year) && $year > 0)? $year:date('Y');
		$month = (isset($month) && $month > 0)? $month:date('m');
        $cur_year = date('Y');

        if ($year < $cur_year) {
        	$month = '12';
        	$k = 11;
        }else{
        	$k = 12;
        }

        $date = $year.'-'.$month.'-01';

        // echo "<pre>";print_r($date);exit;
        $months_texts = array();
        $percentages = array();
        $group_by = " GROUP BY county_id";
        $rates_group_by = " GROUP BY c.id";

        $final_data = array();
       // echo "$sql";die();
        for ($i=$k; $i >=0; $i--) { 
            $month_date =  date("Y-m-d", strtotime( date($date)." -$i months"));
            $month =  date("mY", strtotime( date($date)." -$i months"));
            $month_num =  date("m", strtotime( date($date)." -$i months"));
            $year =  date("Y", strtotime( date($date)." -$i months"));
            // echo "<pre>";print_r($month);
            $j = $i+1;            
            $month_text =  date("M Y", strtotime( date($date)." -$i months")); 
            array_push($months_texts,$month_text);    

            $firstdate = $year . '-' . $month_num . '-01';
            $lastdate = $year . '-' . $month_num . '-31';
	        // $firstdate = $year . '-' . $month . '-01';
	        // echo "<pre>";print_r($firstdate);
	        // echo "<pre>";print_r($lastdate);exit;
            $total_fac_q = "
			    	SELECT county_id,sum(facilities) as facilities, 
			    	sum(reported) as reported 
			    	FROM `rtk_county_percentage` 
			    	WHERE month='$month' $total_fac_append $group_by";
			// echo "<pre>";print_r($total_fac_q);exit;
	        $total_fac = $this->db->query($total_fac_q);
	        $total_facilities = $total_fac->result_array();
	        // echo "<pre>";print_r($total_facilities);exit;
		        // $facility_count = $total_facilities[0]['facilities'];
	        foreach ($total_facilities as $key => $value) {
	        	$c_id = $value['county_id'];
	        	$t_fac = $value['facilities'];
            	$final_data[$month][$c_id]['county_id'] = $c_id;
            	$final_data[$month][$c_id]['month'] = $month_date;
            	$final_data[$month][$c_id]['total_facilities'] = $t_fac;
	        }

	        // echo "<pre>";print_r($final_data);exit;
	        $rates_sql = "
	        	SELECT 
				    c.id AS county_id,
				    c.county,
				    d.id as district_id,
				    d.district,
				    l.order_date AS order_date,
				    COUNT(DISTINCT l.facility_code) AS count
				FROM
				    lab_commodity_orders l,
				    districts d,
				    counties c
				WHERE
				    l.order_date BETWEEN '$firstdate' AND '$lastdate'
				        AND l.district_id = d.id
				        AND d.county = c.id $rates_group_by
	        ";
	        // echo "<pre>";print_r($rates_sql);exit;
	        $reporting_rates = $this->db->query($rates_sql)->result_array();
            // echo "<pre>";print_r($reporting_rates);exit;
            foreach ($reporting_rates as $key => $value) {
            	$c_id = $value['county_id'];
            	$c_name = $value['county'];
	            $count = $value['count'];
	            $facility_count = $final_data[$month][$c_id]['total_facilities'];
				$final_data[$month][$c_id]['reported'] = $count;
				$final_data[$month][$c_id]['county_name'] = $c_name;

	        	$reporting_percentage = ($count/$facility_count)*100;

	        	$percentage = round($reporting_percentage);

		        if(!is_numeric($percentage)){
		            $percentage = 0;
		        }

		        if($percentage>100){
		            $percentage = 100;
		        }

		        $final_data[$month][$c_id]['percentage'] = $percentage;
            }

		    // echo "<pre>";print_r($final_data);exit;
        }//end of for   

		// echo "<pre>";print_r($final_data);exit;
        // echo "<pre>";print_r($trend_details);exit;
        $reporting_rates_table = "";
        // $trend_chart = "<script type=\"text/javascript\">";

        $reporting_rates_table .= '
				<table class="table table-bordered table-striped table-hover compact datatableajax">
				<thead>
					<th>County</th>
					<th>Month</th>
					<th>Total Facilities</th>
					<th>Reported</th>
					<th>Percentage</th>
				</thead>
				
				<tbody>';
				
		foreach ($final_data as $keys => $monthly) {
			foreach ($monthly as $key => $value) {
				// echo "<pre>";print_r($value);exit;
				$c_name = $value['county_name'];

				if (isset($c_name) && $c_name !='') {
					$first_column = '<td>
								 '.$value['county_name'].'
							</td>';

					$reporting_rates_table .= '
						<tr>
							<td><a style="text-decoration: none!important;color:black;" href="'.base_url().'dashboardv2/county_dashboard/'.$value['county_id'].'" target="_blank">'.$value['county_name'].'</a></td>
							<td>
								 '.date("F Y",strtotime($value['month'])).'
							</td>
							<td>
								 '.$value['total_facilities'].'
							</td>
							<td>
								 '.$value['reported'].'
							</td>
							<td>
								 <strong>'.$value['percentage'].'%</strong>
							</td>

						</tr>';
				}
			}
		}

		$reporting_rates_table .= '</tbody></table>
        ';

        echo $reporting_rates_table;
	}

	public function get_facility_summary_table_data($commodity_id)
	{
		$columns = array();
		$columns[] = "facility_code";
		$columns[] = "facility_name";
		$columns[] = "district_name";
		$columns[] = "county_name";
		$columns[] = "beg_bal";
		$columns[] = "q_rec";
		$columns[] = "q_used";
		$columns[] = "tests_done";
		$columns[] = "losses";
		$columns[] = "positive_adj";
		$columns[] = "negative_adj";
		$columns[] = "closing_bal";

		$limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        $searched = $this->input->post('search')['value'];

        // $searched = "2060";
  		// $start = 6560;
        $totalData = Facility_order_summaries::get_facility_order_data_count(NULL, NULL,NULL, $commodity_id , $quarter,$month,$year);
        
        switch ($commodity_id) {
        	case 4:
        		$balances = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 4 , $beg_date, $end_date, $quarter,"facility",$month,$year);
        		break;
        	case 5:
        		$balances = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 5 , $beg_date, $end_date, $quarter,"facility",$month,$year);
        		break;
        	
        	default:
        		# code...
        		break;
        }
        
        // echo "<pre>";print_r($balances_scr);exit;

        $totalFiltered = $totalData; 
            
        if(empty($searched))
        {            
            $f_data = Facility_order_summaries::get_facility_order_data(NULL, NULL,NULL, $commodity_id , $quarter,$month,$year,$limit,$start,$order,$dir);
        }
        else {
            // $search = $this->input->post('search')['value']; 
            $search = $searched; 

            $f_data =  Facility_order_summaries::get_facility_order_data(NULL, NULL,NULL, $commodity_id , $quarter,$month,$year,$limit,$start,$order,$dir,$search);

            $totalFiltered = count($f_data);
        }

        $data = array();
        if(!empty($f_data))
        {
            foreach ($f_data as $fac)
            {
            	// echo "<pre>";print_r($fac);exit;
            	$f_code = $fac['facility_code'];
                $nestedData['facility_code'] = $fac['facility_code'];
                $nestedData['facility_name'] = $fac['facility_name'];
                $nestedData['district_name'] = $fac['district'];
                $nestedData['county_name'] = $fac['county'];

                // $nestedData['beg_bal'] = number_format($fac['beginning_bal']);
				$nestedData['beg_bal'] = number_format($balances[$f_code]['beginning_balance']);
                
                $nestedData['q_rec'] = number_format($fac['q_received']);
                $nestedData['q_used'] = number_format($fac['q_used']);
                $nestedData['tests_done'] = number_format($fac['no_of_tests_done']);
                $nestedData['losses'] = number_format($fac['losses']);
                $nestedData['positive_adj'] = number_format($fac['positive_adj']);
                $nestedData['negative_adj'] = number_format($fac['negative_adj']);
                
                // $nestedData['closing_bal'] = number_format($fac['closing_stock']);
				$nestedData['closing_bal'] = number_format($balances[$f_code]['closing_balance']);

                $data[] = $nestedData;

            }
        }
         // echo "<pre>";print_r($data);exit;
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data); 
	}

	public function get_facility_order_details($order_id)
	{
		$query = "
			SELECT *
			FROM lab_commodity_details, counties, facilities, districts, lab_commodity_orders, lab_commodity_categories, lab_commodities
			WHERE lab_commodity_details.facility_code = facilities.facility_code
			AND counties.id = districts.county
			AND facilities.facility_code = lab_commodity_orders.facility_code
			AND lab_commodity_details.commodity_id = lab_commodities.id
			AND lab_commodity_categories.id = lab_commodities.category
			AND facilities.district = districts.id
			AND lab_commodity_details.order_id = lab_commodity_orders.id
			AND lab_commodity_orders.id = '$order_id'";
		
		// echo $query;exit;
		$result = $this->db->query($query)->result_array();
		// echo "<pre>";print_r($result);exit;
		$table = '';
		$table .= '<table class="table table-bordered compact table-sm">';
		$table .= '<thead>';
		$table .= '
                <th>Commodity Name</th>
                <th>Beginning Balance</th>
                <th>Quantity Received</th>
                <th>Quantity Used</th>
                <th>Tests Done</th>
                <th>Losses</th>
                <th>Adj (+ve)</th>	
                <th>Adj (-ve)</th>	
                <th>End of Month Physical Count</th>
                <th>Expiring in <u>less than</u> 6 Months</th>
                <th>Days out of Stock</th>	
                <th>Requested for&nbsp;Re-Supply</th>';
		$table .= '</thead>';
		$table .= '<tbody>';
		foreach ($result as $detail) {
			$table .= '
                    <tr>
                        <td><b>'. $detail['commodity_name'].'</b></td>
                        <td>'.number_format($detail['beginning_bal']).'</td>
                        <td>'.number_format($detail['q_received']).'</td>
                        <td>'.number_format($detail['q_used']).'</td>
                        <td>'.number_format($detail['no_of_tests_done']).'</td>
                        <td>'.number_format($detail['losses']).'</td>
                        <td>'.number_format($detail['positive_adj']).'</td>
                        <td>'.number_format($detail['negative_adj']).'</td>
                        <td>'.number_format($detail['closing_stock']).'</td>
                        <td>'.number_format($detail['q_expiring']).'</td>
                        <td>'.number_format($detail['days_out_of_stock']).'</td>	
                        <td>'.number_format($detail['q_requested']).'</td>
                    </tr>';
        }

		$table .= '</tbody>';

		$table .= '</table>';
		echo $table;
	}

	public function get_opening_and_closing_balances_dash_v2($county_id = NULL, $district_id = NULL, $facility_code = NULL, $commodity_id = NULL, $quarter = NULL, $by = NULL,$month = NULL, $year = NULL)
	{
		// echo "MONTH: ".$month." YEAR: ".$year;exit;
		// echo "COUNTY: ".$county_id." DISTRICT ID:".$district_id." MFL".$facility_code." COMMODITY: ".$commodity_id." QUARTER: ".$quarter." BY: ".$by." YEAR".$year;exit;
        $county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
        $district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
        $facility_code = (isset($facility_code) && $facility_code!='')? $facility_code:NULL;
        $year = (isset($year) && $year>0)? $year:NULL;
        $month = (isset($month) && $month>0)? $month:NULL;
        $order_id = (isset($order_id) && $order_id>0)? $order_id:0;
        
        if (isset($month) && $month > 0) {
            $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');

            $lastdate = $end_date;
            $year_ = date('Y',strtotime($beg_date));
            $month_ = date('m',strtotime($beg_date));
            $beg_date = $year."-".$month."-01";
            $end_date = $year."-".$month."-31";
        }

        // echo "<pre>";print_r($beg_date);exit;
        // echo "<pre>";print_r($end_date);exit;
        $cache_key = "get_opening_and_closing_balances".$county_id.$district_id.$facility_code.'4'.$beg_date.$end_date.$quarter.'facility'.$month.$year."facility".date('Y-m-d');
        // echo "<pre>";print_r($cache_key);exit;
        $cache_check = $this->memcached_check($cache_key);
        // echo "<pre>";print_r($cache_check);exit;
        if ($cache_check['status'] == "FAILED") {
        	$balances_scr = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 4 , $beg_date, $end_date, $quarter,'facility',$month,$year);

            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($balances_scr);

            $cache_save = $this->memcached_save($cache_data);
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $balances_scr = json_decode($cache_check['data'],true);
        }
        // echo "<pre>";print_r($balances_scr);exit;

        $cache_key = "get_opening_and_closing_balances".$county_id.$district_id.$facility_code.'5'.$beg_date.$end_date.$quarter.'facility'.$month.$year."facility".date('Y-m-d');
        $cache_check = $this->memcached_check($cache_key);
        if ($cache_check['status'] == "FAILED") {
        	$balances_conf = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 5 , $beg_date, $end_date, $quarter,'facility',$month,$year);

            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($balances_conf,true);

            $cache_save = $this->memcached_save($cache_data);
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $balances_conf = json_decode($cache_check['data'],true);
        }

        // echo "<pre>";print_r($balances_scr);exit;
        foreach ($balances_scr as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
            $c_id = $value['county_id'];
            $d_id = $value['district_id'];
            $f_code = $value['facility_code'];
            $comm_id = $value['commodity_id'];

        	if (count($county_bal[$c_id][$comm_id]) > 0) {
        		$county_bal[$c_id][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$county_bal[$c_id][$comm_id]['beginning_balance'] += $value['beginning_balance'];
        		$county_bal[$c_id][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$county_bal[$c_id][$comm_id]['closing_balance'] += $value['closing_balance'];

        	}else{
        		$county_bal[$c_id][$comm_id]['county_id'] = $value['county_id'];
        		$county_bal[$c_id][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$county_bal[$c_id][$comm_id]['beginning_balance'] = $value['beginning_balance'];
        		$county_bal[$c_id][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$county_bal[$c_id][$comm_id]['closing_balance'] = $value['closing_balance'];
        	}

        	if (count($dist_bal[$d_id]) > 0) {
        		$dist_bal[$d_id][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$dist_bal[$d_id][$comm_id]['beginning_balance'] += $value['beginning_balance'];
        		$dist_bal[$d_id][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$dist_bal[$d_id][$comm_id]['closing_balance'] += $value['closing_balance'];
        	}else{
        		$dist_bal[$d_id][$comm_id]['county_id'] = $value['county_id'];
        		$dist_bal[$d_id][$comm_id]['district_id'] = $value['district_id'];
        		$dist_bal[$d_id][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$dist_bal[$d_id][$comm_id]['beginning_balance'] = $value['beginning_balance'];
        		$dist_bal[$d_id][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$dist_bal[$d_id][$comm_id]['closing_balance'] = $value['closing_balance'];
        	}

        	if (count($fac_bal[$f_code]) > 0) {
        		$fac_bal[$f_code][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$fac_bal[$f_code][$comm_id]['beginning_balance'] += $value['beginning_balance'];
        		$fac_bal[$f_code][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$fac_bal[$f_code][$comm_id]['closing_balance'] += $value['closing_balance'];
        	}else{
        		$fac_bal[$f_code][$comm_id]['facility_code'] = $value['facility_code'];
        		$fac_bal[$f_code][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$fac_bal[$f_code][$comm_id]['beginning_balance'] = $value['beginning_balance'];
        		$fac_bal[$f_code][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$fac_bal[$f_code][$comm_id]['closing_balance'] = $value['closing_balance'];
        	}
        	
        }//end of balance data foreach

        // echo "<pre>";print_r($county_bal);exit;
         // echo "<pre>";print_r($counties);exit;
        foreach ($balances_conf as $key => $value) {
        	// echo "<pre>";print_r($value);exit;
        	$c_id = $value['county_id'];
            $d_id = $value['district_id'];
            $f_code = $value['facility_code'];
            $comm_id = $value['commodity_id'];

        	if (count($county_bal[$c_id]) > 0) {
        		$county_bal[$c_id][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$county_bal[$c_id][$comm_id]['beginning_balance'] += $value['beginning_balance'];
        		$county_bal[$c_id][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$county_bal[$c_id][$comm_id]['closing_balance'] += $value['closing_balance'];
        	}else{
        		$county_bal[$c_id][$comm_id]['county_id'] = $value['county_id'];
        		$county_bal[$c_id][$comm_id]['facility_code'] = $value['facility_code'];
        		$county_bal[$c_id][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$county_bal[$c_id][$comm_id]['beginning_balance'] = $value['beginning_balance'];
        		$county_bal[$c_id][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$county_bal[$c_id][$comm_id]['closing_balance'] = $value['closing_balance'];

        	}

        	if (count($dist_bal[$d_id]) > 0) {
        		$dist_bal[$d_id][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$dist_bal[$d_id][$comm_id]['beginning_balance'] += $value['beginning_balance'];
        		$dist_bal[$d_id][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$dist_bal[$d_id][$comm_id]['closing_balance'] += $value['closing_balance'];
        	}else{
        		$dist_bal[$d_id][$comm_id]['county_id'] = $value['county_id'];
        		$dist_bal[$d_id][$comm_id]['district_id'] = $value['district_id'];
        		$dist_bal[$d_id][$comm_id]['facility_code'] = $value['facility_code'];
                $dist_bal[$d_id][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$dist_bal[$d_id][$comm_id]['beginning_balance'] = $value['beginning_balance'];
        		$dist_bal[$d_id][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$dist_bal[$d_id][$comm_id]['closing_balance'] = $value['closing_balance'];
        	}

        	if (count($fac_bal[$f_code]) > 0) {
        		$fac_bal[$f_code][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$fac_bal[$f_code][$comm_id]['beginning_balance'] += $value['beginning_balance'];
        		$fac_bal[$f_code][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$fac_bal[$f_code][$comm_id]['closing_balance'] += $value['closing_balance'];
        	}else{
        		$fac_bal[$f_code][$comm_id]['county_id'] = $value['county_id'];
        		$fac_bal[$f_code][$comm_id]['district_id'] = $value['district_id'];
        		$fac_bal[$f_code][$comm_id]['facility_code'] = $value['facility_code'];
        		$fac_bal[$f_code][$comm_id]['beginning_order_date'] = $value['beginning_order_date'];
        		$fac_bal[$f_code][$comm_id]['beginning_balance'] = $value['beginning_balance'];
        		$fac_bal[$f_code][$comm_id]['closing_order_date'] = $value['closing_order_date'];
        		$fac_bal[$f_code][$comm_id]['closing_balance'] = $value['closing_balance'];
        	}
        	
        }//end of balance data foreach
        

        $balance_data['county_balances'] = $county_bal;
        $balance_data['district_balances'] = $dist_bal;
        $balance_data['facility_balances'] = $fac_bal;

        return $balance_data;
	}

	public function dhis_data_analysis_dashboard($county_id = NULL,$district_id = NULL,$facility_code = NULL,$commodity_id = NULL,$quarter = NULL,$year = NULL,$month = NULL,$by = NULL,$format = NULL)
	{
		$page_caching = $this->output->cache(CACHE_REFRESH_INTERVAL);
		$default = "tracer";
		$county_id = (isset($county_id) && $county_id > 0)? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id > 0)? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code > 0)? $facility_code:NULL;
		$quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;

		$data['filter_years'] = $this->get_years_to_date();
		$filtered_name = NULL;

		$prev_yr = date("Y",strtotime('-1 YEAR'));
		$quarters_prev_yr = $this->get_year_quarter(NULL,NULL,$prev_yr,1);
		$quarters_curr_yr = $this->get_year_quarter(NULL,NULL,NULL,1);
		$data['quarters'] = array_merge_recursive($quarters_curr_yr, $quarters_prev_yr);
		// $data['filtered'] = $filtered;
		// echo "<pre>";print_r($data);exit;
        $current_date = date('Y-m-d');

		// $data['data_analysis'] = $result;

		$page_title = "DHIS FCDRR Analysis Dashboard.";
		$data['page_title'] = $page_title;

		/*IN  PROGRESS*/

		$cache_key = "get_dhis_data_analysis".date('Y-m-d');
        $cache_check = $this->memcached_check($cache_key);
        if ($cache_check['status'] == "FAILED") {
        	$analysis = $this->get_dhis_data_analysis();

            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($analysis,true);

            $cache_save = $this->memcached_save($cache_data);
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $analysis = json_decode($cache_check['data'],true);
        }
		
		// echo "<pre>analysis: ";print_r($analysis);exit;

		// echo "<pre>";print_r($dhis_data_analysis);exit;
		$data['dhis_data_analysis'] = $analysis;
        // echo "<pre>";print_r($district_drawing_data);exit;

        $data['beginning_balance_date_rtk'] = $analysis['totals']['beginning_balance_order_date'];
        $data['closing_balance_date_rtk'] = $analysis['totals']['closing_balance_order_date'];

        $data['beginning_balance_date_dhis'] = $analysis['totals']['dhis']['beginning_balance'];
        $data['closing_balance_date_dhis'] = $analysis['totals']['dhis']['closing_balance'];

        $graph_stuff = array();
        $graph_stuff['categories'] = 'Screening';
        $graph_stuff['series_data']['HCMP'] = $analysis['totals']['rtk'][4]['beginning_balance'];
        $graph_stuff['series_data']['DHIS'] = $analysis['totals']['dhis'][4]['beginning_balance'];
        $graph_stuff['div'] = 'beginning_balances_graph_scr';

        // echo "<pre>";print_r($graph_stuff);
        $data['beginning_balances_graph_scr'] = $this->column_highchart($graph_stuff);
        // echo "<pre>";print_r($data['beginning_balances_graph_scr']);exit;
        $graph_stuff = array();
        $graph_stuff['categories'] = 'Confirmatory';
        $graph_stuff['series_data']['HCMP'] = $analysis['totals']['rtk'][5]['beginning_balance'];
        $graph_stuff['series_data']['DHIS'] = $analysis['totals']['dhis'][5]['beginning_balance'];
        $graph_stuff['div'] = 'beginning_balances_graph_conf';

        $data['beginning_balances_graph_conf'] = $this->column_highchart($graph_stuff);

        $graph_stuff = array();
        $graph_stuff['categories'] = 'Screening';
        $graph_stuff['series_data']['HCMP'] = $analysis['totals']['rtk'][4]['q_used'];
        $graph_stuff['series_data']['DHIS'] = $analysis['totals']['dhis'][4]['q_used'];
        $graph_stuff['div'] = 'quantity_used_graph_scr';

        // echo "<pre>";print_r($graph_stuff);
        $data['quantity_used_graph_scr'] = $this->column_highchart($graph_stuff);
        // echo "<pre>";print_r($data['quantity_used_graph_scr']);exit;
        $graph_stuff = array();
        $graph_stuff['categories'] = 'Confirmatory';
        $graph_stuff['series_data']['HCMP'] = $analysis['totals']['rtk'][5]['q_used'];
        $graph_stuff['series_data']['DHIS'] = $analysis['totals']['dhis'][5]['q_used'];
        $graph_stuff['div'] = 'quantity_used_graph_conf';

        $data['quantity_used_graph_conf'] = $this->column_highchart($graph_stuff);

        $graph_stuff = array();
        $graph_stuff['categories'] = 'Screening';
        $graph_stuff['series_data']['HCMP'] = $analysis['totals']['rtk'][4]['q_received'];
        $graph_stuff['series_data']['DHIS'] = $analysis['totals']['dhis'][4]['q_received'];
        $graph_stuff['div'] = 'quantity_received_graph_scr';

        // echo "<pre>";print_r($graph_stuff);
        $data['quantity_received_graph_scr'] = $this->column_highchart($graph_stuff);
        // echo "<pre>";print_r($data['quantity_received_graph_scr']);exit;
        $graph_stuff = array();
        $graph_stuff['categories'] = 'Confirmatory';
        $graph_stuff['series_data']['HCMP'] = $analysis['totals']['rtk'][5]['q_received'];
        $graph_stuff['series_data']['DHIS'] = $analysis['totals']['dhis'][5]['q_received'];
        $graph_stuff['div'] = 'quantity_received_graph_conf';

        $data['quantity_received_graph_conf'] = $this->column_highchart($graph_stuff);

        $graph_stuff = array();
        $graph_stuff['categories'] = 'Screening';
        $graph_stuff['series_data']['HCMP'] = $analysis['totals']['rtk'][4]['closing_balance'];
        $graph_stuff['series_data']['DHIS'] = $analysis['totals']['dhis'][4]['closing_balance'];
        $graph_stuff['div'] = 'closing_balance_graph_scr';

        // echo "<pre>";print_r($graph_stuff);
        $data['closing_balance_graph_scr'] = $this->column_highchart($graph_stuff);
        // echo "<pre>";print_r($data['closing_balance_graph_scr']);exit;
        $graph_stuff = array();
        $graph_stuff['categories'] = 'Confirmatory';
        $graph_stuff['series_data']['HCMP'] = $analysis['totals']['rtk'][5]['closing_balance'];
        $graph_stuff['series_data']['DHIS'] = $analysis['totals']['dhis'][5]['closing_balance'];
        $graph_stuff['div'] = 'closing_balance_graph_conf';

        $data['closing_balance_graph_conf'] = $this->column_highchart($graph_stuff);

        // echo "<pre>";print_r($data['closing_balance_graph_scr']);exit;

        $current = date('m');
        $curMonth = $current;
        $curQuarter = ceil($curMonth/3);//KARSAN

        if ($quarter > 0) {
        	$curQuarter = $quarter;
        }

		$title_append = (isset($filtered_name) && $filtered_name !='')? $filtered_name:NULL;

		if ($quarter > 0) {
			// $title_append .= " Report";
		}else{
			// $title_append .= " as of ".date('F',strtotime("-1 MONTH"))." Report";
			$title_append .= " as of ".date('jS F Y').".";
		}

		$title_prepend = (isset($county_id) && $county_id > 0)? 'County':'Sub-County';

		if ($quarter > 0) {
			$quartered = 1;
			// $quarter_append = ' <span class="font-blue-steel">*</span>';
		}


		// echo "<pre>";print_r($kemsa_issue_data_analysis_arr);exit;
		$data['title_append'] = $title_append;
		$data['quartered'] = $quartered;
		$data['quarter_append'] = $quarter_append;
		// $data['tracer_commodities'] = $commodities;
		$data['facility_count'] = $facility_count;
		$data['content_view'] = 'dashboard/dashboard';
		$data['county_data'] = $counties;
		$data['title_prepend'] = $title_prepend;
		$data['county_count'] = count($counties_using_CD4);
		$data['district_data'] = $districts;
		$data['commodity_divisions'] = $commodity_divisions;
		$data['title'] = "DHIS Analysis Dashboard";
		$data['district_data'] = $districts;
		// echo "<pre>";print_r($data);exit;
		$this -> load -> view("v2/dashboard/dashboard_dhis_fcdrr_data_analysis", $data);
	
	}

	public function get_dashboard_summary_details($level = NULL, $level_id = NULL, $type = NULL, $county_id = NULL, $district_id = NULL,$commodity_id = NULL, $quarter = NULL,$month_year = NULL,$year = NULL) {
			// echo "<pre>level: ".$level." level_id: ".$level_id." type: ".$type." county_id: ".$county_id." district_id: ".$district_id." facility_code: ".$facility_code." commodity_id: ".$commodity_id." MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter." BY: ".$by." commodity_id: ".$commodity_id;
			// echo exit;
		$commodity_id = ($commodity_id > 0)? $commodity_id:4;

			switch ($type) {
				case 'drawing_rights':
					//KARSAN IN PROGRESS
					$summary_details_drawing_rights = $this->summary_details_drawing_rights($level,$level_id,$type, $county_id,$district_id,$commodity_id,$quarter,$month_year,$year);
					// echo "<pre>";print_r($summary_details_drawing_rights);exit;
					// echo "<pre>";print_r($data);exit;	
				break;
				
				default:
					$kemsa_issues = $this->get_kemsa_issue_data(NULL,NULL,NULL,NULL,$quarter,$year,$month,"commodity");

					$data['kemsa_issued_screening'] = $kemsa_issues[4]['qty_issued'];
					$data['kemsa_issued_confirmatory'] = $kemsa_issues[5]['qty_issued'];

					$summary_data_s = $this->get_county_order_data($county_id, $district_id, $facility_code, 4 , $quarter,$month,$year);
					// echo "<pre>";print_r($summary_data_s);exit;
					$summary_data_c = $this->get_county_order_data($county_id, $district_id, $facility_code, 5 , $quarter,$month,$year);

					$kemsa_issued_screening = $kemsa_issues[4]['qty_issued'];
					$kemsa_issued_confirmatory = $kemsa_issues[5]['qty_issued'];

					// echo "<pre>";print_r($kemsa_issued_series_scr);exit;
					$received_site_scr = $summary_data_s['results'][0]['q_received'];
					$received_site_conf = $summary_data_c['results'][0]['q_received'];

					$data['received_site_screening'] = $received_site_scr;
					$data['received_site_confirmatory'] = $received_site_conf;

					$data['positive_adj_screening'] = $summary_data_s['results'][0]['positive_adj'];
					$data['positive_adj_confirmatory'] = $summary_data_c['results'][0]['positive_adj'];

					$data['negative_adj_screening'] = $summary_data_s['results'][0]['negative_adj'];
					$data['negative_adj_confirmatory'] = $summary_data_c['results'][0]['negative_adj'];

					$data['losses_screening'] = $summary_data_s['results'][0]['losses'];
					$data['losses_confirmatory'] = $summary_data_c['results'][0]['losses'];

					$data['quantity_used_screening'] = $summary_data_s['results'][0]['q_used'];
					$data['quantity_used_confirmatory'] = $summary_data_c['results'][0]['q_used'];

					$data['summary_data_date'] = $summary_data_s['date_text'];
					// echo "<pre>";print_r($data);exit;
					$consumed_site_scr = $summary_data_s['results'][0]['q_used'];
					$consumed_site_conf = $summary_data_c['results'][0]['q_used'];

					$data['consumed_site_screening'] = $consumed_site_scr;
					$data['consumed_site_confirmatory'] = $consumed_site_conf;


					$balances_scr = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 4 , $beg_date, $end_date, $quarter,NULL,$month,$year);
					$balances_conf = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 5 , $beg_date, $end_date, $quarter,NULL,$month,$year);
					// echo "<pre>";print_r($balances_scr);exit;
					// $closing_bal_scr = $summary_data_s['results'][0]['beginning_bal'];
					// $closing_bal_conf = $summary_data_c['results'][0]['beginning_bal'];

					$closing_bal_scr = $balances_scr['closing_balance']['closing_balance'];
					$closing_bal_conf = $balances_conf['closing_balance']['closing_balance'];
					$closing_bal_order_date = $balances_scr['closing_balance']['order_date'];

					$beginning_bal_scr = $balances_scr['beginning_balance']['beginning_balance'];
					$beginning_bal_conf = $balances_conf['beginning_balance']['beginning_balance'];
					$beginning_bal_order_date = $balances_scr['beginning_balance']['order_date'];

					$data['beginning_balance_screening'] = $beginning_bal_scr;
					$data['beginning_balance_confirmatory'] = $beginning_bal_conf;
					$data['beginning_balance_order_date'] = $beginning_bal_order_date;

					$data['closing_balance_screening'] = $closing_bal_scr;
					$data['closing_balance_confirmatory'] = $closing_bal_conf;
					$data['closing_balance_order_date'] = $closing_bal_order_date;	
				break;
			}
			// echo "<pre>";print_r($data);exit;	
			
	}

	public function summary_details_drawing_rights($level = NULL, $level_id = NULL, $type = NULL, $county_id = NULL, $district_id = NULL,$commodity_id = NULL, $quarter = NULL,$month_year = NULL,$year = NULL)
	{
		// echo "<pre>level: ".$level." level_id: ".$level_id." type: ".$type." county_id: ".$county_id." district_id: ".$district_id." facility_code: ".$facility_code." commodity_id: ".$commodity_id." MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter." BY: ".$by." commodity_id: ".$commodity_id;
		// echo exit;

		$drawing_data_details = $this->get_drawing_rights_details($county_id,$district_id,$facility_code,$quarter,$month,$year);

		// echo "<pre>";print_r($drawing_data_details);exit;
		$data['national_drawing_totals_year'] = $drawing_data_details['data_year'];
		$data['national_drawing_totals'] = $drawing_data_details['national_drawing_totals'];
		$data['county_drawing_totals'] = $drawing_data_details['county_drawing_totals'];
		$data['county_drawing_data'] = $county_drawing_data;
		$data['county_drawing_data_details'] = $drawing_data_details;
		$data['county_drawing_totals_details'] = $drawing_data_details['county_drawing_totals_details'];
		$data['county_drawing_data_year'] = $year;

		// echo "<pre>";print_r($data);
		// echo exit;
		switch ($level) {
			case 'national':
				$county_id = $level_id;
				/*$table_heads = '
				<th>County</th>
				<th colspan="2">Screening</th>
				<th colspan="2">Confirmatory</th>
				<th>Year</th>
				<th>Action</th>
				';*/

				$table_heads = '
				<tr>
					<th rowspan="2">County</th>
					<th colspan="3">Screening</th>
					<th colspan="3">Confirmatory</th>
					<th rowspan="2">Quarter</th>
					<th rowspan="2">Year</th>
				</tr>
				<tr>
					<th>Total</th>
					<th>Allocated</th>
					<th>Balance</th>
					<th>Total</th>
					<th>Allocated</th>
					<th>Balance</th>
				</tr>
				';

				$sub_level = "subcounty";
				break;
			case 'county':
				$county_id = $level_id;
				$table_heads = "
				<th>County</th>
				<th>Subcounty</th>
				<th>Screening</th>
				<th>Confirmatory</th>
				<th>Action</th>
				";
				$sub_level = "subcounty";
				break;
			case 'subcounty':
				$district_id = $level_id;
				$table_heads = "
				<th>Facility Name</th>
				<th>MFL Code</th>
				<th>Sub-County</th>
				<th>County</th>
				<th>Screening</th>
				<th>Confirmatory</th>
				<th>Action</th>
				";
				$sub_level = "facility";
				
				break;
			case 'facility':
				$facility_code = $level_id;
				$table_heads = "
				<th>Facility Name</th>
				<th>MFL Code</th>
				<th>Sub-County</th>
				<th>County</th>
				<th>Drawing Rights</th>
				<th>Action</th>
				";
				$sub_level = "facility_orders";
				break;

			case 'order':
				$order_id = $level_id;
				
				$sub_level = 'order';
				break;
			
			default:
				$table_heads = "
				<th>Facility Name</th>
				<th>MFL Code</th>
				<th>Sub-County</th>
				<th>County</th>
				<th>Drawing Rights</th>
				<th>Action</th>
				";
				break;
		}

		$back_button = '';

		if ($level == 'order'){
			$summary_data = $this->get_order_data_by_id($order_id);
			// echo "<pre>";print_r($summary_data);exit;

			$table = '';
			$table .= '<table class="table table-bordered compact table-sm">';
			$table .= '<thead>';
			$table .= '
	                <th>Commodity Name</th>
	                <th>Beginning Balance</th>
	                <th>Quantity Received</th>
	                <th>Quantity Used</th>
	                <th>Tests Done</th>
	                <th>Losses</th>
	                <th>Adj (+ve)</th>	
	                <th>Adj (-ve)</th>	
	                <th>End of Month Physical Count</th>
	                <th>Expected Closing Balance</th>
	                <th>Expiring in <u>less than</u> 6 Months</th>
	                <th>Days out of Stock</th>	
	                <th>Requested for&nbsp;Re-Supply</th>';
			$table .= '</thead>';
			$table .= '<tbody>';

			foreach ($summary_data as $detail) {
				// echo "<pre>";print_r($detail);exit;
				$mfl = $detail['facility_code'];
				$d_id = $detail['district_id'];
				$f_name = $detail['facility_name'];
				$d_name = $detail['district'];
				$report_for = $detail['report_for'];
				$comm_id = $detail['commodity_id'];

				$beg_bal = $detail['beginning_bal'];
				$rec = $detail['q_received'];
				$pos_adj = $detail['positive_adj'];
				$used = $detail['q_used'];
				$losses = $detail['losses'];
				$neg_adj = $detail['negative_adj'];
				$clos_bal = $detail['closing_stock'];

				$expected = ($beg_bal + $rec + $pos_adj)-($used + $losses + $neg_adj); 

				$table .= '
	                    <tr>
	                        <td><b>'. $detail['commodity_name'].'</b></td>
	                        <td>'.number_format($detail['beginning_bal']).'</td>
	                        <td>'.number_format($detail['q_received']).'</td>
	                        <td>'.number_format($detail['q_used']).'</td>
	                        <td>'.number_format($detail['no_of_tests_done']).'</td>
	                        <td>'.number_format($detail['losses']).'</td>
	                        <td>'.number_format($detail['positive_adj']).'</td>
	                        <td>'.number_format($detail['negative_adj']).'</td>
	                        <td><strong>'.number_format($detail['closing_stock']).'</strong></td>
	                        <td><strong>'.number_format($expected).'</strong></td>
	                        <td>'.number_format($detail['q_expiring']).'</td>
	                        <td>'.number_format($detail['days_out_of_stock']).'</td>	
	                        <td>'.number_format($detail['q_requested']).'</td>
	                    </tr>';
	        }

	        $back_button = '<a class="btn btn-primary" id="summary_modal_details" data-level = "subcounty" data-level-id = "'.$d_id.'" data-commodity-id = "'.$commodity_id.'" data-modal-level="subcounty" style="width: 250px;margin: 5px 0px;">Back to Facility List</a>';
	        $title_data = '
	        	<div class="col-md-12">
	        		<h5 class="pull-left">MFL: <strong>'.$mfl.'</strong> </br>Facility Name: <strong>'.$f_name.'</strong> </br>District: <strong>'.$d_name.'</strong></h5>
	        		<h5 class="pull-right">Report for: <strong>'.$report_for.'</strong></h5>
	        		<h5 class="pull-left">Formula Used: <strong>(Beginning Balance + Quantity Received + Positive Adjustments) - (Quantity Used + Losses + Negative Adjustments)</strong></h5>
	        	</div>
	        ';

			$table .= '</tbody>';

			$table .= '</table>';

			$html .= $title_data.$table;

		}else {

	        $html = '<table class="table table-bordered datatableajax display cell-border compact" cellspacing="0" width="100%" id="datatableajax">
						<thead>
							<tr>
								'.$table_heads.'
							</tr>
						</thead>
						<tbody>';

			// number_format($national_drawing_totals['screening_total_annual']

				switch ($level) {
					case 'national':
						$county_id = $level_id;
						$top_summary = '';

						$scr_total_annual = $conf_total_annual = 0;
						foreach ($data['county_drawing_totals'] as $key => $value) {
							// echo "<pre>";print_r($value);exit;
							$scr_total_annual += $value['screening_default_annual'];
							$conf_total_annual += $value['confirmatory_default_annual'];

							$html .= '
							<tr>
								<td><a style="text-decoration: none!important;" href="'.base_url().'dashboardv2/county_dashboard/'.$value['county_id'].'" target="_blank" class="font-blue-steel bold">'.$value['county'].'</a></td>
								<td>'.number_format($value['screening_total_annual']).'</td>
								<td>'.number_format($value['screening_distributed']).'</td>
								<td>'.number_format($value['screening_total_annual'] - $value['screening_distributed']).'</td>
								<td>'.number_format($value['confirmatory_total_annual']).'</td>
								<td>'.number_format($value['confirmatory_distributed']).'</td>
								<td>'.number_format($value['confirmatory_total_annual'] - $value['confirmatory_distributed']).'</td>
								<td>'.$value['quarter'].'</td>
								<td>'.$value['year'].'</td>
								';
							// $html .= '<td><a class="btn btn-primary" id="summary_modal_details" data-level = "county" data-summary-type="'.$type.'" data-level-id = "'.$value['county_id'].'" data-modal-level="county">View Details</a></td>';
							$html .= '</tr>';
						}

						$top_summary .='
							<table class="table table-bordered">
								<thead>
									<th colspan="2">Totals</th>
								</thead>
								<tbody>
									<tr>
										<td>Screening Total: </td>
										<td><strong>'.number_format($scr_total_annual).'</strong></td>
									</tr>
									<tr>
										<td>Confirmatory Total: </td>
										<td><strong>'.number_format($conf_total_annual).'</strong></td>
									</tr>
								</tbody
							</table>
						';

						break;

					case 'county':
						$county_id = $level_id;
						$top_summary = '';

						$scr_total_annual = $conf_total_annual = 0;
						echo "<pre>";print_r($data);exit;
						foreach ($data['county_drawing_totals'] as $key => $value) {
							// echo "<pre>";print_r($value);exit;
							$scr_total_annual += $value['screening_total_annual'];
							$conf_total_annual += $value['confirmatory_total_annual'];

							$html .= '
							<tr>
								<td>'.$value['county'].'</td>
								<td>'.$value['screening_total_annual'].'</td>
								<td>'.number_format($value['confirmatory_total_annual']).'</td>';
							// $html .= '<td><a class="btn btn-primary" id="summary_modal_details" data-level = "county" data-summary-type="'.$type.'" data-level-id = "'.$value['county_id'].'" data-modal-level="county">View Details</a></td>';
							$html .= '</tr>';
						}

						$top_summary .='
							<table class="table table-bordered">
								<thead>
									<th colspan="2">Totals</th>
								</thead>
								<tbody>
									<tr>
										<td>Screening Total: </td>
										<td><strong>'.number_format($scr_total_annual).'</strong></td>
									</tr>
									<tr>
										<td>Confirmatory Total: </td>
										<td><strong>'.number_format($conf_total_annual).'</strong></td>
									</tr>
								</tbody
							</table>
						';
						break;

					case 'subcounty':
						$district_id = $level_id;
						$html .= '
						<tr>	
							<td>'.$value['facility_name'].'</td>
							<td>'.$value['facility_code'].'</td>
							<td>'.$value['district_name'].'</td>
							<td>'.$value['county_name'].'</td>';
						// $html .= '<td>'.date('F Y',strtotime("-1 MONTH",strtotime($value['order_date']))).'</td>';
						$html .= '<td>'.date('F Y',strtotime("-1 MONTH",strtotime($value['order_date']))).'</td>';
						$html .= '
							<td>'.number_format($beg_bal).'</td>
							<td>'.number_format($expected).'</td>
							<td>'.number_format($reported).'</td>
							<td class="'.$class.'"><strong>'.number_format($difference).'</strong></td>';
						break;
					case 'facility':
						$facility_code = $level_id;
						$html .= '
						<tr>
							<td>'.$value['facility_name'].'</td>
							<td>'.$value['facility_code'].'</td>
							<td>'.$value['district_name'].'</td>
							<td>'.$value['county_name'].'</td>
							<td>'.date('F Y',strtotime("-1 MONTH",strtotime($value['order_date']))).'</td>
							<td>'.number_format($beg_bal).'</td>
							<td>'.number_format($expected).'</td>
							<td>'.number_format($reported).'</td>
							<td class="'.$class.'"><strong>'.number_format($difference).'</strong></td>';
						break;
					
					default:
						$html .= '
						<tr>
							<td>'.$value['facility_name'].'</td>
							<td>'.$value['facility_code'].'</td>
							<td>'.$value['district_name'].'</td>
							<td>'.$value['county_name'].'</td>
							<td>'.date('F Y',strtotime("-1 MONTH",strtotime($value['order_date']))).'</td>
							<td>'.number_format($beg_bal).'</td>
							<td>'.number_format($expected).'</td>
							<td>'.number_format($reported).'</td>
							<td class="'.$class.'"><strong>'.number_format($difference).'</strong></td>';
						break;
				}

				// echo "<pre>";print_r($level);exit;

				// $level = $value['level'];

				// echo "<pre>";print_r($level);exit;
				// echo "<pre>";print_r($lvl);exit;
				// echo "<pre>";print_r($value);exit;
				// echo "<pre>";print_r($sub_level);exit;

				switch ($level) {
					case 'national':
						/*$html .= '<td><a class="btn btn-primary" id="summary_modal_details" data-level = "'.$value['level'].'" data-level-id = "'.$value['level_id'].'" data-commodity-id = "'.$commodity_id.'" data-modal-level="county">View Details</a></td>';*/
						// echo "BUTTON LEVEL: COUNTY";exit;
						// $html .= '<td><a class="btn btn-primary" id="summary_modal_details" data-level = "county" data-level-id = "'.$data['county_drawing_totals'][1]['county_id'].'" data-modal-level="county">View Details</a></td>';
						break;

					case 'county':
						/*$html .= '<td><a class="btn btn-primary" id="summary_modal_details" data-level = "'.$value['level'].'" data-level-id = "'.$value['level_id'].'" data-commodity-id = "'.$commodity_id.'" data-modal-level="county">View Details</a></td>';*/
						// echo "BUTTON LEVEL: COUNTY";exit;
						$html .= '<td><a class="btn btn-primary" id="summary_modal_details" data-level = "'.$value['level'].'" data-level-id = "'.$value['district_id'].'" data-commodity-id = "'.$commodity_id.'" data-order-date="'.date('mY',strtotime($value['order_date'])).'" data-modal-level="subcounty">View Details</a></td>';
						break;

					case 'subcounty':
						/*$html .= '<td><a class="btn btn-primary" id="summary_modal_details" data-level = "'.$value['level'].'" data-level-id = "'.$value['level_id'].'" data-commodity-id = "'.$commodity_id.'" data-modal-level="subcounty">View Details</a></td>';*/

						// echo "BUTTON LEVEL: SUBCOUNTY";exit;
						// $html .= '<td><a class="btn btn-primary" id="summary_modal_details" data-level = "'.$value['level'].'" data-level-id = "'.$value['level_id'].'" data-commodity-id = "'.$commodity_id.'" data-modal-level="facility">View FCDRR</a></td>';
						
						// $html .= '<td><a class="btn btn-primary" id="summary_modal_details" data-level = "'.$value['level'].'" data-level-id = "'.$value['facility_code'].'" data-commodity-id = "'.$commodity_id.'" data-order-id = "'.$value['order_id'].'"  data-modal-level="facility">View FCDRR</a></td>';

						// $html .= '<td><a class="btn btn-primary" data-level = "order" data-level-id = "'.$value['order_id'].'" data-commodity-id = "'.$commodity_id.'" data-order-id = "'.$value['order_id'].'"  data-modal-level="facility">View FCDRR</a></td>';

						$html .= '<td><a class="btn btn-primary" id="summary_modal_details" data-level = "order" data-level-id = "'.$value['order_id'].'" data-commodity-id = "'.$commodity_id.'" data-modal-level="order">View FCDRR</a></td>';

						$back_button = '<a class="btn btn-primary col-md-3 pull-left" id="summary_modal_details" data-level = "county" data-level-id = "'.$value['county_id'].'" data-commodity-id = "'.$commodity_id.'" data-modal-level="county" style="width: 250px;margin: 5px 0px;">Back to Subounty List</a>';

						break;

					case 'facility':
						// echo "BUTTON LEVEL: FACILITY";exit;
						$html .= '<td><a class="btn btn-primary" id="summary_modal_details" data-level = "'.$value['level'].'" data-level-id = "'.$value['level_id'].'" data-commodity-id = "'.$commodity_id.'" data-order-id = "'.$value['order_id'].'" data-modal-level="facility">View Orders</a></td>';
						$back_button = '<td><a class="btn btn-primary" id="summary_modal_details" data-level = "subcounty" data-level-id = "'.$value['district_id'].'" data-commodity-id = "'.$commodity_id.'" data-modal-level="subcounty" style="width: 250px;margin: 5px 0px;">Back to Subcounty List</a></td>';
						break;
					
					default:
						// echo "BUTTON LEVEL: COUNTY";exit;
						$html .= '<td><a class="btn btn-primary" id="summary_modal_details" data-level = "'.$value['level'].'" data-level-id = "'.$value['level_id'].'" data-commodity-id = "'.$commodity_id.'" data-order-id = "'.$value['order_id'].'" data-modal-level="county">View Details</a></td>';
						break;
				}

				// $html .= '</tr>';
			
			$html .='<tbody></table>';
		}

		echo $back_button.$top_summary.$html;
		// echo $back_button.$html;
		// echo "<pre>";print_r($back_button.$top_summary.$html);exit;
	}

}//END OF DASHBOARDV2 CLASS
?>