<?php
/*
Richard Karsan 2017
*/
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once ('home_controller.php');

class Allocation_Management extends MY_Controller {
	function __construct() {
        parent::__construct();
        $this->load->library('Excel');
        //$this->load->database();
       ini_set('memory_limit', '-1');
       ini_set('max_input_vars', 3000);

    }

    public function index() {
        echo "blee";
    }
    public function county_reports(){
        $county = $this->session->userdata('county_id'); 
        $user_id = $county;
        $user_type=13;
        require('rtk_management.php');
        $rtk = new rtk_management();
        
        $commodity_id = 4;
        $month = $this->session->userdata('Month');

        if ($month == '') {
            $month = date('mY', time());
        }
            // $month= '042016';
        $year = substr($month, -4);
        $months_texts = array();
        $percentages = array();

        for ($i=11; $i >=0; $i--) { 
            $months =  date("mY", strtotime( date( 'Y-m-01' )." -$i months"));
            // echo $months;exit;
            $j = $i+1;            
            $month_text =  date("M Y", strtotime( date( 'Y-m-01' )." -$j months")); 
            array_push($months_texts,$month_text);
            $sql2 = "select sum(reported) as reported, sum(facilities) as total, month from rtk_county_percentage where month ='$months' and county_id = '$county'";

            $result2 = $this->db->query($sql2)->result_array();            
            foreach ($result2 as $key => $value) {
                $reported = $value['reported'];
                $total = $value['total'];
                $percentage = round(($reported/$total)*100);
                if($percentage>100){
                    $percentage = 100;
                }
                array_push($percentages, $percentage);
                $trend_details[$month] = array('reported'=>$reported,'total'=>$total,'percentage'=>$percentage);
            }
        }
        // echo $month;exit;
        $m = substr($month, 0, 2);
        $y = substr($month, 2);
        $new_month = $y . '-' . $m . '-01';
        $new_month_late = $y . '-' . $m . '-15';
        $new_month_end = $y . '-' . $m . '-31';

        // echo "<pre>";print_r($new_month);exit;

    $sql3 = "select *  from rtk_county_percentage where month ='$month' and county_id = '$county' ";

    // echo "<pre>";print_r($sql3);exit;
    $result3 = $this->db->query($sql3)->result_array();  

    // echo "<pre>";print_r($county_facilities);exit;
    // echo "$sql3";
    // echo "<pre>";print_r($result3);exit;
    
    $total_facilities = $result3[0]['facilities'];
    $reported_facilities=$result3[0]['reported'];
    $nonreported_facilities= $total_facilities - $reported_facilities;
    $data['reported_facilities'] = $reported_facilities;
    $data['reported_facilities_percentage'] = $result3[0]['percentage'];
    $data['total_facilities'] = $total_facilities;
    $data['nonreported_facilities'] =$nonreported_facilities;
    $data['nonreported_facilities_percentage'] = ceil(($nonreported_facilities/$total_facilities)*100);
    // echo '<pre>';print_r($result3);die;

    $sql4 = "SELECT 
                facilities.facility_name,
                facilities.facility_code,
                lab_commodity_orders.order_date
            FROM
                facilities,
                districts,
                counties,
                lab_commodity_orders
            WHERE
                lab_commodity_orders.facility_code = facilities.facility_code
                    AND facilities.district = districts.id
                    AND counties.id = districts.county
                    AND counties.id = $county
                    AND facilities.rtk_enabled = 1
                    AND lab_commodity_orders.order_date BETWEEN '$new_month' AND '$new_month_end'";
    $result4 = $this->db->query($sql4)->result_array();  
    // echo $sql4;exit;
    // echo "<pre>";print_r($result4);exit;
    $county_facilities = $rtk::get_total_facilities_rtk_in_county($county);
    // echo "<pre>";print_r($county_facilities);exit;
    $reported_facility_codes = $unreported_keys = $reported_keys = $reported = $unreported = array();

    foreach ($result4 as $key => $value) {
        $filter['facility_code'] = $value['facility_code'];
        $filter['result4key'] = $key;
        array_push($reported_facility_codes, $filter);
    }

    // echo "<pre>";print_r($reported_facility_codes);exit;

    foreach ($county_facilities as $key => $value) {
        $facility_code = $value['facility_code'];
        // echo $facility_code;exit;
        $found_key = array_search($facility_code, array_column($reported_facility_codes, 'facility_code'));

        if($found_key != FALSE){
            $reported_facility_keys = $reported_facility_codes[$found_key]['result4key'];
            array_push($reported_keys, $reported_facility_keys);
        }else{
            array_push($unreported, $value);

        }
        // $facility_code = $reported_facility_codes[$key][''];
        //echo "<pre>THIS: ".$facility_code." FOUND KEY: ".$found_key;
    }

    // echo "<pre>";print_r($unreported);exit;

    $sql5 = "SELECT 
            count(facilities.facility_code) as late_facilities
        FROM
            facilities,
            districts,
            counties,
            lab_commodity_orders
        WHERE
            lab_commodity_orders.facility_code = facilities.facility_code
                AND facilities.district = districts.id
                AND counties.id = districts.county
                AND counties.id = $county
                AND facilities.rtk_enabled = 1
                AND lab_commodity_orders.order_date BETWEEN '$new_month_late' AND '$new_month_end'";
    $result5 = $this->db->query($sql5)->result_array();  

    $data['reported_facilities_text'] = $result4;
    $data['unreported_facilities_text'] = $unreported;
    $data['late_reported_facilities'] = $result5;
    $data['trend_details'] = json_encode($trend_details);        
    $data['months_texts'] = str_replace('"',"'",json_encode($months_texts));        
    $data['percentages'] = str_replace('"',"'",json_encode($percentages));              
    $data['first_month'] = date("M Y", strtotime( date( 'Y-m-01' )." -12 months")); 
    $data['last_month'] = date("M Y", strtotime( date( 'Y-m-01' )." -1 months")); 
 
        $data['graphdata'] = $rtk->partner_commodity_percentages($user_type,$user_id, $commodity_id);
        $data['filter_months'] = $this->get_months_between_dates();
        // print_r($data['filter_months']);die;
        // echo "<pre>";print_r($data['filter_months']);exit;
        $data['banner_text'] = 'Allocation Reports';
        $data['active_zone'] = "$zone";
        $data['content_view'] = 'rtk/rtk/clc/county_reports';
        $data['title'] = 'County Reports ';       
        $this->load->view("rtk/template", $data);
    }

function cd4_national_reports(){
    require('cd4_management.php');
    $cd4 = new cd4_management();

    $user_type = $this->session->userdata('user_type_id'); 
    $county_id = $this->session->userdata('county_id'); 
    // echo "<pre>";print_r($user_type);exit;
    /*Comment to make commit work*/
    $data['restricted'] = 0;
    if ($county_id > 0 && $user_type == 13) {
        // echo "<pre>";print_r("CAUGHT");exit;
        $data['default_county'] = $county_id;
        $data['user'] = 'CMLT';
        $data['restricted'] = 1;
        $reporting_details = $cd4->cd4_facilities_not_reported(NULL,NULL,$county_id);
        $summary = $this->cd4_report_summary(NULL,NULL,$county_id);
    }else{
        $reporting_details = $cd4->cd4_facilities_not_reported();
        $summary = $this->cd4_report_summary();
    }

    // echo "<pre>";print_r($summary);exit;
    // echo "<pre>";print_r($data);exit;
    // echo "<pre>";print_r($summary);exit;
    $curdate = date('Y-m-d');
    $data['filter_months'] = $this->get_months_between_dates(NULL,$curdate);
    // echo "<pre>";print_r($data['filter_months']);exit;
    // echo "<pre>";print_r($summary);exit;
    $data['reporting_summary'] = $summary;
    // $data['current_month'] = date("M Y", strtotime(date( 'Y-m-01' )." -$j months"));
    $fullmonth = date('mY', time());      
    $month = substr($fullmonth, 0,2);
    $year = substr($fullmonth, -4);

    // echo "<pre>";print_r($reporting_details);exit;
    $data['non_reported_list'] = $reporting_details['new_unreported'];
    $data['reported_list'] = $reporting_details['reported'];
    // echo "<pre> "; print_r($data['trend_details']);
    // die;

    // echo "<pre>";print_r($data);exit;
    $data['banner_text'] = 'CD4 National Summary';
    $data['content_view'] = 'rtk/rtk/admin/cd4_national_reports';
    $data['title'] = 'National Reports';       
    $this->load->view("rtk/template", $data);
}

public function cd4_report_summary($year, $month, $county = NULL, $district=NULL) {
    $year = (isset($year) && $year > 0)?$year:date('Y');
    $month = (isset($month) && $month > 0)?$month:date('m', strtotime('-1 month'));

    // echo "<pre>";print_r($month);exit;
    $distname = districts::get_district_name($district);
    $districtname = $distname[0]['district'];
    $district_id = $district;
    $returnable = array();
    $nonreported;
    $reported_percentage;
    $late_percentage;
    $conditions = '';

    if (isset($district)) {
        $conditions .= ' and districts.id = '.$district;
    }
    if (isset($county)) {
        $conditions .= ' and counties.id = '.$county;
    }

    // Sets the timezone and date variables for last day of previous month and this month
    date_default_timezone_set('EUROPE/moscow');
    $month = $month + 1;
    $prev_month = $month - 1;
    $last_day_current_month = date('Y-m-d', mktime(0, 0, 0, $month, 0, $year));
    $first_day_current_month = date('Y-m-', mktime(0, 0, 0, $month, 0, $year));
    $first_day_current_month .= '01';
    $lastday_thismonth = date('Y-m-d', strtotime("last day of this month"));
    $month -= 1;        
    $day15 = $year . '-' . $month . '-15';
    // $day11 = $year . '-' . $month . '-11';
    // $day12 = $year . '-' . $month . '-12';
    $late_reporting = 0;
    $text_month = date('F', strtotime($day15));
    // $reporting_month = date('F,Y', strtotime('first day of previous month'));
    $reporting_month = date('F,Y', strtotime($day15));

    $q = "SELECT * 
    FROM facilities, districts, counties
    WHERE facilities.district = districts.id
    AND districts.county = counties.id
    $conditions
    AND facilities.cd4_enabled =1
    ORDER BY  `facilities`.`facility_name` ASC ";
    // echo "$q";
    // echo "<pre>";print_r($q);exit;
    $q_res = $this->db->query($q);
    $total_reporting_facilities = $q_res->num_rows();

    $q1 = "SELECT DISTINCT cd4_fcdrr.facility_code, cd4_fcdrr.id,cd4_fcdrr.order_date
    FROM cd4_fcdrr, districts, counties
    WHERE districts.id = cd4_fcdrr.district_id
    AND districts.county = counties.id
    $conditions
    AND cd4_fcdrr.order_date
    BETWEEN '$first_day_current_month'
    AND '$last_day_current_month'
    group by cd4_fcdrr.facility_code";

    // echo "<pre>";print_r($q1);exit;
    $q_res1 = $this->db->query($q1);
    $new_q_res1 = $q_res1 ->result_array();
    $total_reported_facilities = $q_res1->num_rows();


    foreach ($q_res1->result_array() as $vals) {
        if ($vals['order_date'] >$day15 ) {
            $late_reporting += 1;
            //                echo "<pre>";var_dump($vals);echo "</pre>";
        }
    }

    $nonreported = $total_reporting_facilities - $total_reported_facilities;
    if ($total_reporting_facilities == 0) {
        $non_reported_percentage = 0;
    } else {
        $non_reported_percentage = $nonreported / $total_reporting_facilities * 100;
    }

            // $non_reported_percentage = number_format($non_reported_percentage, 0);

    if ($total_reporting_facilities == 0) {
        $reported_percentage = 0;
    } else {
        $reported_percentage = $total_reported_facilities / $total_reporting_facilities * 100;
    }

    // $reported_percentage = number_format($reported_percentage, 0);

    if ($total_reporting_facilities == 0) {
        $late_percentage = 0;
    } else {
        $late_percentage = $late_reporting / $total_reporting_facilities * 100;
    }


    $late_percentage = number_format($late_percentage, 0);
    if ($total_reported_facilities > $total_reporting_facilities) {
        $reported_percentage = 100;
        $nonreported = 0;
        $total_reported_facilities = $total_reporting_facilities;
    }
    if ($late_reporting > $total_reporting_facilities) {
        $late_reporting = $total_reporting_facilities;
        $late_percentage = $reported_percentage;
    }

    $returnable = array(
        'reporting_month'=>$reporting_month,
        'Month' => $text_month, 
        'Year' => $year, 
        'full_date' => date('Y-m-d', strtotime($day15)),
        'district' => $districtname, 
        'county_id' => $county,  
        'district_id' => $district_id,  
        'total_facilities' => $total_reporting_facilities, 
        'reported' => $total_reported_facilities, 
        'reported_percentage' => $reported_percentage, 
        'nonreported' => $nonreported, 
        'nonreported_percentage' => $non_reported_percentage, 
        'late_reports' => $late_reporting, 
        'late_reports_percentage' => $late_percentage
    );

    // echo "<pre>";print_r($returnable);exit;
    return $returnable;
}

public function update_cd4_reporting_percentages($year = NULL, $month=null){
    $year = (isset($year) && $year > 0)?$year:date('Y');
    $month = (isset($month) && $month > 0)?$month:date('m');
    $month = str_pad($month, 2, "0", STR_PAD_LEFT);

    // echo "<pre>";print_r($month);exit;
    $monthyear = $month.$year;
    // echo "<pre>";print_r($monthyear);exit;

    $sql = "SELECT id FROM counties";

    $result = $this->db->query($sql)->result_array();
    $operations = array();

    foreach ($result as $key => $value) {
        $county_id = $value['id'];              

        $reports = $this->cd4_report_summary($year,$month,$county_id);  
        // echo "<pre>";print_r($reports);exit;
        $cd4_reported = $reports['reported']; 
        $cd4_total_facilities = $reports['total_facilities'];        
        $cd4_percentage = ceil(($cd4_reported/$cd4_total_facilities)*100);
        
        // echo "<pre>"; print_r($reports);
        $percentage_data['county_id'] = $county_id;
        $percentage_data['cd4_facilities'] = $cd4_total_facilities;
        $percentage_data['cd4_reported'] = $cd4_reported;
        $percentage_data['cd4_percentage'] = $cd4_percentage;
        $percentage_data['reported_month'] = $monthyear;

        $q = $this->db->where('county_id',$county_id);
        $q = $this->db->where('reported_month',$monthyear);
        $q = $this->db->get('cd4_county_percentage')->result_array();

        // echo "<pre>";print_r($q);exit;
        if (count($q)) {
            $update = $this->db->where('county_id',$county_id);
            $update = $this->db->update('cd4_county_percentage',$percentage_data);
            // echo "<pre>";print_r("UPDATE: ".$update);exit;
            // echo "<pre>";print_r($update);exit;
            $operation['update'][] = $percentage_data; 
        } else {
            $insert = $this->db->insert('cd4_county_percentage',$percentage_data);
            // echo "<pre>";print_r("INSERT: ".$insert);exit;
            $operation['insert'][] = $percentage_data; 
        }


        /*$q = "insert into cd4_county_percentage (county_id, cd4_facilities,cd4_reported,cd4_percentage,reported_month) values ($county_id,$cd4_total_facilities,$cd4_reported,$cd4_percentage,'$monthyear')";

        $this->db->query($q);*/
        // echo "<pre>";print_r('SUCCESS');
    }
        echo "<pre>";print_r($operation);exit;
}
 
function get_cd4_stock_card2($commodity, $fullmonth =NULL, $county=NULL){
    if (!isset($fullmonth)) {
       $fullmonth = date('mY');
    }

    if (!isset($commodity) && $commodity < 1) {
       $commodity = 1;
    }

    $conditions ='';
     if (isset($county) && $county > 0) {
       $conditions = ' and counties.id = '.$county;
    }

    $month = substr($fullmonth, 0,2);
    $year = substr($fullmonth, -4);

    $firstdate = $year . '-' . $month . '-01';
    $num_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
    $lastdate = $year . '-' . $month . '-' . $num_days;

    $sql = "SELECT 
            EXTRACT(YEAR_MONTH FROM cd4_fcdrr_commodities.created_at) AS current_month,
            EXTRACT(MONTH FROM cd4_fcdrr_commodities.created_at) AS month,
            EXTRACT(YEAR FROM cd4_fcdrr_commodities.created_at) AS year,
            cd4_fcdrr_commodities.commodity_id,
            cd4_commodities.commodity_name,
            cd4_lab_commodity_categories.name as type,
            SUM(cd4_fcdrr_commodities.q_requested) AS q_requested,
            SUM(cd4_fcdrr_commodities.beginning_bal) AS beginning_bal,
            SUM(cd4_fcdrr_commodities.q_received) AS q_received,
            SUM(cd4_fcdrr_commodities.q_used) AS q_used,
            SUM(cd4_fcdrr_commodities.no_of_tests_done) AS no_of_tests_done,
            SUM(cd4_fcdrr_commodities.positive_adj) AS positive_adj,
            SUM(cd4_fcdrr_commodities.negative_adj) AS negative_adj,
            SUM(cd4_fcdrr_commodities.losses) AS losses,
            SUM(cd4_fcdrr_commodities.closing_stock) AS closing_stock
        FROM
            districts,
            counties,
            facilities,
            cd4_fcdrr_commodities,
            cd4_commodities, cd4_lab_commodity_categories
        WHERE
            facilities.district = districts.id
                AND districts.county = counties.id
                AND cd4_fcdrr_commodities.facility_code = facilities.facility_code
                AND cd4_fcdrr_commodities.commodity_id = cd4_commodities.id
                AND cd4_commodities.category = cd4_lab_commodity_categories.id
                and cd4_lab_commodity_categories.id = '$commodity'
                $conditions
                AND DATE(cd4_fcdrr_commodities.created_at) BETWEEN date('$firstdate') AND date('$lastdate')
                group by cd4_commodities.id";
    // echo $sql;
        // echo "<pre>";print_r($sql);exit;
        $result = $this->db->query($sql);
    if ($result) {
        $result = $result->result_array();
    } else {
        $result = NULL;
    }
    
    // echo "<pre>";print_r($result);exit;
    

    foreach ($result as $key => $value) {
       $type = $value['type'];
       $month = $value['month'];
       $year = $value['year'];
       $commodity_name = $value['commodity_name'];
       $beginning_bal = $value['beginning_bal'];
       $q_received = $value['q_received'];
       $q_used = $value['q_used'];
       $no_of_tests_done = $value['no_of_tests_done'];
       $positive_adj = $value['positive_adj'];
       $negative_adj = $value['negative_adj'];
       $losses = $value['losses'];
       $q_requested = $value['q_requested'];
       $closing_stock = $value['closing_stock'];

       $output[] = array(
            $type, 
            $commodity_name,
            $beginning_bal, 
            $q_received, 
            $q_used, 
            $no_of_tests_done, 
            $positive_adj, 
            $negative_adj,
            $losses,
            $closing_stock,
            $q_requested);
    }

    echo json_encode($output);
    // return $result;

}

  
function get_cd4_reporting_summary($fullmonth, $county = null){
     require('cd4_management.php');
    $cd4 = new cd4_management();

    if (!isset($fullmonth)) {     
    
        $fullmonth = date('mY', time());              
    }

    $month = substr($fullmonth, 0,2);
    $year = substr($fullmonth, -4);

    $reporting_summary = $cd4->cd4_reports_summary($year,$month,$county);

    echo json_encode($reporting_summary);
}
function get_cd4_reporting_facility_details($fullmonth, $county=null){
    require('cd4_management.php');
    $cd4 = new cd4_management();

    if (!isset($fullmonth)) {     
        $fullmonth = date('mY', time());              
    }

    $month = substr($fullmonth, 0,2);
    $year = substr($fullmonth, -4);

    // echo "<pre>";print_r($month);
    // echo "<pre>";print_r($year);exit;
    $reporting_details = $cd4->cd4_facilities_not_reported($month, $year, $county);
    // echo "<pre>";print_r($reporting_details);exit;
    $reporting_list = $reporting_details['reported'];
    $nonreporting_list = $reporting_details['new_unreported'];

    foreach ($reporting_list as $key => $value) {     

        $r_county = $value['county'];
        $r_district = $value['district'];
        $r_facilty_code = $value['facility_code'];
        $r_facilty_name = $value['facility_name'];
        $link = $value['order_id'];
        // $link = '<a href="'.base_url().'cd4_management/fcdrr_details/'.$value['order_id'].'"> View FCDRR</a>';

        $reported [] = array($r_county, $r_district, $r_facilty_code,$r_facilty_name, $link);

        // echo $link;
    }

    foreach ($nonreporting_list as $key => $value) {
         

        $nr_county = $value['county'];
        $nr_district = $value['district'];
        $nr_facilty_code = $value['facility_code'];
        $nr_facilty_name = $value['facility_name'];

        $nonreported [] = array($nr_county, $nr_district, $nr_facilty_code,$nr_facilty_name);
    }

    $full_date = $year.'-'.$month.'-01';
    $date_text = date("F Y", strtotime( $full_date." -1 MONTHS"));

    $array_all = array(
        'month'=>$month,
        'year'=>$year,
        'date_text' => $date_text,
        'nonreported' => $nonreported, 
        'reported'=>$reported
    );
    echo json_encode($array_all);
}
  function get_cd4_reporting_percentage2($county = NULL){

    $sql = "select * from cd4_county_percentage";
    $result = $this->db->query($sql)->result_array();

    $months_texts = array();
    $percentages = array();
    $cd4_trend_details = array();

    $conditions = '';
    $county_name = '';
    if (isset($county) && $county > 0) {
        $conditions= ' and county_id = '.$county;
        $county_details = $this->db->query("SELECT county FROM counties WHERE id = $county")->result_array();
        $county_name = ": ".$county_details[0]['county']." County";

        // echo "<pre>";print_r($county_details);exit;
    }

    for ($i=36; $i >=0; $i--) { 
            $month =  date("mY", strtotime( date( 'Y-m-01' )." -$i months"));
            $j = $i+1;            
            $month_text =  date("M Y", strtotime( date( 'Y-m-01' )." -$j months")); 
            array_push($months_texts,$month_text);
            /*$sql = "select sum(cd4_reported) as reported, sum(cd4_facilities) as total, reported_month from cd4_county_percentage 
                where reported_month ='$month' $conditions";*/
            $sql = "
                SELECT 
                    SUM(cd4_reported) AS reported,
                    SUM(cd4_facilities) AS total,
                    reported_month
                FROM
                    cd4_county_percentage
                WHERE
                    reported_month = '$month' $conditions
                ";
            // echo "<pre>";print_r($sql);exit;
            $result = $this->db->query($sql)->result_array();            
            
            foreach ($result as $key => $value) {
                $reported = $value['reported'];
                $total = $value['total'];
                $percentage = round(($reported/$total)*100);
                if($percentage>100){
                    $percentage = 100;
                }
                array_push($percentages, $percentage);
            }
    } 

        // echo "<pre>";print_r($percentages);exit;
    // $months_texts = str_replace('"',"'",json_encode($months_texts));        
    // $percentages= str_replace('"',"'",json_encode($percentages)); 

    $cd4_trend_details = array('county_name'=>$county_name,'months_texts'=>$months_texts,'percentages'=>$percentages);

    // echo "<pre>";print_r($cd4_trend_details);exit;

    echo json_encode($cd4_trend_details);
    // echo $months_texts; 
    // return $cd4_trend_details;
  }
  
function national_reports(){
    $user_type = $this->session->userdata('user_type_id'); 

    require('rtk_management.php');
    $rtk = new rtk_management();

    $commodity_id = 4; // used in graph data
        
    $fullmonth = date('mY', time());
      
   //          // $month= '042016';
    $month = substr($fullmonth, 0,2);
    $year = substr($fullmonth, -4);
    $num_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
    $first_date = $year . '-' . $month . '-01';    
    $last_date = $year . '-' . $month .'-'. $num_days;      

    // $countysd = 1;
     
        // $data['county_summary'] = $rtk->_requested_vs_allocated($year, $month, $countysd); 
        // $data['graphdata'] = $rtk->partner_commodity_percentages($user_id,$user_type, $commodity_id);
        // echo "yes";
        // print_r($data['county_summary']);die;
        

        $months_texts = array();
        $percentages = array();

        for ($i=11; $i >=0; $i--) { 
            $month =  date("mY", strtotime( date( 'Y-m-01' )." -$i months"));
            $j = $i+1;            
            $month_text =  date("M Y", strtotime( date( 'Y-m-01' )." -$j months")); 
            array_push($months_texts,$month_text);
            $sql = "select sum(reported) as reported, sum(facilities) as total, month from rtk_county_percentage 
                where month ='$month'";

            $res_trend = $this->db->query($sql)->result_array();            
            foreach ($res_trend as $key => $value) {
                $reported = $value['reported'];
                $total = $value['total'];
                $percentage = round(($reported/$total)*100);
                if($percentage>100){
                    $percentage = 100;
                }
                array_push($percentages, $percentage);
                $trend_details[$month] = array('reported'=>$reported,'total'=>$total,'percentage'=>$percentage);
            }
        }   


        $data['trend_details'] = json_encode($trend_details);        
        $data['months_texts'] = str_replace('"',"'",json_encode($months_texts));        
        $data['percentages'] = str_replace('"',"'",json_encode($percentages)); 
        $data['first_month'] = date("M Y", strtotime( date( 'Y-m-01' )." -12 months")); 
        $data['last_month'] = date("M Y", strtotime( date( 'Y-m-01' )." -1 months")); 
       // print_r($data['percentages']);die;
        
        $data['banner_text'] = 'RTK National Summary';
        $data['content_view'] = 'rtk/rtk/admin/national_reports';
        $data['title'] = 'National Reports';       
        $this->load->view("rtk/template", $data);
    
    }
    function get_county_reporting_trend($county_id, $district_id){
      $month = $this->session->userdata('Month');

    if ($month == '') {
        $month = date('mY', time());
    }
    $year = substr($month, -4);
    $months_texts = array();
    $percentages = array();

    for ($i=11; $i >=0; $i--) { 
        $months =  date("mY", strtotime( date( 'Y-m-01' )." -$i months"));
        $j = $i+1;            
        $month_text =  date("M Y", strtotime( date( 'Y-m-01' )." -$j months")); 
        array_push($months_texts,$month_text);
        $sql2 = "select sum(reported) as reported, sum(facilities) as total, month from rtk_county_percentage where month ='$months' and county_id = '$county_id'";
// echo "$sql2";
        $result2 = $this->db->query($sql2)->result_array();            
        // print_r($result2);die;
        foreach ($result2 as $key => $value) {
            $reported = $value['reported'];
            $total = $value['total'];
            $percentage = round(($reported/$total)*100);
            if($percentage>100){
                $percentage = 100;
            }
            array_push($percentages, $percentage);
            $trend_details[$month] = array('reported'=>$reported,'total'=>$total,'percentage'=>$percentage,'months_texts'=>$months_texts);
        }
    }

    $output =  json_encode($trend_details);        

    }
    //get amcs from facility_amc table and facility details then insert into allocation table
    function get_amcs(){
        //get facility's details
        $sql = "SELECT 
                    counties.county,
                    counties.id as county_id,
                    districts.district,
                    districts.id as district_id,
                    facilities.facility_code,
                    facilities.facility_name,
                    facilities.zone                   
                FROM
                    facilities,
                    counties,
                    districts
                WHERE
                    counties.id = districts.county
                        AND districts.id = facilities.district limit 0,100";
       $result = $this->db->query($sql)->result_array();

       //get the amcs of all four commodities for each facility
       foreach ($result as $key => $value) {
           $facility_code = $value['facility_code'];

           $sql2 = "Select amc, commodity_id, latest from facility_amc where facility_code = '$facility_code'";
           $result2 = $this->db->query($sql2)->result_array();
       
            $county_id = $value['county_id'];
            $county = $value['county'];
            $district_id = $value['district_id'];
            $district = $value['district'];
            $zone = $value['zone'];
            $oldfacility_name = $value['facility_name'];
            $facility_name = str_replace("'", "", $oldfacility_name);
            $amc_s3 = $result2[0]['amc'];
            $amc_c3 = $result2[1]['amc'];
            $amc_t3 = $result2[2]['amc'];
            $amc_d3 = 0; 

            $month = $result2[0]['latest'];
            if (empty($month)) {
                $newmonth = '';

            }else{
                $monthnumber = date("m", strtotime($month));
                $newyear = date("Y", strtotime($month));
                $newmonth = $monthnumber.$newyear;
            }
            //insert the details into allocation details          
            $sql3 = "INSERT INTO `allocation_details`
                    (`county_id`, `county`, `district_id`, `district`, `mfl`, `facility_name`, `zone`, `amc_s`, `amc_t`, `amc_c`, `amc_d`,`month`) 
                    VALUES ('$county_id','$county','district_id','$district','$facility_code','$facility_name','$zone',
                    '$amc_s3','$amc_t3','$amc_c3','$amc_d3','$month')";
        
            $this->db->query($sql3);
       }
                   
    }
    public function allocation_reports(){
        
 
        $data['banner_text'] = 'Allocation Reports';
        $data['active_zone'] = "$zone";
        $data['content_view'] = 'rtk/rtk/allocation/allocation_reports';
        $data['title'] = 'Download Allocation Reports ';       
        $this->load->view("rtk/template", $data);
    }
function get_counties($county_id = NULL){
        $and = (isset($county_id) && $county_id > 0)? " WHERE counties.id = $county_id" :NULL;
        $sql = "select counties.id as county_id, counties.county from counties $and order by county asc";
        // echo "<pre>";print_r($sql);exit;
        $counties = $this->db->query($sql)->result_array();

        $option_county .= '<option value = "">Select County</option>';
        foreach ($counties as $key => $value) {
            $option_county .= '<option value = "' . $value['county_id'] . '">' . $value['county'] . '</option>';
        }    
        $output = array('counties_list'=>$option_county);
        echo json_encode($output);

    }
    function get_cd4_commodities(){
        $sql = 'select * from cd4_lab_commodity_categories';
        $comm = $this->db->query($sql)->result_array();
        // $option_comm .= '<option value = "0">--Select Commodity--</option>';
       foreach ($comm as $key => $value) {
            $option_comm .= '<option value = "' . $value['id'] . '">' . $value['name'] . '</option>';
        } 
        $output = array('cd4_commodities'=>$option_comm);
        echo json_encode($output);
    }
    function get_districts(){
        $sql2 = 'select districts.id as district_id, districts.district from districts order by district asc';
        $districts = $this->db->query($sql2)->result_array();
        $option_district .= '<option value = "0">--Select Sub-County--</option>';
       foreach ($districts as $key => $value) {
            $option_district .= '<option value = "' . $value['district_id'] . '">' . $value['district'] . '</option>';
        } 
        $output = array('district_list'=>$option_district);
        echo json_encode($output);
    }
    function get_counties_districts($county){
     
        // $county = $this->session->userdata('county_id');   
        $sql = 'select counties.id as county_id, counties.county from counties';
        $sql2 = 'select districts.id as district_id, districts.district from districts';
        $sql3 = 'select districts.id as district_id, districts.district from districts where county = '.$county;
        $counties = $this->db->query($sql)->result_array();
        $districts = $this->db->query($sql2)->result_array();
        $districts_county = $this->db->query($sql3)->result_array();
       
        // echo('<pre>'); print_r($districts);die;
       $option_district .= '<option value = "">--Select Sub-County--</option>';
       foreach ($districts as $key => $value) {
            $option_district .= '<option value = "' . $value['district_id'] . '">' . $value['district'] . '</option>';
        } 
        $option_district_county .= '<option value = "0">--All Sub-Counties--</option>';
       foreach ($districts_county as $key => $value) {
            $option_district_county .= '<option value = "' . $value['district_id'] . '">' . $value['district'] . '</option>';
        } 

        $option_county .= '<option value = "">--Select County</option>';
        foreach ($counties as $key => $value) {
            $option_county .= '<option value = "' . $value['county_id'] . '">' . $value['county'] . '</option>';
        } 

          $month = $this->session->userdata('Month');

    if ($month == '') {
        $month = date('mY', time());
    }
    $year = substr($month, -4);
    $months_texts = array();
    $percentages = array();

    for ($i=11; $i >=0; $i--) { 
        $months =  date("mY", strtotime( date( 'Y-m-01' )." -$i months"));
        $j = $i+1;            
        $month_text =  date("M Y", strtotime( date( 'Y-m-01' )." -$j months")); 
        array_push($months_texts,$month_text);
        $sql2 = "select sum(reported) as reported, sum(facilities) as total, month from rtk_county_percentage where month ='$months' and county_id = '$county'";
// echo "$sql2";
        $result2 = $this->db->query($sql2)->result_array();            
        // print_r($result2);die;
        foreach ($result2 as $key => $value) {
            $reported = $value['reported'];
            $total = $value['total'];
            $percentage = round(($reported/$total)*100);
            if($percentage>100){
                $percentage = 100;
            }
            array_push($percentages, $percentage);
            $trend_details[$month] = array('reported'=>$reported,'total'=>$total,'percentage'=>$percentage,'months_texts'=>$months_texts);
        }
    }
        // print_r($trend_details);die;
        $output = array('counties_list'=>$option_county,'districts_list'=>$option_district,'district_county_list'=>$option_district_county, 'trend_details'=>$trend_details);  
        echo json_encode($output);
        
    }


    //get allocation details from all facilities in the country
    function get_all_facilities_amcs(){
        //get all the details from allocation_details table in db
    $sql = "SELECT 
                counties.county  as county_name, districts.district as district_name, allocation_details.*
            FROM
                allocation_details,
                facilities,
                counties,
                districts
            WHERE
                district_id = '$district'
                    AND districts.county = counties.id
                    AND facilities.district = districts.id
                    AND facilities.facility_code = allocation_details.facility_code_name limit 0,100";
    $result = $this->db->query($sql)->result_array();
    
    foreach ($result as $key => $id_details) {
        
        $facility_code = $id_details['mfl'];
        // for each selected facility, get the last reported lab details fro all four commodities
        $sql2 = "SELECT 
				    closing_stock, days_out_of_stock, q_requested
				FROM
				    lab_commodity_details AS a
				WHERE
				    facility_code = '$facility_code'
				        AND commodity_id between 4 and 7
				        AND created_at IN (SELECT 
				            MAX(created_at)
				        FROM
				            lab_commodity_details AS b
				        WHERE
				            a.facility_code = b.facility_code)";
        $result2 = $this->db->query($sql2)->result_array();

        $county = $id_details['county_name'];
        $district = $id_details['district_name'];
        $facility_name = $id_details['facility_name'];
        $amc_s3 = $id_details['amc_s3'];
        $amc_c3 = $id_details['amc_c3'];
        $amc_t3 = $id_details['amc_t3'];
        $amc_d3 = 0;

        $ending_bal_s3 = $result2[0]['closing_stock'];
        $q_requested_s3 = $result2[0]['q_requested'];
        $days_out_of_stock_s3 = $result2[0]['days_out_of_stock'];

        $ending_bal_c3 = $result2[1]['closing_stock'];
        $q_requested_c3 = $result2[1]['q_requested'];
        $days_out_of_stock_c3 = $result2[1]['days_out_of_stock'];

        $ending_bal_t3 = $result2[2]['closing_stock'];
        $q_requested_t3 = $result2[2]['q_requested'];
        $days_out_of_stock_t3 = $result2[2]['days_out_of_stock'];

        $ending_bal_d3 = $result2[3]['closing_stock'];
        $q_requested_d3 = $result2[3]['q_requested'];
        $days_out_of_stock_d3 = $result2[3]['days_out_of_stock'];

        //based on the results, put them in an array to beused in the excell file.

        $alocation_details[] = array($county,$district,$facility_code,$facility_name,
                                     $ending_bal_s3,$amc_s3,$days_out_of_stock_s3,$q_requested_s3,
                                     $ending_bal_c3,$amc_c3,$days_out_of_stock_c3,$q_requested_c3,
                                     $ending_bal_t3,$amc_t3,$days_out_of_stock_t3,$q_requested_t3,
                                     $ending_bal_d3,$amc_d3,$days_out_of_stock_d3,$q_requested_d3);
}
        // echo"<pre>";print_r($alocation_details);
    
         $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Counties');
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', 'Commodity Consumption Data (in Tests)');
        $this->excel->getActiveSheet()->setCellValue('A4', 'County');
        $this->excel->getActiveSheet()->setCellValue('B4', 'Sub-County');
        $this->excel->getActiveSheet()->setCellValue('C4', 'MFL Code');
        $this->excel->getActiveSheet()->setCellValue('D4', 'Facility Name');
        $this->excel->getActiveSheet()->setCellValue('E4', 'Screening KHB');
        $this->excel->getActiveSheet()->setCellValue('I4', 'Confirmatory - First Response');
        $this->excel->getActiveSheet()->setCellValue('M4', 'Tie Breaker');
        $this->excel->getActiveSheet()->setCellValue('Q4', 'DBS Bundles');

        $this->excel->getActiveSheet()->setCellValue('A5', '');
        $this->excel->getActiveSheet()->setCellValue('B5', '');
        $this->excel->getActiveSheet()->setCellValue('C5', '');
        $this->excel->getActiveSheet()->setCellValue('D5', '');

        $this->excel->getActiveSheet()->setCellValue('E5', 'Ending Balance');
        $this->excel->getActiveSheet()->setCellValue('F5', 'AMC');
        $this->excel->getActiveSheet()->setCellValue('G5', 'Days Out of Stock');
        $this->excel->getActiveSheet()->setCellValue('H5', 'Quantity Requested');

        $this->excel->getActiveSheet()->setCellValue('I5', 'Ending Balance');
        $this->excel->getActiveSheet()->setCellValue('J5', 'AMC');
        $this->excel->getActiveSheet()->setCellValue('K5', 'Days Out of Stock');
        $this->excel->getActiveSheet()->setCellValue('L5', 'Quantity Requested');

        $this->excel->getActiveSheet()->setCellValue('M5', 'Ending Balance');
        $this->excel->getActiveSheet()->setCellValue('N5', 'AMC');
        $this->excel->getActiveSheet()->setCellValue('O5', 'Days Out of Stock');
        $this->excel->getActiveSheet()->setCellValue('P5', 'Quantity Requested');
        
        $this->excel->getActiveSheet()->setCellValue('Q5', 'Ending Balance');
        $this->excel->getActiveSheet()->setCellValue('R5', 'AMC');
        $this->excel->getActiveSheet()->setCellValue('S5', 'Days Out of Stock');
        $this->excel->getActiveSheet()->setCellValue('T5', 'Quantity Requested');
        //merge cell A1 until C1
        $this->excel->getActiveSheet()->mergeCells('A1:K1');
        $this->excel->getActiveSheet()->mergeCells('E4:H4');
        $this->excel->getActiveSheet()->mergeCells('I4:L4');
        $this->excel->getActiveSheet()->mergeCells('M4:P4');
        $this->excel->getActiveSheet()->mergeCells('Q4:T4');
        //set aligment to center for that merged cell (A1 to C1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('I4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('M4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('Q4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A4:T4')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A5:T5')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');

       for($col = ord('A'); $col <= ord('Q'); $col++){
                //set column dimension
                $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(false);
                 //change the font size
                $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
        }
                            
        foreach ($alocation_details as $row){
                $exceldata[] = $row;
        }
             //Fill data 
                $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A6');
                $this->excel->getActiveSheet()->getStyle('C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                 
                $filename='National Allocation (all facilities).xls'; //save our workbook as this file name
                header('Content-Type: application/vnd.ms-excel'); //mime type
                header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
 
                //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                //if you want to save it as .XLSX Excel 2007 format
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
                //force user to download the Excel file without writing it to server's HD
                $objWriter->save('php://output');
    
    }
    function get_zonal_facilities_amcs($zone){
            //get all the details from allocation_details for a particular zone in db
        $sql = "SELECT 
                counties.county  as county_name, districts.district as district_name, allocation_details.*
            FROM
                allocation_details,
                facilities,
                counties,
                districts
            WHERE
                district_id = '$district'
                    AND districts.county = counties.id
                    AND facilities.district = districts.id
                    AND facilities.facility_code = allocation_details.facility_code and counties.zone = '$zone'";
        $result = $this->db->query($sql)->result_array();
    
    
    foreach ($result as $key => $id_details) {
        
        $facility_code = $id_details['mfl'];
        // for each selected facility, get the last reported lab details fro all four commodities
        $sql2 = "SELECT 
                    closing_stock, days_out_of_stock, q_requested
                FROM
                    lab_commodity_details AS a
                WHERE
                    facility_code = '$facility_code'
                        AND commodity_id between 4 and 7
                        AND created_at IN (SELECT 
                            MAX(created_at)
                        FROM
                            lab_commodity_details AS b
                        WHERE
                            a.facility_code = b.facility_code)";
        $result2 = $this->db->query($sql2)->result_array();

        $county = $id_details['county'];
        $district = $id_details['district'];
        $facility_name = $id_details['facility_name'];
        $amc_s3 = $id_details['amc_s3'];
        $amc_c3 = $id_details['amc_c3'];
        $amc_t3 = $id_details['amc_t3'];
        $amc_d3 = 0;

        $ending_bal_s3 = $result2[0]['closing_stock'];
        $q_requested_s3 = $result2[0]['q_requested'];
        $days_out_of_stock_s3 = $result2[0]['days_out_of_stock'];

        $ending_bal_c3 = $result2[1]['closing_stock'];
        $q_requested_c3 = $result2[1]['q_requested'];
        $days_out_of_stock_c3 = $result2[1]['days_out_of_stock'];

        $ending_bal_t3 = $result2[2]['closing_stock'];
        $q_requested_t3 = $result2[2]['q_requested'];
        $days_out_of_stock_t3 = $result2[2]['days_out_of_stock'];

        $ending_bal_d3 = $result2[3]['closing_stock'];
        $q_requested_d3 = $result2[3]['q_requested'];
        $days_out_of_stock_d3 = $result2[3]['days_out_of_stock'];

        //based on the results, put them in an array to beused in the excell file.

        $alocation_details[] = array($county,$district,$facility_code,$facility_name,
                                     $ending_bal_s3,$amc_s3,$days_out_of_stock_s3,$q_requested_s3,
                                     $ending_bal_c3,$amc_c3,$days_out_of_stock_c3,$q_requested_c3,
                                     $ending_bal_t3,$amc_t3,$days_out_of_stock_t3,$q_requested_t3,
                                     $ending_bal_d3,$amc_d3,$days_out_of_stock_d3,$q_requested_d3);
}
        // echo"<pre>";print_r($alocation_details);
    
         $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Counties');
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', 'Commodity Consumption Data (in Tests)');
        $this->excel->getActiveSheet()->setCellValue('A4', 'County');
        $this->excel->getActiveSheet()->setCellValue('B4', 'Sub-County');
        $this->excel->getActiveSheet()->setCellValue('C4', 'MFL Code');
        $this->excel->getActiveSheet()->setCellValue('D4', 'Facility Name');
        $this->excel->getActiveSheet()->setCellValue('E4', 'Screening KHB');
        $this->excel->getActiveSheet()->setCellValue('I4', 'Confirmatory - First Response');
        $this->excel->getActiveSheet()->setCellValue('M4', 'Tie Breaker');
        $this->excel->getActiveSheet()->setCellValue('Q4', 'DBS Bundles');

        $this->excel->getActiveSheet()->setCellValue('A5', '');
        $this->excel->getActiveSheet()->setCellValue('B5', '');
        $this->excel->getActiveSheet()->setCellValue('C5', '');
        $this->excel->getActiveSheet()->setCellValue('D5', '');

        $this->excel->getActiveSheet()->setCellValue('E5', 'Ending Balance');
        $this->excel->getActiveSheet()->setCellValue('F5', 'AMC');
        $this->excel->getActiveSheet()->setCellValue('G5', 'Days Out of Stock');
        $this->excel->getActiveSheet()->setCellValue('H5', 'Quantity Requested');

        $this->excel->getActiveSheet()->setCellValue('I5', 'Ending Balance');
        $this->excel->getActiveSheet()->setCellValue('J5', 'AMC');
        $this->excel->getActiveSheet()->setCellValue('K5', 'Days Out of Stock');
        $this->excel->getActiveSheet()->setCellValue('L5', 'Quantity Requested');

        $this->excel->getActiveSheet()->setCellValue('M5', 'Ending Balance');
        $this->excel->getActiveSheet()->setCellValue('N5', 'AMC');
        $this->excel->getActiveSheet()->setCellValue('O5', 'Days Out of Stock');
        $this->excel->getActiveSheet()->setCellValue('P5', 'Quantity Requested');
        
        $this->excel->getActiveSheet()->setCellValue('Q5', 'Ending Balance');
        $this->excel->getActiveSheet()->setCellValue('R5', 'AMC');
        $this->excel->getActiveSheet()->setCellValue('S5', 'Days Out of Stock');
        $this->excel->getActiveSheet()->setCellValue('T5', 'Quantity Requested');
        //merge cell A1 until C1
        $this->excel->getActiveSheet()->mergeCells('A1:K1');
        $this->excel->getActiveSheet()->mergeCells('E4:H4');
        $this->excel->getActiveSheet()->mergeCells('I4:L4');
        $this->excel->getActiveSheet()->mergeCells('M4:P4');
        $this->excel->getActiveSheet()->mergeCells('Q4:T4');
        //set aligment to center for that merged cell (A1 to C1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('I4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('M4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('Q4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A4:T4')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A5:T5')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');

       for($col = ord('A'); $col <= ord('Q'); $col++){
                //set column dimension
                $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(false);
                 //change the font size
                $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
        }
                            
        foreach ($alocation_details as $row){
                $exceldata[] = $row;
        }
              //Fill data 
                $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A6');
                $this->excel->getActiveSheet()->getStyle('C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                 
                $filename='Zonal Allocation (Zone '.$zone.').xls'; //save our workbook as this file name
                header('Content-Type: application/vnd.ms-excel'); //mime type
                header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
 
                //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                //if you want to save it as .XLSX Excel 2007 format
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
                //force user to download the Excel file without writing it to server's HD
                $objWriter->save('php://output');        

    }
    function get_county_facilities_amcs($county){
        //get all the details from allocation_details for a particular county in db
    $sql = "SELECT * FROM allocation_details where county_id = '$county'";
    $result = $this->db->query($sql)->result_array();
        
    foreach ($result as $key => $id_details) {
        
        $facility_code = $id_details['mfl'];
        // for each selected facility, get the last reported lab details fro all four commodities
        $sql2 = "SELECT 
                    closing_stock, days_out_of_stock, q_requested
                FROM
                    lab_commodity_details AS a
                WHERE
                    facility_code = '$facility_code'
                        AND commodity_id between 4 and 7
                        AND created_at IN (SELECT 
                            MAX(created_at)
                        FROM
                            lab_commodity_details AS b
                        WHERE
                            a.facility_code = b.facility_code)";
        $result2 = $this->db->query($sql2)->result_array();

        $county = $id_details['county'];
        $district = $id_details['district'];
        $facility_name = $id_details['facility_name'];
        $amc_s3 = $id_details['amc_s3'];
        $amc_c3 = $id_details['amc_c3'];
        $amc_t3 = $id_details['amc_t3'];
        $amc_d3 = 0;

        $ending_bal_s3 = $result2[0]['closing_stock'];
        $q_requested_s3 = $result2[0]['q_requested'];
        $days_out_of_stock_s3 = $result2[0]['days_out_of_stock'];

        $ending_bal_c3 = $result2[1]['closing_stock'];
        $q_requested_c3 = $result2[1]['q_requested'];
        $days_out_of_stock_c3 = $result2[1]['days_out_of_stock'];

        $ending_bal_t3 = $result2[2]['closing_stock'];
        $q_requested_t3 = $result2[2]['q_requested'];
        $days_out_of_stock_t3 = $result2[2]['days_out_of_stock'];

        $ending_bal_d3 = $result2[3]['closing_stock'];
        $q_requested_d3 = $result2[3]['q_requested'];
        $days_out_of_stock_d3 = $result2[3]['days_out_of_stock'];

        //based on the results, put them in an array to beused in the excell file.

        $alocation_details[] = array($county,$district,$facility_code,$facility_name,
                                     $ending_bal_s3,$amc_s3,$days_out_of_stock_s3,$q_requested_s3,
                                     $ending_bal_c3,$amc_c3,$days_out_of_stock_c3,$q_requested_c3,
                                     $ending_bal_t3,$amc_t3,$days_out_of_stock_t3,$q_requested_t3,
                                     $ending_bal_d3,$amc_d3,$days_out_of_stock_d3,$q_requested_d3);
}
        // echo"<pre>";print_r($alocation_details);
    
         $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Counties');
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', 'Commodity Consumption Data (in Tests)');
        $this->excel->getActiveSheet()->setCellValue('A4', 'County');
        $this->excel->getActiveSheet()->setCellValue('B4', 'Sub-County');
        $this->excel->getActiveSheet()->setCellValue('C4', 'MFL Code');
        $this->excel->getActiveSheet()->setCellValue('D4', 'Facility Name');
        $this->excel->getActiveSheet()->setCellValue('E4', 'Screening KHB');
        $this->excel->getActiveSheet()->setCellValue('I4', 'Confirmatory - First Response');
        $this->excel->getActiveSheet()->setCellValue('M4', 'Tie Breaker');
        $this->excel->getActiveSheet()->setCellValue('Q4', 'DBS Bundles');

        $this->excel->getActiveSheet()->setCellValue('A5', '');
        $this->excel->getActiveSheet()->setCellValue('B5', '');
        $this->excel->getActiveSheet()->setCellValue('C5', '');
        $this->excel->getActiveSheet()->setCellValue('D5', '');

        $this->excel->getActiveSheet()->setCellValue('E5', 'Ending Balance');
        $this->excel->getActiveSheet()->setCellValue('F5', 'AMC');
        $this->excel->getActiveSheet()->setCellValue('G5', 'Days Out of Stock');
        $this->excel->getActiveSheet()->setCellValue('H5', 'Quantity Requested');

        $this->excel->getActiveSheet()->setCellValue('I5', 'Ending Balance');
        $this->excel->getActiveSheet()->setCellValue('J5', 'AMC');
        $this->excel->getActiveSheet()->setCellValue('K5', 'Days Out of Stock');
        $this->excel->getActiveSheet()->setCellValue('L5', 'Quantity Requested');

        $this->excel->getActiveSheet()->setCellValue('M5', 'Ending Balance');
        $this->excel->getActiveSheet()->setCellValue('N5', 'AMC');
        $this->excel->getActiveSheet()->setCellValue('O5', 'Days Out of Stock');
        $this->excel->getActiveSheet()->setCellValue('P5', 'Quantity Requested');
        
        $this->excel->getActiveSheet()->setCellValue('Q5', 'Ending Balance');
        $this->excel->getActiveSheet()->setCellValue('R5', 'AMC');
        $this->excel->getActiveSheet()->setCellValue('S5', 'Days Out of Stock');
        $this->excel->getActiveSheet()->setCellValue('T5', 'Quantity Requested');
        //merge cell A1 until C1
        $this->excel->getActiveSheet()->mergeCells('A1:K1');
        $this->excel->getActiveSheet()->mergeCells('E4:H4');
        $this->excel->getActiveSheet()->mergeCells('I4:L4');
        $this->excel->getActiveSheet()->mergeCells('M4:P4');
        $this->excel->getActiveSheet()->mergeCells('Q4:T4');
        //set aligment to center for that merged cell (A1 to C1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('I4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('M4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('Q4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A4:T4')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A5:T5')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');

       for($col = ord('A'); $col <= ord('Q'); $col++){
                //set column dimension
                $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(false);
                 //change the font size
                $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
                 
                //$this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
                            
        foreach ($alocation_details as $row){
                $exceldata[] = $row;
        }
        

                //Fill data 
                $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A6');
                 
                // $this->excel->getActiveSheet()->getStyle('A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                // $this->excel->getActiveSheet()->getStyle('B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                 
                $filename='County Allocation('.$alocation_details[0][0].' County).xls'; //save our workbook as this file name
                header('Content-Type: application/vnd.ms-excel'); //mime type
                header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
 
                //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                //if you want to save it as .XLSX Excel 2007 format
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
                //force user to download the Excel file without writing it to server's HD
                $objWriter->save('php://output');        

    }
    function get_subcounty_facilities_amcs($district){
            //get all the details from allocation_details for a particular sub-county in db
    $sql = "SELECT 
                counties.county  as county_name, districts.district as district_name, allocation_details.*
            FROM
                allocation_details,
                facilities,
                counties,
                districts
            WHERE
                district_id = '$district'
                    AND districts.county = counties.id
                    AND facilities.district = districts.id
                    AND facilities.facility_code = allocation_details.facility_code";
    $result = $this->db->query($sql)->result_array();
        // echo"<pre>";print_r($result);die;
    
        
    foreach ($result as $key => $id_details) {
        
        $facility_code = $id_details['facility_code'];
        // for each selected facility, get the last reported lab details for all four commodities
        $sql2 = "SELECT 
                    closing_stock, days_out_of_stock, q_requested
                FROM
                    lab_commodity_details AS a
                WHERE
                    facility_code = '$facility_code'
                        AND commodity_id between 4 and 7
                        AND created_at IN (SELECT 
                            MAX(created_at)
                        FROM
                            lab_commodity_details AS b
                        WHERE
                            a.facility_code = b.facility_code)";
        $result2 = $this->db->query($sql2)->result_array();

        $county = $id_details['county_name'];
        $district = $id_details['district_name'];
        $facility_name = $id_details['facility_name'];
        $amc_s3 = $id_details['allocate_s'];
        $amc_c3 = $id_details['allocate_c'];
        $amc_t3 = $id_details['allocate_t'];
        $amc_d3 = 0;

        $ending_bal_s3 = $result2[0]['closing_stock'];
        $q_requested_s3 = $result2[0]['q_requested'];
        $days_out_of_stock_s3 = $result2[0]['days_out_of_stock'];

        $ending_bal_c3 = $result2[1]['closing_stock'];
        $q_requested_c3 = $result2[1]['q_requested'];
        $days_out_of_stock_c3 = $result2[1]['days_out_of_stock'];

        $ending_bal_t3 = $result2[2]['closing_stock'];
        $q_requested_t3 = $result2[2]['q_requested'];
        $days_out_of_stock_t3 = $result2[2]['days_out_of_stock'];

        $ending_bal_d3 = $result2[3]['closing_stock'];
        $q_requested_d3 = $result2[3]['q_requested'];
        $days_out_of_stock_d3 = $result2[3]['days_out_of_stock'];

        //based on the results, put them in an array to beused in the excell file.

        $alocation_details[] = array($county,$district,$facility_code,$facility_name,
                                     $ending_bal_s3,$amc_s3,$days_out_of_stock_s3,$q_requested_s3,
                                     $ending_bal_c3,$amc_c3,$days_out_of_stock_c3,$q_requested_c3,
                                     $ending_bal_t3,$amc_t3,$days_out_of_stock_t3,$q_requested_t3,
                                     $ending_bal_d3,$amc_d3,$days_out_of_stock_d3,$q_requested_d3);
}
        // echo"<pre>";print_r($alocation_details);die;
    
         $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Counties');
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', 'Commodity Consumption Data (in Tests)');
        $this->excel->getActiveSheet()->setCellValue('A4', 'County');
        $this->excel->getActiveSheet()->setCellValue('B4', 'Sub-County');
        $this->excel->getActiveSheet()->setCellValue('C4', 'MFL Code');
        $this->excel->getActiveSheet()->setCellValue('D4', 'Facility Name');
        $this->excel->getActiveSheet()->setCellValue('E4', 'Screening KHB');
        $this->excel->getActiveSheet()->setCellValue('I4', 'Confirmatory - First Response');
        $this->excel->getActiveSheet()->setCellValue('M4', 'Tie Breaker');
        $this->excel->getActiveSheet()->setCellValue('Q4', 'DBS Bundles');

        $this->excel->getActiveSheet()->setCellValue('A5', '');
        $this->excel->getActiveSheet()->setCellValue('B5', '');
        $this->excel->getActiveSheet()->setCellValue('C5', '');
        $this->excel->getActiveSheet()->setCellValue('D5', '');

        $this->excel->getActiveSheet()->setCellValue('E5', 'Ending Balance');
        $this->excel->getActiveSheet()->setCellValue('F5', 'AMC');
        $this->excel->getActiveSheet()->setCellValue('G5', 'Days Out of Stock');
        $this->excel->getActiveSheet()->setCellValue('H5', 'Quantity Requested');

        $this->excel->getActiveSheet()->setCellValue('I5', 'Ending Balance');
        $this->excel->getActiveSheet()->setCellValue('J5', 'AMC');
        $this->excel->getActiveSheet()->setCellValue('K5', 'Days Out of Stock');
        $this->excel->getActiveSheet()->setCellValue('L5', 'Quantity Requested');

        $this->excel->getActiveSheet()->setCellValue('M5', 'Ending Balance');
        $this->excel->getActiveSheet()->setCellValue('N5', 'AMC');
        $this->excel->getActiveSheet()->setCellValue('O5', 'Days Out of Stock');
        $this->excel->getActiveSheet()->setCellValue('P5', 'Quantity Requested');
        
        $this->excel->getActiveSheet()->setCellValue('Q5', 'Ending Balance');
        $this->excel->getActiveSheet()->setCellValue('R5', 'AMC');
        $this->excel->getActiveSheet()->setCellValue('S5', 'Days Out of Stock');
        $this->excel->getActiveSheet()->setCellValue('T5', 'Quantity Requested');
        //merge cell A1 until C1
        $this->excel->getActiveSheet()->mergeCells('A1:K1');
        $this->excel->getActiveSheet()->mergeCells('E4:H4');
        $this->excel->getActiveSheet()->mergeCells('I4:L4');
        $this->excel->getActiveSheet()->mergeCells('M4:P4');
        $this->excel->getActiveSheet()->mergeCells('Q4:T4');
        //set aligment to center for that merged cell (A1 to C1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('I4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('M4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('Q4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A4:T4')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A5:T5')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');

       for($col = ord('A'); $col <= ord('Q'); $col++){
                //set column dimension
                $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(false);
                 //change the font size
                $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
                 
                //$this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
                            
        foreach ($alocation_details as $row){
                $exceldata[] = $row;
        }
        

                //Fill data 
                $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A6');
                 
                // $this->excel->getActiveSheet()->getStyle('A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                // $this->excel->getActiveSheet()->getStyle('B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                 
                $filename='County Allocation('.$alocation_details[0][1].' Sub-County).XLSX'; //save our workbook as this file name
                header('Content-Type: application/vnd.ms-excel'); //mime type
                header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
 
                //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                //if you want to save it as .XLSX Excel 2007 format
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
                //force user to download the Excel file without writing it to server's HD
                $objWriter->save('php://output');          

    }
    function get_commodity_facilities_amcs($commodity){
        //get the amc for the selected commodity
    	if ($commodity == 4){
    		 $sql = "SELECT county, district, mfl, facility_name, amc_s3 as amc FROM allocation_details limit 0,100";
             $commodity_name = 'Screening-KHB';

    	}else if($commodity == 5){
    		 $sql = "SELECT county, district, mfl, facility_name, amc_c3 as amc FROM allocation_details";
             $commodity_name = 'Confirmatory-First Response';

    	}else if($commodity==6){
    		 $sql = "SELECT county, district, mfl, facility_name, amc_t3 as amc FROM allocation_details";
             $commodity_name = 'Tie Breaker';

    	}else if($commodity==7){
    		 $sql = "SELECT county, district, mfl, facility_name, amc_d3 as amc FROM allocation_details";
             $commodity_name = 'DBS Bundles';

    	}       
        $result = $this->db->query($sql)->result_array();
        
        foreach ($result as $key => $id_details) {
        
        $facility_code = $id_details['mfl'];
        // for each selected facility, get the last reported lab details for selected commodity
        $sql2 = "SELECT 
                    closing_stock, days_out_of_stock, q_requested
                FROM
                    lab_commodity_details AS a
                WHERE
                    facility_code = '$facility_code'
                        AND commodity_id = '$commodity'
                        AND created_at IN (SELECT 
                            MAX(created_at)
                        FROM
                            lab_commodity_details AS b
                        WHERE
                            a.facility_code = b.facility_code)";
        $result2 = $this->db->query($sql2)->result_array();

        $county = $id_details['county'];
        $district = $id_details['district'];
        $facility_name = $id_details['facility_name'];
        $amc = $id_details['amc'];       

        $ending_bal = $result2[0]['closing_stock'];
        $q_requested = $result2[0]['q_requested'];
        $days_out_of_stock= $result2[0]['days_out_of_stock'];

     //based on the results, put them in an array to be used in the excel file.

        $alocation_details[] = array($county,$district,$facility_code,$facility_name,
                                     $ending_bal,$amc,$days_out_of_stock,$q_requested);
        }
        // echo"<pre>";print_r($alocation_details);die;
         $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Counties');
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', 'Commodity Consumption Data (in Tests)');
        $this->excel->getActiveSheet()->setCellValue('A4', 'County');
        $this->excel->getActiveSheet()->setCellValue('B4', 'Sub-County');
        $this->excel->getActiveSheet()->setCellValue('C4', 'MFL Code');
        $this->excel->getActiveSheet()->setCellValue('D4', 'Facility Name');
        $this->excel->getActiveSheet()->setCellValue('E4', ' '.$commodity_name.'');
        
        $this->excel->getActiveSheet()->setCellValue('A5', '');
        $this->excel->getActiveSheet()->setCellValue('B5', '');
        $this->excel->getActiveSheet()->setCellValue('C5', '');
        $this->excel->getActiveSheet()->setCellValue('D5', '');

        $this->excel->getActiveSheet()->setCellValue('E5', 'Ending Balance');
        $this->excel->getActiveSheet()->setCellValue('F5', 'AMC');
        $this->excel->getActiveSheet()->setCellValue('G5', 'Days Out of Stock');
        $this->excel->getActiveSheet()->setCellValue('H5', 'Quantity Requested');

        //merge cell A1 until C1
        $this->excel->getActiveSheet()->mergeCells('A1:K1');
        $this->excel->getActiveSheet()->mergeCells('E4:H4');
       
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A4:H4')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A5:H5')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');

       for($col = ord('A'); $col <= ord('H'); $col++){
                //set column dimension
                $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(false);
                 //change the font size
                $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
        }
                            
        foreach ($alocation_details as $row){
                $exceldata[] = $row;
        }      
                //Fill data 
                $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A6');
                 
                $this->excel->getActiveSheet()->getStyle('C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                 
                $filename='Commodity Allocation('.$commodity_name .').xls'; //save our workbook as this file name
                header('Content-Type: application/vnd.ms-excel'); //mime type
                header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
 
                //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                //if you want to save it as .XLSX Excel 2007 format
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
                //force user to download the Excel file without writing it to server's HD
                $objWriter->save('php://output');  
    }

     function get_months_facilities_amcs($month){
        $sql = "SELECT * FROM allocation_details where month = '$month'";
        $result = $this->db->query($sql)->result_array();
       
        //convert the date into text
        $year = substr($month, -4);
        $month = substr_replace($month, "", -4);              
        $firstdate = $year . '-' . $month . '-01';     
        $month_text =  date("F Y", strtotime($firstdate)); 

        foreach ($result as $key => $id_details) {
        
        $facility_code = $id_details['mfl'];
        $county = $id_details['county'];
        $district = $id_details['district'];
        $facility_name = $id_details['facility_name'];
        $amc_s3 = $id_details['amc_s3'];
        $amc_c3 = $id_details['amc_c3'];
        $amc_t3 = $id_details['amc_t3'];
        $amc_d3 = 0;        
        
        //based on the results, put them in an array to be used in the excel file.

        $alocation_details[] = array($county,$district,$facility_code,$facility_name,$amc_s3,$amc_c3,$amc_t3,$amc_d3);
}
        // echo"<pre>";print_r($alocation_details);
    
         $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Counties');
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', 'Commodity Consumption Data (in Tests)');
        $this->excel->getActiveSheet()->setCellValue('A4', 'County');
        $this->excel->getActiveSheet()->setCellValue('B4', 'Sub-County');
        $this->excel->getActiveSheet()->setCellValue('C4', 'MFL Code');
        $this->excel->getActiveSheet()->setCellValue('D4', 'Facility Name');
        $this->excel->getActiveSheet()->setCellValue('E4', 'Screening KHB');
        $this->excel->getActiveSheet()->setCellValue('F4', 'Confirmatory - First Response');
        $this->excel->getActiveSheet()->setCellValue('G4', 'Tie Breaker');
        $this->excel->getActiveSheet()->setCellValue('H4', 'DBS Bundles');

        $this->excel->getActiveSheet()->setCellValue('A5', '');
        $this->excel->getActiveSheet()->setCellValue('B5', '');
        $this->excel->getActiveSheet()->setCellValue('C5', '');
        $this->excel->getActiveSheet()->setCellValue('D5', '');
        $this->excel->getActiveSheet()->setCellValue('E5', 'AMC');
        $this->excel->getActiveSheet()->setCellValue('F5', 'AMC');
        $this->excel->getActiveSheet()->setCellValue('G5', 'AMC');
        $this->excel->getActiveSheet()->setCellValue('H5', 'AMC');

        //merge cell A1 until C1
        $this->excel->getActiveSheet()->mergeCells('A1:K1');
        //set aligment to center for that merged cell (A1 to C1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('F4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('G4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('H4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A4:H4')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A5:H5')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');

       for($col = ord('A'); $col <= ord('H'); $col++){
                //set column dimension
                $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(false);
                 //change the font size
                $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
                 
        }
                            
        foreach ($alocation_details as $row){
                $exceldata[] = $row;
        }        

                //Fill data 
                $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A6');
                $this->excel->getActiveSheet()->getStyle('C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                 
                $filename='Monthly Allocation ('.$month_text.').xls'; //save our workbook as this file name
                header('Content-Type: application/vnd.ms-excel'); //mime type
                header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
 
                //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                //if you want to save it as .XLSX Excel 2007 format
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
                //force user to download the Excel file without writing it to server's HD
                $objWriter->save('php://output');

    }
   function get_fcdrr_details($type, $month, $county_id, $district_id, $commodity){

        // error_reporting(1);
       // error_reporting(E_ALL);
       // ini_set('display_errors', TRUE);
       // ini_set('display_startup_errors', TRUE);

        // echo "<pre>";print_r($commodity);exit;
    // echo $district_id;exit;
        $district_conditions = '';
        if($district_id > 0){
            $district_conditions = ' AND districts.id = '.$district_id;
        }else{$district_conditions = NULL;}

        $commodity_conditions = '';
        if($commodity >0){
            $commodity_conditions = ' AND lab_commodity_details.commodity_id = '.$commodity;
        }else{
            $commodity_conditions = ' AND lab_commodity_details.commodity_id IN(4,5,7,8) ';
            // $commodity_conditions = NULL;
        }

        $conditions='';
        if ($county_id>0) {
           $conditions = 'AND counties.id = '.$county_id;
        }

        // echo "<pre>";print_r($conditions);exit;
        $date = explode('_', $month);
        // echo "<pre>";print_r($date);exit;
        // $month = $date[0]; 
        // $year = $date[1];

        $year = substr($month, -4);
        $month = substr_replace($month, "", -4);

        // echo "<pre>";print_r($month);exit;


        // echo $month;exit;             
        
        // $firstdate = $year . '-' . $month . '-01';     
        // $lastdate = $year . '-' . $month . '-31';   

        $firstdate = $year . '-' . $month . '-01';     
        $lastdate = $year . '-' . $month . '-31'; 
        // echo "<pre>";print_r($firstdate);exit;

        /*if ($month = date('m') && $year = date('Y')) {
            $new_month = date("m", strtotime("+0 month",strtotime($firstdate)));
            $new_year = date("Y", strtotime("+0 month",strtotime($firstdate)));
        }else{
            $new_month = date("m", strtotime("+1 month",strtotime($firstdate)));
            $new_year = date("Y", strtotime("+1 month",strtotime($firstdate)));
        }*/

        $new_month = date("m", strtotime("+1 month",strtotime($firstdate)));
        $new_year = date("Y", strtotime("+1 month",strtotime($firstdate)));

        $new_first_date = $new_year.'-'.$new_month.'-01';
        $new_last_date = $new_year.'-'.$new_month.'-31';

        $month_text =  date("F Y", strtotime($firstdate)); 


        // echo "<pre>";print_r($new_last_date);exit;
        // echo $new_first_date." ".$new_last_date;exit;
        $sql = "
                SELECT 
                    counties.county,
                    districts.district,
                    facilities.facility_code,
                    facilities.facility_name,                    
                    lab_commodities.commodity_name,
                    lab_commodity_details.*
                FROM
                    lab_commodity_details,
                    lab_commodities,
                    facilities,
                    districts,
                    counties
                WHERE
                    lab_commodity_details.created_at BETWEEN '$firstdate' AND '$lastdate'                    
                        AND lab_commodities.id = lab_commodity_details.commodity_id $commodity_conditions
                        AND facilities.facility_code = lab_commodity_details.facility_code
                        AND facilities.district = districts.id
                        AND districts.county = counties.id
                       $conditions $district_conditions
                ORDER BY counties.county , districts.district , facilities.facility_code , lab_commodity_details.commodity_id ";

        $sql2 = "
                SELECT 
                    counties.county,
                    districts.district,
                    facilities.facility_code,
                    facilities.facility_name,
                    lab_commodity_orders.id AS labs_order_id,
                    lab_commodity_orders.order_date AS labs_order_date, 
                    lab_commodities.commodity_name,
                    lab_commodity_details.*
                FROM
                    lab_commodity_orders,
                    lab_commodity_details,
                    lab_commodities,
                    facilities,
                    districts,
                    counties
                WHERE
                    lab_commodity_orders.order_date BETWEEN '$new_first_date' AND '$new_last_date'
                        AND lab_commodities.id = lab_commodity_details.commodity_id
                        AND facilities.facility_code = lab_commodity_details.facility_code
                        AND facilities.district = districts.id
                        AND districts.county = counties.id
                        AND counties.id = $county_id
                        $district_conditions $commodity_conditions
                        AND lab_commodity_orders.id = lab_commodity_details.order_id
                ORDER BY counties.county , districts.district , facilities.facility_code , lab_commodity_details.commodity_id
                ";
        // echo "<pre>";print_r($sql2);exit;
        // $result = $this->db->query($sql)->result_array();
        $result = $this->db->query($sql2)->result_array();
       // echo "$sql2";die;
        // echo "<pre>";print_r($result);exit;
        //convert the date into text        

        foreach ($result as $key => $details) {
        
        $facility_code = $details['facility_code'];
        $county = $details['county'];
        $district = $details['district'];
        $facility_name = $details['facility_name'];
        $commodity_name = $details['commodity_name'];
        $beginning_bal = $details['beginning_bal'];
        $physicalbeginning_bal = $details['physical_beginning_bal'];
        $q_received = $details['q_received'];
        $q_used = $details['q_used'];
        $q_requested = $details['q_requested'];
        $no_of_tests_done = $details['no_of_tests_done'];
        $losses = $details['losses'];
        $positive_adj = $details['positive_adj'];
        $negative_adj = $details['negative_adj'];
        $closing_stock = $details['closing_stock'];
        $physicalclosing_stock = $details['physical_closing_stock'];
        $days_out_of_stock = $details['days_out_of_stock'];
        $q_expiring = $details['q_expiring'];
        $amc = $details['amc'];
       // $no_of_tests_done = 0;        
        
        //based on the results, put them in an array to be used in the excel file.

         // $fcdrr_details[] = array($county,$district,$facility_code,$facility_name,$commodity_name,$beginning_bal,$q_received,$q_used,$no_of_tests_done,$losses,$positive_adj,$negative_adj, $closing_stock,$days_out_of_stock,$q_expiring,$amc);
         $fcdrr_details[] = array($county,$district,$facility_code,$facility_name,$commodity_name,$beginning_bal,$q_received,$q_used,$q_requested,$no_of_tests_done,$losses,$positive_adj,$negative_adj, $closing_stock,$days_out_of_stock,$q_expiring);
}
        // echo"<pre>";print_r($fcdrr_details); die();
        // echo "<pre>";print_r($this->excel);exit;
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle(' Counties');
        //set cell A1 content with some text
        /*$this->excel->getActiveSheet()->setCellValue('A1', 'Commodity Consumption Data (in Tests)');
        $this->excel->getActiveSheet()->setCellValue('A4', 'County');
        $this->excel->getActiveSheet()->setCellValue('B4', 'Sub-County');
        $this->excel->getActiveSheet()->setCellValue('C4', 'MFL Code');
        $this->excel->getActiveSheet()->setCellValue('D4', 'Facility Name');
        $this->excel->getActiveSheet()->setCellValue('E4', 'Commodity Name');
        $this->excel->getActiveSheet()->setCellValue('F4', 'Beginning Balance');
        $this->excel->getActiveSheet()->setCellValue('G4', 'Quantity Received');
        $this->excel->getActiveSheet()->setCellValue('H4', 'Quantity Used');
        $this->excel->getActiveSheet()->setCellValue('I4', 'Tests Done');
        $this->excel->getActiveSheet()->setCellValue('J4', 'Losses');
        $this->excel->getActiveSheet()->setCellValue('K4', 'Positive Adjustments');
        $this->excel->getActiveSheet()->setCellValue('L4', 'Negative Adjustments');
        $this->excel->getActiveSheet()->setCellValue('M4', 'Ending Balance');
        $this->excel->getActiveSheet()->setCellValue('N4', 'Days out of Stock');
        $this->excel->getActiveSheet()->setCellValue('O4', 'Quantity Expiring');
        $this->excel->getActiveSheet()->setCellValue('P4', 'Average Monthly Consumption');*/

        // $this->excel->getActiveSheet()->setCellValue('A1', 'Commodity Consumption Data (in Tests)');
        $this->excel->getActiveSheet()->setCellValue('A1', 'County');
        $this->excel->getActiveSheet()->setCellValue('B1', 'Sub-County');
        $this->excel->getActiveSheet()->setCellValue('C1', 'MFL Code');
        $this->excel->getActiveSheet()->setCellValue('D1', 'Facility Name');
        $this->excel->getActiveSheet()->setCellValue('E1', 'Commodity Name');
        $this->excel->getActiveSheet()->setCellValue('F1', 'Beginning Balance');
        $this->excel->getActiveSheet()->setCellValue('G1', 'Quantity Received');
        $this->excel->getActiveSheet()->setCellValue('H1', 'Quantity Used');
        $this->excel->getActiveSheet()->setCellValue('I1', 'Quantity Requested');
        $this->excel->getActiveSheet()->setCellValue('J1', 'Tests Done');
        $this->excel->getActiveSheet()->setCellValue('K1', 'Losses');
        $this->excel->getActiveSheet()->setCellValue('L1', 'Positive Adjustments');
        $this->excel->getActiveSheet()->setCellValue('M1', 'Negative Adjustments');
        $this->excel->getActiveSheet()->setCellValue('N1', 'Ending Balance');
        $this->excel->getActiveSheet()->setCellValue('O1', 'Days out of Stock');
        $this->excel->getActiveSheet()->setCellValue('P1', 'Quantity Expiring in 6 Months');
        // $this->excel->getActiveSheet()->setCellValue('P1', 'Average Monthly Consumption');

        // echo "<pre>";print_r($this->excel->getActiveSheet());exit;

        //merge cell A1 until C1
        // $this->excel->getActiveSheet()->mergeCells('A1:Q1');
        //set aligment to center for that merged cell (A1 to C1)
        // $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle('E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle('F4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle('G4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle('H4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //make the font become bold
        // $this->excel->getActiveSheet()->getStyle('A4:Q4')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1:P1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        // $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
        // $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');

       for($col = ord('A'); $col <= ord('P'); $col++){
                //set column dimension
                $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
                 //change the font size
                // $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
                 
        }

        $exceldata = array();
        foreach ($fcdrr_details as $row){
                $exceldata[] = $row;
        }        
        // echo "<pre>";print_r($exceldata);exit;
                //Fill data 
                // $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A5');
                $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A2');
                 
                $filename= 'RTK FCDRR Details ('.$month_text.').xlsx'; //save our workbook as this file name
                header('Content-Type: application/vnd.ms-excel'); //mime type
                header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
 
                //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                //if you want to save it as .XLSX Excel 2007 format
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
                //force user to download the Excel file without writing it to server's HD
                ob_end_clean();
                $objWriter->save('php://output');

    }
    public function get_one_fcdrr_details($type, $month, $county_id, $district_id, $commodity){
        // echo $type." ".$month." ".$county_id." ".$district_id." ".$commodity;exit;
        $district_conditions = '';
        if($district_id > 0){
            $district_conditions = ' AND districts.id = '.$district_id;
        }

        $commodity_conditions = '';
        if($commodity >0){
            $commodity_conditions = ' AND lab_commodity_details.commodity_id = '.$commodity;
        }else{
            $commodity_conditions = ' AND lab_commodity_details.commodity_id IN(4,5,7,8) ';
            // $commodity_conditions = NULL;
        }

        $year = substr($month, -4);
        $month = substr_replace($month, "", -4);              
        $firstdate = $year . '-' . $month . '-01';     
        $lastdate = $year . '-' . $month . '-31';     
        $month_text =  date("F Y", strtotime($firstdate)); 

        if($type = 1){
            $type_text = 'closing_stock';
            // $type_title = 'Computed Ending Balance';
            $type_title = 'Ending Balance';
        }else if($type = 2){            
            $type_text = 'q_used';
            $type_title = 'Quantity Used';
        }else if($type = 3){            
            $type_text = 'no_of_tests_done';
            // $type_title = 'No of Tests done';
            $type_title = 'Tests done';
        }else if($type = 4){            
            $type_text = 'amc';
            $type_title = 'Average Monthly Consumption';
        }
        $conditions='';
        if ($county_id>0) {
           $conditions = 'AND counties.id = '.$county_id;
        }
        $sql = "SELECT 
                    counties.county,
                    districts.district,
                    facilities.facility_code,
                    facilities.facility_name,                    
                    lab_commodities.commodity_name,
                    lab_commodity_details.$type_text as type_of_text
                FROM
                    lab_commodity_details,
                    lab_commodities,
                    facilities,
                    districts,
                    counties
                WHERE
                    lab_commodity_details.created_at BETWEEN '$firstdate' AND '$lastdate'                    
                        AND lab_commodities.id = lab_commodity_details.commodity_id $commodity_conditions
                        AND facilities.facility_code = lab_commodity_details.facility_code
                        AND facilities.district = districts.id
                        AND districts.county = counties.id
                        $conditions $district_conditions
                ORDER BY counties.county , districts.district , facilities.facility_code , lab_commodity_details.commodity_id";
        $result = $this->db->query($sql)->result_array();
       
        //convert the date into text        

        foreach ($result as $key => $details) {
        
        $facility_code = $details['facility_code'];
        $county = $details['county'];
        $district = $details['district'];
        $facility_name = $details['facility_name'];
        $commodity_name = $details['commodity_name'];
        $type_value = $details['type_of_text'];
        // $q_received = $details['q_received'];
        // $q_used = $details['q_used'];
        // $no_of_tests_done = $details['no_of_tests_done'];
        // $positive_adj = $details['positive_adj'];
        // $negative_adj = $details['negative_adj'];
        // $closing_stock = $details['closing_stock'];
        // $days_out_of_stock = $details['days_out_of_stock'];
        // $q_expiring = $details['q_expiring'];
       // $no_of_tests_done = 0;        
        
        //based on the results, put them in an array to be used in the excel file.

        $fcdrr_details[] = array($county,$district,$facility_code,$facility_name,$commodity_name,$type_value);
}
        // echo"<pre>";print_r($alocation_details);
    
         $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle(' Counties');
        //set cell A1 content with some text
        // $this->excel->getActiveSheet()->setCellValue('A1', $district.' RTKCommodity Consumption Data (in Tests)');
        $this->excel->getActiveSheet()->setCellValue('A1', 'County');
        $this->excel->getActiveSheet()->setCellValue('B1', 'Sub-County');
        $this->excel->getActiveSheet()->setCellValue('C1', 'MFL Code');
        $this->excel->getActiveSheet()->setCellValue('D1', 'Facility Name');
        $this->excel->getActiveSheet()->setCellValue('E1', 'Commodity Name');
        $this->excel->getActiveSheet()->setCellValue('F1',  $type_title);
        // $this->excel->getActiveSheet()->setCellValue('G4', 'Quantity Received');
        // $this->excel->getActiveSheet()->setCellValue('H4', 'Quantity Used');
        // $this->excel->getActiveSheet()->setCellValue('I4', 'Tests Done');
        // $this->excel->getActiveSheet()->setCellValue('J4', 'Positive Adjustments');
        // $this->excel->getActiveSheet()->setCellValue('K4', 'Negative Adjustments');
        // $this->excel->getActiveSheet()->setCellValue('L4', 'Ending Balance');
        // $this->excel->getActiveSheet()->setCellValue('M4', 'Days out of Stock');
        // $this->excel->getActiveSheet()->setCellValue('N4', 'Quantity Expiring');

        

        //merge cell A1 until C1
        // $this->excel->getActiveSheet()->mergeCells('A1:F1');
        //set aligment to center for that merged cell (A1 to C1)
        /*$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('F4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('G4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('H4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);*/
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        // $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
        // $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');

       for($col = ord('A'); $col <= ord('F'); $col++){
                //set column dimension
                $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
                 //change the font size
                // $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
                 
        }
                            
        foreach ($fcdrr_details as $row){
                $exceldata[] = $row;
        }        

                //Fill data 
                $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A2');
                 
                $filename= $county.' County '.$type_title.' ('.$month_text.').xlsx'; //save our workbook as this file name
                header('Content-Type: application/vnd.ms-excel'); //mime type
                header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
 
                //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                //if you want to save it as .XLSX Excel 2007 format
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
                //force user to download the Excel file without writing it to server's HD
                ob_end_clean();
                $objWriter->save('php://output');

    }
    function get_combination_facilities_amcs(){
        $sql = "SELECT * FROM allocation_details where district_id = '$district'";
        $result = $this->db->query($sql)->result_array();
        // print_r($result);
        echo json_encode($result);        

    }

}
