<?php
// echo "<pre>";
// print_r($lab_categories);die;
?>

<style>
    .no-margin{
        margin: 0!important;
    }

    .no-padding{
        padding: 0!important;
    }

    .no-spacing{
        padding: 0!important;
        margin: 0!important;
    }

    .no-border-radius{
        border-radius: 0!important;
    }

    table {
        font-size: 11px;
    }
</style>

<div class="col-md-12 alert alert-info">
    <strong><p>CD4 Facilities in this district: </p></strong>
    <?php //echo "<pre>";print_r($facilities);exit; ?>

    <?php foreach ($facilities as $fac => $fac_val) { ?>
        <ul>
            <li><?php echo $fac_val['facility_code']." - ".$fac_val['facility_name']; ?></li>
        </ul>
    <?php } ?>
</div>
<div class="col-md-12">
    <?php echo form_open(); ?>
    <?php
    $checker = 0;
// echo "<pre>";print_r($final_array);exit;
    foreach ($final_array as $key => $value) {
        $fac_code = $value['facility_code'];
// echo "<pre>";print_r($value);exit;
        ?>
        <div class="col-md-12 alert alert-info no-margin no-border-radius">
            <center>
                <strong>
                    <p class="no-spacing"><?php echo strtoupper($value['facility_code']." - ".$value['facility_name']); ?></p>
                </strong>
            </center>
        </div>

        <table width="10%" class="table table-sm table-bordered table-striped data-table">

            <!-- <tr class="info">
                <td colspan = "4">
                    
                </td>            
            </tr>  -->

    <?php 
    foreach ($value['categories'] as $keyy => $valuee) {
        // echo "<pre>";print_r($valuee);exit;
        ?>                       

        <td colspan = "4">
            <b>
                <?php echo $keyy; ?>
            </b>
        </td>            
    </tr> 

    <tr>       

        <td>
            <b>Commodity</b>
        </td>
        <td>
            <b>AMC</b>
        </td>
        <td>
            <b>Quantity to allocate</b>
        </td>
        <td>
            <b>Feedback</b>
        </td>
    </tr>

    <tr>

    <?php 
    foreach ($valuee as $lab_commodities) {
        $category = $lab_commodities['category']; 
        // echo "<pre>";print_r($lab_commodities);exit;
    ?>

    <tr commodity_id="<?php echo $lab_commodities['id']; ?>">

        <td class="commodity_names" id="commodity_name_<?php echo $lab_commodities['id'];?>" style = "text-align:left">
        </b>
            <?php echo $lab_commodities['commodity_name']; ?>
        </td>
        <td>
            <input id="amc[<?php echo $fac_code; ?>][<?php echo $lab_commodities['id']; ?>]" name = "amc[<?php echo $fac_code; ?>][<?php echo $lab_commodities['id']; ?>]" class='input input-sm' type="number" style = "text-align:center"/>
        </td>
        <td>
            <input id="quantity[<?php echo $fac_code; ?>][<?php echo $lab_commodities['id']; ?>]" name = "quantity[<?php echo $fac_code; ?>][<?php echo $lab_commodities['id']; ?>]" class='input input-sm' type="number" style = "text-align:center"/>
        </td>  
        <td>
            <input id="feedback[<?php echo $fac_code; ?>][<?php echo $lab_commodities['id']; ?>]" name = "feedback[<?php echo $fac_code; ?>][<?php echo $lab_commodities['id']; ?>]" class='input input-sm' type="number" style = "text-align:center"/>
        </td> 

    </tr>

    <?php $checker++;
} 

?>
<?php }//end of category foreach?>
</table>
<?php }//end of final_array foreach?>




<?php form_close(); ?>

<script type="text/javascript">    
    $("table").tablecloth({
        bordered: true,
        condensed: true,
        striped: true,            
        clean: true,                
    });

    $('table').dataTable();

    $('#calc').css('color','blue');
    $('#calc').css('text-align','center');
    $('#calc').css('font-size','12px');
</script>

