<style type="text/css">
	.btn{
		border-radius: 0px!important;
		width:180px!important;
	}
	.margin-top{
		margin-top: 5px!important;
	}
	.margin-vert{
		margin: 5px 0px!important;
	}
	.no-spacing{
		padding: 0!important;
		margin: 0!important;
	}
</style>
<?php //echo "<pre>";print_r($allocations);exit; ?>
<div>
  <?php if ($success_status == '1'): ?>
    <div class="col-md-6 col-md-offset-3 alert alert-success">
      <!-- Allocation has been deleted <strong>successfully </strong> -->
      Deletion <strong>successful. </strong>
    </div>
  <?php elseif ($success_status == '2'): ?>
  	<div class="col-md-6 col-md-offset-3 alert alert-danger">
      Insertion <strong>failed.</strong> Ensure you have selected a device and a facility.
    </div>
  <?php endif; ?>
</div>

<div class="col-md-12">
	<p class="alert alert-info">Duplicate facility devices will also be deleted.</br>
	If facility was not cd4_enabled, it will be automatically enabled on addition of cd4 device.</p>
</div>

<div class="col-md-12">
	<?php echo form_open('cd4_management/cd4_facility_management_add_device'); ?>
	<div class="col-md-5 no-spacing">
		<select class="form-control select2" name="facility" id="select2" required>
			<option value="">Select Facility</option>
			<?php foreach ($facilities as $key => $value) { ?>
			<option value="<?php echo $value['facility_code']; ?>"><?php echo $value['facility_code'].' | '.$value['facility_name']; ?></option>
			<?php } ?>
		</select>
	</div>
	<div class="col-md-5 no-spacing">
		<select class="form-control select2" name="device" id="select2" required>
			<option value="">Select Device</option>
			<?php foreach ($devices as $key => $value) { ?>
			<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
			<?php } ?>
		</select>
	</div>
	<div class="col-md-2 no-spacing">
		<button class="btn btn-primary col-md-2">Add Device</button>
	</div>
</div>

<div class="col-md-12">
	<table class="table table-bordered table-fluid compact col-md-12" id="datatable_">
		<thead>
			<th>Facility Code</th>
			<th>Facility Name</th>
			<th>County</th>
			<th>District</th>
			<th>Device</th>
			<th>Action</th>
		</thead>
		<tbody>
			<?php foreach ($cd4_sites as $key => $value) { ?>
			<tr>
				<td><?php echo $value['facility_code']; ?></td>
				<td><?php echo $value['facility_name']; ?></td>
				<td><?php echo $value['county_name']; ?></td>
				<td><?php echo $value['district']; ?></td>
				<td><?php echo $value['device_name']; ?></td>
				<td>
				<a class="btn btn-sm btn-primary col-md-12 margin-vert " href="<?php echo base_url().'cd4_management/cd4_facility_management_delete_equipment/'.$value['facility_code'].'/'.$value['device_id'] ?>">Delete Equipment</a>
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>

<script>
$(document).ready(function() {
	$('#datatable_').dataTable({
     "sDom": "T lfrtip",
     "aaSorting": [],
     "bJQueryUI": false,
      "bPaginate": false,
      "oLanguage": {
        "sLengthMenu": "_MENU_ Records per page",
        "sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
      },
      "oTableTools": {
      "aButtons": [ 
          "copy",
          "print",
          {
            "sExtends": "collection",
            "sButtonText": 'Save',
            "aButtons": ["csv", "xls", "pdf"]
          }     
      
      ],  
      "sSwfPath": "<?php echo base_url();?>assets/datatable/media/swf/copy_csv_xls_pdf.swf"
    }
  });

	$('.select2').select2();
});
</script>