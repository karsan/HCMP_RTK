<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>HCMP RTK | Dashboard</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css"/>
<!-- <link href="<?php echo base_url().'assets/v2/dashboard/'; ?>/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/> -->
<link href="<?php echo base_url().'assets/v2/dashboard/'; ?>/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url().'assets/v2/dashboard/'; ?>/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url().'assets/v2/dashboard/'; ?>/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url().'assets/v2/dashboard/'; ?>/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="<?php echo base_url().'assets/v2/dashboard/'; ?>global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url().'assets/v2/dashboard/'; ?>global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="<?php echo base_url().'assets/v2/dashboard/'; ?>/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="<?php echo base_url().'assets/v2/dashboard/'; ?>/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url().'assets/v2/dashboard/'; ?>/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url().'assets/v2/dashboard/'; ?>/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url().'assets/v2/dashboard/'; ?>/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?php echo base_url().'assets/v2/dashboard/'; ?>/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url().'assets/v2/highcharts/css/highcharts.css'; ?>" rel="stylesheet" type="text/css"/>

<link href=<?php echo base_url()."assets/v2/dashboard/vendors/select2/dist/css/select2.min.css"?> rel="stylesheet">
<link href=<?php echo base_url()."assets/v2/dashboard/vendors/select2/dist/css/select2.min.css"?> rel="stylesheet">
<link href=<?php echo base_url().'assets/v2/DataTables/datatables.min.css'; ?> rel="stylesheet">
<link href=<?php echo base_url().'assets/v2/font-awesome/css/font-awesome.min.css'; ?> rel="stylesheet">

<script src="<?php echo base_url().'assets/v2/dashboard/'; ?>global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/v2/DataTables/datatables.min.js';?>"></script>

<script src="<?php echo base_url();?>assets/FusionCharts/FusionCharts.js" type="text/javascript"></script>
<!-- END THEME STYLES -->

<!-- <script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script> -->

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/b-1.4.2/b-colvis-1.4.2/b-flash-1.4.2/b-html5-1.4.2/b-print-1.4.2/sc-1.4.3/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/b-1.4.2/b-colvis-1.4.2/b-flash-1.4.2/b-html5-1.4.2/b-print-1.4.2/sc-1.4.3/datatables.min.js"></script>


<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<style>
	.logo{
		 width: 45px;
    	float: left;
	}

	.logo-text{
		margin: 10px 0 0 5px;
		color: white;
	    font-size: 18px;
	    font-weight: bold;
	    text-decoration: none;
    	float: left;
	}
	.no-margin{
		margin:0!important;
	}
	.no-padding{
		padding:0!important;
	}
	.no-spacing{
		padding:0!important;
		margin:0!important;
	}

	.select2{
		float:left!important;
		width:100%!important;
  	}

  	/** Select2 **/
	.select2-container--default .select2-selection--single,
	.select2-container--default .select2-selection--multiple {
	  background-color: #fff;
	  border: 1px solid #ccc;
	  border-radius: 0;
	  min-height: 38px;
	}

	.select2-container--default .select2-selection--single .select2-selection__rendered {
	  color: #73879C;
	  padding-top: 5px;
	}

	.select2-container--default .select2-selection--multiple .select2-selection__rendered {
	  padding-top: 3px;
	}

	.select2-container--default .select2-selection--single .select2-selection__arrow {
	  height: 36px;
	}

	.select2-container--default .select2-selection--multiple .select2-selection__choice,
	.select2-container--default .select2-selection--multiple .select2-selection__clear {
	  margin-top: 2px;
	  border: none;
	  border-radius: 0;
	  padding: 3px 5px;
	}

	.select2-container--default.select2-container--focus .select2-selection--multiple {
	  border: 1px solid #ccc;
	}
	/** /Select2 **/

	.filter_button{
	    padding:9px 22px!important;
	    margin:0;
	    float:left;
	    border-radius:0px;
	    width:100%
	  }
	.padding-sm{
		padding:30px;
	}
	.no-margin-bottom{
		margin-bottom: 0!important;
		padding-bottom: 0!important;
	}
	.indent{
    	text-indent: 15px;
	}

	.dataTables_wrapper { font-size: 12px!important; }

	.page-content{
		/*width: 98%!important;*/
	}
	.header-links{
		color:white!important;
		font-size: 16px!important;
	  	font-weight: bold;
	}

	.header-links:hover{
		color:black!important;
		/*font-size: 16px!important;*/
	  	/*font-weight: bold;*/
	}

	.bold-links{
		color: inherit;
		font-weight: bold;
	    text-decoration: none;
	}

</style>