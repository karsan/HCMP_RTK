<style>
	.login-button{
		font-size: 16px;
		color:white;
	}

	.login-button:hover{
		font-size: 16px;
		color:#4B77BE !important;
	}
</style>
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="<?php echo base_url(); ?>" style="width: 110px!important;">
				<img src="<?php echo base_url().'assets/img/coat_of_arms_2016.png'; ?>" alt="logo" class="logo"/>
				<p class="logo-text">RTK</p>
			</a>
			<div class="menu-toggler sidebar-toggler hide">
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
			<!-- 
				<li class="header-links">
					<a href="<?php echo base_url().'dashboardv2/index'; ?>" class="">
						NATIONAL
					</a>
				</li>
				<li class="header-links">
					<a href="<?php echo base_url().'dashboardv2/county_dashboard'; ?>" class="">
						COUNTY
					</a>
				</li>
				<li class="header-links">
					<a href="<?php echo base_url().'dashboardv2/subcounty_dashboard'; ?>" class="">
						SUB-COUNTY
					</a>
				</li>
				<li class="header-links">
					<a href="<?php echo base_url().'dashboardv2/facility_dashboard'; ?>" class="">
						FACILITIES
					</a>
				</li>
				
				-->
				<li class="header-links">
					<a href="<?php echo base_url().'user'; ?>" class="login-button clearfix">
					<p class="pull-left">LOGIN</p>
					</a>
				</li>
			</ul> 
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER