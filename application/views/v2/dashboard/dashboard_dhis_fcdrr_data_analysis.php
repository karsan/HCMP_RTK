<?php $this->load->view('v2/dashboard/dashboard_header'); ?>
<?php //echo "<pre>";print_r($county_id);exit; ?>
<?php //echo $test_graph;exit; ?>

<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solid  page-sidebar-closed">
	<style>
		.red{
			color:red!important;
		}
		.green{
			color:green!important;
		}
		.btn{
			width: 100%;
		}
	</style>
<?php $this->load->view('v2/dashboard/dashboard_top_header'); ?>
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->

<div class="page-container">
<?php $this->load->view('v2/dashboard/dashboard_sidebar'); ?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- <div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="<?php //echo base_url(); ?>">Dashboard</a>
					</li>
				</ul>
			</div> -->
						
			<h3 class="page-title no-margin">
			DHIS FCDRR Data Analysis Dashboard 
			<!-- <small> data as at <?php //echo date('F/Y') ?> </small> -->
			<!-- <small> data as of <?php echo date('F Y', strtotime("-1 MONTH")) ?> reports. </small> -->
			</h3></br>
			<!-- END PAGE HEADER-->

		<!-- 
			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<span class="caption-subject font-green-haze bold uppercase">Screening DHIS FCDRR Data</span>
							<span class="caption-helper"><?php echo $title_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table a_datatable display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>#</th>
										<th>County</th>
										<th>Subcounty</th>
										<th>MFL</th>
										<th>DHIS Code</th>
										<th>Facility Name</th>

										<th>Beginning Balance</th>
										<th>Quantity Used</th>
										<th>Tests Done</th>
										<th>Quantity Requested</th>
										<th >Closing Balance</th>
										
										<th>Order Date</th>
										<th>RTK Status</th>
										<th>Data Retrieval Date</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 

										foreach ($dhis_data_analysis[4] as $key => $value) { 
											$order_date = '01-'.$value['month'].'-'.$value['year'];
											$rtk_check = $value['rtk_check'];
											$rtk_status = (isset($rtk_check) && $rtk_check > 0)? "PRESENT":"ABSENT";
											$check_class = ($rtk_check>0)? " green ":" red ";
									?>
									<tr>
										<td></td>
										<td><?php echo $value['county']; ?></td>
										<td><?php echo $value['district']; ?></td>
										<td><?php echo $value['facility_code']; ?></td>
										<td><?php echo $value['dhis_code']; ?></td>
										<td><?php echo $value['facility_name']; ?></td>
										
										<td><?php echo number_format($value['beginning_bal']) ?></td>
										<td><?php echo number_format($value['q_used']) ?></td>
										<td><?php echo number_format($value['no_of_tests_done']) ?></td>
										<td><?php echo number_format($value['q_requested']) ?></td>
										<td><?php echo number_format($value['closing_stock']) ?></td>

										<td><?php echo date('F Y',strtotime($order_date)) ?></td>
										<td class="<?php echo $check_class; ?>"><strong><?php echo $rtk_status; ?></strong></td>
										<td><?php echo date('d F Y'); ?></td>

										<td><button class="btn btn-primary" >Import</button></td>
									</tr>
									<?php } ?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<span class="caption-subject font-green-haze bold uppercase">Confirmatory DHIS FCDRR Data</span>
							<span class="caption-helper"><?php echo $title_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table b_datatable display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>#</th>
										<th>County</th>
										<th>Subcounty</th>
										<th>MFL</th>
										<th>DHIS Code</th>
										<th>Facility Name</th>

										<th>Beginning Balance</th>
										<th>Quantity Used</th>
										<th>Tests Done</th>
										<th>Quantity Requested</th>
										<th >Closing Balance</th>
										
										<th>Order Date</th>
										<th>RTK Status</th>
										<th>Data Retrieval Date</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										foreach ($dhis_data_analysis[5] as $key => $value) { 
											$order_date = '01-'.$value['month'].'-'.$value['year'];
											$rtk_check = $value['rtk_check'];
											$rtk_status = (isset($rtk_check) && $rtk_check > 0)? "PRESENT":"ABSENT";

											$check_class = ($rtk_check>0)? " green ":" red ";
									?>
									<tr>
										<td></td>
										<td><?php echo $value['county']; ?></td>
										<td><?php echo $value['district']; ?></td>
										<td><?php echo $value['facility_code']; ?></td>
										<td><?php echo $value['dhis_code']; ?></td>
										<td><?php echo $value['facility_name']; ?></td>
										
										<td><?php echo number_format($value['beginning_bal']) ?></td>
										<td><?php echo number_format($value['q_used']) ?></td>
										<td><?php echo number_format($value['no_of_tests_done']) ?></td>
										<td><?php echo number_format($value['q_requested']) ?></td>
										<td><?php echo number_format($value['closing_stock']) ?></td>

										<td><?php echo date('F Y',strtotime($order_date)) ?></td>
										<td class="<?php echo $check_class; ?>"><strong><?php echo $rtk_status; ?></strong></td>
										<td><?php echo date('d F Y'); ?></td>

										<td><button class="btn btn-primary" >Import</button></td>
									</tr>
									<?php } ?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>
 		-->
			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-6 no-padding">
					<div class="portlet light clearfix">
						<div class="portlet-title">
							<div class="caption">
								<!-- <i class="icon-share font-red-sunglo hide"></i> -->
								<span class="caption-subject font-green-haze bold uppercase">Quantity Received Differences</span>
								<!-- <span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span> -->
								<span class="caption-helper"><?php echo $title_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="col-md-12 no-spacing" style="">
								<div class="col-md-6 no-padding">
									<div id="site_activities_content" class="">
										<div id="quantity_received_graph_scr" style="height: 300px;"></div>
									</div>
								</div>
								<div class="col-md-6 no-padding">
									<div id="site_activities_content" class="">
										<div id="quantity_received_graph_conf" style="height: 300px;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6 no-padding">
					<div class="portlet light clearfix">
						<div class="portlet-title">
							<div class="caption">
								<!-- <i class="icon-share font-red-sunglo hide"></i> -->
								<span class="caption-subject font-green-haze bold uppercase">Quantity Used Differences</span>
								<!-- <span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span> -->
								<span class="caption-helper"><?php echo $title_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="col-md-12 no-spacing" style="">
								<div class="col-md-6 no-padding">
									<div id="site_activities_content" class="">
										<div id="quantity_used_graph_scr" style="height: 300px;"></div>
									</div>
								</div>
								<div class="col-md-6 no-padding">
									<div id="site_activities_content" class="">
										<div id="quantity_used_graph_conf" style="height: 300px;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-6 no-padding">
					<div class="portlet light clearfix">
						<div class="portlet-title">
							<div class="caption">
								<!-- <i class="icon-share font-red-sunglo hide"></i> -->
								<span class="caption-subject font-green-haze bold uppercase">Beginning Balance Differences</span>
								<span class="caption-helper">Beginning Balance Date: <?php echo date('F Y',strtotime($beginning_balance_date_rtk)); ?></span>
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->
							</div>
						</div>
						<div class="portlet-body">
							<div class="col-md-12 no-spacing" style="">
								<div class="col-md-6 no-padding">
									<div id="site_activities_content" class="">
										<div id="beginning_balances_graph_scr" style="height: 300px;"></div>
									</div>
								</div>
								<div class="col-md-6 no-padding">
									<div id="site_activities_content" class="">
										<div id="beginning_balances_graph_conf" style="height: 300px;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6 no-padding">
					<div class="portlet light clearfix">
						<div class="portlet-title">
							<div class="caption">
								<!-- <i class="icon-share font-red-sunglo hide"></i> -->
								<span class="caption-subject font-green-haze bold uppercase">Closing Balance Differences</span>
								<!-- <span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span> -->
								<span class="caption-helper">Closing Balance Date: <?php echo date('F Y',strtotime($closing_balance_date_rtk)); ?></span>
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->
							</div>
						</div>
						<div class="portlet-body">
							<div class="col-md-12 no-spacing" style="">
								<div class="col-md-6 no-padding">
									<div id="site_activities_content" class="">
										<div id="closing_balance_graph_scr" style="height: 300px;"></div>
									</div>
								</div>
								<div class="col-md-6 no-padding">
									<div id="site_activities_content" class="">
										<div id="closing_balance_graph_conf" style="height: 300px;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<!-- <i class="icon-share font-red-sunglo hide"></i> -->
							<span class="caption-subject font-green-haze bold uppercase">Screening Beginning Balance Differences</span>
							<!-- <span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span> -->
							<span class="caption-helper"><?php echo $title_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table c_datatable display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th rowspan="2">#</th>
										<th rowspan="2">County</th>
										<th rowspan="2">Subcounty</th>
										<th rowspan="2">MFL</th>
										<th rowspan="2">Facility Name</th>
										<th rowspan="2">Order Date</th>

										<th colspan="2">Beginning Balances</th>
										<th rowspan="2">Difference</th>
									</tr>
									<tr>
										<th>DHIS</th>
										<th>RTK</th>
									</tr>
								</thead>
								<tbody>
									<?php 

										foreach ($dhis_data_analysis[4] as $key => $value) { 
											$order_date = $value['year'].'-'.$value['month'].'-01';
											$dhis_bal = $value['dhis_data']['beginning_bal'];
											$rtk_bal = $value['rtk_data']['beginning_bal'];

											$class_c = ($difference_c>0)? " green ":" red ";
											$class_c = ($difference_c==0)? "":$class_c;

											$diff = $dhis_bal - $rtk_bal;
											$rtk_status = (isset($diff) && $diff > 0)? "PRESENT":"ABSENT";
											$check_class = ($diff>0)? " green ":" red ";
									?>
									<tr>
										<td></td>
										<td><?php echo $value['county']; ?></td>
										<td><?php echo $value['district']; ?></td>
										<td><?php echo $value['facility_code']; ?></td>
										<td><?php echo $value['facility_name']; ?></td>
										<td><?php echo date('F Y',strtotime($order_date)) ?></td>
										<td><?php echo number_format($dhis_bal) ?></td>
										<td><?php echo number_format($rtk_bal) ?></td>
										<td class="<?php echo $check_class; ?>"><strong><?php echo $diff ?></strong></td>
									</tr>
									<?php } ?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<!-- <i class="icon-share font-red-sunglo hide"></i> -->
							<span class="caption-subject font-green-haze bold uppercase">Confirmatory Beginning Balance Differences</span>
							<!-- <span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span> -->
							<span class="caption-helper"><?php echo $title_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table d_datatable display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th rowspan="2">#</th>
										<th rowspan="2">County</th>
										<th rowspan="2">Subcounty</th>
										<th rowspan="2">MFL</th>
										<th rowspan="2">Facility Name</th>
										<th rowspan="2">Order Date</th>

										<th colspan="2">Beginning Balances</th>
										<th rowspan="2">Difference</th>
									</tr>
									<tr>
										<th>DHIS</th>
										<th>RTK</th>
									</tr>
								</thead>
								<tbody>
									<?php 

										foreach ($dhis_data_analysis[5] as $key => $value) { 										
											$order_date = $value['year'].'-'.$value['month'].'-01';
											$dhis_bal = $value['dhis_data']['beginning_bal'];
											$rtk_bal = $value['rtk_data']['beginning_bal'];

											$class_c = ($difference_c>0)? " green ":" red ";
											$class_c = ($difference_c==0)? "":$class_c;

											$diff = $dhis_bal - $rtk_bal;
											$rtk_status = (isset($diff) && $diff > 0)? "PRESENT":"ABSENT";
											$check_class = ($diff>0)? " green ":" red ";
									?>
									<tr>
										<td></td>
										<td><?php echo $value['county']; ?></td>
										<td><?php echo $value['district']; ?></td>
										<td><?php echo $value['facility_code']; ?></td>
										<td><?php echo $value['facility_name']; ?></td>
										<td><?php echo date('F Y',strtotime($order_date)) ?></td>
										
										<td><?php echo number_format($dhis_bal) ?></td>
										<td><?php echo number_format($rtk_bal) ?></td>
										<td class="<?php echo $check_class; ?>"><strong><?php echo $diff ?></strong></td>
									</tr>
									<?php } ?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<!-- <i class="icon-share font-red-sunglo hide"></i> -->
							<span class="caption-subject font-green-haze bold uppercase">Screening Quantity Received Differences</span>
							<!-- <span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span> -->
							<span class="caption-helper"><?php echo $title_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table e_datatable display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th rowspan="2">#</th>
										<th rowspan="2">County</th>
										<th rowspan="2">Subcounty</th>
										<th rowspan="2">MFL</th>
										<th rowspan="2">Facility Name</th>
										<th rowspan="2">Order Date</th>

										<th colspan="2">Quantity Received</th>
										<th rowspan="2">Difference</th>
									</tr>
									<tr>
										<th>DHIS</th>
										<th>RTK</th>
									</tr>
								</thead>
								<tbody>
									<?php 

										foreach ($dhis_data_analysis[4] as $key => $value) { 
											$order_date = $value['year'].'-'.$value['month'].'-01';

											$dhis_received = $value['dhis_data']['q_received'];
											$rtk_received = $value['rtk_data']['q_received'];

											$diff = $dhis_received - $rtk_received;
											$rtk_status = (isset($diff) && $diff > 0)? "PRESENT":"ABSENT";
											$check_class = ($diff>0)? " green ":" red ";
									?>
									<tr>
										<td></td>
										<td><?php echo $value['county']; ?></td>
										<td><?php echo $value['district']; ?></td>
										<td><?php echo $value['facility_code']; ?></td>
										<td><?php echo $value['facility_name']; ?></td>
										<td><?php echo date('F Y',strtotime($order_date)) ?></td>
										
										<td><?php echo number_format($dhis_received) ?></td>
										<td><?php echo number_format($rtk_received) ?></td>
										<td class="<?php echo $check_class; ?>"><strong><?php echo $diff ?></strong></td>
									</tr>
									<?php } ?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<!-- <i class="icon-share font-red-sunglo hide"></i> -->
							<span class="caption-subject font-green-haze bold uppercase">Confirmatory Quantity Received Differences</span>
							<!-- <span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span> -->
							<span class="caption-helper"><?php echo $title_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table f_datatable display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th rowspan="2">#</th>
										<th rowspan="2">County</th>
										<th rowspan="2">Subcounty</th>
										<th rowspan="2">MFL</th>
										<th rowspan="2">Facility Name</th>
										<th rowspan="2">Order Date</th>

										<th colspan="2">Beginning Balances</th>
										<th rowspan="2">Difference</th>
									</tr>
									<tr>
										<th>DHIS</th>
										<th>RTK</th>
									</tr>
								</thead>
								<tbody>
									<?php 

										foreach ($dhis_data_analysis[5] as $key => $value) { 
											$order_date = $value['year'].'-'.$value['month'].'-01';
											$dhis_received = $value['dhis_data']['q_received'];
											$rtk_received = $value['rtk_data']['q_received'];

											$diff = $dhis_received - $rtk_received;
											$rtk_status = (isset($diff) && $diff > 0)? "PRESENT":"ABSENT";
											$check_class = ($diff>0)? " green ":" red ";
									?>
									<tr>
										<td></td>
										<td><?php echo $value['county']; ?></td>
										<td><?php echo $value['district']; ?></td>
										<td><?php echo $value['facility_code']; ?></td>
										<td><?php echo $value['facility_name']; ?></td>
										<td><?php echo date('F Y',strtotime($order_date)) ?></td>
										
										<td><?php echo number_format($dhis_received) ?></td>
										<td><?php echo number_format($rtk_received) ?></td>
										<td class="<?php echo $check_class; ?>"><strong><?php echo $diff ?></strong></td>
									</tr>
									<?php } ?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>

		</div>
	</div>
</div>


<div class="modal fade view-details-modal" id="view-details-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
        <h3 class="modal-title" id="myLargeModalLabel">DHIS FCDRR Analysis</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>

      <div class="modal-body clearfix" >
      	<div id="modal-body-data">
        	<!-- <i class="fa fa-cog fa-spin"></i> -->
      	</div>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div> -->
  </div>
</div>


<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 <?php echo date('Y'); ?> &copy; RTK.
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
<?php $this->load->view('v2/dashboard/dashboard_footer'); ?>
</html>

<script>
	
</script>

<script>
var url ='<?php echo base_url()?>';
		// alert("I work");

$(function () { 
	<?php echo $beginning_balances_graph_scr; ?>
	<?php echo $beginning_balances_graph_conf; ?>
	<?php echo $quantity_used_graph_scr; ?>
	<?php echo $quantity_used_graph_conf; ?>
	<?php echo $quantity_received_graph_scr; ?>
	<?php echo $quantity_received_graph_conf; ?>
	<?php echo $closing_balance_graph_scr; ?>
	<?php echo $closing_balance_graph_conf; ?>


    // $('.datatable').DataTable();
    /*DATATABLES*/
	    var a = $('.a_datatable').DataTable( {
	        "columnDefs": [ {
	            "searchable": false,
	            "orderable": false,
	            "targets": 0
	        } ],
	        "order": [[ 1, 'asc' ]]
	    } );
	 
	    a.on( 'order.dt search.dt', function () {
	        a.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = i+1;
	        } );
	    } ).draw();

	    var a = $('.b_datatable').DataTable( {
	        "columnDefs": [ {
	            "searchable": false,
	            "orderable": false,
	            "targets": 0
	        } ],
	        "order": [[ 1, 'asc' ]]
	    } );
	 
	    a.on( 'order.dt search.dt', function () {
	        a.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = i+1;
	        } );
	    } ).draw();

	    var a = $('.c_datatable').DataTable( {
	        "columnDefs": [ {
	            "searchable": false,
	            "orderable": false,
	            "targets": 0
	        } ],
	        "order": [[ 1, 'asc' ]]
	    } );
	 
	    a.on( 'order.dt search.dt', function () {
	        a.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = i+1;
	        } );
	    } ).draw();

	    var a = $('.d_datatable').DataTable( {
	        "columnDefs": [ {
	            "searchable": false,
	            "orderable": false,
	            "targets": 0
	        } ],
	        "order": [[ 1, 'asc' ]]
	    } );
	 
	    a.on( 'order.dt search.dt', function () {
	        a.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = i+1;
	        } );
	    } ).draw();

	    var a = $('.e_datatable').DataTable( {
	        "columnDefs": [ {
	            "searchable": false,
	            "orderable": false,
	            "targets": 0
	        } ],
	        "order": [[ 1, 'asc' ]]
	    } );
	 
	    a.on( 'order.dt search.dt', function () {
	        a.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = i+1;
	        } );
	    } ).draw();

	    var a = $('.f_datatable').DataTable( {
	        "columnDefs": [ {
	            "searchable": false,
	            "orderable": false,
	            "targets": 0
	        } ],
	        "order": [[ 1, 'asc' ]]
	    } );
	 
	    a.on( 'order.dt search.dt', function () {
	        a.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = i+1;
	        } );
	    } ).draw();

	    /*var a = $('.b_datatable').DataTable( {
	        "columnDefs": [ {
	            "searchable": false,
	            "orderable": false,
	            "targets": 0
	        } ],
	        "order": [[ 1, 'asc' ]],
	        dom: 'Bfrtip',
	        buttons: [
		        'copy', 'excel', 'pdf'
		    ],
	        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // converting to interger to find total
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // computing column Total of the complete result 
				
	     	var issuedTotalOld = api
            .column( 5 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

            var issuedTotalNew = api
            .column( 6 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

            var issuedDiff = api
            .column( 7 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

            var requestedTotalOld = api
            .column( 8 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

            var requestedTotalNew = api
            .column( 9 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

            var requestedDiff = api
            .column( 10 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );
			
				
            // Update footer by showing the total with the reference of the column index 
	    $( api.column( 0 ).footer() ).html('Total');
            
            $( api.column( 5 ).footer() ).html(issuedTotalOld);
            $( api.column( 6 ).footer() ).html(issuedTotalNew);
            $( api.column( 7 ).footer() ).html(issuedDiff);
            $( api.column( 8 ).footer() ).html(requestedTotalOld);
            $( api.column( 9 ).footer() ).html(requestedTotalNew);
            $( api.column( 10 ).footer() ).html(requestedDiff);
        }
	    } );
	 
	    a.on( 'order.dt search.dt', function () {
	        a.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = i+1;
	        } );
	    } ).draw();*/
    /*END OF DATATABLES*/


    $('#view-details-modal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var first_key = button.data('first-key') // Extract info from data-* attributes
		var second_key = button.data('second-key') // Extract info from data-* attributes
		var commodity_id = button.data('commodity-id') // Extract info from data-* attributes
		var delivery_note = button.data('delivery-note') // Extract info from data-* attributes
		if (quarter = "") {quarter = 'NULL'}
		var delivery_note = encodeURIComponent(button.data('delivery-note')) // Extract info from data-* attributes

		// console.log('dashboardv2/get_kemsa_issue_data_analysis_modal/'+first_key+'/'+second_key+'/'+commodity_id+'/'+delivery_note);
		var modal = $(this)
		ajax_request_replace_div_content('dashboardv2/get_kemsa_issue_data_analysis_modal/'+first_key+'/'+second_key+'/'+commodity_id+'/'+delivery_note,"#modal-body-data");

	})

	<?php if($filtered > 0): ?>
	<?php echo $utilization_graph_scr; ?>;
	<?php echo $utilization_graph_conf; ?>;

	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/'+subcounty_id+'/NULL/4/NULL/NULL/scr_consumption_chart_filtered',"#scr_consumption_chart_filtered"); 

	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/'+subcounty_id+'/NULL/5/NULL/NULL/conf_consumption_chart_filtered',"#conf_consumption_chart_filtered"); 

    <?php endif; ?>
	<?php //echo $utilization_bgg_screening_graph; ?>;
	<?php //echo $utilization_bgg_confirmatory_graph; ?>;
	<?php //echo $test_graph; ?>;
	<?php //echo $drawing_rights_graph; ?>;
	<?php //echo $drawing_utilization_graph; ?>;

	$(".select2").select2({
          placeholder: "0",
          // containerCssClass: "float-left" 
        });

	/*START OF INHERIT*/
	$('.top_filter_button').button().click(function(x){
          x.preventDefault(); 

          // var county_text = $('#county_filter option:selected').text();
          // var subcounty_text = $('#sub_county_filter option:selected').text();

          // var county_id = $('#county_filter option:selected').val();
          // var subcounty_id = $('#sub_county_filter option:selected').val();
          // var facility_id = $('#facility_id option:selected').val();
          var quarter = $('#quarter_filter option:selected').val();
          // console.log(quarter);
          if(quarter==0){
            window.location.replace(url+"dashboardv2/issues_discrepancy_dashboard");
          }else{
            	window.location.replace(url+"dashboardv2/issues_discrepancy_dashboard/NULL/NULL/"+quarter);
          }
          // alert(county_id);
          // alert(subcounty_id);
       });

});/*END OF JQUERY FUNCTION*/

var county_id = <?php echo json_encode($county_id); ?>;
var subcounty_id = <?php echo json_encode($subcounty_id); ?>;
// console.log(subcounty_id);
if (subcounty_id > 0) { 
		ajax_request_replace_div_content('dashboardv2/get_national_trend/NULL/'+subcounty_id,"#trend-chart"); 
		ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/subcounty/NULL/'+subcounty_id+'/NULL/4/NULL/NULL/scr_consumption_chart',"#scr_consumption_chart"); 
		ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/subcounty/NULL/'+subcounty_id+'/NULL/5/NULL/NULL/conf_consumption_chart',"#conf_consumption_chart"); 
}else{
	// ajax_request_replace_div_content('dashboardv2/get_national_trend/',"#trend-chart"); 
	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/subcounty/NULL/NULL/NULL/4/NULL/NULL/scr_consumption_chart',"#scr_consumption_chart"); 
	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/subcounty/NULL/NULL/NULL/5/NULL/NULL/conf_consumption_chart',"#conf_consumption_chart"); 
}

function ajax_request_replace_div_content(function_url,div){
	var function_url =url+function_url;
	var loading_icon=url+"assets/img/loader2.gif";
	$.ajax({
		type: "POST",
		url: function_url,
		beforeSend: function() {
			$(div).html("<img style='margin-left:20%;' src="+loading_icon+">");
		},
		success: function(msg) {
			// console.log(msg);
			$(div).html(msg);
			$('#datatable').DataTable(); 
		}
	});
} 

</script>