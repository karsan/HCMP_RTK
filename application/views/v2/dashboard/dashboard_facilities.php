<?php $this->load->view('v2/dashboard/dashboard_header'); ?>
<?php //echo "<pre>";print_r($test_graph);exit; ?>
<?php //echo $test_graph;exit; ?>

<style>
	.logo{
		 width: 45px;
    	float: left;
	}

	.logo-text{
		margin: 10px 0 0 5px;
		color: white;
	    font-size: 18px;
	    font-weight: bold;
	    text-decoration: none;
    	float: left;
	}
	.no-margin{
		margin:0!important;
	}
	.no-padding{
		padding:0!important;
	}

	.select2{
		float:left!important;
		width:100%!important;
  	}

  	/** Select2 **/
	.select2-container--default .select2-selection--single,
	.select2-container--default .select2-selection--multiple {
	  background-color: #fff;
	  border: 1px solid #ccc;
	  border-radius: 0;
	  min-height: 38px;
	}

	.select2-container--default .select2-selection--single .select2-selection__rendered {
	  color: #73879C;
	  padding-top: 5px;
	}

	.select2-container--default .select2-selection--multiple .select2-selection__rendered {
	  padding-top: 3px;
	}

	.select2-container--default .select2-selection--single .select2-selection__arrow {
	  height: 36px;
	}

	.select2-container--default .select2-selection--multiple .select2-selection__choice,
	.select2-container--default .select2-selection--multiple .select2-selection__clear {
	  margin-top: 2px;
	  border: none;
	  border-radius: 0;
	  padding: 3px 5px;
	}

	.select2-container--default.select2-container--focus .select2-selection--multiple {
	  border: 1px solid #ccc;
	}
	/** /Select2 **/

	.filter_button{
	    padding:9px 22px!important;
	    margin:0;
	    float:left;
	    border-radius:0px;
	    width:100%
	  }
	.padding-sm{
		padding:30px;
	}
	.no-margin-bottom{
		margin-bottom: 0!important;
		padding-bottom: 0!important;
	}
	.indent{
    	text-indent: 15px;
	}
</style>
<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solid  page-sidebar-closed">
<?php $this->load->view('v2/dashboard/dashboard_top_header'); ?>
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->

<div class="page-container">
<?php $this->load->view('v2/dashboard/dashboard_sidebar'); ?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- <div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="<?php //echo base_url(); ?>">Dashboard</a>
					</li>
				</ul>
			</div> -->
						
			<h3 class="page-title no-margin">
			Facilities Dashboard 
			<!-- <small> data as at <?php //echo date('F/Y') ?> </small> -->
			<small> data as of <?php echo date('F Y', strtotime("-1 MONTH")) ?> reports. </small>
			</h3></br>
			<!-- END PAGE HEADER-->

			<div class="clearfix"></div>
			<div class="row portlet light no-margin-bottom">
			<div class="portlet-body margin-sm-hor clearfix">
			<?php if ($quartered > 0) { ?>
			<div class="col-md-12 no-padding clearfix">
				<strong><p class="no-margin" style="line-height: 1.5em"> <span class="font-blue-steel">*</span> Fields with an asterisk do not change when filtered quarterly.</p></strong>
			</div>
			<?php } ?>

                  <div class="col-md-5 padding-hor-sm no-margin no-padding">
                  <select class="form-control select2 padding-hor-sm" id="county_filter">
                  <option value="0">All Counties</option>
                  <?php foreach ($county_data as $county => $value):?> 
                      <option value="<?php echo $value['id']; ?>"><?php echo $value['county']; ?></option>
                  <?php endforeach; ?>
                  </select>
                  </div>

                  <div class="col-md-5 padding-hor-sm no-margin no-padding">
                    <!-- <select class="form-control select2 padding-hor-sm" id="sub_county_filter"> -->
                    <!-- 
                    <select class="form-control select2 padding-hor-sm" id="">
                    <option value="0">All Subcounties</option>
                    </select> 
                    -->
                    <select class="form-control select2 padding-hor-sm" id="quarter_filter">
                    <option value="0">All quarters</option>
                    <option value="1">Quarter 1</option>
                    <option value="2">Quarter 2</option>
                    <option value="3">Quarter 3</option>
                    <option value="4">Quarter 4</option>
                    </select> 

                  </div>
                 <div class="col-md-2 no-padding no-margin">
                    <button class="btn btn-primary top_filter_button filter_button">Filter</button>
                  </div>
			</div>
			
            </div>

			<div class="clearfix"></div>
			<div class="row portlet light">

			<!-- <div class="col-md-12"> -->
			<div class="col-md-6">
				<!-- BEGIN SAMPLE TABLE PORTLET-->
				<div class="portlet light no-padding">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-share font-red-sunglo hide"></i>
							<!-- <span class="caption-subject font-blue-steel bold uppercase"><?php echo $title_prepend; ?> Summary</span> -->
							<span class="caption-subject font-blue-steel bold uppercase"><?php echo $title_prepend; ?> Stock Card </span>
								<span class="caption-helper"> <?php echo $title_append ?> </span>
							<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-scrollable">
							<table class="table table-bordered table-hover">
							<thead>
								<th></th>
								<th>Screening</th>
								<th>Confirmatory</th>
							</thead>
							
							<tbody>
							
							<tr>
								<td>
									 <strong>Tests Done</strong>
								</td>
								<td>
									 <?php echo number_format($screening_tests_done); ?>
								</td>
								<td>
									 <?php echo number_format($confirmatory_tests_done); ?>
								</td>
							</tr>

							<!-- <tr>
								<td>
									 <strong>Closing balance</strong>
								</td>
								<td>
									 <?php echo number_format($screening_closing_balance); ?>
								</td>
								<td>
									 <?php echo number_format($confirmatory_closing_balance); ?>
								</td>
							</tr> -->

							<!-- <tr>
								<td>
									 <strong>Requested</strong>
								</td>
								<td>
									 <?php echo $screening_requested = number_format($screening_requested); ?>
								</td>
								<td>
									 <?php echo $confirmatory_requested = number_format($confirmatory_requested); ?>
								</td>
							</tr> -->

							<tr>
								<td>
									 <strong>Expiries</strong>
								</td>
								<td>
									 <?php echo number_format($screening_expiring); ?>
								</td>
								<td>
									 <?php echo number_format($confirmatory_expiring); ?>
								</td>
							</tr>

							</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- END SAMPLE TABLE PORTLET-->
			</div>

			<div class="col-md-6">
			<!-- BEGIN SAMPLE TABLE PORTLET-->
			<div class="portlet light ">
				<div class="portlet-title">
					<div class="caption">
						<span class="caption-subject font-blue-steel bold uppercase"><?php echo $title_prepend; ?> Summary </span>
						<!-- <span class="caption-subject font-blue-steel bold uppercase"><?php echo $title_prepend; ?> Drawing Rights Summary</span> -->
							<span class="caption-helper"><strong><?php echo $title_append; ?></strong></span>
						<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->
					</div>
				</div>
				<div class="portlet-body">
				<?php
					if ($county_id > 0) {
						$t = "Total allocated by county.";
					}else{
						$t = "Total allocated by counties.";
					}

					if ($quartered > 0) {
						$a = "Quarter balance.";
					}else{
						$a = "Available balance for next allocation.";
					}

					
				?>
					<div class="table-scrollable">
						<table class="table table-bordered table-hover">
						<thead>
							<th></th>
							<th>Screening</th>
							<th>Confirmatory</th>
						</thead>
						
						<tbody>
						
						<tr>
							<td>
								 <strong>Total drawing rights</strong>
							</td>
							<td>
								<?php echo number_format($drawing_total_screening); ?>
							</td>
							<td>
								<?php echo number_format($drawing_total_confirmatory); ?>
							</td>
						</tr>

						<tr>
							<td>
								 <strong><?php echo $t; ?></strong>
							</td>
							<td>
									 <?php //echo number_format($drawing_distributed_screening); ?>
									 <?php echo number_format($total_allocated_screening); ?>
							</td>
							<td>
									 <?php //echo number_format($drawing_distributed_confirmatory); ?>
									 <?php echo number_format($total_allocated_confirmatory); ?>
							</td>
						</tr>

						<tr>
							<td class="indent">
								 <strong>- Issued from KEMSA.</strong>
							</td>
							<td>
								<?php echo number_format($kemsa_issued_screening); ?>
							</td>
							<td>
								<?php echo number_format($kemsa_issued_confirmatory); ?>
							</td>
						</tr>

						<tr>
							<td class="indent">
								 <strong>- Received at site.</strong>
							</td>
							<td>
									 <?php echo number_format($received_site_screening); ?>
							</td>
							<td>
									 <?php echo number_format($received_site_confirmatory); ?>
							</td>
						</tr>

						<tr>
							<td class="indent">
								 <strong>- Consumed at site.</strong>
							</td>
							<td>
									 <?php echo number_format($consumed_site_screening); ?>
							</td>
							<td>
									 <?php echo number_format($consumed_site_confirmatory); ?>
							</td>
						</tr>

						<tr>
							<td class="indent">
								 <strong>- Closing balance.</strong>
							</td>
							<td>
								<?php echo number_format($closing_balance_screening); ?>
							</td>
							<td>
								<?php echo number_format($closing_balance_confirmatory); ?>
							</td>
						</tr>

						<tr>
							<td>
								 <strong> <?php echo $a; ?> </strong>
							</td>
							<td>
									 <?php echo number_format($drawing_balance_screening); ?>
							</td>
							<td>
									 <?php echo number_format($drawing_balance_confirmatory); ?>
							</td>
						</tr>

						</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- END SAMPLE TABLE PORTLET-->
			</div>

			<!-- END PORTLET-->
				
			</div>
				

			<div class="row portlet light">
			<div class="col-md-12 col-sm-12 no-margin no-padding">
			
			<!-- BEGIN PORTLET-->
			<div class="portlet light ">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-share font-red-sunglo hide"></i>
						<!-- <span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Utilization vs Received. </span> -->
						<!-- <span class="caption-subject font-green-haze bold uppercase">Allocated vs Utilized vs Received. </span> -->
						<span class="caption-subject font-green-haze bold uppercase">Allocated vs Received vs Consumed </span>
						<span class="caption-helper"><?php echo $title_append; ?></span>

					</div>
				</div>
				<div class="portlet-body">
					<div class="col-md-12 no-padding">
						<div class="col-md-6 no-padding">
							<div id="site_activities_content" class="">
								<div id="utilization_graph_scr" style="height: 350px;"></div>
							</div>
						</div>
						<div class="col-md-6 no-padding">
							<div id="site_activities_content" class="">
								<div id="utilization_graph_conf" style="height: 350px;"></div>
							</div>
						</div>
						<!-- <div class="col-md-12 no-padding">
							<div id="site_activities_content" class="">
								<div id="container" style="height: 350px;"></div>
							</div>
						</div> -->
					</div>
				</div>
			</div>
			<!-- END PORTLET-->
				
			</div>
				
			</div>
			<div class="clearfix"></div>
			<div class="row">
	
			<div class="col-md-12 no-padding">
			<!-- BEGIN PORTLET-->
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Consumption Trend</span>
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->

								<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<!-- <div id="site_activities_loading">
								<img src="<?php echo base_url().'assets/v2/dashboard/'; ?>admin/layout/img/loading.gif" alt="loading"/>
							</div> -->
							<div id="site_activities_content" class="">
								<div id="consumption-trend-chart" style="height:450px;"></div>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
			</div>
			</div>

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 col-sm-12">
			<!-- BEGIN PORTLET-->
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Reporting Rates Trend</span>
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->
								<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>

							</div>
						</div>
						<div class="portlet-body">
							<!-- <div id="site_activities_loading">
								<img src="<?php echo base_url().'assets/v2/dashboard/'; ?>admin/layout/img/loading.gif" alt="loading"/>
							</div> -->
							<div id="site_activities_content" class="">
								<div id="trend-chart" style="height:450px;"></div>
								<!-- <div id="trend-chart-script" style="height:450px;"></div> -->
							</div>
						</div>
					</div>
					<!-- END PORTLET-->

			</div>
			</div>

			<h3 class="page-title no-margin">
			Allocations 
			<!-- <small> data as at <?php echo date('l/m/Y', strtotime("-1 MONTH")); ?></small> -->
			<small> <?php echo date('Y', strtotime("-1 MONTH")); ?></small>
			</h3></br>

			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<!-- BEGIN PORTLET-->
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Total Drawing Rights vs Total Allocated (Screening). </span>
								</br>
								<span class="caption-helper"> Quarters: <?php echo $drawing_rights_quarter." ".$title_append; ?></span>
								<!-- <span class="caption-helper"><?php //echo $title_append; ?></span> -->
							</div>
						</div>
						<div class="portlet-body">
							<!-- <div id="site_activities_loading">
								<img src="<?php echo base_url().'assets/v2/dashboard/'; ?>admin/layout/img/loading.gif" alt="loading"/>
							</div> -->
							<div id="site_activities_content" class="">
								<div id="utilization_bgg_screening_graph" style="height: 550px;"></div>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
			</div>

			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<!-- BEGIN PORTLET-->
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Total Drawing Rights vs Total Allocated (Confirmatory)</span>
								</br>
								<span class="caption-helper"> Quarters: <?php echo $drawing_rights_quarter." ".$title_append; ?></span>
								<span class="caption-helper"><?php //echo $title_append; ?></span>
								<!-- <span class="caption-helper">as at <?php echo date('l/m/Y') ?></span> -->
							</div>
						</div>
						<div class="portlet-body">
							<!-- <div id="site_activities_loading">
								<img src="<?php echo base_url().'assets/v2/dashboard/'; ?>admin/layout/img/loading.gif" alt="loading"/>
							</div> -->
							<div id="site_activities_content" class="">
								<div id="utilization_bgg_confirmatory_graph" style="height: 550px;"></div>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
			</div>

		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 <?php echo date('Y'); ?> &copy; RTK.
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
<?php $this->load->view('v2/dashboard/dashboard_footer'); ?>
</html>

<script>
var url ='<?php echo base_url()?>';
		// alert("I work");

$(function () { 
	<?php echo $utilization_bgg_screening_graph; ?>;
	<?php echo $utilization_bgg_confirmatory_graph; ?>;
	<?php //echo $test_graph; ?>;
	<?php echo $drawing_rights_graph; ?>;
	<?php echo $drawing_utilization_graph; ?>;
	<?php echo $utilization_graph_scr; ?>;
	<?php echo $utilization_graph_conf; ?>;

	$(".select2").select2({
          placeholder: "0",
          // containerCssClass: "float-left" 
        });

	/*START OF INHERIT*/
	$('.top_filter_button').button().click(function(x){
          x.preventDefault(); 

          var county_text = $('#county_filter option:selected').text();
          var subcounty_text = $('#sub_county_filter option:selected').text();

          var county_id = $('#county_filter option:selected').val();
          var subcounty_id = $('#sub_county_filter option:selected').val();
          var facility_id = $('#facility_id option:selected').val();
          var quarter = $('#quarter_filter option:selected').val();

          if(county_id==0){
            window.location.replace(url+"dashboardv2/index/NULL/NULL/"+quarter);
          }else{
            if(subcounty_id > 0){
            	window.location.replace(url+"dashboardv2/index/NULL/"+subcounty_id+"/"+quarter);
            }else{
            	window.location.replace(url+"dashboardv2/index/"+county_id+"/NULL/"+quarter);
            }
          }
          // alert(county_id);
          // alert(subcounty_id);
       });

	$('#county_filter').on('change', function(){
          // console.log("County change");return;
        var county_val=$('#county_filter').val()
        var drop_down='';
        var facility_select = "<?php echo base_url(); ?>reports/get_sub_county_json_data/"+county_val;
        $.getJSON( facility_select ,function( json ) {
         $("#sub_county_filter").html('<option value="NULL" selected="selected">All Sub-Counties</option>');
          $.each(json, function( key, val ) {
            drop_down +="<option value='"+json[key]["id"]+"'>"+json[key]["district"]+"</option>"; 
          });
          $("#sub_county_filter").append(drop_down);
        });
        
        });

	// Subcounty filter
    $('#sub_county_filter').on('change', function(){
        var subcounty_val=$('#sub_county_filter').val();
        var drop_down='';
        if(subcounty_val=="NULL"){
          $("#facility_id").html('<option value="NULL" selected="selected">All Facilities</option>');
        }else{
          var facility_select = "<?php echo base_url(); ?>reports/get_facility_json/"+subcounty_val;
          $.getJSON( facility_select ,function( json ) {
           $("#facility_id").html('<option value="NULL" selected="selected">All Facilities</option>');
            $.each(json, function( key, val ) {
              drop_down +="<option value='"+json[key]["facility_code"]+"'>"+json[key]["facility_name"]+"</option>"; 
            });
            $("#facility_id").append(drop_down);
          });  
        }                
        
      });
	/**/

	$('.top_filter_button').button().click(function(x){
          x.preventDefault(); 
          var county_text = $('#county_filter option:selected').text();
          var subcounty_text = $('#sub_county_filter option:selected').text();
          var county_id = $('#county_filter option:selected').val();
          var subcounty_id = $('#sub_county_filter option:selected').val();
          var facility_id = $('#facility_id option:selected').val();
          if(county_id==0){
            ajax_request_replace_div_content('dashboard/stocking_levels/NULL/NULL/NULL/NULL/'+tracer+'/'+division,"#mos");
            ajax_request_replace_div_content('dashboard/consumption/NULL/NULL/NULL/NULL/NULL/NULL/NULL/'+division,"#consumption");
            ajax_request_replace_div_content('dashboard/expiry/NULL/NULL/NULL/NULL/NULL/NULL/'+division+'/'+tracer,"#actual");
            $('.county-name').html("National "+" &nbsp;");
          }else{
            if(subcounty_id=='NULL'){
              $('.county-name').html(county_text+" &nbsp;");
               /*DASHBOARD/EXPIRY HAS 7 PARAMETERS*/
              ajax_request_replace_div_content('dashboard/stocking_levels/'+county_id+'/NULL/NULL/NULL/'+tracer+'/'+division,"#mos");
              ajax_request_replace_div_content('dashboard/consumption/'+county_id+'/NULL/NULL/NULL/NULL/NULL/NULL/'+division,"#consumption");
              ajax_request_replace_div_content('dashboard/expiry/NULL/'+county_id+'/NULL/NULL/NULL/NULL/'+division+'/'+tracer,"#actual");
            }else{
              if(facility_id=='NULL'){
                  $('.county-name').html(subcounty_text+" &nbsp;");
                   /*DASHBOARD/EXPIRY HAS 7 PARAMETERS*/
                  ajax_request_replace_div_content('dashboard/stocking_levels/NULL/'+subcounty_id+'/NULL/NULL/'+tracer+'/'+division,"#mos");
                  ajax_request_replace_div_content('dashboard/consumption/NULL/'+subcounty_id+'/NULL/NULL/NULL/NULL/NULL/'+division,"#consumption");
                  ajax_request_replace_div_content('dashboard/expiry/NULL/NULL/'+subcounty_id+'/NULL/NULL/NULL/'+division+'/'+tracer,"#actual");
              }else{
                  $('.county-name').html(subcounty_text+" &nbsp;");
                   /*DASHBOARD/EXPIRY HAS 7 PARAMETERS*/
                  ajax_request_replace_div_content('dashboard/stocking_levels/NULL/NULL/'+facility_id+'/NULL/'+tracer+'/'+division,"#mos");
                  ajax_request_replace_div_content('dashboard/consumption/NULL/NULL/'+facility_id+'/NULL/NULL/NULL/NULL/'+division,"#consumption");
                  ajax_request_replace_div_content('dashboard/expiry/NULL/NULL/NULL/'+facility_id+'/NULL/NULL/'+division+'/'+tracer,"#actual");

              }
              
              
            }
          }
          // alert(county_id);
          // alert(subcounty_id);
       });

});/*END OF JQUERY FUNCTION*/

var county_id = <?php echo json_encode($county_id); ?>;
var subcounty_id = <?php echo json_encode($subcounty_id); ?>;

if (county_id > 0) {
	if (subcounty_id > 0) {
		// ajax_request_replace_div_content('dashboardv2/get_national_trend/NULL/'+subcounty_id,"#trend-chart"); 
		// ajax_request_replace_div_content('dashboardv2/get_consumption_trend/NULL/'+subcounty_id,"#consumption-trend-chart"); 
	}else{
		ajax_request_replace_div_content('dashboardv2/get_national_trend/'+county_id,"#trend-chart"); 
		ajax_request_replace_div_content('dashboardv2/get_consumption_trend/'+county_id,"#consumption-trend-chart"); 
	}
}else{
	ajax_request_replace_div_content('dashboardv2/get_national_trend/',"#trend-chart"); 
	ajax_request_replace_div_content('dashboardv2/get_consumption_trend/',"#consumption-trend-chart"); 
}
// ajax_request_replace_div_content('dashboardv2/get_drawing_rights/NULL/screening',"#utilization_bgg_screening_graph"); 
// ajax_request_replace_div_content('dashboardv2/get_drawing_rights/NULL/confirmatory',"#utilization_bgg_confirmatory_graph"); 

function ajax_request_replace_div_content(function_url,div){
	var function_url =url+function_url;
	var loading_icon=url+"assets/img/loader2.gif";
	$.ajax({
		type: "POST",
		url: function_url,
		beforeSend: function() {
			$(div).html("<img style='margin-left:20%;' src="+loading_icon+">");
		},
		success: function(msg) {
			console.log(msg);
			$(div).html(msg);
		}
	});
} 

</script>