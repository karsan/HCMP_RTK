<?php $this->load->view('v2/dashboard/dashboard_header'); ?>
<?php //echo "<pre>";print_r($test_graph);exit; ?>
<?php //echo $test_graph;exit; ?>


<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solid  page-sidebar-closed">
<style>
	.red{
		color:red!important;
	}
	.green{
		color:green!important;
	}
</style>
<?php $this->load->view('v2/dashboard/dashboard_top_header'); ?>
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->

<div class="page-container">
<?php $this->load->view('v2/dashboard/dashboard_sidebar'); ?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- <div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="<?php //echo base_url(); ?>">Dashboard</a>
					</li>
				</ul>
			</div> -->
						
			<h3 class="page-title no-margin">
			National Dashboard 
			<!-- <small> data as at <?php //echo date('F/Y') ?> </small> -->
			<small> data as of <?php echo date('F Y', strtotime("-1 MONTH")) ?> reports. </small>
			</h3></br>
			<!-- END PAGE HEADER-->

			<div class="clearfix"></div>
			<div class="row portlet light no-margin-bottom">
			<div class="portlet-body margin-sm-hor clearfix">

				<?php if ($filtered > 0) { ?>
					<div class="col-md-12 no-padding clearfix">
						<strong><p class="no-margin" style="line-height: 1.5em"> <span class="font-blue-steel">*</span> Sections marked using an asterisk do not change when filtered.</p></strong>
					</div>
				<?php } ?>

				<div class="col-md-3 padding-hor-sm no-margin no-padding">
				  <select class="form-control select2 padding-hor-sm" id="year_filter">
				  <option value="0">Select Year</option>
				  <?php foreach ($filter_years as $year => $value):?> 
				      <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
				  <?php endforeach; ?>
				  </select>
				</div>

				<div class="col-md-3 padding-hor-sm no-margin no-padding">
				<select class="form-control select2 padding-hor-sm" id="quarter_filter">
				    <option value="0">Select Quarter</option>
				    <option value="1">Quarter 1</option>
				    <option value="2">Quarter 2</option>
				    <option value="3">Quarter 3</option>
				    <option value="4">Quarter 4</option>
				</select> 
				</div>

				<div class="col-md-3 padding-hor-sm no-margin no-padding">
				  <select class="form-control select2 padding-hor-sm" id="month_filter">
				  <option value="0">Select Month</option>
				  <?php foreach ($filter_months as $month => $value):?> 
				      <option value="<?php echo $value['month_year']; ?>"><?php echo $value['month_year_full']; ?></option>
				  <?php endforeach; ?>
				  </select>
				</div>

				<div class="col-md-3 no-padding no-margin">
				<button class="btn btn-primary top_filter_button filter_button">Filter</button>
				</div>
			</div>

            <div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
			<!-- BEGIN PORTLET-->
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Consumption Trend</span>
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->

								<!-- <span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span> -->
								<span class="caption-helper"><?php echo $title_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<!-- <div id="site_activities_loading">
								<img src="<?php echo base_url().'assets/v2/dashboard/'; ?>admin/layout/img/loading.gif" alt="loading"/>
							</div> -->
							<div id="site_activities_content" class="">
								<div id="consumption-trend-chart" style="height:450px;"></div>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
			</div>
			</div>

			<div class="clearfix"></div>
			<div class="row portlet light">

				<!-- <div class="col-md-12"> -->
				<div class="col-md-6">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet light no-padding">
						<div class="portlet-title no-margin">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<!-- <span class="caption-subject font-blue-steel bold uppercase"><?php echo $title_prepend; ?> Summary</span> -->
								<span class="caption-subject font-blue-steel bold uppercase"><?php echo $title_prepend; ?> Stock Card </span>
									<span class="caption-helper"> <?php echo $title_append ?> </span>
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-bordered table-hover">
								<thead>
									<th></th>
									<th>Screening</th>
									<th>Confirmatory</th>
								</thead>
								
								<tbody>
								
								<tr>
									<td>
										 <strong>Tests Done (Reported on FCDRRs)</strong>
									</td>
									<td>
										 <?php echo number_format($screening_tests_done); ?>
									</td>
									<td>
										 <?php echo number_format($confirmatory_tests_done); ?>
									</td>
								</tr>

								<tr>
									<td>
										 <strong>Tests Done (Retrieved from DHIS)</strong>
									</td>
									<td>
										 <?php echo number_format($dhis_screening_tests_done); ?>
									</td>
									<td>
										 <?php echo number_format($dhis_confirmatory_tests_done); ?>
									</td>
								</tr>

								<!-- <tr>
									<td>
										 <strong>Closing balance</strong>
									</td>
									<td>
										 <?php echo number_format($screening_closing_balance); ?>
									</td>
									<td>
										 <?php echo number_format($confirmatory_closing_balance); ?>
									</td>
								</tr> -->

								<!-- <tr>
									<td>
										 <strong>Requested</strong>
									</td>
									<td>
										 <?php echo $screening_requested = number_format($screening_requested); ?>
									</td>
									<td>
										 <?php echo $confirmatory_requested = number_format($confirmatory_requested); ?>
									</td>
								</tr> -->

								<tr>
									<td>
										 <strong>Expiries</strong>
									</td>
									<td>
										 <?php echo number_format($screening_expiring); ?>
									</td>
									<td>
										 <?php echo number_format($confirmatory_expiring); ?>
									</td>
								</tr>

								</tbody>
								</table>
							</div>
						</div>
					</div>

					<div class="portlet light no-padding">
						<div class="portlet-title no-margin">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<!-- <span class="caption-subject font-blue-steel bold uppercase"><?php echo $title_prepend; ?> Summary</span> -->
								<span class="caption-subject font-blue-steel bold uppercase">KEMSA Stock Summary </span>
									<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
									<!-- <span class="caption-helper"> <?php echo $title_append ?> </span> -->
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->
							</div>
						</div>
						<div class="portlet-body">
							<div class="col-md-12 no-spacing" id="stock-status-table"></div>
							
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
				</div>

				<div class="col-md-6">
				<div class="portlet light no-padding">
					<div class="portlet-title no-margin">
						<div class="caption">
							<span class="caption-subject font-blue-steel bold uppercase"><?php echo $title_prepend; ?> Summary </span>
							<!-- <span class="caption-subject font-blue-steel bold uppercase"><?php echo $title_prepend; ?> Drawing Rights Summary</span> -->
								<span class="caption-helper"><strong><?php echo $title_append; ?></strong></span>
							<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->
						</div>
					</div>
					<div class="portlet-body">
					<?php
						if ($county_id > 0) {
							$t = "Total allocated by county ";
						}else{
							$t = "Total allocated by counties ";
						}

						if ($quartered > 0) {
							$a = "Quarter balance ";
						}else{
							$a = "Available balance for next allocation ";
						}

						
					?>
						<?php if ($filtered_year == '2017'): ?>
							<div class="col-md-12">
								
								<p><span class="font-blue-steel"><strong>*NB</strong></span>: For the year of 2017, <strong>Total drawing rights</strong> and <strong>Total allocated by counties</strong> values do not include data from Quarter 1 as allocations were done outside the system.</p>
							</div>
						<?php endif ?>

						<div class="table-scrollable">
							<table class="table table-bordered table-hover">
							<thead>
								<th></th>
								<th>Screening</th>
								<th>Confirmatory</th>
							</thead>
							
							<tbody>
							
							<tr>
								<td>
									 <!-- <strong>Total drawing rights <small>(<?php echo "Year: ".date('Y'); ?>)</small></strong> -->
									 <!-- <strong>Total drawing rights <small>(<?php echo "Year: ".$county_drawing_data_year; ?>)</small></strong> -->
									 <strong>Total drawing rights <small>(<?php echo "Year: ".$national_drawing_totals_year; ?>)</small></strong>
								</td>
								<td>
									<?php //echo number_format($drawing_total_screening); ?>
									<?php //echo number_format($national_drawing_totals['screening_total_annual']); ?>
									<!-- <a href="#" class="bold-links font-blue-steel" data-level="national" data-level-id = "0" data-summary-type = "drawing_rights" data-commodity-id = "4" data-toggle="modal" data-target=".summary-details-modal"><?php echo number_format($national_drawing_totals['screening_total_annual']); ?></a> -->

									<a href="#" class="bold-links font-blue-steel" data-level="national" data-level-id = "0" data-summary-type = "drawing_rights" data-commodity-id = "4" data-toggle="modal" data-target=".summary-details-modal"><?php echo number_format($national_drawing_totals['screening_default_annual']); ?></a>

								</td>
								<td>
									<?php //echo number_format($drawing_total_confirmatory); ?>
									<?php //echo number_format($national_drawing_totals['confirmatory_total_annual']); ?>

									<!-- <a href="#" class="bold-links font-blue-steel" data-level="national" data-level-id = "0" data-summary-type = "drawing_rights" data-commodity-id = "4" data-toggle="modal" data-target=".summary-details-modal"><?php echo number_format($national_drawing_totals['confirmatory_total_annual']); ?></a> -->
									<a href="#" class="bold-links font-blue-steel" data-level="national" data-level-id = "0" data-summary-type = "drawing_rights" data-commodity-id = "4" data-toggle="modal" data-target=".summary-details-modal"><?php echo number_format($national_drawing_totals['confirmatory_default_annual']); ?></a>
								</td>
							</tr>

							<tr>
								<td>
									 <!-- <strong><?php echo $t; ?> <small>(<?php echo "Year: ".$total_allocated_year; ?>)</small></strong> -->
									 <strong><?php echo $t; ?> <small>(<?php echo "Year: ".$national_drawing_totals_year; ?>)</small></strong>
								</td>
								<td>
										 <?php //echo number_format($drawing_distributed_screening); ?>
										 <?php //echo number_format($total_allocated_screening); ?>
										 <?php //echo number_format($national_drawing_totals['screening_distributed']); ?>

										<a href="#" class="bold-links font-blue-steel" data-level="national" data-level-id = "0" data-summary-type = "drawing_rights" data-commodity-id = "4" data-toggle="modal" data-target=".summary-details-modal"><?php echo number_format($national_drawing_totals['screening_distributed']); ?></a>

								</td>
								<td>
										 <?php //echo number_format($drawing_distributed_confirmatory); ?>
										 <?php //echo number_format($total_allocated_confirmatory); ?>
										 <?php //echo number_format($national_drawing_totals['confirmatory_distributed']); ?>

										<a href="#" class="bold-links font-blue-steel" data-level="national" data-level-id = "0" data-summary-type = "drawing_rights" data-commodity-id = "4" data-toggle="modal" data-target=".summary-details-modal"><?php echo number_format($national_drawing_totals['confirmatory_distributed']); ?></a>

								</td>
							</tr>

							<tr>
								<td class="indent">
									 <strong>- Issued from KEMSA  <small>(<?php echo $kemsa_issued_data_date; ?>)</small></strong>
								</td>
								<td>
									<?php echo number_format($kemsa_issued_screening); ?>
								</td>
								<td>
									<?php echo number_format($kemsa_issued_confirmatory); ?>
								</td>
							</tr>

							<tr>
								<td class="indent">
									 <strong>- Received at site  <small>(<?php echo $summary_data_date; ?>)</small></strong>
								</td>
								<td>
										 <?php echo number_format($received_site_screening); ?>
								</td>
								<td>
										 <?php echo number_format($received_site_confirmatory); ?>
								</td>
							</tr>

							<tr>
								<td class="indent">
									 <strong>- Consumed at site  <small>(<?php echo $summary_data_date;  ?>)</small></strong>
								</td>
								<td>
										 <?php echo number_format($consumed_site_screening); ?>
								</td>
								<td>
										 <?php echo number_format($consumed_site_confirmatory); ?>
								</td>
							</tr>

							<tr>
								<td class="indent">
									 <strong>- Beginning balance <small>(<?php echo date('F Y',strtotime($beginning_balance_order_date)); ?>)</small> </strong>
								</td>
								<td>
									<?php echo number_format($beginning_balance_screening); ?>
								</td>
								<td>
									<?php echo number_format($beginning_balance_confirmatory); ?>
								</td>
							</tr>

							<tr>
								<td class="indent">
									 <strong>- Closing balance <small>(<?php echo date('F Y',strtotime($closing_balance_order_date)) ?>)</small> </strong>
								</td>
								<td>
									<?php echo number_format($closing_balance_screening); ?>
								</td>
								<td>
									<?php echo number_format($closing_balance_confirmatory); ?>
								</td>
							</tr>

							<tr>
								<td>
									 <strong> <?php echo $a; ?>  <small>(<?php echo "Year: ".$national_drawing_totals_year; ?>)</small></strong>
								</td>
								<td>
										 <?php echo number_format($national_drawing_totals['screening_total_annual'] - $national_drawing_totals['screening_distributed']); ?>
								</td>
								<td>
										 <?php echo number_format($national_drawing_totals['confirmatory_total_annual'] - $national_drawing_totals['confirmatory_distributed']); ?>
								</td>
							</tr>

							<tr>
								<td>
									 <strong> Total Balance Discrepancy  <small>(<?php echo "Year: ".$national_drawing_totals_year; ?>)</small></strong>
								</td>
								<td>
									<?php 

										$beg_bal_s = $beginning_balance_screening;
										$clos_bal_s = $closing_balance_screening;
										$rec_s = $received_site_screening;
										$pos_adj_s = $positive_adj_screening;
										$neg_adj_s = $negative_adj_screening;
										$used_s = $quantity_used_screening;
										$losses_s = $losses_screening;
										$reported_s = $closing_balance_screening;

										$expected_s = ($beg_bal_s + $rec_s + $pos_adj_s)-($used_s + $losses_s + $neg_adj_s); 
										// echo "<pre>";print_r($expected_s);exit;
										$difference_s = $reported_s - $expected_s;

										$class_s = ($difference_s>0)? " green ":" red ";
										$class_s = ($difference_s==0)? "":$class_s;

										 echo number_format($difference_s); 
									?>
								</td>
								<td>
									<?php 

										$beg_bal_c = $beginning_balance_confirmatory;
										$clos_bal_c = $closing_balance_confirmatory;
										$rec_c = $received_site_confirmatory;
										$pos_adj_c = $positive_adj_confirmatory;
										$neg_adj_c = $negative_adj_confirmatory;
										$used_c = $quantity_used_confirmatory;
										$losses_c = $losses_confirmatory;
										$reported_c = $closing_balance_confirmatory;

										$expected_c = ($beg_bal_c + $rec_c + $pos_adj_c)-($used_c + $losses_c + $neg_adj_c); 

										$difference_c = $reported_c - $expected_c;

										$class_c = ($difference_c>0)? " green ":" red ";
										$class_c = ($difference_c==0)? "":$class_c;

										 echo number_format($difference_c); 

									?>
								</td>
							</tr>

							</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="portlet light no-padding">
					<div class="portlet-title no-margin">
						<div class="caption">
							<span class="caption-subject font-blue-steel bold uppercase"><?php echo $title_prepend; ?> Discrepancy Summary </span>
							<!-- <span class="caption-subject font-blue-steel bold uppercase"><?php echo $title_prepend; ?> Drawing Rights Summary</span> -->
								<!-- <span class="caption-helper"><strong><?php echo $title_append; ?></strong></span> -->
							<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-scrollable">
							<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<!-- <th></th> -->
									<th colspan="3">Screening</th>
									<th colspan="3">Confirmatory</th>
								</tr>
								<tr>
									<th>Expected Balance</th>
									<th>Reported</th>
									<th>Difference</th>
									<th>Expected Balance</th>
									<th>Reported</th>
									<th>Difference</th>
								</tr>
							</thead>
							
							<tbody>
								<?php 
									// echo "<pre>";print_r($national_order_data);exit;
									$beg_bal_s = $national_order_data['screening']['beginning_bal'];
									$rec_s = $national_order_data['screening']['q_received'];
									$pos_adj_s = $national_order_data['screening']['positive_adj'];
									$used_s = $national_order_data['screening']['q_used'];
									$losses_s = $national_order_data['screening']['losses'];
									$neg_adj_s = $national_order_data['screening']['negative_adj'];
									$clos_bal_s = $national_order_data['screening']['closing_stock'];
									$reported_s = $national_order_data['screening']['closing_stock'];

									$expected_s = ($beg_bal_s + $rec_s + $pos_adj_s)-($used_s + $losses_s + $neg_adj_s); 
									// echo "<pre>";print_r($expected_s);exit;
									$difference_s = $reported_s - $expected_s;

									$class_s = ($difference_s>0)? " green ":" red ";
									$class_s = ($difference_s==0)? "":$class_s;

									$beg_bal_c = $national_order_data['confirmatory']['beginning_bal'];
									$rec_c = $national_order_data['confirmatory']['q_received'];
									$pos_adj_c = $national_order_data['confirmatory']['positive_adj'];
									$used_c = $national_order_data['confirmatory']['q_used'];
									$losses_c = $national_order_data['confirmatory']['losses'];
									$neg_adj_c = $national_order_data['confirmatory']['negative_adj'];
									$clos_bal_c = $national_order_data['confirmatory']['closing_stock'];
									$reported_c = $national_order_data['confirmatory']['closing_stock'];

									$expected_c = ($beg_bal_c + $rec_c + $pos_adj_c)-($used_c + $losses_c + $neg_adj_c); 

									$difference_c = $reported_c - $expected_c;

									$class_c = ($difference_c>0)? " green ":" red ";
									$class_c = ($difference_c==0)? "":$class_c;

								 ?>
							<tr>
								<td><?php echo number_format($expected_s) ?></td>
								<td><?php echo number_format($reported_s) ?></td>
								<td class="<?php echo $class_s ?>"><strong><?php echo number_format($difference_s); ?></strong></td>
								<td><?php echo number_format($expected_c) ?></td>
								<td><?php echo number_format($reported_c) ?></td>
								<td class="<?php echo $class_c ?>"><strong><?php echo number_format($difference_c); ?></strong></td>
							</tr>

							</tbody>
							</table>
						</div>
					</div>
				</div>
				</div>

				<!-- END PORTLET-->
				
			</div>
				

			<div class="row portlet light no-spacing">
			<div class="col-md-6 col-sm-6 no-spacing">
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Reporting Rates Trend</span>
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->
								<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>

							</div>
						</div>
						<div class="portlet-body">
							<!-- <div id="site_activities_loading">
								<img src="<?php echo base_url().'assets/v2/dashboard/'; ?>admin/layout/img/loading.gif" alt="loading"/>
							</div> -->
							<div id="site_activities_content" class="">
								<div id="trend-chart" style="height:300px;"></div>
								<!-- <div id="trend-chart-script" style="height:450px;"></div> -->
							</div>
						</div>
					</div>
					<!-- END PORTLET-->

			</div>
			<div class="col-md-6 col-sm-6 no-margin no-padding">
			
			<!-- BEGIN PORTLET-->
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-share font-red-sunglo hide"></i>
						<!-- <span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Utilization vs Received. </span> -->
						<!-- <span class="caption-subject font-green-haze bold uppercase">Allocated vs Utilized vs Received. </span> -->
						<span class="caption-subject font-green-haze bold uppercase">Allocated vs Received vs Consumed </span>
						<span class="caption-helper"><?php echo $title_append; ?></span>

					</div>
				</div>
				<div class="portlet-body">
					<div class="col-md-12 no-padding">
						<div class="col-md-6 no-padding">
							<div id="site_activities_content" class="">
								<div id="utilization_graph_scr" style="height: 300px;"></div>
							</div>
						</div>
						<div class="col-md-6 no-padding">
							<div id="site_activities_content" class="">
								<div id="utilization_graph_conf" style="height: 300px;"></div>
							</div>
						</div>
						<!-- <div class="col-md-12 no-padding">
							<div id="site_activities_content" class="">
								<div id="container" style="height: 350px;"></div>
							</div>
						</div> -->
					</div>
				</div>
			</div>

			</div>
			<!-- END PORTLET-->
			</div>

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
					<div class="portlet light clearfix">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase">County Drawing Right Details </span>
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->

								<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="col-md-12 no-spacing" style="">
								<table class="table datatable display cell-border compact" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>#</th>
											<th>County</th>
											<th>Quarters</th>
											<th>Year</th>
											<th>Screening Total</th>
											<th>Screening Distributed</th>
											<th>Screening Balance</th>
											<th>Confirmatory Total</th>
											<th>Confirmatory Distributed</th>
											<th>Confirmatory Balance</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											foreach ($county_drawing_totals as $key => $value) { 
												// echo "<pre>";print_r($value);exit;
        										$curQuarter = ceil(date('m')/3);//KARSAN
        										// echo "<pre>";print_r($curQuarter);
        										$c_id = $value['county_id'];
        										// echo "<pre>";print_r($county_drawing_totals_details[$c_id][$curQuarter]);exit;
										?>
										<tr>
											<td></td>
											<td><a style="text-decoration: none!important;color:black;" href="<?php echo base_url().'dashboardv2/county_dashboard/'.$value['county_id']; ?>" target="_blank"><?php echo $value['county']; ?></a></td>
											<td><?php echo $value['quarter_multiplier']; ?></td>
											<td><?php echo $value['year']; ?></td>
											<!-- <td><?php echo number_format($value['screening_total_annual']); ?></td> -->
											<td><?php echo number_format($county_drawing_totals_details[$c_id][$curQuarter]['screening_total']); ?></td>
											<td><?php echo number_format($value['screening_distributed']); ?></td>
											<td><?php echo number_format($value['screening_total_annual'] - $value['screening_distributed']); ?></td>
											<td><?php echo number_format($county_drawing_totals_details[$c_id][$curQuarter]['confirmatory_total']); ?></td>
											<!-- <td><?php echo number_format($value['confirmatory_total_annual']); ?></td> -->
											<td><?php echo number_format($value['confirmatory_distributed']); ?></td>
											<td><?php echo number_format($value['confirmatory_total_annual'] - $value['confirmatory_distributed']); ?></td>
										</tr>
										<?php } ?>
									</tbody>	
								</table>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
			</div>
			</div>

			<div class="clearfix"></div>
			<div class="row portlet light no-spacing">
				<div class="col-md-12 col-sm-12 no-spacing">
						<div class="portlet light ">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-share font-red-sunglo hide"></i>
									<span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Reporting Rates</span>
									<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->
									<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>

								</div>
							</div>
							<div class="portlet-body">
								<!-- <div id="site_activities_loading">
									<img src="<?php echo base_url().'assets/v2/dashboard/'; ?>admin/layout/img/loading.gif" alt="loading"/>
								</div> -->
								<div id="site_activities_content" class="">
									<div id="trend-chart-table" style=""></div>
									<!-- <div id="trend-chart-script" style="height:450px;"></div> -->
								</div>
							</div>
						</div>
						<!-- END PORTLET-->

				</div>
			</div>

			<!-- <div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
					<div class="portlet light clearfix">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase">County Drawing Rights (Screening)</span>
								<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="col-md-12 no-spacing" style="">
								<table class="table datatable display cell-border compact" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>#</th>
											<th>County</th>
											<th>Quarter</th>
											<th>Year</th>
											<th>Total Drawing Rights</th>
											<th>Total Allocated</th>
											<th>Total Used</th>
											<th>Balance</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											foreach ($county_drawing_data as $key => $value) { 
											$tot = $value['screening_total'] * $cur_quarter;
											$bal = $tot - $value['screening_distributed'];
										?>
										<tr>
											<td></td>
											<td><?php echo $value['county']; ?></td>
											<td><?php echo $value['quarter']; ?></td>
											<td><?php echo $value['year']; ?></td>
											<td><?php echo number_format($value['screening_total']); ?></td>
											<td><?php echo number_format($value['screening_distributed']); ?></td>
											<td><?php echo number_format($value['screening_used']); ?></td>
											<td><?php echo number_format($value['screening_balance']); ?></td>
										</tr>
										<?php } ?>
									</tbody>	
								</table>
							</div>
						</div>
					</div>
			</div>
			</div> -->	

			<!-- <div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
					<div class="portlet light clearfix">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase">County Drawing Rights (Confirmatory)</span>
								<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="col-md-12 no-spacing" style="">
								<table class="table datatable-c display cell-border compact" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>#</th>
											<th>County</th>
											<th>Quarter</th>
											<th>Year</th>
											<th>Total Drawing Rights</th>
											<th>Total Allocated</th>
											<th>Total Used</th>
											<th>Balance</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											foreach ($county_drawing_data as $key => $value) { 
											$tot = $value['confirmatory_total'] * $cur_quarter;
											$bal = $tot - $value['confirmatory_distributed'];
										?>
										<tr>
											<td></td>
											<td><?php echo $value['county']; ?></td>
											<td><?php echo $value['quarter']; ?></td>
											<td><?php echo $value['year']; ?></td>
											<td><?php echo number_format($value['confirmatory_total']); ?></td>
											<td><?php echo number_format($value['confirmatory_distributed']); ?></td>
											<td><?php echo number_format($value['confirmatory_used']); ?></td>
											<td><?php echo number_format($value['confirmatory_balance']); ?></td>
										</tr>
										<?php } ?>
									</tbody>	
								</table>
							</div>
						</div>
					</div>
			</div>
			</div> -->	

			<h3 class="page-title no-margin">
			Allocations 
			<!-- <small> data as at <?php echo date('l/m/Y', strtotime("-1 MONTH")); ?></small> -->
			<small> <?php echo date('Y', strtotime("-1 MONTH")); ?></small>
			</h3></br>

			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-12 col-sm-12 no-spacing">
					<!-- BEGIN PORTLET-->
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Total Drawing Rights vs Total Allocated (Screening) </span>
								</br>
								<span class="caption-helper"> Quarters: <?php echo $drawing_rights_quarter." ".$title_append." ".$quarter_append; ?></span>
								<!-- <span class="caption-helper"><?php //echo $title_append; ?></span> -->
							</div>
						</div>
						<div class="portlet-body">
							<!-- <div id="site_activities_loading">
								<img src="<?php echo base_url().'assets/v2/dashboard/'; ?>admin/layout/img/loading.gif" alt="loading"/>
							</div> -->
							<div id="site_activities_content" class="">
								<!-- <div id="utilization_bgg_screening_graph" style="height: 550px;"></div> -->
								<div id="utilization_bgg_screening_graph_clickable" style="height: 550px;"></div>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
			</div>

			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-12 col-sm-12 no-spacing">
					<!-- BEGIN PORTLET-->
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Total Drawing Rights vs Total Allocated (Confirmatory)</span>
								</br>
								<span class="caption-helper"> Quarters: <?php echo $drawing_rights_quarter." ".$title_append." ".$quarter_append; ?></span>
								<span class="caption-helper"><?php //echo $title_append; ?></span>
								<!-- <span class="caption-helper">as at <?php echo date('l/m/Y') ?></span> -->
							</div>
						</div>
						<div class="portlet-body">
							<!-- <div id="site_activities_loading">
								<img src="<?php echo base_url().'assets/v2/dashboard/'; ?>admin/layout/img/loading.gif" alt="loading"/>
							</div> -->
							<div id="site_activities_content" class="">
								<!-- <div id="utilization_bgg_confirmatory_graph" style="height: 550px;"></div> -->
								<div id="utilization_bgg_confirmatory_graph_clickable" style="height: 550px;"></div>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
			</div>

		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 <?php echo date('Y'); ?> &copy; RTK.
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
<?php $this->load->view('v2/dashboard/dashboard_footer'); ?>

<div class="modal fade summary-details-modal" id="summary-details-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width: 1000px;">
    <div class="modal-content">
    	<div class="modal-header">
        <h3 class="modal-title" id="myLargeModalLabel">Summary Details</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>

      <div class="modal-body" >
      	<div id="modal-body-data">
        	<!-- <i class="fa fa-cog fa-spin"></i> -->
      	</div>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div> -->
  </div>
</div>
</div>
</html>

<script>
	var url ='<?php echo base_url()?>';
			// alert("I work");
	var quarter = <?php echo json_encode($quarter); ?>;
	var filtered_month = <?php echo json_encode($filtered_month); ?>;
	var filtered_year = <?php echo json_encode($filtered_year); ?>;
	if (filtered_month < 1) {filtered_month = "NULL"}
	if (filtered_year < 1) {filtered_year = "NULL"}
	// console.log(filtered_month);
	$(function () { 
		<?php //echo $utilization_bgg_screening_graph; ?>;
		<?php echo $utilization_bgg_screening_graph_clickable; ?>;
		<?php echo $utilization_bgg_confirmatory_graph_clickable; ?>;
		<?php //echo $utilization_bgg_confirmatory_graph; ?>;
		<?php //echo $test_graph; ?>;
		<?php echo $drawing_rights_graph; ?>;
		<?php echo $drawing_utilization_graph; ?>;
		<?php echo $utilization_graph_scr; ?>;
		<?php echo $utilization_graph_conf; ?>;
		// console.log('dashboardv2/get_consumption_trend/NULL/NULL/NULL/'+filtered_month+'/'+filtered_year+'/'+quarter);
		ajax_request_replace_div_content('dashboardv2/get_national_trend/NULL/NULL/NULL/NULL/'+filtered_year,"#trend-chart"); 
		ajax_request_replace_div_content('dashboardv2/get_reporting_rates/NULL/NULL/NULL/NULL/'+filtered_year+'/county/table',"#trend-chart-table"); 
		ajax_request_replace_div_content('dashboardv2/get_consumption_trend/NULL/NULL/NULL/'+filtered_month+'/'+filtered_year+'/'+quarter,"#consumption-trend-chart"); 
		ajax_request_replace_div_content('dashboardv2/get_kemsa_stock_status/'+filtered_year,"#stock-status-table");

		$(".select2").select2({
	          placeholder: "0",
	          // containerCssClass: "float-left" 
	        });

		/*START OF INHERIT*/
		$('.top_filter_button').button().click(function(x){
	          x.preventDefault(); 

	          var county_text = $('#county_filter option:selected').text();
	          var subcounty_text = $('#sub_county_filter option:selected').text();

	          var county_id = $('#county_filter option:selected').val();
	          var subcounty_id = $('#sub_county_filter option:selected').val();
	          var facility_id = $('#facility_id option:selected').val();
	          var quarter = $('#quarter_filter option:selected').val();
	          var month_year = $('#month_filter option:selected').val();
	          var year = $('#year_filter option:selected').val();

	            window.location.replace(url+"dashboardv2/national_dashboard/NULL/NULL/"+quarter+"/"+month_year+"/"+year);
	         /* if(county_id==0){
	            window.location.replace(url+"dashboardv2/national_dashboard/NULL/NULL/"+quarter+"/"+month_year+"/"+year);
	          }else{
	            if(subcounty_id > 0){
	            	window.location.replace(url+"dashboardv2/national_dashboard/NULL/"+subcounty_id+"/"+quarter+"/"+month_year+"/"+year);
	            }else{
	            	window.location.replace(url+"dashboardv2/national_dashboard/"+county_id+"/NULL/"+quarter+"/"+month_year+"/"+year);
	            }
	          }*/
	          // alert(county_id);
	          // alert(subcounty_id);
	    });

		$('#county_filter').on('change', function(){
	        var county_val=$('#county_filter').val()
	        var drop_down='';
	        var facility_select = "<?php echo base_url(); ?>reports/get_sub_county_json_data/"+county_val;
	        $.getJSON( facility_select ,function( json ) {
		         $("#sub_county_filter").html('<option value="NULL" selected="selected">All Sub-Counties</option>');
		          $.each(json, function( key, val ) {
		            drop_down +="<option value='"+json[key]["id"]+"'>"+json[key]["district"]+"</option>"; 
		          });
		          $("#sub_county_filter").append(drop_down);
	        });
	        
	    });

		// Subcounty filter
	    $('#sub_county_filter').on('change', function(){
	        var subcounty_val=$('#sub_county_filter').val();
	        var drop_down='';
	        if(subcounty_val=="NULL"){
	          $("#facility_id").html('<option value="NULL" selected="selected">All Facilities</option>');
	        }else{
	          var facility_select = "<?php echo base_url(); ?>reports/get_facility_json/"+subcounty_val;
	          $.getJSON( facility_select ,function( json ) {
	           $("#facility_id").html('<option value="NULL" selected="selected">All Facilities</option>');
	            $.each(json, function( key, val ) {
	              drop_down +="<option value='"+json[key]["facility_code"]+"'>"+json[key]["facility_name"]+"</option>"; 
	            });
	            $("#facility_id").append(drop_down);
	          });  
	        }                
	        
	      });
		/**/

		/*DATATABLES*/
			var t = $('.datatable').DataTable( {
		        "columnDefs": [ {
		            "searchable": false,
		            "orderable": false,
		            "targets": 0
		        } ],
		        "order": [[ 1, 'asc' ]]
		    } );
		 
		    t.on( 'order.dt search.dt', function () {
		        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		            cell.innerHTML = i+1;
		        } );
		    } ).draw();

		    var s = $('.datatable-c').DataTable( {
		        "columnDefs": [ {
		            "searchable": false,
		            "orderable": false,
		            "targets": 0
		        } ],
		        "order": [[ 1, 'asc' ]]
		    } );
		 
		    s.on( 'order.dt search.dt', function () {
		        s.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		            cell.innerHTML = i+1;
		        } );
		    } ).draw();
		/*END OF DATATABLES*/

	    $('#summary-details-modal').on('show.bs.modal', function (event) {
			var county_text = $('#county_filter option:selected').text();
			var subcounty_text = $('#sub_county_filter option:selected').text();

			var county_id = $('#county_filter option:selected').val();
			var subcounty_id = $('#sub_county_filter option:selected').val();
			var facility_id = $('#facility_id option:selected').val();
			var quarter = $('#quarter_filter option:selected').val();
			var month_year = $('#month_filter option:selected').val();
			var year = $('#year_filter option:selected').val();

			var link = $(event.relatedTarget) // link that triggered the modal
			var level = link.data('level') // Extract info from data-* attributes
			var level_id = link.data('level-id') // Extract info from data-* attributes
			var commodity_id = link.data('commodity-id') // Extract info from data-* attributes
			var order_id = link.data('order-id') // Extract info from data-* attributes
			var order_date = link.data('order-date') // Extract info from data-* attributes
			var type = link.data('summary-type') // Extract info from data-* attributes

			// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
			// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
			var modal = $(this)
			console.log('dashboardv2/get_dashboard_summary_details/'+level+'/'+level_id+'/'+type+'/'+county_id+'/'+subcounty_id+'/'+commodity_id+'/'+facility_id+'/'+quarter+'/'+month_year+'/'+year);
			// modal.find('.modal-title').text('New message to ' + recipient)
			// modal.find('.modal-body').html(level + " "+ level_id);
			ajax_request_replace_div_content('dashboardv2/get_dashboard_summary_details/'+level+'/'+level_id+'/'+type+'/'+county_id+'/'+subcounty_id+'/'+commodity_id+'/'+facility_id+'/'+quarter+'/'+month_year+'/'+year,"#modal-body-data");

		});

		$(document).on("click", "#summary_modal_details", function(event){
			var county_id = $('#county_filter option:selected').val();
			var subcounty_id = $('#sub_county_filter option:selected').val();
			var facility_id = $('#facility_id option:selected').val();
			var quarter = $('#quarter_filter option:selected').val();
			var month_year = $('#month_filter option:selected').val();
			var year = $('#year_filter option:selected').val();

			var level = $(this).data('level') // Extract info from data-* attributes
			var level_id = $(this).data('level-id') // Extract info from data-* attributes
			var commodity_id = $(this).data('commodity-id') // Extract info from data-* attributes
			var order_id = $(this).data('order-id') // Extract info from data-* attributes
			var order_date = $(this).data('order-date') // Extract info from data-* attributes
			var type = $(this).data('summary-type') // Extract info from data-* attributes

			// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
			// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.

			console.log('dashboardv2/get_dashboard_summary_details/'+level+'/'+level_id+'/'+type+'/'+county_id+'/'+subcounty_id+'/'+commodity_id+'/'+facility_id+'/'+quarter+'/'+month_year+'/'+year);
			if (order_id > 0) {
				ajax_request_replace_div_content('dashboardv2/get_facility_order_details/'+order_id,"#modal-body-data");
			}else{
				ajax_request_replace_div_content('dashboardv2/get_dashboard_summary_details/'+level+'/'+level_id+'/'+type+'/'+county_id+'/'+subcounty_id+'/'+commodity_id+'/'+facility_id+'/'+quarter+'/'+month_year+'/'+year,"#modal-body-data");
			}

			// modal.find('.modal-title').text('New message to ' + recipient)
			// modal.find('.modal-body').html(level + " "+ level_id);
		});

	});/*END OF JQUERY FUNCTION*/

	var county_id = <?php echo json_encode($county_id); ?>;
	var subcounty_id = <?php echo json_encode($subcounty_id); ?>;


	// ajax_request_replace_div_content('dashboardv2/get_drawing_rights/NULL/screening',"#utilization_bgg_screening_graph"); 
	// ajax_request_replace_div_content('dashboardv2/get_drawing_rights/NULL/confirmatory',"#utilization_bgg_confirmatory_graph"); 

	function ajax_request_replace_div_content(function_url,div){
		var function_url =url+function_url;
		var loading_icon=url+"assets/img/loader2.gif";
		$.ajax({
			type: "POST",
			url: function_url,
			beforeSend: function() {
				$(div).html("<img style='margin-left:20%;' src="+loading_icon+">");
			},
			success: function(msg) {
				// console.log(msg);
				$(div).html(msg);
				$('.datatableajax').DataTable();

			}
		});
	} 

</script>