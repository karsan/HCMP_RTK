<?php $this->load->view('v2/dashboard/dashboard_header'); ?>
<?php //echo "<pre>";print_r($county_id);exit; ?>
<?php //echo $test_graph;exit; ?>

<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed">
<?php $this->load->view('v2/dashboard/dashboard_top_header'); ?>
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->

<div class="page-container">
<?php $this->load->view('v2/dashboard/dashboard_sidebar'); ?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- <div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="<?php //echo base_url(); ?>">Dashboard</a>
					</li>
				</ul>
			</div> -->
						
			<h3 class="page-title no-margin">
			Facility Dashboard 
			<!-- <small> data as at <?php //echo date('F/Y') ?> </small> -->
			<small> data as of <?php echo date('F Y', strtotime("-1 MONTH")) ?> reports. </small>
			</h3></br>
			<!-- END PAGE HEADER-->

			<div class="clearfix"></div>
			<div class="row portlet light no-margin-bottom">
				<div class="portlet-body margin-sm-hor clearfix">
				<?php if ($quartered > 0) { ?>
				<div class="col-md-12 no-padding clearfix">
					<strong><p class="no-margin" style="line-height: 1.5em"> <span class="font-blue-steel">*</span> Fields with an asterisk do not change when filtered quarterly.</p></strong>
				</div>
				<?php } ?>

	                  <div class="col-md-3 padding-hor-sm no-margin no-padding">
	                  <select class="form-control select2 select2-fac-search padding-hor-sm" id="facility_filter">
	                  <option value="0">All Facilities</option>
	                  <?php foreach ($facility_data as $facility => $value):?> 
	                      <option value="<?php echo $value['facility_code']; ?>"><?php echo $value['facility_code'].' | '.$value['facility_name']; ?></option>
	                  <?php endforeach; ?>
	                  </select>
	                  </div>

	                  <div class="col-md-3 padding-hor-sm no-margin no-padding">
	                    <select class="form-control select2 padding-hor-sm" id="quarter_filter">
		                    <option value="0">Select quarter</option>
		                    <option value="1">Quarter 1</option>
		                    <option value="2">Quarter 2</option>
		                    <option value="3">Quarter 3</option>
		                    <option value="4">Quarter 4</option>
	                    </select> 

	                  </div>

	              	<div class="col-md-3 padding-hor-sm no-margin no-padding">
		              <select class="form-control select2 padding-hor-sm" id="month_filter">
		              <option value="0">Select Month</option>
		              <?php foreach ($filter_months as $month => $value):?> 
		                  <option value="<?php echo $value['month_year']; ?>"><?php echo $value['month_year_full']; ?></option>
		              <?php endforeach; ?>
		              </select>
	              	</div>

	                 <div class="col-md-3 no-padding no-margin">
	                    <button class="btn btn-primary top_filter_button filter_button">Filter</button>
	                  </div>
				</div>
            </div>
        <?php if($filtered == 0): ?>

            <div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
			<!-- BEGIN PORTLET-->
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Screening Consumption Trend</span>
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->

								<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<!-- <div id="site_activities_loading">
								<img src="<?php echo base_url().'assets/v2/dashboard/'; ?>admin/layout/img/loading.gif" alt="loading"/>
							</div> -->
							<div id="site_activities_content" class="">
								<div id="scr_consumption_chart" style="height:450px;"></div>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
			</div>
			</div>

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
			<!-- BEGIN PORTLET-->
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Confirmatory Consumption Trend</span>
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->

								<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<!-- <div id="site_activities_loading">
								<img src="<?php echo base_url().'assets/v2/dashboard/'; ?>admin/layout/img/loading.gif" alt="loading"/>
							</div> -->
							<div id="site_activities_content" class="">
								<div id="conf_consumption_chart" style="height:450px;"></div>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
			</div>
			</div>

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
			
					<div class="portlet light clearfix">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Screening Summaries</span>
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->

								<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="col-md-12 no-spacing" id="scr_fac_summaries">
								<table class="table display cell-border compact" style="table-layout:fixed;" cellspacing="0" width="100%" id="summary_datatable_s">
									<thead>
										<tr>
											<!-- <th rowspan="2">#</th> -->
											<th rowspan="2">Facility Code</th>
											<th rowspan="2">Facility Name</th>
											<th rowspan="2">Sub-County</th>
											<th rowspan="2">County</th>
											<!-- <th rowspan="2">Allocated</th> -->
											<!-- <th rowspan="2">Issued from KEMSA</th> -->
											<th rowspan="2">Beginning Balance</th>
											<th rowspan="2">Quantity Received</th>
											<th rowspan="2">Quantity Used</th>
											<th rowspan="2">Tests Done</th>
											<th rowspan="2">Losses</th>
											<th colspan="2">Adjustments</th>
											<!-- <th rowspan="2">Adjustments(-)</th> -->
											<th rowspan="2">Closing Balance</th>
										</tr>
										<tr>
											<th>Positive</th>
											<th>Negative</th>
										</tr>
									</thead>

									<tfoot>
										<tr>
											<!-- <th rowspan="2">#</th> -->
											<th rowspan="2">Facility Code</th>
											<th rowspan="2">Facility Name</th>
											<th rowspan="2">Sub-County</th>
											<th rowspan="2">County</th>
											<!-- <th rowspan="2">Allocated</th> -->
											<!-- <th rowspan="2">Issued from KEMSA</th> -->
											<th rowspan="2">Beginning Balance</th>
											<th rowspan="2">Quantity Received</th>
											<th rowspan="2">Quantity Used</th>
											<th rowspan="2">Tests Done</th>
											<th rowspan="2">Losses</th>
											<th colspan="2">Adjustments</th>
											<!-- <th rowspan="2">Adjustments(-)</th> -->
											<th rowspan="2">Closing Balance</th>
										</tr>
										<tr>
											<th>Positive</th>
											<th>Negative</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
			</div>
			</div>	

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
			
					<div class="portlet light clearfix">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Confirmatory Summaries</span>
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->

								<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="col-md-12 no-spacing" id="conf_fac_summaries">
								<table class="table display cell-border compact" style="table-layout:fixed;" cellspacing="0" width="100%" id="summary_datatable_c">
									<thead>
										<tr>
											<!-- <th rowspan="2">#</th> -->
											<th rowspan="2">Facility Code</th>
											<th rowspan="2">Facility Name</th>
											<th rowspan="2">Sub-County</th>
											<th rowspan="2">County</th>
											<!-- <th rowspan="2">Allocated</th> -->
											<!-- <th rowspan="2">Issued from KEMSA</th> -->
											<th rowspan="2">Beginning Balance</th>
											<th rowspan="2">Quantity Received</th>
											<th rowspan="2">Quantity Used</th>
											<th rowspan="2">Tests Done</th>
											<th rowspan="2">Losses</th>
											<th colspan="2">Adjustments</th>
											<!-- <th rowspan="2">Adjustments(-)</th> -->
											<th rowspan="2">Closing Balance</th>
										</tr>
										<tr>
											<th>Positive</th>
											<th>Negative</th>
										</tr>
									</thead>

									<tfoot>
										<tr>
											<!-- <th rowspan="2">#</th> -->
											<th rowspan="2">Facility Code</th>
											<th rowspan="2">Facility Name</th>
											<th rowspan="2">Sub-County</th>
											<th rowspan="2">County</th>
											<!-- <th rowspan="2">Allocated</th> -->
											<!-- <th rowspan="2">Issued from KEMSA</th> -->
											<th rowspan="2">Beginning Balance</th>
											<th rowspan="2">Quantity Received</th>
											<th rowspan="2">Quantity Used</th>
											<th rowspan="2">Tests Done</th>
											<th rowspan="2">Losses</th>
											<th colspan="2">Adjustments</th>
											<!-- <th rowspan="2">Adjustments(-)</th> -->
											<th rowspan="2">Closing Balance</th>
										</tr>
										<tr>
											<th>Positive</th>
											<th>Negative</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
			</div>
			</div>		

		<?php else: ?>
			
			 
			<div class="row portlet light">
			<div class="col-md-6 col-sm-6 no-spacing">
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Reporting Rates Trend</span>
								<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>

							</div>
						</div>
						<div class="portlet-body">
							<div id="site_activities_content" class="">
								<div id="trend-chart" style="height:300px;"></div>
							</div>
						</div>
					</div>

			</div> 

			<div class="col-md-6 col-sm-6 no-margin no-padding">
			
				<div class="portlet light ">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-share font-red-sunglo hide"></i>
							<span class="caption-subject font-green-haze bold uppercase">Allocated vs Received vs Consumed </span>
							<span class="caption-helper"><?php echo $title_append; ?></span>

						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-padding">
							<div class="col-md-6 no-padding">
								<div id="site_activities_content" class="">
									<div id="utilization_graph_scr" style="height: 300px;"></div>
								</div>
							</div>
							<div class="col-md-6 no-padding">
								<div id="site_activities_content" class="">
									<div id="utilization_graph_conf" style="height: 300px;"></div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div> 
			
			</div>

			 
			<div class="clearfix"></div>
			<div class="row portlet light">

			<div class="col-md-6">
				<div class="portlet light no-padding">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-share font-red-sunglo hide"></i>
							<span class="caption-subject font-blue-steel bold uppercase"><?php echo $title_prepend; ?> Stock Card </span>
								<span class="caption-helper"> <?php echo $title_append ?> </span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-scrollable">
							<table class="table table-bordered table-hover">
							<thead>
								<th></th>
								<th>Screening</th>
								<th>Confirmatory</th>
							</thead>
							
							<tbody>
							
							<tr>
								<td>
									 <strong>Tests Done</strong>
								</td>
								<td>
									 <?php echo number_format($screening_tests_done); ?>
								</td>
								<td>
									 <?php echo number_format($confirmatory_tests_done); ?>
								</td>
							</tr>
 			
							<!-- <tr>
								<td>
									 <strong>Closing balance</strong>
								</td>
								<td>
									 <?php echo number_format($screening_closing_balance); ?>
								</td>
								<td>
									 <?php echo number_format($confirmatory_closing_balance); ?>
								</td>
							</tr> -->

							<!-- <tr>
								<td>
									 <strong>Requested</strong>
								</td>
								<td>
									 <?php echo $screening_requested = number_format($screening_requested); ?>
								</td>
								<td>
									 <?php echo $confirmatory_requested = number_format($confirmatory_requested); ?>
								</td>
							</tr> 
							-->
			
							<tr>
								<td>
									 <strong>Expiries</strong>
								</td>
								<td>
									 <?php echo number_format($screening_expiring); ?>
								</td>
								<td>
									 <?php echo number_format($confirmatory_expiring); ?>
								</td>
							</tr>

							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			

			<div class="col-md-6">
				<div class="portlet light no-spacing">
					<div class="portlet-title">
						<div class="caption">
							<span class="caption-subject font-blue-steel bold uppercase"><?php echo $title_prepend; ?> Summary </span>
								<span class="caption-helper"><strong><?php echo $title_append; ?></strong></span>
						</div>
					</div>
					<div class="portlet-body">
					<?php
						if ($county_id > 0) {
							$t = "Total allocated by county.";
						}else{
							$t = "Total allocated by counties.";
						}

						if ($quartered > 0) {
							$a = "Quarter balance.";
						}else{
							$a = "Available balance for next allocation.";
						}

						
					?>
				 
						<div class="table-scrollable">
							<table class="table table-bordered table-hover">
							<thead>
								<th></th>
								<th>Screening</th>
								<th>Confirmatory</th>
							</thead>
							
							<tbody>
							
							<!-- <tr>
								<td>
									 <strong>Total drawing rights</strong>
								</td>
								<td>
									<?php echo number_format($drawing_total_screening); ?>
								</td>
								<td>
									<?php echo number_format($drawing_total_confirmatory); ?>
								</td>
							</tr> -->

							<!-- <tr>
								<td>
									 <strong><?php echo $t; ?></strong>
								</td>
								<td>
										 <?php //echo number_format($drawing_distributed_screening); ?>
										 <?php echo number_format($total_allocated_screening); ?>
								</td>
								<td>
										 <?php //echo number_format($drawing_distributed_confirmatory); ?>
										 <?php echo number_format($total_allocated_confirmatory); ?>
								</td>
							</tr> -->

							<tr>
								<td class="indent">
									 <strong>- Issued from KEMSA  <small>(<?php echo "Issue date: ".date('F Y',strtotime($kemsa_issue_date)); ?>)</small> </strong>
								</td>
								<td>
									<?php echo number_format($kemsa_issued_screening); ?>
								</td>
								<td>
									<?php echo number_format($kemsa_issued_confirmatory); ?>
								</td>
							</tr>

							<tr>
								<td class="indent">
									 <!-- <strong>- Received at site <small>(<?php echo "Order date: ".date('F Y',strtotime($summary_data_date)); ?>)</small> </strong> -->
									 <strong>- Received at site <small>(<?php echo "Order date: ".$summary_data_date_text; ?>)</small> </strong>
								</td>
								<td>
										 <?php echo number_format($received_site_screening); ?>
								</td>
								<td>
										 <?php echo number_format($received_site_confirmatory); ?>
								</td>
							</tr>

							<tr>
								<td class="indent">
									 <!-- <strong>- Consumed at site <small>(<?php echo "Order date: ".date('F Y',strtotime($summary_data_date)); ?>)</small> </strong> -->
									 <strong>- Consumed at site <small>(<?php echo "Order date: ".$summary_data_date_text; ?>)</small> </strong>
								</td>
								<td>
										 <?php echo number_format($consumed_site_screening); ?>
								</td>
								<td>
										 <?php echo number_format($consumed_site_confirmatory); ?>
								</td>
							</tr>

							<tr>
								<td class="indent">
									 <strong>- Beginning balance <small>(<?php echo date('F Y',strtotime($beginning_balance_order_date)) ?>)</small> </strong>
								</td>
								<td>
									<?php echo number_format($beginning_balance_screening); ?>
								</td>
								<td>
									<?php echo number_format($beginning_balance_confirmatory); ?>
								</td>
							</tr>

							<tr>
								<td class="indent">
									 <strong>- Closing balance <span class=""><small>(<?php echo date('F Y',strtotime($closing_balance_order_date)) ?>)</small></span> </strong>
								</td>
								<td>
									<?php echo number_format($closing_balance_screening); ?>
								</td>
								<td>
									<?php echo number_format($closing_balance_confirmatory); ?>
								</td>
							</tr>

							<!-- <tr>
								<td>
									 <strong> <?php echo $a; ?> </strong>
								</td>
								<td>
										 <?php echo number_format($drawing_balance_screening); ?>
								</td>
								<td>
										 <?php echo number_format($drawing_balance_confirmatory); ?>
								</td>
							</tr> -->

							<!-- <tr>
								<td>
									 <strong> <?php echo $a; ?> </strong>
								</td>
								<td>
										 <?php echo number_format($drawing_balance_screening); ?>
								</td>
								<td>
										 <?php echo number_format($drawing_balance_confirmatory); ?>
								</td>
							</tr> -->

							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			
				
			</div>

			<div class="clearfix"></div>
			<div class="row portlet light">

			<div class="col-md-12">
				<div class="portlet light no-padding">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-share font-red-sunglo hide"></i>
							<span class="caption-subject font-blue-steel bold uppercase"><?php echo $title_prepend; ?> Orders </span>
								<span class="caption-helper"> <?php echo $title_append ?> </span>
						</div>
					</div>
					<div class="portlet-body">
						<table class="table datatable compact table-bordered table-hover">
							<thead>
								<th>County</th>
								<th>Subcounty</th>
								<th>Facility Code</th>
								<th>Facility Name</th>
								<th>Order Date</th>
								<th>Action</th>
							</thead>
							
							<tbody>
								
								<?php foreach ($facility_orders as $key => $value) { ?>
								<tr>
									<td>
										 <?php echo $value['county']; ?>
									</td>
									<td>
										 <?php echo $value['district']; ?>
									</td>
									<td>
										 <?php echo $value['facility_name']; ?>
									</td>
									<td>
										 <?php echo $value['facility_code']; ?>
									</td>
									<td>
										 <?php echo date('F Y',strtotime("-1 MONTH", strtotime($value['order_date']))); ?>
									</td>
									<td>
										<!-- <a href="<?php echo site_url('rtk_management/lab_order_details/' . $order['id']); ?>" class="link">View</a> -->
										<button class="btn btn-primary btn-sm col-md-12" data-toggle="modal" data-order-id="<?php echo $value['order_id']; ?>" data-target=".view-details-modal">View Details</button>
									</td>

								</tr>
								<?php } ?>

							</tbody>
						</table>
					</div>
				</div>
			</div>
				
			</div>	
				

			
			<!-- <h3 class="page-title no-margin">
			Allocations 
			<small> <?php echo date('Y', strtotime("-1 MONTH")); ?></small>
			</h3></br> -->

			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-12 col-sm-12 no-padding">
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase">Facility Consumption (Screening). </span>
								</br>
								<span class="caption-helper"> Quarters: <?php echo $drawing_rights_quarter." ".$title_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<div id="site_activities_content" class="">
								<div id="scr_consumption_chart_filtered" style="height: 550px;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-12 col-sm-12 no-padding">
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase">Facility Consumption (Confirmatory). </span>
								</br>
								<span class="caption-helper"> Quarters: <?php echo $drawing_rights_quarter." ".$title_append; ?></span>
								<span class="caption-helper"><?php //echo $title_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<div id="site_activities_content" class="">
								<div id="conf_consumption_chart_filtered" style="height: 550px;"></div>
							</div>
						</div>
					</div>
				</div>
			</div> 
		
		<?php endif; ?>

		</div>
	</div>
	<!-- END CONTENT -->
</div>

<div class="modal fade view-details-modal" id="view-details-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
        <h3 class="modal-title" id="myLargeModalLabel">FCDRR Details</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>

      <div class="modal-body" >
      	<div id="modal-body-data">
        	<!-- <i class="fa fa-cog fa-spin"></i> -->
      	</div>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div> -->
  </div>
</div>
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 <?php echo date('Y'); ?> &copy; RTK.
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
<?php $this->load->view('v2/dashboard/dashboard_footer'); ?>
</html>

<script>
	
</script>

<script>
var url ='<?php echo base_url()?>';
		// alert("I work");
var county_id = <?php echo json_encode($county_id); ?>;
var subcounty_id = <?php echo json_encode($subcounty_id); ?>;
var facility_code = <?php echo json_encode($facility_code); ?>;
var quarter = <?php echo json_encode($quarter); ?>;
var filtered_month = <?php echo json_encode($filtered_month); ?>;
var filtered_year = <?php echo json_encode($filtered_year); ?>;
if (filtered_month < 1) {filtered_month = "NULL"}
if (filtered_year < 1) {filtered_year = "NULL"}

$(function () { 
	$('.datatable').DataTable();

	$('#view-details-modal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var order_id = button.data('order-id') // Extract info from data-* attributes
		var modal = $(this)
		// modal.find('.modal-title').text('New message to ' + recipient)
		// modal.find('.modal-body').html(level + " "+ level_id);
		ajax_request_replace_div_content('dashboardv2/get_facility_order_details/'+order_id,"#modal-body-data");

	})


	$('#summary_datatable_s').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":{
		     "url": "<?php echo base_url().'dashboardv2/get_facility_summary_table_data/4'; ?>",
		     "dataType": "json",
		     "type": "POST",
		     "data":{},
		           },
	    	"columns": [
		          { "data": "facility_code" },
		          { "data": "facility_name" },
		          { "data": "district_name" },
		          { "data": "county_name" },
		          { "data": "beg_bal" },
		          { "data": "q_rec" },
		          { "data": "q_used" },
		          { "data": "tests_done" },
		          { "data": "losses" },
		          { "data": "positive_adj" },
		          { "data": "negative_adj" },
		          { "data": "closing_bal" },
		       ]	 

	});

	$('#summary_datatable_c').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":{
		     "url": "<?php echo base_url().'dashboardv2/get_facility_summary_table_data/5'; ?>",
		     "dataType": "json",
		     "type": "POST",
		     "data":{}
		           },
	    	"columns": [
		          { "data": "facility_code" },
		          { "data": "facility_name" },
		          { "data": "district_name" },
		          { "data": "county_name" },
		          { "data": "beg_bal" },
		          { "data": "q_rec" },
		          { "data": "q_used" },
		          { "data": "tests_done" },
		          { "data": "losses" },
		          { "data": "positive_adj" },
		          { "data": "negative_adj" },
		          { "data": "closing_bal" },
		       ]	 

	});

	<?php if($filtered > 0): ?>
		<?php echo $utilization_graph_scr; ?>;
		<?php echo $utilization_graph_conf; ?>;

		if (quarter > 0) {
			/*ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/NULL/'+facility_code+'/4/NULL/'+quarter+'/scr_consumption_chart',"#scr_consumption_chart"); 
			ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/NULL/'+facility_code+'/5/NULL/'+quarter+'/conf_consumption_chart',"#conf_consumption_chart");*/

			ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/NULL/'+facility_code+'/4/'+filtered_month+'/'+quarter+'/scr_consumption_chart_filtered/'+filtered_year,"#scr_consumption_chart_filtered"); 

			ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/NULL/'+facility_code+'/5/'+filtered_month+'/'+quarter+'/conf_consumption_chart_filtered/'+filtered_year,"#conf_consumption_chart_filtered");

		}else{
			ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/NULL/'+facility_code+'/4/'+filtered_month+'/'+quarter+'/scr_consumption_chart_filtered/'+filtered_year,"#scr_consumption_chart_filtered"); 

			ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/NULL/'+facility_code+'/5/'+filtered_month+'/'+quarter+'/conf_consumption_chart_filtered/'+filtered_year,"#conf_consumption_chart_filtered");
		}


    <?php endif; ?>

	$(".select2").select2({
          placeholder: "0",
          // containerCssClass: "float-left" 
        });

	/*START OF INHERIT*/
	$('.top_filter_button').button().click(function(x){
          x.preventDefault(); 

          var county_text = $('#county_filter option:selected').text();
          var subcounty_text = $('#sub_county_filter option:selected').text();
          var facility_text = $('#facility_filter option:selected').text();

          var county_id = $('#county_filter option:selected').val();
          var subcounty_id = $('#sub_county_filter option:selected').val();
          var facility_code_ = $('#facility_filter option:selected').val();
          var quarter = $('#quarter_filter option:selected').val();
          var month_year = $('#month_filter option:selected').val();

          if(facility_code_==0){
            window.location.replace(url+"dashboardv2/facility_dashboard/NULL/NULL/NULL/"+quarter+"/"+month_year);
          }else{
            if(facility_code_ > 0){
            	window.location.replace(url+"dashboardv2/facility_dashboard/NULL/NULL/"+facility_code_+"/"+quarter+"/"+month_year);
            }
          }
          // alert(county_id);
          // alert(subcounty_id);
       });

	// $('.datatable').DataTable();
  
});/*END OF JQUERY FUNCTION*/

/*console.log("MFL: "+facility_code);
console.log("QUARTER: "+quarter);*/
if (facility_code > 0) { 
	$('.select2-fac-search').val(facility_code).trigger('change');  
        <?php if($filtered > 0): ?>
			ajax_request_replace_div_content('dashboardv2/get_national_trend/NULL/NULL/'+facility_code,"#trend-chart"); 
        <?php endif; ?>
        if (quarter > 0) {
			ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/NULL/'+facility_code+'/NULL/4/'+filtered_month+'/'+quarter+'/scr_consumption_chart/'+filtered_year,"#scr_consumption_chart"); 
			ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/NULL/'+facility_code+'/NULL/5/'+filtered_month+'/'+quarter+'/conf_consumption_chart/'+filtered_year,"#conf_consumption_chart");
		}else{
			ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/NULL/'+facility_code+'/NULL/4/'+filtered_month+'/'+quarter+'/scr_consumption_chart/'+filtered_year,"#scr_consumption_chart"); 
			ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/NULL/'+facility_code+'/NULL/5/'+filtered_month+'/'+quarter+'/conf_consumption_chart/'+filtered_year,"#conf_consumption_chart"); 
		}

		/*ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/NULL/'+facility_code+'/4/NULL/NULL/scr_consumption_chart',"#scr_consumption_chart"); 
		ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/NULL/'+facility_code+'/5/NULL/NULL/conf_consumption_chart',"#conf_consumption_chart"); */
}else{
	// ajax_request_replace_div_content('dashboardv2/get_national_trend/',"#trend-chart"); 
	/*ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/NULL/NULL/4/NULL/NULL/scr_consumption_chart',"#scr_consumption_chart"); 
	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/NULL/NULL/5/NULL/NULL/conf_consumption_chart',"#conf_consumption_chart");*/ 

		if (quarter > 0) {
			ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/NULL/NULL/4/'+filtered_month+'/'+quarter+'/scr_consumption_chart/'+filtered_year,"#scr_consumption_chart"); 
			ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/NULL/NULL/5/'+filtered_month+'/'+quarter+'/conf_consumption_chart/'+filtered_year,"#conf_consumption_chart");
		}else{

			ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/NULL/NULL/4/'+filtered_month+'/'+quarter+'/scr_consumption_chart/'+filtered_year,"#scr_consumption_chart"); 
			ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/NULL/NULL/5/'+filtered_month+'/'+quarter+'/conf_consumption_chart/'+filtered_year,"#conf_consumption_chart"); 
		}


	// ajax_request_replace_div_content('dashboardv2/get_facility_summary_table/facility/NULL/NULL/NULL/4/NULL/NULL/scr_fac_data_table',"#scr_fac_data_table"); 
	// ajax_request_replace_div_content('dashboardv2/get_facility_summary_table/facility/NULL/NULL/NULL/5/NULL/NULL/conf_fac_data_table',"#conf_fac_data_table"); 
}

function ajax_request_replace_div_content(function_url,div){
	var function_url =url+function_url;
	var loading_icon=url+"assets/img/loader2.gif";
	$.ajax({
		type: "POST",
		url: function_url,
		beforeSend: function() {
			$(div).html("<img style='margin-left:20%;' src="+loading_icon+">");
		},
		success: function(msg) {
			// console.log(msg);
			$(div).html(msg);
			$('.datatable').DataTable();
		}
	});
} 

</script>