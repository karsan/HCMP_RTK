<?php $this->load->view('v2/dashboard/dashboard_header'); ?>
<?php //echo "<pre>";print_r($county_id);exit; ?>
<?php //echo $test_graph;exit; ?>

<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solid  page-sidebar-closed">
	<style>
		.red{
			color:red!important;
		}
		.green{
			color:green!important;
		}
		.btn{
			width: 100%;
		}
	</style>
<?php $this->load->view('v2/dashboard/dashboard_top_header'); ?>
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->

<div class="page-container">
<?php $this->load->view('v2/dashboard/dashboard_sidebar'); ?>
	
	<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<!-- <div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<a href="<?php //echo base_url(); ?>">Dashboard</a>
						</li>
					</ul>
				</div> -->
						
			<h3 class="page-title no-margin">
			Balance Discrepancy Dashboard 
			<!-- <small> data as at <?php //echo date('F/Y') ?> </small> -->
			<small> data as of <?php echo date('F Y', strtotime("-1 MONTH")) ?> reports. </small>
			</h3></br>
			<!-- END PAGE HEADER-->

			<div class="clearfix"></div>
			<div class="row portlet light no-margin-bottom">
			<div class="portlet-body margin-sm-hor clearfix">
				<?php if ($quartered > 0) { ?>
					<!-- <div class="col-md-12 no-padding clearfix">
						<strong><p class="no-margin" style="line-height: 1.5em"> <span class="font-blue-steel">*</span> Fields with an asterisk do not change when filtered quarterly.</p></strong>
					</div> -->
				<?php } ?>

                 <div class="col-md-5 padding-hor-sm no-margin no-padding">
				  <select class="form-control select2 padding-hor-sm" id="year_filter">
				  <option value="0">Select Year</option>
				  <?php foreach ($filter_years as $year => $value):?> 
				      <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
				  <?php endforeach; ?>
				  </select>
				</div>

                  <div class="col-md-5 padding-hor-sm no-margin no-padding">
                    <select class="form-control select2 padding-hor-sm" id="quarter_filter">
                    <option value="0">All quarters</option>
                    <?php foreach ($quarters as $qr => $value):?> 
                      <option value="<?php echo $value['quarter_year']; ?>"><?php echo $value['quarter_text']; ?></option>
                  	<?php endforeach; ?>
                    </select> 

                  </div>
                 <div class="col-md-2 no-padding no-margin">
                    <button class="btn btn-primary top_filter_button filter_button">Filter</button>
                  </div>
			</div>
			
            </div>

        <?php if($filtered == 0): ?>
			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<!-- <i class="icon-share font-red-sunglo hide"></i> -->
							<span class="caption-subject font-green-haze bold uppercase">National Summary </span>
							<!-- <span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span> -->
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<!-- <th></th> -->
									<th colspan="3">Screening</th>
									<th colspan="3">Confirmatory</th>
								</tr>
								<tr>
									<th>Expected Balance</th>
									<th>Reported</th>
									<th>Difference</th>
									<th>Expected Balance</th>
									<th>Reported</th>
									<th>Difference</th>
								</tr>
							</thead>
							
							<tbody>
								<?php 
									// echo "<pre>";print_r($national_order_data);
									$beg_bal_s = $national_order_data_s['beginning_bal'];
									$rec_s = $national_order_data_s['q_received'];
									$pos_adj_s = $national_order_data_s['positive_adj'];
									$used_s = $national_order_data_s['q_used'];
									$losses_s = $national_order_data_s['losses'];
									$neg_adj_s = $national_order_data_s['negative_adj'];
									$clos_bal_s = $national_order_data_s['closing_stock'];
									$reported_s = $national_order_data_s['closing_stock'];

									$expected_s = ($beg_bal_s + $rec_s + $pos_adj_s)-($used_s + $losses_s + $neg_adj_s); 
									// echo "<pre>";print_r($expected_s);exit;
									$difference_s = $reported_s - $expected_s;

									$class_s = ($difference_s>0)? " green ":" red ";
									$class_s = ($difference_s==0)? "":$class_s;

									$beg_bal_c = $national_order_data_c['beginning_bal'];
									$rec_c = $national_order_data_c['q_received'];
									$pos_adj_c = $national_order_data_c['positive_adj'];
									$used_c = $national_order_data_c['q_used'];
									$losses_c = $national_order_data_c['losses'];
									$neg_adj_c = $national_order_data_c['negative_adj'];
									$clos_bal_c = $national_order_data_c['closing_stock'];
									$reported_c = $national_order_data_c['closing_stock'];

									$expected_c = ($beg_bal_c + $rec_c + $pos_adj_c)-($used_c + $losses_c + $neg_adj_c); 

									$difference_c = $reported_c - $expected_c;

									$class_c = ($difference_c>0)? " green ":" red ";
									$class_c = ($difference_c==0)? "":$class_c;

								 ?>
							<tr>
								<td><?php echo number_format($expected_s) ?></td>
								<td><?php echo number_format($reported_s) ?></td>
								<td class="<?php echo $class_s ?>"><strong><?php echo number_format($difference_s); ?></strong></td>
								<td><?php echo number_format($expected_c) ?></td>
								<td><?php echo number_format($reported_c) ?></td>
								<td class="<?php echo $class_c ?>"><strong><?php echo number_format($difference_c); ?></strong></td>
							</tr>

							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<!-- <i class="icon-share font-red-sunglo hide"></i> -->
							<span class="caption-subject font-green-haze bold uppercase">County Balances (Screening)</span>
							<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table c_datatable display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th rowspan="2">#</th>
										<th rowspan="2">County</th>
										<th rowspan="2">Beginning Balance</th>
										<!-- <th>Beginning Balance Date</th> -->
										<th rowspan="2">Received</th>
										<th rowspan="2">Used</th>
										<th colspan="2">Adjustments</th>
										<th rowspan="2">Losses</th>
										<!-- <th rowspan="2">Closing Balance</th> -->
										<!-- <th>Closing Balance Date</th> -->
										<th rowspan="2">Expected Balance</th>
										<th rowspan="2">Reported</th>
										<th rowspan="2">Difference</th>
										<th rowspan="2">Action</th>
									</tr>
									<tr>
										<th>+ve</th>
										<th>-ve</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										foreach ($county_data_s as $key => $value) { 
											$beg_bal = $value['beginning_bal'];
											$rec = $value['q_received'];
											$pos_adj = $value['positive_adj'];
											$used = $value['q_used'];
											$losses = $value['losses'];
											$neg_adj = $value['negative_adj'];

											$clos_bal = $value['closing_stock'];

											// $expected = $value['beginning_bal'] - $value['q_used'];
											$reported = $value['closing_stock'];

											$expected = ($beg_bal + $rec + $pos_adj)-($used + $losses + $neg_adj); 
											// echo "<pre>";print_r($reported);
											// echo "<pre>";print_r($expected);exit;
											$difference = $reported - $expected;
											$class = ($difference>0)? " green ":" red ";
											$class = ($difference==0)? "":$class;

										if ($difference != 0):
									?>
									<tr>
										<td></td>
										<td><?php echo $value['county_name']; ?></td>
										<td><?php echo number_format($value['beginning_bal']); ?></td>
										<!-- <td><?php echo date('F Y',strtotime($value['beginning_order_date'])); ?></td> -->
										<td><?php echo number_format($value['q_received']); ?></td>
										<td><?php echo number_format($value['q_used']); ?></td>
										<td><?php echo number_format($value['positive_adj']); ?></td>
										<td><?php echo number_format($value['negative_adj']); ?></td>
										<td><?php echo number_format($value['losses']); ?></td>
										<!-- <td><?php echo date('F Y',strtotime($value['closing_order_date'])); ?></td> -->
										<td><?php echo number_format($expected) ?></td>
										<td><?php echo number_format($reported) ?></td>
										<td class="<?php echo $class ?>"><strong><?php echo number_format($difference); ?></strong></td>
										<td><button class="btn btn-primary" data-level = "<?php echo $value['level'] ?>" data-level-id = "<?php echo $value['level_id'] ?>" data-commodity-id = "<?php echo $value['commodity_id'] ?>" data-order-date = "<?php echo date('mY',strtotime($value['beginning_order_date'])) ?>" data-toggle="modal" data-target=".view-details-modal">View Details</button></td>
									</tr>
									<?php endif; } ?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>	

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-share font-red-sunglo hide"></i>
							<span class="caption-subject font-green-haze bold uppercase">County Balances (Confirmatory)</span>
							<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table c_datatable_c display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th rowspan="2">#</th>
										<th rowspan="2">County</th>
										<th rowspan="2">Beginning Balance</th>
										<!-- <th>Beginning Balance Date</th> -->
										<th rowspan="2">Received</th>
										<th rowspan="2">Used</th>
										<th colspan="2">Adjustments</th>
										<th rowspan="2">Losses</th>
										<!-- <th rowspan="2">Closing Balance</th> -->
										<!-- <th>Closing Balance Date</th> -->
										<th rowspan="2">Expected Balance</th>
										<th rowspan="2">Reported</th>
										<th rowspan="2">Difference</th>
										<th rowspan="2">Action</th>
									</tr>
									<tr>
										<th>+ve</th>
										<th>-ve</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										foreach ($county_data_c as $key => $value) { 
										$beg_bal = $value['beginning_bal'];
										$rec = $value['q_received'];
										$pos_adj = $value['positive_adj'];
										$used = $value['q_used'];
										$losses = $value['losses'];
										$neg_adj = $value['negative_adj'];

										$clos_bal = $value['closing_stock'];

										$expected = $value['beginning_bal'] - $value['q_used'];
										$reported = $value['closing_stock'];

										$expected = ($beg_bal + $rec + $pos_adj)-($used + $losses + $neg_adj); 

										$difference = $reported - $expected;
										$class = ($difference>0)? " green ":" red ";
										$class = ($difference==0)? "":$class;

										if ($difference != 0):
									?>
									<tr>
										<td></td>
										<td><?php echo $value['county_name']; ?></td>
										<td><?php echo number_format($value['beginning_bal']); ?></td>
										<!-- <td><?php echo date('F Y',strtotime($value['beginning_order_date'])); ?></td> -->
										<td><?php echo number_format($value['q_received']); ?></td>
										<td><?php echo number_format($value['q_used']); ?></td>
										<td><?php echo number_format($value['positive_adj']); ?></td>
										<td><?php echo number_format($value['negative_adj']); ?></td>
										<td><?php echo number_format($value['losses']); ?></td>
										<!-- <td><?php echo date('F Y',strtotime($value['closing_order_date'])); ?></td> -->
										<td><?php echo number_format($expected) ?></td>
										<td><?php echo number_format($reported) ?></td>
										<td class="<?php echo $class ?>"><strong><?php echo number_format($difference); ?></strong></td>
										<td><button class="btn btn-primary" data-level = "<?php echo $value['level'] ?>" data-level-id = "<?php echo $value['level_id'] ?>" data-commodity-id = "<?php echo $value['commodity_id'] ?>" data-toggle="modal" data-target=".view-details-modal">View Details</button></td>
									</tr>
									<?php endif;} ?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>	

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<!-- <i class="icon-share font-red-sunglo hide"></i> -->
							<span class="caption-subject font-green-haze bold uppercase">Sub-County Balances (Screening)</span>
							<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table s_datatable display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th rowspan="2">#</th>
										<th rowspan="2">Sub-County</th>
										<th rowspan="2">County</th>
										<!-- <th>Beginning Balance Date</th> -->
										<th rowspan="2">Beginning Balance</th>
										<th rowspan="2">Received</th>
										<th rowspan="2">Used</th>
										<th colspan="2">Adjustments</th>
										<th rowspan="2">Losses</th>
										<!-- <th rowspan="2">Closing Balance</th> -->
										<!-- <th>Closing Balance Date</th> -->
										<th rowspan="2">Expected Balance</th>
										<th rowspan="2">Reported</th>

										<!-- <th>Expected Balance</th> -->
										<!-- <th>Reported</th> -->
										<th rowspan="2">Difference</th>
										<th rowspan="2">Action</th>
									</tr>
									<tr>
										<th>+ve</th>
										<th>-ve</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										foreach ($district_data_s as $key => $value) { 
										$beg_bal = $value['beginning_bal'];
										$rec = $value['q_received'];
										$pos_adj = $value['positive_adj'];
										$used = $value['q_used'];
										$losses = $value['losses'];
										$neg_adj = $value['negative_adj'];

										$clos_bal = $value['closing_stock'];

										$expected = $value['beginning_bal'] - $value['q_used'];
										$reported = $value['closing_stock'];

										$expected = ($beg_bal + $rec + $pos_adj)-($used + $losses + $neg_adj); 

										$difference = $reported - $expected;
										$class = ($difference>0)? " green ":" red ";
										$class = ($difference==0)? "":$class;

										if ($difference != 0):
									?>
									<tr>
										<td></td>
										<td><?php echo $value['district_name']; ?></td>
										<td><?php echo $value['county_name']; ?></td>
										<td><?php echo number_format($value['beginning_bal']); ?></td>
										<!-- <td><?php echo date('F Y',strtotime($value['beginning_order_date'])); ?></td> -->
										<td><?php echo number_format($value['q_received']); ?></td>
										<td><?php echo number_format($value['q_used']); ?></td>
										<td><?php echo number_format($value['positive_adj']); ?></td>
										<td><?php echo number_format($value['negative_adj']); ?></td>
										<td><?php echo number_format($value['losses']); ?></td>

										<td><?php echo number_format($expected) ?></td>
										<td><?php echo number_format($reported) ?></td>
										<td class="<?php echo $class ?>"><strong><?php echo number_format($difference); ?></strong></td>
										<td><button class="btn btn-primary" data-level = "<?php echo $value['level'] ?>" data-level-id = "<?php echo $value['level_id'] ?>" data-commodity-id = "<?php echo $value['commodity_id'] ?>" data-toggle="modal" data-target=".view-details-modal">View Details</button></td>
									</tr>
									<?php endif;} ?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>	

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-share font-red-sunglo hide"></i>
							<span class="caption-subject font-green-haze bold uppercase">Sub-County Balances (Confirmatory)</span>
							<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table s_datatable_c display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th rowspan="2">#</th>
										<th rowspan="2">Sub-County</th>
										<th rowspan="2">County</th>
										<!-- <th>Beginning Balance Date</th> -->
										<th rowspan="2">Beginning Balance</th>
										<th rowspan="2">Received</th>
										<th rowspan="2">Used</th>
										<th colspan="2">Adjustments</th>
										<th rowspan="2">Losses</th>
										<!-- <th rowspan="2">Closing Balance</th> -->
										<!-- <th>Closing Balance Date</th> -->
										<th rowspan="2">Expected Balance</th>
										<th rowspan="2">Reported</th>

										<!-- <th>Expected Balance</th> -->
										<!-- <th>Reported</th> -->
										<th rowspan="2">Difference</th>
										<th rowspan="2">Action</th>
									</tr>
									<tr>
										<th>+ve</th>
										<th>-ve</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										foreach ($district_data_c as $key => $value) { 
										$beg_bal = $value['beginning_bal'];
										$rec = $value['q_received'];
										$pos_adj = $value['positive_adj'];
										$used = $value['q_used'];
										$losses = $value['losses'];
										$neg_adj = $value['negative_adj'];

										$clos_bal = $value['closing_stock'];

										$expected = $value['beginning_bal'] - $value['q_used'];
										$reported = $value['closing_stock'];

										$expected = ($beg_bal + $rec + $pos_adj)-($used + $losses + $neg_adj); 

										$difference = $reported - $expected;
										$class = ($difference>0)? " green ":" red ";
										$class = ($difference==0)? "":$class;

										if ($difference != 0):
									?>
									<tr>
										<td></td>
										<td><?php echo $value['district_name']; ?></td>
										<td><?php echo $value['county_name']; ?></td>
										<td><?php echo number_format($value['beginning_bal']); ?></td>
										<!-- <td><?php echo date('F Y',strtotime($value['beginning_order_date'])); ?></td> -->
										<td><?php echo number_format($value['q_received']); ?></td>
										<td><?php echo number_format($value['q_used']); ?></td>
										<td><?php echo number_format($value['positive_adj']); ?></td>
										<td><?php echo number_format($value['negative_adj']); ?></td>
										<td><?php echo number_format($value['losses']); ?></td>
										
										<td><?php echo number_format($expected) ?></td>
										<td><?php echo number_format($reported) ?></td>
										<td class="<?php echo $class ?>"><strong><?php echo number_format($difference); ?></strong></td>
										<td><button class="btn btn-primary" data-level = "<?php echo $value['level'] ?>" data-level-id = "<?php echo $value['level_id'] ?>" data-commodity-id = "<?php echo $value['commodity_id'] ?>" data-toggle="modal" data-target=".view-details-modal">View Details</button></td>
									</tr>
									<?php endif; } ?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-share font-red-sunglo hide"></i>
							<span class="caption-subject font-green-haze bold uppercase">Facility Balances (Screening)</span>
							<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table f_datatable display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>#</th>
										<th>Facility Name</th>
										<th>MFL Code</th>
										<th>Sub-County</th>
										<th>County</th>
										<th>Expected Balance</th>
										<th>Reported</th>
										<th>Difference</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										foreach ($facility_data_s as $key => $value) { 
										$beg_bal = $value['beginning_bal'];
										$rec = $value['q_received'];
										$pos_adj = $value['positive_adj'];
										$used = $value['q_used'];
										$losses = $value['losses'];
										$neg_adj = $value['negative_adj'];

										$clos_bal = $value['closing_stock'];

										$expected = $value['beginning_bal'] - $value['q_used'];
										$reported = $value['closing_stock'];

										$expected = ($beg_bal + $rec + $pos_adj)-($used + $losses + $neg_adj); 

										$difference = $reported - $expected;
										$class = ($difference>0)? " green ":" red ";
										$class = ($difference==0)? "":$class;

										if ($difference != 0):
									?>
									<tr>
										<td></td>
										<td><?php echo $value['facility_name']; ?></td>
										<td><?php echo $value['facility_code']; ?></td>
										<td><?php echo $value['district_name']; ?></td>
										<td><?php echo $value['county_name']; ?></td>
										<td><?php echo number_format($expected) ?></td>
										<td><?php echo number_format($reported) ?></td>
										<td class="<?php echo $class ?>"><strong><?php echo number_format($difference); ?></strong></td>
										<td><a href="#" class="btn btn-primary">View FCDRR</a></td>
										<!-- <td><button class="btn btn-primary" data-level = "<?php echo $value['level'] ?>" data-level-id = "<?php echo $value['level_id'] ?>" data-commodity-id = "<?php echo $value['commodity_id'] ?>" data-toggle="modal" data-target=".view-details-modal">View FCDRR</button></td> -->
									</tr>
									<?php endif; } ?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>	

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-share font-red-sunglo hide"></i>
							<span class="caption-subject font-green-haze bold uppercase">Facility Balances (Confirmatory)</span>
							<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table f_datatable_c display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>#</th>
										<th>Facility Name</th>
										<th>MFL Code</th>
										<th>Sub-County</th>
										<th>County</th>
										<th>Expected Balance</th>
										<th>Reported</th>
										<th>Difference</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										foreach ($facility_data_c as $key => $value) { 
										$beg_bal = $value['beginning_bal'];
										$rec = $value['q_received'];
										$pos_adj = $value['positive_adj'];
										$used = $value['q_used'];
										$losses = $value['losses'];
										$neg_adj = $value['negative_adj'];

										$clos_bal = $value['closing_stock'];

										$expected = $value['beginning_bal'] - $value['q_used'];
										$reported = $value['closing_stock'];

										$expected = ($beg_bal + $rec + $pos_adj)-($used + $losses + $neg_adj); 

										$difference = $reported - $expected;
										$class = ($difference>0)? " green ":" red ";
										$class = ($difference==0)? "":$class;

										if ($difference != 0):
									?>
									<tr>
										<td></td>
										<td><?php echo $value['facility_name']; ?></td>
										<td><?php echo $value['facility_code']; ?></td>
										<td><?php echo $value['district_name']; ?></td>
										<td><?php echo $value['county_name']; ?></td>
										<td><?php echo number_format($expected) ?></td>
										<td><?php echo number_format($reported) ?></td>
										<td class="<?php echo $class ?>"><strong><?php echo number_format($difference); ?></strong></td>
										<td><a href="#" class="btn btn-primary">View FCDRR</a></td>
										<!-- <td><button class="btn btn-primary" data-level = "<?php echo $value['level'] ?>" data-level-id = "<?php echo $value['level_id'] ?>" data-commodity-id = "<?php echo $value['commodity_id'] ?>" data-toggle="modal" data-target=".view-details-modal">View FCDRR</button></td> -->
									</tr>
									<?php endif;} ?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>	

			<!-- <div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-share font-red-sunglo hide"></i>
							<span class="caption-subject font-green-haze bold uppercase">Issued vs Received (Screening)</span>
							<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table fi_datatable display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>#</th>
										<th>Facility</th>
										<th>MFL Code</th>
										<th>Sub-County</th>
										<th>County</th>
										<th>Issued from KEMSA</th>
										<th>Received at site</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										foreach ($facility_data_s as $key => $value) { 
										$beg_bal = $value['beginning_bal'];
										$rec = $value['q_received'];
										$pos_adj = $value['positive_adj'];
										$used = $value['q_used'];
										$losses = $value['losses'];
										$neg_adj = $value['negative_adj'];

										$clos_bal = $value['closing_stock'];

										$expected = $value['beginning_bal'] - $value['q_used'];
										$reported = $value['closing_stock'];

										$expected = ($beg_bal + $rec + $pos_adj)-($used + $losses + $neg_adj); 

										$difference = $reported - $expected;
										$class = ($difference>0)? " green ":" red ";
										$class = ($difference==0)? "":$class;

										if ($difference != 0):
									?>
									<tr>
										<td></td>
										<td><?php echo $value['facility_name']; ?></td>
										<td><?php echo $value['facility_code']; ?></td>
										<td><?php echo $value['district_name']; ?></td>
										<td><?php echo $value['county_name']; ?></td>
										<td><center><?php echo "-" ?></center></td>
										<td><?php echo number_format($received) ?></td>
										<td><a href="#" class="btn btn-primary">View PODS</a></td>
									</tr>
									<?php endif;} ?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
			</div> -->

			<!-- <div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-share font-red-sunglo hide"></i>
							<span class="caption-subject font-green-haze bold uppercase">Issued vs Received (Confirmatory)</span>
							<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table fi_datatable_c display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>#</th>
										<th>Facility</th>
										<th>MFL Code</th>
										<th>Sub-County</th>
										<th>County</th>
										<th>Issued from KEMSA</th>
										<th>Received at site</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										foreach ($facility_data_c as $key => $value) { 
										$beg_bal = $value['beginning_bal'];
										$rec = $value['q_received'];
										$pos_adj = $value['positive_adj'];
										$used = $value['q_used'];
										$losses = $value['losses'];
										$neg_adj = $value['negative_adj'];

										$clos_bal = $value['closing_stock'];

										$expected = $value['beginning_bal'] - $value['q_used'];
										$reported = $value['closing_stock'];

										$expected = ($beg_bal + $rec + $pos_adj)-($used + $losses + $neg_adj); 

										$difference = $reported - $expected;
										$class = ($difference>0)? " green ":" red ";
										$class = ($difference==0)? "":$class;

										if ($difference != 0):
									?>
									<tr>
										<td></td>
										<td><?php echo $value['facility_name']; ?></td>
										<td><?php echo $value['facility_code']; ?></td>
										<td><?php echo $value['district_name']; ?></td>
										<td><?php echo $value['county_name']; ?></td>
										<td><center><?php echo "-" ?></center></td>
										<td><?php echo number_format($received) ?></td>
										<td><a href="#" class="btn btn-primary">View PODS</a></td>
									</tr>
									<?php endif;} ?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
			</div> -->	
 		
	<?php else: ?>

	<?php endif; ?>

		</div>
	</div>
</div>


<div class="modal fade view-details-modal" id="view-details-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width: 1000px;">
    <div class="modal-content">
    	<div class="modal-header">
        <h3 class="modal-title" id="myLargeModalLabel">Discrepancy details</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>

      <div class="modal-body" >
      	<div id="modal-body-data">
        	<!-- <i class="fa fa-cog fa-spin"></i> -->
      	</div>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div> -->
  </div>
</div>
</div>


<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 <?php echo date('Y'); ?> &copy; RTK.
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
<?php $this->load->view('v2/dashboard/dashboard_footer'); ?>
</html>

<script>
	
</script>

<script>
var url ='<?php echo base_url()?>';
		// alert("I work");

$(function () { 
/*DATATABLES*/
    // $('.datatable').DataTable();
    var a = $('.c_datatable').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    a.on( 'order.dt search.dt', function () {
        a.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    var b = $('.c_datatable_c').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    b.on( 'order.dt search.dt', function () {
        b.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    var c = $('.s_datatable').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    c.on( 'order.dt search.dt', function () {
        c.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    var d = $('.s_datatable_c').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    d.on( 'order.dt search.dt', function () {
        d.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    //facility
    var e = $('.f_datatable').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    e.on( 'order.dt search.dt', function () {
        e.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    var f = $('.f_datatable_c').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    f.on( 'order.dt search.dt', function () {
        f.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    var g = $('.fi_datatable').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    g.on( 'order.dt search.dt', function () {
        g.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    var h = $('.fi_datatable_c').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    h.on( 'order.dt search.dt', function () {
        h.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
/*END OF DATATABLES*/

    $('#view-details-modal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var level = button.data('level') // Extract info from data-* attributes
		var level_id = button.data('level-id') // Extract info from data-* attributes
		var commodity_id = button.data('commodity-id') // Extract info from data-* attributes
		var order_id = $(this).data('order-id') // Extract info from data-* attributes
		var order_date = $(this).data('order-date') // Extract info from data-* attributes
		// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		var modal = $(this)
		console.log('dashboardv2/get_discrepancy_details/'+level+'/'+level_id+'/'+commodity_id);
		// modal.find('.modal-title').text('New message to ' + recipient)
		// modal.find('.modal-body').html(level + " "+ level_id);
		ajax_request_replace_div_content('dashboardv2/get_discrepancy_details/'+level+'/'+level_id+'/'+commodity_id+'/'+order_date,"#modal-body-data");

	})

    $(document).on("click", "#modal_balance_details", function(event){
	    // alert( "GO" ); 
	    // console.log("CLICKED");
		var level = $(this).data('level') // Extract info from data-* attributes
		var level_id = $(this).data('level-id') // Extract info from data-* attributes
		var commodity_id = $(this).data('commodity-id') // Extract info from data-* attributes
		var order_id = $(this).data('order-id') // Extract info from data-* attributes
		var order_date = $(this).data('order-date') // Extract info from data-* attributes

		console.log('dashboardv2/get_discrepancy_details/'+level+'/'+level_id+'/'+commodity_id+'/'+order_date);
		if (order_id > 0) {
			ajax_request_replace_div_content('dashboardv2/get_facility_order_details/'+order_id,"#modal-body-data");
		}else{
			ajax_request_replace_div_content('dashboardv2/get_discrepancy_details/'+level+'/'+level_id+'/'+commodity_id+'/'+order_date,"#modal-body-data");
		}
	});

	<?php if($filtered > 0): ?>
	<?php echo $utilization_graph_scr; ?>;
	<?php echo $utilization_graph_conf; ?>;

	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/'+subcounty_id+'/NULL/4/NULL/NULL/scr_consumption_chart_filtered',"#scr_consumption_chart_filtered"); 

	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/'+subcounty_id+'/NULL/5/NULL/NULL/conf_consumption_chart_filtered',"#conf_consumption_chart_filtered"); 

    <?php endif; ?>
	<?php //echo $utilization_bgg_screening_graph; ?>;
	<?php //echo $utilization_bgg_confirmatory_graph; ?>;
	<?php //echo $test_graph; ?>;
	<?php //echo $drawing_rights_graph; ?>;
	<?php //echo $drawing_utilization_graph; ?>;

	$(".select2").select2({
          placeholder: "0",
          // containerCssClass: "float-left" 
        });

	/*START OF INHERIT*/
	$('.top_filter_button').button().click(function(x){
          x.preventDefault(); 

          var quarter = $('#quarter_filter option:selected').val();
          var year = $('#year_filter option:selected').val();

		  window.location.replace(url+"dashboardv2/balance_discrepancy_dashboard/NULL/NULL/"+year+"/"+quarter);
          
          // alert(county_id);
          // alert(subcounty_id);
       });

});/*END OF JQUERY FUNCTION*/

var county_id = <?php echo json_encode($county_id); ?>;
var subcounty_id = <?php echo json_encode($subcounty_id); ?>;
// console.log(subcounty_id);
if (subcounty_id > 0) { 
		ajax_request_replace_div_content('dashboardv2/get_national_trend/NULL/'+subcounty_id,"#trend-chart"); 
		ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/subcounty/NULL/'+subcounty_id+'/NULL/4/NULL/NULL/scr_consumption_chart',"#scr_consumption_chart"); 
		ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/subcounty/NULL/'+subcounty_id+'/NULL/5/NULL/NULL/conf_consumption_chart',"#conf_consumption_chart"); 
}else{
	// ajax_request_replace_div_content('dashboardv2/get_national_trend/',"#trend-chart"); 
	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/subcounty/NULL/NULL/NULL/4/NULL/NULL/scr_consumption_chart',"#scr_consumption_chart"); 
	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/subcounty/NULL/NULL/NULL/5/NULL/NULL/conf_consumption_chart',"#conf_consumption_chart"); 
}

function ajax_request_replace_div_content(function_url,div){
	var function_url =url+function_url;
	var loading_icon=url+"assets/img/loader2.gif";
	$.ajax({
		type: "POST",
		url: function_url,
		beforeSend: function() {
			$(div).html("<img style='margin-left:20%;' src="+loading_icon+">");
		},
		success: function(msg) {
			// console.log(msg);
			$(div).html(msg);
			$('#datatable').DataTable(); 
		}
	});
} 

</script>