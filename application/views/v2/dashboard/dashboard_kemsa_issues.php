<?php $this->load->view('v2/dashboard/dashboard_header'); ?>
<?php //echo "<pre>";print_r($county_id);exit; ?>
<?php //echo $test_graph;exit; ?>

<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solid  page-sidebar-closed">
	<style>
		.red{
			color:red!important;
		}
		.green{
			color:green!important;
		}
		.btn{
			width: 100%;
		}
	</style>
<?php $this->load->view('v2/dashboard/dashboard_top_header'); ?>
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->

<div class="page-container">
<?php $this->load->view('v2/dashboard/dashboard_sidebar'); ?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- <div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="<?php //echo base_url(); ?>">Dashboard</a>
					</li>
				</ul>
			</div> -->
						
			<h3 class="page-title no-margin">
			KEMSA Issues Dashboard 
			<!-- <small> data as at <?php //echo date('F/Y') ?> </small> -->
			<small> data as of <?php echo date('F Y', strtotime("-1 MONTH")) ?> reports. </small>
			</h3></br>
			<!-- END PAGE HEADER-->

			<div class="clearfix"></div>
			<div class="row portlet light no-margin-bottom">
			<div class="portlet-body margin-sm-hor clearfix">
			<?php if ($quartered > 0) { ?>
				<!-- <div class="col-md-12 no-padding clearfix">
					<strong><p class="no-margin" style="line-height: 1.5em"> <span class="font-blue-steel">*</span> Fields with an asterisk do not change when filtered quarterly.</p></strong>
				</div> -->
			<?php } ?>

                <div class="col-md-5 padding-hor-sm no-margin no-padding">
				  <select class="form-control select2 padding-hor-sm" id="year_filter">
				  <option value="0">Select Year</option>
				  <?php foreach ($filter_years as $year => $value):?> 
				      <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
				  <?php endforeach; ?>
				  </select>
				</div>

                  <div class="col-md-5 padding-hor-sm no-margin no-padding">
                    <select class="form-control select2 padding-hor-sm" id="quarter_filter">
                    <option value="0">All quarters</option>
                    <?php foreach ($quarters as $qr => $value):?> 
                      <option value="<?php echo $value['quarter_year']; ?>"><?php echo $value['quarter_text']; ?></option>
                  	<?php endforeach; ?>
                    </select> 

                  </div>
                 <div class="col-md-2 no-padding no-margin">
                    <button class="btn btn-primary top_filter_button filter_button">Filter</button>
                  </div>
			</div>
			
            </div>

    <?php //if($filtered == 0): ?>
			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<!-- <i class="icon-share font-red-sunglo hide"></i> -->
							<span class="caption-subject font-green-haze bold uppercase">County KEMSA Issues (Screening)</span>
							<!-- <span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span> -->
							<span class="caption-helper"><?php echo $title_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table c_datatable display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>#</th>
										<th>County</th>
										<th>Issued from KEMSA</th>
										<th>Date Issued</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										// echo "<pre>";print_r($county_data_s);exit;
										foreach ($issue_county_data_s as $key => $value) { 
											// echo "<pre>";print_r($value);exit;
											$kemsa_issued = $value['qty_issued'];
											$kemsa_issue_date = $value['issue_date'];
											
											$rec = $value['q_received'];

											$difference = $kemsa_issued - $rec;
											$class = ($difference>0)? " green ":" red ";
											$class = ($difference==0)? "":$class;

											if ($difference != 0):
									?>
									<tr>
										<td></td>
										<td><?php echo $value['county_name']; ?></td>
										<td><?php echo number_format($kemsa_issued) ?></td>
										<td><?php echo date('d F Y',strtotime($kemsa_issue_date)) ?></td>

										<td><button class="btn btn-primary" data-level = "<?php echo $value['level'] ?>" data-level-id = "<?php echo $value['level_id'] ?>" data-commodity-id = "<?php echo $value['commodity_id'] ?>"  data-quarter = "<?php echo $value['quarter'] ?>"  data-year = "<?php echo $value['year'] ?>" data-toggle="modal" data-target=".view-details-modal">View Details</button></td>
									</tr>
									<?php endif; } ?>
								</tbody>	
								<tfoot>
						            <tr>
						                <th colspan="4" style="text-align:right">Total:</th>
						                <th></th>
						            </tr>
						        </tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>	

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-share font-red-sunglo hide"></i>
							<span class="caption-subject font-green-haze bold uppercase">County KEMSA Issues (Confirmatory)</span>
							<!-- <span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span> -->
							<span class="caption-helper"><?php echo $title_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table c_datatable_c display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>#</th>
										<th>County</th>
										<th>Issued from KEMSA</th>
										<th>Date of Issue</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										foreach ($issue_county_data_c as $key => $value) { 
											$kemsa_issued = $value['qty_issued'];
											$kemsa_issue_date = $value['issue_date'];
											$rec = $value['q_received'];

											$difference = $kemsa_issued - $rec;
											$class = ($difference>0)? " green ":" red ";
											$class = ($difference==0)? "":$class;

											if ($difference != 0):
									?>
									<tr>
										<td></td>
										<td><?php echo $value['county_name']; ?></td>
										<td><?php echo number_format($kemsa_issued) ?></td>
										<td><?php echo date('d F Y',strtotime($kemsa_issue_date)) ?></td>

										<td><button class="btn btn-primary" data-level = "<?php echo $value['level'] ?>" data-level-id = "<?php echo $value['level_id'] ?>" data-commodity-id = "<?php echo $value['commodity_id'] ?>"  data-quarter = "<?php echo $value['quarter'] ?>"  data-year = "<?php echo $value['year'] ?>" data-toggle="modal" data-target=".view-details-modal">View Details</button></td>
									</tr>
									<?php endif;} ?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>	

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<!-- <i class="icon-share font-red-sunglo hide"></i> -->
							<span class="caption-subject font-green-haze bold uppercase">Sub-County KEMSA Issues (Screening)</span>
							<!-- <span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span> -->
							<span class="caption-helper"><?php echo $title_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table s_datatable display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>#</th>
										<th>Sub-County</th>
										<th>County</th>
										<th>Issued from KEMSA</th>
										<th>Date of Issue</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										foreach ($issue_district_data_s as $key => $value) { 
											$kemsa_issued = $value['qty_issued'];
											$kemsa_issue_date = $value['issue_date'];
											$rec = $value['q_received'];

											$difference = $kemsa_issued - $rec;
											$class = ($difference>0)? " green ":" red ";
											$class = ($difference==0)? "":$class;

											if ($difference != 0):
									?>
									<tr>
										<td></td>
										<td><?php echo $value['district_name']; ?></td>
										<td><?php echo $value['county_name']; ?></td>
										<td><?php echo number_format($kemsa_issued) ?></td>
										<td><?php echo date('d F Y',strtotime($kemsa_issue_date)) ?></td>
										<td><button class="btn btn-primary" data-level = "<?php echo $value['level'] ?>" data-level-id = "<?php echo $value['level_id'] ?>" data-commodity-id = "<?php echo $value['commodity_id'] ?>"  data-quarter = "<?php echo $value['quarter'] ?>"  data-year = "<?php echo $value['year'] ?>" data-toggle="modal" data-target=".view-details-modal">View Details</button></td>
									</tr>
									<?php endif;} ?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>	

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-share font-red-sunglo hide"></i>
							<span class="caption-subject font-green-haze bold uppercase">Sub-County KEMSA Issues (Confirmatory)</span>
							<!-- <span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span> -->
							<span class="caption-helper"><?php echo $title_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table s_datatable_c display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>#</th>
										<th>Sub-County</th>
										<th>County</th>
										<th>Issued from KEMSA</th>
										<th>Date of Issue</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										foreach ($issue_district_data_c as $key => $value) { 
											$kemsa_issued = $value['qty_issued'];
											$kemsa_issue_date = $value['issue_date'];
											$rec = $value['q_received'];

											$difference = $kemsa_issued - $rec;
											$class = ($difference>0)? " green ":" red ";
											$class = ($difference==0)? "":$class;

											if ($difference != 0):
									?>
									<tr>
										<td></td>
										<td><?php echo $value['district_name']; ?></td>
										<td><?php echo $value['county_name']; ?></td>
										<td><?php echo number_format($kemsa_issued) ?></td>
										<td><?php echo date('d F Y',strtotime($kemsa_issue_date)) ?></td>
										<td><button class="btn btn-primary" data-level = "<?php echo $value['level'] ?>" data-level-id = "<?php echo $value['level_id'] ?>" data-commodity-id = "<?php echo $value['commodity_id'] ?>"  data-quarter = "<?php echo $value['quarter'] ?>"  data-year = "<?php echo $value['year'] ?>" data-toggle="modal" data-target=".view-details-modal">View Details</button></td>
									</tr>
									<?php endif; } ?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>

			<!-- <div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-share font-red-sunglo hide"></i>
							<span class="caption-subject font-green-haze bold uppercase">Facility KEMSA Issues (Screening)</span>
							<span class="caption-helper"><?php echo $title_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table f_datatable display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>#</th>
										<th>Facility</th>
										<th>MFL Code</th>
										<th>Sub-County</th>
										<th>County</th>
										<th>Issued from KEMSA</th>
										<th>Date of Issue</th>
										<th>Delivery Note</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										foreach ($issue_facility_data_s as $key => $value) { 
											$kemsa_issued = $value['qty_issued'];
											$kemsa_issue_date = $value['issue_date'];
											
											$rec = $value['q_received'];

											$difference = $kemsa_issued - $rec;
											$class = ($difference>0)? " green ":" red ";
											$class = ($difference==0)? "":$class;

											if ($difference != 0):
									?>
									<tr>
										<td></td>
										<td><?php echo $value['facility_name']; ?></td>
										<td><?php echo $value['facility_code']; ?></td>
										<td><?php echo $value['district_name']; ?></td>
										<td><?php echo $value['county_name']; ?></td>
										<td><?php echo number_format($kemsa_issued) ?></td>
										<td><?php echo date('d F Y',strtotime($kemsa_issue_date)) ?></td>
										<td><?php echo $value['delivery_note']; ?></td>
									</tr>
									<?php endif; } ?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>	

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-share font-red-sunglo hide"></i>
							<span class="caption-subject font-green-haze bold uppercase">Facility KEMSA Issues (Confirmatory)</span>
							<span class="caption-helper"><?php echo $title_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table f_datatable_c display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>#</th>
										<th>Facility</th>
										<th>MFL Code</th>
										<th>Sub-County</th>
										<th>County</th>
										<th>Issued from KEMSA</th>
										<th>Date of Issue</th>
										<th>Delivery Note</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										foreach ($issue_facility_data_c as $key => $value) { 
											$kemsa_issued = $value['qty_issued'];
											$kemsa_issue_date = $value['issue_date'];
											$rec = $value['q_received'];

											$difference = $kemsa_issued - $rec;
											$class = ($difference>0)? " green ":" red ";
											$class = ($difference==0)? "":$class;

											if ($difference != 0):
									?>
									<tr>
										<td></td>
										<td><?php echo $value['facility_name']; ?></td>
										<td><?php echo $value['facility_code']; ?></td>
										<td><?php echo $value['district_name']; ?></td>
										<td><?php echo $value['county_name']; ?></td>
										<td><?php echo number_format($kemsa_issued) ?></td>
										<td><?php echo date('d F Y',strtotime($kemsa_issue_date)) ?></td>
										<td><?php echo $value['delivery_note']; ?></td>
									</tr>
									<?php endif;} ?>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>
			</div> -->	 		
	<?php //else: ?>

	<?php //endif; ?>

		</div>
	</div>
</div>


<div class="modal fade view-details-modal" id="view-details-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
        <h3 class="modal-title" id="myLargeModalLabel">KEMSA Issue Details</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>

      <div class="modal-body" >
      	<div id="modal-body-data">
        	<!-- <i class="fa fa-cog fa-spin"></i> -->
      	</div>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div> -->
  </div>
</div>


<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 <?php echo date('Y'); ?> &copy; RTK.
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
<?php $this->load->view('v2/dashboard/dashboard_footer'); ?>
</html>

<script>
	
</script>

<script>
var url ='<?php echo base_url()?>';
		// alert("I work");

$(function () { 
    // $('.datatable').DataTable();
    var a = $('.c_datatable').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    a.on( 'order.dt search.dt', function () {
        a.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();


    var b = $('.c_datatable_c').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    b.on( 'order.dt search.dt', function () {
        b.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    var c = $('.s_datatable').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    c.on( 'order.dt search.dt', function () {
        c.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    var d = $('.s_datatable_c').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    d.on( 'order.dt search.dt', function () {
        d.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    //facility
    var e = $('.f_datatable').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    e.on( 'order.dt search.dt', function () {
        e.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    var f = $('.f_datatable_c').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    f.on( 'order.dt search.dt', function () {
        f.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    var g = $('.fi_datatable').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    g.on( 'order.dt search.dt', function () {
        g.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    var h = $('.fi_datatable_c').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    h.on( 'order.dt search.dt', function () {
        h.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    $('#view-details-modal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var level = button.data('level') // Extract info from data-* attributes
		var level_id = button.data('level-id') // Extract info from data-* attributes
		var commodity_id = button.data('commodity-id') // Extract info from data-* attributes
		var quarter = button.data('quarter') // Extract info from data-* attributes
		var year = button.data('year') // Extract info from data-* attributes

		if (quarter == "") {quarter = 0}
		if (year == "") {year = 0}

		console.log('dashboardv2/get_kemsa_issue_details/'+level+'/'+level_id+'/'+commodity_id+"/"+quarter+"/"+year);
		// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		var modal = $(this)
		// modal.find('.modal-title').text('New message to ' + recipient)
		// modal.find('.modal-body').html(level + " "+ level_id);
		ajax_request_replace_div_content('dashboardv2/get_kemsa_issue_details/'+level+'/'+level_id+'/'+commodity_id+"/"+quarter+"/"+year,"#modal-body-data");

	})

	<?php if($filtered > 0): ?>
	<?php echo $utilization_graph_scr; ?>;
	<?php echo $utilization_graph_conf; ?>;

	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/'+subcounty_id+'/NULL/4/NULL/NULL/scr_consumption_chart_filtered',"#scr_consumption_chart_filtered"); 

	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/'+subcounty_id+'/NULL/5/NULL/NULL/conf_consumption_chart_filtered',"#conf_consumption_chart_filtered"); 

    <?php endif; ?>
	<?php //echo $utilization_bgg_screening_graph; ?>;
	<?php //echo $utilization_bgg_confirmatory_graph; ?>;
	<?php //echo $test_graph; ?>;
	<?php //echo $drawing_rights_graph; ?>;
	<?php //echo $drawing_utilization_graph; ?>;

	$(".select2").select2({
          placeholder: "0",
          // containerCssClass: "float-left" 
        });

	/*START OF INHERIT*/
	$('.top_filter_button').button().click(function(x){
          x.preventDefault(); 

          // var county_text = $('#county_filter option:selected').text();
          // var subcounty_text = $('#sub_county_filter option:selected').text();

          // var county_id = $('#county_filter option:selected').val();
          // var subcounty_id = $('#sub_county_filter option:selected').val();
          // var facility_id = $('#facility_id option:selected').val();
          var quarter = $('#quarter_filter option:selected').val();
          var year = $('#year_filter option:selected').val();
          // console.log(quarter);
          if(quarter==0 && year ==0){
            window.location.replace(url+"dashboardv2/kemsa_issues_dashboard");
          }else{
            	window.location.replace(url+"dashboardv2/kemsa_issues_dashboard/NULL/NULL/"+quarter+'/NULL/'+year);
          }
          // alert(county_id);
          // alert(subcounty_id);
       });

});/*END OF JQUERY FUNCTION*/

var county_id = <?php echo json_encode($county_id); ?>;
var subcounty_id = <?php echo json_encode($subcounty_id); ?>;
// console.log(subcounty_id);
if (subcounty_id > 0) { 
		ajax_request_replace_div_content('dashboardv2/get_national_trend/NULL/'+subcounty_id,"#trend-chart"); 
		ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/subcounty/NULL/'+subcounty_id+'/NULL/4/NULL/NULL/scr_consumption_chart',"#scr_consumption_chart"); 
		ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/subcounty/NULL/'+subcounty_id+'/NULL/5/NULL/NULL/conf_consumption_chart',"#conf_consumption_chart"); 
}else{
	// ajax_request_replace_div_content('dashboardv2/get_national_trend/',"#trend-chart"); 
	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/subcounty/NULL/NULL/NULL/4/NULL/NULL/scr_consumption_chart',"#scr_consumption_chart"); 
	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/subcounty/NULL/NULL/NULL/5/NULL/NULL/conf_consumption_chart',"#conf_consumption_chart"); 
}

function ajax_request_replace_div_content(function_url,div){
	var function_url =url+function_url;
	var loading_icon=url+"assets/img/loader2.gif";
	$.ajax({
		type: "POST",
		url: function_url,
		beforeSend: function() {
			$(div).html("<img style='margin-left:20%;' src="+loading_icon+">");
		},
		success: function(msg) {
			// console.log(msg);
			$(div).html(msg);
			$('#datatable').DataTable(); 
		}
	});
} 

</script>