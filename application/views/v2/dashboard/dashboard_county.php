<?php $this->load->view('v2/dashboard/dashboard_header'); ?>
<?php //echo "<pre>";print_r($county_id);exit; ?>
<?php //echo $test_graph;exit; ?>


<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed">
<?php $this->load->view('v2/dashboard/dashboard_top_header'); ?>
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->

<div class="page-container">
<?php $this->load->view('v2/dashboard/dashboard_sidebar'); ?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<h3 class="page-title no-margin">
			County Dashboard 
			<!-- <small> data as at <?php //echo date('F/Y') ?> </small> -->
			<small> data as of <?php echo date('F Y', strtotime("-1 MONTH")) ?> reports. </small>
			</h3></br>
			<!-- END PAGE HEADER-->

			<div class="clearfix"></div>
			<div class="row portlet light no-margin-bottom">
			<div class="portlet-body margin-sm-hor clearfix">

				<?php if ($quartered > 0) { ?>
					<div class="col-md-12 no-padding clearfix">
						<strong><p class="no-margin" style="line-height: 1.5em"> <span class="font-blue-steel">*</span> Fields with an asterisk do not change when filtered quarterly.</p></strong>
					</div>
				<?php } ?>

				<div class="col-md-3 padding-hor-sm no-margin no-padding">
				<select class="form-control select2 select2-county-search padding-hor-sm" id="county_filter">
				<option value="0">Select County</option>
				<?php foreach ($county_data as $county => $value):?> 
				  <option value="<?php echo $value['id']; ?>"><?php echo $value['county']; ?></option>
				<?php endforeach; ?>
				</select>
				</div>

				<div class="col-md-2 padding-hor-sm no-margin no-padding">
				  <select class="form-control select2 padding-hor-sm" id="year_filter">
				  <option value="0">Select Year</option>
				  <?php foreach ($filter_years as $year => $value):?> 
				      <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
				  <?php endforeach; ?>
				  </select>
				</div>

				<div class="col-md-2 padding-hor-sm no-margin no-padding">
				<select class="form-control select2 padding-hor-sm" id="quarter_filter">
				    <option value="0">Select Quarter</option>
				    <option value="1">Quarter 1</option>
				    <option value="2">Quarter 2</option>
				    <option value="3">Quarter 3</option>
				    <option value="4">Quarter 4</option>
				</select> 

				</div>

				<div class="col-md-2 padding-hor-sm no-margin no-padding">
				  <select class="form-control select2 padding-hor-sm" id="month_filter">
				  <option value="0">Select Month</option>
				  <?php foreach ($filter_months as $month => $value):?> 
				      <option value="<?php echo $value['month_year']; ?>"><?php echo $value['month_year_full']; ?></option>
				  <?php endforeach; ?>
				  </select>
				</div>

				<div class="col-md-3 no-padding no-margin">
				<button class="btn btn-primary top_filter_button filter_button">Filter</button>
				</div>
			</div>
			
            </div>
        <?php if($filtered == 0): ?>
            <div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Screening Consumption Trend</span>
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->

								<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<!-- <div id="site_activities_loading">
								<img src="<?php echo base_url().'assets/v2/dashboard/'; ?>admin/layout/img/loading.gif" alt="loading"/>
							</div> -->
							<div id="site_activities_content" class="">
								<div id="scr_consumption_chart" style="height:450px;"></div>
							</div>
						</div>
					</div>
			</div>
			</div>
		
			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Confirmatory Consumption Trend</span>
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->

								<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<!-- <div id="site_activities_loading">
								<img src="<?php echo base_url().'assets/v2/dashboard/'; ?>admin/layout/img/loading.gif" alt="loading"/>
							</div> -->
							<div id="site_activities_content" class="">
								<div id="conf_consumption_chart" style="height:450px;"></div>
							</div>
						</div>
					</div>
			</div>
			</div>

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
					<div class="portlet light clearfix">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Screening Summaries</span>
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->

								<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="col-md-12 no-spacing" style="">
								<table class="table datatable display cell-border compact" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th rowspan="2">#</th>
											<th rowspan="2">County</th>
											<th rowspan="2">Allocated</th>
											<th rowspan="2">Issued from KEMSA</th>
											<th rowspan="2">Beginning Balance</th>
											<th rowspan="2">Quantity Received</th>
											<th rowspan="2">Quantity Used</th>
											<th rowspan="2">Tests Done</th>
											<th rowspan="2">Losses</th>
											<th colspan="2">Adjustments</th>
											<!-- <th rowspan="2">Adjustments(-)</th> -->
											<th rowspan="2">Closing Balance</th>
										</tr>
										<tr>
											<th>Positive</th>
											<th>Negative</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($counties_drawing_data_s as $key => $value) { ?>
										<tr>
											<td></td>
											<!-- <td><?php //echo $value['county_name']; ?></td> -->
											<td><a style="text-decoration: none!important;color:black;" href="<?php echo base_url().'dashboardv2/county_dashboard/'.$value['county_id']; ?>" target="_blank"><?php echo $value['county_name']; ?></a></td>


											<td><?php echo number_format($value['allocated']) ?></td>
											<td><?php echo number_format($value['issued_kemsa']) ?></td>
											<td><?php echo number_format($value['beg_bal']) ?></td>
											<td><?php echo number_format($value['q_rec']) ?></td>
											<td><?php echo number_format($value['q_used']) ?></td>
											<td><?php echo number_format($value['tests_done']) ?></td>
											<td><?php echo number_format($value['losses']) ?></td>
											<td><?php echo number_format($value['positive_adj']) ?></td>
											<td><?php echo number_format($value['negative_adj']) ?></td>
											<td><?php echo number_format($value['closing_bal']) ?></td>
										</tr>
										<?php } ?>
									</tbody>	
								</table>
							</div>
						</div>
					</div>
			</div>
			</div>	

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
					<div class="portlet light clearfix">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Confirmatory Summaries</span>
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->

								<span class="caption-helper"><?php echo $title_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="col-md-12 no-spacing" style="">
								<table class="table datatable-c display cell-border compact" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th rowspan="2">#</th>
											<th rowspan="2">County</th>
											<th rowspan="2">Allocated</th>
											<th rowspan="2">Issued from KEMSA</th>
											<th rowspan="2">Beginning Balance</th>
											<th rowspan="2">Quantity Received</th>
											<th rowspan="2">Quantity Used</th>
											<th rowspan="2">Tests Done</th>
											<th rowspan="2">Losses</th>
											<th colspan="2">Adjustments</th>
											<!-- <th rowspan="2">Adjustments(-)</th> -->
											<th rowspan="2">Closing Balance</th>
										</tr>
										<tr>
											<th>Positive</th>
											<th>Negative</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($counties_drawing_data_c as $key => $value) { ?>
										<tr>
											<td></td>
											<td><a style="text-decoration: none!important;color:black;" href="<?php echo base_url().'dashboardv2/county_dashboard/'.$value['county_id']; ?>" target="_blank"><?php echo $value['county_name']; ?></a></td>
											<td><?php echo number_format($value['allocated']) ?></td>
											<td><?php echo number_format($value['issued_kemsa']) ?></td>
											<td><?php echo number_format($value['beg_bal']) ?></td>
											<td><?php echo number_format($value['q_rec']) ?></td>
											<td><?php echo number_format($value['q_used']) ?></td>
											<td><?php echo number_format($value['tests_done']) ?></td>
											<td><?php echo number_format($value['losses']) ?></td>
											<td><?php echo number_format($value['positive_adj']) ?></td>
											<td><?php echo number_format($value['negative_adj']) ?></td>
											<td><?php echo number_format($value['closing_bal']) ?></td>
										</tr>
										<?php } ?>
									</tbody>	
								</table>
							</div>
						</div>
					</div>
			</div>
			</div>		
		<?php else: ?>
			
			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-6 no-padding">
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<!-- <span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Screening Consumption Trend</span> -->
								<span class="caption-subject font-green-haze bold uppercase">Screening Consumption Trend</span>
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->

								<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<!-- <div id="site_activities_loading">
								<img src="<?php echo base_url().'assets/v2/dashboard/'; ?>admin/layout/img/loading.gif" alt="loading"/>
							</div> -->
							<div id="site_activities_content" class="">
								<div id="scr_consumption_chart" style="height:450px;"></div>
							</div>
						</div>
					</div>
			</div>

			<div class="col-md-6 no-padding">
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<!-- <span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Confirmatory Consumption Trend</span> -->
								<span class="caption-subject font-green-haze bold uppercase">Confirmatory Consumption Trend</span>
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->

								<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<!-- <div id="site_activities_loading">
								<img src="<?php echo base_url().'assets/v2/dashboard/'; ?>admin/layout/img/loading.gif" alt="loading"/>
							</div> -->
							<div id="site_activities_content" class="">
								<div id="conf_consumption_chart" style="height:450px;"></div>
							</div>
						</div>
					</div>
			</div>
			</div>


			<div class="clearfix"></div>
			<div class="row portlet light">
			<div class="col-md-6 col-sm-6 no-spacing">
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase"><?php echo $title_prepend; ?> Reporting Rates Trend</span>
								<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>

							</div>
						</div>
						<div class="portlet-body">
							<div id="site_activities_content" class="">
								<div id="trend-chart" style="height:300px;"></div>
							</div>
						</div>
					</div>
			</div>
			<div class="col-md-6 col-sm-6 no-margin no-padding">
			
			<div class="portlet light ">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-share font-red-sunglo hide"></i>
						<span class="caption-subject font-green-haze bold uppercase">Allocated vs Received vs Consumed </span>
						<span class="caption-helper"><?php echo $title_append; ?></span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="col-md-12 no-padding">
						<div class="col-md-6 no-padding">
							<div id="site_activities_content" class="">
								<div id="utilization_graph_scr" style="height: 300px;"></div>
							</div>
						</div>
						<div class="col-md-6 no-padding">
							<div id="site_activities_content" class="">
								<div id="utilization_graph_conf" style="height: 300px;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>

			</div>
			</div>
			 
			<div class="clearfix"></div>
			<div class="row portlet light">
			<div class="col-md-6">
				<div class="portlet light no-padding">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-share font-red-sunglo hide"></i>
							<span class="caption-subject font-blue-steel bold uppercase"><?php echo $title_prepend; ?> Stock Card </span>
								<span class="caption-helper"> <?php echo $title_append ?> </span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-scrollable">
							<table class="table table-bordered table-hover">
							<thead>
								<th></th>
								<th>Screening</th>
								<th>Confirmatory</th>
							</thead>
							
							<tbody>
							
							<tr>
								<td>
									 <strong>Tests Done (Reported on FCDRRs)</strong>
								</td>
								<td>
									 <?php echo number_format($screening_tests_done); ?>
								</td>
								<td>
									 <?php echo number_format($confirmatory_tests_done); ?>
								</td>
							</tr>

							<tr>
								<td>
									 <strong>Tests Done (Retrieved from DHIS)</strong>
								</td>
								<td>
									 <?php echo number_format($dhis_screening_tests_done); ?>
								</td>
								<td>
									 <?php echo number_format($dhis_confirmatory_tests_done); ?>
								</td>
							</tr>

							<tr>
								<td>
									 <strong>Expiries</strong>
								</td>
								<td>
									 <?php echo number_format($screening_expiring); ?>
								</td>
								<td>
									 <?php echo number_format($confirmatory_expiring); ?>
								</td>
							</tr>

							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6">
			<div class="portlet light no-padding">
				<div class="portlet-title">
					<div class="caption">
						<span class="caption-subject font-blue-steel bold uppercase"><?php echo $title_prepend; ?> Summary </span>
							<span class="caption-helper"><strong><?php echo $title_append; ?></strong></span>
					</div>
				</div>
				<div class="portlet-body">
			 
				<?php
					if ($county_id > 0) {
						$t = "Total allocated by county";
					}else{
						$t = "Total allocated by counties";
					}

					if ($quartered > 0) {
						$a = "Quarter balance";
					}else{
						$a = "Available balance for next allocation";
					}					
				?>
				
					<div class="table-scrollable">
						<table class="table table-bordered table-hover">
						<thead>
							<th></th>
							<th>Screening</th>
							<th>Confirmatory</th>
						</thead>
						
						<tbody>
						
						<tr>
							<td>
								 <strong>Total drawing rights <small>(<?php echo "Quarters: ".$drawing_quarters." Year: ".date('Y'); ?>)</small> </strong>
							</td>
							<td>
								<?php echo number_format($drawing_total_screening); ?>
							</td>
							<td>
								<?php echo number_format($drawing_total_confirmatory); ?>
							</td>
						</tr>

						<?php if ($filtered_month < 1) {
							echo '
								<tr>
									<td>
										 <strong>'. $t.' <small>(Year: '.date('Y').')</small></strong>
									</td>
									<td>'.number_format($total_allocated_screening).'</td>
									<td>'.number_format($total_allocated_confirmatory).'</td>
								</tr>
							';
						} ?>

						<tr>
							<td class="indent">
								 <strong>- Issued from KEMSA   <small>(<?php echo $kemsa_issued_data_date; ?>)</small></strong>
							</td>
							<td>
								<?php echo number_format($kemsa_issued_screening); ?>
							</td>
							<td>
								<?php echo number_format($kemsa_issued_confirmatory); ?>
							</td>
						</tr>

						<tr>
							<td class="indent">
								 <strong>- Received at site <small>(<?php echo $summary_data_date; ?>)</small></strong>
							</td>
							<td>
									 <?php echo number_format($received_site_screening); ?>
							</td>
							<td>
									 <?php echo number_format($received_site_confirmatory); ?>
							</td>
						</tr>

						<tr>
							<td class="indent">
								 <strong>- Consumed at site <small>(<?php echo $summary_data_date; ?>)</small></strong>
							</td>
							<td>
									 <?php echo number_format($consumed_site_screening); ?>
							</td>
							<td>
									 <?php echo number_format($consumed_site_confirmatory); ?>
							</td>
						</tr>

						<tr>
							<td class="indent">
								 <strong>- Positive Adjustments <small>(<?php echo $summary_data_date; ?>)</small></strong>
							</td>
							<td>
									 <?php echo number_format($positive_adj_screening) ?>
							</td>
							<td>
									 <?php echo number_format($positive_adj_confirmatory) ?>
							</td>
						</tr>

						<tr>
							<td class="indent">
								 <strong>- Negative Adjustments <small>(<?php echo $summary_data_date; ?>)</small></strong>
							</td>
							<td>
									 <?php echo number_format($negative_adj_screening); ?>
							</td>
							<td>
									 <?php echo number_format($negative_adj_confirmatory); ?>
							</td>
						</tr>

						<tr>
							<td class="indent">
								 <strong>- Losses <small>(<?php echo $summary_data_date; ?>)</small></strong>
							</td>
							<td>
									 <?php echo number_format($losses_screening); ?>
							</td>
							<td>
									 <?php echo number_format($losses_confirmatory); ?>
							</td>
						</tr>

						<tr>
							<td class="indent">
								 <strong>- Beginning balance <small>(<?php echo "Month: ".date('F Y',strtotime($beginning_balance_order_date)) ?>)</small> </strong>
							</td>
							<td>
								<?php echo number_format($beginning_balance_screening); ?>
							</td>
							<td>
								<?php echo number_format($beginning_balance_confirmatory); ?>
							</td>
						</tr>

						<tr>
							<td class="indent">
								 <strong>- Closing balance <span class=""><small>(<?php echo  "Month: ".date('F Y',strtotime($closing_balance_order_date)) ?>)</small></span> </strong>
							</td>
							<td>
								<?php echo number_format($closing_balance_screening); ?>
							</td>
							<td>
								<?php echo number_format($closing_balance_confirmatory); ?>
							</td>
						</tr>

						<?php if ($filtered_month < 1) {
							echo '
								<tr>
									<td>
										 <strong>'. $a.'<small>(Year: '.date('Y').')</small></strong>
									</td>
									<td>'.number_format($drawing_balance_screening).'</td>
									<td>'.number_format($drawing_balance_confirmatory).'</td>
								</tr>
							';
						} ?>

						<tr>
								<td>
									 <strong> Total Balance Discrepancy  <small>(<?php echo $summary_data_date; ?>)</small></strong>
								</td>
								<td>
									<?php 

										$beg_bal_s = $beginning_balance_screening;
										$clos_bal_s = $closing_balance_screening;
										$rec_s = $received_site_screening;
										$pos_adj_s = $positive_adj_screening;
										$neg_adj_s = $negative_adj_screening;
										$used_s = $quantity_used_screening;
										$losses_s = $losses_screening;
										$reported_s = $closing_balance_screening;

										$expected_s = ($beg_bal_s + $rec_s + $pos_adj_s)-($used_s + $losses_s + $neg_adj_s); 
										// echo "<pre>";print_r($expected_s);exit;
										$difference_s = $reported_s - $expected_s;

										$class_s = ($difference_s>0)? " green ":" red ";
										$class_s = ($difference_s==0)? "":$class_s;

										 echo number_format($difference_s); 
									?>
								</td>
								<td>
									<?php 

										$beg_bal_c = $beginning_balance_confirmatory;
										$clos_bal_c = $closing_balance_confirmatory;
										$rec_c = $received_site_confirmatory;
										$pos_adj_c = $positive_adj_confirmatory;
										$neg_adj_c = $negative_adj_confirmatory;
										$used_c = $quantity_used_confirmatory;
										$losses_c = $losses_confirmatory;
										$reported_c = $closing_balance_confirmatory;

										$expected_c = ($beg_bal_c + $rec_c + $pos_adj_c)-($used_c + $losses_c + $neg_adj_c); 

										$difference_c = $reported_c - $expected_c;

										$class_c = ($difference_c>0)? " green ":" red ";
										$class_c = ($difference_c==0)? "":$class_c;

										 echo number_format($difference_c); 

									?>
								</td>
							</tr>

						</tbody>
						</table>
					</div>
				</div>
			</div>
			</div>
				
			</div>	

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
					<div class="portlet light clearfix">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase">Sub-County Screening Summaries</span>
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->

								<span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="col-md-12 no-spacing" style="">
								<table class="table datatable display cell-border compact" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th rowspan="2">#</th>
											<th rowspan="2">Sub-County</th>
											<th rowspan="2">County</th>
											<th rowspan="2">Allocated</th>
											<!-- <th rowspan="2">Issued from KEMSA</th> -->
											<th rowspan="2">Beginning Balance</th>
											<th rowspan="2">Quantity Received</th>
											<th rowspan="2">Quantity Used</th>
											<th rowspan="2">Tests Done</th>
											<th rowspan="2">Losses</th>
											<th colspan="2">Adjustments</th>
											<!-- <th rowspan="2">Adjustments(-)</th> -->
											<th rowspan="2">Closing Balance</th>
										</tr>
										<tr>
											<th>Positive</th>
											<th>Negative</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($district_drawing_data_s as $key => $value) { ?>
										<tr>
											<td></td>
											<td><a style="text-decoration: none!important;color:black;" href="<?php echo base_url().'dashboardv2/subcounty_dashboard/NULL/'.$value['district_id']; ?>" target="_blank"><?php echo $value['district_name']; ?></a></td>
											<td><?php echo $value['county_name']; ?></td>
											<td><?php echo number_format($value['allocated']) ?></td>
											<!-- <td><?php echo number_format($value['issued_kemsa']) ?></td> -->
											<td><?php echo number_format($value['beg_bal']) ?></td>
											<td><?php echo number_format($value['q_rec']) ?></td>
											<td><?php echo number_format($value['q_used']) ?></td>
											<td><?php echo number_format($value['tests_done']) ?></td>
											<td><?php echo number_format($value['losses']) ?></td>
											<td><?php echo number_format($value['positive_adj']) ?></td>
											<td><?php echo number_format($value['negative_adj']) ?></td>
											<td><?php echo number_format($value['closing_bal']) ?></td>
										</tr>
										<?php } ?>
									</tbody>	
								</table>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
			</div>
			</div>	

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
			
					<div class="portlet light clearfix">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase">Sub-County Confirmatory Summaries</span>
								<!-- <span class="caption-helper"><?php echo $title_append; ?></span> -->

								<span class="caption-helper"><?php echo $title_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="col-md-12 no-spacing" style="">
								<table class="table datatable-c display cell-border compact" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th rowspan="2">#</th>
											<th rowspan="2">Sub-County</th>
											<th rowspan="2">County</th>
											<th rowspan="2">Allocated</th>
											<!-- <th rowspan="2">Issued from KEMSA</th> -->
											<th rowspan="2">Beginning Balance</th>
											<th rowspan="2">Quantity Received</th>
											<th rowspan="2">Quantity Used</th>
											<th rowspan="2">Tests Done</th>
											<th rowspan="2">Losses</th>
											<th colspan="2">Adjustments</th>
											<!-- <th rowspan="2">Adjustments(-)</th> -->
											<th rowspan="2">Closing Balance</th>
										</tr>
										<tr>
											<th>Positive</th>
											<th>Negative</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($district_drawing_data_c as $key => $value) { ?>
										<tr>
											<td></td>
											<td><a style="text-decoration: none!important;color:black;" href="<?php echo base_url().'dashboardv2/subcounty_dashboard/NULL/'.$value['district_id']; ?>" target="_blank"><?php echo $value['district_name']; ?></a></td>
											<td><?php echo $value['county_name']; ?></td>
											<td><?php echo number_format($value['allocated']) ?></td>
											<!-- <td><?php echo number_format($value['issued_kemsa']) ?></td> -->
											<td><?php echo number_format($value['beg_bal']) ?></td>
											<td><?php echo number_format($value['q_rec']) ?></td>
											<td><?php echo number_format($value['q_used']) ?></td>
											<td><?php echo number_format($value['tests_done']) ?></td>
											<td><?php echo number_format($value['losses']) ?></td>
											<td><?php echo number_format($value['positive_adj']) ?></td>
											<td><?php echo number_format($value['negative_adj']) ?></td>
											<td><?php echo number_format($value['closing_bal']) ?></td>
										</tr>
										<?php } ?>
									</tbody>	
								</table>
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
			</div>
			</div>

			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-12 col-sm-12 no-padding">
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase">Subcounty Consumption (Screening). </span>
								</br>
								<span class="caption-helper"> Quarters: <?php echo $drawing_rights_quarter." ".$title_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<div id="site_activities_content" class="">
								<div id="scr_consumption_chart_filtered" style="height: 550px;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-12 col-sm-12 no-padding">
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-green-haze bold uppercase">Subcounty Consumption (Confirmatory). </span>
								</br>
								<span class="caption-helper"> Quarters: <?php echo $drawing_rights_quarter." ".$title_append; ?></span>
								<span class="caption-helper"><?php //echo $title_append; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<div id="site_activities_content" class="">
								<div id="conf_consumption_chart_filtered" style="height: 550px;"></div>
							</div>
						</div>
					</div>
				</div>
			</div> 

		<?php endif; ?>
		</div>
	</div>
</div>

<div class="page-footer">
	<div class="page-footer-inner pull-right" style="color:white;">
		 RTK&copy;<?php echo date('Y'); ?>
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
<?php $this->load->view('v2/dashboard/dashboard_footer'); ?>
</html>

<script>
	
</script>

<script>
var url ='<?php echo base_url()?>';
		// alert("I work");
var county_id = <?php echo json_encode($county_id); ?>;
var subcounty_id = <?php echo json_encode($subcounty_id); ?>;
var quarter = <?php echo json_encode($quarter); ?>;
var filtered_month = <?php echo json_encode($filtered_month); ?>;
var filtered_year = <?php echo json_encode($filtered_year); ?>;
if (filtered_month < 1) {filtered_month = "NULL"}
if (filtered_year < 1) {filtered_year = "NULL"}

$(function () { 
    // $('.datatable').DataTable();
    //DATATABLES
		    var t = $('.datatable').DataTable( {
		        "columnDefs": [ {
		            "searchable": false,
		            "orderable": false,
		            "targets": 0
		        } ],
		        "order": [[ 1, 'asc' ]],
		        dom: 'Bfrtip',
		        buttons: [
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdfHtml5'
		        ]
		    } );
		 
		    t.on( 'order.dt search.dt', function () {
		        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		            cell.innerHTML = i+1;
		        } );
		    } ).draw();

		    var s = $('.datatable-c').DataTable( {
		        "columnDefs": [ {
		            "searchable": false,
		            "orderable": false,
		            "targets": 0
		        } ],
		        "order": [[ 1, 'asc' ]],
		        dom: 'Bfrtip',
		        buttons: [
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdfHtml5'
		        ]
		    } );
		 
		    s.on( 'order.dt search.dt', function () {
		        s.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		            cell.innerHTML = i+1;
		        } );
		    } ).draw();
	    
    <?php if($filtered > 0): ?>
	    var county_id = <?php echo json_encode($county_id); ?>;
		<?php echo $utilization_graph_scr; ?>;
		<?php echo $utilization_graph_conf; ?>;

		ajax_request_replace_div_content('dashboardv2/get_consumption_trend_clickable_by/subcounty/'+county_id+'/NULL/NULL/4/'+filtered_month+'/'+quarter+'/scr_consumption_chart_filtered/'+filtered_year+"/1","#scr_consumption_chart_filtered"); 

		ajax_request_replace_div_content('dashboardv2/get_consumption_trend_clickable_by/subcounty/'+county_id+'/NULL/NULL/5/'+filtered_month+'/'+quarter+'/conf_consumption_chart_filtered/'+filtered_year+"/1","#conf_consumption_chart_filtered"); 

    <?php endif; ?>
	<?php //echo $utilization_bgg_screening_graph; ?>;
	<?php //echo $utilization_bgg_confirmatory_graph; ?>;
	<?php //echo $test_graph; ?>;
	<?php //echo $drawing_rights_graph; ?>;
	<?php //echo $drawing_utilization_graph; ?>;

	$(".select2").select2({
          placeholder: "0",
          // containerCssClass: "float-left" 
        });

	/*START OF INHERIT*/
	$('.top_filter_button').button().click(function(x){
          x.preventDefault(); 

          var county_text = $('#county_filter option:selected').text();
          var subcounty_text = $('#sub_county_filter option:selected').text();

          var county_id = $('#county_filter option:selected').val();
          var subcounty_id = $('#sub_county_filter option:selected').val();
          var facility_id = $('#facility_id option:selected').val();
          var quarter = $('#quarter_filter option:selected').val();
          var month_year = $('#month_filter option:selected').val();
          var year = $('#year_filter option:selected').val();

          if(county_id==0){
            window.location.replace(url+"dashboardv2/county_dashboard/NULL/NULL/"+quarter+"/"+month_year+"/"+year);
          }else{
            if(subcounty_id > 0){
            	window.location.replace(url+"dashboardv2/county_dashboard/NULL/"+subcounty_id+"/"+quarter+"/"+month_year+"/"+year);
            }else{
            	window.location.replace(url+"dashboardv2/county_dashboard/"+county_id+"/NULL/"+quarter+"/"+month_year+"/"+year);
            }
          }
          // alert(county_id);
          // alert(subcounty_id);
       });

	$('#county_filter').on('change', function(){
          // console.log("County change");return;
        var county_val=$('#county_filter').val()
        var drop_down='';
        var facility_select = "<?php echo base_url(); ?>reports/get_sub_county_json_data/"+county_val;
        $.getJSON( facility_select ,function( json ) {
         $("#sub_county_filter").html('<option value="NULL" selected="selected">All Sub-Counties</option>');
          $.each(json, function( key, val ) {
            drop_down +="<option value='"+json[key]["id"]+"'>"+json[key]["district"]+"</option>"; 
          });
          $("#sub_county_filter").append(drop_down);
        });
        
    });

	// Subcounty filter
    $('#sub_county_filter').on('change', function(){
        var subcounty_val=$('#sub_county_filter').val();
        var drop_down='';
        if(subcounty_val=="NULL"){
          $("#facility_id").html('<option value="NULL" selected="selected">All Facilities</option>');
        }else{
          var facility_select = "<?php echo base_url(); ?>reports/get_facility_json/"+subcounty_val;
          $.getJSON( facility_select ,function( json ) {
           $("#facility_id").html('<option value="NULL" selected="selected">All Facilities</option>');
            $.each(json, function( key, val ) {
              drop_down +="<option value='"+json[key]["facility_code"]+"'>"+json[key]["facility_name"]+"</option>"; 
            });
            $("#facility_id").append(drop_down);
          });  
        }                
        
      });
	/**/
});/*END OF JQUERY FUNCTION*/

// console.log(quarter);

if (county_id > 0) {
	$('.select2-county-search').val(county_id).trigger('change');  
		ajax_request_replace_div_content('dashboardv2/get_national_trend/'+county_id+'/NULL/NULL/NULL/'+filtered_year,"#trend-chart"); 
		/*ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/county/'+county_id+'/NULL/NULL/4/'+filtered_month+'/'+quarter+'/scr_consumption_chart',"#scr_consumption_chart"); 
		ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/county/'+county_id+'/NULL/NULL/5/NULL/NULL/conf_consumption_chart',"#conf_consumption_chart");*/
		if (quarter > 0) {

			/*ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/county/'+county_id+'/NULL/NULL/4/'+filtered_month+'/'+quarter+'/scr_consumption_chart/'+filtered_year,"#scr_consumption_chart"); 
			ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/county/'+county_id+'/NULL/NULL/5/'+filtered_month+'/'+quarter+'/conf_consumption_chart/'+filtered_year,"#conf_consumption_chart");*/

			ajax_request_replace_div_content('dashboardv2/get_consumption_trend_clickable_by/county/'+county_id+'/NULL/NULL/4/'+filtered_month+'/'+quarter+'/scr_consumption_chart/'+filtered_year,"#scr_consumption_chart"); 
			ajax_request_replace_div_content('dashboardv2/get_consumption_trend_clickable_by/county/'+county_id+'/NULL/NULL/5/'+filtered_month+'/'+quarter+'/conf_consumption_chart/'+filtered_year,"#conf_consumption_chart");


			ajax_request_replace_div_content('dashboardv2/get_national_trend/'+county_id+'/NULL/NULL/NULL/'+filtered_year,"#trend-chart"); 
		}else{
			console.log('dashboardv2/get_consumption_trend_clickable_by/county/'+county_id+'/NULL/NULL/4/'+filtered_month+'/'+quarter+'/scr_consumption_chart/'+filtered_year);
			console.log('dashboardv2/get_consumption_trend_clickable_by/county/'+county_id+'/NULL/NULL/5/'+filtered_month+'/NULL/conf_consumption_chart/'+filtered_year);
			console.log('dashboardv2/get_national_trend/'+county_id+'/NULL/NULL/NULL/'+filtered_year);

			/*ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/county/'+county_id+'/NULL/NULL/4/'+filtered_month+'/'+quarter+'/scr_consumption_chart/'+filtered_year,"#scr_consumption_chart"); 
			ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/county/'+county_id+'/NULL/NULL/5/'+filtered_month+'/NULL/conf_consumption_chart/'+filtered_year,"#conf_consumption_chart"); */

			ajax_request_replace_div_content('dashboardv2/get_consumption_trend_clickable_by/county/'+county_id+'/NULL/NULL/4/'+filtered_month+'/'+quarter+'/scr_consumption_chart/'+filtered_year,"#scr_consumption_chart"); 
			ajax_request_replace_div_content('dashboardv2/get_consumption_trend_clickable_by/county/'+county_id+'/NULL/NULL/5/'+filtered_month+'/NULL/conf_consumption_chart/'+filtered_year,"#conf_consumption_chart"); 

			ajax_request_replace_div_content('dashboardv2/get_national_trend/'+county_id+'/NULL/NULL/NULL/'+filtered_year,"#trend-chart"); 
		}

}else{
	// ajax_request_replace_div_content('dashboardv2/get_national_trend/',"#trend-chart"); 
	if (quarter > 0) {
		/*ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/county/NULL/NULL/NULL/4/'+filtered_month+'/'+quarter+'/scr_consumption_chart/'+filtered_year,"#scr_consumption_chart"); 
		ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/county/NULL/NULL/NULL/5/'+filtered_month+'/'+quarter+'/conf_consumption_chart/'+filtered_year,"#conf_consumption_chart");*/

		ajax_request_replace_div_content('dashboardv2/get_consumption_trend_clickable_by/county/NULL/NULL/NULL/4/'+filtered_month+'/'+quarter+'/scr_consumption_chart/'+filtered_year,"#scr_consumption_chart"); 
		ajax_request_replace_div_content('dashboardv2/get_consumption_trend_clickable_by/county/NULL/NULL/NULL/5/'+filtered_month+'/'+quarter+'/conf_consumption_chart/'+filtered_year,"#conf_consumption_chart");

		ajax_request_replace_div_content('dashboardv2/get_national_trend/'+county_id+'/NULL/NULL/NULL/'+filtered_year,"#trend-chart"); 
	}else{
		/*ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/county/NULL/NULL/NULL/4/'+filtered_month+'/'+quarter+'/scr_consumption_chart/'+filtered_year,"#scr_consumption_chart"); 
		ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/county/NULL/NULL/NULL/5/'+filtered_month+'/NULL/conf_consumption_chart/'+filtered_year,"#conf_consumption_chart");*/ 

		ajax_request_replace_div_content('dashboardv2/get_consumption_trend_clickable_by/county/NULL/NULL/NULL/4/'+filtered_month+'/'+quarter+'/scr_consumption_chart/'+filtered_year,"#scr_consumption_chart"); 
		ajax_request_replace_div_content('dashboardv2/get_consumption_trend_clickable_by/county/NULL/NULL/NULL/5/'+filtered_month+'/NULL/conf_consumption_chart/'+filtered_year,"#conf_consumption_chart");
		ajax_request_replace_div_content('dashboardv2/get_national_trend/'+county_id+'/NULL/NULL/NULL/'+filtered_year,"#trend-chart"); 
	}
}
// ajax_request_replace_div_content('dashboardv2/get_drawing_rights/NULL/screening',"#utilization_bgg_screening_graph"); 
// ajax_request_replace_div_content('dashboardv2/get_drawing_rights/NULL/confirmatory',"#utilization_bgg_confirmatory_graph"); 

function ajax_request_replace_div_content(function_url,div){
	var function_url =url+function_url;
	var loading_icon=url+"assets/img/loader2.gif";
	$.ajax({
		type: "POST",
		url: function_url,
		beforeSend: function() {
			$(div).html("<img style='margin-left:20%;' src="+loading_icon+">");
		},
		success: function(msg) {
			// console.log(msg);
			$(div).html(msg);
		}
	});
} 

</script>