<?php $this->load->view('v2/dashboard/dashboard_header'); ?>
<?php //echo "<pre>";print_r($pods);exit; ?>
<?php //echo $test_graph;exit; ?>

<style>
	.logo{
		 width: 45px;
    	float: left;
	}

	.logo-text{
		margin: 10px 0 0 5px;
		color: white;
	    font-size: 18px;
	    font-weight: bold;
	    text-decoration: none;
    	float: left;
	}
	.no-margin{
		margin:0!important;
	}
	.no-padding{
		padding:0!important;
	}

	.select2{
		float:left!important;
		width:100%!important;
  	}

  	/** Select2 **/
	.select2-container--default .select2-selection--single,
	.select2-container--default .select2-selection--multiple {
	  background-color: #fff;
	  border: 1px solid #ccc;
	  border-radius: 0;
	  min-height: 38px;
	}

	.select2-container--default .select2-selection--single .select2-selection__rendered {
	  color: #73879C;
	  padding-top: 5px;
	}

	.select2-container--default .select2-selection--multiple .select2-selection__rendered {
	  padding-top: 3px;
	}

	.select2-container--default .select2-selection--single .select2-selection__arrow {
	  height: 36px;
	}

	.select2-container--default .select2-selection--multiple .select2-selection__choice,
	.select2-container--default .select2-selection--multiple .select2-selection__clear {
	  margin-top: 2px;
	  border: none;
	  border-radius: 0;
	  padding: 3px 5px;
	}

	.select2-container--default.select2-container--focus .select2-selection--multiple {
	  border: 1px solid #ccc;
	}
	/** /Select2 **/

	.filter_button{
	    padding:9px 22px!important;
	    margin:0;
	    float:left;
	    border-radius:0px;
	    width:100%
	  }
	.padding-sm{
		padding:30px;
	}
	.no-margin-bottom{
		margin-bottom: 0!important;
		padding-bottom: 0!important;
	}
	.indent{
    	text-indent: 15px;
	}
</style>
<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solid">
<?php $this->load->view('v2/dashboard/dashboard_top_header'); ?>
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->

<div class="page-container">
<?php $this->load->view('v2/dashboard/dashboard_sidebar'); ?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<div class="clearfix"></div>
			<div class="row portlet light">

				<div class="col-md-12">
				<!-- <div class="col-md-6"> -->
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-red-sunglo hide"></i>
								<span class="caption-subject font-blue-steel bold uppercase">
								Proof of Delivery (POD) for <?php echo date('Y'); ?> Issues.
								</span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-bordered table-hover">
								<thead>
									<th>County</th>
									<th>File Name</th>
									<th>View</th>
								</thead>
								
								<tbody>
								<?php foreach ($pods as $key => $value){ ?>
									<tr>
										<td>
											 <?php echo $value['county']; ?>
										</td>
										<td>
											<?php echo $value['file_name']; ?>
										</td>
										<td>
											<a href="<?php echo base_url().'dashboardv2/download_pod/'.$value['file_name']; ?>"><strong>Click to view.</strong></a> 
										</td>
									</tr>
								<?php } ?>

								</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
				</div>
			</div>
		
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 <?php echo date('Y'); ?> &copy; RTK.
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
<?php $this->load->view('v2/dashboard/dashboard_footer'); ?>
</html>

<script>
var url ='<?php echo base_url()?>';
</script>