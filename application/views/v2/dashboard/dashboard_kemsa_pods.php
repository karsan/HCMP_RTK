<?php $this->load->view('v2/dashboard/dashboard_header'); ?>
<?php //echo "<pre>";print_r($county_id);exit; ?>
<?php //echo $test_graph;exit; ?>

<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solid  page-sidebar-closed">
	<style>
		.red{
			color:red!important;
		}
		.green{
			color:green!important;
		}
		.btn{
			width: 100%;
		}
	</style>
<?php $this->load->view('v2/dashboard/dashboard_top_header'); ?>
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->

<div class="page-container">
<?php $this->load->view('v2/dashboard/dashboard_sidebar'); ?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<h3 class="page-title no-margin">
			KEMSA POD Dashboard 
			<small> data as of <?php echo date('F Y', strtotime("-1 MONTH")) ?> reports. </small>
			</h3></br>

			<div class="clearfix"></div>
			<div class="row">
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<!-- <i class="icon-share font-red-sunglo hide"></i> -->
							<span class="caption-subject font-green-haze bold uppercase">County PODs</span>
							<!-- <span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span> -->
							<span class="caption-helper"><?php echo $title_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table c_datatable display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>#</th>
										<th>Month</th>
										<th>County/POD Folder</th>
										<th>POD/File Name</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										// echo "<pre>";print_r($pod_data);exit;
										foreach ($pod_data as $month_key => $month_val) {
											$pod_month = $month_key;
											// echo "<pre>";print_r($month_val);exit;

											foreach ($month_val as $county_key => $county_val) {
												// echo "<pre>";print_r($county_key);exit;
												$pod_county = $county_key;
												foreach ($county_val['file_data'] as $file_data_key => $file_data_val) { 
													// echo "<pre>";print_r($file_data_val);exit;
													$func_url = base_url().'dashboardv2/download_pod/'.$file_data_val['file_url_local'];
									?>
									<tr>
										<td></td>
										<td><?php echo $pod_month; ?></td>
										<td><?php echo $pod_county; ?></td>
										<td><?php echo $file_data_val['file_name']; ?></td>

										<td><a class="btn btn-primary btn-sm" href="<?php echo $func_url; ?>">Download</a></td>
									</tr>
									<?php }}} ?>
								</tbody>	
								<tfoot>
						            <tr>
						                <th colspan="4" style="text-align:right">Total:</th>
						                <th></th>
						            </tr>
						        </tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>	

		</div>
	</div>
</div>

<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 <?php echo date('Y'); ?> &copy; RTK.
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
<?php $this->load->view('v2/dashboard/dashboard_footer'); ?>
</html>

<script>
	
</script>

<script>
var url ='<?php echo base_url()?>';
		// alert("I work");

$(function () { 
    // $('.datatable').DataTable();
    var a = $('.c_datatable').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    a.on( 'order.dt search.dt', function () {
        a.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();


    var b = $('.c_datatable_c').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    b.on( 'order.dt search.dt', function () {
        b.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    var c = $('.s_datatable').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    c.on( 'order.dt search.dt', function () {
        c.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    var d = $('.s_datatable_c').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    d.on( 'order.dt search.dt', function () {
        d.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    //facility
    var e = $('.f_datatable').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    e.on( 'order.dt search.dt', function () {
        e.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    var f = $('.f_datatable_c').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    f.on( 'order.dt search.dt', function () {
        f.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    var g = $('.fi_datatable').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    g.on( 'order.dt search.dt', function () {
        g.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    var h = $('.fi_datatable_c').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    h.on( 'order.dt search.dt', function () {
        h.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    $('#view-details-modal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var level = button.data('level') // Extract info from data-* attributes
		var level_id = button.data('level-id') // Extract info from data-* attributes
		var commodity_id = button.data('commodity-id') // Extract info from data-* attributes
		var quarter = button.data('quarter') // Extract info from data-* attributes
		var year = button.data('year') // Extract info from data-* attributes
		if (quarter = "") {quarter = 'NULL'}
		// console.log('dashboardv2/get_issue_discrepancy_details/'+level+'/'+level_id+'/'+commodity_id+"/"+quarter+"/"+year);
		// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		var modal = $(this)
		// modal.find('.modal-title').text('New message to ' + recipient)
		// modal.find('.modal-body').html(level + " "+ level_id);
		ajax_request_replace_div_content('dashboardv2/get_kemsa_issue_details/'+level+'/'+level_id+'/'+commodity_id+"/"+quarter+"/"+year,"#modal-body-data");

	})

	<?php if($filtered > 0): ?>
	<?php echo $utilization_graph_scr; ?>;
	<?php echo $utilization_graph_conf; ?>;

	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/'+subcounty_id+'/NULL/4/NULL/NULL/scr_consumption_chart_filtered',"#scr_consumption_chart_filtered"); 

	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/'+subcounty_id+'/NULL/5/NULL/NULL/conf_consumption_chart_filtered',"#conf_consumption_chart_filtered"); 

    <?php endif; ?>
	<?php //echo $utilization_bgg_screening_graph; ?>;
	<?php //echo $utilization_bgg_confirmatory_graph; ?>;
	<?php //echo $test_graph; ?>;
	<?php //echo $drawing_rights_graph; ?>;
	<?php //echo $drawing_utilization_graph; ?>;

	$(".select2").select2({
          placeholder: "0",
          // containerCssClass: "float-left" 
        });

	/*START OF INHERIT*/
	$('.top_filter_button').button().click(function(x){
          x.preventDefault(); 

          // var county_text = $('#county_filter option:selected').text();
          // var subcounty_text = $('#sub_county_filter option:selected').text();

          // var county_id = $('#county_filter option:selected').val();
          // var subcounty_id = $('#sub_county_filter option:selected').val();
          // var facility_id = $('#facility_id option:selected').val();
          var quarter = $('#quarter_filter option:selected').val();
          // console.log(quarter);
          if(quarter==0){
            window.location.replace(url+"dashboardv2/issues_discrepancy_dashboard");
          }else{
            	window.location.replace(url+"dashboardv2/issues_discrepancy_dashboard/NULL/NULL/"+quarter);
          }
          // alert(county_id);
          // alert(subcounty_id);
       });

});/*END OF JQUERY FUNCTION*/

var county_id = <?php echo json_encode($county_id); ?>;
var subcounty_id = <?php echo json_encode($subcounty_id); ?>;
// console.log(subcounty_id);
if (subcounty_id > 0) { 
		ajax_request_replace_div_content('dashboardv2/get_national_trend/NULL/'+subcounty_id,"#trend-chart"); 
		ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/subcounty/NULL/'+subcounty_id+'/NULL/4/NULL/NULL/scr_consumption_chart',"#scr_consumption_chart"); 
		ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/subcounty/NULL/'+subcounty_id+'/NULL/5/NULL/NULL/conf_consumption_chart',"#conf_consumption_chart"); 
}else{
	// ajax_request_replace_div_content('dashboardv2/get_national_trend/',"#trend-chart"); 
	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/subcounty/NULL/NULL/NULL/4/NULL/NULL/scr_consumption_chart',"#scr_consumption_chart"); 
	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/subcounty/NULL/NULL/NULL/5/NULL/NULL/conf_consumption_chart',"#conf_consumption_chart"); 
}

function ajax_request_replace_div_content(function_url,div){
	var function_url =url+function_url;
	var loading_icon=url+"assets/img/loader2.gif";
	$.ajax({
		type: "POST",
		url: function_url,
		beforeSend: function() {
			$(div).html("<img style='margin-left:20%;' src="+loading_icon+">");
		},
		success: function(msg) {
			// console.log(msg);
			$(div).html(msg);
			$('#datatable').DataTable(); 
		}
	});
} 

</script>