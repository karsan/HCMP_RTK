<style>
	.text-indent {
    	/*text-indent: 25px;*/
	}
	.title{
		color: #FFF!important;
		font-size: 13px;
	}
</style>
<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<div class="page-sidebar navbar-collapse collapse">
			<ul class="page-sidebar-menu page-sidebar-menu-light page-sidebar-menu-hover-submenu page-sidebar-menu-closed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				
				<li class="sidebar-toggler-wrapper">
					<div class="sidebar-toggler">
					</div>
				</li>
				</br>
				<li class="start active">
					<a href="<?php echo base_url().'dashboardv2'; ?>">
					<i class="icon-home"></i>
					<span class="title">HOME</span>
					<span class="selected"></span>
					</a>
				</li>
				
				<li class="heading">
					<h3 class="uppercase">
						<i class="fa fa-bar-chart"></i>
						REPORTS
					</h3>
				</li>

				<li class="start text-indent">
					<a href="<?php echo base_url().'dashboardv2/national_dashboard'; ?>">
					<i class="fa fa-building"></i>
					<span class="title">NATIONAL</span>
					<span class="selected"></span>
					</a>
				</li>

				<li class="start text-indent">
					<a href="<?php echo base_url().'dashboardv2/county_dashboard'; ?>">
					<i class="fa fa-building-o"></i>
					<span class="title">COUNTY</span>
					<span class="selected"></span>
					</a>
				</li>

				<li class="start text-indent">
					<a href="<?php echo base_url().'dashboardv2/subcounty_dashboard'; ?>">
					<i class="fa fa-building-o"></i>
					<span class="title">SUB-COUNTY</span>
					<span class="selected"></span>
					</a>
				</li>

				<li class="start text-indent">
					<a href="<?php echo base_url().'dashboardv2/facility_dashboard'; ?>">
					<!-- <a href="#"> -->
					<!-- <i class="fa fa-building-o"></i> -->
					<i class="fa fa-hospital-o"></i>
					<span class="title">FACILITIES</span>
					<span class="selected"></span>
					</a>
				</li>

				<li class="start text-indent">
					<a href="<?php echo base_url().'dashboardv2/balance_discrepancy_dashboard'; ?>">
					<!-- <a href="#"> -->
					<!-- <i class="fa fa-building-o"></i> -->
					<i class="fa fa-warning"></i>
					<span class="title">DISCREPANCIES - BALANCES</span>
					<span class="selected"></span>
					</a>
				</li>

				<li class="start text-indent">
					<a href="<?php echo base_url().'dashboardv2/issues_discrepancy_dashboard'; ?>">
					<!-- <a href="#"> -->
					<!-- <i class="fa fa-building-o"></i> -->
					<i class="fa fa-warning"></i>
					<span class="title">DISCREPANCIES - ISSUES</span>
					<span class="selected"></span>
					</a>
				</li>

				<li class="start text-indent">
					<a href="<?php echo base_url().'dashboardv2/kemsa_issues_dashboard'; ?>">
					<!-- <i class="icon-bar-chart"></i> -->
					<i class="fa fa-medkit"></i>
					<span class="title">KEMSA ISSUES</span>
					<span class="selected"></span>
					</a>
				</li>

				<li class="start text-indent">
					<a href="<?php echo base_url().'dashboardv2/kemsa_pod_dashboard'; ?>">
					<!-- <i class="icon-bar-chart"></i> -->
					<i class="fa fa-file-archive-o"></i>
					<span class="title">KEMSA PODs</span>
					<span class="selected"></span>
					</a>
				</li>

				<li class="start text-indent">
					<a href="<?php echo base_url().'dashboardv2/dhis_data_analysis_dashboard'; ?>">
					<!-- <i class="icon-bar-chart"></i> -->
					<i class="fa fa-line-chart"></i>
					<span class="title">DHIS DATA ANALYSIS</span>
					<span class="selected"></span>
					</a>
				</li>


				
				<!-- <li class="heading">
					<h3 class="uppercase">
						<i class="icon-logout"></i>
						SYSTEM ACCESS
					</h3>
				</li>
				<li class="text-indent">
					<a href="javascript:;">
					<span class="title">LOGIN</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="<?php echo base_url().'user'; ?>">
							RTK/CD4</a>
						</li>
					</ul>
				</li> -->
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
	<!-- END SIDEBAR