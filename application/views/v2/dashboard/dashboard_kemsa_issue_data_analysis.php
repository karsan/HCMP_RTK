<?php $this->load->view('v2/dashboard/dashboard_header'); ?>
<?php //echo "<pre>";print_r($county_id);exit; ?>
<?php //echo $test_graph;exit; ?>

<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solid  page-sidebar-closed">
	<style>
		.red{
			color:red!important;
		}
		.green{
			color:green!important;
		}
		.btn{
			width: 100%;
		}
	</style>
<?php $this->load->view('v2/dashboard/dashboard_top_header'); ?>
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->

<div class="page-container">
<?php $this->load->view('v2/dashboard/dashboard_sidebar'); ?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- <div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="<?php //echo base_url(); ?>">Dashboard</a>
					</li>
				</ul>
			</div> -->
						
			<h3 class="page-title no-margin">
			KEMSA Issue Data Analysis Dashboard 
			<!-- <small> data as at <?php //echo date('F/Y') ?> </small> -->
			<!-- <small> data as of <?php echo date('F Y', strtotime("-1 MONTH")) ?> reports. </small> -->
			</h3></br>
			<!-- END PAGE HEADER-->

			<div class="clearfix"></div>
			<div class="row">
			<!-- <h4>Value Differences</h4> -->
			<div class="col-md-12 no-padding">
				<div class="portlet light clearfix">
					<div class="portlet-title">
						<div class="caption">
							<!-- <i class="icon-share font-red-sunglo hide"></i> -->
							<span class="caption-subject font-green-haze bold uppercase">KEMSA Issue Discrepancies</span>
							<!-- <span class="caption-helper"><?php echo $title_append.$quarter_append; ?></span> -->
							<span class="caption-helper"><?php echo $title_append; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="col-md-12 no-spacing" style="">
							<table class="table a_datatable display cell-border compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th rowspan="2">#</th>
										<th rowspan="2">County</th>
										<th rowspan="2">Subcounty</th>
										<th rowspan="2">MFL</th>
										<th rowspan="2">Facility Name</th>

										<th colspan="3">Issued Records</th>
										<th colspan="3">Requested Records</th>
										<th colspan="2" >Data Retrieval Dates</th>
										
										<th rowspan="2">Issue Date</th>
										<th rowspan="2">Delivery Note</th>
										<!-- <th>Action</th> -->
									</tr>
									<tr>
										<th>Old</th>
										<th>New</th>
										<th>Difference</th>
										<th>Old</th>
										<th>New</th>
										<th>Difference</th>
										<th>Old</th>
										<th>New</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										// echo "<pre>";print_r($county_data_s);exit;
										foreach ($data_analysis as $key => $value) { 
											// $issued_diff = $value['qty_issued_current'] - $value['qty_issued_new'];
											// $requested_diff = $value['qty_requested_current'] - $value['qty_requested_new'];
											
											$issued_diff = $value['qty_issued_new'] - $value['qty_issued_current'];
											$requested_diff = $value['qty_requested_new'] - $value['qty_requested_current'];
											
											$i_class = ($issued_diff>0)? " green ":" red ";
											$i_class = ($issued_diff==0)? "":$i_class;

											$r_class = ($requested_diff>0)? " green ":" red ";
											$r_class = ($requested_diff==0)? "":$r_class;
									?>
									<tr>
										<td></td>
										<td><?php echo $value['county']; ?></td>
										<td><?php echo $value['district']; ?></td>
										<td><?php echo $value['facility_code']; ?></td>
										<td><?php echo $value['facility_name']; ?></td>
										
										<td><?php echo number_format($value['qty_issued_current']) ?></td>
										<td><?php echo number_format($value['qty_issued_new']) ?></td>
										<td class="<?php echo $i_class; ?>"><?php echo number_format($issued_diff) ?></td>

										<td><?php echo number_format($value['qty_requested_current']) ?></td>
										<td><?php echo number_format($value['qty_requested_new']) ?></td>
										<td class="<?php echo $r_class; ?>"><?php echo number_format($requested_diff) ?></td>

										<td><?php echo date('d F Y',strtotime($value['old_retrieval_date'])) ?></td>
										<td><?php echo date('d F Y',strtotime($value['created_at'])) ?></td>

										<td><?php echo date('d F Y',strtotime($value['issue_date'])) ?></td>
										<td><?php echo $value['delivery_note']; ?></td>


										<!-- <td><button class="btn btn-primary" data-first-key = "<?php echo $data_first_key ?>" data-second-key = "<?php echo $data_second_key ?>" data-commodity-id = "<?php echo $data_comm_id ?>"  data-delivery-note = "<?php echo $data_d_note ?>" data-toggle="modal" data-target=".view-details-modal">View Details</button></td> -->
									</tr>
									<?php } ?>
								</tbody>	
								<tfoot>
						            <tr>
						                <!-- <th colspan="4" style="text-align:right">Total:</th> -->
						                <th></th>
						                <th></th>
						                <th></th>
						                <th></th>
						                <th></th>
						                <th></th>
						                <th></th>
						                <th></th>
						                <th></th>
						                <th></th>
						                <th></th>
						                <th></th>
						                <th></th>
						                <th></th>
						                <th></th>

						            </tr>
						        </tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>

		</div>
	</div>
</div>


<div class="modal fade view-details-modal" id="view-details-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
        <h3 class="modal-title" id="myLargeModalLabel">KEMSA Issue Details</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>

      <div class="modal-body clearfix" >
      	<div id="modal-body-data">
        	<!-- <i class="fa fa-cog fa-spin"></i> -->
      	</div>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div> -->
  </div>
</div>


<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 <?php echo date('Y'); ?> &copy; RTK.
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
<?php $this->load->view('v2/dashboard/dashboard_footer'); ?>
</html>

<script>
	
</script>

<script>
var url ='<?php echo base_url()?>';
		// alert("I work");

$(function () { 
    // $('.datatable').DataTable();
    /*DATATABLES*/
	    var a = $('.a_datatable').DataTable( {
	        "columnDefs": [ {
	            "searchable": false,
	            "orderable": false,
	            "targets": 0
	        } ],
	        "order": [[ 1, 'asc' ]],
	        dom: 'Bfrtip',
	        buttons: [
		        'copy', 'excel', 'pdf'
		    ],
	        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // converting to interger to find total
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // computing column Total of the complete result 
				
	     	var issuedTotalOld = api
            .column( 5 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

            var issuedTotalNew = api
            .column( 6 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

            var issuedDiff = api
            .column( 7 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

            var requestedTotalOld = api
            .column( 8 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

            var requestedTotalNew = api
            .column( 9 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

            var requestedDiff = api
            .column( 10 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );
			
				
            // Update footer by showing the total with the reference of the column index 
	    $( api.column( 0 ).footer() ).html('Total');
            
            $( api.column( 5 ).footer() ).html(issuedTotalOld);
            $( api.column( 6 ).footer() ).html(issuedTotalNew);
            $( api.column( 7 ).footer() ).html(issuedDiff);
            $( api.column( 8 ).footer() ).html(requestedTotalOld);
            $( api.column( 9 ).footer() ).html(requestedTotalNew);
            $( api.column( 10 ).footer() ).html(requestedDiff);
        }
	    } );
	 
	    a.on( 'order.dt search.dt', function () {
	        a.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = i+1;
	        } );
	    } ).draw();




    /*END OF DATATABLES*/


    $('#view-details-modal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var first_key = button.data('first-key') // Extract info from data-* attributes
		var second_key = button.data('second-key') // Extract info from data-* attributes
		var commodity_id = button.data('commodity-id') // Extract info from data-* attributes
		var delivery_note = button.data('delivery-note') // Extract info from data-* attributes
		if (quarter = "") {quarter = 'NULL'}
		var delivery_note = encodeURIComponent(button.data('delivery-note')) // Extract info from data-* attributes

		// console.log('dashboardv2/get_kemsa_issue_data_analysis_modal/'+first_key+'/'+second_key+'/'+commodity_id+'/'+delivery_note);
		var modal = $(this)
		ajax_request_replace_div_content('dashboardv2/get_kemsa_issue_data_analysis_modal/'+first_key+'/'+second_key+'/'+commodity_id+'/'+delivery_note,"#modal-body-data");

	})

	<?php if($filtered > 0): ?>
	<?php echo $utilization_graph_scr; ?>;
	<?php echo $utilization_graph_conf; ?>;

	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/'+subcounty_id+'/NULL/4/NULL/NULL/scr_consumption_chart_filtered',"#scr_consumption_chart_filtered"); 

	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/facility/NULL/'+subcounty_id+'/NULL/5/NULL/NULL/conf_consumption_chart_filtered',"#conf_consumption_chart_filtered"); 

    <?php endif; ?>
	<?php //echo $utilization_bgg_screening_graph; ?>;
	<?php //echo $utilization_bgg_confirmatory_graph; ?>;
	<?php //echo $test_graph; ?>;
	<?php //echo $drawing_rights_graph; ?>;
	<?php //echo $drawing_utilization_graph; ?>;

	$(".select2").select2({
          placeholder: "0",
          // containerCssClass: "float-left" 
        });

	/*START OF INHERIT*/
	$('.top_filter_button').button().click(function(x){
          x.preventDefault(); 

          // var county_text = $('#county_filter option:selected').text();
          // var subcounty_text = $('#sub_county_filter option:selected').text();

          // var county_id = $('#county_filter option:selected').val();
          // var subcounty_id = $('#sub_county_filter option:selected').val();
          // var facility_id = $('#facility_id option:selected').val();
          var quarter = $('#quarter_filter option:selected').val();
          // console.log(quarter);
          if(quarter==0){
            window.location.replace(url+"dashboardv2/issues_discrepancy_dashboard");
          }else{
            	window.location.replace(url+"dashboardv2/issues_discrepancy_dashboard/NULL/NULL/"+quarter);
          }
          // alert(county_id);
          // alert(subcounty_id);
       });

});/*END OF JQUERY FUNCTION*/

var county_id = <?php echo json_encode($county_id); ?>;
var subcounty_id = <?php echo json_encode($subcounty_id); ?>;
// console.log(subcounty_id);
if (subcounty_id > 0) { 
		ajax_request_replace_div_content('dashboardv2/get_national_trend/NULL/'+subcounty_id,"#trend-chart"); 
		ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/subcounty/NULL/'+subcounty_id+'/NULL/4/NULL/NULL/scr_consumption_chart',"#scr_consumption_chart"); 
		ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/subcounty/NULL/'+subcounty_id+'/NULL/5/NULL/NULL/conf_consumption_chart',"#conf_consumption_chart"); 
}else{
	// ajax_request_replace_div_content('dashboardv2/get_national_trend/',"#trend-chart"); 
	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/subcounty/NULL/NULL/NULL/4/NULL/NULL/scr_consumption_chart',"#scr_consumption_chart"); 
	ajax_request_replace_div_content('dashboardv2/get_consumption_trend_by/subcounty/NULL/NULL/NULL/5/NULL/NULL/conf_consumption_chart',"#conf_consumption_chart"); 
}

function ajax_request_replace_div_content(function_url,div){
	var function_url =url+function_url;
	var loading_icon=url+"assets/img/loader2.gif";
	$.ajax({
		type: "POST",
		url: function_url,
		beforeSend: function() {
			$(div).html("<img style='margin-left:20%;' src="+loading_icon+">");
		},
		success: function(msg) {
			// console.log(msg);
			$(div).html(msg);
			$('#datatable').DataTable(); 
		}
	});
} 

</script>