<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo base_url().'assets/v2/dashboard/'; ?>/global/plugins/respond.min.js"></script>
<script src="<?php echo base_url().'assets/v2/dashboard/'; ?>/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo base_url().'assets/v2/dashboard/'; ?>/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>

<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url().'assets/v2/dashboard/'; ?>/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/v2/dashboard/'; ?>/global/plugins/bootstrap/js/popper.min.js" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/v2/dashboard/'; ?>/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/v2/dashboard/'; ?>/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url().'assets/v2/dashboard/'; ?>/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/v2/dashboard/'; ?>/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/v2/dashboard/'; ?>/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url().'assets/v2/dashboard/'; ?>/admin/layout/scripts/demo.js" type="text/javascript"></script> -->
<script src="<?php echo base_url().'assets/v2/dashboard/'; ?>/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/v2/dashboard/'; ?>/admin/pages/scripts/tasks.js" type="text/javascript"></script>

<script src=<?php echo base_url()."assets/v2/dashboard/vendors/select2/dist/js/select2.full.min.js"?> ></script>


<script src="<?php echo base_url().'assets/v2/highcharts/js/highcharts.js'; ?>" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/v2/highcharts/js/exporting.js'; ?>" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   Layout.init(); // init layout
   // QuickSidebar.init(); // init quick sidebar
   Index.init();   
   // Tasks.initDashboardWidget();
});
</script>
<!-- END JAVASCRIPTS -->