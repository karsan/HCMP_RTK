<style type="text/css">
	.margin-top{
		margin-top: 30px!important;
	}
	.lb-sm {
	  font-size: 12px;
	}

	.lb-md {
	  font-size: 16px;
	}

	.lb-lg {
	  font-size: 20px;
	}
</style>
<center>
<h4 class="alert alert-info col-md-6 col-md-offset-3">
Kindly upload BD Facs Presto CSV for <strong><?php echo date('F Y',strtotime("-1 month")); ?></strong> before you are allowed to report
</h4>
</center>
<div class="col-md-6 col-md-offset-3 clearfix margin-top">
	<?php if (isset($success) && $success>0 && $success == "1") {?>
		<div class="alert alert-success fade in alert-dismissable">DATA UPLOADED SUCCESSFULLY</div>
		<a class="btn btn-success" href="<?php echo base_url().'cd4_management/get_cd4_report/'.$facility_code; ?>"> Proceed to report</a>
	<?php }else{ ?>
		<?php  $att=array("name"=>'allocation_csv_form','id'=>'allocation_csv_form'); echo form_open_multipart('cd4_management/presto_csv',$att); ?>
			<select name="month" class="form-control" required>
				<option value="0">Select Month</option>
				<?php $months_count = count($months); for ($i=0; $i < $months_count; $i++) { ?>
					<option value="<?php echo $months[$i];?>"> <?php echo $months[$i]; ?></option>
				<?php }?>
			</select>
			<input type="hidden" name="facility_code" required value="<?php echo $facility_code ?>">
			<input type="file" name="userfile" id="file" required="required" class="form-control"><br>
			<button type="submit">Upload</button>
		<?php echo form_close(); ?>
	<?php } ?>
</div>