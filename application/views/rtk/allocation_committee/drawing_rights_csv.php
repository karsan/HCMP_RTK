<style type="text/css">
	.margin-top{
		margin-top: 30px!important;
	}
</style>
<?php //echo "<pre>";print_r($success);exit; ?>
<div class="col-md-6 col-md-offset-3 clearfix margin-top">
	<?php if (isset($success) && $success>0 && $success == "1") {?>
		<div class="alert alert-success fade in alert-dismissable">DATA UPLOADED</div>
	<?php }elseif (isset($success) && $success>0 && $success == "2") {?>
		<div class="alert alert-danger fade in alert-dismissable">DATA NOT UPLOADED</div>
	<?php } ?>

	<h2>New drawing right</h2>
	<p>All counties listed</p>

	<?php  $att=array("name"=>'drawing_rights_new_form','id'=>'drawing_rights_new_form'); echo form_open_multipart('rtk_management/drawing_rights_master/addition',$att); ?>
	<table class="table table-striped">
		<tr>
			<td colspan="2">
				<select name="county" class="form-control">
				<option value="">Select county</option>
					<?php foreach ($counties as $key => $value) { ?>
						<option value="<?php echo $value['id']; ?>"><?php echo $value['county']; ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<input class="form-control" type="number" name="screening" placeholder="Screening new amount" />
			</td>
			<td>
				<input class="form-control" type="number" name="confirmatory" placeholder="Confirmatory new amount" />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<button class="btn btn-primary" type="submit">Add new</button>
			</td>
		</tr>
	</table>
	<?php echo form_close(); ?>

	<hr class="half-rule"/>

	<h2>Edit drawing rights</h2>
	<p>Only counties already in the county_drawing_rights table are displayed here. If adding a new county's allocation data, use new drawing rights form</p>

	<?php  $att=array("name"=>'drawing_rights_master','id'=>'drawing_rights_edit_form'); echo form_open_multipart('rtk_management/drawing_rights_master/update',$att); ?>
	<table class="table table-striped">
		<tr>
			<td colspan="2">
				<select name="county" class="form-control">
					<option value="">Select county</option>
					<?php foreach ($drawing_rights_counties as $key => $value) { ?>
						<option value="<?php echo $value['county_id']; ?>"><?php echo $value['county']; ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<input class="form-control" type="number" name="screening" placeholder="Screening new amount" />
			</td>
			<td>
				<input class="form-control" type="number" name="confirmatory" placeholder="Confirmatory new amount" />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<button class="btn btn-primary" type="submit">Submit update</button>
			</td>
		</tr>
	</table>
	<?php echo form_close(); ?>

	<hr class="half-rule"/>

	<h2>Upload drawing rights</h2>
	<?php if (isset($success) && $success>0 && $success == "1") {?>
		<div class="alert alert-success fade in alert-dismissable">DATA UPLOADED SUCCESSFULLY</div>
	<?php } ?>
	<?php  $att=array("name"=>'drawing_rights_csv_form','id'=>'drawing_rights_csv_form'); echo form_open_multipart('rtk_management/drawing_rights_csv',$att); ?>
		<table class="table table-striped">
			<tr>
				<td>
					<input type="file" name="file" id="file" required="required" class="form-control"><br>
				</td>
			</tr>
			<tr>
				<td>
					<button type="submit" class="btn btn-primary">Upload</button>
				</td>
			</tr>
		</table>
	<?php echo form_close(); ?>

	<hr class="half-rule"/>

	
</div>