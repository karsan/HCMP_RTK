<div class="col-md-6 col-md-offset-3">
<center>
	<span class="" style="font-size: 15px;"><strong>Data is shown as per individual row in the facilities table</strong></span>
</center>
</div>
<div class="col-md-6 col-md-offset-3">
	<?php //echo "<pre>";print_r($facility_data);exit; ?>
	<table class="table table-bordered clearfix" style="margin:5px;">
		<tr>
			<td><strong>District ID</strong></td>
			<td><?php echo $facility_data['district_id']; ?></td>
		</tr>

		<tr>
			<td><strong>District Name</strong></td>
			<td><?php echo $facility_data['district_name']; ?></td>
		</tr>

		<tr>
			<td><strong>County ID</strong></td>
			<td><?php echo $facility_data['county_id']; ?></td>
		</tr>

		<tr>
			<td><strong>County Name</strong></td>
			<td><?php echo $facility_data['county_name']; ?></td>
		</tr>

		<tr>
			<td><strong>Facility ID</strong></td>
			<td><?php echo $facility_data['facility_id']; ?></td>
		</tr>

		<tr>
			<td><strong>Facility Code</strong></td>
			<td><?php echo $facility_data['facility_code']; ?></td>
		</tr>

		<tr>
			<td><strong>Facility Name</strong></td>
			<td><?php echo $facility_data['facility_name']; ?></td>
		</tr>

		<tr>
			<td><strong>Partner</strong></td>
			<td><?php echo $facility_data['partner']; ?></td>
		</tr>

		<tr>
			<td><strong>Owner</strong></td>
			<td><?php echo $facility_data['owner']; ?></td>
		</tr>

		<tr>
			<td><strong>Type</strong></td>
			<td><?php echo $facility_data['type']; ?></td>
		</tr>

		<tr>
			<td><strong>Level</strong></td>
			<td><?php echo $facility_data['level']; ?></td>
		</tr>

		<tr>
			<td><strong>RTK Enabled</strong></td>
			<td><?php echo $facility_data['rtk_enabled']; ?></td>
		</tr>

		<tr>
			<td><strong>Using HCMP</strong></td>
			<td><?php echo $facility_data['using_hcmp']; ?></td>
		</tr>

	</table>
</div>