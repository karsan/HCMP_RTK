<style type="text/css">
	.btn{
		border-radius: 0px!important;
	}
	.margin-top{
		margin-top: 5px!important;
	}
</style>
<div class="content col-md-12">
	<center>
	<h1>Administrative Interface</h1>
	</center>
	<div class="col-md-12">
		<center>
			<div class="col-md-3">
				<div class="col-md-12"><h5>DATA MANAGEMENT</h5></div>
				<a class="btn btn-primary col-md-12 margin-top" href="<?php echo base_url().'rtk_admin/drawing_rights_dashboard'; ?>">Drawing Rights</a><br>
				<a class="btn btn-primary col-md-12 margin-top" href="<?php echo base_url().'rtk_admin/admin_allocations'; ?>">Allocations</a><br>
				<a class="btn btn-primary col-md-12 margin-top" href="<?php echo base_url().'rtk_admin/admin_facilities_dashboard'; ?>">Facility Management</a><br>
				<a class="btn btn-primary col-md-12 margin-top" href="<?php echo base_url().'rtk_admin/fcdrr_management'; ?>">FCDRR Management</a><br>
				<a class="btn btn-primary col-md-12 margin-top" href="<?php echo base_url().'rtk_admin/reporting_rates_recalculate_interface'; ?>">Reporting Rates Recalculation</a><br>
				<a class="btn btn-primary col-md-12 margin-top" href="<?php echo base_url().'rtk_admin/admin_cd4_management'; ?>">CD4 Facility Management</a><br>
				<a class="btn btn-primary col-md-12 margin-top" href="<?php echo base_url().'rtk_admin/dhis_fcdrr_data_management_interface'; ?>">DHIS FCDRR Management</a><br>
			</div>

			<div class="col-md-3">
				<div class="col-md-12"><h5>DATA UPLOAD</h5></div>
				<a class="btn btn-primary col-md-12 margin-top" href="<?php echo base_url().'cd4_management/cd4_facilities_csv_interface'; ?>">BD Facs Presto Sites Excel Upload</a><br>
				<a class="btn btn-primary col-md-12 margin-top" href="<?php echo base_url().'rtk_admin/cd4_sites_interface'; ?>">CD4 Excel Upload</a><br>
				<a class="btn btn-primary col-md-12 margin-top" href="<?php echo base_url().'rtk_admin/kemsa_issues_excel_interface'; ?>">KEMSA Issues Upload</a><br>
				<a class="btn btn-primary col-md-12 margin-top" href="<?php echo base_url().'rtk_admin/kemsa_allocations_excel_interface'; ?>">KEMSA Excel Allocations Upload</a><br>
				<a class="btn btn-primary col-md-12 margin-top" href="<?php echo base_url().'rtk_admin/kemsa_pod_upload_interface'; ?>">KEMSA POD Upload</a><br>
				<a class="btn btn-primary col-md-12 margin-top" href="<?php echo base_url().'rtk_admin/dhis_csv_interface'; ?>">DHIS CSV Upload</a><br>
				<a class="btn btn-primary col-md-12 margin-top" href="<?php echo base_url().'rtk_admin/dhis_facilities_csv_interface'; ?>">DHIS Facilities CSV Upload</a><br>
				
			</div>

			<div class="col-md-3">
				<div class="col-md-12"><h5>NOTIFICATION MANAGEMENT</h5></div>
				<a class="btn btn-primary col-md-12 margin-top" href="<?php echo base_url().'rtk_admin/send_issues_mail_interface'; ?>">KEMSA Issue Email Management</a><br>
			</div>
		</center>
	</div>
</div>