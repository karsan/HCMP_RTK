<style type="text/css">
	.margin-top{
		margin-top: 30px!important;
	}
</style>
<div class="col-md-10 col-md-offset-1 clearfix margin-top">
	<?php if (isset($success) && $success>0 && $success == "1") {?>
		<div class="alert alert-success fade in alert-dismissable">MAIL SENT SUCCESSFULLY</div>
	<?php } ?>
	<div class="col-md-12 alert alert-info">
		<h5>This functionality is fully dependendent on the KEMSA issues API.</h5>
		<h5>If county alone is selected, whole county's issues for the current month i.e. <?php echo date('F Y') ?> will be forwarded to them.</h5>
		<h5>County issue once sent for a specific month will be stored to ensure no repeat emails are sent.</h5>
	</div>
	<?php echo form_open('rtk_admin/send_issues_mail') ?>
		<div class="col-md-12">
			<div class="col-md-4">
				<select name="county" class="form-control select2">
						<option value="0">Select County</option>		
						<?php foreach ($counties as $key => $value) { ?>
							<option value="<?php echo $value['id']; ?>"><?php echo $value['id'].' | '.$value['county']; ?></option>
						<?php } ?>
				</select>
			</div>
			<div class="col-md-4">
				<select name="date" class="form-control select2">
						<option value="0">Select Issue Date</option>		
						<?php foreach ($months as $key => $value) { ?>
							<option value="<?php echo $value['yearmonth']; ?>"><?php echo $value['month_year_full']; ?></option>
						<?php } ?>
				</select>
			</div>

			<div class="col-md-2">
				Check if running test: <input type="checkbox" name="testing" class="input ">
			</div>
			<div class="col-md-2">
				<button class="btn btn-primary" type="submit">Send Issues Email</button>
			</div>
		</div>
	<?php echo form_close(); ?>
		<div class="col-md-12 margin-top">
			<table class="table table-bordered compact" id="datatable_">
				<thead>
					<th>County</th>
					<th>Email Status</th>
					<th>Issue Date</th>
					<th>Date Email Sent</th>
				</thead>
				<tbody>
					<?php foreach ($issue_emails as $key => $value) { ?>
					<tr>
						<td><?php echo $value['county']; ?></td>
						<td><?php echo $value['email_status']; ?></td>
						<td><?php echo date('jS F Y',strtotime($value['issue_date'])); ?></td>
						<td><?php echo date('jS F Y',strtotime($value['date_sent'])); ?></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
</div>

<script>
$(document).ready(function() {
	$('#datatable_').dataTable({
     "sDom": "T lfrtip",
     "aaSorting": [],
     "bJQueryUI": false,
      "bPaginate": false,
      "oLanguage": {
        "sLengthMenu": "_MENU_ Records per page",
        "sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
      },
      "oTableTools": {
      "aButtons": [ 
          "copy",
          "print",
          {
            "sExtends": "collection",
            "sButtonText": 'Save',
            "aButtons": ["csv", "xls", "pdf"]
          }     
      
      ],  
      "sSwfPath": "<?php echo base_url();?>assets/datatable/media/swf/copy_csv_xls_pdf.swf"
    }
  });

	$('.select2').select2();
});
</script>