<style type="text/css">
	.margin-top{
		margin-top: 30px!important;
	}
</style>
<div class="col-md-6 col-md-offset-3 clearfix margin-top">
	<?php if (isset($success) && $success>0 && $success == "1") {?>
		<div class="alert alert-success fade in alert-dismissable">ALLOCATION MOVED SUCCESSFULLY</div>
	<?php } ?>
	<div class="alert alert-info">
		<p>District: <strong><?php echo $district_name ?></strong></p>
		<p>Current allocation date: <strong><?php echo $allocation_month." ".$allocation_year ?></strong></p>
		<!-- <p><strong>Note: </strong> Do not move an allocation from one month to another when there exists an allocation in the destination month.</p> -->
		<p><small><strong>Note: </strong> Delete any allocations in the destination month for this district if present.</small>	</p>
		<p><small>Select month to move <strong><?php echo $allocation_month." ".$allocation_year ?></strong> allocation for <strong><?php echo $district_name; ?> </strong>to.</small></p>
	</div>
	<?php  $att=array("name"=>'admin_post_move_allocation','id'=>'admin_post_move_allocation'); echo form_open('rtk_admin/admin_post_move_allocation',$att); ?>
		<input type="hidden" name="allocation_month" value="<?php echo $allocation_month." ".$allocation_year; ?>">
		<input type="hidden" name="district_id" value="<?php echo $district_id; ?>">
		<select class="form-control col-md-6" name="destination_month">
			<option value="0">Select month</option>
		<?php 
			foreach ($month_year_data as $keyy => $valuee) {
			// echo "<pre>";print_r($value);exit; 
		?>
			<option value="<?php echo $valuee;?>"><?php echo $valuee; ?></option>
		<?php } ?>
	</select>
	<button type="submit" class="btn btn-sm btn-primary col-md-6 pull-right margin-top">Move allocation</button>

	<?php echo form_close(); ?>
</div>
