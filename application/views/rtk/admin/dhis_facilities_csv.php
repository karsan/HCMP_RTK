<style type="text/css">
	.margin-top{
		margin-top: 30px!important;
	}
	.lb-sm {
	  font-size: 12px;
	}

	.lb-md {
	  font-size: 16px;
	}

	.lb-lg {
	  font-size: 20px;
	}
</style>
<!-- <center>
<h4 class="alert alert-info col-md-6 col-md-offset-3">
For this to work, csv needs to have MFL's at second column and two row for headers only. Only made to work for BD Facs Presto. Modify in the function.
</h4>
</center> -->
<div class="col-md-6 col-md-offset-3 clearfix margin-top">
	<?php if (isset($success) && $success>0 && $success == "1") {?>
		<div class="alert alert-success fade in alert-dismissable">DATA UPLOADED SUCCESSFULLY</div>
	<?php }else{ ?>
		<?php  $att=array("name"=>'dhis_csv_form','id'=>'dhis_facilities_csv_form'); echo form_open_multipart('rtk_admin/dhis_facilities_csv',$att); ?>
			<input type="file" name="userfile" id="file" required="required" class="form-control"><br>
			<button type="submit">Upload</button>
		<?php echo form_close(); ?>
	<?php } ?>
</div>