<style type="text/css">
	.no-border-radius{
		border-radius: 0!important;
	}
	.no-margin{
		margin:0px!important;
	}

	.btn{
		border-radius: 0px!important;
		height:30px!important;
	}
	.margin-top{
		margin-top: 5px!important;
	}
	.margin-vert{
		margin: 5px 0px!important;
	}
	.no-spacing{
		padding: 0!important;
		margin: 0!important;
	}

</style>
<div class="col-md-12 clearfix margin-top">
	<?php if (isset($success) && $success>0 && $success == "1") {?>
		<div class="alert alert-success fade in alert-dismissable">OPERATION SUCCESSFUL</div>
	<?php } ?>
	<div class="col-md-12 alert alert-info">
		<h5><strong>Note: </strong></h5>
		<h5>Currently, this function only works based on district due to DHIS API limitations .</h5>
		<h5>The DHIS API only allows for requests for up to about <strong>200</strong> facilities at a time.</h5>
	</div>
		<div class="col-md-12">
			<div class="col-md-8 no-spacing">
				<select class="form-control select2 no-border-radius county_filter" name="county" id="county_filter" required>
					<option value="0">Select County</option>		
						<?php foreach ($counties as $key => $value) { ?>
							<option value="<?php echo $value['id']; ?>"><?php echo $value['county']; ?></option>
						<?php } ?>
				</select>
			</div>

			<div class="col-md-4 no-spacing">
				<button class="btn btn-primary col-md-12 no-border-radius top_filter_button" id='top_filter_button' type="submit">Filter</button>
			</div>
		</div>

</div>

<div class="col-md-12">
	<table class="table table-bordered table-fluid compact col-md-12" id="datatable_">
		<thead>
			<th>County</th>
			<th>District</th>
			<th>Status</th>
			<th>Action</th>
		</thead>
		<tbody>
			<?php foreach ($all_district_logs as $key => $value) { ?>
			<tr>
				<td><?php echo $value['county']; ?></td>
				<td><?php echo $value['district']; ?></td>
				<td>
					<?php 
						$status = $value['log_status'];
						if ($value['log_status'] > 0) { ?>
						<p class="label label-success">Pulled / Avaliable Locally</p>	
						<?php }else{?>
						<p class="label label-danger">Not Pulled / Pending</p>	
					<?php } ?>		
				</td>
				<td>
					<?php 
						$status = $value['log_status'];
						if ($value['log_status'] > 0) { ?>
						<button class="btn btn-primary" data-level = "subcounty" data-level-id = "<?php echo $value['district_id'] ?>" data-operation="view_data_by_facility" data-toggle="modal" data-target=".view-details-modal">View / Import Data </button>

						<button class="btn btn-primary" data-level = "subcounty" data-level-id = "<?php echo $value['district_id'] ?>" data-operation="pull_data" data-toggle="modal" data-target=".view-details-modal">Pull Data</button>
						
						<?php }else{?>
						<button class="btn btn-primary" data-level = "subcounty" data-level-id = "<?php echo $value['district_id'] ?>" data-operation="pull_data" data-toggle="modal" data-target=".view-details-modal">Pull Data</button>
					<?php } ?>		
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>

<div class="modal fade view-details-modal" id="view-details-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width: 1000px;">
    <div class="modal-content">
    	<div class="modal-header">
        <h3 class="modal-title" id="myLargeModalLabel">DHIS Data Import</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>

      <div class="modal-body clearfix">
      	<div id="modal-body-data">
        	<!-- <i class="fa fa-cog fa-spin"></i> -->
      	</div>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div> -->
  </div>
</div>
</div>

<script>
$(document).ready(function() {
var url ='<?php echo base_url()?>';
	$('#datatable_').dataTable({
     "sDom": "T lfrtip",
     "aaSorting": [],
     "bJQueryUI": false,
      "bPaginate": false,
      "oLanguage": {
        "sLengthMenu": "_MENU_ Records per page",
        "sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
      },
      "oTableTools": {
      "aButtons": [ 
          "copy",
          "print",
          {
            "sExtends": "collection",
            "sButtonText": 'Save',
            "aButtons": ["csv", "xls", "pdf"]
          }     
      
      ],  
      "sSwfPath": "<?php echo base_url();?>assets/datatable/media/swf/copy_csv_xls_pdf.swf"
    }
  });

	$('.select2').select2();

	$('#view-details-modal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var level = button.data('level') // Extract info from data-* attributes
		var level_id = button.data('level-id') // Extract info from data-* attributes
		var operation = button.data('operation') // Extract info from data-* attributes
		// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		var modal = $(this)
		console.log('rtk_admin/get_dhis_fcdrr_details/'+level+'/'+level_id+'/'+operation);
		// modal.find('.modal-title').text('New message to ' + recipient)
		// modal.find('.modal-body').html(level + " "+ level_id);
		ajax_request_replace_div_content('rtk_admin/get_dhis_fcdrr_details/'+level+'/'+level_id+'/'+operation,"#modal-body-data");

	});

	$(document).on("click", "#modal_fcdrr_details", function(event){
	    // alert( "GO" ); 
	    // console.log("CLICKED");
		// var button = $(event.relatedTarget) // Button that triggered the modal
		var level = $(this).data('level') // Extract info from data-* attributes
		var level_id = $(this).data('level-id') // Extract info from data-* attributes
		var operation = $(this).data('operation') // Extract info from data-* attributes

		var month_year = $(this).data('month-year') // Extract info from data-* attributes
		console.log(operation);
		console.log('rtk_admin/get_dhis_fcdrr_details/'+level+'/'+level_id+'/'+operation);
		if (month_year) {
			console.log('rtk_admin/get_dhis_order_details/'+month_year+'/'+level_id);
			// ajax_request_replace_div_content('dashboardv2/get_facility_order_details/'+order_id,"#modal-body-data");
			if (operation === 'import_data_by_fcdrr') {
				console.log("IMPORTING...");
				console.log('rtk_admin/import_dhis_order/'+month_year+'/'+level_id);
				ajax_request_replace_div_content('rtk_admin/import_dhis_order/'+month_year+'/'+level_id,"#modal-body-data");
			}else{
				ajax_request_replace_div_content('rtk_admin/get_dhis_order_details/'+month_year+'/'+level_id,"#modal-body-data");
				
			}
		}else{
			ajax_request_replace_div_content('rtk_admin/get_dhis_fcdrr_details/'+level+'/'+level_id+'/'+operation,"#modal-body-data");
		}
	});

	$('.top_filter_button').button().click(function(x){
		x.preventDefault(); 

		var county_id = $('#county_filter option:selected').val();
		var year = $('#year_filter option:selected').val();

		// console.log(county_id);return;
		window.location.replace(url+"rtk_admin/dhis_fcdrr_data_management_interface/NULL/"+county_id);

		// alert(county_id);
		// alert(subcounty_id);
	});

});//END OF JQUERY

function ajax_request_replace_div_content(function_url,div){
	var url ='<?php echo base_url()?>';
	var function_url =url+function_url;
	var loading_icon=url+"assets/img/loader2.gif";
	$.ajax({
		type: "POST",
		url: function_url,
		beforeSend: function() {
			$(div).html("<img style='margin-left:20%;' src="+loading_icon+">");
		},
		success: function(msg) {
			// console.log(msg);
			$(div).html(msg);
			$('#datatable').dataTable({
		     "sDom": "T lfrtip",
		     "aaSorting": [],
		     "bJQueryUI": false,
		      "bPaginate": false,
		      "oLanguage": {
		        "sLengthMenu": "_MENU_ Records per page",
		        "sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
		      },
		      "oTableTools": {
		      "aButtons": [ 
		          "copy",
		          "print",
		          {
		            "sExtends": "collection",
		            "sButtonText": 'Save',
		            "aButtons": ["csv", "xls", "pdf"]
		          }     
		      
		      ],  
		      "sSwfPath": "<?php echo base_url();?>assets/datatable/media/swf/copy_csv_xls_pdf.swf"
		    }
		  });
		}
	});
} 
</script>