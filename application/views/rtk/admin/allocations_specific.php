<style type="text/css">
	.btn{
		border-radius: 0px!important;
	}
	.margin-top{
		margin-top: 5px!important;
	}
</style>
<div class="col-md-12">
	<table class="table table-bordered table-fluid col-md-12" id="datatable">
		<thead>
			<th>Order ID</th>
			<th>Order Date</th>
			<th>District</th>
			<th>Action</th>
		</thead>
		<tbody>
			<?php foreach ($orders as $key => $value) { ?>
			<tr>
				<td><?php echo $value['order_id']; ?></td>
				<td><?php echo $value['order_date']; ?></td>
				<td><?php echo $value['district']; ?></td>
				<td><a class="btn btn-primary" href="<?php echo base_url().'rtk_management/admin_allocation_specific/'.$value['district_id'].'/'.$value['order_date']; ?>">View Details</a></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>

<script>
$(document).ready(function() {
	$('#datatable').dataTable({
     "sDom": "T lfrtip",
     "aaSorting": [],
     "bJQueryUI": false,
      "bPaginate": false,
      "oLanguage": {
        "sLengthMenu": "_MENU_ Records per page",
        "sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
      },
      "oTableTools": {
      "aButtons": [ 
          "copy",
          "print",
          {
            "sExtends": "collection",
            "sButtonText": 'Save',
            "aButtons": ["csv", "xls", "pdf"]
          }     
      
      ],  
      "sSwfPath": "<?php echo base_url();?>assets/datatable/media/swf/copy_csv_xls_pdf.swf"
    }
  });
});
</script>