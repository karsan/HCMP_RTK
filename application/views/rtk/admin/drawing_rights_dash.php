<style type="text/css">
	.margin-top{
		margin-top: 30px!important;
	}
</style>
<div class="col-md-12 alert alert-info">
	<div class="col-md-6">
		<ul class="list">
			<strong>County functionalities</strong>
			<li>Update quarterly drawing rights</li>
			<li>New drawing rights</li>
			<li>Edit drawing rights</li>
			<li>Reverse drawing rights</li>
			<li>Upload drawing rights</li>
		</ul>
	</div>

	<div class="col-md-6">
		<ul class="list">
			<strong>Subcounty functionalities</strong>
			<li>New drawing rights</li>
			<li>Edit drawing rights</li>
		</ul>
	</div>
</div>
<?php //echo "<pre>";print_r($success);exit; ?>
	<?php if (isset($success) && $success>0 && $success == "1") {?>
		<div class="col-md-12 margin-top alert alert-success fade in alert-dismissable">OPERATION SUCCESSFUL.</div>
	<?php }elseif (isset($success) && $success>0 && $success == "2") {?>
		<div class="col-md-12 margin-top alert alert-danger fade in alert-dismissable">OPERATION UNSUCCESSFUL.</div>
	<?php } ?>

<div class="col-md-6 clearfix margin-top">
	<h2>County update quarterly drawing rights</h2>
	<p>All counties listed</p>
	<p>Function computes balances from previous quarter(s) and adds balance to quarterly drawing rights.</p>
	<p>Should be scheduled but due to the difference in the dates that allocations are opened, is to be triggered.</p>
	<p>If all counties are to be updated, select <strong>Quarter</strong> only.</p>

	<?php  $att=array("name"=>'drawing_rights_update_form','id'=>'drawing_rights_update_form'); echo form_open_multipart('rtk_admin/drawing_rights_quarterly_update',$att); ?>
	<table class="table table-striped">
		<tr>
			<td colspan="2">
				<select name="entity_id" class="form-control">
				<option value="">Select County</option>
					<?php foreach ($counties as $key => $value) { ?>
						<option value="<?php echo $value['id']; ?>"><?php echo $value['county']; ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<select name="quarter" class="form-control">
				<option value="">Select Quarter</option>
					<?php foreach ($quarter_data as $key => $value) { ?>
						<option value="<?php echo $value['quarter_year']; ?>"><?php echo $value['quarter_text']; ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<button class="btn btn-primary" type="submit">Update</button>
			</td>
		</tr>
	</table>
	<?php echo form_close(); ?>

	<hr class="half-rule"/>

	<h2>County edit drawing rights</h2>
	<p>Only counties already in the county_drawing_rights table are displayed here. If adding a new county's allocation data, use new drawing rights form</p>

	<?php  $att=array("name"=>'drawing_rights_master','id'=>'drawing_rights_edit_form'); echo form_open_multipart('rtk_admin/drawing_rights_master/update/county',$att); ?>
	<table class="table table-striped">
		<tr>
			<td colspan="2">
				<select name="entity_id" class="form-control">
					<option value="">Select County</option>
					<?php foreach ($drawing_rights_counties as $key => $value) { ?>
						<option value="<?php echo $value['county_id']; ?>"><?php echo $value['county']; ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<input class="form-control" type="number" name="screening" placeholder="Screening new amount" />
			</td>
			<td>
				<input class="form-control" type="number" name="confirmatory" placeholder="Confirmatory new amount" />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<button class="btn btn-primary" type="submit">Submit update</button>
			</td>
		</tr>
	</table>
	<?php echo form_close(); ?>

	<hr class="half-rule"/>

	<h2>County reverse drawing rights distributions</h2>
	<!-- <?php if (isset($success) && $success>0 && $success == "1") {?>
		<div class="alert alert-success fade in alert-dismissable">DATA UPLOADED SUCCESSFULLY</div>
	<?php } ?> -->
	<p>This reverses the latest drawing rights allocation back to the county's available resource pool.</p>
	<p>This action is <strong>irreversible.</strong> The distributed value will be added back to the county's drawing rights ready for redistribution and the subcounty drawing rights will be set to 0 along with what they have used thus far to ensure logic remains unbroken.</p>
	<p>This is not advisable after subcounties have allocated to facilities unless data is expendable.</p>
	<!-- <p>Adds value back to screening_allowed_current and confirmatory_allowed_current columns</p> -->

	<?php  $att=array("name"=>'drawing_rights_reversal','id'=>'drawing_rights_reversal_form'); echo form_open_multipart('rtk_admin/drawing_rights_reversal',$att); ?>
	<table class="table table-striped">
		<tr>
			<td colspan="2">
				<select name="entity_id" class="form-control">
					<option value="">Select County</option>
					<?php foreach ($drawing_rights_counties as $key => $value) { ?>
						<option value="<?php echo $value['county_id']; ?>"><?php echo $value['county']; ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<!-- <tr>
			<td>
				<input class="form-control" type="number" name="screening" placeholder="Screening new amount" />
			</td>
			<td>
				<input class="form-control" type="number" name="confirmatory" placeholder="Confirmatory new amount" />
			</td>
		</tr> -->
		<tr>
			<td colspan="2">
				<button class="btn btn-primary" type="submit">Reverse distribution</button>
			</td>
		</tr>
	</table>
	<?php echo form_close(); ?>

	<hr class="half-rule"/>

	<h2>County upload drawing rights</h2>
	<!-- <?php if (isset($success) && $success>0 && $success == "1") {?>
		<div class="alert alert-success fade in alert-dismissable">DATA UPLOADED SUCCESSFULLY</div>
	<?php } ?> -->
	<?php  $att=array("name"=>'drawing_rights_csv_form','id'=>'drawing_rights_csv_form'); echo form_open_multipart('rtk_admin/drawing_rights_csv',$att); ?>
		<table class="table table-striped">
			<tr>
				<td>
					<input type="file" name="file" id="file" required="required" class="form-control"><br>
				</td>
			</tr>
			<tr>
				<td>
					<button type="submit" class="btn btn-primary">Upload</button>
				</td>
			</tr>
		</table>
	<?php echo form_close(); ?>

	<hr class="half-rule"/>
</div>

<div class="col-md-6 clearfix margin-top">
	<h2>County new drawing right</h2>
	<p>All counties listed</p>

	<?php  $att=array("name"=>'drawing_rights_new_form','id'=>'drawing_rights_new_form'); echo form_open_multipart('rtk_admin/drawing_rights_master/addition/county',$att); ?>
	<table class="table table-striped">
		<tr>
			<td colspan="2">
				<select name="entity_id" class="form-control">
				<option value="">Select County</option>
					<?php foreach ($counties as $key => $value) { ?>
						<option value="<?php echo $value['id']; ?>"><?php echo $value['county']; ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<input class="form-control" type="number" name="screening" placeholder="Screening new amount" />
			</td>
			<td>
				<input class="form-control" type="number" name="confirmatory" placeholder="Confirmatory new amount" />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<button class="btn btn-primary" type="submit">Add new</button>
			</td>
		</tr>
	</table>
	<?php echo form_close(); ?>

	<hr class="half-rule"/>

	<h2>County edit drawing rights</h2>
	<p>Only counties already in the county_drawing_rights table are displayed here. If adding a new county's allocation data, use new drawing rights form</p>

	<?php  $att=array("name"=>'drawing_rights_master','id'=>'drawing_rights_edit_form'); echo form_open_multipart('rtk_admin/drawing_rights_master/update/county',$att); ?>
	<table class="table table-striped">
		<tr>
			<td colspan="2">
				<select name="entity_id" class="form-control">
					<option value="">Select County</option>
					<?php foreach ($drawing_rights_counties as $key => $value) { ?>
						<option value="<?php echo $value['county_id']; ?>"><?php echo $value['county']; ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<input class="form-control" type="number" name="screening" placeholder="Screening new amount" />
			</td>
			<td>
				<input class="form-control" type="number" name="confirmatory" placeholder="Confirmatory new amount" />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<button class="btn btn-primary" type="submit">Submit update</button>
			</td>
		</tr>
	</table>
	<?php echo form_close(); ?>

	<hr class="half-rule"/>

	<h2>County reverse drawing rights distributions</h2>
	<!-- <?php if (isset($success) && $success>0 && $success == "1") {?>
		<div class="alert alert-success fade in alert-dismissable">DATA UPLOADED SUCCESSFULLY</div>
	<?php } ?> -->
	<p>This reverses the latest drawing rights allocation back to the county's available resource pool.</p>
	<p>This action is <strong>irreversible.</strong> The distributed value will be added back to the county's drawing rights ready for redistribution and the subcounty drawing rights will be set to 0 along with what they have used thus far to ensure logic remains unbroken.</p>
	<p>This is not advisable after subcounties have allocated to facilities unless data is expendable.</p>
	<!-- <p>Adds value back to screening_allowed_current and confirmatory_allowed_current columns</p> -->

	<?php  $att=array("name"=>'drawing_rights_reversal','id'=>'drawing_rights_reversal_form'); echo form_open_multipart('rtk_admin/drawing_rights_reversal',$att); ?>
	<table class="table table-striped">
		<tr>
			<td colspan="2">
				<select name="entity_id" class="form-control">
					<option value="">Select County</option>
					<?php foreach ($drawing_rights_counties as $key => $value) { ?>
						<option value="<?php echo $value['county_id']; ?>"><?php echo $value['county']; ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<!-- <tr>
			<td>
				<input class="form-control" type="number" name="screening" placeholder="Screening new amount" />
			</td>
			<td>
				<input class="form-control" type="number" name="confirmatory" placeholder="Confirmatory new amount" />
			</td>
		</tr> -->
		<tr>
			<td colspan="2">
				<button class="btn btn-primary" type="submit">Reverse distribution</button>
			</td>
		</tr>
	</table>
	<?php echo form_close(); ?>

	<hr class="half-rule"/>

	<h2>County upload drawing rights</h2>
	<!-- <?php if (isset($success) && $success>0 && $success == "1") {?>
		<div class="alert alert-success fade in alert-dismissable">DATA UPLOADED SUCCESSFULLY</div>
	<?php } ?> -->
	<?php  $att=array("name"=>'drawing_rights_csv_form','id'=>'drawing_rights_csv_form'); echo form_open_multipart('rtk_admin/drawing_rights_csv',$att); ?>
		<table class="table table-striped">
			<tr>
				<td>
					<input type="file" name="file" id="file" required="required" class="form-control"><br>
				</td>
			</tr>
			<tr>
				<td>
					<button type="submit" class="btn btn-primary">Upload</button>
				</td>
			</tr>
		</table>
	<?php echo form_close(); ?>

	<hr class="half-rule"/>
</div>

<!-- SUBCOUNTY SIDE -->
<div class="col-md-6 clearfix margin-top">
	<h2>Subcounty new drawing right</h2>
	<p>All subcounties listed</p>

	<?php  $att=array("name"=>'drawing_rights_new_form','id'=>'drawing_rights_new_form'); echo form_open_multipart('rtk_admin/drawing_rights_master/addition/district',$att); ?>
	<table class="table table-striped">
		<tr>
			<td colspan="2">
				<select name="entity_id" class="form-control">
				<option value="">Select subcounty</option>
					<?php foreach ($districts as $key => $value) { ?>
						<option value="<?php echo $value['id']; ?>"><?php echo $value['district']; ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<input class="form-control" type="number" name="screening" placeholder="Screening new amount" />
			</td>
			<td>
				<input class="form-control" type="number" name="confirmatory" placeholder="Confirmatory new amount" />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<button class="btn btn-primary" type="submit">Add new</button>
			</td>
		</tr>
	</table>
	<?php echo form_close(); ?>

	<hr class="half-rule"/>

	<h2>Subcounty edit drawing rights</h2>
	<p>Only subcounties already in the district_drawing_rights table are displayed here. If adding a new subcounty's allocation data, use new drawing rights form</p>

	<?php  $att=array("name"=>'drawing_rights_master','id'=>'drawing_rights_edit_form'); echo form_open_multipart('rtk_admin/drawing_rights_master/update/district',$att); ?>
	<table class="table table-striped">
		<tr>
			<td colspan="2">
				<select name="entity_id" class="form-control">
					<option value="">Select subcounty</option>
					<?php foreach ($drawing_rights_districts as $key => $value) { ?>
						<option value="<?php echo $value['district_id']; ?>"><?php echo $value['district']; ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<input class="form-control" type="number" name="screening" placeholder="Screening new amount" />
			</td>
			<td>
				<input class="form-control" type="number" name="confirmatory" placeholder="Confirmatory new amount" />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<button class="btn btn-primary" type="submit">Submit update</button>
			</td>
		</tr>
	</table>
	<?php echo form_close(); ?>

	<hr class="half-rule"/>

