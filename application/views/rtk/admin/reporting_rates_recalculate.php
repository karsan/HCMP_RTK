<style type="text/css">
	.margin-top{
		margin-top: 30px!important;
	}
</style>
<div class="col-md-10 col-md-offset-1 clearfix margin-top">
	<?php if (isset($success) && $success>0 && $success == "1") {?>
		<div class="alert alert-success fade in alert-dismissable">REPORTING RATES REFRESHED SUCCESSFULLY</div>
	<?php } ?>
	<div class="col-md-12 alert alert-info">
		<h5>You do not need to select both a county and a subcounty, either one is enough.</h5>
		<h5>If county alone is selected, whole county's reporting rate data for that month will be recalculated.</h5>
		<h5>Date selection is a <strong>must</strong>. Will look into doing an annual recalculation in future.</h5>
	</div>
	<?php echo form_open('rtk_admin/reporting_rates_recalculate') ?>
		<div class="col-md-12">
			<div class="col-md-3">
				<select name="county" class="form-control">
						<option value="0">Select County</option>		
						<?php foreach ($counties as $key => $value) { ?>
							<option value="<?php echo $value['id']; ?>"><?php echo $value['county']; ?></option>
						<?php } ?>
				</select>
			</div>
			<div class="col-md-3">
				<select name="subcounty" class="form-control">
						<option value="0">Select Subcounty</option>		
						<?php foreach ($districts as $key => $value) { ?>
							<option value="<?php echo $value['id']; ?>"><?php echo $value['district']; ?></option>
						<?php } ?>
				</select>
			</div>
			<div class="col-md-3">
				<select name="date" class="form-control">
						<option value="0">Select Date</option>		
						<?php foreach ($months as $key => $value) { ?>
							<option value="<?php echo $value['yearmonth']; ?>"><?php echo $value['month_year_full']; ?></option>
						<?php } ?>
				</select>
			</div>
			<div class="col-md-3">
				<button class="btn btn-primary" type="submit">Recalculate</button>
			</div>
		</div>
	<?php echo form_close(); ?>
</div>