<style>
	.float-left{
		float:left!important;
	}
	.margin{
		margin: 2px 5px;
	}


</style>
<div class="col-md-12 ">
	<center>
	<span class="" style="font-size:15px;margin:10px;"><strong>NB: Changes via this interface have super admin privileges and are thus irreversible</strong></span>
	</center>
</div>
<div class="col-md-12">
  <?php if ($success_status == '1'): ?>
    <div class="col-md-6 col-md-offset-3 alert alert-success">
      Update has been effected <strong>successfully </strong>
    </div>
   <?php elseif($success_status == '2'): ?>
   	<div class="col-md-6 col-md-offset-3 alert alert-success">
      Deletion has been effected <strong>successfully </strong>
    </div>
  <?php endif; ?>
</div>

<div class="col-md-12">
	<?php //echo "<pre>";print_r($facilities);exit; ?>
	<table class="table table-bordered table-fluid col-md-12" id="datatable">
	<thead>
	<tr>
		<th>County</th>
		<th>Subcounty</th>
		<th>MFL</th>
		<th>Facility Name</th>
		<th>Action</th>
	</tr>
	</thead>
		<?php foreach ($facilities as $key => $value) { ?>
			<tr>
				<td><?php echo $value['county_name']; ?></td>
				<td><?php echo $value['district_name']; ?></td>
				<td><?php echo $value['facility_code']; ?></td>
				<td><?php echo $value['facility_name']; ?></td>
				<td class="col-md-2">
					<a class="btn-sm btn-primary float-left margin" href="<?php echo base_url().'rtk_admin/admin_facilities/edit/'.$value['facility_id']; ?>">Edit</a>
				
					<a class="btn-sm btn-danger float-left margin" href="<?php echo base_url().'rtk_admin/admin_facilities/delete/'.$value['facility_id']; ?>">Delete</a>

					<a class="btn-sm btn-success float-left margin" href="<?php echo base_url().'rtk_admin/admin_facilities/view/'.$value['facility_id']; ?>">View details in DB</a>
				</td>
			</tr>
		<?php } ?>
	</table>
</div>

