<style type="text/css">
	.btn{
		border-radius: 0px!important;
		width:180px!important;
	}
	.margin-top{
		margin-top: 5px!important;
	}
	.margin-vert{
		margin: 5px 0px!important;
	}
</style>
<?php //echo "<pre>";print_r($allocations);exit; ?>
<div>
  <?php if ($success_status == '1'): ?>
    <div class="col-md-6 col-md-offset-3 alert alert-success">
      <!-- Allocation has been deleted <strong>successfully </strong> -->
      Operation <strong>successful. </strong>
    </div>

  <?php endif; ?>
</div>

<div class="col-md-12">
	<p>Deletion is based on month and district ID. If multiple allocations are present in that month for a given district, they will all be removed.</p>
</div>
<div class="col-md-12">
	<table class="table table-bordered table-fluid col-md-12" id="datatable">
      <thead>
        <tr>
          <th>Reports for</th>
          <th>County</th>
          <th>District</th>
          <th>MFL&nbsp;Code</th>
          <th>Facility Name</th>
          <th>Compiled By</th>                      
          <th>Order&nbsp;Date</th>
          <th>Action</th>
      </tr>
              </thead>
              <tbody>
                <?php
                foreach ($lab_order_list as $order) {
                  $english_date = date('D dS M Y',strtotime($order['order_date']));
                  $reportmonth = date('F Y',strtotime('-1 month',strtotime($order['order_date'])));
                  // $reportmonth = date('F Y',strtotime($order['order_date']));
                 ?>
                  <tr>
                    <td><?php echo $reportmonth; ?></td>        
                    <td><?php echo $order['county']; ?></td>
                    <td><?php echo $order['district']; ?></td>
                    <td><?php echo $order['facility_code']; ?></td>
                    <td><?php echo $order['facility_name']; ?></td>
                    <td><?php echo $order['compiled_by']; ?></td>
                    <!--td><?php echo "Lab Commodities"; ?></td-->
                    <td><?php echo $english_date; ?></td>
                    <td>
                    <a href="<?php echo site_url('rtk_management/lab_order_details/' . $order['id']); ?>" class="btn btn-xs btn-primary">View</a>
                    <a href="<?php echo site_url('rtk_management/edit_lab_order_details/' . $order['id']); ?>" class="btn btn-xs btn-success">Edit</a>
                    <a href="<?php echo site_url('rtk_admin/admin_delete_fcdrr/' . $order['id']); ?>" class="btn btn-xs btn-danger">Delete All</a>
                    <a href="<?php echo site_url('rtk_admin/admin_delete_fcdrr_duplicates/' . $order['id']); ?>" class="btn btn-xs btn-danger">Delete Duplicates</a>
                    </td>
                    </tr> 
                    <?php
                }
                ?>
            </tbody>
        </table>
</div>

<script>
$(document).ready(function() {
	// $('#datatable').dataTable();
});
</script>