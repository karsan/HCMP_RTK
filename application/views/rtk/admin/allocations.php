<style type="text/css">
	.btn{
		border-radius: 0px!important;
		width:180px!important;
	}
	.margin-top{
		margin-top: 5px!important;
	}
	.margin-vert{
		margin: 5px 0px!important;
	}
</style>
<?php //echo "<pre>";print_r($allocations);exit; ?>
<div>
  <?php if ($success_status == '1'): ?>
    <div class="col-md-6 col-md-offset-3 alert alert-success">
      <!-- Allocation has been deleted <strong>successfully </strong> -->
      Operation <strong>successful. </strong>
    </div>

  <?php endif; ?>
</div>

<div class="col-md-12">
	<p>Deletion is based on month and district ID. If multiple allocations are present in that month for a given district, they will all be removed.</p>
</div>
<div class="col-md-12">
	<table class="table table-bordered table-fluid col-md-12" id="datatable">
		<thead>
			<th>Zone</th>
			<th>County</th>
			<th>District</th>
			<th>Created at</th>
			<th>Month</th>
			<th>Action</th>
		</thead>
		<tbody>
			<?php foreach ($allocations as $key => $value) { ?>
			<tr>
				<td><?php echo $value['zone']; ?></td>
				<td><?php echo $value['county']; ?></td>
				<td><?php echo $value['district']; ?></td>
				<td><?php echo $value['created_at']; ?></td>
				<td><?php echo date('F Y', strtotime($value['month'])); ?></td>
				<td>
				<a class="btn btn-sm btn-primary col-md-12 margin-vert " href="<?php echo base_url().'rtk_admin/admin_delete_allocation/'.$value['district_id'].'/'.$value['month'] ?>">Delete month</a><br>
				<a class="btn btn-sm btn-primary col-md-12 margin-vert " href="<?php echo base_url().'rtk_admin/admin_move_allocation_form/'.$value['district_id'].'/'.$value['month']; ?>">Move allocation</a><br>
				<!-- <a class="btn btn-sm btn-primary pull-left margin-top" href="<?php echo base_url().'rtk_admin/admin_move_allocation/forward/'.$value['district_id'].'/'.$value['month'] ?>"><span class="glyphicon glyphicon-arrow-right"></span> Move forward one month</a><br>
				<a class="btn btn-sm btn-primary pull-left margin-top" href="<?php echo base_url().'rtk_admin/admin_move_allocation/backward/'.$value['district_id'].'/'.$value['month'] ?>"><span class="glyphicon glyphicon-arrow-left"></span> Move allocation</a> -->
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>

<script>
$(document).ready(function() {
	/*$('#datatable').dataTable({
     "sDom": "T lfrtip",
     "aaSorting": [],
     "bJQueryUI": false,
      "bPaginate": false,
      "oLanguage": {
        "sLengthMenu": "_MENU_ Records per page",
        "sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
      },
      "oTableTools": {
      "aButtons": [ 
          "copy",
          "print",
          {
            "sExtends": "collection",
            "sButtonText": 'Save',
            "aButtons": ["csv", "xls", "pdf"]
          }     
      
      ],  
      "sSwfPath": "<?php echo base_url();?>assets/datatable/media/swf/copy_csv_xls_pdf.swf"
    }
  });*/
});
</script>