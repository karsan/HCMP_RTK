<style type="text/css">
  .panel-body,span:hover,.status_item:hover
  { 
    cursor: pointer !important; 
  }
  .panel {
    border-radius: 0;
  }
  .panel-body {
    padding: 8px;
  }

  .borderless{
    border-radius: 0px; 
  }
  .form-group{
    margin-bottom: 10px;
  }
</style>
<div class="col-md-12">
	<?php echo form_open('rtk_admin/admin_facility_edit'); ?>
	<?php //echo "<pre>";print_r($facility_data);exit; ?>
	<div class="col-md-12">
		<div class=" col-md-6 col-md-offset-3">
          <div class="form-group">
            <center>

              <fieldset class="col-md-12">
              <center>            
                <div class="col-md-12">
                  <p class="label label-info">Tables edited: <strong>lab_commodity_orders,lab_commodity_details,facilities</strong></p>
                </div>

                </br>
                <div class="input-group form-group u_mgt">
                  <span class="input-group-addon sponsor ">Facility code</span>
                  <input type="text" required="required" name="facility_code" id="facility_code" class="form-control " placeholder="Enter Facility Code" value="<?php echo $facility_data['facility_code']; ?>">
                  <input type="hidden" name="old_facility_code" value="<?php echo $facility_data['facility_code']; ?>">
                  <input type="hidden" name="facility_id" value="<?php echo $facility_data['facility_id']; ?>">
                </div>

                <div class="input-group form-group u_mgt">
                  <span class="input-group-addon sponsor ">Facility name</span>
                  <input type="text" required="required" name="facility_name" id="facility_name" class="form-control " placeholder="Enter Facility Name" value="<?php echo $facility_data['facility_name']; ?>">
                </div>

                <div class="input-group form-group u_mgt ">
                  <span class="input-group-addon sponsor">Current district</span>
                  <input type="text" required="required" name="current_district" id="current_district" disabled="true" class="form-control " placeholder="Current District" value="<?php echo $facility_data['district_name']; ?>">
                </div>

                <div class="input-group form-group u_mgt sub_county">
                  <span class="input-group-addon sponsor ">Update district</span>
                  <select class="form-control " id="district" name="district" >
                        <option value="<?php echo $facility_data['district_id']; ?>">Select update sub-county. Leave as is for no change</option>
                        <?php foreach ($districts as $key => $value) { ?>
                        <option value="<?php echo $value['district_id']; ?>"><?php echo $value['district_name']; ?></option>
                        <?php } ?>                   
                  </select>
                </div>

                <div class="">
                  <button type="submit" class="btn btn-primary col-md-6 col-md-offset-3">Save</button>
                </div>
                
                </center>
                
              </fieldset>

          </center></div>
        </div>
	</div>
	<?php echo form_close(); ?>
</div>