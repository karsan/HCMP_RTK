	<?php //echo "<pre>";print_r($allocation_list); ?>
<style type="text/css">
	.margin-top{
		margin-top: 30px!important;
	}
</style>
<div class="col-md-12 clearfix margin-top">
	<?php echo $table_html; ?>
</div>

<div class="modal fade view-details-modal" id="view-details-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width: 1000px;">
    <div class="modal-content">
    	<div class="modal-header">
        <h3 class="modal-title" id="myLargeModalLabel">DHIS Data Import</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>

      <div class="modal-body clearfix">
      	<div id="modal-body-data">
        	<!-- <i class="fa fa-cog fa-spin"></i> -->
      	</div>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div> -->
  </div>
</div>
</div>

<script>
$(document).ready(function() {
var url ='<?php echo base_url()?>';
	$('.select2').select2();

	$('#view-details-modal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var level = button.data('level') // Extract info from data-* attributes
		var level_id = button.data('level-id') // Extract info from data-* attributes
		var operation = button.data('operation') // Extract info from data-* attributes
		// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		var modal = $(this)
		console.log('rtk_admin/get_dhis_fcdrr_details/'+level+'/'+level_id+'/'+operation);
		// modal.find('.modal-title').text('New message to ' + recipient)
		// modal.find('.modal-body').html(level + " "+ level_id);
		ajax_request_replace_div_content('rtk_admin/get_dhis_fcdrr_details/'+level+'/'+level_id+'/'+operation,"#modal-body-data");

	});

	$(document).on("click", "#modal_fcdrr_details", function(event){
	    // alert( "GO" ); 
	    // console.log("CLICKED");
		// var button = $(event.relatedTarget) // Button that triggered the modal
		var level = $(this).data('level') // Extract info from data-* attributes
		var level_id = $(this).data('level-id') // Extract info from data-* attributes
		var operation = $(this).data('operation') // Extract info from data-* attributes

		var month_year = $(this).data('month-year') // Extract info from data-* attributes
		// console.log(operation);
		console.log('rtk_admin/get_dhis_fcdrr_details/'+level+'/'+level_id+'/'+operation);
		if (month_year) {
			console.log('rtk_admin/get_dhis_order_details/'+month_year+'/'+level_id);
			// ajax_request_replace_div_content('dashboardv2/get_facility_order_details/'+order_id,"#modal-body-data");
			if (operation === 'import_data_by_fcdrr') {
				console.log("IMPORTING...");
				console.log('rtk_admin/import_dhis_order/'+month_year+'/'+level_id);
				ajax_request_replace_div_content('rtk_admin/import_dhis_order/'+month_year+'/'+level_id,"#modal-body-data");
			}else{
				ajax_request_replace_div_content('rtk_admin/get_dhis_order_details/'+month_year+'/'+level_id,"#modal-body-data");
				
			}
		}else{
			ajax_request_replace_div_content('rtk_admin/get_dhis_fcdrr_details/'+level+'/'+level_id+'/'+operation,"#modal-body-data");
		}
	});


});//END OF JQUERY

function ajax_request_replace_div_content(function_url,div){
	var url ='<?php echo base_url()?>';
	var function_url =url+function_url;
	var loading_icon=url+"assets/img/loader2.gif";
	console.log(div);
	$.ajax({
		type: "POST",
		url: function_url,
		beforeSend: function() {
			$(div).html("<img style='margin-left:20%;' src="+loading_icon+">");
		},
		success: function(msg) {
			console.log(msg);
			$(div).html(msg);
		}
	});
} 
</script>