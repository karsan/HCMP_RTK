<?php //echo "<pre>";print_r($allocation_list); ?>
<style type="text/css">
  .margin-top{
    margin-top: 30px!important;
  }
</style>
<div class="col-md-12 clearfix margin-top">
  <center>
    <h4>ALL COUNTIES ALLOCATION LIST</h4>
  </center>
</div>
<div class="col-md-12 clearfix">
  <div class="col-md-9 padding-hor-sm no-margin no-padding">
    <select class="form-control select2 padding-hor-sm" id="month_filter">
    <option value="0">Select Month</option>
    <?php foreach ($filter_months as $month => $value):?> 
        <option value="<?php echo $value['month_year']; ?>"><?php echo $value['month_year_full']; ?></option>
    <?php endforeach; ?>
    </select>
  </div>
  <div class="col-md-3 no-padding no-margin">
    <button class="btn btn-primary col-md-12 top_filter_button filter_button">Filter</button>
  </div>
</div>
<div class="col-md-12 clearfix margin-top">
<table class="table table-bordered" id="allocation_list_table">
  <thead>
    <th>County</th>
    <th>Allocation Date</th>
    <th>Subcounties Allocated</th>
    <th>Subcounties Approved</th>
    <th>Allocation Status</th>
    <th>Actions</th>
  </thead>
  <tbody>
    <?php 
      foreach ($allocation_list as $key => $value) {
      foreach ($value as $keyy => $valuee) {
      // echo "<pre>";print_r($value);exit;
      ?>
      <tr>
        <td><?php echo $valuee['county_name']; ?></td>
        <td><?php echo $valuee['month_name'].' '.$valuee['month_year']; ?></td>
        <td><?php echo $valuee['allocated_districts'].'/'.$valuee['total_districts']; ?></td>
        <td><?php echo $valuee['approved_districts'].'/'.$valuee['total_districts']; ?></td>
        <td><?php echo $valuee['allocation_status']; ?></td>
        <td>
          <a class="btn btn-success" href="<?php echo base_url().'rtk_management/cmlt_allocation_list_by_month/'.$valuee['county_id'].'/'.$valuee['month_name'].'/'.$valuee['month_year'] ?>" target="_blank"><i class="glyphicon glyphicon-eye-open"></i> View/Edit</a>
          <a class="btn btn-primary" href="<?php echo base_url().'rtk_management/download_allocation_list/cmlt/'.$valuee['county_id'].'/NULL/'.$valuee['month_name'].'/'.$valuee['month_year'] ?>" target="_blank">
          <i class="glyphicon glyphicon-download"></i> Download</a>
        </td>
      </tr>
    <?php }} ?>
  </tbody>
</table>

</div>

<script>
$(function () { 
var url ='<?php echo base_url()?>';
  $('#allocation_list_table').dataTable({
     "sDom": "T lfrtip",
     "aaSorting": [],
     "bJQueryUI": false,
      "bPaginate": false,
      "oLanguage": {
        "sLengthMenu": "_MENU_ Records per page",
        "sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
      },
      "oTableTools": {
      "aButtons": [      
      
      ],  
      "sSwfPath": "<?php echo base_url();?>assets/datatable/media/swf/copy_csv_xls_pdf.swf"
    }
  });

  $(".select2").select2({
    placeholder: "0",
    // containerCssClass: "float-left" 
  });

  $('.top_filter_button').button().click(function(x){
      x.preventDefault(); 

      /*var county_text = $('#county_filter option:selected').text();
      var subcounty_text = $('#sub_county_filter option:selected').text();

      var county_id = $('#county_filter option:selected').val();
      var subcounty_id = $('#sub_county_filter option:selected').val();
      var facility_id = $('#facility_id option:selected').val();
      var quarter = $('#quarter_filter option:selected').val();
      var year = $('#year_filter option:selected').val();*/
      
      var month_year = $('#month_filter option:selected').val();

      window.location.replace(url+"rtk_management/admin_allocation_list/NULL/NULL/"+month_year);

      /*if(county_id==0){
        window.location.replace(url+"dashboardv2/county_dashboard/NULL/NULL/"+quarter+"/"+month_year+"/"+year);
      }else{
        if(subcounty_id > 0){
          window.location.replace(url+"dashboardv2/county_dashboard/NULL/"+subcounty_id+"/"+quarter+"/"+month_year+"/"+year);
        }else{
          window.location.replace(url+"dashboardv2/county_dashboard/"+county_id+"/NULL/"+quarter+"/"+month_year+"/"+year);
        }
      }*/


   });
});/*END OF JQUERY FUNCTION*/
</script>