<style type="text/css">
.select_options{
	width:14%;
	float:left;
	height:30px;
	font-size:12px;
	margin-left: 30px
}
</style>

<div class="dash_main" style=" margin-top: 10px;">

	<!-- <h3> <?php echo $banner_text;?></h3><br/> <br/> -->

	<div class="clc_contents">
<!-- <div class="accordion-group"> -->
<div class="col-md-12" style=" margin: 10px 0;">
	<div class="col-md-3 no-spacing">
		<select id="county_select" class="select2">
			<?php if ($restricted > 0): ?>
				<?php echo '<option>Current County</option>'; ?>
			else: ?>
				<?php echo '<option>All Counties</option>'; ?>
			<?php endif; ?>
		</select>
	</div>

	<div class="col-md-3 no-spacing">
		<select id="commodity_select" class="form-control flat-select" >
			<option value="1">FACS Calibur Reagents and Consumables</option>	
		</select>
	</div>

	<div class="col-md-3 no-spacing">
		<select id="date_select" class="select2" >
			<?php 
				foreach ($filter_months as $month => $value):
					$txt = date('F Y', strtotime('-1 MONTH',strtotime($value['full_date'])));
			?> 
		      <option value="<?php echo $value['monthyear']; ?>"><?php echo $txt; ?></option>
			<?php endforeach; ?>
		</select>
	</div>

	<!-- 
		<select id="fcdrr_commodity_select" class="form-control select_options" > By Commodity
		<option value="0"> -- All FCDRR Details--</option>							
		<option value="1"> Ending Balances</option>							
		<option value="2"> Quantity Used</option>							
		<option value="3"> Tests Done</option>							
		<option value="4"> AMC</option>							
		</select>&nbsp;&nbsp;&nbsp;

		<!-- <button type="button" id="btn_consumption_select" class="btn btn-primary my_navs">Download Consumption Report</button> 
	-->

	<div class="col-md-3 no-spacing">	
		<button type="button" id="update_button" class="btn btn-primary my_navs  filter_button">Filter</button>
	</div>

</div>

	<!-- <div class="accordion-group" style="width: 49%;float: left; margin-left: 5px; margin-top: 20px; height: 350px"> -->
	<div class="col-md-12">
		<div id="bycons" class="inner_divs panel panel-info">
			<div class="panel-heading accordion-heading " data-toggle=""  href="#trend_graphs" style="font-size:13px;font-weight:bold;" >Reporting Rates Trend<span id="filtered_county"></span></div>
			<div id="trend_graphs" class="accordion-body ">
				<div class="accordion-inner ">
					<div id="trend-chart" class="accordion-body "></div>
				</div>
			</div>
		</div>
	</div>

	<!-- <div class="accordion-group" style="width: 49%;float: left; margin-left: 5px; margin-top: 120px; height: 350px"> -->
	<div class="col-md-12 margin-vert">
		<div id="bycons" class="inner_divs panel panel-success">
			<div class="panel-heading accordion-heading " data-toggle=""  href="#cons_graphs" style="font-size:13px;font-weight:bold" >Monthly Reporting Summary : <span id="filter_date_summary"><?php echo date("F Y", strtotime( $reporting_summary['full_date']." -1 MONTHS")); ?></span></div>
			<div id="cons_graphs" class="accordion-body clearfix">
				<div class="accordion-inner ">
					<div class="col-md-12 margin-top">
						<div class="col-md-6">
							<div class="col-md-12">
								
							</div>
							<div class="col-md-12">
								<table class="table table-bordered">
									<tr>
										<td colspan="2">Total facilities: <strong><span id="total_sites_no"><?php echo $reporting_summary['total_facilities']; ?></span></strong></td>
										<!-- <td><button class="btn btn-success" id="" data-toggle="modal" data-target="#reported_facilities">View</button> </td> -->
									</tr>

									<tr>
										<td>Reported sites: <strong><span id="reported_sites_no"><?php echo $reporting_summary['reported']; ?></span></strong></td>
										<td><button class="btn btn-success" id="" data-toggle="modal" data-target="#reported_facilities">View</button> </td>
									</tr>

									<tr>
										<td>Non-reported sites: <strong><span id="nonreported_sites_no"><?php echo $reporting_summary['nonreported']; ?></span></strong></td>
										<td><button class="btn btn-success" id="" data-toggle="modal" data-target="#nonreported_facilities" >View</button></td>
									</tr>
								</table>
								 
								<!-- <?php echo $reporting_summary['nonreported']; ?> -->
							</div>
						</div>
						<div class="col-md-6" style="margin: 10px 0px;">
							<div id="reporting-piechart" class="section_data" style="height: 300px"> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- <div class="accordion-group" style="width: 49%;float: left; margin-left: 3px; margin-top: 20px;overflow-y: scroll;height: 350px"> -->
	<div class="col-md-12">
		<div id="bycons" class="table_divs panel panel-warning">
			<div class="panel-heading accordion-heading " data-toggle=""  data-parent="#accordion2" href="#" style="font-size:13px;font-weight:bold;" >Stock Card : <span id="filter_date_stock_card"><?php echo date("F Y", strtotime( $reporting_summary['full_date']." -1 MONTHS")); ?></span></div>
			<div id="" class="accordion-body ">
				<div class="accordion-inner margin-top">						

					<table class="table table-bordered table-sm compact" id="stock_card" style="font-size:13px;">
						<thead>
							<tr>
								<th>Commodity</th>
								<th>Kit</th>
								<th>Beginning Balance</th>
								<th>Received Quantity</th>
								<th>Used Total</th>
								<th>Total Tests</th>
								<th>Positive Adjustments</th>
								<th>Negative Adjustments</th>
								<th>Losses</th>
								<th>Closing Balance</th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>	

	<div class="modal fade" id="reported_facilities" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel"><b>Reported Facilities</b> </h4>
			</div>
			<div class="modal-body">

				<table class="table" id="reported_table">
					<thead>
						<th>County</th>
						<th>Sub County</th>
						<th>Facility Code</th>
						<th>Facility Name</th>
						<th>Date</th>
						<th>FCDRR</th>
					</thead>
					<tbody id="reported_tbody">


					</tbody>
				</table>
			</div>

		</div>
	</div>
	</div>	

	<div class="modal fade" id="nonreported_facilities" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel"><b>Non Reported Facilities</b> </h4>
				</div>
				<div class="modal-body">

					<table class="table table-bordered compact" id="nonreported_table">
						<thead>
							<th>County</th>
							<th>Sub County</th>
							<th>Facility Code</th>
							<th>Facility Name</th>
							<th>Date</th>
						</thead>
						<tbody>

						</tbody id="nonreported_tbody">
					</table>
				</div>

			</div>
		</div>
	</div>
	</div>

</div>
</div>

<script type="text/javascript">
	<?php if ($restricted > 0) {?>
		var county_id = <?php echo $default_county; ?>;
	<?php } ?>
	$(document).ready(function(e){
		$.fn.dataTable.ext.errMode = 'none'; $('#table-id').on('error.dt', function(e, settings, techNote, message) { console.log( 'An error occurred: ', message); })
		populate_stock_card();
		populate_reporting_trend();
		populate_montlhy_reporting_summary();
		populate_nonreported_list();
		populate_reported_list();

		$('#update_button').click(function(){
			populate_stock_card();
			populate_reporting_trend();
			populate_montlhy_reporting_summary();
			populate_nonreported_list();
			populate_reported_list();

		});
	function populate_montlhy_reporting_summary(){
			// var county = $('#county_select').val();
			<?php if ($restricted > 0) {?>
				var county = <?php echo $default_county; ?>
			<?php }else{ ?>
				var county = $('#county_select').val();
			<?php } ?>

			var month = $('#date_select').val();

			// console.log("populate_montlhy_reporting_summary: county: "+county);
			// console.log("populate_montlhy_reporting_summary: month: "+month);
			console.log("<?php echo base_url() . 'allocation_management/get_cd4_reporting_summary/'; ?>"+month+'/'+county);
			$.ajax({
				url: "<?php echo base_url() . 'allocation_management/get_cd4_reporting_summary/'; ?>"+month+'/'+county,
				dataType: 'json',
				success: function(s){
					console.log("populate_montlhy_reporting_summary");
					console.log(s);

					var reported = s.reported;
					var nonreported = s.nonreported;
					var total_facilities = s.total_facilities;
					var reporting_month = s.reporting_month;

					var reported_percentage = s.reported_percentage;
					var nonreported_percentage = s.nonreported_percentage;

					$('#reported_sites_no').html(reported);
					$('#nonreported_sites_no').html(nonreported);
					$('#total_sites_no').html(total_facilities);
					$('#filter_date_summary').html(reporting_month);
					$('#filter_date_stock_card').html(reporting_month);

					$('#reporting-piechart').highcharts({
						chart: {
							plotBackgroundColor: null,
							plotBorderWidth: null,
							plotShadow: false,
							type: 'pie'
						},
						credits:false,

						title: {
							text: 'Reported and Non Reported Facilities'
						},
						tooltip: {
							pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
						},
						plotOptions: {
							pie: {
								allowPointSelect: true,
								cursor: 'pointer',
								dataLabels: {
									enabled: true,
									format: '<b>{point.name}</b>: {point.percentage:.1f} %',
									style: {
										color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
									}
								}
							}
						},
						series: [{
							name: 'Facilities Reporting Status',
							colorByPoint: true,
							data: [{
								name: 'Reported Facilities',
								y: reported_percentage
							}, {
								name: 'Not Reported',
								y: nonreported_percentage,
								sliced: true,
								selected: true
							}]
						}]
					});

				},
				error: function(e){
					console.log(e.responseText);
				}            
			});
	}//end of populate_montlhy_reporting_summary()

<?php if ($restricted > 0) {?>
	var county_id = <?php echo $default_county; ?>
<?php } ?>

$.ajax({
	url: "<?php echo base_url() . "allocation_management/get_counties/".$default_county; ?>",
	dataType: 'json',
	success: function(s){
		// console.log(s);
		var countieslist = s.counties_list;
		$('#county_select').html(countieslist);

	},
	error: function(e){
		console.log(e.responseText);
	}            
}); 

$.ajax({
	url: "<?php echo base_url() . 'allocation_management/get_cd4_commodities'; ?>",
	dataType: 'json',
	success: function(s){
		console.log(s);
		var commodity_list = s.cd4_commodities;
		$('#commodity_select').html(commodity_list);

	},
	error: function(e){
		console.log(e.responseText);
	}            
});
function populate_reporting_trend(){

	<?php if ($restricted > 0) {?>
		var county = <?php echo $default_county; ?>;
	<?php }else{ ?>
		var county = $('#county_select').val();
	<?php } ?>
	// console.log("populate_reporting_trend: county: "+county);
	var url = "<?php echo base_url() . 'allocation_management/get_cd4_reporting_percentage2/'; ?>"+county;
	console.log("url: "+ url);
	$.ajax({
		url: url,
		dataType: 'json',
		success: function(s){
			console.log(s);
			var county_name = s.county_name;
			var percentages = s.percentages;
			var months_texts = s.months_texts;

			$('#trend-chart').highcharts({
				title: {
					text: 'Reporting Rates',
					x: -20 //center
				},
				subtitle: {
					text: 'Source: HCMP',
					x: -20
				},
				xAxis: {
					categories: months_texts,

				},
				credits:false,
				yAxis: {
					title: {
						text: 'Percentages'
					},
					plotLines: [{
						value: 0,
						width: 1,
						color: '#808080'
					}]
				},
				tooltip: {
					valueSuffix: '%'
				},
				series: [{
					name: 'Percentages',
					data: percentages
				}]
			});

			$('#filtered_county').html(county_name);
		},
		error: function(e){
			console.log(e.responseText);
		}            
	});
}
var oTable = $('#stock_card').dataTable({	
	"bPaginate":false,				
	"bFilter": true,
	"bSearchable":true,
	"bInfo":true
});
var reported_table = $('#reported_table').dataTable({	
	"bPaginate":false,				
	"bFilter": true,
	"bSearchable":true,
	"bInfo":true
});var nonreported_table = $('#nonreported_table').dataTable({	
	"bPaginate":false,				
	"bFilter": true,
	"bSearchable":true,
	"bInfo":true
});

var stock_card_url = '';

function populate_stock_card(){
	var commodity = $('#commodity_select').val();
	var month = $('#date_select').val();
	// var county = $('#county_select').val();
	<?php if ($restricted > 0) {?>
		var county = <?php echo $default_county; ?>;
	<?php }else{ ?>
		var county = $('#county_select').val();
	<?php } ?>
	var stock_card_url = "<?php echo base_url() . 'allocation_management/get_cd4_stock_card2/'; ?>"+commodity+'/'+month+'/'+county;
	// console.log("populate_stock_card url: "+stock_card_url);
	// alert(stock_card_url);

	console.log("stock_card_url: "+stock_card_url);

	$.ajax({

		url: stock_card_url,
		dataType: 'json',
		success: function(s){
		// console.log(s);
		oTable.fnClearTable();
		for(var i = 0; i < s.length; i++) {
			oTable.fnAddData([
				s[i][0],
				s[i][1],
				s[i][2],
				s[i][3],
				s[i][4],
				s[i][5],
				s[i][7],
				s[i][8],
				s[i][9],
				s[i][10],
				s[i][11]

				]);
		} // End For
	},
	error: function(e){
		console.log(e.responseText);
	}
	});
}

function populate_reported_list(){

	var commodity = $('#commodity_select').val();
	var month = $('#date_select').val();
	<?php if ($restricted > 0) {?>
		var county = <?php echo $default_county; ?>;
    	/*Comment to make commit work*/
	<?php }else{ ?>
		var county = $('#county_select').val();
	<?php } ?>
	var reported_url = "<?php echo base_url() . 'allocation_management/get_cd4_reporting_facility_details/'; ?>"+month+'/'+county;
	console.log("reported_url: "+reported_url);
	// alert(stock_card_url);

	$.ajax({

		url: reported_url,
		dataType: 'json',
		success: function(s){
			console.log(s);
			var reported_list = s.reported;
			var list_date = s.date_text;
			reported_table.fnClearTable();
			for(var i = 0; i < reported_list.length; i++) {

				var fcdrr_link = "<?php echo base_url() . 'cd4_management/fcdrr_details/'; ?>"+reported_list[i][4];
				var link = '<a class="btn btn-primary btn-sm" target="_blank" href="'+fcdrr_link+'"> View FCDRR</a>';
				// alert (link);
				reported_table.fnAddData([
					reported_list[i][0],
					reported_list[i][1],
					reported_list[i][2],
					reported_list[i][3],
					list_date,
					link
				// reported_list[i][4]
				]);

				/*var reported_tr = '';
				reported_tr += '<tr>';
				reported_tr += '<td>'+reported_list[i][0]+'</td>';
				reported_tr += '<td>'+reported_list[i][1]+'</td>';
				reported_tr += '<td>'+reported_list[i][2]+'</td>';
				reported_tr += '<td>'+reported_list[i][3]+'</td>';
				reported_tr += '<td>'+link+'</td>';
				reported_tr += '</tr>';

				$('#reported_tbody').append(reported_tr);*/
			} // End For
	},
	error: function(e){
		console.log(e.responseText);
	}
	});
}

function populate_nonreported_list(){

	var commodity = $('#commodity_select').val();
	var month = $('#date_select').val();
	<?php if ($restricted > 0) {?>
		var county = <?php echo $default_county; ?>;
	<?php }else{ ?>
		var county = $('#county_select').val();
	<?php } ?>
	
	var reported_url = "<?php echo base_url() . 'allocation_management/get_cd4_reporting_facility_details/'; ?>"+month+'/'+county;



	$.ajax({

		url: reported_url,
		dataType: 'json',
		success: function(s){
			console.log(s);
			var non_reported_list = s.nonreported;
			var list_date = s.date_text;
			nonreported_table.fnClearTable();
			for(var i = 0; i < non_reported_list.length; i++) {
				nonreported_table.fnAddData([
					non_reported_list[i][0],
					non_reported_list[i][1],
					non_reported_list[i][2],
					non_reported_list[i][3],
					list_date,
					]);
} // End For
},
error: function(e){
	console.log(e.responseText);

}
});
}

});

</script>