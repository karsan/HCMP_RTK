<?php //echo "<pre>";print_r($issue_list); ?>
<style type="text/css">
	.margin-top{
		margin-top: 30px!important;
	}
</style>
<div class="col-md-12 clearfix margin-top">
<table class="table table-bordered" id="issue_list_table">
	<thead>
		<th>Issue Date</th>
		<th>County</th>
		<!-- <th>Month</th> -->
		<th>Screening Kits </th>
		<!-- <th>Screening Units</th> -->
		<th>Confirmatory Kits </th>
		<!-- <th>Confirmatory Units</th> -->
	</thead>
	<tbody>
		<?php foreach ($issue_list as $key => $value) {?>
			<tr>
				<td><?php echo date('d-F-Y',strtotime($value['issue_date'])); ?></td>
				<td><?php echo $value['county']; ?></td>
				<!-- <td><?php echo $value['screening_units']; ?></td> -->
				<td><?php echo "<strong>".round($value['screening_units']/100)."</strong> (<strong><small>".round($value['screening_units'])." units</small></strong>)"; ?></td>
				<!-- <td><?php echo $value['confirmatory_units']; ?></td> -->
				<td><?php echo "<strong>".round($value['confirmatory_units']/30)."</strong> (<strong><small>".round($value['confirmatory_units'])." units</small></strong>)"; ?></td>
				<!-- <td><?php echo round($value['confirmatory_units']/30); ?></td> -->
				<!-- <td>
					<a class="btn btn-success" href="<?php echo base_url().'rtk_management/cmlt_allocation_list_by_month/'.$county_id.'/'.$value['month_name'].'/'.$value['month_year'] ?>"><i class="glyphicon glyphicon-eye-open"></i> View/Edit</a>
				</td> -->
			</tr>
		<?php } ?>
	</tbody>
</table>

</div>

<script>
	$('#issue_list_table').dataTable({
     "sDom": "T lfrtip",
     "aaSorting": [],
     "bJQueryUI": false,
      "bPaginate": false,
      "oLanguage": {
        "sLengthMenu": "_MENU_ Records per page",
        "sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
      },
      "oTableTools": {
      "aButtons": [      
      
      ],  
      "sSwfPath": "<?php echo base_url();?>assets/datatable/media/swf/copy_csv_xls_pdf.swf"
    }
  });

</script>