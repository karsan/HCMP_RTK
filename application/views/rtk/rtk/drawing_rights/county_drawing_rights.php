<?php //echo "<pre>";print_r($drawing_rights);exit; ?>
<style type="text/css">
  .margin-top{
    margin-top: 30px!important;
  }
</style>
<div class="col-md-12 clearfix margin-top">
<div class="col-md-12 alert alert-info">
 <strong>Current quarter: </strong><?php echo $cur_quarter; ?>
 <strong></strong>
 <!-- <strong>Totals and balances </strong>are calculated based on the above.    -->
 </div>
<table class="table table-bordered table-condensed table-hover table-responsive" id="datatable">
<!-- <table class="table table-bordered" id=""> -->
  <thead>
    <tr>
    <th rowspan="2">Year</th>
    <th rowspan="2">Quarters <br><small>(No. of quarters allocated this year)</small></th>
    <th colspan="3">Screening</th>
    <th colspan="3">Confirmatory</th>
    <th rowspan="2">Actions</th>
    </tr>
    <tr>
    <th>Total <br><small>[ For <?php echo $no_of_quarters ?> Quarter(s) ]</small></th>
    <!-- <th>Allocated <br><small>[ In <?php echo $no_of_quarters ?> Quarter(s) ]</small></th> -->
    <th>Used <br><small>[ In <?php echo $no_of_quarters ?> Quarter(s) ]</small></th>
    <th>Balance <br><small>[Current]</small></th>
    <!-- <th>Allowed</th> -->

    <th>Total <br><small>[ For <?php echo $no_of_quarters ?> Quarter(s) ]</small></th>
    <!-- <th>Allocated <br><small>[ In <?php echo $no_of_quarters ?> Quarter(s) ]</small></th> -->
    <th>Used <br><small>[ In <?php echo $no_of_quarters ?> Quarter(s) ]</small></th>
    <th>Balance <br><small>[Current]</small></th>
    <!-- <th>Allowed</th> -->
    </tr>
  </thead>
  <tbody>
    <?php //echo "<pre>";print_r($drawing_rights_new);exit; ?>
    <?php //foreach ($drawing_rights as $key => $value) {?>
      <!-- <tr>
        <td><?php echo $value['year']; ?></td>
        <td><?php echo $no_of_quarters; ?></td>
        <td><?php echo number_format($value['screening_total']); ?></td>
        <td><?php echo number_format($used_data['screening_used']); ?></td>
        <td><?php echo number_format($value['screening_total'] - $used_data['screening_used']); ?></td>
        <td><?php echo number_format($value['confirmatory_total']); ?></td>
        <td><?php echo number_format($used_data['confirmatory_used']); ?></td>
        <td><?php echo number_format($value['confirmatory_total'] - $used_data['confirmatory_used']); ?></td>
        <td>
          <a class="btn btn-success" href="<?php echo base_url().'rtk_management/county_drawing_rights_details/'.$county_id.'/NULL/'.$cur_quarter ?>"><i class="glyphicon glyphicon-eye-open"></i> View Distribution</a>
        </td>
      </tr> -->
    <?php //} ?>

    <tr>
      <td><?php echo $drawing_rights_new['year']; ?></td>
      <td><?php echo $drawing_rights_new['quarter']; ?></td>
      <td><?php echo number_format($drawing_rights_new['screening_total']); ?></td>
      <td><?php echo number_format($used_data_new['screening_used']); ?></td>
      <td><?php echo number_format($drawing_rights_new['screening_total'] - $used_data_new['screening_used']); ?></td>
      <td><?php echo number_format($drawing_rights_new['confirmatory_total']); ?></td>
      <td><?php echo number_format($drawing_rights_new['confirmatory_used']); ?></td>
      <td><?php echo number_format($drawing_rights_new['confirmatory_total'] - $used_data_new['confirmatory_used']); ?></td>
      <td>
        <a class="btn btn-success" href="<?php echo base_url().'rtk_management/county_drawing_rights_details/'.$county_id.'/NULL/'.$cur_quarter ?>"><i class="glyphicon glyphicon-eye-open"></i> View Distribution</a>
      </td>
    </tr>
  </tbody>
</table>

</div>
<script>
$('#datatable').dataTable({
     "sDom": "T lfrtip",
     "aaSorting": [],
     "bJQueryUI": false,
      "bPaginate": true,
      "oLanguage": {
        "sLengthMenu": "_MENU_ Records per page",
        "sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
      },
      "oTableTools": {
      "aButtons": [      
      "copy",
      "print",
      {
        "sExtends": "collection",
        "sButtonText": 'Save',
        "aButtons": ["csv", "xls", "pdf"]
      }
      ],  
      "sSwfPath": "<?php echo base_url();?>assets/datatable/media/swf/copy_csv_xls_pdf.swf"
    }
  });
</script>