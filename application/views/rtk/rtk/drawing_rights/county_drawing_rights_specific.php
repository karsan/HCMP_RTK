<?php //echo "<pre>";print_r($county_drawing_rights);exit; ?>
<style type="text/css">
  .margin-top{
    margin-top: 30px!important;
  }
  .no-margin{
    margin:0px!important;
  }
  .no-padding{
    padding:0px!important;
  }
  .borderless{
    border-radius:0px!important; 
  }
  .btn-width{
    width:100%!important;
  }
</style>
<div class="col-md-12 clearfix margin-top">
<div class="col-md-12">
<div class="col-md-12 no-padding">
  <?php if ($success_status == '1'): ?>
    <div class="col-md-12 alert alert-success">
      <small>
      Updated <strong>Successfully </strong>
      </small>
    </div>
    <?php endif; ?>

  <?php if ($success_status == '2'): ?>
    <div class="col-md-12 alert alert-danger">
      <small>
      Operation <strong>Unsuccessful. </strong>
      <p>Allocated units have gone beyond available balance</p>
      </small>
    </div>
  <?php endif; ?>

  <?php if ($success_status == '3'): ?>
    <div class="col-md-12 alert alert-info">
      <small>
      Operation <strong>Unsuccessful. </strong>
      <p>Kindly enter values to the form before submitting</p>
      </small>
    </div>
  <?php endif; ?>
<div class="col-md-8 no-padding">
  <center>
    <!-- <h3>Drawing Rights</h3> -->
    <h4>Quarter: <strong><?php echo $current_quarter.'</strong> Year: <strong> '.$current_quarter_year; ?></strong></h4>
    <!-- <p><label class="label label-info">Figures here can only be viewed once allocation has been done.To make changes, contact your System Administrator</label></p> -->
  </center>

  

</div>
<div class="col-md-4 no-padding">
  <div class="col-md-6">
    <button class="btn btn-primary btn-sm pull-right" data-county-id = "<?php echo $county_id ?>" data-toggle="modal" data-target=".view-history-modal">Allocation History </button>
  </div>
  <div class="col-md-6 no-padding">
    
    <a class="btn btn-primary btn-sm btn-width" href="<?php echo base_url().'rtk_management/update_district_drawing_rights/'.$county_id ?>"><i class="glyphicon glyphicon-refresh"></i> Refresh Drawing Rights</a>
    <?php if ($success_status == '1'): ?>
    <div class="btn-width alert alert-success">
      <small>
      Refreshed <strong>Successfully </strong>
      </small>  
    </div>
    <?php endif; ?>
  </div>
</div>

<div class="modal fade view-history-modal" id="view-history-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width: 1000px;">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="myLargeModalLabel">Allocation History</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>

      <div class="modal-body" >
        <div id="modal-body-data">
          <!-- <i class="fa fa-cog fa-spin"></i> -->
        </div>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div> -->
  </div>
</div>
</div>

</div>
<div>
  <div class="col-md-12 alert alert-info">
    <p><strong>Total: </strong> Drawing rights available to county for the quarter.</p>
    <p><strong>Distributed: </strong> Total drawing rights distributed to subcounties in this quarter.</p>
    <p><strong>Balance: </strong> Amount remaining after distribution to subcounties.</p>
    <p><strong>Used: </strong> Total amount used by subcounties.</p>
  </div>

<div class="col-md-12">
  <table class="table table-bordered table-condensed table-sm">
    <tr>
      <td></td>
      <td><strong>Screening</strong></td>
      <td><strong>Confirmatory</strong></td>
    </tr>
    <tr>
      <td><strong>Total Drawing Rights </strong></td>
      <!-- <td><?php echo number_format($county_drawing_data['screening_total']); ?></td>
      <td><?php echo number_format($county_drawing_data['confirmatory_total']); ?></td> -->

      <td><?php echo number_format($county_drawing_data_new['screening_total']); ?></td>
      <td><?php echo number_format($county_drawing_data_new['confirmatory_total']); ?></td>
    </tr>
    <!-- <tr>
      <td><strong>Total Distributed (Annual) </strong></td>
      <td><?php echo number_format($county_drawing_data_new['screening_distributed']); ?></td>
      <td><?php echo number_format($county_drawing_data_new['confirmatory_distributed']); ?></td>
    </tr> -->

    <!-- <tr>
      <td><strong>Total Distribution Balance (Annual)</strong></td>
      <td><?php echo number_format($county_drawing_data['screening_total'] - $county_drawing_data['screening_distributed']); ?></td>
      <td><?php echo number_format($county_drawing_data['confirmatory_total'] - $county_drawing_data['confirmatory_distributed']); ?></td>
    </tr> -->

    <tr>
      <td><strong>Total Used </strong></td>
      <!-- <td><?php echo number_format($used_data['screening_used']); ?></td>
      <td><?php echo number_format($used_data['confirmatory_used']); ?></td> -->

      <td><?php echo number_format($county_drawing_data_new['screening_used']); ?></td>
      <td><?php echo number_format($county_drawing_data_new['confirmatory_used']); ?></td>
    </tr>

    <tr>
      <td><strong>Total Balance </strong></td>
      <!-- <td><?php echo number_format($county_drawing_data['screening_total'] - $used_data['screening_used']); ?></td>
      <td><?php echo number_format($county_drawing_data['confirmatory_total'] - $used_data['confirmatory_used']); ?></td> -->

      <td><?php echo number_format($county_drawing_data_new['screening_total'] - $county_drawing_data_new['screening_used']); ?></td>
      <td><?php echo number_format($county_drawing_data_new['confirmatory_total'] - $county_drawing_data_new['confirmatory_used']); ?></td>
    </tr>
    
    <tr>
      <td><strong>Distributed (Current Quarter) </strong></td>
      <!-- <td><?php echo number_format($distributed_data['screening_allocated']); ?></td>
      <td><?php echo number_format($distributed_data['confirmatory_allocated']); ?></td> -->
      <td><?php echo number_format($county_drawing_data_new['screening_distributed']); ?></td>
      <td><?php echo number_format($county_drawing_data_new['confirmatory_distributed']); ?></td>
    </tr>


  </table>
</div>
  
</div>
<?php //if ($distribution_status > 0): ?>
<?php //echo "DATA PRESENT"; ?>
<table class="table table-bordered table-condensed table-hover table-responsive datatable">
  <thead>
    <tr>
    <th rowspan="2">Subcounty</th>
<!-- FORMER SETTING
    <th colspan="3">Screening</th>
    <th colspan="3">Confirmatory</th>
 -->    
    <th colspan="1">Screening</th>
    <th colspan="1">Confirmatory</th>
    <!-- <th rowspan="2">Actions</th> -->
    </tr>
    <tr>
    <!-- FORMER SETTING
    <th>Total</th>
    <th>Allocated</th>
    <th>Balance</th>
    <th>Total</th>
    <th>Allocated</th>
    <th>Balance</th>
 -->

    <th>Allocated</th>
    <th>Allocated</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($drawing_rights as $key => $value) {?>
      <tr>
        <td><?php echo $value['subcounty']; ?></td>
        <td><?php echo number_format($value['screening_total']); ?></td>
        <!-- 
        <td><?php echo $value['screening_allocated']; ?></td> 
        <td><?php echo $value['screening_balance']; ?></td>
        -->
        <td><?php echo number_format($value['confirmatory_total']); ?></td>
        <!-- 
        <td><?php echo $value['confirmatory_allocated']; ?></td> 
        <td><?php echo $value['confirmatory_balance']; ?></td>
        -->
        <!-- <td>
          <a class="btn btn-success" href="<?php echo base_url().'rtk_management/county_drawing_rights_details/'.$county_id ?>"><i class="glyphicon glyphicon-eye-open"></i> Edit Totals</a>
        </td> -->
      </tr>
    <?php } ?>
  </tbody>
</table>
<?php //else: ?>
    <center>
    <h4>Distribute for Quarter: <strong><?php echo $current_quarter.'</strong> Year: <strong> '.$current_quarter_year; ?></strong> below</h4>
    </center>
  <?php  $att=array("name"=>'drawing_rights_form','id'=>'drawing_rights_form'); echo form_open('rtk_management/save_subcounty_drawing_rights',$att); ?>
  <table class="table table-bordered table-condensed table-hover table-responsive dataTable" >
    <thead>
      <th>Subcounty</th>
      <th>Screening amount</th>
      <th>Confirmatory amount</th>
    </thead>
    <tbody>

    <?php for ($i=0; $i < $subcounty_count; $i++) { ?>
      <tr>
        <td>
          <input type="hidden" name="quarter" value="<?php echo $current_quarter; ?>">
          <input type="hidden" name="quarter_year" value="<?php echo $current_quarter_year; ?>">
          <input type="hidden" name="quarter_month" value="<?php echo $current_quarter_month; ?>">

          <input type="hidden" name="screening_allowed" value="<?php echo $county_drawing_data_new['screening_total'] - $county_drawing_data_new['screening_used']; ?>">
          <input type="hidden" name="confirmatory_allowed" value="<?php echo $county_drawing_data_new['confirmatory_total'] - $county_drawing_data_new['confirmatory_used']; ?>">

          <input type="hidden" name="<?php echo "subcounty_id[$i]"; ?>" value="<?php echo $subcounties[$i]['id']; ?>">
          <input type="hidden" name="<?php echo "county_id[$i]"; ?>" value="<?php echo $subcounties[$i]['county']; ?>">

          <?php echo $subcounties[$i]['district']; ?>
        </td>
        <td>
          <input class="form-control" type="number" name="<?php echo "screening_allocated[$i]"; ?>" placeholder="Screening amount">
        </td>
        <td>
          <input class="form-control" type="number" name="<?php echo "confirmatory_allocated[$i]"; ?>" placeholder="Confirmatory amount">
        </td>
      </tr>
    <?php } ?>
    </tbody>
  </table>

  <input type="submit" class="btn btn-primary btn-large" value="Save Drawing Rights">
  <?php echo form_close(); ?>
<?php //endif ?>
</div>
<script>
$('.datatable').dataTable();
var url ='<?php echo base_url()?>';
  
  var county_id = <?php echo $county_id; ?>;
$('#view-history-modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var modal = $(this);
    console.log("CALLED");
    // modal.find('.modal-title').text('New message to ' + recipient)
    // modal.find('.modal-body').html(level + " "+ level_id);
    ajax_request_replace_div_content('rtk_management/get_allocation_history/'+county_id,"#modal-body-data");

  })

function ajax_request_replace_div_content(function_url,div){
  var function_url =url+function_url;
  var loading_icon=url+"assets/img/loader2.gif";
  $.ajax({
    type: "POST",
    url: function_url,
    beforeSend: function() {
      $(div).html("<img style='margin-left:20%;' src="+loading_icon+">");
    },
    success: function(msg) {
      // console.log(msg);
      $(div).html(msg);
      $('#datatable').DataTable(); 
    }
  });
} 

</script>