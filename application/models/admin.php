<?php
/*
*
*Karsan 2017
*
*/
class Admin extends Doctrine_Record {
	public function setTableDefinition() {
		
	}

	public function get_facility_data($facility_id = NULL)
	{	
		$append = (isset($facility_id) && $facility_id>0)?" AND f.id = $facility_id":NULL;
		$query = "
		SELECT 
			d.id AS district_id,
		    d.district AS district_name,
			c.id AS county_id,
		    c.county AS county_name,
		    f.id AS facility_id,
		    f.*
		FROM
		    facilities f,districts d,counties c
		WHERE 
		f.district = d.id AND d.county = c.id $append
		";

		$result = $this->db->query($query)->result_array();

		return $result;
	}

	public function get_district_data($district_id = NULL)
	{
		$append = (isset($district_id) && $district_id>0)?" AND d.id = $district_id":NULL;
		$query = "
		SELECT 
		    d.id AS district_id,
		    d.district AS district_name,
		    c.id,c.county
		FROM
		    districts d,counties c
		WHERE d.county = c.id $append
		";

		$result = $this->db->query($query)->result_array();

		return $result;
	}

}