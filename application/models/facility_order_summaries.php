<?php
/**
 * @author Karsan
 */
class Facility_order_summaries extends Doctrine_Record {
   function __construct() {
        parent::__construct(); 
        
    }

    public function get_facility_order_data_count($county_id = NULL, $district_id = NULL, $facility_code = NULL, $commodity_id = NULL, $quarter = NULL,$month = NULL, $year = NULL)
    {
		
		// echo "<pre>";print_r($result);exit;
		$result = $this->get_facility_order_data($county_id,$district_id,$facility_code,$commodity_id,$quarter,$month,$year);
		$count = count($result);

		return $count;
	
	}

	public function get_facility_order_data($county_id = NULL, $district_id = NULL, $facility_code = NULL, $commodity_id = NULL, $quarter = NULL,$month = NULL, $year = NULL,$limit = NULL,$start = NULL,$col = NULL, $dir = NULL,$search = NULL)
    {
    	// echo $search;exit;
		// echo "QUARTER: ".$quarter;exit;
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;
		$county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
		$district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
		$facility_code = (isset($facility_code) && $facility_code!='')? $facility_code:NULL;
		$year = (isset($year) && is_numeric($year) && $year>0)? $year:NULL;
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:NULL;

		$start = (isset($start) && is_numeric($start) && $start>0)? $start:NULL;
		$limit = (isset($limit) && is_numeric($limit) && $limit>0)? $limit:10;
		// echo $start." ".$limit;exit;
		$new_criteria = "";
		$limit_append = $order_by = NULL;
		
		if ($limit > 0 && $start > 0) {
			$limit_append .= " LIMIT $start,$limit";
		}else{
			$limit_append .= " LIMIT $limit";
		}

		if ($col > 0 && $dir > 0) {
			$order_by .= " ORDER BY $col,$dir";
		}

		if (isset($search) && $search != '') {
			if (is_numeric($search) && $search > 0) {
				$new_criteria .= " AND l.facility_code LIKE '%$search%' ";
			}elseif (is_string($search)) {
				$new_criteria .= " AND f.facility_name LIKE '%$search%' ";
			}
		}
		// echo $limit_append;exit;
		if ($quarter > 0 && $month < 1) {
			$year = date('Y');
        	switch ($quarter) {
        		case 1:
        			// echo "Quarter: One";exit;
        			$first_month = "01";
        			$last_month = "03";
        			$firstdate = $year . '-' . $first_month . '-01';
	        		$lastdate = $year . '-' . $last_month . '-31';
        			break;
        		case 2:
        			// echo "Quarter: Two";exit;
        			$first_month = "04";
        			$last_month = "06";
        			$firstdate = $year . '-' . $first_month . '-01';
	        		$lastdate = $year . '-' . $last_month . '-31';
        			break;
        		case 3:
        			// echo "Quarter: Three";exit;
        			$first_month = "07";
        			$last_month = "09";
        			$firstdate = $year . '-' . $first_month . '-01';
	        		$lastdate = $year . '-' . $last_month . '-31';
        			break;
        		case 4:
        			// echo "Quarter: Four";exit;
        			$first_month = "10";
        			$last_month = "12";
        			$firstdate = $year . '-' . $first_month . '-01';
	        		$lastdate = $year . '-' . $last_month . '-31';
        			break;
        		
        		default:
        			// echo "Quarter: Invalid";exit;
        			break;
        	}
        	$new_criteria .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
        }elseif (isset($month) && $month > 0) {
            $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');

            $firstdate = $year . '-' . $month . '-01';
	       	$lastdate = $year . '-' . $month . '-31';
        	$new_criteria .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
        }else{
        	$new_criteria .= ($year > 0)?" AND YEAR(l.created_at) = $year":"AND YEAR(lco.order_date) = ".date('Y');
        }

		$year_ = date('Y');

		$new_criteria .= ($district_id > 0)?" AND d.id = $district_id":NULL;
        $new_criteria .= ($county_id > 0)?" AND c.id = $county_id":NULL;
        $new_criteria .= ($facility_code > 0)?" AND f.facility_code = $facility_code":NULL;

		$c_ids = (isset($commodity_id) && $commodity_id!='')? "'".$commodity_id."'" : "4,5";

		$group_by = " GROUP BY commodity_id,f.facility_code";
		// $limit = "  LIMIT 1000";

		// echo $limit_append;exit;
		$query="
			SELECT 
			    c.id AS county_id,
			    c.county,
			    d.id AS district_id,
				d.district,
				f.facility_code,
				f.facility_name,
			    l.district_id,
			    l.commodity_id,
			    lco.order_date,
			    SUM(l.beginning_bal) AS beginning_bal,
			    SUM(l.q_received) AS q_received,
			    SUM(l.q_used) AS q_used,
			    SUM(l.no_of_tests_done) AS no_of_tests_done,
			    SUM(l.closing_stock) AS closing_stock,
			    SUM(l.q_expiring) AS q_expiring,
			    SUM(l.q_requested) AS q_requested,
			    SUM(l.positive_adj) AS positive_adj,
			    SUM(l.negative_adj) AS negative_adj,
			    SUM(l.losses) AS losses,
			    l.created_at
			FROM
			    lab_commodity_details l USE INDEX (order_id,facility_code,district),
			    counties c,
			    districts d,
			    facilities f,
			    lab_commodity_orders lco USE INDEX (district)
			WHERE
			    c.id = d.county
			        AND l.order_id = lco.id
			        AND l.facility_code > 0
			        AND l.facility_code = f.facility_code
			        AND lco.district_id = d.id
			        AND l.commodity_id IN ($c_ids)
			        
			        $new_criteria
					$group_by $limit_append $order_by;        
			";
		// echo "<pre>";print_r($query);exit;

		$result = $this->db->query($query)->result_array();
		// echo "<pre>";print_r($result);exit;
		// $count = count($result);

		return $result;
	
	}
   
}
