<?php
class Cd4_Lab_Commodity_Categories extends Doctrine_Record {

	public function setTableDefinition() {
		$this -> hasColumn('id','int');
		$this -> hasColumn('name','varchar', 50);
		$this -> hasColumn('equipment_id','int');
	}
 	public function setUp() {
		$this -> setTableName('cd4_lab_commodity_categories');
		$this -> hasMany('cd4_commodities as commodities', array('local' => 'id', 'foreign' => 'category'));
	}
	public static function get_all() {
		$query = Doctrine_Query::create() -> select("*") -> from("cd4_lab_commodity_categories") -> orderBy("id");
		$categories = $query -> execute();
		return $categories;
	}

	public static function get_active() {
			$query = Doctrine_Query::create() -> select("*") -> from("cd4_lab_commodity_categories") ->where("active='1'")-> orderBy("id");
			$raw = $query->getSQL();
			// echo "<pre>";print_r($raw);exit;
			$categories = $query -> execute()->toArray();
			// echo "<pre>";print_r($categories);exit;
			return $categories;
	}

	public static function get_active_new($devices) {
		// echo "<pre>";print_r($devices);exit;
        $query = Doctrine_Query::create() 
        -> select("*")
        -> from("cd4_lab_commodity_categories") 
        ->where("active='1'")
        ->andWhere('equipment_id IN ('.implode(',', $devices).')')
        -> orderBy("id");
			$raw = $query->getSQL();
			// echo "<pre>";print_r($raw);exit;
			$categories = $query -> execute()->toArray();
			// echo "<pre>";print_r($categories);exit;
			return $categories;
	}

	public static function get_cd4_commodities($categories) {
		// echo "<pre>";print_r($devices);exit;
        $query = Doctrine_Query::create() 
        -> select("*")
        -> from("cd4_commodities") 
        -> where('category IN (4, '.implode(',', $categories).')')
        -> orderBy("id");
			$raw = $query->getSQL();
			echo "<pre>";print_r($raw);exit;
			$categories = $query -> execute()->toArray();
			// echo "<pre>";print_r($categories);exit;
			return $categories;
	}

	public static function get_active_in_facility() {
			$query = Doctrine_Query::create() -> select("*") -> from("cd4_lab_commodity_categories") ->where("active='1'")-> orderBy("id");
			$categories = $query -> execute();
			return $categories;
	}
 

}
?>