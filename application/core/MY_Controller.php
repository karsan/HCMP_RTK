<?php

class  MY_Controller  extends  CI_Controller  {

    function __construct(){
        parent::__construct(); 

        ini_set('memory_limit', '-1');
        
        /*error_reporting(1);
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);*/
        
        // $cache_delete = $this->output->delete_cache();
    }

    public function under_maintenance()
    {
    	// return $this->load->view('general/under_maintenance');
    }

    public function echo_to_console($data) {
        $output = $data;
        // if (is_array($output))
        // $output = implode( ',', $output);
        $output = json_encode($output,TRUE);
        echo "<script>console.log(".$output.");</script>";
    }

    public function clear_cache()
    {   
        // echo FCPATH.'application/cache/';exit;
        $deletion = delete_files(FCPATH.'application/cache/');
        echo "CACHE CLEARING STATUS: ".$deletion;
    }

    public function memcached_save($data,$expiration = NULL)
    {   
        // echo "<pre>";print_r($data);exit;
        $expiration = (isset($expiration) && $expiration > 0)?$expiration:86400;
        $mem = new Memcached();
        $mem->addServer("127.0.0.1", 11211);
        // echo "<pre>";print_r($mem);exit;
        $save = $mem->set($data['key'], $data['data'],$expiration);
        // echo "<pre>";print_r($save);exit;
        return $save;
    }

    public function memcached_get($key)
    {
        $mem = new Memcached();
        $mem->addServer("127.0.0.1", 11211);
        // echo "<pre>";print_r($key);exit;
        if ($res = $mem->get($key)) {
            $result['key'] = $key;
            $result['data'] = $res;
            $result['status'] = "SUCCESS";
            $result['message'] = "KEY FOUND";
        }else{
            $result['key'] = $key;
            $result['data'] = "NULL";
            $result['status'] = "FAILED";
            $result['message'] = "KEY NOT FOUND";
        }

        return $result;
    }

    public function memcached_check($key)
    {
        $cache_key = $this->generate_memcached_key($key);
        $cache_check = $this->memcached_get($cache_key);

        return $cache_check;
    }

    public function memcached_get_all_keys()
    {
        $mem = new Memcached();
        $mem->addServer("127.0.0.1", 11211);
        // echo "<pre>";print_r($key);exit;
        $result = $mem->getAllKeys();
        // echo "<pre>";print_r($result);exit;

        return $result;
    }

    public function memcached_delete($key)
    {
        $mem = new Memcached();
        $mem->addServer("127.0.0.1", 11211);
        // echo "<pre>";print_r($key);exit;
        if ($res = $mem->delete($key)) {
            $result['key'] = $key;
            $result['status'] = "DELETED";
            $result['message'] = "KEY DELETED";
        }else{
            $result['key'] = $key;
            $result['data'] = "NULL";
            $result['status'] = "FAILED";
            $result['message'] = "KEY NOT DELETED";
        }

        echo "<pre>";print_r($result);exit;
        // return $result;
    }

    public function memcached_flush()
    {
        $mem = new Memcached();
        $mem->addServer("127.0.0.1", 11211);
        // echo "<pre>";print_r($key);exit;
        if ($res = $mem->flush()) {
            $result['status'] = "MEMCACHED FLUSHED";
        }else{
            $result['status'] = "FAILED";
        }

        echo "<pre>";print_r($result);exit;
    }

    public function generate_memcached_key($key)
    {
        // $encoded_string = crypt($key, "memcached");
        $encoded_string = md5($key);
        // $encoded_string = $this->encrypt->encode($key);
        return $encoded_string;
    }

    public function get_year_quarter($month = NULL,$quarter = NULL,$year = NULL,$all = NULL){
        $current_month = date('m');
        $current_year = date('Y');
        $year = (isset($year) && is_numeric($year) && $year>0)? $year:$current_year;
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:$current_month;
        $all = (isset($all) && is_numeric($all) && $all>0)? $all:NULL;
        $year_quarter = NULL;
        $final = array();
        // echo "month: ".$month;exit;
        // echo "quarter: ".$quarter;exit;
        $final = $final_ = array();
        if (isset($all) && $all >0) {
            for ($i=1; $i < 5; $i++) { 
                $final_['quarter'] = $i;
                $final_['year'] = $year;
                $final_['quarter_year'] = $i."_".$year;
                $final_['quarter_text'] = "Quarter ".$i." ".$year;
                array_push($final, $final_);
            }


        }else{
            if (isset($quarter) && $quarter >0) {
                $year_quarter = $quarter;
                if ($quarter == 1) {
                    $months = array('1','2','3');
                }elseif ($quarter == 2) {
                    $months = array('4','5','6');
                }elseif ($quarter == 3) {
                    $months = array('7','8','9');
                }elseif ($quarter == 4) {
                    $months = array('10','11','12');
                }else {            
                    $months = "Invalid quarter";
                }
            }else{
                if ($month <= 3) {
                    $year_quarter = 1;
                    $months = array('1','2','3');
                }elseif ($month <=6 && $month > 3) {
                    $year_quarter = 2;
                    $months = array('4','5','6');
                }elseif ($month <=9 && $month > 6) {
                    $year_quarter = 3;
                    $months = array('7','8','9');
                }elseif ($month > 9) {
                    $year_quarter = 4;
                    $months = array('10','11','12');
                }else {            
                    $year_quarter = "Invalid Month";
                }
            }
            $final['quarter'] = $year_quarter;
            $final['months'] = $months;
            $final['year'] = $year;
        }
        
        // echo "<pre>";print_r($final);exit;
        return $final;
        
    }

    public function _national_reports_sum($year, $month){
        $returnable = array();

        $firstdate = $year . '-' . $month . '-01';
        $firstday = date("Y-m-d", strtotime("$firstdate Month "));

        // $month = date("m", strtotime("$firstdate  Month "));
        // $year = date("Y", strtotime("$firstdate  Month "));
        $num_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $lastdate = $year . '-' . $month . '-' . $num_days;

        $sql = "SELECT
        SUM(CASE
        WHEN lab_commodity_details.days_out_of_stock > 0 THEN 1
        WHEN lab_commodity_details.days_out_of_stock <= 0 THEN 0
        END) AS sum_days,
        counties.county,
        counties.id,
        lab_commodities.commodity_name,
        SUM(lab_commodity_details.beginning_bal) AS sum_opening,
        SUM(lab_commodity_details.q_received) AS sum_received,
        SUM(lab_commodity_details.q_used) AS sum_used,
        SUM(lab_commodity_details.no_of_tests_done) AS sum_tests,
        SUM(lab_commodity_details.positive_adj) AS sum_positive,
        SUM(lab_commodity_details.negative_adj) AS sum_negative,
        SUM(lab_commodity_details.losses) AS sum_losses,
        SUM(lab_commodity_details.closing_stock) AS sum_closing_bal,
        SUM(lab_commodity_details.q_requested) AS sum_requested,
        SUM(lab_commodity_details.q_expiring) AS sum_expiring
        FROM
        lab_commodity_details,
        facilities,
        districts,
        counties,
        lab_commodities
        WHERE
        lab_commodity_details.created_at BETWEEN '$firstdate' AND '$lastdate'
        AND lab_commodity_details.facility_code = facilities.facility_code
        AND facilities.district = districts.id
        AND districts.county = counties.id
        and lab_commodity_details.commodity_id = lab_commodities.id";
        //         and lab_commodities.id between 0 and 6
        // group by counties.id,lab_commodities.id";
        //$returnable = $this->db->query($sql)->result_array();
        $sql4 = $sql . " AND lab_commodities.id = 4 Group By counties.county";
        $res3 = $this->db->query($sql4)->result_array();
        array_push($returnable, $res3);

        $sql5 = $sql . " AND lab_commodities.id = 5 Group By counties.county";
        $res4 = $this->db->query($sql5)->result_array();
        array_push($returnable, $res4);

        $sql6 = $sql . " AND lab_commodities.id = 6 Group By counties.county";
        $res5 = $this->db->query($sql6)->result_array();
        array_push($returnable, $res5);
        // echo $sql4; die;
        // echo "<pre>";print_r($returnable);die;
        return $returnable;
    }

    public function update_county_drawing_rights($county_id = NULL,$quarter = NULL,$year = NULL, $month = NULL){
        // echo "<pre>Update running for county_id: ".$county_id." quarter: ".$quarter." month: ".$month." year: ".$year;
        // exit;

        $c_where .= (isset($county_id) && $county_id > 0) ? " WHERE id = $county_id" : NULL;
        
        $c_query = "SELECT * FROM counties $c_where";

        $c_data = $this->db->query($c_query)->result_array();
        // echo "<pre>";print_r($c_data);exit;
        $final_data = $final_data_ = array();

        foreach ($c_data as $county => $county_val) {
            $c_id_ = $county_val['id'];

            if ($quarter < 1) {
            for ($q=1; $q < 5; $q++) { 
                    // echo $q;
                    $final_data[$q]= $this->update_county_drawing_rights_($c_id_, $q, $month,$year);
                }    
            }else{
                // echo '<pre>'.$c_id_.' '.$quarter.' '.$month.' '.$year;
                $final_data[$quarter]= $this->update_county_drawing_rights_($c_id_, $quarter, $month,$year);
            }

            // echo "<pre>";print_r($final_data);exit;
            $quarter_balances = array();
            foreach ($final_data as $key => $value) {
                // echo "<pre>";print_r($value);exit;
                // echo "<pre>";print_r($key);exit;
                $final_c_data = $value['final_c_data'];
                // echo "<pre>FINAL_C_DATA: ";print_r($final_c_data);
                // exit;
                $final_quarter_data = $value['final_quarter_data'];
                // echo "<pre>FINAL QUARTER DATA: ";print_r($final_quarter_data);
                // exit;
                foreach ($final_c_data as $key => $value) {
                    // echo "<pre>";print_r($value);exit;
                    $c_id = $value['county_id'];
                    $c_data_quarter = $value['quarter'];
                    $c_data_month = $value['month'];
                    $c_data_yr = $value['year'];

                    $c_data_quarter_prev = $this->get_previous_and_next_quarter($c_data_quarter);
                    // echo "<pre>";print_r($c_data_quarter_prev);exit;

                    $prevQuarter = $c_data_quarter_prev['previous_quarter'];
                    $nextQuarter = $c_data_quarter_prev['next_quarter'];
                    $nextQuarterYr = $c_data_quarter_prev['next_year'];
                    $prevQuarterYr = $c_data_quarter_prev['previous_year'];
                    $prevQuarterMonth = date('m',strtotime($c_data_quarter_prev['previous_last_date']));

                    $c_data_quarter_prev = $c_data_quarter_prev['previous_quarter'];
                    $c_data_yr_prev = $value['year'] - 1;

                    $qr_total_s = $final_quarter_data[0]['screening_quarter_total'];
                    $qr_total_c = $final_quarter_data[0]['confirmatory_quarter_total'];
                    // echo "<pre>";print_r($qr_total_s.' '.$qr_total_c);exit;

                    $used_data = $this->get_county_used_total($c_id,$c_data_quarter,$c_data_yr);
                    // echo "<pre>";print_r($used_data);exit;
                    // echo "<pre>";print_r($quarter_balance);exit;
                    /*DOC: 
                    The function below checks if entry exists and if not, computes and enters it. Does the math and adds previous quarter balance to quarter total and inserts.
                    */
                    $drawing_rights_data = $this->get_county_drawing_rights_details($county_id,$c_data_yr,$c_data_quarter,$c_data_month);

                    // echo "<pre>";print_r($drawing_rights_data);
                    // exit;

                    $prev_bal_scr = $quarter_balance[$c_id][$c_data_yr][$c_data_quarter_prev]['scr'];
                    $prev_bal_conf = $quarter_balance[$c_id][$c_data_yr][$c_data_quarter_prev]['conf'];
                    $prev_bal_scr = (isset($prev_bal_scr) && is_numeric($prev_bal_scr))?$prev_bal_scr:0;
                    $prev_bal_conf = (isset($prev_bal_conf) && is_numeric($prev_bal_conf))?$prev_bal_conf:0;
                    
                    /*
                        INFORMATION ON BELOW IF/ELSE LIMITERS.
                        In 2017, 
                        Q1 allocation was done outside RTK.
                        The remaining 3 were done in April, June and September.
                        Reasons, elections and logistical issues.
                        Two in April and June (Q2 & Q3), One in September (Q4).
                    */

                    // echo "<pre>";print_r($final_quarter_data);
                    // exit;
                    // echo "<pre>value: ";print_r($value);
                    // exit;
                    // echo "<pre>drawing_rights_data: ";print_r($drawing_rights_data);exit;
                    if ($c_data_quarter == 4 && $c_data_yr == 2017) {
                        $scr_total = $prev_bal_scr;
                        $conf_total = $prev_bal_conf;
                    }else{
                        // $scr_total = $final_quarter_data[0]['screening_quarter_total'] + $prev_bal_scr;
                        // $conf_total = $final_quarter_data[0]['confirmatory_quarter_total'] + $prev_bal_conf;
                        /*
                        DOC: Moved calculations of balances to function get_county_drawing_rights_details()
                        */
                        $scr_total = $drawing_rights_data['screening_total'];
                        $conf_total = $drawing_rights_data['confirmatory_total'];
                    }

                    if ($c_data_quarter == 2 && $c_data_yr == 2017) {
                        $scr_total = $qr_total_s * 2;
                        $conf_total = $qr_total_c * 2;
                    }
                    
                    $scr_bal = $scr_total - $used_data['screening_used'];
                    // echo "<pre>scr_bal: ";print_r($scr_bal);exit;
                    $conf_bal = $conf_total - $used_data['confirmatory_used'];
                    $scr_used = $used_data['screening_used'];
                    $conf_used = $used_data['confirmatory_used'];
                    
                    if ($c_data_quarter == 1 && $c_data_yr == 2017) {
                        $scr_used = $conf_used = 0;
                        // $scr_total = $conf_total = 0;
                        $scr_bal = $conf_bal = 0;
                    }

                    // echo "scr_total: ".$scr_total." conf_total: ".$conf_total;exit;
                    $f_data['screening_total'] = $scr_total;
                    $f_data['confirmatory_total'] = $conf_total;
                    $f_data['screening_used'] = $scr_used;
                    $f_data['confirmatory_used'] = $conf_used;

                    $f_data['screening_balance'] = $scr_bal;
                    $f_data['confirmatory_balance'] = $conf_bal;
                    // echo "<pre>f_data: ";print_r($f_data);exit;
                    $quarter_balance[$c_id][$c_data_yr][$c_data_quarter]['scr'] = $scr_bal;
                    $quarter_balance[$c_id][$c_data_yr][$c_data_quarter]['conf'] = $conf_bal;
                    // echo "<pre>QUARTER BALANCE: ";print_r($quarter_balance);
                    // echo "<pre>";print_r($c_data_quarter_prev);exit;
                    

                    // echo "<pre>";print_r($quarter_balance);exit;
                    // echo "<pre>f_data: ";print_r($f_data);exit;
                    $f_data_final = array_merge($value,$f_data);
                    // echo "<pre>FINAL INSERT DATA: ";print_r($f_data_final);
                    // exit;
                    if ($c_data_quarter == 1 && $c_data_yr == 2017) {
                        $scr_used = $conf_used = 0;
                        $scr_total = $conf_total = 0;
                        
                        // $f_data_final['screening_total'] = 0;
                        // $f_data_final['confirmatory_total'] = 0;

                        $f_data_final['screening_balance'] = 0;
                        $f_data_final['confirmatory_balance'] = 0;

                        $f_data_final['screening_used'] = 0;
                        $f_data_final['confirmatory_used'] = 0;
                        $f_data_final['screening_distributed'] = 0;
                        $f_data_final['confirmatory_distributed'] = 0;
                    }

                    if ($c_data_quarter == 2 && $c_data_yr == 2017) {
                        $scr_total = $qr_total_s * 2;
                        $conf_total = $qr_total_c * 2;

                        $f_data_final['screening_total'] = $scr_total;
                        $f_data_final['confirmatory_total'] = $conf_total;
                    }

                    $where_array = array(
                        'county_id' => $c_id, 
                        'quarter' => $c_data_quarter,
                        'month' => $c_data_month,
                        'year' => $c_data_yr,
                    );

                    // echo "<pre>FINAL INSERT DATA: ";print_r($f_data_final);
                    // echo "<pre>";print_r($where_array);exit;
                    $q = $this->db->where($where_array);
                    $q = $this->db->get_where('county_drawing_rights_details',$where_array)->result_array();
                    // echo "<pre>";print_r($q);exit;
                    // echo "<pre>";print_r($allocation_final[$key]);exit;
                    // echo "<pre>f_data_final: ";print_r($f_data_final);exit;
                    unset($f_data_final['screening_total']);
                    unset($f_data_final['confirmatory_total']);
                    unset($f_data_final['screening_balance']);
                    unset($f_data_final['confirmatory_balance']);

                    // echo "<pre>COUNTY_ID: ";print_r($county_id);
                    // echo "<pre>count GET WHERE: ";print_r(count($q));
                    // echo "<pre>f_data_final: ";print_r($f_data_final);exit;
                    // echo "<pre>";print_r($q);exit;
                    if (count($q)>0) {
                        $q = $this->db->where($where_array);
                        $update = $this->db->update('county_drawing_rights_details',$f_data_final);
                        // echo "<pre>";print_r($update);
                    } else {
                        $insert = $this->db->insert('county_drawing_rights_details',$f_data_final);   
                    }//end of count $q if                   
                }//end of final_c_data foreach

            }//end of final_data foreach
            
                // echo "<pre>";print_r($county_val);exit;
            $duplicates_rm = $this->drawing_rights_rm_duplicates();

            // echo '<pre>'.$quarter.' '.$month.' '.$year;exit;
            $c_details_q = "
            SELECT
            county_id,
            SUM(screening_distributed) as screening_distributed,
            SUM(confirmatory_distributed) as confirmatory_distributed
            FROM
            county_drawing_rights_details
            WHERE county_id = $c_id_ AND year = '$year'";
            // echo "<pre>";print_r($c_details_q);exit;
            $c_details_result = $this->db->query($c_details_q)->result_array();
            // echo "<pre>";print_r($c_details_result);exit;
            
            $final_data_['county_id'] = $c_id_;
            $screening_distributed = $c_details_result[0]['screening_distributed'];
            $confirmatory_distributed = $c_details_result[0]['confirmatory_distributed'];

            $final_data_['screening_distributed'] = $screening_distributed;
            $final_data_['confirmatory_distributed'] = $confirmatory_distributed;
            // echo "<pre>";print_r($c_details_result);exit;
            // echo "<pre>";print_r($final_data_);exit;
            $drawing_details_q = "SELECT * FROM county_drawing_rights WHERE county_id = $c_id_";
            // echo "<pre>";print_r($drawing_details_q);exit;
            $drawing_details = $this->db->query($drawing_details_q)->result_array();

            // echo "<pre>";print_r($drawing_details);exit;
            $county_total_scr = $drawing_details[0]['screening_total'];
            $county_total_conf = $drawing_details[0]['confirmatory_total'];
            
            // echo "<pre>";print_r($county_total_scr);
            // echo "<pre>";print_r($county_total_conf);exit;

            // $current = date('Y-m-d');
            $curMonth = date('m');
            $curQuarter = ceil($curMonth/3);
            // echo "<pre>";print_r($curQuarter);exit;
            $final_data_['quarter'] = $curQuarter;
            $final_data_['screening_allowed_current'] = ($county_total_scr * $curQuarter) - $screening_distributed;
            $final_data_['confirmatory_allowed_current'] = ($county_total_conf * $curQuarter) - $confirmatory_distributed;

            array_push($final_data, $final_data_);
            // echo "<pre>";print_r($final_data_);exit;
        }

        // echo "<pre>";print_r($final_data);exit;

        foreach ($final_data as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $county_id = $value['county_id'];
            $q = $this->db->where('county_id',$county_id);
            $q = $this->db->get('county_drawing_rights')->result_array();
            // echo "<pre>";print_r($q);exit;

            // echo "<pre>";print_r($final_data[$key]);exit;
            if (count($q) > 0) {
                $q = $this->db->where('county_id',$county_id);
                $update = $this->db->update('county_drawing_rights',$final_data[$key]);
                // echo "<pre>";print_r($update);exit;
                // echo "<pre>";print_r($f_data_final);exit;
                // echo "<pre>COUNTY ID: ".$county_id." UPDATE STATUS: ".$update;
                // echo "<pre>SCREENING DISTRIBUTED: ";print_r($final_data[$key]['screening_distributed']);
                // echo "<pre>CONFIRMATORY DISTRIBUTED: ";print_r($final_data[$key]['confirmatory_distributed']);
            } else {
                // $insert = $this->db->insert('district_drawing_rights',$final_quarter_data[$key]);   
            }
        }//end of final_quarter_data foreach

            // echo "<pre>";print_r($q);exit;

        echo "<pre>UPDATE DATA: ";print_r($f_data_final);
        // echo "<pre>";print_r($allocation_final);exit;
        echo "<pre>Tables updated successfully: county_drawing_rights, county_drawing_rights_details";
    }

    public function update_county_drawing_rights_($county_id = NULL,$quarter = NULL,$month = NULL,$year = NULL){
        // echo "<pre>";print_r($month);exit;
        $quarter_data = $this->get_year_quarter($month,$quarter,$year);
        // echo "<pre>";print_r($quarter_data);exit;
        // $districts = $this->db->query("SELECT * FROM districts WHERE county = $county_id")->result_array();
        // echo "<pre>";print_r($districts);exit;
        $county_id = (isset($county_id) && $county_id > 0) ? $county_id : NULL;

        $c_where .= (isset($county_id) && $county_id > 0) ? " AND c.id = $county_id" : NULL;
        
        $c_query = "SELECT c.id as county_id, c.*,cd.* FROM counties c,county_drawing_rights cd WHERE c.id = cd.county_id AND cd.county_id > 0 $c_where";

        $c_data = $this->db->query($c_query)->result_array();

        // echo "<pre>";print_r($c_data);exit;
        $and_data = '';
        
        // echo $and_data;exit;
        $year = $quarter_data['year'];
        $months = $quarter_data['months'];
        $quarter = $quarter_data['quarter'];

        $months_string = implode(',', $months);
        $current_month = date('m');
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:$current_month;
        
        // echo $month;exit;
        // echo "<pre>";print_r($months_string);exit;

        // echo "<pre>";print_r($total_used_data);exit;
        $final_c_data = $final_c_data_ = $final_result = array();
        $final_quarter_data = $final_quarter_data_ = array();

        foreach ($c_data as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $c_id = $value['county_id'];

            $final_c_data_['county_id'] = $c_id;
            $final_c_data_['quarter'] = $quarter;

            $final_quarter_data_['county_id'] = $c_id;
            $final_quarter_data_['quarter'] = $quarter;

            $and_data .= (isset($c_id) && $c_id > 0) ? " AND a.county_id = $c_id" : NULL;

            $scr_quarter_total = $conf_quarter_total = 0;

            foreach ($months as $keyy => $valuee) {
                // echo "<pre>";print_r($valuee);exit;
                $for_month = $valuee;
                $final_c_data_['month'] = $for_month;
                $final_c_data_['year'] = $year;
                // echo "<pre>";print_r($for_month);exit;
                $date_ = date('Ym',strtotime($year.'-'.$for_month.'-01'));
                // echo "<pre>";print_r($date_);exit;
                $query1 = "
                    SELECT 
                    c.zone as   zone,
                    c.county as county_name,
                    d.district,
                    f.facility_name,
                    f.facility_code,
                    a.allocate_s,
                    ROUND((a.allocate_s/100)) as screening_kits,
                    a.allocate_c,
                    ROUND((a.allocate_c/30)) as confirmatory_kits
                    FROM
                    allocation_details a,
                    districts d,
                    counties c,
                    facilities f
                    WHERE
                    d.id = a.district_id AND c.id = d.county
                    AND d.id = f.district
                    AND f.facility_code = a.facility_code
                    AND c.id = $c_id 
                    AND MONTH(a.month) = $for_month AND YEAR(a.month) = $year
                    GROUP BY a.facility_code,d.id
                ";

                // echo "<pre>";print_r($query1);exit;
                $result_1 = $this->db->query($query1)->result_array();
                // echo "<pre>";print_r($result_1);exit;
                $county_screening_total = $county_confirmatory_total = 0;
                $c_scr_total = $c_conf_total = 0;
                
                // echo "<pre>";print_r($result_1);exit;
                // array_push($final_result,$result_1);
                $final_result[$date_] = $result_1;
                foreach ($result_1 as $fac => $fac_val) {
                    // echo "<pre>";print_r($fac_val);exit;
                    $c_scr_total += $fac_val['allocate_s'];
                    $c_conf_total += $fac_val['allocate_c'];
                }//end of county facilities for_month each

                // echo "<pre>";print_r($c_scr_total);
                // echo "<pre>";print_r($c_conf_total);exit;
                $scr_quarter_total += $c_scr_total;
                $conf_quarter_total += $c_conf_total;

                $final_c_data_['screening_distributed'] = $c_scr_total;
                $final_c_data_['confirmatory_distributed'] = $c_conf_total;
                array_push($final_c_data, $final_c_data_);

                $drawing_rights_data = $this->get_county_drawing_rights_details($county_id,$year,$quarter,$for_month);

            }//end of months foreach

            // echo "<pre>";print_r($final_c_data);exit;
            // $total_used_data = $this->get_county_used_total($county_id);
            // echo "<pre>";print_r($total_used_data);exit;
            // echo "<pre>";print_r($quarter);exit;
            $used_data = $this->get_county_used_total($county_id,$quarter,$year);
            // echo "<pre>";print_r($used_data);exit;
            

            // echo "<pre>";print_r($drawing_rights_data);
            // exit;

            // $final_quarter_data_['screening_distributed'] = $scr_quarter_total;
            // $final_quarter_data_['confirmatory_distributed'] = $conf_quarter_total;
            
            $final_quarter_data_['screening_distributed'] = $drawing_rights_data['screening_distributed'];
            $final_quarter_data_['confirmatory_distributed'] = $drawing_rights_data['confirmatory_distributed'];
            
            $final_quarter_data_['screening_used'] = $used_data['screening_used'];
            $final_quarter_data_['confirmatory_used'] = $used_data['confirmatory_used'];

            $final_quarter_data_['screening_quarter_total'] = ceil($value['screening_total']);
            $final_quarter_data_['confirmatory_quarter_total'] = ceil($value['confirmatory_total']);

            // $final_quarter_data_['screening_total'] = ceil($value['screening_total'] * $quarter);
            // $final_quarter_data_['confirmatory_total'] = ceil($value['confirmatory_total'] * $quarter);

            $final_quarter_data_['screening_total'] = ceil($drawing_rights_data['screening_total']);
            $final_quarter_data_['confirmatory_total'] = ceil($drawing_rights_data['confirmatory_total']);

            // $final_quarter_data_['screening_balance'] = ceil(($value['screening_total'] * $quarter) - $used_data['screening_used']);
            // $final_quarter_data_['confirmatory_balance'] = ceil(($value['confirmatory_total'] * $quarter) - $used_data['confirmatory_used']);

            $final_quarter_data_['screening_balance'] = ceil($drawing_rights_data['screening_balance']);
            $final_quarter_data_['confirmatory_balance'] = ceil($drawing_rights_data['confirmatory_balance']);

            // $final_quarter_data_['screening_quarter_balance'] = ceil($value['screening_total'] - $used_data['screening_used']);
            // $final_quarter_data_['confirmatory_quarter_balance'] = ceil($value['confirmatory_total'] - $used_data['confirmatory_used']);

            // echo "<pre>";print_r($final_quarter_data_);exit;
            array_push($final_quarter_data, $final_quarter_data_);
        }//end of c_data foreach

        // echo "<pre>";print_r($final_result);exit;
        // echo "<pre>";print_r($final_quarter_data);exit;
        // echo "<pre>";print_r($final_c_data);exit;
        
        $data['final_c_data'] = $final_c_data;
        $data['final_quarter_data'] = $final_quarter_data;
        $data['final_result'] = $final_result;

        return $data;
    }

    public function get_county_drawing_rights_details($county_id,$year = NULL,$quarter = NULL, $month = NULL)
    {
        // echo "<pre>";print_r($month);exit;
        // echo "<pre>";print_r($county_id);exit;
        $quarter_borders = $this->get_previous_and_next_quarter($quarter,$year);
        // echo "<pre>";print_r($quarter_borders);exit;

        $prevQuarter = $quarter_borders['previous_quarter'];
        $nextQuarter = $quarter_borders['next_quarter'];
        $nextQuarterYr = $quarter_borders['next_year'];
        $prevQuarterYr = $quarter_borders['previous_year'];
        $prevQuarterMonth = date('m',strtotime($quarter_borders['previous_last_date']));

        $c_data_query = "SELECT * FROM county_drawing_rights_details WHERE county_id = $county_id AND quarter = $quarter AND year = $year AND month = $month GROUP BY county_id";
        // echo "<pre>";print_r($c_data_query);
        // exit;
        $c_data = $this->db->query($c_data_query);
        // echo "<pre>";print_r($c_data);exit;
        if ($c_data == false) {
            // echo "<pre>";print_r($c_data_query);
            // echo "<pre>";print_r($c_data);exit;
        }

        $reinsert = $recalculate = 0;
        if ($c_data->num_rows() > 0) {
            $c_data = $c_data->result_array();
            // echo "<pre>";print_r($c_data);exit;

            if ($c_data[0]['screening_total'] < 1) {
                $reinsert = 1;//initial insert requires recalculation
                $recalculate = 1;
            }else{
                $drawing_rights_data = $c_data[0];
            }
            // echo "<pre>";print_r($reinsert);exit;
        }else{//if data is not present
            $c_data_query = "SELECT * FROM county_drawing_rights_details WHERE county_id = $county_id AND quarter = $prevQuarter AND year = $prevQuarterYr AND month = $prevQuarterMonth GROUP BY county_id";
            // echo "<pre>";print_r($c_data_query);exit;
            $c_data = $this->db->query($c_data_query)->result_array();
            // echo "<pre>";print_r($c_data);exit;
            $c_q = "SELECT * FROM county_drawing_rights WHERE county_id = $county_id";
            $c_q_res = $this->db->query($c_q)->result_array();
            // echo "<pre>";print_r($c_q_res);exit;
            $quarter_rights_s = $c_q_res[0]['screening_total'];
            $quarter_rights_c = $c_q_res[0]['confirmatory_total'];
            // echo "<pre>";print_r($c_data);exit;
            $county_drawing_data['id'] = $c_data[0]['id'];
            $county_drawing_data['county_id'] = $c_data[0]['county_id'];
            
            // $county_drawing_data['quarter'] = $c_data[0]['quarter'];
            // $county_drawing_data['month'] = $c_data[0]['month'];
            // $county_drawing_data['year'] = $c_data[0]['year'];
            
            $county_drawing_data['quarter'] = $quarter;
            $county_drawing_data['month'] = $month;
            $county_drawing_data['year'] = $year;

            $county_drawing_data['screening_total'] = ceil($quarter_rights_s + $c_data[0]['screening_balance']);

            $used_data = $this->get_county_used_total($county_id,$quarter,$year);

            // $county_drawing_data['screening_used'] = $c_data[0]['screening_used'];
            // $county_drawing_data['screening_balance'] = $c_data[0]['screening_balance'];

            $county_drawing_data['screening_used'] = $used_data['screening_used'];
            // echo "<pre>";print_r($county_drawing_data);exit;

            $county_drawing_data['screening_balance'] = ceil($county_drawing_data['screening_total'] - $county_drawing_data['screening_used']);
            $county_drawing_data['screening_distributed'] = 0;

            $county_drawing_data['confirmatory_total'] = ceil($quarter_rights_c + $c_data[0]['confirmatory_balance']);
            $county_drawing_data['confirmatory_used'] = $used_data['confirmatory_used'];
            // $county_drawing_data['confirmatory_balance'] = $c_data[0]['confirmatory_balance'];
            $county_drawing_data['confirmatory_balance'] = ceil($county_drawing_data['confirmatory_total'] - $county_drawing_data['confirmatory_used']);


            $county_drawing_data['confirmatory_distributed'] = $c_data[0]['confirmatory_distributed'];
            $county_drawing_data['created_at'] = $c_data[0]['created_at'];
            $county_drawing_data['updated_at'] = $c_data[0]['updated_at'];
            // echo "<pre>";print_r($county_drawing_data);exit;

            $details = array(
                'county_id' =>$county_id , 
                'quarter' =>$quarter , 
                'month' =>$month , 
                'year' =>$year , 
                'screening_total' =>$county_drawing_data['screening_total'] , 
                'screening_used' =>$used_data['screening_used'] , 
                'screening_balance' =>$county_drawing_data['screening_balance'] , 
                'screening_distributed' =>$county_drawing_data['screening_distributed'] , 
                'confirmatory_total' =>$county_drawing_data['confirmatory_total'] , 
                'confirmatory_used' =>$used_data['confirmatory_used'] , 
                'confirmatory_balance' =>$county_drawing_data['confirmatory_balance'] , 
                'confirmatory_distributed' =>$county_drawing_data['confirmatory_distributed'] , 
            );


            // echo "<pre>";print_r($details);exit;
            $drawing_rights_data = $county_drawing_data;
            if ($county_id > 0) {
                $ins = $this->db->insert('county_drawing_rights_details',$details);
                // echo "<pre>INSERT DATA: ";print_r($details);
            }else{
                // echo "<pre>IGNORED DATA: ";print_r($details);

            }
        }

        // echo "<pre>";print_r($reinsert);exit;

        if ($reinsert > 0){
            $c_data_query = "SELECT * FROM county_drawing_rights_details WHERE county_id = $county_id AND quarter = $prevQuarter AND year = $prevQuarterYr AND month = $prevQuarterMonth GROUP BY county_id";
            // echo "<pre>";print_r($c_data_query);exit;
            $c_data = $this->db->query($c_data_query)->result_array();
            // echo "<pre>";print_r($c_data);exit;
            $c_q = "SELECT * FROM county_drawing_rights WHERE county_id = $county_id";
            $c_q_res = $this->db->query($c_q)->result_array();
            // echo "<pre>";print_r($c_q_res);exit;
            $quarter_rights_s = $c_q_res[0]['screening_total'];
            $quarter_rights_c = $c_q_res[0]['confirmatory_total'];
            // echo "<pre>";print_r($c_data);exit;
            $county_drawing_data['id'] = $c_data[0]['id'];
            $county_drawing_data['county_id'] = $c_data[0]['county_id'];
            
            // $county_drawing_data['quarter'] = $c_data[0]['quarter'];
            // $county_drawing_data['month'] = $c_data[0]['month'];
            // $county_drawing_data['year'] = $c_data[0]['year'];
            
            $county_drawing_data['quarter'] = $quarter;
            $county_drawing_data['month'] = $month;
            $county_drawing_data['year'] = $year;

            $county_drawing_data['screening_total'] = ceil($quarter_rights_s + $c_data[0]['screening_balance']);
            $county_drawing_data['screening_used'] = $c_data[0]['screening_used'];
            $county_drawing_data['screening_balance'] = $c_data[0]['screening_balance'];
            // $county_drawing_data['screening_distributed'] = $c_data[0]['screening_distributed'];
            $county_drawing_data['screening_distributed'] = 0;


            $county_drawing_data['confirmatory_total'] = ceil($quarter_rights_c + $c_data[0]['confirmatory_balance']);
            $county_drawing_data['confirmatory_used'] = $c_data[0]['confirmatory_used'];
            $county_drawing_data['confirmatory_balance'] = $c_data[0]['confirmatory_balance'];
            $county_drawing_data['confirmatory_distributed'] = $c_data[0]['confirmatory_distributed'];
            $county_drawing_data['created_at'] = $c_data[0]['created_at'];
            $county_drawing_data['updated_at'] = $c_data[0]['updated_at'];
            // echo "<pre>";print_r($county_drawing_data);exit;

            $details = array(
                'county_id' =>$county_id , 
                'quarter' =>$quarter , 
                'month' =>$month , 
                'year' =>$year , 
                'screening_total' =>$county_drawing_data['screening_total'] , 
                'screening_used' =>$used_data['screening_used'] , 
                'screening_balance' =>$county_drawing_data['screening_balance'] , 
                'screening_distributed' =>$county_drawing_data['screening_distributed'] , 
                'confirmatory_total' =>$county_drawing_data['confirmatory_total'] , 
                'confirmatory_used' =>$used_data['confirmatory_used'] , 
                'confirmatory_balance' =>$county_drawing_data['confirmatory_balance'] , 
                'confirmatory_distributed' =>$county_drawing_data['confirmatory_distributed'] , 
            );
            // echo "<pre>";print_r($details);exit;
            $where_array = array(
                'county_id' => $county_id, 
                'quarter' => $quarter,
                'month' => $month,
                'year' => $year,
            );
            
            // $q = $this->db->get_where('county_drawing_rights_details',$where_array)->result_array();
            // echo "<pre>GET WHERE: ";print_r($q);
            // echo "<pre>UPDATE ROW: ";print_r($q);exit;
            // echo "<pre>WHERE ARRAY: ";print_r($where_array);
            // echo "<pre>UPDATE DETAILS: ";print_r($details);
            // $q = $this->db->where($where_array);
            // $update = $this->db->update('county_drawing_rights_details',$details,$where_array);
            $update_query = "
            UPDATE county_drawing_rights_details SET 
            `screening_total`='".$details['screening_total']."', 
            `screening_used`='".$details['screening_used']."', 
            `screening_balance`='".$details['screening_balance']."', 
            `screening_distributed`='".$details['screening_distributed']."', 
            `confirmatory_used`='".$details['confirmatory_used']."', 
            `confirmatory_balance`='".$details['confirmatory_balance']."', 
            `confirmatory_distributed`='".$details['confirmatory_distributed']."' 
            WHERE county_id='$county_id'
            AND quarter='$quarter'
            AND month='$month'
            AND year='$year'
            ";
            // echo "<pre>";print_r($update_query);exit;
            $update = $this->db->query($update_query);
            // echo "<pre>UPDATE: ";print_r($update);
            $drawing_rights_data = $county_drawing_data;
        }

        // echo "<pre>drawing_rights_data: ";print_r($drawing_rights_data);
        return $drawing_rights_data;
    }

    public function drawing_rights_rm_duplicates($county_id = NULL)
    {
        $duplicates_q = "
        DELETE r1 FROM county_drawing_rights_details r1,
        county_drawing_rights_details r2 
        WHERE
        r1.id > r2.id
        AND r1.county_id = r2.county_id
        AND r1.month = r2.month
        AND r1.quarter = r2.quarter
        AND r1.year = r2.year";

        $duplicates_q_res = $this->db->query($duplicates_q);
    }

    public function get_allocated_data($county_id = NULL, $district_id = NULL, $month = NULL, $year = NULL, $quarter = NULL){
        // echo "<pre>".$county_id." ".$district_id." ".$month." ".$year." ".$quarter;exit;
        // $county_id = $this->session->userdata('county_id');
        $county_id = (isset($county_id) && $county_id > 0) ? $county_id : NULL;
        $district_id = (isset($district_id) && $district_id > 0) ? $district_id : NULL;
        $month = (isset($month) && $month!='')? $month:date('m');
        $year = (isset($year) && $year>0)? $year:date('Y');
        // echo "<pre>";print_r($year);exit;
        if ($quarter > 0) {
            // $year = date('Y');
            switch ($quarter) {
                case 1:
                    // echo "Quarter: One";exit;
                $first_month = "01";
                $last_month = "03";
                $first_date = $year . '-' . $first_month . '-01';
                $last_date = $year . '-' . $last_month . '-31';
                break;
                case 2:
                    // echo "Quarter: Two";exit;
                $first_month = "04";
                $last_month = "06";
                $first_date = $year . '-' . $first_month . '-01';
                $last_date = $year . '-' . $last_month . '-31';
                break;
                case 3:
                    // echo "Quarter: Three";exit;
                $first_month = "07";
                $last_month = "09";
                $first_date = $year . '-' . $first_month . '-01';
                $last_date = $year . '-' . $last_month . '-31';
                break;
                case 4:
                    // echo "Quarter: Four";exit;
                $first_month = "10";
                $last_month = "12";
                $first_date = $year . '-' . $first_month . '-01';
                $last_date = $year . '-' . $last_month . '-31';
                break;
                
                default:
                    // echo "Quarter: Invalid";exit;
                break;
            }
            $new_criteria .= " AND lco.order_date BETWEEN '$first_date' AND '$last_date'"; 
        }else{
            $new_criteria .= ($year > 0)?" AND YEAR(l.created_at) = $year":NULL;

            if (is_numeric($month)) { 
                $month = $month;
            }
            else{
                $month = date('m');
                
            }

            $first_date = $year . '-' . $month . '-01';
            $last_date = $year . '-' . $month . '-31';  
        }

        // echo $first_date." ".$last_date;exit;
        // echo "<pre>";print_r($month);exit;

        $criteria = '';
        $criteria .= ($district_id > 0)?" AND districts.id = $district_id":NULL;

        $new_criteria = ($district_id > 0)?" AND d.id = $district_id":NULL;
        $new_criteria .= ($county_id > 0)?" AND c.id = $county_id":NULL;
        // $new_criteria .= ($month > 0)?" AND a.month = $month ":NULL;
        $new_criteria .= " AND a.month BETWEEN '$first_date' AND '$last_date'";

        $new_query = "
        SELECT 
        c.id as   county_id,
        c.county as county_name,
        d.district,
        f.facility_name,
        f.facility_code,
        a.allocate_s,
        ROUND((a.allocate_s/100)) as screening_kits,
        a.allocate_c,
        ROUND((a.allocate_c/30)) as confirmatory_kits,
        a.month as month
        FROM
        allocation_details a,
        districts d,
        counties c,
        facilities f
        WHERE d.id = a.district_id AND c.id = d.county
        AND d.id = f.district
        AND f.facility_code = a.facility_code
        $new_criteria
        GROUP BY a.facility_code,d.id
        ";

        // echo $new_query;exit;
        $result = $this->db->query($new_query)->result_array();
        // echo "<pre>";print_r($result);exit;
        return $result;
    }

    public function update_county_distributed($county_id = NULL, $month = NULL, $year = NULL){
        $q = "SELECT * FROM counties";
        $counties = $this->db->query($q)->result_array();

        $allocated_data = $allocated_data_final = array();
        foreach ($counties as $key => $value) {
            $c_id = $value['id'];
            $county_name = $value['county'];
            $total_allocate_s = $total_allocate_c = $alloc_s = $alloc_c = 0;
            for ($i=1; $i < 11; $i++) { 
                $month_ =  date("m", strtotime( date( 'Y-m-01' )." -$i months"));
                $year_ =  date("Y", strtotime( date( 'Y-m-01' )." -$i months"));
                $month_name =  date("F", strtotime( date( 'Y-m-01' )." -$i months"));
                $allocated_data_ = $this->get_allocated_data($c_id, NULL,$month_,$year_);
                // echo "<pre>";print_r($allocated_data_);exit;

                foreach ($allocated_data_ as $keyy => $valuee) {
                     $alloc_data = array();
                     $county_id_ = $valuee['county_id_'];
                     $alloc_s += $valuee['allocate_s'];
                     $alloc_c += $valuee['allocate_c'];
                }

            }
                // echo "<pre>";print_r($allocated_data);exit;

                // echo "<pre>";print_r($alloc_s);exit;
         $alloc_data['county_id'] = $c_id;
         $alloc_data['county'] = $county_name;
         $alloc_data['screening_distributed'] = $alloc_s;
         $alloc_data['confirmatory_distributed'] = $alloc_c;
         $alloc_data['month'] = $month_;
         $alloc_data['month_name'] = $month_name;

         array_push($allocated_data, $alloc_data);
            // echo "<pre>";print_r($allocated_data);exit;
        }

        // echo "<pre>";print_r($allocated_data);exit;

        foreach ($allocated_data as $keys => $values) {
                // echo "<pre>";print_r($values);exit;
            $screening_distributed = $confirmatory_distributed = NULL;
            $cid = $values['county_id'];
            $screening_distributed = $values['screening_distributed'];
            $confirmatory_distributed = $values['confirmatory_distributed'];

            $update_q = "
                UPDATE `county_drawing_rights` 
                SET 
                `screening_distributed`='$screening_distributed', 
                `confirmatory_distributed`='$confirmatory_distributed' 
                WHERE `county_id`='$cid';
            ";
            $update = $this->db->query($update_q);
        }

        echo "COUNTRY DISTRIBUTED/ALLOCATED VALUES SUCCESSFULLY UPDATED";
    }

    public function update_county_used($county_id = NULL, $quarter = NULL, $year = NULL, $month = NULL){
        // echo "<pre>";print_r($county_id);exit;
        $c_where .= (isset($county_id) && $county_id > 0) ? " WHERE id = $county_id" : NULL;
        
        $q = "SELECT * FROM counties $c_where";
        $counties = $this->db->query($q)->result_array();
        // echo "<pre>";print_r($counties);exit;
        $allocated_data = $allocated_data_final = array();
        foreach ($counties as $key => $value) {
            $c_id = $value['id'];
            $county_name = $value['county'];

            $used_data = $this->get_county_used_total($c_id,$quarter,$year);
            // echo "<pre>";print_r($used_data);exit;
            $total_allocate_s = $total_allocate_c = $alloc_s = $alloc_c = 0;

            $alloc_data['county_id'] = $c_id;
            $alloc_data['quarter'] = $quarter;
            $alloc_data['year'] = $year;
            $alloc_data['screening_used'] = $used_data['screening_used'];
            $alloc_data['confirmatory_used'] = $used_data['confirmatory_used'];

         array_push($allocated_data, $alloc_data);
            // echo "<pre>";print_r($allocated_data);exit;
        }

        // echo "<pre>";print_r($allocated_data);exit;

        foreach ($allocated_data as $keys => $values) {
                // echo "<pre>";print_r($values);exit;
            $cid = $values['county_id'];
            $qr = $values['quarter'];
            $yr = $values['year'];
            $screening_used = $values['screening_used'];
            $confirmatory_used = $values['confirmatory_used'];

            $update_q = "
                UPDATE `county_drawing_rights` 
                SET 
                `screening_used`='$screening_used', 
                `confirmatory_used`='$confirmatory_used' 
                WHERE `county_id`='$cid' AND year = $yr AND quarter = $qr;
            ";
            $update = $this->db->query($update_q);

            $update_q_2 = "
                UPDATE `county_drawing_rights_details` 
                SET 
                `screening_used`='$screening_used', 
                `confirmatory_used`='$confirmatory_used' 
                WHERE `county_id`='$cid' AND year = $yr AND quarter = $qr;
            ";
            $update_2 = $this->db->query($update_q_2);
        }

        echo "COUNTY USED VALUES SUCCESSFULLY UPDATED";
    }    

    public function run_defined_sql($id=NULL){
        /*$sq1 = "
        DELETE FROM `rtk`.`cd4_facility_device` WHERE `id`='110'
        ";
        $result = $this->db->query($sq1);
        echo "<pre>QUERY 1 STATUS: ";print_r($result);*/

        $sq1_2 = "
        INSERT INTO `rtk`.`cd4_facility_device` (`facility_code`, `device`, `enabled`) VALUES ('13668', '2','1')
        ";
        $result_2 = $this->db->query($sq1_2);

        echo "<pre>QUERY ".$sq1_2." 2 STATUS: ";print_r($result_2);
    }

    public function get_district_data($county_id = NULL, $district_id = NULL){

        $returnable = array();
        $and = "";
            // echo "<pre>";print_r($firstdate);
            // echo "<pre>";print_r($lastdate);exit;

        $and .= (isset($district_id) && $district_id!='')? " AND d.id = $district_id" :NULL; 
        $and .= (isset($county_id) && $county_id!='')? " AND c.id = $county_id" :NULL; 

            // $and .= (isset($district_id) && $district_id!='')? " AND districts.id = $district_id" :NULL;

        $sql = "SELECT 
        c.id AS county_id, c.county AS county_name, d.*
        FROM
        counties c,
        districts d
        WHERE
        d.county = c.id $and";

            // echo $sql;exit;
        $result = $this->db->query($sql);

        if (count($result)>0) {
            $result = $result->result_array();
        }
        return $result;

    }

    public function get_facility_data($county_id = NULL, $district_id = NULL, $facility_code = NULL){
        $returnable = array();
        $and = "";
            // echo "<pre>";print_r($firstdate);
            // echo "<pre>";print_r($lastdate);exit;

        $and .= (isset($district_id) && $district_id!='')? " AND d.id = $district_id" :NULL; 
        $and .= (isset($county_id) && $county_id!='')? " AND c.id = $county_id" :NULL; 
        $and .= (isset($facility_code) && $facility_code!='')? " AND f.facility_code = $facility_code" :NULL; 

            // $and .= (isset($district_id) && $district_id!='')? " AND districts.id = $district_id" :NULL;

        $sql = "SELECT 
        c.id AS county_id,
        c.county AS county_name,
        d.id AS district_id,
        d.district district_name,
        f.facility_code,
        f.facility_name
        FROM
        counties c,
        districts d,
        facilities f
        WHERE
        d.county = c.id AND f.district = d.id $and";

            // echo $sql;exit;
        $result = $this->db->query($sql);

        if (count($result)>0) {
            $result = $result->result_array();
        }
        return $result;

    }

    public function update_q2_q3_drawing_rights($county_id = NULL){
        $q2_update = $this->update_county_drawing_rights(NULL,2);
            // echo "<pre>";print_r($q2_update);exit;
        $query = "
        SELECT 
        county_id,
        SUM(screening_allocated) as screening_allocated,
        SUM(confirmatory_allocated) as confirmatory_allocated
        FROM
        district_drawing_rights
        WHERE county_id > 0
        GROUP BY county_id";

        $result = $this->db->query($query)->result_array();

        foreach ($result as $key => $value) {
                // echo "<pre>";print_r($value);exit;
            $county_id = $value['county_id'];
            $scr_alloc = $value['screening_allocated'];
            $conf_alloc = $value['confirmatory_allocated'];

            $q = $this->db->where('county_id',$county_id);
            $q = $this->db->get('county_drawing_rights')->result_array();
                // echo "<pre>";print_r($q);exit;
            $curr_total_s = $q[0]['screening_distributed'];
            $curr_total_c = $q[0]['confirmatory_distributed'];
                // echo "<pre>";print_r($curr_total_s);
                // echo "<pre>";print_r($curr_total_c);exit;
            $tot_s = $curr_total_s + $scr_alloc;
            $tot_c = $curr_total_c + $conf_alloc;

            $final_data['county_id'] = $county_id;
            $final_data['screening_distributed'] = $tot_s;
            $final_data['confirmatory_distributed'] = $tot_c;


            if (count($q)>0) {
                $q = $this->db->where('county_id',$county_id);
                $update = $this->db->update('county_drawing_rights',$final_data);
                    // echo "<pre>";print_r($update);exit;
                echo "<pre>COUNTY ID: ".$county_id." UPDATE STATUS: ".$update;
                echo "<pre>SCREENING DISTRIBUTED: ";print_r($final_data['screening_distributed']);
                echo "<pre>CONFIRMATORY DISTRIBUTED: ";print_r($final_data['confirmatory_distributed']);
            } else {
                    $insert = $this->db->insert('district_drawing_rights',$final_quarter_data[$key]);   
            }
            }//end of final_quarter_data foreach
    }

    public function get_county_used_total($county_id = NULL,$quarter = NULL,$year = NULL){
        $excluded_facilities = '13023,15204';//KNH AND MTRH
        $criteria = '';
        $criteria .= ($county_id > 0) ? "AND c.id = $county_id" : NULL;
        // $criteria .= ($district_id>0)? "AND d.id = $district_id AND a.district_id = d.id":NULL;

        if ($quarter > 0) {
            switch ($quarter) {
                case 1:
                    // echo "Quarter: One";exit;
                $year = (isset($year) && $year>0)? $year:date('Y');
                $first_month = "01";
                $last_month = "03";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $criteria .= " AND a.month BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                case 2:
                    // echo "Quarter: Two";exit;
                $year = (isset($year) && $year>0)? $year:date('Y');
                $first_month = "04";
                $last_month = "06";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $criteria .= " AND a.month BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                case 3:
                    // echo "Quarter: Three";exit;
                $year = (isset($year) && $year>0)? $year:date('Y');
                $first_month = "07";
                $last_month = "09";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $criteria .= " AND a.month BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                case 4:
                    // echo "Quarter: Four";exit;
                $year = (isset($year) && $year>0)? $year:date('Y');
                $first_month = "10";
                $last_month = "12";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $criteria .= " AND a.month BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                
                default:
                    // echo "Quarter: Invalid";exit;
                break;
            }
        }else{
            $criteria .= ($year > 0)?" AND YEAR(a.month) = $year ":" AND YEAR(a.month) = ".date('Y');
        }

        $months_query = "
        SELECT
        a.month,
        a.created_at
        FROM
        allocation_details a,districts d,counties c, facilities f
        WHERE
        a.district_id = d.id  AND d.county = c.id AND f.district = d.id
        $criteria
        GROUP BY month(month),YEAR(month)";

        // echo $months_query;exit;
        $months = $this->db->query($months_query)->result_array();

        $county_total = $county_total_ = array();

        $month_total = $month_total_ = 0;
        foreach ($months as $month_ => $month_val) {
            // echo "<pre>";print_r($month_val);exit;
            $month = date('m',strtotime($month_val['month']));
            $year = date('Y',strtotime($month_val['month']));
            // echo "<pre>";print_r($m);exit;

            if (is_numeric($month)) { $month = $month; }
            else{
                $monthNum = $month;
                $dateObj = DateTime::createFromFormat('!F', $monthNum);
                $month = $dateObj->format('m');         // March
                $month_name = $dateObj->format('F');         // March
            }
            


            $criteria = '';
            $criteria .= ($district_id > 0)?" AND districts.id = $district_id":NULL;

            $first_date = $year . '-' . $month . '-01';
            $last_date = $year . '-' . $month . '-31';

            
            $new_criteria .= ($county_id > 0)?" AND c.id = $county_id":NULL;
            $new_criteria .= ($district_id > 0)?" AND d.id = $district_id":NULL;
            $new_criteria .= " AND f.facility_code NOT IN(".$excluded_facilities.")";
            
            $new_query = "
            SELECT 
            c.zone as   zone,
            c.county as county_name,
            d.district,
            f.facility_name,
            f.facility_code,
            a.allocate_s,
            ROUND((a.allocate_s/100)) as screening_kits,
            a.allocate_c,
            ROUND((a.allocate_c/30)) as confirmatory_kits
            FROM
            allocation_details a,
            districts d,
            counties c,
            facilities f
            WHERE
            d.id = a.district_id AND c.id = d.county
            AND d.id = f.district
            AND f.facility_code = a.facility_code
            $new_criteria
            AND MONTH(a.month) = $month AND YEAR(a.month) = $year
            GROUP BY a.facility_code,d.id
            ";

            // echo $new_query;exit;
            $result_new = $this->db->query($new_query)->result_array();
            // echo "<pre>";print_r($result_new);exit;

            foreach ($result_new as $key => $value) {
                // echo "<pre>";print_r($value);exit;
                if ($year == '2017') {
                    $month_total_s = $month_total_s + $value['allocate_s'];
                    $month_total_c = $month_total_c + $value['allocate_c'];

                    /*if ($month == '01' || $month == '03') {
                        //do nothing
                    }else{
                        $month_total_s = $month_total_s + $value['allocate_s'];
                        $month_total_c = $month_total_c + $value['allocate_c'];
                    }*/
                }else{
                    $month_total_s = $month_total_s + $value['allocate_s'];
                    $month_total_c = $month_total_c + $value['allocate_c'];
                }
            }


        }
            // echo "<pre>";print_r($month_total_s);
            // echo "<pre>";print_r($month_total_c);exit;
        $data = array();
        $data['county_id'] = $county_id;
        $data['quarter'] = $quarter;
        $data['year'] = $year;
        $data['screening_used'] = $month_total_s;
        $data['confirmatory_used'] = $month_total_c;

        // echo "<pre>";print_r($data);exit;
        return $data;
    }

    public function get_county_distributed($county_id = NULL){
        $criteria = '';

        $criteria .= ($county_id > 0) ? " WHERE county_id = $county_id" : NULL;

        $query = "
        SELECT 
        county_id,
        district_id,
        SUM(screening_allocated) AS screening_allocated,
        SUM(confirmatory_allocated) AS confirmatory_allocated,
        SUM(screening_used) AS screening_used,
        SUM(confirmatory_used) AS confirmatory_used
        FROM
        district_drawing_rights
        $criteria";

        $result = $this->db->query($query);

        if (count($result) > 0) {
            $result = $result->result_array();
            $result = array_pop($result);
        }else{
            $result = NULL;
        }

        // echo "<pre>";print_r($result);exit;
        return $result;
    }

    public function get_allocation_status($district_id = NULL,$date = NULL,$county_id = NULL){
        $month = date('m',strtotime($date));
        $year = date('Y',strtotime($date));

        // echo "<pre>";print_r($district_id);exit;
        // echo "<pre>";print_r($month);
        // echo "<pre>";print_r($year);exit;
        $status_criteria = '';
        $status_criteria .= ($county_id > 0)?" AND c.id = $county_id":NULL;
        $status_criteria .= ($district_id > 0)?" AND d.id = $district_id":NULL;
        
        /*$status_query = "
        SELECT * FROM allocations 
        WHERE 
        MONTH(month) = '$month' 
        AND YEAR(month) = '$year'
        $status_criteria
        LIMIT 1";*/
        
        $status_query = "
        SELECT 
        a.*,
        c.*,
        d.*
        FROM
        allocations a, counties c, districts d
        WHERE
        a.district_id = d.id
        AND d.county = c.id
        AND MONTH(month) = '$month'
        AND YEAR(month) = '$year'
        $status_criteria";

        // echo $status_query;exit;
        $allocation_status = $this->db->query($status_query);
        // echo "<pre>";print_r($allocation_status);exit;
        if (empty($allocation_status)) {
            $data = NULL;
        }else{
            $allocation_status = $allocation_status->result_array();
            $data = $allocation_status;
            // $data = array_pop($allocation_status);
        }

        return $data;
    }

    public function get_facility_reporting_rates($facility_code = NULL,$month_date = NULL){
        $year = date('Y');
        $last_year = date('Y',strtotime("-1 YEAR"));
        
        $criteria = "";
        $criteria .= (isset($month_date) && $month_date !='') ? " AND MONTH(order_date) = MONTH('$month_date') AND YEAR(order_date) = YEAR('$month_date')" : NULL;

        $fac_data_q = "SELECT 
                            *
        FROM
        lab_commodity_orders
        WHERE
        facility_code = $facility_code $criteria";
        // echo $fac_data_q;exit;
        $result = $this->db->query($fac_data_q)->result_array();
        // echo "<pre>";print_r($result);exit;
        if (count($result) > 0) {
            $total = 1;
            $reported = 1;
            $count = 1;
            $order_date = $result[0]['order_date'];
        }else{
            $total = 0;
            $reported = 0;
            $count = 0;
            // $order_date = $result[0]['order_date'];
        }

        $percentage_complete = ceil($reported / $total * 100);
        $percentage_complete = number_format($percentage_complete, 0);

        $data['total'] = $total;
        $data['reported'] = $reported;
        $data['order_date'] = $month_date;
        $data['percentage'] = $percentage_complete;
        $data['count'] = $count;

        return $data;
    }

    public function get_order_data($county_id = NULL, $district_id = NULL, $facility_code = NULL, $commodity_id = NULL, $quarter = NULL, $by = NULL,$year = NULL,$order_id = NULL,$month = NULL){
        // echo "COUNTY: ".$county_id." DISTRICT ID:".$district_id." MFL".$facility_code." COMMODITY: ".$commodity_id." QUARTER: ".$quarter." BY: ".$by." YEAR".$year." MONTH: ".$month;exit;
        $county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
        $district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
        $facility_code = (isset($facility_code) && $facility_code!='')? $facility_code:NULL;
        $year = (isset($year) && $year>0)? $year:NULL;
        $month = (isset($month) && $month>0)? $month:NULL;
        $order_id = (isset($order_id) && $order_id>0)? $order_id:0;
        // echo "<pre>";print_r($quarter);
        // echo "<pre>";print_r($year);exit;
        // echo "<pre>";print_r($by);exit;
        $new_criteria = "";

        if ($quarter > 0 && $month < 1) {
            $year = (isset($year) && $year>0)? $year:date('Y');
            switch ($quarter) {
                case 1:
                    // echo "Quarter: One";exit;
                $first_month = "01";
                $last_month = "03";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                case 2:
                    // echo "Quarter: Two";exit;
                $first_month = "04";
                $last_month = "06";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                case 3:
                    // echo "Quarter: Three";exit;
                $first_month = "07";
                $last_month = "09";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                case 4:
                    // echo "Quarter: Four";exit;
                $first_month = "10";
                $last_month = "12";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                
                default:
                    // echo "Quarter: Invalid";exit;
                break;
            }
        }elseif (isset($month) && $month > 0) {
            $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');

            $firstdate = $year . '-' . $month . '-01';
            $lastdate = $year . '-' . $month . '-31';
            $new_criteria .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 

        }else{
            // $new_criteria .= ($year > 0)?" AND YEAR(lco.order_date) = $year ":" AND YEAR(lco.order_date) = ".date('Y');
            $new_criteria .= ($year > 0)?" AND YEAR(lco.order_date) = $year ":" AND YEAR(lco.order_date) > 2016 ";

        }

        // $year = date('Y');

        $new_criteria .= ($district_id > 0)?" AND d.id = $district_id":NULL;
        $new_criteria .= ($county_id > 0)?" AND c.id = $county_id":NULL;
        $new_criteria .= ($facility_code > 0)?" AND f.facility_code = $facility_code":NULL;
        // $new_criteria .= ($order_id > 0)?" AND lco.id = $order_id":NULL;

        $c_ids = (isset($commodity_id) && $commodity_id!='')? "'".$commodity_id."'" : "4,5";
        $order_by = " ORDER BY  lco.id DESC";
        switch ($by) {
            case 'county':
            $group_by = "GROUP BY c.id,commodity_id,lco.order_date ";
            break;
            case 'subcounty':
            $group_by = "GROUP BY d.id,commodity_id,lco.order_date ";
            break;
            case 'facility':
            $group_by = "GROUP BY f.facility_code,commodity_id,lco.order_date,lco.id ";
            $order_by = " ORDER BY  d.id DESC";
            break;   
            case 'national':
            $group_by = "GROUP BY commodity_id ";
            $order_by = " ";
            break;   
            default:
            $group_by = "GROUP BY commodity_id,lco.order_date ";
            break;
        }

        // echo $new_criteria;exit;
        // echo $group_by;exit;

        $query="
        SELECT 
            c.id AS county_id,
            c.county,
            d.id AS district_id,
            d.district,
            f.facility_name,
            f.facility_code,
            l.district_id,
            l.commodity_id,
            lco.id AS order_id,
            lco.order_date,
            lco.report_for,
            SUM(l.beginning_bal) AS beginning_bal,
            SUM(l.q_received) AS q_received,
            SUM(l.q_used) AS q_used,
            SUM(l.no_of_tests_done) AS no_of_tests_done,
            SUM(l.closing_stock) AS closing_stock,
            SUM(l.q_expiring) AS q_expiring,
            SUM(l.q_requested) AS q_requested,
            SUM(l.positive_adj) AS positive_adj,
            SUM(l.negative_adj) AS negative_adj,
            SUM(l.losses) AS losses,
            l.created_at
        FROM
            lab_commodity_details l USE INDEX (ORDER_ID , FACILITY_CODE , DISTRICT),
            counties c,
            districts d,
            facilities f USE INDEX (FACILITY_CODE),
            lab_commodity_orders lco USE INDEX (FACILITY_CODE)
        WHERE
            c.id = d.county
            AND l.order_id = lco.id
            AND l.facility_code > 0
            AND l.facility_code = f.facility_code
            AND lco.district_id = d.id
            AND l.commodity_id IN ($c_ids)
        $new_criteria
        $group_by
        $order_by;        
        ";
        // echo "<pre>";print_r($query);exit;

        $cache_key = date('Y-m-d').$query;
        // echo "<pre>";print_r($cache_key);exit;
        // echo "<pre>";print_r(md5($cache_key));exit;
        $cache_check = $this->memcached_check($cache_key);
        // echo "<pre>";print_r($cache_check);exit;
        if ($cache_check['status'] == "FAILED") {
            $res = $this->db->query($query)->result_array();
            // echo "<pre>summary_data_s: ";print_r($summary_data_s);exit;

            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($res);
            // echo "<pre>";print_r($cache_data);exit;
            $cache_save = $this->memcached_save($cache_data);
            // echo "<pre>Save: ";print_r($cache_save);exit;
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $res = json_decode($cache_check['data'],TRUE);
        }

        // echo "<pre>";print_r($res);exit;
        return $res;
    }

    public function get_order_data_formatted($county_id = NULL, $district_id = NULL, $facility_code = NULL, $commodity_id = NULL, $quarter = NULL, $by = NULL,$year = NULL,$order_id = NULL,$month = NULL){
        //IN PROGRESS

        $county_id = (isset($county_id) && $county_id > 0)? $county_id:NULL;
        $district_id = (isset($district_id) && $district_id > 0)? $district_id:NULL;
        $facility_code = (isset($facility_code) && $facility_code > 0)? $facility_code:NULL;
        $year = (isset($year) && $year > 0)? $year:NULL;
        $quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;

        // $data['filter_years'] = $this->get_years_to_date();

        if (isset($month)) {
            // $year = substr($month, -4);
            $year = date('Y');
            $month = substr($month, 0, 2);
            $monthyear = $year . '-' . $month . '-01';
            $set_month = $month;
        } else {
            $month = $this->session->userdata('Month');
            if ($month == '') {
                $month = date('mY', time());
            }
            // $year = date('Y');
            $month = substr_replace($month, "", -4);
            $monthyear = $year . '-' . $month . '-01';
        }

        $cache_key = "get_opening_and_closing_balances".$county_id.$district_id.$facility_code.'5'.$beg_date.$end_date.$quarter.'facility'.$set_month.$year."facility".date('Y-m-d');
        $cache_check = $this->memcached_check($cache_key);
        if ($cache_check['status'] == "FAILED") {
            $balances_conf = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 5 , $beg_date, $end_date, $quarter,'facility',$set_month,$year);

            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($balances_conf,true);

            $cache_save = $this->memcached_save($cache_data);
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $balances_conf = json_decode($cache_check['data'],true);
        }

        $cache_key = "get_opening_and_closing_balances".$county_id.$district_id.$facility_code.'4'.$beg_date.$end_date.$quarter.'NULL'.$set_month.$year.date('Y-m-d');
        $cache_check = $this->memcached_check($cache_key);
        if ($cache_check['status'] == "FAILED") {
            $nat_balances_scr = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 4 , $beg_date, $end_date, $quarter,NULL,$set_month,$year);
            // echo "<pre>HERE: ";print_r($nat_balances_scr);exit;
            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($nat_balances_scr,true);

            $cache_save = $this->memcached_save($cache_data);
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $nat_balances_scr = json_decode($cache_check['data'],true);
        }

        $cache_key = "get_opening_and_closing_balances".$county_id.$district_id.$facility_code.'5'.$beg_date.$end_date.$quarter.'NULL'.$set_month.$year.date('Y-m-d');
        $cache_check = $this->memcached_check($cache_key);
        if ($cache_check['status'] == "FAILED") {
            $nat_balances_conf = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 5 , $beg_date, $end_date, $quarter,NULL,$set_month,$year);

            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($nat_balances_conf,true);

            $cache_save = $this->memcached_save($cache_data);
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $nat_balances_conf = json_decode($cache_check['data'],true);
        }

        $year_ = (isset($year) && $year > 0)?$year:date('Y');

        $cache_key = "summary_data_s".$district_id.$quarter_."facility".$year_.date('Y-m-d');
        // echo "<pre>";print_r($cache_key);exit;
        // echo "<pre>";print_r(md5($cache_key));exit;
        $cache_check = $this->memcached_check($cache_key);
        // echo "<pre>";print_r($cache_check);exit;
        if ($cache_check['status'] == "FAILED") {
            $summary_data_s = $this->get_order_data(NULL, $district_id, NULL, 4 , $quarter_,"facility",$year_);
            // echo "<pre>summary_data_s: ";print_r($summary_data_s);exit;

            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($summary_data_s);

            $cache_save = $this->memcached_save($cache_data);
            // echo "<pre>Save: ";print_r($cache_save);exit;
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $summary_data_s = json_decode($cache_check['data'],TRUE);
        }
        // echo "<pre>DECODED: ";print_r($summary_data_s);exit; 

        $cache_key = "summary_data_c".$district_id.$quarter_."facility".$year_.date('Y-m-d');
        $cache_check = $this->memcached_check($cache_key);
        if ($cache_check['status'] == "FAILED") {
            $summary_data_c = $this->get_order_data(NULL, $district_id, NULL, 5 , $quarter_,"facility",$year_);
            // echo "<pre>";print_r($summary_data_s);exit;

            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($summary_data_c);

            $cache_save = $this->memcached_save($cache_data);
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $summary_data_c = json_decode($cache_check['data'],TRUE);
        }

        $national_arr_s = $county_arr_s = $county_arr_s_ = $dist_arr_s = $dist_arr_s_ = $fac_arr_s = $fac_arr_s_ = array();
        $national_arr_c = $county_arr_c = $county_arr_c_ = $dist_arr_c = $dist_arr_c_ = $fac_arr_c = $fac_arr_c_ = array();

        $national_arr_s['beginning_bal'] = $nat_balances_scr['beginning_balance']['beginning_balance'];
        $national_arr_s['beginning_bal_order_date'] = $nat_balances_scr['beginning_balance']['order_date'];
        $national_arr_s['closing_stock'] = $nat_balances_scr['closing_balance']['closing_balance'];
        $national_arr_s['closing_bal_order_date'] = $nat_balances_scr['closing_balance']['order_date'];

        $national_arr_c['beginning_bal'] = $nat_balances_conf['beginning_balance']['beginning_balance'];
        $national_arr_c['beginning_bal_order_date'] = $nat_balances_conf['beginning_balance']['order_date'];
        $national_arr_c['closing_stock'] = $nat_balances_conf['closing_balance']['closing_balance'];
        $national_arr_c['closing_bal_order_date'] = $nat_balances_conf['closing_balance']['order_date'];

        foreach ($summary_data_s as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $c_id = $value['county_id'];
            $c_name = $value['county'];
            $d_id = $value['district_id'];
            $d_name = $value['district'];
            $f_code = $value['facility_code'];
            $f_name = $value['facility_name'];

            // $national_arr_s['beginning_bal'] += $value['beginning_bal'];
            // $national_arr_s['closing_stock'] += $value['closing_stock'];
            
            $national_arr_s['q_received'] += $value['q_received'];
            $national_arr_s['q_used'] += $value['q_used'];
            $national_arr_s['no_of_tests_done'] += $value['no_of_tests_done'];
            $national_arr_s['positive_adj'] += $value['positive_adj'];
            $national_arr_s['negative_adj'] += $value['negative_adj'];
            $national_arr_s['losses'] += $value['losses'];

            $national_arr_s['level'] = "national";
            $national_arr_s['level_id'] = 0;
            $national_arr_s['commodity_id'] = 4;

            
            if (count($county_arr_s[$c_id]) > 0) {
                // $county_arr_s[$c_id]['beginning_bal'] += $value['beginning_bal'];
                // $county_arr_s[$c_id]['closing_stock'] += $value['closing_stock'];

                $county_arr_s[$c_id]['q_received'] += $value['q_received'];
                $county_arr_s[$c_id]['q_used'] += $value['q_used'];
                $county_arr_s[$c_id]['no_of_tests_done'] += $value['no_of_tests_done'];
                $county_arr_s[$c_id]['positive_adj'] += $value['positive_adj'];
                $county_arr_s[$c_id]['negative_adj'] += $value['negative_adj'];
                $county_arr_s[$c_id]['losses'] += $value['losses'];

            }else{
                $county_arr_s[$c_id]['county_id'] = $value['county_id'];
                $county_arr_s[$c_id]['county_name'] = $value['county'];
                $county_arr_s[$c_id]['district_id'] = $value['district_id'];
                $county_arr_s[$c_id]['district_name'] = $value['district'];
                $county_arr_s[$c_id]['facility_code'] = $value['facility_code'];
                $county_arr_s[$c_id]['facility_name'] = $value['facility_name'];
                $county_arr_s[$c_id]['order_date'] = $value['order_date'];
                
                // $county_arr_s[$c_id]['beginning_bal'] = $value['beginning_bal'];
                // $county_arr_s[$c_id]['closing_stock'] = $value['closing_stock'];

                $county_arr_s[$c_id]['beginning_bal'] = $county_balances_scr[$c_id]['beginning_balance'];
                $county_arr_s[$c_id]['beginning_order_date'] = $county_balances_scr[$c_id]['beginning_order_date'];
                $county_arr_s[$c_id]['closing_stock'] = $county_balances_scr[$c_id]['closing_balance'];
                $county_arr_s[$c_id]['closing_order_date'] = $county_balances_scr[$c_id]['closing_order_date'];
                
                $county_arr_s[$c_id]['q_received'] = $value['q_received'];
                $county_arr_s[$c_id]['q_used'] = $value['q_used'];
                $county_arr_s[$c_id]['no_of_tests_done'] = $value['no_of_tests_done'];
                $county_arr_s[$c_id]['positive_adj'] = $value['positive_adj'];
                $county_arr_s[$c_id]['negative_adj'] = $value['negative_adj'];
                $county_arr_s[$c_id]['losses'] = $value['losses'];

                $county_arr_s[$c_id]['level'] = "county";
                $county_arr_s[$c_id]['level_id'] = $value['county_id'];
                $county_arr_s[$c_id]['commodity_id'] = 4;

            }

            if (count($dist_arr_s[$d_id]) > 0) {
                // $dist_arr_s[$d_id]['beginning_bal'] += $value['beginning_bal'];
                // $dist_arr_s[$d_id]['closing_stock'] += $value['closing_stock'];
                
                $dist_arr_s[$d_id]['q_received'] += $value['q_received'];
                $dist_arr_s[$d_id]['q_used'] += $value['q_used'];
                $dist_arr_s[$d_id]['no_of_tests_done'] += $value['no_of_tests_done'];
                $dist_arr_s[$d_id]['positive_adj'] += $value['positive_adj'];
                $dist_arr_s[$d_id]['negative_adj'] += $value['negative_adj'];
                $dist_arr_s[$d_id]['losses'] += $value['losses'];
            }else{
                $dist_arr_s[$d_id]['county_id'] = $value['county_id'];
                $dist_arr_s[$d_id]['county_name'] = $value['county'];
                $dist_arr_s[$d_id]['district_id'] = $value['district_id'];
                $dist_arr_s[$d_id]['district_name'] = $value['district'];
                $dist_arr_s[$d_id]['facility_code'] = $value['facility_code'];
                $dist_arr_s[$d_id]['facility_name'] = $value['facility_name'];
                $dist_arr_s[$d_id]['order_date'] = $value['order_date'];

                // $dist_arr_s[$d_id]['beginning_bal'] = $value['beginning_bal'];
                // $dist_arr_s[$d_id]['closing_stock'] = $value['closing_stock'];

                $dist_arr_s[$d_id]['beginning_bal'] = $dist_bal[$d_id][4]['beginning_balance'];
                $dist_arr_s[$d_id]['beginning_order_date'] = $dist_bal[$d_id][4]['beginning_order_date'];
                $dist_arr_s[$d_id]['closing_stock'] = $dist_bal[$d_id][4]['closing_balance'];
                $dist_arr_s[$d_id]['closing_order_date'] = $dist_bal[$d_id][4]['closing_order_date'];
                
                $dist_arr_s[$d_id]['q_received'] = $value['q_received'];
                $dist_arr_s[$d_id]['q_used'] = $value['q_used'];
                $dist_arr_s[$d_id]['no_of_tests_done'] = $value['no_of_tests_done'];
                $dist_arr_s[$d_id]['positive_adj'] = $value['positive_adj'];
                $dist_arr_s[$d_id]['negative_adj'] = $value['negative_adj'];
                $dist_arr_s[$d_id]['losses'] = $value['losses'];

                $dist_arr_s[$d_id]['level'] = "subcounty";
                $dist_arr_s[$d_id]['level_id'] = $value['district_id'];
                $dist_arr_s[$d_id]['commodity_id'] = 4;
            }

            if (count($fac_arr_s[$f_code]) > 0) {
                // $fac_arr_s[$f_code]['beginning_bal'] += $value['beginning_bal'];
                // $fac_arr_s[$f_code]['closing_stock'] += $value['closing_stock'];

                $fac_arr_s[$f_code]['q_received'] += $value['q_received'];
                $fac_arr_s[$f_code]['q_used'] += $value['q_used'];
                $fac_arr_s[$f_code]['no_of_tests_done'] += $value['no_of_tests_done'];
                $fac_arr_s[$f_code]['positive_adj'] += $value['positive_adj'];
                $fac_arr_s[$f_code]['negative_adj'] += $value['negative_adj'];
                $fac_arr_s[$f_code]['losses'] += $value['losses'];
            }else{
                $fac_arr_s[$f_code]['county_id'] = $value['county_id'];
                $fac_arr_s[$f_code]['county_name'] = $value['county'];
                $fac_arr_s[$f_code]['district_id'] = $value['district_id'];
                $fac_arr_s[$f_code]['district_name'] = $value['district'];
                $fac_arr_s[$f_code]['facility_code'] = $value['facility_code'];
                $fac_arr_s[$f_code]['facility_name'] = $value['facility_name'];
                $fac_arr_s[$f_code]['order_date'] = $value['order_date'];
                
                // $fac_arr_s[$f_code]['beginning_bal'] = $value['beginning_bal'];
                // $fac_arr_s[$f_code]['closing_stock'] = $value['closing_stock'];
                
                $fac_arr_s[$f_code]['beginning_bal'] = $fac_bal[$f_code][4]['beginning_balance'];
                $fac_arr_s[$f_code]['beginning_order_date'] = $fac_bal[$f_code][4]['beginning_order_date'];
                $fac_arr_s[$f_code]['closing_stock'] = $fac_bal[$f_code][4]['closing_balance'];
                $fac_arr_s[$f_code]['closing_order_date'] = $fac_bal[$f_code][4]['closing_order_date'];

                $fac_arr_s[$f_code]['q_received'] = $value['q_received'];
                $fac_arr_s[$f_code]['q_used'] = $value['q_used'];
                $fac_arr_s[$f_code]['no_of_tests_done'] = $value['no_of_tests_done'];
                $fac_arr_s[$f_code]['positive_adj'] = $value['positive_adj'];
                $fac_arr_s[$f_code]['negative_adj'] = $value['negative_adj'];
                $fac_arr_s[$f_code]['losses'] = $value['losses'];

                $fac_arr_s[$f_code]['level'] = "facility";
                $fac_arr_s[$f_code]['level_id'] = $value['facility_code'];
                $fac_arr_s[$f_code]['commodity_id'] = 4;

            }
            
        }//end of summary data foreach

         // echo "<pre>";print_r($counties);exit;
        foreach ($summary_data_c as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $c_id = $value['county_id'];
            $c_name = $value['county'];
            $d_id = $value['district_id'];
            $d_name = $value['district'];
            $f_code = $value['facility_code'];
            $f_name = $value['facility_name'];

            // $national_arr_c['beginning_bal'] += $value['beginning_bal'];
            // $national_arr_c['closing_stock'] += $value['closing_stock'];

            $national_arr_c['q_received'] += $value['q_received'];
            $national_arr_c['q_used'] += $value['q_used'];
            $national_arr_c['no_of_tests_done'] += $value['no_of_tests_done'];
            $national_arr_c['positive_adj'] += $value['positive_adj'];
            $national_arr_c['negative_adj'] += $value['negative_adj'];
            $national_arr_c['losses'] += $value['losses'];

            $national_arr_c['level'] = "national";
            $national_arr_c['level_id'] = 0;
            $national_arr_c['commodity_id'] = 5;
            
            if (count($county_arr_c[$c_id]) > 0) {
                // $county_arr_c[$c_id]['beginning_bal'] += $value['beginning_bal'];
                // $county_arr_c[$c_id]['closing_stock'] += $value['closing_stock'];
                
                $county_arr_c[$c_id]['q_received'] += $value['q_received'];
                $county_arr_c[$c_id]['q_used'] += $value['q_used'];
                $county_arr_c[$c_id]['no_of_tests_done'] += $value['no_of_tests_done'];
                $county_arr_c[$c_id]['positive_adj'] += $value['positive_adj'];
                $county_arr_c[$c_id]['negative_adj'] += $value['negative_adj'];
                $county_arr_c[$c_id]['losses'] += $value['losses'];
            }else{
                $county_arr_c[$c_id]['county_id'] = $value['county_id'];
                $county_arr_c[$c_id]['county_name'] = $value['county'];
                $county_arr_c[$c_id]['district_id'] = $value['district_id'];
                $county_arr_c[$c_id]['district_name'] = $value['district'];
                $county_arr_c[$c_id]['facility_code'] = $value['facility_code'];
                $county_arr_c[$c_id]['facility_name'] = $value['facility_name'];
                $county_arr_c[$c_id]['order_date'] = $value['order_date'];

                // $county_arr_c[$c_id]['beginning_bal'] = $value['beginning_bal'];
                // $county_arr_c[$c_id]['closing_stock'] = $value['closing_stock'];
                    
                $county_arr_c[$c_id]['beginning_bal'] = $county_balances_conf[$c_id]['beginning_balance'];
                $county_arr_c[$c_id]['beginning_order_date'] = $county_balances_conf[$c_id]['beginning_order_date'];
                $county_arr_c[$c_id]['closing_stock'] = $county_balances_conf[$c_id]['closing_balance'];
                $county_arr_c[$c_id]['closing_order_date'] = $county_balances_conf[$c_id]['closing_order_date'];

                $county_arr_c[$c_id]['q_received'] = $value['q_received'];
                $county_arr_c[$c_id]['q_used'] = $value['q_used'];
                $county_arr_c[$c_id]['no_of_tests_done'] = $value['no_of_tests_done'];
                $county_arr_c[$c_id]['positive_adj'] = $value['positive_adj'];
                $county_arr_c[$c_id]['negative_adj'] = $value['negative_adj'];
                $county_arr_c[$c_id]['losses'] = $value['losses'];

                $county_arr_c[$c_id]['level'] = "county";
                $county_arr_c[$c_id]['level_id'] = $value['county_id'];
                $county_arr_c[$c_id]['commodity_id'] = 5;
            }

            if (count($dist_arr_c[$d_id]) > 0) {
                // $dist_arr_c[$d_id]['beginning_bal'] += $value['beginning_bal'];
                // $dist_arr_c[$d_id]['closing_stock'] += $value['closing_stock'];
                $dist_arr_c[$d_id]['q_received'] += $value['q_received'];
                $dist_arr_c[$d_id]['q_used'] += $value['q_used'];
                $dist_arr_c[$d_id]['no_of_tests_done'] += $value['no_of_tests_done'];
                $dist_arr_c[$d_id]['positive_adj'] += $value['positive_adj'];
                $dist_arr_c[$d_id]['negative_adj'] += $value['negative_adj'];
                $dist_arr_c[$d_id]['losses'] += $value['losses'];
            }else{
                $dist_arr_c[$d_id]['county_id'] = $value['county_id'];
                $dist_arr_c[$d_id]['county_name'] = $value['county'];
                $dist_arr_c[$d_id]['district_id'] = $value['district_id'];
                $dist_arr_c[$d_id]['district_name'] = $value['district'];
                $dist_arr_c[$d_id]['facility_code'] = $value['facility_code'];
                $dist_arr_c[$d_id]['facility_name'] = $value['facility_name'];
                $dist_arr_c[$d_id]['order_date'] = $value['order_date'];
                
                // $dist_arr_c[$d_id]['beginning_bal'] = $value['beginning_bal'];
                // $dist_arr_c[$d_id]['closing_stock'] = $value['closing_stock'];

                $dist_arr_c[$d_id]['beginning_bal'] = $dist_bal[$d_id][5]['beginning_balance'];
                $dist_arr_c[$d_id]['beginning_order_date'] = $dist_bal[$d_id][5]['beginning_order_date'];
                $dist_arr_c[$d_id]['closing_stock'] = $dist_bal[$d_id][5]['closing_balance'];
                $dist_arr_c[$d_id]['closing_order_date'] = $dist_bal[$d_id][5]['closing_order_date'];

                $dist_arr_c[$d_id]['q_received'] = $value['q_received'];
                $dist_arr_c[$d_id]['q_used'] = $value['q_used'];
                $dist_arr_c[$d_id]['no_of_tests_done'] = $value['no_of_tests_done'];
                $dist_arr_c[$d_id]['positive_adj'] = $value['positive_adj'];
                $dist_arr_c[$d_id]['negative_adj'] = $value['negative_adj'];
                $dist_arr_c[$d_id]['losses'] = $value['losses'];

                $dist_arr_c[$d_id]['level'] = "subcounty";
                $dist_arr_c[$d_id]['level_id'] = $value['district_id'];
                $dist_arr_c[$d_id]['commodity_id'] = 5;
            }

            if (count($fac_arr_c[$f_code]) > 0) {
                // $fac_arr_c[$f_code]['beginning_bal'] += $value['beginning_bal'];
                // $fac_arr_c[$f_code]['closing_stock'] += $value['closing_stock'];

                $fac_arr_c[$f_code]['q_received'] += $value['q_received'];
                $fac_arr_c[$f_code]['q_used'] += $value['q_used'];
                $fac_arr_c[$f_code]['no_of_tests_done'] += $value['no_of_tests_done'];
                $fac_arr_c[$f_code]['positive_adj'] += $value['positive_adj'];
                $fac_arr_c[$f_code]['negative_adj'] += $value['negative_adj'];
                $fac_arr_c[$f_code]['losses'] += $value['losses'];
            }else{
                $fac_arr_c[$f_code]['county_id'] = $value['county_id'];
                $fac_arr_c[$f_code]['county_name'] = $value['county'];
                $fac_arr_c[$f_code]['district_id'] = $value['district_id'];
                $fac_arr_c[$f_code]['district_name'] = $value['district'];
                $fac_arr_c[$f_code]['facility_code'] = $value['facility_code'];
                $fac_arr_c[$f_code]['facility_name'] = $value['facility_name'];
                $fac_arr_c[$f_code]['order_date'] = $value['order_date'];

                // $fac_arr_c[$f_code]['beginning_bal'] = $value['beginning_bal'];
                // $fac_arr_c[$f_code]['closing_stock'] = $value['closing_stock'];
                $fac_arr_c[$f_code]['beginning_bal'] = $fac_bal[$f_code][5]['beginning_balance'];
                $fac_arr_c[$f_code]['beginning_order_date'] = $fac_bal[$f_code][5]['beginning_order_date'];
                $fac_arr_c[$f_code]['closing_stock'] = $fac_bal[$f_code][5]['closing_balance'];
                $fac_arr_c[$f_code]['closing_order_date'] = $fac_bal[$f_code][5]['closing_order_date'];

                $fac_arr_c[$f_code]['q_received'] = $value['q_received'];
                $fac_arr_c[$f_code]['q_used'] = $value['q_used'];
                $fac_arr_c[$f_code]['no_of_tests_done'] = $value['no_of_tests_done'];
                $fac_arr_c[$f_code]['positive_adj'] = $value['positive_adj'];
                $fac_arr_c[$f_code]['negative_adj'] = $value['negative_adj'];
                $fac_arr_c[$f_code]['losses'] = $value['losses'];

                $fac_arr_c[$f_code]['level'] = "facility";
                $fac_arr_c[$f_code]['level_id'] = $value['facility_code'];
                $fac_arr_c[$f_code]['commodity_id'] = 5;
            }
            
        }//end of summary data foreach

        $data['national_order_data_s'] = $national_arr_s;
        $data['county_data_s'] = $county_arr_s;
        $data['district_data_s'] = $dist_arr_s;
        $data['facility_data_s'] = $fac_arr_s;

        $data['national_order_data_c'] = $national_arr_c;
        $data['county_data_c'] = $county_arr_c;
        $data['district_data_c'] = $dist_arr_c;
        $data['facility_data_c'] = $fac_arr_c;

        // echo "<pre>";print_r($data);exit;

        return $data;
    }

    public function get_order_data_by_id($order_id)
    {
        $query = "
            SELECT *
            FROM lab_commodity_details, counties, facilities, districts, lab_commodity_orders, lab_commodity_categories, lab_commodities
            WHERE lab_commodity_details.facility_code = facilities.facility_code
            AND counties.id = districts.county
            AND facilities.facility_code = lab_commodity_orders.facility_code
            AND lab_commodity_details.commodity_id = lab_commodities.id
            AND lab_commodity_categories.id = lab_commodities.category
            AND facilities.district = districts.id
            AND lab_commodity_details.order_id = lab_commodity_orders.id
            AND lab_commodity_orders.id = '$order_id'";
        
        // echo $query;exit;
        $result = $this->db->query($query);

        if ($result->num_rows() > 0) {
            $result = $result->result_array();
        }else{
            $result = NULL;
        }

        return $result;
    }

    public function update_issues_from_kemsa_api($commodity_id = NULL,$year = NULL){
        /*KARSAN PENDING FRIDAY 8 MAY*/
        $counties = $this->db->query("SELECT * FROM counties")->result_array();
        $districts = $this->db->query("SELECT * FROM districts")->result_array();
        //random to allow for commit
        // $commodity_id = (isset($commodity_id) && $commodity_id > 0)? $commodity_id :NULL;
        $kemsa_scr_id = 'NL05TES003';//Determine
        $kemsa_conf_id = 'NL05TES069';//First response

        $yr = (isset($year) && $year > 0)? $year:date('Y');
        $year_beg = $yr."-01-01";
        $year_end = $yr."-12-31";

        /*$year_beg = date("Y-m-d", strtotime(date('Y-01-01')));
        $year_end = date("Y-m-d", strtotime(date('Y-12-31')));*/

        switch ($commodity_id) {
            case 4:
            $kemsa_current_id = $kemsa_scr_id;
            $pack_size = 100;
            break;

            case 5:
            $kemsa_current_id = $kemsa_conf_id;
            $pack_size = 30;
            break;
            
            default:
            $commodity_id = 4;
            $kemsa_current_id = $kemsa_scr_id;
            $pack_size = 100;
            break;
        }

        // echo "<pre>";print_r($kemsa_current_id);exit;
        $kemsa_api_token = '$2a$06$BciA87SeFAPc4OT1HNLIYO5LxxWBq/2GlMVMT/K8AviAX1OUhrWsa';

        $final_issue_data = $final_issue_data_ = array();

        foreach ($counties as $key => $value) {
            $county_id = $value['id'];
            $county_name = $value['county'];
            $county_name = trim($county_name);
            $county_name = strtoupper($county_name);
            // $commodity_id = 4;

            // echo "<pre>";print_r($year_beg);
            // echo "<pre>";print_r($year_end);exit;

            $data = array(
                'filter[where][county]'=>$county_name,
                'filter[where][product_code]'=>$kemsa_current_id,
                'filter[where][movementdate][between][0]'=>$year_beg,
                'filter[where][movementdate][between][1]'=>$year_end
            );

            // echo "<pre>";print_r($data);exit;
            $api_url = 'https://api.kemsa.co.ke/kem_issuesreports?';
            $url_data = http_build_query($data);
            $api_url = $api_url.$url_data;

            $curl = curl_init();
            $headr = array();
            $headr[] = 'apitoken: $2a$06$BciA87SeFAPc4OT1HNLIYO5LxxWBq/2GlMVMT/K8AviAX1OUhrWsa';

            // echo "<pre>";print_r(FCPATH.'ssl/ca-bundle.crt');exit;
            curl_setopt_array($curl, array(
              CURLOPT_URL => $api_url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_SSL_VERIFYPEER => false,
              CURLOPT_SSL_VERIFYHOST => 0,
              // CURLOPT_HEADER => true,
              // CURLINFO_HEADER_OUT => true,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_CAINFO => FCPATH.'ssl/ca-bundle.crt',
              CURLOPT_CAPATH => FCPATH.'ssl/ca-bundle.crt',
              CURLOPT_HTTPHEADER => $headr,
              CURLOPT_TIMEOUT => 0
          ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            $information = curl_getinfo($curl);

            curl_close($curl);
            echo "<pre>";print_r($information);
            if ($err) {
              echo "cURL Error #:" . $err;
          } else {
                echo "<pre>";print_r($response);exit;

            $issue_data = json_decode($response, true);
                // echo "<pre>";print_r($issue_data);exit;
            foreach ($issue_data as $keyy => $valuee) {
                $f_code = $valuee['mfl_code'];
                if (is_numeric($f_code)) {
                    $f_query = "SELECT district FROM facilities WHERE facility_code = $f_code";
                        // echo "<pre>";print_r($f_query);
                    $dist_data = $this->db->query($f_query);
                        // echo "<pre>";print_r($dist_data);exit;
                    if (count($dist_data) > 0) {
                        $dist_data = $dist_data ->result_array();

                        $d_id = $dist_data[0]['district'];
                        $c_id = $county_id;
                        $com_id = $commodity_id;

                            // $q_issued = $valuee['movementqty'];
                            // $q_requested = $valuee['demandqty'];

                        $q_issued = $valuee['movementqty'] * $pack_size;
                        $q_requested = $valuee['demandqty'] * $pack_size;

                        $v_issued = $valuee['issued_value'];
                        $v_requested = $valuee['demand_value'];
                        $d_note = $valuee['dnote'];
                        $issue_date = $valuee['movementdate'];
                        $issue_date = date('Y-m-d',strtotime($issue_date));
                            // echo "<pre>";print_r($issue_date);exit;

                        $kemsa_issue_query = "SELECT * FROM kemsa_issues WHERE facility_code = $f_code AND delivery_note = '$d_note' AND commodity_id = '$com_id'";
                        $kemsa_issue = $this->db->query($kemsa_issue_query)->result_array();

                            // echo "<pre>";print_r($comment);exit;
                        $issue_data = array();
                            if (empty($kemsa_issue)) {//county comment insert or update
                                $issue_data['county_id'] = $c_id;
                                $issue_data['district_id'] = $d_id;
                                $issue_data['facility_code'] = $f_code;
                                $issue_data['commodity_id'] = $com_id;

                                $issue_data['qty_issued'] = $q_issued;
                                $issue_data['qty_requested'] = $q_requested;
                                $issue_data['value_issued'] = $v_issued;
                                $issue_data['value_requested'] = $v_requested;
                                $issue_data['delivery_note'] = $d_note;
                                $issue_data['issue_date'] = $issue_date;
                                // echo "<pre>";print_r($issue_data);exit;

                                $result = $this->db->insert('kemsa_issues',$issue_data);
                            }else{
                                /*$kemsa_issue_id = $kemsa_issue[0]['id'];

                                $issue_data['qty_issued'] = $q_issued;
                                $issue_data['qty_requested'] = $q_requested;
                                $issue_data['value_issued'] = $v_issued;
                                $issue_data['value_requested'] = $v_requested;
                                $issue_data['delivery_note'] = $d_note;

                                $this->db->where('id', $kemsa_issue_id);
                                $result = $this->db->update('kemsa_issues', $issue_data);*/
                            }//end of if issue is present
                        }//end of if district data is present
                    }

                    // echo "<pre>";print_r($dist_data);exit;

                    echo "<pre>COUNTY ID: ".$county_id." FACILITY CODE: ".$f_code." DISTRICT ID: ".$d_id." TIME INTERVAL: ".$year_beg." - ".$year_end." COMMODITY ID: ".$com_id;

                }// end of issue_data foreach
            }//end of if curl success 

        }//end of counties for each
    }

    public function update_issues_from_kemsa_api_v2($commodity_id = NULL, $year = NULL){
        $counties = $this->db->query("SELECT * FROM counties")->result_array();
        $districts = $this->db->query("SELECT * FROM districts")->result_array();
        $output = array();
        // $commodity_id = (isset($commodity_id) && $commodity_id > 0)? $commodity_id :NULL;
        $kemsa_scr_id = 'NL05TES003';//Determine
        $kemsa_conf_id = 'NL05TES069';//First response

        $yr = (isset($year) && $year > 0)? $year:date('Y');
        $year_beg = $yr."-01-01";
        $year_end = $yr."-12-31";

        switch ($commodity_id) {
            case 4:
            $kemsa_current_id = $kemsa_scr_id;
            $pack_size = 100;
            break;

            case 5:
            $kemsa_current_id = $kemsa_conf_id;
            $pack_size = 30;
            break;
            
            default:
            $commodity_id = 4;
            $kemsa_current_id = $kemsa_scr_id;
            $pack_size = 100;
            break;
        }

        // echo "<pre>";print_r($kemsa_current_id);exit;
        $kemsa_api_token = '$2a$06$BciA87SeFAPc4OT1HNLIYO5LxxWBq/2GlMVMT/K8AviAX1OUhrWsa';

        $final_issue_data = $final_issue_data_ = array();

        // $year_beg = date("Y-m-d", strtotime(date('Y-01-01')));
        // $year_end = date("Y-m-d", strtotime(date('Y-12-31')));

        // echo "<pre>";print_r($year_beg);
        // echo "<pre>";print_r($year_end);exit;

        $start_date = $year . '-' . $month . '-01';
        $end_date = $year . '-' . $month . '-31';


        $cache_key = "update_issues_from_kemsa_api_v2_".$commodity_id.'_'.$year.'_'.date('Y-m-d');
        // echo "<pre>";print_r($cache_key);exit;
        $cache_check = $this->memcached_check($cache_key);
        // echo "<pre>";print_r($cache_check);
        if ($cache_check['status'] == "FAILED") {
            // echo "<pre>";print_r($data);exit;
            $data = array(
                'filter[where][product_code]'=>$kemsa_current_id,
                'filter[where][movementdate][between][0]'=>$year_beg,
                'filter[where][movementdate][between][1]'=>$year_end
            );

            // echo "<pre>";print_r($data);exit;
            $api_url = 'https://api.kemsa.co.ke/kem_issuesreports?';
            $url_data = http_build_query($data);
            $api_url = $api_url.$url_data;

            $curl = curl_init();
            $headr = array();
            $headr[] = 'apitoken: $2a$06$BciA87SeFAPc4OT1HNLIYO5LxxWBq/2GlMVMT/K8AviAX1OUhrWsa';

            curl_setopt_array($curl, array(
              CURLOPT_URL => $api_url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_SSL_VERIFYPEER => false,
              CURLOPT_SSL_VERIFYHOST => 0,
              // CURLOPT_HEADER => true,
              // CURLINFO_HEADER_OUT => true,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_CAINFO => FCPATH.'ssl/ca-bundle.crt',
              CURLOPT_CAPATH => FCPATH.'ssl/ca-bundle.crt',
              CURLOPT_HTTPHEADER => $headr,
              CURLOPT_TIMEOUT => 0
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            $information = curl_getinfo($curl);

            curl_close($curl);
            // echo "<pre>";print_r($information);
            if ($err) {
              echo "cURL Error #:" . $err;exit;
            } else {
                // echo "<pre>";print_r($response);exit;
                $cache_issue_data = $response;//response comes as json
                $issue_data = json_decode($response, true);
                // echo "<pre>";print_r($issue_data);exit;
            }//end of if curl success

            // echo "<pre>";print_r($issue_data);exit;
            // echo "<pre>";print_r($cache_key);exit;
            $cache_data['key'] = $this->generate_memcached_key($cache_key);
            $cache_data['data'] = $cache_issue_data;

            // echo "<pre>";print_r($cache_data);exit;
            $cache_save = $this->memcached_save($cache_data);
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $issue_data = json_decode($cache_check['data'], true);
        }
 
        // echo "<pre>";print_r($issue_data);exit;
        $new_data = $present_data = $conflicting_data = array();

        foreach ($issue_data as $keyy => $valuee) {
            $f_code = $valuee['mfl_code'];
            if (is_numeric($f_code)) {
                $f_query = "SELECT district FROM facilities WHERE facility_code = $f_code";
                    // echo "<pre>";print_r($f_query);
                $dist_data_ = $this->db->query($f_query);
                // echo "<pre>NUM ROWS: ".$dist_data_->num_rows();
                    // echo "<pre>";print_r($dist_data);exit;
                if ($dist_data_->num_rows() > 0) {
                    $dist_data = $dist_data_ ->result_array();
                    // echo "<pre>";print_r($dist_data);
                    $d_id = $dist_data[0]['district'];

                    $d_query = "SELECT county FROM districts WHERE id = $d_id";
                    // echo "<pre>";print_r($d_query);
                    $c_data_ = $this->db->query($d_query);
                    if ($c_data_->num_rows() > 0){
                        $c_data = $c_data_->result_array();
                        // echo "<pre>";print_r($c_data);exit;
                        $c_id = $c_data[0]['county'];
                        $com_id = $commodity_id;

                            // $q_issued = $valuee['movementqty'];
                            // $q_requested = $valuee['demandqty'];

                        $q_issued = $valuee['movementqty'] * $pack_size;
                        $q_requested = $valuee['demandqty'] * $pack_size;

                        $v_issued = $valuee['issued_value'];
                        $v_requested = $valuee['demand_value'];
                        $d_note = $valuee['dnote'];
                        $issue_date = $valuee['movementdate'];
                        $issue_date = date('Y-m-d',strtotime($issue_date));
                            // echo "<pre>";print_r($issue_date);exit;

                        $kemsa_issue_query = "SELECT * FROM kemsa_issues WHERE facility_code = $f_code AND delivery_note = '$d_note' AND commodity_id = '$com_id'";
                        $kemsa_issue = $this->db->query($kemsa_issue_query)->result_array();
                        
                        $issue_data = array();
                        $issue_data['county_id'] = $c_id;
                        $issue_data['district_id'] = $d_id;
                        $issue_data['facility_code'] = $f_code;
                        $issue_data['commodity_id'] = $com_id;

                        $issue_data['qty_issued'] = $q_issued;
                        $issue_data['qty_requested'] = $q_requested;
                        $issue_data['value_issued'] = $v_issued;
                        $issue_data['value_requested'] = $v_requested;
                        $issue_data['delivery_note'] = $d_note;
                        $issue_data['issue_date'] = $issue_date;

                            // echo "<pre>";print_r($comment);exit;
                        if (empty($kemsa_issue)) {//county comment insert or update
                            // echo "<pre>";print_r($issue_data);exit;
                            array_push($new_data, $issue_data);
                            $result = $this->db->insert('kemsa_issues',$issue_data);
                        }else{
                            $kemsa_issue_id = $kemsa_issue[0]['id'];

                            $curr_qty_issued = $kemsa_issue[0]['qty_issued'];
                            $curr_qty_requested = $kemsa_issue[0]['qty_requested'];
                            $curr_v_issued = $kemsa_issue[0]['value_issued'];
                            $curr_v_requested = $kemsa_issue[0]['value_requested'];
                            
                            $new_q_issued = $q_issued;
                            $new_q_requested = $q_requested;
                            $new_v_issued = $v_issued;
                            $new_v_requested = $v_requested;

                            if ($curr_qty_issued != $new_q_issued) {
                                // echo "<pre>";print_r($kemsa_issue);
                                // echo "<pre>";print_r($issue_data);exit;
                                $chk_q = "SELECT * FROM kemsa_issue_discrepancies WHERE facility_code = $f_code AND delivery_note = '$d_note' AND commodity_id = '$com_id'";
                                $d_chk = $this->db->query($chk_q);

                                $discrepancy_data['county_id'] = $c_id;
                                $discrepancy_data['district_id'] = $d_id;
                                $discrepancy_data['facility_code'] = $f_code;
                                $discrepancy_data['commodity_id'] = $com_id;
                                $discrepancy_data['qty_issued_current'] = $curr_qty_issued;
                                $discrepancy_data['qty_issued_new'] = $new_q_issued;
                                $discrepancy_data['qty_requested_current'] = $curr_qty_requested;
                                $discrepancy_data['qty_requested_new'] = $new_q_requested;
                                $discrepancy_data['value_issued'] = $v_issued;
                                $discrepancy_data['value_requested'] = $v_requested;
                                $discrepancy_data['delivery_note'] = $d_note;
                                $discrepancy_data['issue_date'] = $issue_date;
                                $discrepancy_data['kemsa_issue_id'] = $kemsa_issue_id;

                                if ($d_chk->num_rows() > 0){

                                }else{
                                    // echo "<pre>";print_r($issue_data);exit;
                                    $result = $this->db->insert('kemsa_issue_discrepancies',$discrepancy_data);
                                }

                                array_push($conflicting_data, $discrepancy_data);
                                // echo "<pre>";print_r($discrepancy_data);exit;
                            }else{
                                // echo "<pre>";print_r($issue_data);exit;
                                array_push($present_data, $kemsa_issue);
                            }
                                // echo "<pre>";print_r($issue_data);exit;

                            /*$issue_data['qty_issued'] = $q_issued;
                            $issue_data['qty_requested'] = $q_requested;
                            $issue_data['value_issued'] = $v_issued;
                            $issue_data['value_requested'] = $v_requested;
                            $issue_data['delivery_note'] = $d_note;
                            array_push($present_data, $issue_data);

                            $this->db->where('id', $kemsa_issue_id);*/
                            // $result = $this->db->update('kemsa_issues', $issue_data);
                        }//end of if issue is present

                        }//end of if county data is present
                    }//end of if district data is present

                }

                // echo "<pre>";print_r($dist_data);exit;

                $output[] = "COUNTY ID: ".$c_id." FACILITY CODE: ".$f_code." DISTRICT ID: ".$d_id." TIME INTERVAL: ".$year_beg." - ".$year_end." COMMODITY ID: ".$com_id." Q_ISSUED: ".$q_issued." ISSUE_DATE: ".$issue_date." DELIVERY_NOTE: ".$d_note;

        }// end of issue_data foreach

        $return_data['new_data'] = $new_data;
        $return_data['conflicting_data'] = $conflicting_data;
        $return_data['present_data'] = $present_data;
        // echo "<pre>";print_r($conflicting_data);exit;
        // echo "<pre>";print_r($output);exit;
        return $return_data;
    }

    public function update_issues_from_kemsa_api_v3($year = NULL)
    {
        $year = (isset($year) && $year > 0)? $year:date('Y');

        // echo "<pre>";print_r($api);exit;
        
        $kemsa_scr_id = 'NL05TES003';//Determine
        $kemsa_conf_id = 'NL05TES069';//First response
        
        $commodity_array['scr']['commodity_id'] = 4;
        $commodity_array['scr']['kemsa_commodity_id'] = $kemsa_scr_id;
        $commodity_array['conf']['commodity_id'] = 5;
        $commodity_array['conf']['kemsa_commodity_id'] = $kemsa_conf_id;

        // echo "<pre>";print_r($commodity_array);exit;

        foreach ($commodity_array as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $comm_id = $value['commodity_id'];
            $kemsa_annual_issue_data[$comm_id] = $this->update_issues_from_kemsa_api_v2($comm_id,$year);
        }

        echo "<pre>RESULTS: ";print_r($kemsa_annual_issue_data);exit;
    }

    public function get_kemsa_issue_data($county_id = NULL, $district_id = NULL, $facility_code = NULL, $commodity_id = NULL, $quarter = NULL,$year = NULL,$month = NULL,$by = NULL,$format = NULL){
        // echo "MONTH: ".$month."BY: ".$by." county_id: ".$county_id." district_id: ".$district_id." facility_code: ".$facility_code." YEAR: ".$year." QUARTER: ".$quarter." BY: ".$by." commodity_id: ".$commodity_id;exit;
        // echo $county_id." ".$district_id." ".$facility_code." ".$commodity_id." ".$quarter." ".$year." ".$month;exit;
        $county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
        $district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
        $facility_code = (isset($facility_code) && $facility_code!='')? $facility_code:NULL;
        $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:NULL;
        $by = (isset($by) && $by!='')? $by:NULL;
        // echo "<pre>";print_r($quarter);exit;
        $new_criteria = "";

        // echo "MONTH: ".$month." YEAR: ".$year;exit;
        if ($quarter > 0 && $month < 1) {
            // $year = date('Y');
            // echo "<pre>";print_r($year);exit;
            switch ($quarter) {
                case 1:
                    // echo "Quarter: One";exit;
                $first_month = "01";
                $last_month = "03";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND DATE(k.issue_date) BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                case 2:
                    // echo "Quarter: Two";exit;
                $first_month = "04";
                $last_month = "06";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND DATE(k.issue_date) BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                case 3:
                    // echo "Quarter: Three";exit;
                $first_month = "07";
                $last_month = "09";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND DATE(k.issue_date) BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                case 4:
                    // echo "Quarter: Four";exit;
                $first_month = "10";
                $last_month = "12";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND DATE(k.issue_date) BETWEEN '$firstdate' AND '$lastdate'"; 
                break;

                case 2017:
                    // echo "Quarter: Four";exit;
                $first_month = "04";
                $last_month = "12";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND DATE(k.issue_date) BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                
                default:
                $new_criteria .= ($year > 0)?" AND YEAR(k.issue_date) = $year":NULL;
                    // echo "Quarter: Invalid";exit;
                break;
            }
            // $new_criteria .= " AND k.issue_date BETWEEN '$firstdate' AND '$lastdate'"; 

        }elseif (isset($month) && $month > 0) {
            $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');

            $firstdate = $year . '-' . $month . '-01';
            $lastdate = $year . '-' . $month . '-31';

            $new_criteria .= " AND DATE(k.issue_date) BETWEEN '$firstdate' AND '$lastdate'"; 

        }else{
            // echo "<pre>";print_r($year);exit;
            $new_criteria .= ($month > 0)?" AND MONTH(k.issue_date) = $month":NULL;
            $new_criteria .= ($year > 0)?" AND YEAR(k.issue_date) = $year":" AND YEAR(k.issue_date) = ".date('Y');
        }

        // $order_by = " ORDER BY k.district_id";

        switch ($by) {
            case 'county':
            $group_by = " GROUP BY c.id,k.commodity_id ";
            $columns = "SUM(k.qty_issued) AS qty_issued,SUM(k.qty_requested) AS qty_requested,SUM(k.value_issued) AS value_issued,SUM(k.value_requested) AS value_requested";
            break;
            case 'subcounty':
            $group_by = " GROUP BY d.id,k.commodity_id ";
            $columns = "SUM(k.qty_issued) AS qty_issued,SUM(k.qty_requested) AS qty_requested,SUM(k.value_issued) AS value_issued,SUM(k.value_requested) AS value_requested";
            break;
            case 'facility':
            $group_by = " GROUP BY f.facility_code,k.commodity_id,k.issue_date ";
            $columns = "SUM(k.qty_issued) AS qty_issued,SUM(k.qty_requested) AS qty_requested,SUM(k.value_issued) AS value_issued,SUM(k.value_requested) AS value_requested";
            // $columns = "k.qty_issued,k.qty_requested";
            break;
            case 'commodity':
            $group_by = " GROUP BY k.commodity_id ";
            $order_by = " ORDER BY k.issue_date DESC";
            // $columns = "k.qty_issued,k.qty_requested";
            $columns = "SUM(k.qty_issued) AS qty_issued,SUM(k.qty_requested) AS qty_requested,SUM(k.value_issued) AS value_issued,SUM(k.value_requested) AS value_requested";
            break;   
            case 'issue_date':
            $group_by = "  GROUP BY k.facility_code,k.commodity_id,k.issue_date ";
            $order_by = " ORDER BY k.issue_date DESC";
            $columns = "SUM(k.qty_issued) AS qty_issued,SUM(k.qty_requested) AS qty_requested,SUM(k.value_issued) AS value_issued,SUM(k.value_requested) AS value_requested";
            // $columns = "k.qty_issued,k.qty_requested";
            break;   
            default:
                // $group_by = "GROUP BY commodity_id ";
            $group_by = " GROUP BY k.facility_code,k.commodity_id,k.issue_date";
            $columns = "SUM(k.qty_issued) AS qty_issued,SUM(k.qty_requested) AS qty_requested,SUM(k.value_issued) AS value_issued,SUM(k.value_requested) AS value_requested";
            // $columns = "k.qty_issued,k.qty_requested";
            break;
        }
        // echo $group_by;exit;
        // $year = date('Y');

        // echo $new_criteria;exit;
        $new_criteria .= ($district_id > 0)?" AND d.id = $district_id":NULL;
        $new_criteria .= ($county_id > 0)?" AND c.id = $county_id":NULL;
        $new_criteria .= ($facility_code > 0)?" AND f.facility_code = $facility_code":NULL;
        // echo "<pre>";print_r($new_criteria);exit;
        $c_ids = (isset($commodity_id) && $commodity_id!='')? "'".$commodity_id."'" : "4,5";


        // $order_by = " ORDER BY k.issue_date, k.commodity_id";

        $query="
            SELECT 
            c.id AS county_id,
            c.county,
            d.id AS district_id,
            d.district,
            f.facility_name,
            f.facility_code,
            k.issue_date,
            k.commodity_id,
            k.delivery_note,
            $columns
            FROM
            kemsa_issues k,
            counties c,
            districts d,
            facilities f
            WHERE
            c.id = d.county
            AND f.district = d.id
            AND k.facility_code = f.facility_code
            AND k.commodity_id IN ($c_ids)
            $new_criteria
            $group_by $order_by;        
        ";
        // echo "<pre>";print_r($query);exit;

        $res = $this->db->query($query);
        // $res = $res->result_array();
        // echo "<pre>";print_r($res);exit;
        // echo "<pre>";print_r($by);exit;
        switch ($by) {
            case 'county':
                $data['first_date'] = $firstdate;
                $data['last_date'] = $lastdate;

            if ($res) {
                $res = $res->result_array();
                    // echo "<pre>";print_r($res);exit;
                foreach ($res as $key => $value) {
                    $county_id = $value['county_id'];
                    $data[$county_id]['facility_code'] = $value['facility_code'];
                    $data[$county_id]['county'] = $value['county'];
                    $data[$county_id]['county_id'] = $value['county_id'];
                    $data[$county_id]['district'] = $value['district'];
                    $data[$county_id]['district_id'] = $value['district_id'];
                    $data[$county_id]['commodity_id'] = $value['commodity_id'];
                    $data[$county_id]['issue_date'] = $value['issue_date'];
                    $data[$county_id]['qty_requested'] = $value['qty_requested'];
                    $data[$county_id]['qty_issued'] = $value['qty_issued'];
                    $data[$county_id]['delivery_note'] = $value['delivery_note'];
                        // array_push($data_, $data);
                }

            }


            break;
            case 'subcounty':
                $data['first_date'] = $firstdate;
                $data['last_date'] = $lastdate;

            if ($res) {
                $res = $res->result_array();
                    // echo "<pre>";print_r($res);exit;
                foreach ($res as $key => $value) {
                    $district_id = $value['district_id'];
                        // $qty_issued = $value['qty_issued'];

                    $data[$district_id]['facility_code'] = $value['facility_code'];
                    $data[$district_id]['county'] = $value['county'];
                    $data[$district_id]['county_id'] = $value['county_id'];
                    $data[$district_id]['district'] = $value['district'];
                    $data[$district_id]['district_id'] = $value['district_id'];
                    $data[$district_id]['commodity_id'] = $value['commodity_id'];
                    $data[$district_id]['issue_date'] = $value['issue_date'];
                    $data[$district_id]['qty_issued'] = $value['qty_issued'];
                    $data[$district_id]['qty_requested'] = $value['qty_requested'];
                    $data[$district_id]['delivery_note'] = $value['delivery_note'];

                        // array_push($data_, $data);
                }

            }

            break;
            case 'facility':
                $data['first_date'] = $firstdate;
                $data['last_date'] = $lastdate;
            if ($res) {
                $res = $res->result_array();
                    // echo "<pre>";print_r($beg_data);exit;
                // echo "<pre>";print_r($res);exit;
                foreach ($res as $key => $value) {
                    $mfl = $value['facility_code'];

                    $data[$mfl]['facility_code'] = $value['facility_code'];
                    $data[$mfl]['facility_name'] = $value['facility_name'];
                    $data[$mfl]['county'] = $value['county'];
                    $data[$mfl]['county_id'] = $value['county_id'];
                    $data[$mfl]['district'] = $value['district'];
                    $data[$mfl]['district_id'] = $value['district_id'];
                    $data[$mfl]['commodity_id'] = $value['commodity_id'];

                    $data[$mfl]['issue_date'] = $value['issue_date'];
                    $data[$mfl]['qty_issued'] = $value['qty_issued'];
                    $data[$mfl]['qty_requested'] = $value['qty_requested'];
                    $data[$mfl]['delivery_note'] = $value['delivery_note'];
                        // array_push($data_, $data);
                }

            }

            break;

            case 'facility_month_year':
                // $data['first_date'] = $firstdate;
                // $data['last_date'] = $lastdate;
            if ($res) {
                $res = $res->result_array();
                    // echo "<pre>";print_r($beg_data);exit;
                // echo "<pre>";print_r($res);exit;
                foreach ($res as $key => $value) {
                    $mfl = $value['facility_code'];
                    $m_yr = date('mY',strtotime($value['issue_date']));

                    // echo "<pre>";print_r($m_yr);exit;
                    $data[$m_yr][$mfl]['facility_code'] = $value['facility_code'];
                    $data[$m_yr][$mfl]['county'] = $value['county'];
                    $data[$m_yr][$mfl]['county_id'] = $value['county_id'];
                    $data[$m_yr][$mfl]['district'] = $value['district'];
                    $data[$m_yr][$mfl]['district_id'] = $value['district_id'];
                    $data[$m_yr][$mfl]['commodity_id'] = $value['commodity_id'];
                    $data[$m_yr][$mfl]['facility_name'] = $value['facility_name'];

                    $data[$m_yr][$mfl]['issue_date'] = $value['issue_date'];
                    $data[$m_yr][$mfl]['qty_issued'] = $value['qty_issued'];
                    $data[$m_yr][$mfl]['qty_requested'] = $value['qty_requested'];
                    $data[$m_yr][$mfl]['delivery_note'] = $value['delivery_note'];
                        // array_push($data_, $data);
                }

            }

            break;

            case 'commodity':
            if ($res) {
                $res = $res->result_array();
                    // echo "<pre>";print_r($beg_data);exit;
                foreach ($res as $key => $value) {
                    $commodity_id = $value['commodity_id'];

                    $data[$commodity_id]['facility_code'] = $value['facility_code'];
                    $data[$commodity_id]['county'] = $value['county'];
                    $data[$commodity_id]['county_id'] = $value['county_id'];
                    $data[$commodity_id]['district'] = $value['district'];
                    $data[$commodity_id]['district_id'] = $value['district_id'];
                    $data[$commodity_id]['commodity_id'] = $value['commodity_id'];

                    $data[$commodity_id]['issue_date'] = $value['issue_date'];
                    $data[$commodity_id]['qty_issued'] = $value['qty_issued'];
                    $data[$commodity_id]['qty_requested'] = $value['qty_requested'];
                    $data[$commodity_id]['delivery_note'] = $value['delivery_note'];

                        // array_push($data_, $data);
                }

            }

            break;

            default:
            if ($res) {
                $res = $res->result_array();
            }else{
                $res = NULL;
            }
            $data = $res;

            break;
        }

        // echo "<pre>";print_r($data);exit;
        if (isset($format) && $format == "json") {
            $data = json_encode($data,JSON_UNESCAPED_SLASHES);
        }

        return $data;
    }

    public function send_issues_email($county_id = NULL, $district_id = NULL, $facility_code = NULL, $month = NULL,$year = NULL,$quarter = NULL,$testing = NULL)
    {
        // echo "COUNTY: ".$county_id." DISTRICT ID:".$district_id." MFL".$facility_code." COMMODITY: ".$commodity_id." QUARTER: ".$quarter." BY: ".$by." YEAR: ".$year." MONTH: ".$month;exit;

        if ($quarter < 1 && $year < 1 && $month < 1) {
            echo "<pre>";print_r("NO QUARTER, MONTH OR YEAR PROVIDED.");exit;
        }
        $current_month = date('m');
        $current_year = date('Y');
        $year = (isset($year) && is_numeric($year) && $year>0)? $year:$current_year;
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:$current_month;

        $monthyear = $year . '-' . $month . '-01';
        $englishdate = date('F Y', strtotime($monthyear));
        $county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
        $district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
        $facility_code = (isset($facility_code) && $facility_code!='')? $facility_code:NULL;
        $by = (isset($by) && $by!='')? $by:NULL;

        $c_where .= (isset($county_id) && $county_id > 0) ? " WHERE id = $county_id" : NULL;
        $c_query = "SELECT * FROM counties $c_where";
        $counties = $this->db->query($c_query)->result_array();
        // echo "<pre>";print_r($counties);exit;
        $output = array();
        // echo "<pre>";print_r($monthyear);exit;
        foreach ($counties as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $c_id = $value['id'];
            $c_name = $value['county'];

            $y = date('Y',strtotime($monthyear));
            $m = date('m',strtotime($monthyear));

            $q_data = $this->get_year_quarter($m,NULL,$y);

            $q = $q_data['quarter'];

            $sent_q = "SELECT COUNT(*) AS count FROM kemsa_issue_emails WHERE quarter = '".$q."' AND year = '".$y."' AND county_id = $c_id";

            // echo $sent_q;exit;

            $sent_check = $this->db->query($sent_q)->result_array();
            // echo "<pre>";print_r($sent_check);exit;
            // echo $sent_check[0]['count'];exit;
            $sent_count = $sent_check[0]['count'];
            // $sent_count = 0;
            if ($sent_count < 1) {
                $send_mail = $this->send_issues_email_($c_id, $district_id, $facility_code, $month,$year,$quarter,$testing);
                $output[]="MONTH: ".$month." YEAR: ".$year." EMAIL SENT TO: ".$c_id." ".$c_name." COUNTY";
            }else{
                $output[]="MONTH: ".$month." YEAR: ".$year." quarter: ".$quarter." EMAIL WAS ALREADY SENT TO: ".$c_id." ".$c_name." COUNTY";
            }
        }

        echo "<pre>";print_r($output);exit;
        return $output;
    }

    public function send_issues_email_($county_id = NULL, $district_id = NULL, $facility_code = NULL, $month = NULL,$year = NULL,$quarter = NULL,$testing = NULL){
        // echo "COUNTY: ".$county_id." DISTRICT ID:".$district_id." MFL".$facility_code." COMMODITY: ".$commodity_id." QUARTER: ".$quarter." BY: ".$by." YEAR: ".$year." MONTH: ".$month." TESTING: ".$testing;exit;
        $current_month = date('m');
        $current_year = date('Y');
        $year = (isset($year) && is_numeric($year) && $year>0)? $year:$current_year;
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:$current_month;
        $testing = (isset($testing) && $testing>0)? $testing:0;
        $monthyear = $year . '-' . $month . '-01';
        $englishdate = date('F Y', strtotime($monthyear));
        $prev_englishdate = date('F Y', strtotime("-1 MONTH",strtotime($monthyear)));
        $prev_month = date('m', strtotime("-1 MONTH",strtotime($monthyear)));
        $prev_year = date('Y', strtotime("-1 MONTH",strtotime($monthyear)));
        // echo "<pre>";print_r($prev_year);exit;
        // $allocation_date = date('F Y', strtotime($monthyear,"-1 "));
        // echo "<pre>";print_r($prev_englishdate);exit;
        /*if ($county_id > 15) {
            echo "15 REACHED";exit;
        }*/

        $reportname = '';
        $c_where .= (isset($county_id) && $county_id > 0) ? " WHERE id = $county_id" : NULL;
        $c_query = "SELECT * FROM counties $c_where";
        $counties = $this->db->query($c_query)->result_array();
        // echo "<pre>";print_r($counties);exit;
        if (isset($county_id) && $county_id > 0) {
            $reportname .= $counties[0]['county'].' County ';     
            $team_name = $counties[0]['county'].' County ';     
        }
        // echo "<pre>";print_r($reportname);exit;
        
        $reportname .= 'KEMSA Issues for ' . $englishdate;

        $html_title = "<div ALIGN=CENTER><img src='" . base_url() . "assets/img/coat_of_arms.png' height='70' width='70'style='vertical-align: top;' > </img></div>
            <div style='text-align:center; font-family: arial,helvetica,clean,sans-serif;display: block; font-weight: bold; font-size: 14px;'>     Ministry of Health</div>
            <div style='text-align:center; font-family: arial,helvetica,clean,sans-serif;display: block; font-weight: bold;display: block; font-size: 13px;'>Health Commodities Management Platform</div>
            <div style='text-align:center; font-size: 14px;display: block;font-weight: bold;'>Rapid Test Kits (RTK) System</div>
            <div style='text-align:center; font-size: 14px;display: block;font-weight: bold;'>National Aggregate Stock Card  for $englishdate</div><hr />

            <style>table.data-table {border: 1px solid #DDD;font-size: 13px;border-spacing: 0px;}
            table.data-table th {border: none;color: #036;text-align: center;background-color: #F5F5F5;border: 1px solid #DDD;border-top: none;max-width: 450px;}
            table.data-table td, table th {padding: 4px;}
            table.data-table td {border: none;border-right: 1px solid #DDD;height: 30px;margin: 0px;border-bottom: 1px solid #DDD;}
            .col5{background:#D8D8D8;}</style>";
        $table_head = '
            <table border="0" class="data-table" style="width: 100%; margin: 10px auto;">
            <thead border="0" style="margin: 10px auto;font-weight:900">
            <tr>
                <th colspan = "1" rowspan = "2">Facility Code</th>
                <th colspan = "1" rowspan = "2">Facility Name</th>
                <th colspan = "1" rowspan = "2">County</th>
                <th colspan = "1" rowspan = "2">Sub-County</th>
                <th colspan = "3">Screening (Determine)</th>
                <th colspan = "3">Confirmatory (First Response)</th>
            </tr>
            <tr>
                <th>Issued</th>
                <th>Allocated</th>
                <th>Difference</th>
                
                <th>Issued</th>
                <th>Allocated</th>
                <th>Difference</th>
            </tr>
            </thead>
            <tbody>';
        $table_body = '';

        $fac_arr = $fac_arr_ = array();

        
        if ($quarter > 0) {
            $month_ = NULL;
        }else{
            $month_ = $month;
        }

        $quarter_data = $this->get_year_quarter($month,NULL,$year,NULL);
        $quarter = $quarter_data['quarter'];

        foreach ($counties as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $c_id = $value['id'];
            $issue_data = $this->get_kemsa_issue_data($county_id,$district_id,$facility_code,$commodity_id,$quarter,$year,$month_);
            // echo "<pre>";print_r($issue_data);exit;
            // $allocation_data = $this->get_allocation_data($county_id,$district_id,$facility_code,$commodity_id,$quarter,$prev_year,$prev_month);
            // echo "<pre>";print_r($quarter_data);exit;
            // $m_ = (isset($month) && )
            $allocation_data = $this->get_allocation_data($county_id,$district_id,$facility_code,$commodity_id,$quarter,$year,$month_);
            // echo "<pre>";print_r($allocation_data);
            // echo "<pre>";print_r($issue_data);exit;
            //CURRENT DEBUG
            $count = count($issue_data);

            foreach ($issue_data as $keyy => $valuee) {
                $mfl_ = $valuee['facility_code'];
                $fac_name_ = $valuee['facility_name'];
                $county_ = $valuee['county'];
                $district_ = $valuee['district'];
                $com_id_ = $valuee['commodity_id'];
                $issued_ = $valuee['qty_issued'];

                $fac_arr[$mfl_]['facility_code'] = $mfl_;
                $fac_arr[$mfl_]['facility_name'] = $fac_name_;
                $fac_arr[$mfl_]['county'] = $county_;
                $fac_arr[$mfl_]['district'] = $district_;
                $fac_arr[$mfl_]['issue_date'] = $valuee['issue_date'];

                /*$fac_arr[$mfl_][4] = 0;
                $fac_arr[$mfl_][5] = 0;*/
                
                // echo "<pre>".$mfl_." comm_id_ ".$com_id_." issued_ ".$issued_;
                $fac_arr[$mfl_][$com_id_] += $issued_; 
                $fac_arr[$mfl_]['issue_count'][$com_id_] += 1; 
            }
        }

        // echo "<pre>";print_r($fac_arr);exit;
        foreach ($fac_arr as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $f_code = $value['facility_code'];
            if (empty($allocation_data[$f_code])) {
                
            }else{
                $allocated_s = $allocated_c = 0;
                $allocated_s = $allocation_data[$f_code]['allocate_s_packs'];
                $allocated_c = $allocation_data[$f_code]['allocate_c_packs'];
                $district_name = $allocation_data[$f_code]['district_name'];
                $district_name = (isset($district_name) && $district_name !='')? $district_name :$fac_arr[$f_code]['district'];

                $table_body .= '<tr>';
                $table_body .= '<td>' . $value['facility_code'] . '</td>';
                $table_body .= '<td>' . $value['facility_name'] . '</td>';
                $table_body .= '<td>' . $value['county'] . '</td>';
                $table_body .= '<td>' . $district_name . '</td>';
                // $table_body .= '<td>' . "Screening" . '</td>';
                $kits_issued_s = round($value[4] / 100);
                $kits_issued_c = round($value[5] / 30);

                /*$diff_s = $allocated_s - $kits_issued_s;
                $diff_c = $allocated_c - $kits_issued_c;*/

                $diff_s = $kits_issued_s - $allocated_s;
                $diff_c = $kits_issued_c - $allocated_c;

                $style_green = ' style = "color:green;font-weight:bold;" ';
                $style_red = ' style = "color:red;font-weight:bold;" ';

                $styling_s = ($diff_s>0)? $style_green:$style_red;
                $styling_s = ($diff_s==0)? "":$styling_s;

                $styling_c = ($diff_s>0)? $style_green:$style_red;
                $styling_c = ($diff_c==0)? "":$styling_c;

                $table_body .= '<td>' . number_format($kits_issued_s) . '</td>';
                $table_body .= '<td>' . number_format($allocated_s) . '</td>';
                $table_body .= '<td "'.$styling_s.'">' . number_format($diff_s) . '</td>';

                $table_body .= '<td>' . number_format($kits_issued_c) . '</td>';
                $table_body .= '<td>' . number_format($allocated_c) . '</td>';
                $table_body .= '<td "'.$styling_c.'">' . number_format($diff_c) . '</td>';

                $table_body .= '</tr>';
            }
        }//issue data foreach

        // echo "<pre>";print_r($table_body);exit;
        $table_foot = '</tbody></table>';
        $table_data = $table_head. $table_body. $table_foot;
        $html_data = $html_title . $table_head . $table_body . $table_foot;
        // echo "<pre>";print_r($table_data);exit;
        $pdf_title = "<div style='text-align:center; font-size: 14px;display: block;font-weight: bold;'>Rapid Test Kits (RTK) System</div>
        <div style='text-align:center; font-size: 14px;display: block;font-weight: bold;'>KEMSA Issues for $englishdate</div>
        <div style='text-align:center; font-size: 14px;display: block;font-weight: bold;'>$team_name</div>";
        
        // echo "$html_data";die();
        // echo "<pre>";print_r($html_data);exit;

        // $pdf = $this->create_pdf($html_data, $reportname);
        $pdf_data['pdf_html_body'] = $table_data;
        $pdf_data['file_name'] = $reportname;
        $pdf_data['pdf_title'] = $pdf_title;
        $pdf_data['pdf_water_mark'] = 'KEMSA ISSUES';
        $pdf_data['pdf_view_option'] = "save_file";
        $pdf_data['pdf_save_location'] = "tmp/";
        // echo "<pre>";print_r($pdf_data);exit;
        $pdf = $this->hcmp_functions->create_pdf($pdf_data);
        // echo "<pre>";print_r($pdf);exit;
        //echo "<pre>";print_r($pdf);exit;
        // $email_address,$message,$subject,$attach_file=NULL,$bcc_email=NULL,$cc_email=NULL,$full_name=NULL

        $message = '<b>Dear '.$team_name.' Team,</b><br/></br/>Find attached the KEMSA issues list for your sites for  ' . $englishdate.' (For '.$prev_englishdate.' allocations).';
        // $message .= 'Kindly ignore the issues list sent on 7th November 2017';
        $message .= '<br>In case of any issues/concerns ,kindly reach out to the key contact persons at NASCOP & KEMSA.<br><br>Thanks.<br>--<br>RTK Support Team<br><b><small>This email was automatically generated. Please do not respond to this email address or it will be ignored.</small></b>';

        
        // echo "<pre>";print_r($testing);exit;
        // $testing = 1;
        if ($testing > 0) {
            $email_address = 'karsanrichard@gmail.com';
            $cc_email = 'tngugi@clintonhealthaccess.org';
            $bcc_email = 'sethrichard40@gmail.com';
        }else{
            $recepients = $this->get_email_recepients($county_id,1);
            
            $email_address = $recepients['main'];
            $cc_email = $recepients['cc'];
            $bcc_email = $recepients['bcc'];
        }

        // $email_address = 'karsanrichard@gmail.com';
        // $cc_email = 'tngugi@clintonhealthaccess.org';
        // $bcc_email = 'sethrichard40@gmail.com';

        // echo $email_address." ".$cc_email." ".$bcc_email;exit;
        //CURRENT DEBUG
        $subject = $team_name.' RTK KEMSA Issues for '. $englishdate;
        $file_path = FCPATH.'tmp/'.$reportname.'.pdf';
        // $attach_file = array('attach_file' => $file_path);
        $attach_file = $file_path;
        // echo 'HERE';exit;
        $fac_arr_count = count($fac_arr);
        // echo "<pre>";print_r($fac_arr_count);exit;
        // $fac_arr_count = 0;

        if ($fac_arr_count>0) {
            
            $send_mail =    $this->hcmp_functions->send_email($email_address,$message,$subject,$attach_file,$bcc_email,$cc_email,$full_name);
            // echo "<pre>";print_r($send_mail);exit;

            $y = date('Y',strtotime($monthyear));
            $m = date('m',strtotime($monthyear));

            $q_data = $this->get_year_quarter($m,NULL,$y);
            // echo "<pre>";print_r($issue_date);
            // echo "<pre>";print_r($q_data);exit;
            $q = $q_data['quarter'];

            $data = array(
               'county_id' => $county_id,
               'issue_date' => $monthyear,
               'quarter' => $q,
               'month' => $month,
               'year' => $year,
               'date_sent' => date('Y-m-d'),
               'sent_to' => $email_address,
               'email_status' => 1
            );

            // echo "<pre>";print_r($data);exit;

            $insert = $this->db->insert('kemsa_issue_emails', $data); 
        }

        return $data;
    }

    public function send_defined_email($county_id = NULL, $district_id = NULL, $facility_code = NULL, $month = NULL,$year = NULL,$quarter = NULL)
    {
        $current_month = date('m');
        $current_year = date('Y');
        $year = (isset($year) && is_numeric($year) && $year>0)? $year:$current_year;
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:$current_month;
        $monthyear = $year . '-' . $month . '-01';
        $englishdate = date('F Y', strtotime($monthyear));
        $county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
        $district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
        $facility_code = (isset($facility_code) && $facility_code!='')? $facility_code:NULL;
        $by = (isset($by) && $by!='')? $by:NULL;

        $c_where .= (isset($county_id) && $county_id > 0) ? " WHERE id = $county_id" : NULL;
        $c_query = "SELECT * FROM counties $c_where";
        $counties = $this->db->query($c_query)->result_array();
        $output = array();
        // echo "<pre>";print_r($monthyear);exit;
        foreach ($counties as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $c_id = $value['id'];
            $c_name = $value['county'];

            $send_mail = $this->send_defined_email_($c_id, $district_id, $facility_code, $month,$year,$quarter);
            $output[]="MONTH: ".$month." EMAIL SENT TO: ".$c_id." COUNTY";
            // echo "<pre>";print_r($output);exit;
        }

        echo "<pre>";print_r($output);exit;
    }

    public function send_defined_email_($county_id = NULL, $district_id = NULL, $facility_code = NULL, $month = NULL,$year = NULL,$quarter = NULL){
        $current_month = date('m');
        $current_year = date('Y');
        $year = (isset($year) && is_numeric($year) && $year>0)? $year:$current_year;
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:$current_month;
        $monthyear = $year . '-' . $month . '-01';
        $englishdate = date('F Y', strtotime($monthyear));
        $prev_englishdate = date('F Y', strtotime("-1 MONTH",strtotime($monthyear)));
        $prev_month = date('m', strtotime("-1 MONTH",strtotime($monthyear)));
        // echo "<pre>";print_r($prev_month);exit;
        // $allocation_date = date('F Y', strtotime($monthyear,"-1 "));
        // echo "<pre>";print_r($prev_englishdate);exit;
        /*if ($county_id > 15) {
            echo "15 REACHED";exit;
        }*/

        $reportname = '';
        $c_where .= (isset($county_id) && $county_id > 0) ? " WHERE id = $county_id" : NULL;
        $c_query = "SELECT * FROM counties $c_where";
        $counties = $this->db->query($c_query)->result_array();

        if (isset($county_id) && $county_id > 0) {
            $reportname .= $counties[0]['county'].' County ';     
            $team_name = $counties[0]['county'].' County ';     
        }
        // echo "<pre>";print_r($reportname);exit;
        
        $message = '<b>Dear '.$team_name.' Team,</b><br/>';
        $message .= '<br>Kindly ignore the issues list sent on 24th January 2018.';
        $message .= '<br>An updated list will be sent to you as soon as possible.';
        /*$message .= '
            <br>The HCMP System has been opened for January 2018 Allocation to begin.   This exercise will run from December 19th 2017 to January 9th 2018.
            <br>Please note this exercise has been flagged early so as to give you ample time to do a good job.
            <br>
            
            <br><b>To Allocate :</b><br>
                I.      County Administrators need to log in<br>

                II.     Click on ‘ Drawing Rights’ Link on the  Menu<br>

                III.    Your Available Balance for January 2018 allocation will be displayed<br>

                IV.     Click on View Distribution<br>

                V.      Distribute the Available Drawing Rights to your sub counties ( this is a one time activity for all sub counties )<br>

                VI.     Click on ‘Save Drawing Rights’<br>
         
            <br>

                I.      SCMLTs can now log in<br>

                II.     Click on ‘Click to Allocate’ Link<br>

                III.    Proceed to Allocate quantities to your facilities<br>

                IV.     Click ‘Save’ to save your allocation<br>

            <br><b>To Approve Sub County Allocations:</b><br>
                I.      County Administrator log in<br>

                II.     Click on RTK Allocation List<br>

                III.    Click on  Green View/Edit Button on the January 2018 Allocation Date<br>

                IV.     Click on Green View/Verify  Allocation Button on individual sub counties and verify their allocations<br>

                V.      If all is well, Click on  Approve then Click on Save<br>

                VI.     If there is an issue, add your comments then click on Rejected then click on Save<br>

            <br><b>To Edit Sub County Allocations:</b><br>

                I.      Log in as SCMLT<br>

                II.     Click on ‘ Allocation List’<br>

                III.    Click on  Green View/Edit Button on the January 2018 Allocation Date<br>

                IV.     Your allocation will be loaded, make changes required then<br>

                V.      Click On Save<br>';*/

        if ($county_id == 1) {
            /*$message .='
            <br>Kindly note Kenyatta National Hospital should not be allocated for as they have automatically been allocated the fixed amount of  34300 tests for  screening  and 3420 tests for confirmatory.
            <br>Thus as you distribute drawing rights for Langata Sub county, do not factor in KNH.<br><br>';*/
        }

        if ($county_id == 44) {
            /*$message .='
            <br>Kindly note Moi Teaching and Referral Hospital should not be allocated for as they have automatically been allocated the fixed amount of 20600  tests for  screening  and 2070 tests for confirmatory.
            <br>Thus as you distribute drawing rights for Ainabkoi Sub county, do not factor in MTRH.<br><br>';*/
        }

        /*$message .='<br>In case of any issues/concerns, kindly reach out to the key contact persons for clarification.
        <br>Thanks.
        <br>--
        <br><b>RTK Support Team.</b>
        <br><b><small>This email was automatically generated. Please do not respond to this email address or it will be ignored.</small></b>';*/

        $message .='<br><br>Thanks.
        <br>--
        <br><b>RTK Support Team.</b>
        <br><b><small>This email was automatically generated. Please do not respond to this email address or it will be ignored.</small></b>';
        // echo "<pre>";print_r($message);exit;
        $recepients = $this->get_email_recepients($county_id,1);
        // $recepients = $this->get_email_recepients();
        // echo "<pre>";print_r($recepients);exit;
        
        $email_address = $recepients['main'];
        $cc_email = $recepients['cc'];
        $bcc_email = $recepients['bcc'];
        

        // $email_address = 'karsanrichard@gmail.com';
        // $cc_email = 'tngugi@clintonhealthaccess.org';
        // $bcc_email = 'sethrichard40@gmail.com';

        // $subject = 'January 2018 RTK Issues';
        $subject = $team_name.' RTK KEMSA Issues for '. $englishdate;
        // echo $subject;exit;

        $attach_file = NULL;
        // $send_mail = $this->hcmp_functions->send_email($email_address,$message,$subject,$attach_file,$bcc_email,$cc_email,$full_name);
        
    }

    public function send_allocations_email($county_id = NULL, $district_id = NULL, $facility_code = NULL, $month = NULL,$year = NULL,$quarter = NULL)
    {
        $current_month = date('m');
        $current_year = date('Y');
        $year = (isset($year) && is_numeric($year) && $year>0)? $year:$current_year;
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:$current_month;
        $monthyear = $year . '-' . $month . '-01';
        $englishdate = date('F Y', strtotime($monthyear));
        $county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
        $district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
        $facility_code = (isset($facility_code) && $facility_code!='')? $facility_code:NULL;
        $by = (isset($by) && $by!='')? $by:NULL;

        $c_where .= (isset($county_id) && $county_id > 0) ? " WHERE id = $county_id" : NULL;
        $c_query = "SELECT * FROM counties $c_where";
        $counties = $this->db->query($c_query)->result_array();
        $output = array();
        // echo "<pre>";print_r($monthyear);exit;
        foreach ($counties as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $c_id = $value['id'];
            $c_name = $value['county'];

            // echo "<pre>";print_r($c_name);exit;
            $send_mail = $this->send_allocations_email_($c_id, $district_id, $facility_code, $month,$year,$quarter);
            $output[]="MONTH: ".$month." EMAIL SENT TO: ".$c_id." COUNTY";
            // echo "<pre>";print_r($output);exit;
        }

        echo "<pre>";print_r($output);exit;
    }

    public function send_allocations_email_($county_id = NULL, $district_id = NULL, $facility_code = NULL, $month = NULL,$year = NULL,$quarter = NULL){
        $current_month = date('m');
        $current_year = date('Y');
        $year = (isset($year) && is_numeric($year) && $year>0)? $year:$current_year;
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:$current_month;
        $monthyear = $year . '-' . $month . '-01';
        $englishdate = date('F Y', strtotime($monthyear));
        $prev_englishdate = date('F Y', strtotime("-1 MONTH",strtotime($monthyear)));
        $prev_month = date('m', strtotime("-1 MONTH",strtotime($monthyear)));
        // echo "<pre>";print_r($prev_month);exit;
        // $allocation_date = date('F Y', strtotime($monthyear,"-1 "));
        // echo "<pre>";print_r($prev_englishdate);exit;
        /*if ($county_id > 15) {
            echo "15 REACHED";exit;
        }*/

        $reportname = '';
        $c_where .= (isset($county_id) && $county_id > 0) ? " WHERE id = $county_id" : NULL;
        $c_query = "SELECT * FROM counties $c_where";
        $counties = $this->db->query($c_query)->result_array();

        if (isset($county_id) && $county_id > 0) {
            $reportname .= $counties[0]['county'].' County ';     
            $team_name = $counties[0]['county'].' County ';     
        }
        // echo "<pre>";print_r($reportname);exit;
        
        $message = '<b>Dear '.$team_name.' Team,</b><br/>';
        // $message .= '<br>Kindly ignore the issues list sent on 24th January 2018.';
        // $message .= '<br>An updated list will be sent to you as soon as possible.';
        $message .= '
            <br>The HCMP System has been opened for April 2018 Allocation to begin. This exercise will run
                from March 19th 2018 to April 2nd 2018, then have the National TWG review the orders from
                2nd to 9th April ,after which KEMSA will begin distribution of the kits.
            <br>Please note this exercise has been flagged early so as to give you ample time to do a good job.
            <br>
            
            <br><b>To Allocate :</b><br>
                I.      County Administrators need to log in<br>

                II.     Click on ‘ Drawing Rights’ Link on the  Menu<br>

                III.    Your Available Balance for January 2018 allocation will be displayed<br>

                IV.     Click on View Distribution<br>

                V.      Distribute the Available Drawing Rights to your sub counties ( this is a one time activity for all sub counties )<br>

                VI.     Click on ‘Save Drawing Rights’<br>
         
            <br>

                I.      SCMLTs can now log in<br>

                II.     Click on ‘Click to Allocate’ Link<br>

                III.    Proceed to Allocate quantities to your facilities<br>

                IV.     Click ‘Save’ to save your allocation<br>

            <br><b>To Approve Sub County Allocations:</b><br>
                I.      County Administrator log in<br>

                II.     Click on RTK Allocation List<br>

                III.    Click on  Green View/Edit Button on the April 2018 Allocation Date<br>

                IV.     Click on Green View/Verify  Allocation Button on individual sub counties and verify their allocations<br>

                V.      If all is well, Click on  Approve then Click on Save<br>

                VI.     If there is an issue, add your comments then click on Rejected then click on Save<br>

            <br><b>To Edit Sub County Allocations:</b><br>

                I.      Log in as SCMLT<br>

                II.     Click on ‘ Allocation List’<br>

                III.    Click on  Green View/Edit Button on the April 2018 Allocation Date<br>

                IV.     Your allocation will be loaded, make changes required then<br>

                V.      Click On Save<br>';

        if ($county_id == 1) {
            $message .='
            <br>Kindly note Kenyatta National Hospital should not be allocated for as they have automatically been allocated the fixed amount of  34300 tests for  screening  and 3420 tests for confirmatory.
            <br>Thus as you distribute drawing rights for Langata Sub county, do not factor in KNH.<br><br>';
        }

        if ($county_id == 44) {
            $message .='
            <br>Kindly note Moi Teaching and Referral Hospital should not be allocated for as they have automatically been allocated the fixed amount of 20600  tests for  screening  and 2070 tests for confirmatory.
            <br>Thus as you distribute drawing rights for Ainabkoi Sub county, do not factor in MTRH.<br><br>';
        }

        $message .='<br>In case of any issues/concerns, kindly reach out to the key contact persons for clarification.
        <br>Thanks.
        <br>--
        <br><b>RTK Support Team.</b>
        <br><b><small>This email was automatically generated. Please do not respond to this email address as it will be ignored.</small></b>';
        // echo "<pre>";print_r($message);exit;
        $recepients = $this->get_email_recepients($county_id,1);
        // $recepients = $this->get_email_recepients();
        // echo "<pre>";print_r($recepients);exit;
        
        $email_address = $recepients['main'];
        $cc_email = $recepients['cc'];
        $bcc_email = $recepients['bcc'];
        

        // $email_address = 'karsanrichard@gmail.com';
        // $cc_email = 'tngugi@clintonhealthaccess.org';
        // $bcc_email = 'sethrichard40@gmail.com';

        /*echo "<pre>";print_r($email_address);
        echo "<pre>";print_r($cc_email);
        echo "<pre>";print_r($bcc_email);
        exit;*/
        // $subject = 'January 2018 RTK Issues';
        // $subject = $team_name.' RTK Allocations for '. $englishdate;
        $subject = $team_name.' HCMP Allocations April 2018';
        // echo $subject;exit;

        $attach_file = NULL;
        $send_mail = $this->hcmp_functions->send_email($email_address,$message,$subject,$attach_file,$bcc_email,$cc_email,$full_name);
        
    }

    public function get_email_recepients($county_id,$cc_level = NULL)
    {   
        $query = "SELECT * FROM user WHERE county_id = $county_id";
        $users = $this->db->query($query)->result_array();

        $rca_query = "
            SELECT 
                u.*,rca.county AS rca_county
            FROM
                user u,
                rca_county rca
            WHERE
                u.id = rca.rca AND rca.county = $county_id";
        $rca_users = $this->db->query($rca_query)->result_array();
        // echo "<pre>";print_r($rca_users);exit;
        $users = array_merge($users,$rca_users);
        // echo "<pre>";print_r($users);exit;
        $main = $cc = $bcc = NULL;
        // echo "<pre>";print_r($cc_level);exit;
        switch ($cc_level) {
            case 1://KEMSA ISSUES
                $cc .= 'nascoplabcommodities@gmail.com,peter.mwangi@kemsa.co.ke,douglas.onyancha@kemsa.co.ke,bedan.wamuti@kemsa.co.ke,samuel.wataku@kemsa.co.ke,davis.okoth@kemsa.co.ke,caroline.kasera@kemsa.co.ke,jlusike@clintonhealthaccess.org,solwande@clintonhealthaccess.org,nsalim@fieldcoordinationteam.org,tngugi@clintonhealthaccess.org,karsanrichard@gmail.com,jbatuka@usaid.gov,nii6@cdc.gov,Meq7@cdc.gov,hoy4@cdc.gov,ootieno@usaid.gov,Akinoti@usaid.gov,raphael.langat@usamru-k.org,hoz1@cdc.gov,vojiambo@usaid.gov,korosdk@state.gov,uys7@cdc.gov,ivq4@cdc.gov,smnjogo2012@gmail.com,JOndigo@mgic.umaryland.edu,ojoshua@mgic.umaryland.edu,';
                break;
            
            case 2://CHAI INTERNAL
                $cc .= 'jlusike@clintonhealthaccess.org,solwande@clintonhealthaccess.org,tngugi@clintonhealthaccess.org,karsanrichard@gmail.com,';
                break;

            case 3://NASCOP&CDC&CHAI
                $cc .= 'nascoplabcommodities@gmail.com,jlusike@clintonhealthaccess.org,solwande@clintonhealthaccess.org,nsalim@fieldcoordinationteam.org,tngugi@clintonhealthaccess.org,karsanrichard@gmail.com,jbatuka@usaid.gov,nii6@cdc.gov,Meq7@cdc.gov,hoy4@cdc.gov,ootieno@usaid.gov,Akinoti@usaid.gov,raphael.langat@usamru-k.org,hoz1@cdc.gov,vojiambo@usaid.gov,korosdk@state.gov,uys7@cdc.gov,ivq4@cdc.gov,smnjogo2012@gmail.com,ivq4@cdc.gov,';
                break;

            default:
                $cc .= 'karsanrichard@gmail.com,tngugi@clintonhealthaccess.org,';
                
                break;
        }
        
        // echo "<pre>";print_r($cc);exit;
        foreach ($users as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $usertype = $value['usertype_id'];
            $email = rtrim($value['email']);
            switch ($usertype) {
                case 13://CLC 
                $main .= $email.",";
                    break;
                case 5:
                $cc .= $email.",";
                    
                    break;
                case 3:
                $cc .= $email.",";
                    
                    break;
                // $main = $cc;
                default:
                    
                    break;
            }
        }

        $data['main'] = rtrim($main ,',');
        $data['cc'] = rtrim($cc ,',');
        $data['bcc'] = rtrim($bcc ,',');

        // echo "<pre>";print_r($data);exit;
        return $data;
    }

    public function get_opening_and_closing_balances($county_id = NULL, $district_id = NULL, $facility_code = NULL, $commodity_id = NULL, $beg_date = NULL,$end_date = NULL,$quarter = NULL,$by = NULL,$month = NULL,$year = NULL){
        /*
            KARSAN
            IF MONTH = JANUARY, PICK LAST YEAR DATA AS DATA IS GOTTEN ON A -1 MONTH BASIS.
        */
        // echo "COUNTY: ".$county_id." DISTRICT_ID: ".$district_id." MFL: ".$facility_code." BY: ".$by;
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter." beg_date: ".$beg_date." end_date: ".$end_date;exit;
        $county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
        $district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
        $facility_code = (isset($facility_code) && $facility_code!='')? $facility_code:NULL;
        $by = (isset($by) && $by!='')? $by:NULL;
        // $year = (isset($year) && is_numeric($year) && $year>0)? $year:NULL;
        $year = (isset($year) && is_numeric($year) && $year>0)? $year:NULL;
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:NULL;
        $cur_year = date('Y');
        $curr_month = date('m');
        // $beg_date = (isset($beg_date) && $beg_date!='')? $beg_date:date('Y-m-d',strtotime(time));
        // echo "<pre>";print_r($quarter);exit;

        if (isset($month) && $month > 0) {
            $year_ = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');

            $lastdate = $end_date;
            // $year_ = date('Y',strtotime($beg_date));
            // $month_ = date('m',strtotime($beg_date));
            $beg_date = $year_."-".$month."-01";
            $end_date = $year_."-".$month."-31";
        }

        // echo $beg_date.' '.$end_date;exit;

        if (isset($beg_date) && $beg_date!='' && $beg_date!='NULL') {
            $lastdate = $end_date;
            $year_ = date('Y',strtotime($beg_date));
            $month_ = date('m',strtotime($beg_date));

            if (date('m') == 1 && date('m') == $month_) {
                $year_ = date('Y',strtotime("-1 YEAR"));
            }else{
                // $month_ = date('m',strtotime("-1 MONTH",strtotime($beg_date)));
                // $year_ = date('Y',strtotime("-1 MONTH",strtotime($beg_date)));

                $month_ = date('m',strtotime($beg_date));
                $year_ = date('Y',strtotime($beg_date));
            }

            // echo "<pre>";print_r($month_);
            // echo "<pre>";print_r($year_);exit;
            // $firstdate = $year . '-' . $first_month . '-01';
            // $lastdate = $year_ . '-' . $prev_month . '-31';
            $beg_start_period = $year_ . '-' . $month_ . '-01';
            $beg_end_period = $year_ . '-' . $month_ . '-31';
        }else{
            // $curr_month = date('m');
            $year_ = (isset($year) && $year > 0)? $year:date('Y');
            
            if ($curr_month == 1) {
                // echo "JANUARY";exit;
                $year_ = date('Y',strtotime("-1 YEAR"));
                // echo "<pre>";print_r($year_);exit;
            }

            $first_month = "01";
            $prev_month = date('m',strtotime("-1 MONTH"));

            if ($year_ < $cur_year) {
                $prev_month = 12;
            }
            

            // $firstdate = $year . '-' . $first_month . '-01';
            // $lastdate = $year . '-' . $prev_month . '-31';

            $beg_start_period = $year_ . '-' . $first_month . '-01';
            $beg_end_period = $year_ . '-' . $first_month . '-31';
        }

        if (isset($end_date) && $end_date!='' && $end_date!='NULL') {
            // echo "END DATE IF";exit;
            // echo "<pre>";print_r($end_date);exit;
            $lastdate = $end_date;
            // $lastday = date('t',strtotime($beg_date));
            // echo "<pre>";print_r($lastday);exit;
            $year_ = date('Y',strtotime($end_date));
            $month_ = date('m',strtotime($end_date));

            if ($year_ < $cur_year && $year < 1) {
                $month_ = 12;
            }

            if (date('m') == 1 && date('m') == $month_) {
                $year_ = date('Y',strtotime("-1 YEAR"));
            }else{
                $end_beg_date = date('Y-m-t',strtotime($beg_start_period));
                // echo "<pre>";print_r($end_beg_date);exit;
                // $month_ = date('m',strtotime("-1 MONTH",strtotime($end_beg_date)));
                // $year_ = date('Y',strtotime("-1 MONTH",strtotime($end_beg_date)));
                $month_ = date('m',strtotime($end_beg_date));
                $year_ = date('Y',strtotime($end_beg_date));
            }

            // echo "<pre>";print_r($month_);exit;
            

            // $firstdate = $year . '-' . $first_month . '-01';
            // $lastdate = $year_ . '-' . $prev_month . '-31';
            $end_start_period = $year_ . '-' . $month_ . '-01';
            $end_end_period = $year_ . '-' . $month_ . '-31';
        }else{
            // $first_month = "01";
            // echo "<pre>";print_r($curr_month);exit;
            $year_ = (isset($year) && $year > 0)? $year:date('Y');
            // $year_ = date('Y',strtotime("-1 MONTH"));
            $prev_month = date('m',strtotime("-1 MONTH"));
            
            if ($curr_month == 1) {
                $year_ = date('Y',strtotime("-1 YEAR"));
            }
            
            if ($year_ < $cur_year) {
                $prev_month = 12;
            }
            // echo "<pre>prev_month";print_r($prev_month);exit;
            // $firstdate = $year . '-' . $first_month . '-01';
            // $lastdate = $year_ . '-' . $prev_month . '-31';
            $end_start_period = $year_ . '-' . $prev_month . '-01';
            $end_end_period = $year_ . '-' . $prev_month . '-31';
        }
        // echo $beg_start_period." ".$end_start_period;exit;

        // echo "<pre>";print_r($year);exit;
        $new_criteria = "";
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;

        if ($quarter > 0 && $month < 1) {
            $year_ = date('Y');
            switch ($quarter) {
                case 1:
                    // echo "Quarter: One";exit;
                $first_month = "01";
                $last_month = "03";
                $firstdate = $year_ . '-' . $first_month . '-01';
                $lastdate = $year_ . '-' . $last_month . '-31';

                $beg_start_period = $year_ . '-' . $first_month . '-01';
                $beg_end_period = $year_ . '-' . $first_month . '-31';
                $end_start_period = $year_ . '-' . $last_month . '-01';
                $end_end_period = $year_ . '-' . $last_month . '-31';
                break;
                case 2:
                    // echo "Quarter: Two";exit;
                $first_month = "04";
                $last_month = "06";
                $firstdate = $year_ . '-' . $first_month . '-01';
                $lastdate = $year_ . '-' . $last_month . '-31';
                $beg_start_period = $year_ . '-' . $first_month . '-01';
                $beg_end_period = $year_ . '-' . $first_month . '-31';
                $end_start_period = $year_ . '-' . $last_month . '-01';
                $end_end_period = $year_ . '-' . $last_month . '-31';
                break;
                case 3:
                    // echo "Quarter: Three";exit;
                $first_month = "07";
                $last_month = "09";
                $firstdate = $year_ . '-' . $first_month . '-01';
                $lastdate = $year_ . '-' . $last_month . '-31';
                $beg_start_period = $year_ . '-' . $first_month . '-01';
                $beg_end_period = $year_ . '-' . $first_month . '-31';
                $end_start_period = $year_ . '-' . $last_month . '-01';
                $end_end_period = $year_ . '-' . $last_month . '-31';
                break;
                case 4:
                    // echo "Quarter: Four";exit;
                $first_month = "10";
                $last_month = "12";
                $firstdate = $year_ . '-' . $first_month . '-01';
                $lastdate = $year_ . '-' . $last_month . '-31';
                $beg_start_period = $year_ . '-' . $first_month . '-01';
                $beg_end_period = $year_ . '-' . $first_month . '-31';
                $end_start_period = $year_ . '-' . $last_month . '-01';
                $end_end_period = $year_ . '-' . $last_month . '-31';
                break;
                
                default:
                    // echo "Quarter: Invalid";exit;
                break;
            }
            // $new_criteria .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
        }else{
            // $new_criteria .= ($year > 0)?" AND YEAR(l.created_at) = $year":NULL;
        }
            /*echo "<pre>";print_r($beg_start_period);
            echo "<pre>";print_r($beg_end_period);
            echo "<pre>";print_r($end_start_period);
            echo "<pre>";print_r($end_end_period);
            exit;*/

            $year_ = date('Y');

            $new_criteria .= ($district_id > 0)?" AND d.id = $district_id":NULL;
            $new_criteria .= ($county_id > 0)?" AND c.id = $county_id":NULL;
            $new_criteria .= ($facility_code > 0)?" AND f.facility_code = $facility_code":NULL;
        // $new_criteria .= ($year > 0)?"  AND YEAR(lco.order_date) = $year":NULL;


        // $new_criteria .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 

            $c_ids = (isset($commodity_id) && $commodity_id!='')? "'".$commodity_id."'" : "4,5";
        // echo "<pre>";print_r($new_criteria);exit;

            switch ($by) {
                case 'county':
                $group_by = "GROUP BY c.id,commodity_id ";
                break;
                case 'subcounty':
                $group_by = "GROUP BY d.id,commodity_id ";
                break;
                case 'facility':
                $group_by = "GROUP BY f.facility_code,commodity_id ";
                break;
                case 'facility_orders':
                $group_by = "GROUP BY f.facility_code,commodity_id,lco.id ";
                break;   
                default:
                $group_by = "GROUP BY commodity_id ";
                break;
            }

            $query = "";
            $query .="
                SELECT 
                c.id AS county_id,
                c.county,
                d.id AS district_id,
                d.district,
                f.facility_name,
                f.facility_code,
                l.district_id,
                l.commodity_id,
                lco.id AS order_id,
                lco.order_date,
                SUM(l.beginning_bal) AS beginning_bal,
                SUM(l.q_received) AS q_received,
                SUM(l.q_used) AS q_used,
                SUM(l.no_of_tests_done) AS no_of_tests_done,
                SUM(l.closing_stock) AS closing_stock,
                SUM(l.q_expiring) AS q_expiring,
                SUM(l.q_requested) AS q_requested,
                SUM(l.positive_adj) AS positive_adj,
                SUM(l.negative_adj) AS negative_adj,
                SUM(l.losses) AS losses,
                l.created_at
                FROM
                lab_commodity_details l USE INDEX (order_id,facility_code,district),
                counties c,
                districts d,
                facilities f USE INDEX (facility_code),
                lab_commodity_orders lco USE INDEX (facility_code)
                WHERE
                c.id = d.county
                AND l.order_id = lco.id
                AND l.facility_code > 0
                AND l.facility_code = f.facility_code
                AND lco.district_id = d.id
                AND l.commodity_id IN ($c_ids)
                $new_criteria";

            $data = $data_ = array();

            $beg_query= $query." AND lco.order_date BETWEEN '$beg_start_period' AND '$beg_end_period' $group_by";
            // echo "<pre>";print_r($beg_query);exit;
            $beg_data = $this->db->query($beg_query);
            // echo "<pre>";print_r($beg_data);exit;
            $end_query= $query." AND lco.order_date BETWEEN '$end_start_period' AND '$end_end_period' $group_by";
            // echo "<pre>";print_r($end_query);exit;
            $end_data = $this->db->query($end_query);
            // echo "<pre>";print_r($end_data);exit;
            switch ($by) {
                case 'county':
                    if ($beg_data) {
                        $beg_data = $beg_data->result_array();
                        // echo "<pre>";print_r($beg_data);exit;
                        foreach ($beg_data as $key => $value) {
                            $county_id = $value['county_id'];
                            $beg_bal = $value['beginning_bal'];
                            if ($beg_bal < 1) {
                                $beg_bal = 0;
                            }
                            $data[$county_id]['facility_code'] = $value['facility_code'];
                            $data[$county_id]['county'] = $value['county'];
                            $data[$county_id]['county_id'] = $value['county_id'];
                            $data[$county_id]['district'] = $value['district'];
                            $data[$county_id]['district_id'] = $value['district_id'];
                            $data[$county_id]['commodity_id'] = $value['commodity_id'];
                            // $data[$county_id]['beginning_order_date'] = $value['order_date'];
                            $data[$county_id]['beginning_order_date'] = (isset($value['order_date']) && $value['order_date'] !='')? $value['order_date']:$beg_start_period;
                            $data[$county_id]['beginning_balance'] = $beg_bal;
                            $data[$county_id]['created_at'] = $value['created_at'];
                            
                            // array_push($data_, $data);
                        }

                    }
                    
                    
                    if ($end_data) {
                        $end_data = $end_data->result_array();
                        // echo "<pre>";print_r($end_data);exit;
                        foreach ($end_data as $key => $value) {
                            $county_id = $value['county_id'];
                            $closing_bal = $value['closing_stock'];
                            
                            if ($closing_bal < 1) {
                                $closing_bal = 0;
                            }

                            $data[$county_id]['facility_code'] = $value['facility_code'];
                            $data[$county_id]['county'] = $value['county'];
                            $data[$county_id]['county_id'] = $value['county_id'];
                            $data[$county_id]['district'] = $value['district'];
                            $data[$county_id]['district_id'] = $value['district_id'];
                            $data[$county_id]['commodity_id'] = $value['commodity_id'];
                            // $data[$county_id]['closing_order_date'] = $value['order_date'];
                            $data[$county_id]['closing_order_date'] = (isset($value['order_date']) && $value['order_date'] !='')? $value['order_date']:$end_start_period;
                            $data[$county_id]['closing_balance'] = $closing_bal;
                            $data[$county_id]['created_at'] = $value['created_at'];
                            
                            // array_push($data_, $data);
                        }
                    }
                break;
                case 'subcounty':
                    if ($beg_data) {
                        $beg_data = $beg_data->result_array();
                        // echo "<pre>";print_r($beg_data);exit;
                        foreach ($beg_data as $key => $value) {
                            $district_id = $value['district_id'];
                            $beg_bal = $value['beginning_bal'];
                            if ($beg_bal < 1) {
                                $beg_bal = 0;
                            }
                            $data[$district_id]['facility_code'] = $value['facility_code'];
                            $data[$district_id]['county'] = $value['county'];
                            $data[$district_id]['county_id'] = $value['county_id'];
                            $data[$district_id]['district'] = $value['district'];
                            $data[$district_id]['district_id'] = $value['district_id'];
                            $data[$district_id]['commodity_id'] = $value['commodity_id'];
                            // $data[$district_id]['beginning_order_date'] = $value['order_date'];
                            $data[$district_id]['beginning_order_date'] = (isset($value['order_date']) && $value['order_date'] !='')? $value['order_date']:$beg_start_period;
                            $data[$district_id]['beginning_balance'] = $beg_bal;
                            $data[$district_id]['created_at'] = $value['created_at'];
                            
                            // array_push($data_, $data);
                        }

                    }
                    
                    if ($end_data) {
                        $end_data = $end_data->result_array();

                        foreach ($end_data as $key => $value) {
                            $district_id = $value['district_id'];
                            $closing_bal = $value['closing_stock'];
                            if ($closing_bal < 1) {
                                $closing_bal = 0;
                            }

                            $data[$district_id]['facility_code'] = $value['facility_code'];
                            $data[$district_id]['county'] = $value['county'];
                            $data[$district_id]['county_id'] = $value['county_id'];
                            $data[$district_id]['district'] = $value['district'];
                            $data[$district_id]['district_id'] = $value['district_id'];
                            $data[$district_id]['commodity_id'] = $value['commodity_id'];
                            // $data[$district_id]['closing_order_date'] = $value['order_date'];
                            $data[$district_id]['closing_order_date'] = (isset($value['order_date']) && $value['order_date'] !='')? $value['order_date']:$end_start_period;
                            $data[$district_id]['closing_balance'] = $closing_bal;
                            $data[$district_id]['created_at'] = $value['created_at'];
                            
                            // array_push($data_, $data);
                        }
                    }
                break;
                case 'facility':
                    if ($beg_data) {
                        $beg_data = $beg_data->result_array();
                        // echo "<pre>";print_r($beg_data);exit;
                        foreach ($beg_data as $key => $value) {
                            $mfl = $value['facility_code'];
                            $beg_bal = $value['beginning_bal'];
                            if ($beg_bal < 1) {
                                $beg_bal = 0;
                            }
                            $data[$mfl]['facility_code'] = $value['facility_code'];
                            $data[$mfl]['county'] = $value['county'];
                            $data[$mfl]['county_id'] = $value['county_id'];
                            $data[$mfl]['commodity_id'] = $value['commodity_id'];
                            // $data[$mfl]['beginning_order_date'] = $value['order_date'];
                            $data[$mfl]['beginning_order_date'] = (isset($value['order_date']) && $value['order_date'] !='')? $value['order_date']:$beg_start_period;
                            $data[$mfl]['beginning_balance'] = $beg_bal;
                            $data[$mfl]['created_at'] = $value['created_at'];
                            
                            // array_push($data_, $data);
                        }

                    }
                    
                    if ($end_data) {
                        $end_data = $end_data->result_array();

                        foreach ($end_data as $key => $value) {
                            $mfl = $value['facility_code'];
                            $closing_bal = $value['closing_stock'];
                            if ($closing_bal < 1) {
                                $closing_bal = 0;
                            }

                            $data[$mfl]['facility_code'] = $value['facility_code'];
                            $data[$mfl]['county'] = $value['county'];
                            $data[$mfl]['county_id'] = $value['county_id'];
                            $data[$mfl]['district_id'] = $value['district_id'];
                            $data[$mfl]['commodity_id'] = $value['commodity_id'];
                            // $data[$mfl]['closing_order_date'] = $value['order_date'];
                            $data[$mfl]['closing_order_date'] = (isset($value['order_date']) && $value['order_date'] !='')? $value['order_date']:$end_start_period;
                            $data[$mfl]['closing_balance'] = $closing_bal;
                            $data[$mfl]['created_at'] = $value['created_at'];
                            
                            // array_push($data_, $data);
                        }
                    }
                break;   
                default:
                // echo "<pre>THIS: ";print_r($end_data->result_array());exit;
                    if ($beg_data) {
                        $beg_data = $beg_data->result_array();
                        $data['beginning_balance']['county'] = $beg_data['0']['county'];
                        $data['beginning_balance']['commodity_id'] = $beg_data['0']['commodity_id'];
                        // $data['beginning_balance']['order_date'] = $beg_data['0']['order_date'];
                        $data['beginning_balance']['order_date'] = (isset($beg_data['0']['order_date']) && $beg_data['0']['order_date'] !='')? $beg_data['0']['order_date']:$beg_start_period;
                        $data['beginning_balance']['beginning_balance'] = $beg_data['0']['beginning_bal'];
                        // $data['beginning_balance']['created_at'] = $beg_data['0']['created_at'];
                        $data['beginning_balance']['created_at'] = (isset($beg_data['0']['created_at']) && $beg_data['0']['created_at'] !='')? $beg_data['0']['created_at']:$beg_start_period;
                    }
                    
                    if ($end_data) {
                        // echo "if";
                        $end_data = $end_data->result_array();
                        // echo "<pre>";print_r($end_data);exit;
                        // echo "<pre>";print_r($end_data['0']['created_at']);exit;
                        // echo $end_start_period.$end_end_period;exit;
                        $data['closing_balance']['county'] = $end_data['0']['county'];
                        $data['closing_balance']['commodity_id'] = $end_data['0']['commodity_id'];
                        // $data['closing_balance']['order_date'] = $end_data['0']['order_date'];
                        $data['closing_balance']['order_date'] = (isset($end_data['0']['order_date']) && $end_data['0']['order_date'] !='')? $end_data['0']['order_date']:$end_start_period;
                        $data['closing_balance']['closing_balance'] = $end_data['0']['closing_stock'];
                        // $data['closing_balance']['created_at'] = $end_data['0']['created_at'];
                        $data['closing_balance']['created_at'] = (isset($end_data['0']['created_at']) && $end_data['0']['created_at'] !='')? $end_data['0']['created_at']:$end_start_period;
                        // echo "<pre>";print_r($data);exit;

                    }else{
                        // echo "else";
                    }
                // exit;
                break;
            }

        // echo "<pre>";print_r($data);exit;
            return $data;
    }

    public function get_opening_and_closing_balances_dhis($county_id = NULL, $district_id = NULL, $facility_code = NULL, $commodity_id = NULL, $beg_date = NULL,$end_date = NULL,$quarter = NULL,$by = NULL,$month = NULL,$year = NULL){
        /*
            KARSAN
            IF MONTH = JANUARY, PICK LAST YEAR DATA AS DATA IS GOTTEN ON A -1 MONTH BASIS.
        */
        // echo "COUNTY: ".$county_id." DISTRICT_ID: ".$district_id." MFL: ".$facility_code." BY: ".$by;
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter." beg_date: ".$beg_date." end_date: ".$end_date;exit;
        $county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
        $district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
        $facility_code = (isset($facility_code) && $facility_code!='')? $facility_code:NULL;
        $by = (isset($by) && $by!='')? $by:NULL;
        // $year = (isset($year) && is_numeric($year) && $year>0)? $year:NULL;
        $year = (isset($year) && is_numeric($year) && $year>0)? $year:NULL;
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:NULL;
        $cur_year = date('Y');
        $curr_month = date('m');
        // $beg_date = (isset($beg_date) && $beg_date!='')? $beg_date:date('Y-m-d',strtotime(time));
        // echo "<pre>";print_r($quarter);exit;

        if (isset($month) && $month > 0) {
            $year_ = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');

            $lastdate = $end_date;
            // $year_ = date('Y',strtotime($beg_date));
            // $month_ = date('m',strtotime($beg_date));
            $beg_date = $year_."-".$month."-01";
            $end_date = $year_."-".$month."-31";
        }

        // echo $beg_date.' '.$end_date;exit;

        if (isset($beg_date) && $beg_date!='' && $beg_date!='NULL') {
            $lastdate = $end_date;
            $year_ = date('Y',strtotime($beg_date));
            $month_ = date('m',strtotime($beg_date));

            if (date('m') == 1 && date('m') == $month_) {
                $year_ = date('Y',strtotime("-1 YEAR"));
            }else{
                // $month_ = date('m',strtotime("-1 MONTH",strtotime($beg_date)));
                // $year_ = date('Y',strtotime("-1 MONTH",strtotime($beg_date)));

                $month_ = date('m',strtotime($beg_date));
                $year_ = date('Y',strtotime($beg_date));
            }

            // echo "<pre>";print_r($month_);
            // echo "<pre>";print_r($year_);exit;
            // $firstdate = $year . '-' . $first_month . '-01';
            // $lastdate = $year_ . '-' . $prev_month . '-31';
            $beg_start_period = $year_ . '-' . $month_ . '-01';
            $beg_end_period = $year_ . '-' . $month_ . '-31';
        }else{
            // $curr_month = date('m');
            $year_ = (isset($year) && $year > 0)? $year:date('Y');
            
            if ($curr_month == 1) {
                // echo "JANUARY";exit;
                $year_ = date('Y',strtotime("-1 YEAR"));
                // echo "<pre>";print_r($year_);exit;
            }

            $first_month = "01";
            $prev_month = date('m',strtotime("-1 MONTH"));

            if ($year_ < $cur_year) {
                $prev_month = 12;
            }
            

            // $firstdate = $year . '-' . $first_month . '-01';
            // $lastdate = $year . '-' . $prev_month . '-31';

            $beg_start_period = $year_ . '-' . $first_month . '-01';
            $beg_end_period = $year_ . '-' . $first_month . '-31';
        }

        if (isset($end_date) && $end_date!='' && $end_date!='NULL') {
            // echo "END DATE IF";exit;
            // echo "<pre>";print_r($end_date);exit;
            $lastdate = $end_date;
            // $lastday = date('t',strtotime($beg_date));
            // echo "<pre>";print_r($lastday);exit;
            $year_ = date('Y',strtotime($end_date));
            $month_ = date('m',strtotime($end_date));

            if ($year_ < $cur_year && $year < 1) {
                $month_ = 12;
            }

            if (date('m') == 1 && date('m') == $month_) {
                $year_ = date('Y',strtotime("-1 YEAR"));
            }else{
                $end_beg_date = date('Y-m-t',strtotime($beg_start_period));
                // echo "<pre>";print_r($end_beg_date);exit;
                // $month_ = date('m',strtotime("-1 MONTH",strtotime($end_beg_date)));
                // $year_ = date('Y',strtotime("-1 MONTH",strtotime($end_beg_date)));
                $month_ = date('m',strtotime($end_beg_date));
                $year_ = date('Y',strtotime($end_beg_date));
            }

            // echo "<pre>";print_r($month_);exit;
            

            // $firstdate = $year . '-' . $first_month . '-01';
            // $lastdate = $year_ . '-' . $prev_month . '-31';
            $end_start_period = $year_ . '-' . $month_ . '-01';
            $end_end_period = $year_ . '-' . $month_ . '-31';
        }else{
            // $first_month = "01";
            // echo "<pre>";print_r($curr_month);exit;
            $year_ = (isset($year) && $year > 0)? $year:date('Y');
            // $year_ = date('Y',strtotime("-1 MONTH"));
            $prev_month = date('m',strtotime("-1 MONTH"));
            
            if ($curr_month == 1) {
                $year_ = date('Y',strtotime("-1 YEAR"));
            }
            
            if ($year_ < $cur_year) {
                $prev_month = 12;
            }
            // echo "<pre>prev_month";print_r($prev_month);exit;
            // $firstdate = $year . '-' . $first_month . '-01';
            // $lastdate = $year_ . '-' . $prev_month . '-31';
            $end_start_period = $year_ . '-' . $prev_month . '-01';
            $end_end_period = $year_ . '-' . $prev_month . '-31';
        }
        // echo $beg_start_period." ".$end_start_period;exit;

        // echo "<pre>";print_r($year);exit;
        $new_criteria = "";
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;

        if ($quarter > 0 && $month < 1) {
            $year_ = date('Y');
            switch ($quarter) {
                case 1:
                    // echo "Quarter: One";exit;
                $first_month = "01";
                $last_month = "03";
                $firstdate = $year_ . '-' . $first_month . '-01';
                $lastdate = $year_ . '-' . $last_month . '-31';

                $beg_start_period = $year_ . '-' . $first_month . '-01';
                $beg_end_period = $year_ . '-' . $first_month . '-31';
                $end_start_period = $year_ . '-' . $last_month . '-01';
                $end_end_period = $year_ . '-' . $last_month . '-31';
                break;
                case 2:
                    // echo "Quarter: Two";exit;
                $first_month = "04";
                $last_month = "06";
                $firstdate = $year_ . '-' . $first_month . '-01';
                $lastdate = $year_ . '-' . $last_month . '-31';
                $beg_start_period = $year_ . '-' . $first_month . '-01';
                $beg_end_period = $year_ . '-' . $first_month . '-31';
                $end_start_period = $year_ . '-' . $last_month . '-01';
                $end_end_period = $year_ . '-' . $last_month . '-31';
                break;
                case 3:
                    // echo "Quarter: Three";exit;
                $first_month = "07";
                $last_month = "09";
                $firstdate = $year_ . '-' . $first_month . '-01';
                $lastdate = $year_ . '-' . $last_month . '-31';
                $beg_start_period = $year_ . '-' . $first_month . '-01';
                $beg_end_period = $year_ . '-' . $first_month . '-31';
                $end_start_period = $year_ . '-' . $last_month . '-01';
                $end_end_period = $year_ . '-' . $last_month . '-31';
                break;
                case 4:
                    // echo "Quarter: Four";exit;
                $first_month = "10";
                $last_month = "12";
                $firstdate = $year_ . '-' . $first_month . '-01';
                $lastdate = $year_ . '-' . $last_month . '-31';
                $beg_start_period = $year_ . '-' . $first_month . '-01';
                $beg_end_period = $year_ . '-' . $first_month . '-31';
                $end_start_period = $year_ . '-' . $last_month . '-01';
                $end_end_period = $year_ . '-' . $last_month . '-31';
                break;
                
                default:
                    // echo "Quarter: Invalid";exit;
                break;
            }
            // $new_criteria .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 
        }else{
            // $new_criteria .= ($year > 0)?" AND YEAR(l.created_at) = $year":NULL;
        }
            /*echo "<pre>";print_r($beg_start_period);
            echo "<pre>";print_r($beg_end_period);
            echo "<pre>";print_r($end_start_period);
            echo "<pre>";print_r($end_end_period);
            exit;*/

            $beg_month = date('m',strtotime($beg_start_period));
            $beg_year = date('Y',strtotime($beg_start_period));
            $beg_month_year = $beg_month.'_'.$beg_year;

            $end_month = date('m',strtotime($end_start_period));
            $end_year = date('Y',strtotime($end_start_period));
            $end_month_year = $end_month.'_'.$end_year;

            $year_ = date('Y');

            $new_criteria .= ($district_id > 0)?" AND d.id = $district_id":NULL;
            $new_criteria .= ($county_id > 0)?" AND c.id = $county_id":NULL;
            $new_criteria .= ($facility_code > 0)?" AND f.facility_code = $facility_code":NULL;
        // $new_criteria .= ($year > 0)?"  AND YEAR(lco.order_date) = $year":NULL;


        // $new_criteria .= " AND lco.order_date BETWEEN '$firstdate' AND '$lastdate'"; 

            $c_ids = (isset($commodity_id) && $commodity_id!='')? "'".$commodity_id."'" : "4,5";
        // echo "<pre>";print_r($new_criteria);exit;

            switch ($by) {
                case 'county':
                $group_by = "GROUP BY c.id,commodity_id ";
                break;
                case 'subcounty':
                $group_by = "GROUP BY d.id,commodity_id ";
                break;
                case 'facility':
                $group_by = "GROUP BY f.facility_code,commodity_id ";
                break;
                case 'facility_orders':
                $group_by = "GROUP BY f.facility_code,commodity_id,lco.id ";
                break;   
                default:
                $group_by = "GROUP BY commodity_id ";
                break;
            }

            $query = "";
           
            $query .="
                SELECT 
                    c.id AS county_id,
                    c.county, 
                    d.id AS district_id,
                    d.district, 
                    f.facility_code, 
                    f.facility_name, 
                    df.commodity_id,
                    SUM(df.beginning_bal) AS beginning_bal,
                    SUM(df.q_received) AS q_received,
                    SUM(df.q_used) AS q_used,
                    SUM(df.no_of_tests_done) AS no_of_tests_done,
                    SUM(df.closing_stock) AS closing_stock,
                    SUM(df.q_expiring) AS q_expiring,
                    SUM(df.q_requested) AS q_requested,
                    SUM(df.positive_adj) AS positive_adj,
                    SUM(df.negative_adj) AS negative_adj,
                    SUM(df.losses) AS losses,
                    df.month,df.year,df.month_year
                FROM
                    dhis_fcdrr_data df,
                    facilities f,
                    districts d,
                    counties c
                WHERE
                    df.facility_code = f.facility_code
                        AND f.district = d.id
                        AND d.county = c.id
                        $new_criteria
            ";



            $data = $data_ = array();

            $order_by = " ORDER BY month , year DESC ";

            $beg_query = $query." AND df.month_year = '$beg_month_year' $group_by";
            // echo "<pre>";print_r($beg_query);exit;
            $beg_data = $this->db->query($beg_query);
            // echo "<pre>";print_r($beg_data);exit;
            $end_query = $query." AND df.month_year = '$end_month_year' $group_by";
            // echo "<pre>";print_r($end_query);exit;
            $end_data = $this->db->query($end_query);
            // echo "<pre>";print_r($end_data);exit;
            switch ($by) {
                case 'county':
                    if ($beg_data) {
                        $beg_data = $beg_data->result_array();
                        // echo "<pre>";print_r($beg_data);exit;
                        foreach ($beg_data as $key => $value) {
                            $county_id = $value['county_id'];
                            $beg_bal = $value['beginning_bal'];
                            if ($beg_bal < 1) {
                                $beg_bal = 0;
                            }
                            $data[$county_id]['facility_code'] = $value['facility_code'];
                            $data[$county_id]['county'] = $value['county'];
                            $data[$county_id]['county_id'] = $value['county_id'];
                            $data[$county_id]['district'] = $value['district'];
                            $data[$county_id]['district_id'] = $value['district_id'];
                            $data[$county_id]['commodity_id'] = $value['commodity_id'];
                            // $data[$county_id]['beginning_order_date'] = $value['order_date'];
                            $data[$county_id]['beginning_order_date'] = (isset($value['order_date']) && $value['order_date'] !='')? $value['order_date']:$beg_start_period;
                            $data[$county_id]['beginning_balance'] = $beg_bal;
                            $data[$county_id]['created_at'] = $value['created_at'];
                            
                            // array_push($data_, $data);
                        }

                    }
                    
                    
                    if ($end_data) {
                        $end_data = $end_data->result_array();
                        // echo "<pre>";print_r($end_data);exit;
                        foreach ($end_data as $key => $value) {
                            $county_id = $value['county_id'];
                            $closing_bal = $value['closing_stock'];
                            
                            if ($closing_bal < 1) {
                                $closing_bal = 0;
                            }

                            $data[$county_id]['facility_code'] = $value['facility_code'];
                            $data[$county_id]['county'] = $value['county'];
                            $data[$county_id]['county_id'] = $value['county_id'];
                            $data[$county_id]['district'] = $value['district'];
                            $data[$county_id]['district_id'] = $value['district_id'];
                            $data[$county_id]['commodity_id'] = $value['commodity_id'];
                            // $data[$county_id]['closing_order_date'] = $value['order_date'];
                            $data[$county_id]['closing_order_date'] = (isset($value['order_date']) && $value['order_date'] !='')? $value['order_date']:$end_start_period;
                            $data[$county_id]['closing_balance'] = $closing_bal;
                            $data[$county_id]['created_at'] = $value['created_at'];
                            
                            // array_push($data_, $data);
                        }
                    }
                break;
                case 'subcounty':
                    if ($beg_data) {
                        $beg_data = $beg_data->result_array();
                        // echo "<pre>";print_r($beg_data);exit;
                        foreach ($beg_data as $key => $value) {
                            $district_id = $value['district_id'];
                            $beg_bal = $value['beginning_bal'];
                            if ($beg_bal < 1) {
                                $beg_bal = 0;
                            }
                            $data[$district_id]['facility_code'] = $value['facility_code'];
                            $data[$district_id]['county'] = $value['county'];
                            $data[$district_id]['county_id'] = $value['county_id'];
                            $data[$district_id]['district'] = $value['district'];
                            $data[$district_id]['district_id'] = $value['district_id'];
                            $data[$district_id]['commodity_id'] = $value['commodity_id'];
                            // $data[$district_id]['beginning_order_date'] = $value['order_date'];
                            $data[$district_id]['beginning_order_date'] = (isset($value['order_date']) && $value['order_date'] !='')? $value['order_date']:$beg_start_period;
                            $data[$district_id]['beginning_balance'] = $beg_bal;
                            $data[$district_id]['created_at'] = $value['created_at'];
                            
                            // array_push($data_, $data);
                        }

                    }
                    
                    if ($end_data) {
                        $end_data = $end_data->result_array();

                        foreach ($end_data as $key => $value) {
                            $district_id = $value['district_id'];
                            $closing_bal = $value['closing_stock'];
                            if ($closing_bal < 1) {
                                $closing_bal = 0;
                            }

                            $data[$district_id]['facility_code'] = $value['facility_code'];
                            $data[$district_id]['county'] = $value['county'];
                            $data[$district_id]['county_id'] = $value['county_id'];
                            $data[$district_id]['district'] = $value['district'];
                            $data[$district_id]['district_id'] = $value['district_id'];
                            $data[$district_id]['commodity_id'] = $value['commodity_id'];
                            // $data[$district_id]['closing_order_date'] = $value['order_date'];
                            $data[$district_id]['closing_order_date'] = (isset($value['order_date']) && $value['order_date'] !='')? $value['order_date']:$end_start_period;
                            $data[$district_id]['closing_balance'] = $closing_bal;
                            $data[$district_id]['created_at'] = $value['created_at'];
                            
                            // array_push($data_, $data);
                        }
                    }
                break;
                case 'facility':
                    if ($beg_data) {
                        $beg_data = $beg_data->result_array();
                        // echo "<pre>";print_r($beg_data);exit;
                        foreach ($beg_data as $key => $value) {
                            $mfl = $value['facility_code'];
                            $beg_bal = $value['beginning_bal'];
                            if ($beg_bal < 1) {
                                $beg_bal = 0;
                            }
                            $data[$mfl]['facility_code'] = $value['facility_code'];
                            $data[$mfl]['county'] = $value['county'];
                            $data[$mfl]['county_id'] = $value['county_id'];
                            $data[$mfl]['commodity_id'] = $value['commodity_id'];
                            // $data[$mfl]['beginning_order_date'] = $value['order_date'];
                            $data[$mfl]['beginning_order_date'] = (isset($value['order_date']) && $value['order_date'] !='')? $value['order_date']:$beg_start_period;
                            $data[$mfl]['beginning_balance'] = $beg_bal;
                            $data[$mfl]['created_at'] = $value['created_at'];
                            
                            // array_push($data_, $data);
                        }

                    }
                    
                    if ($end_data) {
                        $end_data = $end_data->result_array();

                        foreach ($end_data as $key => $value) {
                            $mfl = $value['facility_code'];
                            $closing_bal = $value['closing_stock'];
                            if ($closing_bal < 1) {
                                $closing_bal = 0;
                            }

                            $data[$mfl]['facility_code'] = $value['facility_code'];
                            $data[$mfl]['county'] = $value['county'];
                            $data[$mfl]['county_id'] = $value['county_id'];
                            $data[$mfl]['district_id'] = $value['district_id'];
                            $data[$mfl]['commodity_id'] = $value['commodity_id'];
                            // $data[$mfl]['closing_order_date'] = $value['order_date'];
                            $data[$mfl]['closing_order_date'] = (isset($value['order_date']) && $value['order_date'] !='')? $value['order_date']:$end_start_period;
                            $data[$mfl]['closing_balance'] = $closing_bal;
                            $data[$mfl]['created_at'] = $value['created_at'];
                            
                            // array_push($data_, $data);
                        }
                    }
                break;   
                default:
                // echo "<pre>THIS: ";print_r($end_data->result_array());exit;
                    if ($beg_data) {
                        $beg_data = $beg_data->result_array();
                        $data['beginning_balance']['county'] = $beg_data['0']['county'];
                        $data['beginning_balance']['commodity_id'] = $beg_data['0']['commodity_id'];
                        // $data['beginning_balance']['order_date'] = $beg_data['0']['order_date'];
                        $data['beginning_balance']['order_date'] = (isset($beg_data['0']['order_date']) && $beg_data['0']['order_date'] !='')? $beg_data['0']['order_date']:$beg_start_period;
                        $data['beginning_balance']['beginning_balance'] = $beg_data['0']['beginning_bal'];
                        // $data['beginning_balance']['created_at'] = $beg_data['0']['created_at'];
                        $data['beginning_balance']['created_at'] = (isset($beg_data['0']['created_at']) && $beg_data['0']['created_at'] !='')? $beg_data['0']['created_at']:$beg_start_period;
                    }
                    
                    if ($end_data) {
                        // echo "if";
                        $end_data = $end_data->result_array();
                        // echo "<pre>";print_r($end_data);exit;
                        // echo "<pre>";print_r($end_data['0']['created_at']);exit;
                        // echo $end_start_period.$end_end_period;exit;
                        $data['closing_balance']['county'] = $end_data['0']['county'];
                        $data['closing_balance']['commodity_id'] = $end_data['0']['commodity_id'];
                        // $data['closing_balance']['order_date'] = $end_data['0']['order_date'];
                        $data['closing_balance']['order_date'] = (isset($end_data['0']['order_date']) && $end_data['0']['order_date'] !='')? $end_data['0']['order_date']:$end_start_period;
                        $data['closing_balance']['closing_balance'] = $end_data['0']['closing_stock'];
                        // $data['closing_balance']['created_at'] = $end_data['0']['created_at'];
                        $data['closing_balance']['created_at'] = (isset($end_data['0']['created_at']) && $end_data['0']['created_at'] !='')? $end_data['0']['created_at']:$end_start_period;
                        // echo "<pre>";print_r($data);exit;

                    }else{
                        // echo "else";
                    }
                // exit;
                break;
            }

        // echo "<pre>";print_r($data);exit;
            return $data;
    }


    public function get_months_between_dates($start_date = NULL,$end_date = NULL)
    {
        //Uses global variable DATA_START_DATE defined in index.php currently as 2016
        $start_date = (isset($start_date) && $start_date !='')?$start_date:DATA_START_DATE;
        $end_date = (isset($end_date) && $end_date !='')?$end_date:date('Y-m-d',strtotime("-1 MONTH"));
        // echo "<pre>";print_r($end_date);
        // echo "<pre>";print_r($start_date);exit;
        $start    = (new DateTime($start_date))->modify('first day of this month');
        $end      = (new DateTime($end_date))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period   = new DatePeriod($start, $interval, $end);
        // echo "<pre>";print_r($period);exit;
        $months = $months_ = $months_year_name = array();
        foreach ($period as $dt) {
            // echo $dt->format("Y-m") . "<br>\n";
            $months_['month']=$dt->format('m');
            $months_['month_name']=$dt->format('F');
            $months_['year']=$dt->format('Y');
            $months_['month_year']=$dt->format('m_Y');
            $months_['monthyear']=$dt->format('mY');
            $months_['year_month']=$dt->format('Y_m');
            $months_['yearmonth']=$dt->format('Ym');
            $months_['month_year_full']=$dt->format('F Y');
            $months_['full_date']=$dt->format('Y-m-d');
            // $months_['last_month_year_full']=$dt->format('F Y');
            array_push($months, $months_);
        }

        $months = array_reverse($months);
        // echo "<pre>";print_r($months);exit;
        return $months;
    }

    public function add_cd4_sites($facility_code, $equipment)
    {
        
    }

    public function update_stocks_from_kemsa_api(){
        $counties = $this->db->query("SELECT * FROM counties")->result_array();
        $districts = $this->db->query("SELECT * FROM districts")->result_array();
        $date = (isset($date) && $date!='')? $date:date('Y-m-d');

        $c_ids = "4,5";
        $query="
            SELECT * FROM
            kemsa_stocks k
            WHERE k.commodity_id IN ($c_ids) AND k.stock_date = '$date';        
        ";
        // echo "<pre>";print_r($query);exit;

        $res = $this->db->query($query);

        if ($res) {
            $res = $res->result_array();
        }else{
            $res = array();
        }
        // echo "<pre>";print_r($res);exit;
        if (count($res) < 1) {
            // $commodity_id = (isset($commodity_id) && $commodity_id > 0)? $commodity_id :NULL;
            $kemsa_scr_id = 'NL05TES003';//Determine
            $kemsa_conf_id = 'NL05TES069';//First response

            // echo "<pre>";print_r($kemsa_current_id);exit;
            $kemsa_api_token = '$2a$06$BciA87SeFAPc4OT1HNLIYO5LxxWBq/2GlMVMT/K8AviAX1OUhrWsa';

            $final_issue_data = $final_issue_data_ = array();

            $api_url = 'https://api.kemsa.co.ke/vrtk_stocksummarys?';
            $url_data = http_build_query($data);
            $api_url = $api_url.$url_data;

            $curl = curl_init();
            $headr = array();
            $headr[] = 'apitoken: $2a$06$BciA87SeFAPc4OT1HNLIYO5LxxWBq/2GlMVMT/K8AviAX1OUhrWsa';

            // echo "<pre>";print_r($api_url);exit;

            curl_setopt_array($curl, array(
              CURLOPT_URL => $api_url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              // CURLOPT_HEADER => true,
              // CURLINFO_HEADER_OUT => true,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_HTTPHEADER => $headr,
              CURLOPT_TIMEOUT => 0
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            $information = curl_getinfo($curl);

            curl_close($curl);
            // echo "<pre>";print_r($information);exit;
            // echo "<pre>";print_r($response);exit;
            if ($err) {
              // echo "cURL Error #:" . $err;
                $echo = $this->echo_to_console($err);
            } else {
                // echo "<pre>";print_r($response);exit;

                $stock_data = json_decode($response, true);
                // echo "<pre>";print_r($issue_data);exit;
                foreach ($stock_data as $key => $value) {
                    $commodity_code = $value['value'];//no idea why
                    $commodity_name = $value['name'];
                    $mtd_receipts = $value['mtd_receipts'];
                    $ytd_receipts = $value['ytd_receipts'];
                    $mtd_issues = $value['mtd_issues'];
                    $ytd_issues = $value['ytd_issues'];
                    $at_hand = $value['at_hand'];
                    $stock_date = $value['date'];
                    $stock_date = date('Y-m-d',strtotime($stock_date));

                    // echo "<pre>";print_r($stock_date);exit;
                    
                    if ($commodity_code == "NL05TES003") {
                        $kemsa_current_id = $kemsa_scr_id;
                        $pack_size = 100;
                        $commodity_id = 4;
                        $valid = 1;
                    }elseif ($commodity_code == "NL05TES069") {
                        $kemsa_current_id = $kemsa_conf_id;
                        $pack_size = 30;
                        $commodity_id = 5;
                        $valid = 1;
                    }else{
                        $commodity_id = 0;
                        $kemsa_current_id = $kemsa_scr_id;
                        $pack_size = 0;
                        $valid = 0;
                    }

                    // echo "<pre>";print_r($valid);
                    if ($valid > 0) {

                        $mtd_receipts_ = $mtd_receipts * $pack_size;
                        $ytd_receipts_ = $ytd_receipts * $pack_size;

                        $mtd_issues_ = $mtd_issues * $pack_size;
                        $ytd_issues_ = $ytd_issues * $pack_size;
                        
                        $at_hand_ = $at_hand * $pack_size;

                        // echo "<pre>";print_r($pack_size);exit;

                        $kemsa_stocks_query = "SELECT * FROM kemsa_stocks WHERE commodity_code = '$commodity_code' AND stock_date = '$stock_date'";
                        // echo "<pre>";print_r($kemsa_stocks_query);exit;
                        $kemsa_stocks = $this->db->query($kemsa_stocks_query);
                        if ($kemsa_stocks) {
                            $kemsa_stocks = $kemsa_stocks->result_array();
                        }else{
                            $kemsa_stocks = array();
                        }

                        // echo "<pre>";print_r($kemsa_stocks);
                            // echo "<pre>";print_r($comment);exit;
                        $stock_data = array();
                        if (count($kemsa_stocks) < 1) {//insert or update
                            // echo "inserted";
                            $stock_data['commodity_id'] = $commodity_id;
                            $stock_data['commodity_code'] = $commodity_code;
                            $stock_data['mtd_receipts'] = $mtd_receipts_;
                            $stock_data['ytd_receipts'] = $ytd_receipts_;

                            $stock_data['mtd_issues'] = $mtd_issues_;
                            $stock_data['ytd_issues'] = $ytd_issues_;
                            $stock_data['at_hand'] = $at_hand_;
                            $stock_data['stock_date'] = $stock_date;

                            // echo "<pre>";print_r($stock_data);exit;
                            $result = $this->db->insert('kemsa_stocks',$stock_data);
                            
                            // echo "<pre>INSERTED: commodity_code: ".$commodity_code." stock_date: ".$stock_date ;
                        }else{
                            // echo "<pre>NOT INSERTED. EXISTS IN DB: commodity_code: ".$commodity_code." stock_date: ".$stock_date ;

                        }
                    }//end of valid if
                    
                }//stock data foreach   

            }//end of if curl success 
        
        }
    }

    public function get_kemsa_stock_data($date = NULL, $commodity_id = NULL,$quarter = NULL,$year = NULL,$month = NULL,$by = NULL){
        // echo "MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter;exit;
        // echo $county_id." ".$district_id." ".$facility_code." ".$commodity_id." ".$quarter." ".$year." ".$month;exit;
        $county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
        $date = (isset($date) && $date!='')? $date:date('Y-m-d');
        $district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
        $facility_code = (isset($facility_code) && $facility_code!='')? $facility_code:NULL;
        $year = (isset($year) && is_numeric($year) && $year>0)? $year:NULL;
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:NULL;
        $by = (isset($by) && $by!='')? $by:NULL;

        $update = $this->update_stocks_from_kemsa_api();
        // echo "<pre>";print_r($quarter);exit;
        $new_criteria = "";

        //DOC: Pre-dating day because KEMSA are setting their stock_date to a day before.
        $date = date("Y-m-d", strtotime("-1 day", strtotime($date)));

        // echo "MONTH: ".$month." YEAR: ".$year;exit;
        if ($quarter > 0 && $month < 1) {
            // $year = date('Y');
            // echo "<pre>";print_r($year);exit;
            $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');
            switch ($quarter) {
                case 1:
                    // echo "Quarter: One";exit;
                $first_month = "01";
                $last_month = "03";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND DATE(k.stock_date) BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                case 2:
                    // echo "Quarter: Two";exit;
                $first_month = "04";
                $last_month = "06";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND DATE(k.stock_date) BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                case 3:
                    // echo "Quarter: Three";exit;
                $first_month = "07";
                $last_month = "09";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND DATE(k.stock_date) BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                case 4:
                    // echo "Quarter: Four";exit;
                $first_month = "10";
                $last_month = "12";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND DATE(k.stock_date) BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                
                default:
                $new_criteria .= ($year > 0)?" AND YEAR(k.stock_date) = $year":NULL;
                    // echo "Quarter: Invalid";exit;
                break;
            }
            // $new_criteria .= " AND k.stock_date BETWEEN '$firstdate' AND '$lastdate'"; 

        }elseif (isset($month) && $month > 0) {
            $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');

            $firstdate = $year . '-' . $month . '-01';
            $lastdate = $year . '-' . $month . '-31';

            $new_criteria .= " AND DATE(k.stock_date) BETWEEN '$firstdate' AND '$lastdate'"; 

        }else{
            // echo "<pre>";print_r($year);exit;
            /*if ($year > 0) {
                $new_criteria .= (isset($date) && $date!='' )?" AND YEAR(k.stock_date) = '$year'":NULL;
            }else{
                $new_criteria .= (isset($date) && $date!='' )?" AND k.stock_date = '$date'":NULL;
            }*/
            $new_criteria .= (isset($date) && $date!='' )?" AND k.stock_date = '$date'":NULL;
            // $new_criteria .= ($year > 0)?" AND YEAR(k.stock_date) = $year":" AND YEAR(k.stock_date) = ".date('Y');
        }

        $c_ids = (isset($commodity_id) && $commodity_id!='')? "'".$commodity_id."'" : "4,5";


        // $order_by = " ORDER BY k.stock_date, k.commodity_id";
        // $order_by = " ORDER BY k.district_id";

        // echo "<pre>";print_r($new_criteria);exit;
        $query="
            SELECT * FROM
            kemsa_stocks k
            WHERE k.commodity_id IN ($c_ids)
            $new_criteria
            $group_by $order_by;        
        ";
        // echo "<pre>";print_r($query);exit;

        $res = $this->db->query($query);
        // echo "<pre>";print_r($res);exit;
        if ($res->num_rows() > 0) {
                $res = $res->result_array();
                // echo "<pre>";print_r($res);exit;
        }else{
            // echo "NO DATA";
            $query = "
                SELECT 
                    *
                FROM
                    rtk.kemsa_stocks
                GROUP BY commodity_id,stock_date
                ORDER BY created_at DESC
                LIMIT 2";
            $res = $this->db->query($query);
            $res = $res->result_array();
        }

        // echo "<pre>";print_r($res->result_array());exit;

        foreach ($res as $key => $value) {
            $commodity_id = $value['commodity_id'];
            $data[$commodity_id]['commodity_id'] = $value['commodity_id'];
            $data[$commodity_id]['commodity_code'] = $value['commodity_code'];
            $data[$commodity_id]['mtd_receipts'] = $value['mtd_receipts'];
            $data[$commodity_id]['ytd_receipts'] = $value['ytd_receipts'];
            $data[$commodity_id]['mtd_issues'] = $value['mtd_issues'];
            $data[$commodity_id]['ytd_issues'] = $value['ytd_issues'];
            $data[$commodity_id]['at_hand'] = $value['at_hand'];
            $data[$commodity_id]['stock_date'] = $value['stock_date'];
            $data[$commodity_id]['created_at'] = $value['created_at'];
                // array_push($data_, $data);
        }

        // echo "<pre>";print_r($data);exit;
        return $data;
    }

    public function get_years_to_date(){
        $cur_date = date('Y');
        $start_date = date('Y',strtotime(DATA_START_DATE));

        // echo $cur_date." ".$start_date;exit;

        $years = array();

        for ($yr = $start_date; $yr <= date('Y'); $yr++) {
                $year = $yr;
            array_push($years, $year);
        }

        $years = array_reverse($years);
        // echo "<pre>";print_r($years);exit;
        return $years;
    }

    public function get_allocation_data($county_id = NULL, $district_id = NULL, $facility_code = NULL, $commodity_id = NULL, $quarter = NULL,$year = NULL,$month = NULL,$by = NULL)
    {
        // echo "county_id: ".$county_id."district_id: ".$district_id."facility_code: ".$facility_code."MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter." BY: ".$by." commodity_id: ".$commodity_id;exit;
        // echo $county_id." ".$district_id." ".$facility_code." ".$commodity_id." ".$quarter." ".$year." ".$month;exit;
        $county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
        $district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
        $facility_code = (isset($facility_code) && $facility_code!='')? $facility_code:NULL;
        $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:NULL;
        $by = (isset($by) && $by!='')? $by:NULL;
        // echo "<pre>";print_r($quarter);exit;
        $new_criteria = "";

        // echo "MONTH: ".$month." YEAR: ".$year;exit;
        if ($quarter > 0 && $month < 1) {
            // $year = date('Y');
            // echo "<pre>";print_r($year);exit;
            switch ($quarter) {
                case 1:
                    // echo "Quarter: One";exit;
                $first_month = "01";
                $last_month = "03";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND DATE(month) BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                case 2:
                    // echo "Quarter: Two";exit;
                $first_month = "04";
                $last_month = "06";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND DATE(month) BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                case 3:
                    // echo "Quarter: Three";exit;
                $first_month = "07";
                $last_month = "09";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND DATE(month) BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                case 4:
                    // echo "Quarter: Four";exit;
                $first_month = "10";
                $last_month = "12";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND DATE(month) BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                
                default:
                $new_criteria .= ($year > 0)?" AND YEAR(month) = $year":NULL;
                    // echo "Quarter: Invalid";exit;
                break;
            }
            // $new_criteria .= " AND k.issue_date BETWEEN '$firstdate' AND '$lastdate'"; 

        }elseif (isset($month) && $month > 0) {
            $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');

            $firstdate = $year . '-' . $month . '-01';
            $lastdate = $year . '-' . $month . '-31';

            $new_criteria .= " AND DATE(month) BETWEEN '$firstdate' AND '$lastdate'"; 

        }else{
            // echo "<pre>";print_r($year);exit;
            $new_criteria .= ($month > 0)?" AND MONTH(month) = $month":NULL;
            $new_criteria .= ($year > 0)?" AND YEAR(month) = $year":" AND YEAR(month) = ".date('Y');
        }

        // $order_by = " ORDER BY k.district_id";

        switch ($by) {
            case 'county':
            $group_by = " GROUP BY c.id,k.commodity_id ";
            break;
            case 'subcounty':
            $group_by = " GROUP BY d.id,k.commodity_id ";
            break;
            case 'facility':
            $group_by = " GROUP BY f.facility_code,k.commodity_id ";
            break;
            case 'commodity':
            $group_by = " GROUP BY k.commodity_id ";
            // $order_by = " ORDER BY k.issue_date DESC";
            break;   
            default:
                // $group_by = "GROUP BY commodity_id ";
            // $group_by = " GROUP BY k.facility_code,k.commodity_id";
            break;
        }
        // echo $group_by;exit;
        // $year = date('Y');

        // echo $new_criteria;exit;
        $new_criteria .= ($district_id > 0)?" AND d.id = $district_id":NULL;
        $new_criteria .= ($county_id > 0)?" AND d.county = $county_id":NULL;
        $new_criteria .= ($facility_code > 0)?" AND a.facility_code = $facility_code":NULL;
        // echo "<pre>";print_r($new_criteria);exit;

        $sql = "
        SELECT 
            d.id as district_id,
            d.district as district_name,
            a.*
        FROM
            districts d,
            allocation_details a
        WHERE
            a.district_id = d.id $new_criteria";
        // echo $sql;exit;
        $result = $this->db->query($sql)->result_array();
        // echo "<pre>";print_r($result);exit;
        $fac_array = array();

        foreach ($result as $key => $value) {
            $f_code = $value['facility_code'];
            $allocate_s = $allocate_c = 0;

            $allocate_s = $value['allocate_s'];
            $allocate_c = $value['allocate_c'];

            $allocate_s = ($allocate_s > 0)? $allocate_s :0;
            $allocate_c = ($allocate_c > 0)? $allocate_c :0;

            if (empty($fac_array[$f_code])) {
                $fac_array[$f_code]['county_id'] = $value['county_id'];
                $fac_array[$f_code]['district_id'] = $value['district_id'];
                $fac_array[$f_code]['district_name'] = $value['district_name'];
                $fac_array[$f_code]['facility_code'] = $value['facility_code'];
                
                $fac_array[$f_code]['allocate_s'] = $allocate_s;
                $fac_array[$f_code]['allocate_s_packs'] = round($allocate_s/100);
                $fac_array[$f_code]['ending_bal_s'] = $value['ending_bal_s'];

                $fac_array[$f_code]['allocate_c'] = $allocate_c;
                $fac_array[$f_code]['allocate_c_packs'] = round($allocate_c/30);
                $fac_array[$f_code]['ending_bal_c'] = $value['ending_bal_c'];

                $fac_array[$f_code]['month'] = $value['month'];
            }else{
                $fac_array[$f_code]['repeat_allocation']['county_id'] = $value['county_id'];
                $fac_array[$f_code]['repeat_allocation']['district_id'] = $value['district_id'];
                $fac_array[$f_code]['repeat_allocation']['district_name'] = $value['district_name'];
                $fac_array[$f_code]['repeat_allocation']['facility_code'] = $value['facility_code'];
                
                $fac_array[$f_code]['repeat_allocation']['allocate_s'] = $allocate_s;
                $fac_array[$f_code]['repeat_allocation']['allocate_s_packs'] = round($allocate_s/100);
                $fac_array[$f_code]['repeat_allocation']['ending_bal_s'] = $value['ending_bal_s'];

                $fac_array[$f_code]['repeat_allocation']['allocate_c'] = $allocate_c;
                $fac_array[$f_code]['repeat_allocation']['allocate_c_packs'] = round($allocate_c/30);
                $fac_array[$f_code]['repeat_allocation']['ending_bal_c'] = $value['ending_bal_c'];

                $fac_array[$f_code]['month'] = $value['month'];
            }
        }

        // echo "<pre>";print_r($fac_array);exit;

        return $fac_array;
        // echo "<pre>";print_r($result);exit;
    }

    public function get_facility_orders($county_id = NULL, $district_id = NULL, $facility_code = NULL, $quarter = NULL,$year = NULL,$month = NULL,$by = NULL)
    {
        $county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
        $district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
        $facility_code = (isset($facility_code) && $facility_code!='')? $facility_code:NULL;
        $year_filtered = (isset($year) && is_numeric($year) && $year>0)? 1:0;
        $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:NULL;
        $by = (isset($by) && $by!='')? $by:NULL;
        // echo "<pre>";print_r($quarter);exit;
        $new_criteria = "";

        // echo "MONTH: ".$month." YEAR: ".$year;exit;
        if ($quarter > 0 && $month < 1) {
            // $year = date('Y');
            // echo "<pre>";print_r($year);exit;
            switch ($quarter) {
                case 1:
                    // echo "Quarter: One";exit;
                $first_month = "01";
                $last_month = "03";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND DATE(order_date) BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                case 2:
                    // echo "Quarter: Two";exit;
                $first_month = "04";
                $last_month = "06";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND DATE(order_date) BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                case 3:
                    // echo "Quarter: Three";exit;
                $first_month = "07";
                $last_month = "09";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND DATE(order_date) BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                case 4:
                    // echo "Quarter: Four";exit;
                $first_month = "10";
                $last_month = "12";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND DATE(order_date) BETWEEN '$firstdate' AND '$lastdate'"; 
                break;
                
                default:
                $new_criteria .= ($year > 0)?" AND YEAR(order_date) = $year":NULL;
                    // echo "Quarter: Invalid";exit;
                break;
            }
            // $new_criteria .= " AND k.issue_date BETWEEN '$firstdate' AND '$lastdate'"; 

        }elseif (isset($month) && $month > 0) {
            $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');

            $firstdate = $year . '-' . $month . '-01';
            $lastdate = $year . '-' . $month . '-31';

            $new_criteria .= " AND DATE(order_date) BETWEEN '$firstdate' AND '$lastdate'"; 

        }else{
            // echo "<pre>";print_r($year);exit;
            $new_criteria .= ($month > 0)?" AND MONTH(order_date) = $month":NULL;
            // $new_criteria .= ($year > 0)?" AND YEAR(order_date) = $year":" AND YEAR(order_date) = ".date('Y');
            $new_criteria .= ($year_filtered > 0)?" AND YEAR(order_date) = $year":NULL;
        }

        // $order_by = " ORDER BY k.district_id";

        switch ($by) {
            case 'county':
            $group_by = " GROUP BY c.id,k.commodity_id ";
            break;
            case 'subcounty':
            $group_by = " GROUP BY d.id,k.commodity_id ";
            break;
            case 'facility':
            $group_by = " GROUP BY f.facility_code,k.commodity_id ";
            break;
            case 'commodity':
            $group_by = " GROUP BY k.commodity_id ";
            // $order_by = " ORDER BY k.issue_date DESC";
            break;   
            default:
                // $group_by = "GROUP BY commodity_id ";
            // $group_by = " GROUP BY k.facility_code,k.commodity_id";
            break;
        }
        // echo $group_by;exit;
        // $year = date('Y');

        // echo $new_criteria;exit;
        $new_criteria .= ($district_id > 0)?" AND f.district = $district_id":NULL;
        $new_criteria .= ($county_id > 0)?" AND c.id = $county_id":NULL;
        $new_criteria .= ($facility_code > 0)?" AND l.facility_code = $facility_code":NULL;
        // echo "<pre>";print_r($new_criteria);exit;

        $sql = "
        SELECT 
            c.id AS county_id,
            c.county,
            d.id AS district_id,
            d.district,
            f.facility_code,
            f.facility_name,
            l.id AS order_id,
            l.order_date,
            l.district_id,
            l.compiled_by,
            l.facility_code
        FROM
            lab_commodity_orders l,
            facilities f,
            districts d,
            counties c
        WHERE
            l.facility_code = f.facility_code
            AND f.district = d.id
            AND d.county = c.id
            $new_criteria $group_by
            ORDER BY l.order_date DESC";
        // echo $sql;exit;
        $result = $this->db->query($sql)->result_array();
        // echo "<pre>";print_r($result);exit;
        $fac_array = array();

        /*
        //WILL ONLY WORK FOR GROUP BY DISTRICT OR COUNTY
        foreach ($result as $key => $value) {
            $f_code = $value['facility_code'];

            $order_id = $value['order_id'];
            $order_date = $value['order_date'];
            $fac_array[$f_code]['county_id'] = $value['county_id'];
            $fac_array[$f_code]['district_id'] = $value['district_id'];
            $fac_array[$f_code]['district_name'] = $value['district_name'];
            $fac_array[$f_code]['facility_code'] = $value['facility_code'];
            
            $fac_array[$f_code]['order_id'] = $order_id;
            $fac_array[$f_code]['order_date'] = $order_date;
        }
        */
        // echo "<pre>";print_r($fac_array);exit;

        // return $fac_array;
        return $result;
    }

    public function get_dhis_data($county_id = NULL, $district_id = NULL, $facility_code = NULL, $commodity_id = NULL, $quarter = NULL,$year = NULL,$month = NULL,$by = NULL)
    {
        // echo "county_id: ".$county_id."district_id: ".$district_id."facility_code: ".$facility_code."MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter." BY: ".$by." commodity_id: ".$commodity_id;exit;
        // echo $county_id." ".$district_id." ".$facility_code." ".$commodity_id." ".$quarter." ".$year." ".$month;exit;
        $county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
        $district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
        $facility_code = (isset($facility_code) && $facility_code!='')? $facility_code:NULL;
        $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:NULL;
        $by = (isset($by) && $by!='')? $by:NULL;
        // echo "<pre>";print_r($quarter);exit;
        $new_criteria = "";

        // echo "MONTH: ".$month." YEAR: ".$year;exit;
        if ($quarter > 0 && $month < 1) {
            // $year = date('Y');
            // echo "<pre>";print_r($year);exit;
            switch ($quarter) {
                case 1:
                    // echo "Quarter: One";exit;
                $first_month = "01";
                $last_month = "03";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND d.month BETWEEN '$first_month' AND '$last_month'"; 
                $new_criteria .= " AND d.year = '$year'"; 
                break;
                case 2:
                    // echo "Quarter: Two";exit;
                $first_month = "04";
                $last_month = "06";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND d.month BETWEEN '$first_month' AND '$last_month'"; 
                $new_criteria .= " AND d.year = '$year'"; 
                break;
                case 3:
                    // echo "Quarter: Three";exit;
                $first_month = "07";
                $last_month = "09";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND d.month BETWEEN '$first_month' AND '$last_month'"; 
                $new_criteria .= " AND d.year = '$year'"; 
                break;
                case 4:
                    // echo "Quarter: Four";exit;
                $first_month = "10";
                $last_month = "12";
                $firstdate = $year . '-' . $first_month . '-01';
                $lastdate = $year . '-' . $last_month . '-31';
                $new_criteria .= " AND d.month BETWEEN '$first_month' AND '$last_month'"; 
                $new_criteria .= " AND d.year = '$year'"; 
                break;
                
                default:
                $new_criteria .= ($year > 0)?" AND d.year = $year":NULL;
                    // echo "Quarter: Invalid";exit;
                break;
            }
            // $new_criteria .= " AND k.issue_date BETWEEN '$firstdate' AND '$lastdate'"; 

        }elseif (isset($month) && $month > 0) {
            $year = (isset($year) && is_numeric($year) && $year>0)? $year:date('Y');

            $firstdate = $year . '-' . $month . '-01';
            $lastdate = $year . '-' . $month . '-31';

            $new_criteria .= " AND d.month BETWEEN '$first_month' AND '$last_month'"; 
            $new_criteria .= " AND d.year = '$year'"; 
        }else{
            // echo "<pre>";print_r($year);exit;
            $new_criteria .= ($month > 0)?" AND d.month = $month":NULL;
            $new_criteria .= ($year > 0)?" AND d.year = $year":" AND year = ".date('Y');
        }

        // $order_by = " ORDER BY k.district_id";

        switch ($by) {
            case 'county':
            $group_by = " GROUP BY c.id,d.commodity_id ";
            break;
            case 'subcounty':
            $group_by = " GROUP BY d.id,d.commodity_id ";
            break;
            case 'facility':
            $group_by = " GROUP BY f.facility_code,d.commodity_id ";
            break;
            case 'commodity':
            $group_by = " GROUP BY d.commodity_id ";
            // $order_by = " ORDER BY k.issue_date DESC";
            break;   
            default:
                $group_by = "GROUP BY d.commodity_id ";
            // $group_by = " GROUP BY k.facility_code,k.commodity_id";
            break;
        }
        // echo $group_by;exit;
        // $year = date('Y');

        // echo $new_criteria;exit;
        $new_criteria .= ($district_id > 0)?" AND di.id = $district_id":NULL;
        $new_criteria .= ($county_id > 0)?" AND di.county = $county_id":NULL;
        $new_criteria .= ($facility_code > 0)?" AND d.facility_code = $facility_code":NULL;
        // echo "<pre>";print_r($new_criteria);exit;

        $sql = "
        SELECT 
            c.id AS county_id,
            c.county,
            di.id AS district_id,
            di.district,
            df.facility_code,
            df.dhis_facility_code,
            d.facility_code as dhis_data_facility_code,
            d.commodity_id,
            SUM(d.tests_done) as tests_done,
            d.month,
            d.year,
            d.month_year,
            d.dimension,
            d.created_at,
            d.updated_at
        FROM
            dhis_data d,
            dhis_facilities df,
            counties c,
            districts di,
            facilities f
        WHERE
            d.dhis_facility_code = df.dhis_facility_code
                AND d.facility_code = f.facility_code
                AND f.district = di.id
                AND di.county = c.id $new_criteria $group_by";

        // echo $sql;exit;
        $result = $this->db->query($sql)->result_array();
        // echo "<pre>";print_r($result);exit;
        $final_array = array();

        foreach ($result as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $f_code = $value['facility_code'];
            $comm_id = $value['commodity_id'];
            $pack_divisor = (isset($comm_id) && $comm_id == 4)? 100:30;
            $tests_done = $value['tests_done'];

            $final_array[$comm_id]['county_id'] = $value['county_id'];
            $final_array[$comm_id]['district_id'] = $value['district_id'];
            $final_array[$comm_id]['district_name'] = $value['district'];
            $final_array[$comm_id]['facility_code'] = $value['facility_code'];
            
            $final_array[$comm_id]['commodity_id'] = $comm_id;
            $final_array[$comm_id]['tests_done'] += $tests_done;
            $final_array[$comm_id]['tests_done_packs'] += round($tests_done/$pack_divisor);

            $final_array[$comm_id]['month'] = $value['month'];
            $final_array[$comm_id]['year'] = $value['year'];
            $final_array[$comm_id]['month_year'] = $value['month_year'];
        }

        // echo "<pre>";print_r($final_array);exit;

        return $final_array;
        // echo "<pre>";print_r($result);exit;
    }

    public function get_fcdrr_update_logs($order_id)
    {   
        $query = "SELECT f.*,u.fname,u.lname,u.email FROM fcdrr_edit_logs f,user u WHERE f.user_id = u.id AND f.order_id = $order_id";
        $result = $this->db->query($query);

        $html ='';
        if ($result->num_rows() > 0) {
            $result = $result->result_array();
            // echo "<pre>";print_r($result);exit;

            $table = '';
            $table .= '<table class="table table-bordered compact table-sm">';
            $table .= '<thead>';
            $table .= '
                    <th>Name</th>
                    <th>Updated On</th>';
            $table .= '</thead>';
            $table .= '<tbody>';

            foreach ($result as $detail) {
                // echo "<pre>";print_r($detail);exit;
                $losses = $detail['losses'];
                $neg_adj = $detail['negative_adj'];
                $clos_bal = $detail['closing_stock'];

                $table .= '
                        <tr>
                            <td>'. $detail['fname']." ".$detail['lname'].'</td>
                            <td>'.date('jS F Y',strtotime($detail['created_at'])).'</td>
                        </tr>';
            }

            $table .= '</tbody>';

            $table .= '</table>';

            $html .= $table;
            echo $html;
        }else{
            $html="There are no update logs for this order.";
            echo $html;
        }
    }//END OF get_fcdrr_update_logs function

    public function send_balance_discrepancies_email($county_id = NULL, $district_id = NULL, $facility_code = NULL, $month = NULL,$year = NULL,$quarter = NULL)
    {
        $current_month = date('m');
        $current_year = date('Y');
        $year = (isset($year) && is_numeric($year) && $year>0)? $year:$current_year;
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:$current_month;
        $monthyear = $year . '-' . $month . '-01';
        $englishdate = date('F Y', strtotime($monthyear));
        $county_id = (isset($county_id) && $county_id!='')? $county_id:NULL;
        $district_id = (isset($district_id) && $district_id!='')? $district_id:NULL;
        $facility_code = (isset($facility_code) && $facility_code!='')? $facility_code:NULL;
        $by = (isset($by) && $by!='')? $by:NULL;

        $c_where .= (isset($county_id) && $county_id > 0) ? " WHERE id = $county_id" : NULL;
        $c_query = "SELECT * FROM counties $c_where";
        $counties = $this->db->query($c_query)->result_array();
        $output = array();
        // echo "<pre>";print_r($monthyear);exit;
        foreach ($counties as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $c_id = $value['id'];
            $c_name = $value['county'];

            $send_mail = $this->send_balance_discrepancies_email_($c_id, $district_id, $facility_code, $month,$year,$quarter);
            $output[]="EMAIL SENT TO: ".$c_id." COUNTY: ".$c_name;
        }

        echo "<pre>";print_r($output);exit;
    }

    public function send_balance_discrepancies_email_($county_id = NULL, $district_id = NULL, $facility_code = NULL, $month = NULL,$year = NULL,$quarter = NULL){
        $current_month = date('m');
        $current_year = date('Y');
        $year = (isset($year) && is_numeric($year) && $year>0)? $year:$current_year;
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:$current_month;
        $monthyear = $year . '-' . $month . '-01';
        $englishdate = date('F Y', strtotime($monthyear));
        $prev_englishdate = date('F Y', strtotime("-1 MONTH",strtotime($monthyear)));
        $prev_month = date('m', strtotime("-1 MONTH",strtotime($monthyear)));

        $reportname = '';
        $c_where .= (isset($county_id) && $county_id > 0) ? " WHERE id = $county_id" : NULL;
        $c_query = "SELECT * FROM counties $c_where";
        $counties = $this->db->query($c_query)->result_array();

        if (isset($county_id) && $county_id > 0) {
            $reportname .= $counties[0]['county'].' County ';     
            $team_name = $counties[0]['county'].' County ';     
        }
        // echo "<pre>";print_r($reportname);exit;
        
        $reportname .= 'Balance Discrepancies as at ' . $englishdate;

        $html_title = "<div ALIGN=CENTER><img src='" . base_url() . "assets/img/coat_of_arms.png' height='70' width='70'style='vertical-align: top;' > </img></div>
            <div style='text-align:center; font-family: arial,helvetica,clean,sans-serif;display: block; font-weight: bold; font-size: 14px;'>     Ministry of Health</div>
            <div style='text-align:center; font-family: arial,helvetica,clean,sans-serif;display: block; font-weight: bold;display: block; font-size: 13px;'>Health Commodities Management Platform</div>
            <div style='text-align:center; font-size: 14px;display: block;font-weight: bold;'>Rapid Test Kits (RTK) System</div>
            <div style='text-align:center; font-size: 14px;display: block;font-weight: bold;'>National Aggregate Stock Card  for $englishdate</div><hr />

            <style>table.data-table {border: 1px solid #DDD;font-size: 13px;border-spacing: 0px;}
            table.data-table th {border: none;color: #036;text-align: center;background-color: #F5F5F5;border: 1px solid #DDD;border-top: none;max-width: 450px;}
            table.data-table td, table th {padding: 4px;}
            table.data-table td {border: none;border-right: 1px solid #DDD;height: 30px;margin: 0px;border-bottom: 1px solid #DDD;}
            .col5{background:#D8D8D8;}</style>";
        $table_head_1 = '
            <br>
            <div class=""  style="text-align:center; font-family: arial,helvetica,clean,sans-serif;display: block; font-size: 12px;">Expected Closing Balance = <b>(Beginning Balance + Quantity Received + Positive Adjustments) - (Quantity Used + Losses + Negative Adjustments) </b></div>
            <table border="0" class="data-table" style="width: 100%; margin: 10px auto;">
            <thead border="0" style="margin: 10px auto;font-weight:900">
                <tr>
                    <th colspan="7">County Aggregates</th>
                </tr>
                <tr>
                    <th rowspan="2">County</th>
                    <th colspan="3">Screening</th>
                    <th colspan="3">Confirmatory</th>
                </tr>
                <tr>
                    <th>Expected Balance</th>
                    <th>Reported</th>
                    <th>Difference</th>

                    <th>Expected Balance</th>
                    <th>Reported</th>
                    <th>Difference</th>
                </tr>
            </thead>
            <tbody>';
        $table_body_1 = '';

        $district_drawing_data_s = $district_drawing_data_c = $district_drawing_data_ss = $district_drawing_data_cc = array();

        $summary_data_s = $this->get_order_data($county_id, NULL, NULL, 4 , $quarter_,"facility");
        // echo "<pre>";print_r($summary_data_s);exit;
        $summary_data_c = $this->get_order_data($county_id, NULL, NULL, 5 , $quarter_,"facility");
        // echo "<pre>";print_r($summary_data_c);exit;  

        $county_arr_s = $county_arr_s_ = $dist_arr_s = $dist_arr_s_ = $fac_arr_s = $fac_arr_s_ =  array();
        $county_arr_c = $county_arr_c_ = $dist_arr_c = $dist_arr_c_ = $fac_arr_c = $fac_arr_c_ =  array();
        $fac_by_month_s = $fac_by_month_s_ = array();
        $fac_by_month_c = $fac_by_month_c_ = array();

        // echo "<pre>";print_r($counties);exit;
        foreach ($summary_data_s as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $c_id = $value['county_id'];
            $c_name = $value['county'];
            $d_id = $value['district_id'];
            $d_name = $value['district'];
            $f_code = $value['facility_code'];
            $f_name = $value['facility_name'];
            $o_date = $value['order_date'];
            $m_y = date('mY',strtotime($o_date));

            if (count($county_arr_s[$c_id]) > 0) {
                $county_arr_s[$c_id]['beginning_bal'] += $value['beginning_bal'];
                $county_arr_s[$c_id]['q_received'] += $value['q_received'];
                $county_arr_s[$c_id]['q_used'] += $value['q_used'];
                $county_arr_s[$c_id]['no_of_tests_done'] += $value['no_of_tests_done'];
                $county_arr_s[$c_id]['closing_stock'] += $value['closing_stock'];
                $county_arr_s[$c_id]['positive_adj'] += $value['positive_adj'];
                $county_arr_s[$c_id]['negative_adj'] += $value['negative_adj'];
                $county_arr_s[$c_id]['losses'] += $value['losses'];

            }else{
                $county_arr_s[$c_id]['county_id'] = $value['county_id'];
                $county_arr_s[$c_id]['county_name'] = $value['county'];
                $county_arr_s[$c_id]['district_id'] = $value['district_id'];
                $county_arr_s[$c_id]['district_name'] = $value['district'];
                $county_arr_s[$c_id]['facility_code'] = $value['facility_code'];
                $county_arr_s[$c_id]['facility_name'] = $value['facility_name'];
                $county_arr_s[$c_id]['order_date'] = $value['order_date'];
                $county_arr_s[$c_id]['beginning_bal'] = $value['beginning_bal'];
                $county_arr_s[$c_id]['q_received'] = $value['q_received'];
                $county_arr_s[$c_id]['q_used'] = $value['q_used'];
                $county_arr_s[$c_id]['no_of_tests_done'] = $value['no_of_tests_done'];
                $county_arr_s[$c_id]['closing_stock'] = $value['closing_stock'];
                $county_arr_s[$c_id]['positive_adj'] = $value['positive_adj'];
                $county_arr_s[$c_id]['negative_adj'] = $value['negative_adj'];
                $county_arr_s[$c_id]['losses'] = $value['losses'];

                $county_arr_s[$c_id]['level'] = "county";
                $county_arr_s[$c_id]['level_id'] = $value['county_id'];
                $county_arr_s[$c_id]['commodity_id'] = 4;

            }

            if (count($dist_arr_s[$d_id]) > 0) {
                $dist_arr_s[$d_id]['beginning_bal'] += $value['beginning_bal'];
                $dist_arr_s[$d_id]['q_received'] += $value['q_received'];
                $dist_arr_s[$d_id]['q_used'] += $value['q_used'];
                $dist_arr_s[$d_id]['no_of_tests_done'] += $value['no_of_tests_done'];
                $dist_arr_s[$d_id]['closing_stock'] += $value['closing_stock'];
                $dist_arr_s[$d_id]['positive_adj'] += $value['positive_adj'];
                $dist_arr_s[$d_id]['negative_adj'] += $value['negative_adj'];
                $dist_arr_s[$d_id]['losses'] += $value['losses'];
            }else{
                $dist_arr_s[$d_id]['county_id'] = $value['county_id'];
                $dist_arr_s[$d_id]['county_name'] = $value['county'];
                $dist_arr_s[$d_id]['district_id'] = $value['district_id'];
                $dist_arr_s[$d_id]['district_name'] = $value['district'];
                $dist_arr_s[$d_id]['facility_code'] = $value['facility_code'];
                $dist_arr_s[$d_id]['facility_name'] = $value['facility_name'];
                $dist_arr_s[$d_id]['order_date'] = $value['order_date'];
                $dist_arr_s[$d_id]['beginning_bal'] = $value['beginning_bal'];
                $dist_arr_s[$d_id]['q_received'] = $value['q_received'];
                $dist_arr_s[$d_id]['q_used'] = $value['q_used'];
                $dist_arr_s[$d_id]['no_of_tests_done'] = $value['no_of_tests_done'];
                $dist_arr_s[$d_id]['closing_stock'] = $value['closing_stock'];
                $dist_arr_s[$d_id]['positive_adj'] = $value['positive_adj'];
                $dist_arr_s[$d_id]['negative_adj'] = $value['negative_adj'];
                $dist_arr_s[$d_id]['losses'] = $value['losses'];

                $dist_arr_s[$d_id]['level'] = "subcounty";
                $dist_arr_s[$d_id]['level_id'] = $value['district_id'];
                $dist_arr_s[$d_id]['commodity_id'] = 4;
            }

            if (count($fac_arr_s[$f_code]) > 0) {
                $fac_arr_s[$f_code]['beginning_bal'] += $value['beginning_bal'];
                $fac_arr_s[$f_code]['q_received'] += $value['q_received'];
                $fac_arr_s[$f_code]['q_used'] += $value['q_used'];
                $fac_arr_s[$f_code]['no_of_tests_done'] += $value['no_of_tests_done'];
                $fac_arr_s[$f_code]['closing_stock'] += $value['closing_stock'];
                $fac_arr_s[$f_code]['positive_adj'] += $value['positive_adj'];
                $fac_arr_s[$f_code]['negative_adj'] += $value['negative_adj'];
                $fac_arr_s[$f_code]['losses'] += $value['losses'];
            }else{
                $fac_arr_s[$f_code]['county_id'] = $value['county_id'];
                $fac_arr_s[$f_code]['county_name'] = $value['county'];
                $fac_arr_s[$f_code]['district_id'] = $value['district_id'];
                $fac_arr_s[$f_code]['district_name'] = $value['district'];
                $fac_arr_s[$f_code]['facility_code'] = $value['facility_code'];
                $fac_arr_s[$f_code]['facility_name'] = $value['facility_name'];
                $fac_arr_s[$f_code]['order_date'] = $value['order_date'];
                $fac_arr_s[$f_code]['beginning_bal'] = $value['beginning_bal'];
                $fac_arr_s[$f_code]['q_received'] = $value['q_received'];
                $fac_arr_s[$f_code]['q_used'] = $value['q_used'];
                $fac_arr_s[$f_code]['no_of_tests_done'] = $value['no_of_tests_done'];
                $fac_arr_s[$f_code]['closing_stock'] = $value['closing_stock'];
                $fac_arr_s[$f_code]['positive_adj'] = $value['positive_adj'];
                $fac_arr_s[$f_code]['negative_adj'] = $value['negative_adj'];
                $fac_arr_s[$f_code]['losses'] = $value['losses'];

                $fac_arr_s[$f_code]['level'] = "facility";
                $fac_arr_s[$f_code]['level_id'] = $value['facility_code'];
                $fac_arr_s[$f_code]['commodity_id'] = 4;

            }

            $fac_by_month[$f_code][$m_y][4]['county_id'] = $value['county_id'];
            $fac_by_month[$f_code][$m_y][4]['county_name'] = $value['county'];
            $fac_by_month[$f_code][$m_y][4]['district_id'] = $value['district_id'];
            $fac_by_month[$f_code][$m_y][4]['district_name'] = $value['district'];
            $fac_by_month[$f_code][$m_y][4]['facility_code'] = $value['facility_code'];
            $fac_by_month[$f_code][$m_y][4]['facility_name'] = $value['facility_name'];
            $fac_by_month[$f_code][$m_y][4]['order_date'] = $value['order_date'];
            $fac_by_month[$f_code][$m_y][4]['beginning_bal'] = $value['beginning_bal'];
            $fac_by_month[$f_code][$m_y][4]['q_received'] = $value['q_received'];
            $fac_by_month[$f_code][$m_y][4]['q_used'] = $value['q_used'];
            $fac_by_month[$f_code][$m_y][4]['no_of_tests_done'] = $value['no_of_tests_done'];
            $fac_by_month[$f_code][$m_y][4]['closing_stock'] = $value['closing_stock'];
            $fac_by_month[$f_code][$m_y][4]['positive_adj'] = $value['positive_adj'];
            $fac_by_month[$f_code][$m_y][4]['negative_adj'] = $value['negative_adj'];
            $fac_by_month[$f_code][$m_y][4]['losses'] = $value['losses'];
            $fac_by_month[$f_code][$m_y][4]['order_date'] = $value['order_date'];
            $fac_by_month[$f_code][$m_y][4]['level'] = "facility";
            $fac_by_month[$f_code][$m_y][4]['level_id'] = $value['facility_code'];
            $fac_by_month[$f_code][$m_y][4]['commodity_id'] = 4;
            
        }//end of summary data foreach

         // echo "<pre>";print_r($counties);exit;
        foreach ($summary_data_c as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $c_id = $value['county_id'];
            $c_name = $value['county'];
            $d_id = $value['district_id'];
            $d_name = $value['district'];
            $f_code = $value['facility_code'];
            $f_name = $value['facility_name'];
            $o_date = $value['order_date'];
            $m_y = date('mY',strtotime($o_date));

            
            if (count($county_arr_c[$c_id]) > 0) {
                $county_arr_c[$c_id]['beginning_bal'] += $value['beginning_bal'];
                $county_arr_c[$c_id]['q_received'] += $value['q_received'];
                $county_arr_c[$c_id]['q_used'] += $value['q_used'];
                $county_arr_c[$c_id]['no_of_tests_done'] += $value['no_of_tests_done'];
                $county_arr_c[$c_id]['closing_stock'] += $value['closing_stock'];
                $county_arr_c[$c_id]['positive_adj'] += $value['positive_adj'];
                $county_arr_c[$c_id]['negative_adj'] += $value['negative_adj'];
                $county_arr_c[$c_id]['losses'] += $value['losses'];
            }else{
                $county_arr_c[$c_id]['county_id'] = $value['county_id'];
                $county_arr_c[$c_id]['county_name'] = $value['county'];
                $county_arr_c[$c_id]['district_id'] = $value['district_id'];
                $county_arr_c[$c_id]['district_name'] = $value['district'];
                $county_arr_c[$c_id]['facility_code'] = $value['facility_code'];
                $county_arr_c[$c_id]['facility_name'] = $value['facility_name'];
                $county_arr_c[$c_id]['order_date'] = $value['order_date'];
                $county_arr_c[$c_id]['beginning_bal'] = $value['beginning_bal'];
                $county_arr_c[$c_id]['q_received'] = $value['q_received'];
                $county_arr_c[$c_id]['q_used'] = $value['q_used'];
                $county_arr_c[$c_id]['no_of_tests_done'] = $value['no_of_tests_done'];
                $county_arr_c[$c_id]['closing_stock'] = $value['closing_stock'];
                $county_arr_c[$c_id]['positive_adj'] = $value['positive_adj'];
                $county_arr_c[$c_id]['negative_adj'] = $value['negative_adj'];
                $county_arr_c[$c_id]['losses'] = $value['losses'];

                $county_arr_c[$c_id]['level'] = "county";
                $county_arr_c[$c_id]['level_id'] = $value['county_id'];
                $county_arr_c[$c_id]['commodity_id'] = 5;
            }

            if (count($dist_arr_c[$d_id]) > 0) {
                $dist_arr_c[$d_id]['beginning_bal'] += $value['beginning_bal'];
                $dist_arr_c[$d_id]['q_received'] += $value['q_received'];
                $dist_arr_c[$d_id]['q_used'] += $value['q_used'];
                $dist_arr_c[$d_id]['no_of_tests_done'] += $value['no_of_tests_done'];
                $dist_arr_c[$d_id]['closing_stock'] += $value['closing_stock'];
                $dist_arr_c[$d_id]['positive_adj'] += $value['positive_adj'];
                $dist_arr_c[$d_id]['negative_adj'] += $value['negative_adj'];
                $dist_arr_c[$d_id]['losses'] += $value['losses'];
            }else{
                $dist_arr_c[$d_id]['county_id'] = $value['county_id'];
                $dist_arr_c[$d_id]['county_name'] = $value['county'];
                $dist_arr_c[$d_id]['district_id'] = $value['district_id'];
                $dist_arr_c[$d_id]['district_name'] = $value['district'];
                $dist_arr_c[$d_id]['facility_code'] = $value['facility_code'];
                $dist_arr_c[$d_id]['facility_name'] = $value['facility_name'];
                $dist_arr_c[$d_id]['order_date'] = $value['order_date'];
                $dist_arr_c[$d_id]['beginning_bal'] = $value['beginning_bal'];
                $dist_arr_c[$d_id]['q_received'] = $value['q_received'];
                $dist_arr_c[$d_id]['q_used'] = $value['q_used'];
                $dist_arr_c[$d_id]['no_of_tests_done'] = $value['no_of_tests_done'];
                $dist_arr_c[$d_id]['closing_stock'] = $value['closing_stock'];
                $dist_arr_c[$d_id]['positive_adj'] = $value['positive_adj'];
                $dist_arr_c[$d_id]['negative_adj'] = $value['negative_adj'];
                $dist_arr_c[$d_id]['losses'] = $value['losses'];

                $dist_arr_c[$d_id]['level'] = "subcounty";
                $dist_arr_c[$d_id]['level_id'] = $value['district_id'];
                $dist_arr_c[$d_id]['commodity_id'] = 5;
            }

            if (count($fac_arr_c[$f_code]) > 0) {
                $fac_arr_c[$f_code]['beginning_bal'] += $value['beginning_bal'];
                $fac_arr_c[$f_code]['q_received'] += $value['q_received'];
                $fac_arr_c[$f_code]['q_used'] += $value['q_used'];
                $fac_arr_c[$f_code]['no_of_tests_done'] += $value['no_of_tests_done'];
                $fac_arr_c[$f_code]['closing_stock'] += $value['closing_stock'];
                $fac_arr_c[$f_code]['positive_adj'] += $value['positive_adj'];
                $fac_arr_c[$f_code]['negative_adj'] += $value['negative_adj'];
                $fac_arr_c[$f_code]['losses'] += $value['losses'];
            }else{
                $fac_arr_c[$f_code]['county_id'] = $value['county_id'];
                $fac_arr_c[$f_code]['county_name'] = $value['county'];
                $fac_arr_c[$f_code]['district_id'] = $value['district_id'];
                $fac_arr_c[$f_code]['district_name'] = $value['district'];
                $fac_arr_c[$f_code]['facility_code'] = $value['facility_code'];
                $fac_arr_c[$f_code]['facility_name'] = $value['facility_name'];
                $fac_arr_c[$f_code]['order_date'] = $value['order_date'];
                $fac_arr_c[$f_code]['beginning_bal'] = $value['beginning_bal'];
                $fac_arr_c[$f_code]['q_received'] = $value['q_received'];
                $fac_arr_c[$f_code]['q_used'] = $value['q_used'];
                $fac_arr_c[$f_code]['no_of_tests_done'] = $value['no_of_tests_done'];
                $fac_arr_c[$f_code]['closing_stock'] = $value['closing_stock'];
                $fac_arr_c[$f_code]['positive_adj'] = $value['positive_adj'];
                $fac_arr_c[$f_code]['negative_adj'] = $value['negative_adj'];
                $fac_arr_c[$f_code]['losses'] = $value['losses'];
                $fac_arr_c[$f_code]['level'] = "facility";
                $fac_arr_c[$f_code]['level_id'] = $value['facility_code'];
                $fac_arr_c[$f_code]['commodity_id'] = 5;
            }

            $fac_by_month[$f_code][$m_y][5]['county_id'] = $value['county_id'];
            $fac_by_month[$f_code][$m_y][5]['county_name'] = $value['county'];
            $fac_by_month[$f_code][$m_y][5]['district_id'] = $value['district_id'];
            $fac_by_month[$f_code][$m_y][5]['district_name'] = $value['district'];
            $fac_by_month[$f_code][$m_y][5]['facility_code'] = $value['facility_code'];
            $fac_by_month[$f_code][$m_y][5]['facility_name'] = $value['facility_name'];
            $fac_by_month[$f_code][$m_y][5]['order_date'] = $value['order_date'];
            $fac_by_month[$f_code][$m_y][5]['beginning_bal'] = $value['beginning_bal'];
            $fac_by_month[$f_code][$m_y][5]['q_received'] = $value['q_received'];
            $fac_by_month[$f_code][$m_y][5]['q_used'] = $value['q_used'];
            $fac_by_month[$f_code][$m_y][5]['no_of_tests_done'] = $value['no_of_tests_done'];
            $fac_by_month[$f_code][$m_y][5]['closing_stock'] = $value['closing_stock'];
            $fac_by_month[$f_code][$m_y][5]['positive_adj'] = $value['positive_adj'];
            $fac_by_month[$f_code][$m_y][5]['negative_adj'] = $value['negative_adj'];
            $fac_by_month[$f_code][$m_y][5]['losses'] = $value['losses'];
            $fac_by_month[$f_code][$m_y][5]['order_date'] = $value['order_date'];
            $fac_by_month[$f_code][$m_y][5]['level'] = "facility";
            $fac_by_month[$f_code][$m_y][5]['level_id'] = $value['facility_code'];
            $fac_by_month[$f_code][$m_y][5]['commodity_id'] = 5;

        }//end of summary data foreach

        // echo "<pre>";print_r($county_arr_s);exit;
        // echo "<pre>";print_r($dist_arr_s);exit;
        // echo "<pre>";print_r($fac_by_month);exit;
        // echo "<pre>";print_r($fac_by_month_c);exit;

        $style_green = ' style = "color:green;font-weight:bold;" ';
        $style_red = ' style = "color:red;font-weight:bold;" ';
        
        /*COUNTY AGGREGATES*/
            $county_name = $county_arr_s[$county_id]['county_name'];
            $beg_bal_s = $county_arr_s[$county_id]['beginning_bal'];
            $rec_s = $county_arr_s[$county_id]['q_received'];
            $pos_adj_s = $county_arr_s[$county_id]['positive_adj'];
            $used_s = $county_arr_s[$county_id]['q_used'];
            $losses_s = $county_arr_s[$county_id]['losses'];
            $neg_adj_s = $county_arr_s[$county_id]['negative_adj'];
            $clos_bal_s = $county_arr_s[$county_id]['closing_stock'];
            $reported_s = $county_arr_s[$county_id]['closing_stock'];

            $expected_s = ($beg_bal_s + $rec_s + $pos_adj_s)-($used_s + $losses_s + $neg_adj_s); 

            // echo "<pre>";print_r($expected_s);exit;
            $diff_s = $reported_s - $expected_s;

            $beg_bal_c = $county_arr_c[$county_id]['beginning_bal'];
            $rec_c = $county_arr_c[$county_id]['q_received'];
            $pos_adj_c = $county_arr_c[$county_id]['positive_adj'];
            $used_c = $county_arr_c[$county_id]['q_used'];
            $losses_c = $county_arr_c[$county_id]['losses'];
            $neg_adj_c = $county_arr_c[$county_id]['negative_adj'];
            $clos_bal_c = $county_arr_c[$county_id]['closing_stock'];
            $reported_c = $county_arr_c[$county_id]['closing_stock'];

            $expected_c = ($beg_bal_c + $rec_c + $pos_adj_c)-($used_c + $losses_c + $neg_adj_c); 

            $diff_c = $reported_c - $expected_c;
            
            $styling_s = ($diff_s>0)? $style_green:$style_red;
            $styling_s = ($diff_s==0)? "":$styling_s;

            $styling_c = ($diff_c>0)? $style_green:$style_red;
            $styling_c = ($diff_c==0)? "":$styling_c;

            $table_body_1 .= '<tr>';
            $table_body_1 .= '<td>' . $county_name . '</td>';
            $table_body_1 .= '<td>' . number_format($expected_s) . '</td>';
            $table_body_1 .= '<td>' . number_format($reported_s) . '</td>';
            $table_body_1 .= '<td "'.$styling_s.'">' . number_format($diff_s) . '</td>';

            $table_body_1 .= '<td>' . number_format($expected_c) . '</td>';
            $table_body_1 .= '<td>' . number_format($reported_c) . '</td>';
            $table_body_1 .= '<td "'.$styling_c.'">' . number_format($diff_c) . '</td>';
            $table_body_1 .= '</tr>';

            // echo "<pre>";print_r($table_body);exit;
            $table_foot_1 = '</tbody></table>';
            $table_data_1 = $table_head_1. $table_body_1. $table_foot_1;
            
            $table_data .= $table_data_1;
        /*END OF COUNTY AGGREGATES*/
    
        /*SUBCOUNTY AGGREGATES*/

            $table_head_2 = '
                <table border="0" class="data-table" style="width: 100%; margin: 10px auto;">
                <thead border="0" style="margin: 10px auto;font-weight:900">
                    <tr>
                        <th colspan="7">Subcounty Aggregates</th>
                    </tr>
                    <tr>
                        <th rowspan="2">Subcounty</th>
                        <th colspan="3">Screening</th>
                        <th colspan="3">Confirmatory</th>
                    </tr>
                    <tr>
                        <th>Expected</th>
                        <th>Reported</th>
                        <th>Difference</th>

                        <th>Expected</th>
                        <th>Reported</th>
                        <th>Difference</th>
                    </tr>
                </thead>
                <tbody>';

            foreach ($dist_arr_s as $key => $value) {
                // echo "<pre>";print_r($key);
                // echo "<pre>";print_r($value);exit;
                $d_id = $key;
                $county_name = $dist_arr_s[$d_id]['county_name'];
                $district_name = $dist_arr_s[$d_id]['district_name'];
                $beg_bal_s = $dist_arr_s[$d_id]['beginning_bal'];
                $rec_s = $dist_arr_s[$d_id]['q_received'];
                $pos_adj_s = $dist_arr_s[$d_id]['positive_adj'];
                $used_s = $dist_arr_s[$d_id]['q_used'];
                $losses_s = $dist_arr_s[$d_id]['losses'];
                $neg_adj_s = $dist_arr_s[$d_id]['negative_adj'];
                $clos_bal_s = $dist_arr_s[$d_id]['closing_stock'];
                $reported_s = $dist_arr_s[$d_id]['closing_stock'];

                $expected_s = ($beg_bal_s + $rec_s + $pos_adj_s)-($used_s + $losses_s + $neg_adj_s); 

                // echo "<pre>";print_r($expected_s);exit;
                $diff_s = $reported_s - $expected_s;

                $beg_bal_c = $dist_arr_c[$d_id]['beginning_bal'];
                $rec_c = $dist_arr_c[$d_id]['q_received'];
                $pos_adj_c = $dist_arr_c[$d_id]['positive_adj'];
                $used_c = $dist_arr_c[$d_id]['q_used'];
                $losses_c = $dist_arr_c[$d_id]['losses'];
                $neg_adj_c = $dist_arr_c[$d_id]['negative_adj'];
                $clos_bal_c = $dist_arr_c[$d_id]['closing_stock'];
                $reported_c = $dist_arr_c[$d_id]['closing_stock'];

                $expected_c = ($beg_bal_c + $rec_c + $pos_adj_c)-($used_c + $losses_c + $neg_adj_c); 

                $diff_c = $expected_c - $reported_c;
                
                $styling_s = ($diff_s>0)? $style_green:$style_red;
                $styling_s = ($diff_s==0)? "":$styling_s;

                $styling_c = ($diff_c>0)? $style_green:$style_red;
                $styling_c = ($diff_c==0)? "":$styling_c;

                $table_body_2 .= '<tr>';
                $table_body_2 .= '<td>' . $district_name . '</td>';
                $table_body_2 .= '<td>' . number_format($expected_s) . '</td>';
                $table_body_2 .= '<td>' . number_format($reported_s) . '</td>';
                $table_body_2 .= '<td "'.$styling_s.'">' . number_format($diff_s) . '</td>';

                $table_body_2 .= '<td>' . number_format($expected_c) . '</td>';
                $table_body_2 .= '<td>' . number_format($reported_c) . '</td>';
                $table_body_2 .= '<td "'.$styling_c.'">' . number_format($diff_c) . '</td>';
                $table_body_2 .= '</tr>';

                // echo "<pre>";print_r($table_body);exit;
            }
            
            $table_foot_2 = '</tbody></table>';
            $table_data_2 = $table_head_2. $table_body_2. $table_foot_2;
            $table_data .= $table_data_2;
        /*END OF SUBCOUNTY AGGREGATES*/
        
        /*FACILITY AGGREGATES*/
            $sorted_fac_arr = $fac_by_month;
            /*usort($sorted_fac_arr, function($a, $b) {
                    // return $a['screening_balance'] - $b['screening_balance'];
                    return $a['district_id'] - $b['district_id'];
            });*/
            // echo "<pre>";print_r($fac_arr_s);exit;
            // echo "<pre>";print_r($sorted_fac_arr);exit;
            $table_head_3 = '
                <table border="0" class="data-table" style="width: 100%; margin: 10px auto;">
                <thead border="0" style="margin: 10px auto;font-weight:900">
                    <tr>
                        <th colspan="10">Facility Aggregates</th>
                    </tr>
                    <tr>
                        <th rowspan="2">MFL</th>
                        <th rowspan="2">Facility Name</th>
                        <th rowspan="2">Subcounty</th>
                        <th rowspan="2">Report Date</th>
                        <th colspan="3">Screening</th>
                        <th colspan="3">Confirmatory</th>
                    </tr>
                    <tr>
                        <th>Expected</th>
                        <th>Reported</th>
                        <th>Difference</th>

                        <th>Expected</th>
                        <th>Reported</th>
                        <th>Difference</th>
                    </tr>
                </thead>
                <tbody>';

            foreach ($sorted_fac_arr as $key => $mnth) {
                // echo "<pre>";print_r($key);exit;
                // echo "<pre>";print_r($value);exit;
                // $f_code = $key;
                foreach ($mnth as $keyy => $value) {
                    // echo "<pre>";print_r($value);exit;
                    $f_code = $value[4]['facility_code'];
                    $county_name = $value[4]['county_name'];
                    $district_name = $value[4]['district_name'];
                    $facility_name = $value[4]['facility_name'];
                    $order_date = $value[4]['order_date'];

                    $beg_bal_s = $value[4]['beginning_bal'];
                    $rec_s = $value[4]['q_received'];
                    $pos_adj_s = $value[4]['positive_adj'];
                    $used_s = $value[4]['q_used'];
                    $losses_s = $value[4]['losses'];
                    $neg_adj_s = $value[4]['negative_adj'];
                    $clos_bal_s = $value[4]['closing_stock'];
                    $reported_s = $value[4]['closing_stock'];

                    $expected_s = ($beg_bal_s + $rec_s + $pos_adj_s)-($used_s + $losses_s + $neg_adj_s); 

                    // echo "<pre>";print_r($expected_s);exit;
                    $diff_s = $reported_s - $expected_s;

                    $beg_bal_c = $value[5]['beginning_bal'];
                    $rec_c = $value[5]['q_received'];
                    $pos_adj_c = $value[5]['positive_adj'];
                    $used_c = $value[5]['q_used'];
                    $losses_c = $value[5]['losses'];
                    $neg_adj_c = $value[5]['negative_adj'];
                    $clos_bal_c = $value[5]['closing_stock'];
                    $reported_c = $value[5]['closing_stock'];

                    $expected_c = ($beg_bal_c + $rec_c + $pos_adj_c)-($used_c + $losses_c + $neg_adj_c); 

                    $diff_c = $expected_c - $reported_c;
                    
                    $styling_s = ($diff_s>0)? $style_green:$style_red;
                    $styling_s = ($diff_s==0)? "":$styling_s;

                    $styling_c = ($diff_c>0)? $style_green:$style_red;
                    $styling_c = ($diff_c==0)? "":$styling_c;

                    // echo "<pre>";print_r($diff_s);
                    // echo "<pre>";print_r($diff_c);
                    if ($diff_s != 0 || $diff_c != 0):

                    $table_body_3 .= '<tr>';
                    $table_body_3 .= '<td>' . $f_code . '</td>';
                    $table_body_3 .= '<td>' . $facility_name . '</td>';
                    // $table_body_3 .= '<td>' . $county_name . '</td>';
                    $table_body_3 .= '<td>' . $district_name . '</td>';
                    $table_body_3 .= '<td>' . date('F Y',strtotime("-1 MONTH",strtotime($order_date))) . '</td>';
                    $table_body_3 .= '<td>' . number_format($expected_s) . '</td>';
                    $table_body_3 .= '<td>' . number_format($reported_s) . '</td>';
                    $table_body_3 .= '<td "'.$styling_s.'">' . number_format($diff_s) . '</td>';

                    $table_body_3 .= '<td>' . number_format($expected_c) . '</td>';
                    $table_body_3 .= '<td>' . number_format($reported_c) . '</td>';
                    $table_body_3 .= '<td "'.$styling_c.'">' . number_format($diff_c) . '</td>';
                    $table_body_3 .= '</tr>';

                    endif;
                }
                
                // echo "<pre>";print_r($table_body);exit;
            }
            
            $table_foot_3 = '</tbody></table>';
            $table_data_3 = $table_head_3. $table_body_3. $table_foot_3;
            $table_data .= $table_data_3;
        /*END OF FACILITY AGGREGATES*/

        // echo "<pre>";print_r($table_data_3);exit;
        $html_data = $html_title . $table_data;
        // echo "<pre>";print_r($html_data);exit;
        // echo "<pre>";print_r($table_data);exit;
        $pdf_title = "<div style='text-align:center; font-size: 14px;display: block;font-weight: bold;'>Rapid Test Kits (RTK) System</div>
        <div style='text-align:center; font-size: 14px;display: block;font-weight: bold;'>Closing Balance Discrepancies as of $englishdate</div>
        <div style='text-align:center; font-size: 14px;display: block;font-weight: bold;'>$team_name</div>";
        
        // echo "$html_data";die();
        // echo "<pre>";print_r($html_data);exit;
        // echo "<pre>";print_r($reportname);exit;

        // $pdf = $this->create_pdf($html_data, $reportname);
        $pdf_data['pdf_html_body'] = $table_data;
        $pdf_data['file_name'] = $reportname;
        $pdf_data['pdf_title'] = $pdf_title;
        $pdf_data['pdf_water_mark'] = 'BALANCE DISCREPANCIES';
        $pdf_data['pdf_view_option'] = "save_file";
        $pdf_data['pdf_save_location'] = "tmp/";
        // echo "<pre>";print_r($pdf_data);exit;
        $pdf = $this->hcmp_functions->create_pdf($pdf_data);
        // echo "<pre>";print_r($pdf);exit;
        //echo "<pre>";print_r($pdf);exit;
        // $email_address,$message,$subject,$attach_file=NULL,$bcc_email=NULL,$cc_email=NULL,$full_name=NULL

        $message = '<b>Dear '.$team_name.' Team,</b><br/><br>Find attached Closing Balances Discrepancies Report for your sites for 2017.<br>The same is accessible on the dashboard or directly via the link below.<br><br><a href="'.base_url()."dashboardv2/balance_discrepancy_dashboard".'">'.base_url()."dashboardv2/balance_discrepancy_dashboard".'</a><br><br>';
        $message.= 'Kindly work with your facilities to get the correct numbers, which should then be updated online by the SMLTS on their log in.<br>To edit FCDRR:';
        $message.='<br>(I)     Log in as SCMLT for your subcounty.';
        $message.='<br>(II)    Click on the <b>Reports</b> link on the side menu.';
        $message.='<br>(III)   Search for report to be edited (by facility,MFL and/or by month)';
        $message.='<br>(IV)    Click on <b>Edit</b> link';
        $message.='<br>(V)     FCDRR loads, make changes where necessary';
        $message.='<br>(VI)    Click on <b>Update Order</b> button to save the changes';
        $message.='<br>(VII)   Move to next facility that needs editing.';
        // $message .= 'Kindly ignore the issues list sent on 7th November 2017';
        $message .= '<br><br>In case of any issues/concerns, kindly reach out to the key contact persons for clarification.<br><br>Thanks.<br>--<br>RTK Support Team<br><b><small>This email was automatically generated. Please do not respond to this email address or it will be ignored.</small></b>';
        // echo "<pre>";print_r($message);exit;
        $recepients = $this->get_email_recepients($county_id,3);
        // echo "<pre>";print_r($recepients);exit;
        
        $email_address = $recepients['main'];
        $cc_email = $recepients['cc'];
        $bcc_email = $recepients['bcc'];
        

        // $email_address = 'karsanrichard@gmail.com';
        // $cc_email = 'tngugi@clintonhealthaccess.org';
        // $bcc_email = 'sethrichard40@gmail.com';

        // echo $cc_email;exit;
        $subject = $team_name.' Closing Balance Discrepancies Report for 2017';
        $file_path = FCPATH.'tmp/'.$reportname.'.pdf';
        // $attach_file = array('attach_file' => $file_path);
        $attach_file = $file_path;
        // echo 'HERE';exit;
        $fac_arr_count = count($fac_arr);
        // echo "<pre>";print_r($attach_file);exit;
        // $fac_arr_count = 0;
        $send_mail = $this->hcmp_functions->send_email($email_address,$message,$subject,$attach_file,$bcc_email,$cc_email,$full_name);
    }

    public function get_previous_and_next_quarter($quarter,$year = NULL)
    {
        $year = ($year>0)?$year:date('Y');
        $prev_quarter = $quarter - 1;
        if ($prev_quarter < 1) {
            $prev_quarter = 4;
            $prev_year = $year - 1;
        }else {
            // $prev_quarter = 4;
            $prev_year = $year;
        }

        $next_quarter = $quarter + 1;
        if ($next_quarter > 4) {
            $next_quarter = 1;
            $next_year = $year + 1;
        }else {
            // $next_quarter = 4;
            $next_year = $year;
        }

        switch ($prev_quarter) {
            case 1:
            $first_month = "01";
            $last_month = "03";
            break;
            case 2:
            $first_month = "04";
            $last_month = "06";
            break;
            case 3:
            $first_month = "07";
            $last_month = "09";
            break;
            case 4:
            $first_month = "10";
            $last_month = "12";
            break;
            
            default:
                // echo "Quarter: Invalid";exit;
            break;
        }

        $prev_firstdate = $prev_year . '-' . $first_month . '-01';
        $prev_lastdate = $prev_year . '-' . $last_month . '-31';

        switch ($next_quarter) {
            case 1:
            $first_month = "01";
            $last_month = "03";
            break;
            case 2:
            $first_month = "04";
            $last_month = "06";
            break;
            case 3:
            $first_month = "07";
            $last_month = "09";
            break;
            case 4:
            $first_month = "10";
            $last_month = "12";
            break;
            
            default:
                // echo "Quarter: Invalid";exit;
            break;
        }

        $next_firstdate = $next_year . '-' . $first_month . '-01';
        $next_lastdate = $next_year . '-' . $last_month . '-31';

        $quarter_data['previous_quarter'] = $prev_quarter;
        $quarter_data['previous_year'] = $prev_year;
        $quarter_data['previous_first_date'] = $prev_firstdate;
        $quarter_data['previous_last_date'] = $prev_lastdate;
        $quarter_data['next_quarter'] = $next_quarter;
        $quarter_data['next_year'] = $next_year;
        $quarter_data['next_first_date'] = $next_firstdate;
        $quarter_data['next_last_date'] = $next_lastdate;

        // echo "<pre>";print_r($quarter_data);exit;
        return $quarter_data;
    }

    public function get_predefined_allocations()
    {
        $query = "SELECT * FROM allocations_predefined";
        $result = $this->db->query($query)->result_array();

        $final_data = array();
        foreach ($result as $key => $value) {
            $mfl = $value['facility_code'];
            $final_data[$mfl]['county'] = $value['county'];
            $final_data[$mfl]['district'] = $value['district'];
            $final_data[$mfl]['facility_code'] = $value['facility_code'];
            $final_data[$mfl]['screening_allocated'] = $value['screening_allocated'];
            $final_data[$mfl]['confirmatory_allocated'] = $value['confirmatory_allocated'];
        }
        return $final_data;
    }

    public function update_reporting_percentages($county_id = NULL,$district_id = NULL,$facility_code = NULL,$month = NULL,$year = NULL)
    {
        //PREVIOUSLY update_all_reporting_percentages
        $date = ($month > 0 && $year > 0)? $year.'-'.$month.'-01':NULL;
        // echo "<pre>";print_r($date);exit;
        $where .= (isset($district_id) && $district_id > 0)? " AND id = $district_id":NULL;
        $where .= (isset($county_id) && $county_id > 0)? " AND county = $county_id":NULL;
        $districts_query = "SELECT * FROM districts WHERE id > 0 $where";
        // echo "<pre>";print_r($districts_query);exit;
        $districts = $this->db->query($districts_query)->result_array();

        // echo "<pre>";print_r($districts);exit;
        $output = array();

        foreach ($districts as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $total = $reported = $nonreported = 0;
            $district = $value['id'];
            $c_id = $value['county'];
            $facilities = Facilities::get_total_facilities_rtk_in_district($district);
            // echo "<pre>";print_r($facilities);exit;
            foreach ($facilities as $keyy => $valuee) {
                // echo "<pre>";print_r($valuee);exit;
                $lab_count = lab_commodity_orders::get_recent_lab_orders($valuee['facility_code'],$date);
                // echo "<pre>";print_r($lab_count);exit;
                if ($lab_count > 0) {
                    $reported = $reported + 1;
                } else {
                    $nonreported = $nonreported + 1;
                }
            }

            $total = $reported + $nonreported;
            $percentage_complete = ceil($reported / $total * 100);
            $percentage_complete = number_format($percentage_complete, 0);

            // echo "<pre>";print_r($total);exit;

            if (isset($date) && $date !='') {
                $month_db = date('mY',strtotime($date));
            }else{
                $m = substr($month, 0, 2);
                $y = substr($month, 2);
                $new_month = $y . '-' . $m . '-01';
                $d = new DateTime("$new_month"); 
                $d->modify('last day of next month');
                $month_db = $d->format('mY');
            }

            // echo $district.' '.$total.' '.$reported.' '.$month_db.' '.$percentage_complete;exit;
            $percentage_update = $this->update_district_reporting_rates($district,$total,$reported,$month_db,$percentage_complete);
            // echo "<pre>";print_r($percentage_update);exit;
            // echo "<pre>DISTRICT ID:$district; TOTAL:$total; REPORTED:$reported; REPORTS FOR $month_db UPDATED";
            $output['districts'][] = "DISTRICT ID:$district TOTAL:$total REPORTED:$reported REPORTS FOR $month_db UPDATED";
            
            // $output['districts'][] = "ALL DISTRICTS UPDATED SUCCESSFULLY";
            $c_where = '';
            $c_where .= (isset($c_id) && $c_id > 0)? " WHERE id = $c_id":NULL;
            $counties_query = "SELECT * FROM counties $c_where";
            // echo "<pre>";print_r($counties_query);exit;
            $counties = $this->db->query($counties_query);
            // echo "<pre>";print_r($counties);exit;
            // echo "<pre>ALL DISTRICTS UPDATED SUCCESSFULLY";
            $row_num = $counties->num_rows();
            // echo "<pre>";print_r($row_num);exit;
            if ($row_num > 0) {
                $counties = $counties ->result_array();
                foreach ($counties as $key => $value) {
                    $county_id = $value['id'];
                    // $month_db = $d->format('mY');
                    // echo $month_db." ".$county_id;exit;
                    $percentage_update_county = $this->update_county_reporting_rates($month_db,$county_id);
                    // echo "<pre>COUNTIES ".$county_id." ".$percentage_update_county."% UPDATED SUCCESFULLY";
                    $output['counties'][] = "COUNTIES ".$county_id." ".$percentage_update_county."% UPDATED SUCCESFULLY";
                }
            }

            // echo "<pre>DISTRICT ".$district." UPDATED SUCCESFULLY";
        
        }

        // echo "<pre>ALL COUNTIES UPDATED SUCCESSFULLY";
        $output['counties'][] =  "ALL COUNTIES UPDATED SUCCESSFULLY";
        // echo "<pre>";print_r($output);exit;
        return $output;
    }

    public function update_district_reporting_rates($district_id,$total_facilities,$reported,$date,$percentage_complete)
    {
        // echo $district." ".$total_facilities." ".$reported." ".$date." ".$percentage_complete;exit;
        
        // $percentage_complete = ceil($reported / $total_facilities * 100);
        // $percentage_complete = number_format($percentage_complete, 0);

        // echo "<pre>";print_r($percentage_complete);exit;

        $check_query = "
        SELECT 
            *
        FROM
            rtk_district_percentage
        WHERE
        district_id = $district_id
        AND month = '$date'";

        $check = $this->db->query($check_query);
        if ($check) {
            $check = $check->result_array();
        } else {
            $check = NULL;
        }
        

        // echo "<pre>";print_r(count($check));exit;

        if ($check > 0) {
            $operation = "UPDATE";
            $update_query = "
            UPDATE rtk_district_percentage 
            SET 
                `facilities` = '$total_facilities',
                `reported` = '$reported',
                `percentage` = '$percentage_complete'
            WHERE
                district_id = $district_id
                AND month=$date";

            // echo $update_query;exit;

            $result = $this->db->query($update_query);
        } else {
            $operation = "INSERT";
            $insert_query = "
            INSERT INTO rtk_district_percentage 
            (`id`, `district_id`, `facilities`, `reported`, `percentage`, `month`) 
            VALUES ('', '$district_id', '$total_facilities', '$reported', '$percentage', '$date')";

            $result = $this->db->query($insert_query);
        }
        
        // echo "<pre>";print_r($result);exit;
        // echo $operation.' '.$result;exit;
        return $operation.' '.$result;

    }

    public function update_county_reporting_rates($month = null, $county_id)
    {
        if (isset($month)) {
            $year = substr($month, -4);
            $month = substr($month, 0, 2);
            $month_year = $month.$year;
        }

        $query = "
        SELECT 
            r.*,
            d.*
        FROM
            rtk_district_percentage r, districts d
        WHERE
            month = $month_year AND d.id = r.district_id AND d.county = $county_id ";
        // echo "<pre>";print_r($query);exit;
        $result = $this->db->query($query)->result_array();
        $total = $reported = $not_reported = 0;
        // echo "<pre>";print_r($result);exit;
        foreach ($result as $key => $value) {
            $total += $value['facilities'];
            $reported += $value['reported'];
        }
        // echo "<pre>";print_r($result);
        // echo "<pre>".$total." ".$reported;exit;
        $percentage = ceil(($reported/$total)*100);
        if ($percentage>100) {
            $percentage = 100;
        } elseif ($percentage<0) {
            $percentage = 0;
        }

        // echo "<pre>";print_r($percentage);exit;
        
        $update_query = "UPDATE rtk_county_percentage SET percentage='$percentage',reported='$reported',facilities='$total' WHERE `county_id`='$county_id' AND month = '$month_year'";
        // echo "<pre>";print_r($update_query);exit;
        $result = $this->db->query($update_query);
        // echo "<pre>";print_r($percentage);exit;
        
        return $percentage;
    }

    public function get_drawing_rights_details($county_id = NULL, $district_id = NULL,$facility_code = NULL, $quarter = NULL,$month_year = NULL,$year = NULL)
    {   
        // echo "COUNTY ID: ".$county_id."DISTRICT: ".$district_id."MFL: ".$facility_code."MONTH: ".$month." YEAR: ".$year." QUARTER: ".$quarter." MONTH_YEAR: ".$month_year;exit;

        $county_id = (isset($county_id) && $county_id > 0)? $county_id:NULL;
        $district_id = (isset($district_id) && $district_id > 0)? $district_id:NULL;
        $facility_code = (isset($facility_code) && $facility_code > 0)? $facility_code:NULL;
        $quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;
        $year = (isset($year) && $year > 0)? $year:NULL;
        $month = (isset($month) && $month > 0)? $month:NULL;
        $filtered = 0;

        $current = date('m');
        $curMonth = $current;
        $curQuarter = ceil($curMonth/3);//KARSAN
        // echo "<pre>";print_r($month);exit;
        if ($quarter > 0 && $month < 1) {
            $curQuarter = $quarter;
            
        }else{
            $quarter_data = $this->get_year_quarter($month,NULL,$year,NULL);
            $curQuarter = $quarter_data['quarter'];
        }
            $data['data_quarter'] = $curQuarter;

        if ($year < date('Y')) {
            $year_quarters = 4;
        }else{
            $year_quarters = $curQuarter;
        }

        if ($year > 0) {
            // echo "IF";exit;
            $filtered = 1;
        }else{
            // echo "ELSE";exit;
            $year = date('Y');
        }
            $data['data_year'] = $year;

        $summary_and .= (isset($county_id) && $county_id!='')? " WHERE county_id = $county_id" :NULL; 
        // $summary_and .= (isset($quarter) && $quarter>0)? " AND quarter = $quarter" :NULL; 
        
        $c_drawing_query = "SELECT * FROM county_drawing_rights $summary_and";
        // echo "<pre>";print_r($c_drawing_query);
        $county_drawing_data = $this->db->query($c_drawing_query)->result_array();
        // echo "<pre>";print_r($county_drawing_data);exit;
        $drawing_totals = array();

        foreach ($county_drawing_data as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $c_id = $value['county_id'];
            if ($year == 2017) {
                if ($quarter < 1 || $quarter > 3) {
                    $qr_multiplier = 3;//In this year, allocations for Q1 were done outside the system and thus not added onto the totals.
                }else{
                    $qr_multiplier = $curQuarter;
                }
            }else{
                $qr_multiplier = $curQuarter;
            }

            // echo "<pre>";print_r($qr_multiplier);exit;
            $drawing_totals[$c_id]['screening_default_annual'] = $value['screening_total'] * $qr_multiplier;
            $drawing_totals[$c_id]['confirmatory_default_annual'] = $value['confirmatory_total'] * $qr_multiplier;
            $drawing_totals[$c_id]['quarter_multiplier'] = $qr_multiplier;
            $drawing_totals[$c_id]['screening_total_per_quarter'] = $value['screening_total'];
            $drawing_totals[$c_id]['confirmatory_total_per_quarter'] = $value['confirmatory_total'];
        }


        $summary_and_ .= (isset($county_id) && $county_id!='')? " AND county_id = $county_id" :NULL; 
        $summary_and_ .= (isset($quarter) && $quarter>0)? " AND quarter = $quarter" :NULL; 
        $c_d_query = "
            SELECT c.county,cd.* 
            FROM county_drawing_rights_details cd,counties c 
            WHERE cd.county_id = c.id AND year = $year $summary_and_ 
            GROUP BY quarter,year,month, county_id 
            ORDER BY month DESC";

        $c_d_query = "
            SELECT c.county,cd.* 
            FROM county_drawing_rights_details cd,counties c 
            WHERE cd.county_id = c.id AND year = $year $summary_and_ 
            GROUP BY quarter,year,month,county_id 
            ORDER BY quarter, month DESC";
        // echo "<pre>";print_r($c_d_query);exit;
        $county_drawing_data_details = $this->db->query($c_d_query)->result_array();
        
        // echo "<pre>";print_r($county_drawing_data_details);exit;
        $qtt_data_arr = array();//array that holds the distributed values to ensure duplicates are not added again.
        $quarter_ = 0;
        foreach ($county_drawing_data_details as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $q_ = $value['quarter'];
            $y_ = $value['year'];
            $m_ = $value['month'];


            $qr = $value['quarter'];
            $c_id = $value['county_id'];

            // $c_drawing_details[$c_id]['screening_distributed'] += $value['screening_distributed'];
            // $c_drawing_details[$c_id]['confirmatory_distributed'] += $value['confirmatory_distributed'];
            $screening_total_raw = $value['screening_total'];
            $confirmatory_total_raw = $value['confirmatory_total'];

            $screening_distributed = $value['screening_distributed'];
            $confirmatory_distributed = $value['confirmatory_distributed'];

            /*DUPLICATION CHECKS*/
                if (!in_array($screening_total_raw, $qtt_data_arr[$c_id]['scr']['total'])) {
                    $county_scr_total_drawing_rights += $screening_total_raw;
                }else{
                    // echo "<pre>";print_r($qtt_data_arr[$c_id]);
                    // echo "<pre>";print_r($screening_total);exit;
                }

                if (!in_array($confirmatory_total_raw, $qtt_data_arr[$c_id]['conf']['total'])) {
                    $county_conf_total_drawing_rights += $confirmatory_total_raw;
                }else{
                    // echo "<pre>";print_r($confirmatory_distributed);exit;
                }

            if (!in_array($screening_distributed, $qtt_data_arr[$c_id]['scr']['distributed'])) {
                $drawing_totals[$c_id]['screening_distributed'] += $value['screening_distributed']; 
                $qtt_data_arr[$c_id]['scr']['distributed_added'][] = $screening_distributed;
            }else{
                // echo "<pre>";print_r($screening_distributed);exit;
            }

            if (!in_array($confirmatory_distributed, $qtt_data_arr[$c_id]['conf']['distributed'])) {
                $drawing_totals[$c_id]['confirmatory_distributed'] += $value['confirmatory_distributed'];
                $qtt_data_arr[$c_id]['conf']['distributed_added'][] = $confirmatory_distributed ;
            }else{
                // echo "<pre>";print_r($confirmatory_distributed);exit;
            }
            /*DUPLICATION CHECKS*/

            $qtt_data_arr[$c_id]['scr']['distributed'][] = $screening_distributed;
            $qtt_data_arr[$c_id]['conf']['distributed'][] = $confirmatory_distributed;

            // $qtt_data_arr[$c_id]['scr']['total'][] = $screening_total_raw;
            // $qtt_data_arr[$c_id]['conf']['total'][] = $confirmatory_total_raw;


            $drawing_totals[$c_id]['year'] = $value['year']; 
            $drawing_totals[$c_id]['county'] = $value['county']; 
            $drawing_totals[$c_id]['county_id'] = $value['county_id']; 
            $c_drawing_details[$c_id][$qr]['screening_total'] = $value['screening_total'];
            $c_drawing_details[$c_id][$qr]['screening_used'] = $value['screening_used'];

            if ($qr > $quarter_) {
                $drawing_totals[$c_id]['screening_total_annual'] = $drawing_totals[$c_id]['confirmatory_total_annual'] = 0;
            }


            if (!in_array($screening_total_raw, $qtt_data_arr[$c_id]['scr']['total_added'])) {
                $drawing_totals[$c_id]['screening_total_annual'] += $value['screening_total'];
                $qtt_data_arr[$c_id]['scr']['total_added'][] = $screening_total_raw;
            }else{
                // echo "<pre>";print_r($screening_distributed);exit;
            }

            $c_drawing_details[$c_id][$qr]['confirmatory_total'] = $value['confirmatory_total'];
            $c_drawing_details[$c_id][$qr]['confirmatory_used'] = $value['confirmatory_used'];


            if (!in_array($confirmatory_total_raw, $qtt_data_arr[$c_id]['scr']['total_added'])) {
                $drawing_totals[$c_id]['confirmatory_total_annual'] += $value['confirmatory_total'];
                $qtt_data_arr[$c_id]['conf']['total_added'][] = $confirmatory_total_raw;
            }else{
                // echo "<pre>";print_r($screening_distributed);exit;
            }

            if (!in_array($screening_distributed, $qtt_data_arr[$c_id][$qr]['scr']['distributed_added'])) {
                $c_drawing_details[$c_id][$qr]['screening_distributed'] += $value['screening_distributed'];
                $qtt_data_arr[$c_id][$qr]['scr']['distributed_added'][] = $screening_distributed;
            }else{
                // echo "<pre>";print_r($screening_distributed);exit;
            }

            $c_drawing_details[$c_id][$qr]['confirmatory_total'] = $value['confirmatory_total'];
            $c_drawing_details[$c_id][$qr]['confirmatory_used'] = $value['confirmatory_used'];


            if (!in_array($confirmatory_distributed, $qtt_data_arr[$c_id][$qr]['scr']['distributed_added'])) {
                $c_drawing_details[$c_id][$qr]['confirmatory_distributed'] += $value['confirmatory_distributed'];
                $qtt_data_arr[$c_id][$qr]['conf']['distributed_added'][] = $confirmatory_distributed;
            }else{
                // echo "<pre>";print_r($screening_distributed);exit;
            }
            

            $drawing_totals[$c_id]['screening_quarter_total'] = $value['screening_total']; 
            $drawing_totals[$c_id]['confirmatory_quarter_total'] = $value['confirmatory_total']; 



            // $drawing_totals[$c_id]['screening_distributed'] += $value['screening_distributed']; 
            // $drawing_totals[$c_id]['confirmatory_distributed'] += $value['confirmatory_distributed']; 

            

            $drawing_totals[$c_id]['quarter_totals'][$qr]['screening_total_annual'] += $value['screening_total'];
            $drawing_totals[$c_id]['quarter_totals'][$qr]['confirmatory_total_annual'] += $value['confirmatory_total'];

            $drawing_totals[$c_id]['quarter'] = $value['quarter'];
            $drawing_totals[$c_id]['year'] = $value['year'];

            $quarter_ = $qr;
        }

        // echo "<pre>";print_r($qtt_data_arr);
        // echo "<pre>";print_r($drawing_totals);
        // echo "<pre>";print_r($c_drawing_details[4]);exit;
        $data['county_drawing_totals'] = $drawing_totals;
        $data['county_drawing_totals_details'] = $c_drawing_details;

        $quarter_ = 0;
        $national_drawing_totals = array();
        // echo "<pre>";print_r($drawing_totals);
        // exit;
        foreach ($drawing_totals as $key => $value) {
            $qr = $value['quarter'];

            $national_drawing_totals['screening_total_annual'] += $value['quarter_totals'][$qr]['screening_total_annual'];
            $national_drawing_totals['confirmatory_total_annual'] += $value['quarter_totals'][$qr]['confirmatory_total_annual'];

            $national_drawing_totals['screening_default_annual'] += $value['screening_default_annual'];
            $national_drawing_totals['confirmatory_default_annual'] += $value['confirmatory_default_annual'];

            // echo "<pre>";print_r($national_drawing_totals);exit;

            $national_drawing_totals['screening_total_per_quarter'] += $value['screening_total_per_quarter'];
            $national_drawing_totals['confirmatory_total_per_quarter'] += $value['confirmatory_total_per_quarter'];

            $national_drawing_totals['screening_quarter_total'] += $value['screening_quarter_total'];
            $national_drawing_totals['confirmatory_quarter_total'] += $value['confirmatory_quarter_total'];

            $national_drawing_totals['screening_distributed'] += $value['screening_distributed'];
            $national_drawing_totals['confirmatory_distributed'] += $value['confirmatory_distributed'];
        }

        // echo "<pre>";print_r($national_drawing_totals);exit;
        $data['national_drawing_totals'] = $national_drawing_totals;
        // echo "<pre>";print_r($data);exit;
        return $data;
    }

    public function update_subcounty_reporting_rates($county_id = NULL,$district_id = NULL,$month = NULL, $year = NULL)
    {
        $current_month = date('m');
        $current_year = date('Y');

        $year = (isset($year) && is_numeric($year) && $year>0)? $year:$current_year;
        $month = (isset($month) && is_numeric($month) && $month>0)? $month:$current_month;

        $month_year = $month.$year;
        // echo "<pre>";print_r($month_year);exit;
        $cache_key = "update_subcounty_reporting_rates".$county_id.'_'.$district_id.'_'.$month.$year.'_'.date('Y-m-d');
        // echo "<pre>";print_r($cache_key);exit;
        // $cache_key_ = $this->generate_memcached_key($cache_key);
        // echo "<pre>";print_r($cache_key);exit;
        $cache_check = $this->memcached_check($cache_key);
        // echo "<pre>";print_r($cache_check);exit;
        if ($cache_check['status'] == "FAILED") {
            $d_where .= (isset($county_id) && $county_id > 0) ? " WHERE county = $county_id" : NULL;
            $d_where .= (isset($district_id) && $district_id > 0) ? " WHERE id = $district_id" : NULL;
        
            $d_query = "SELECT * FROM districts $d_where";

            $d_data = $this->db->query($d_query)->result_array();
            // echo "<pre>";print_r($d_data);exit;

            foreach ($d_data as $district => $dist_data) {
                // echo "<pre>";print_r($dist_data);exit;
                $d_id = $dist_data['id'];
                $d_name = $dist_data['district'];

                $f_date = $year.'-'.$month.'-01';
                $e_date = $year.'-'.$month.'-31';

                $all_q = 'SELECT * FROM  `facilities` WHERE  `district` =' .$d_id. ' AND  `rtk_enabled` = 1 GROUP BY facility_code';
                $all_fac = $this->db->query($all_q)->result_array();
                $reported_q = "
                    SELECT DISTINCT
                        lab_commodity_orders.facility_code,
                        facilities.facility_code,
                        facilities.facility_name,
                        lab_commodity_orders.id,
                        lab_commodity_orders.district_id,
                        lab_commodity_orders.beg_date,
                        lab_commodity_orders.end_date,
                        lab_commodity_orders.order_date,
                        lab_commodity_orders.report_for
                    FROM
                        lab_commodity_orders,
                        districts,
                        counties,
                        facilities
                    WHERE
                        districts.id = lab_commodity_orders.district_id
                            AND districts.county = counties.id
                            AND facilities.facility_code = lab_commodity_orders.facility_code
                            AND districts.id = $d_id
                            AND lab_commodity_orders.beg_date BETWEEN '$f_date' AND '$f_date'
                    GROUP BY lab_commodity_orders.facility_code
                ";
                $reported_fac = $this->db->query($reported_q)->result_array();

                $all_fac_count = count($all_fac);
                $reported_fac_count = count($reported_fac);

                // echo $all_fac_count." ".$reported_fac_count;exit;
                $perc = round(($reported_fac_count/$all_fac_count)*100);
                if ($perc > 100) {
                    $perc = 100;
                }

                if ($perc < 1) {
                    $perc = 0;
                }
                // echo "<pre>";print_r($perc);exit;

                $final_data[$d_id]['district_id'] = $d_id;
                $final_data[$d_id]['district_name'] = $d_name;
                $final_data[$d_id]['all_facilities'] = $all_fac_count;
                $final_data[$d_id]['reported_facilities'] = $reported_fac_count;
                $final_data[$d_id]['month_year'] = $month_year;
                $final_data[$d_id]['perc'] = $perc;

                $update = $this->update_district_reporting_rates($d_id,$all_fac_count,$reported_fac_count,$month_year,$perc);

                // echo "<pre>";print_r($reported_q);exit;
            }

            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($final_data);

            // echo "<pre>";print_r($cache_data);exit;
            $cache_save = $this->memcached_save($cache_data);
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>CHECK:";print_r($cache_check);exit;
            $final_data = json_decode($cache_check['data'], true);
        }

        return $final_data;
    }

    public function get_dhis_fcdrr_data($county_id = NULL,$district_id = NULL,$facility_code = NULL,$dhis_order_id = NULL, $mfl_month_year = NULL, $quarter = NULL,$year = NULL,$month = NULL,$level = NULL,$operation = NULL){
        // echo "<pre>";print_r($operation);exit;
        $param_string = $county_id.$district_id.$facility_code.$commodity_id.$quarter.$year.$month;

        $level_count = NULL;
        if ($district_id > 0) {
            $level = 'subcounty';
            $level_id = $district_id;
            $level_count = " ,count(*) AS order_count ";
            $level_group_by = " GROUP BY df.facility_code ";
        }

        if ($facility_code > 0) {
            $level = 'facility';
            $level_id = $facility_code;
            $level_group_by = " GROUP BY df.facility_code,month,year ";
        }

        if ($dhis_order_id > 0) {
            $level = 'dhis_order';
            $level_id = $dhis_order_id;
            $level_group_by = " GROUP BY df.facility_code,df.commodity_id,month,year ";
        }

        $data['level'] = $level;
        switch ($level) {
            case 'subcounty':
                $curmonth = date('m');
                $curyear = date('Y');

                if ($operation == 'pull') {
                    $check = "
                        SELECT 
                            *
                        FROM
                            dhis_fcdrr_pull_logs
                        WHERE
                            level = '$level' AND level_id = '$level_id'
                        AND MONTH(date_retrieved) >= $curmonth
                        AND YEAR(date_retrieved) >= $curyear;
                    ";
                }else{
                    $check = "SELECT * FROM dhis_fcdrr_pull_logs WHERE level = '$level' AND level_id = '$level_id'";
                }

                
                // echo "<pre>";print_r($check);exit;
                $check_result = $this->db->query($check);
                // echo "<pre>";print_r($check_result);exit;
                if ($check_result->num_rows() > 0) {
                    $check_data = $check_result->result_array();
                    $data['status'] = 'present';
                    $data['check_result'] = $check_data;

                    $lvl_where = '';
                    $lvl_where .= ($district_id > 0)? "AND df.district_id = $district_id":NULL;
                    $lvl_where .= ($facility_code > 0)? "AND df.facility_code = $facility_code":NULL;

                    $q = "SELECT c.county,d.district,f.facility_name,df.* $level_count FROM dhis_fcdrr_data df,facilities f,districts d,counties c WHERE df.facility_code = f.facility_code AND f.district = d.id AND d.county = c.id $lvl_where $level_group_by ORDER BY month,year DESC";
                    // echo "<pre>";print_r($q);exit;
                    $dhis_fcdrr = $this->db->query($q)->result_array();
                    $data['result'] = $dhis_fcdrr;

                }else{
                    $data['status'] = 'absent';
                    $get_data = $this->get_dhis_fcdrr_data_(NULL, $district_id);
                    $data['result'] = $get_data;
                }
                break;

            case 'facility':
                /*TO ACCESS THIS LEVEL, SUBCOUNTY'S DATA HAS TO HAVE BEEN PULLED. THUS THE CHECK SKIP*/
                    $data['status'] = 'present';
                    $data['message'] = 'Assumed always to be present as to access this level subcounty data has to have been pulled.';

                    $lvl_where = '';
                    $lvl_where .= ($facility_code > 0)? "AND df.facility_code = $facility_code":NULL;

                    $q = "SELECT c.county,d.district,f.facility_name,df.* $level_count FROM dhis_fcdrr_data df,facilities f,districts d,counties c WHERE df.facility_code = f.facility_code AND f.district = d.id AND d.county = c.id $lvl_where $level_group_by ORDER BY month,year DESC";
                    // echo "<pre>";print_r($q);exit;
                    $dhis_fcdrr = $this->db->query($q)->result_array();
                    $data['result'] = $dhis_fcdrr;

            break;

            case 'dhis_order':
                /*TO ACCESS THIS LEVEL, SUBCOUNTY'S DATA HAS TO HAVE BEEN PULLED. THUS THE CHECK SKIP*/
                    $data['status'] = 'present';
                    $data['message'] = 'Assumed always to be present as to access this level subcounty data has to have been pulled.';

                    $lvl_where = '';
                    $lvl_where .= ($dhis_order_id > 0)? "AND df.id = $dhis_order_id":NULL;

                    $q = "SELECT c.county,d.district,f.facility_name,df.* $level_count FROM dhis_fcdrr_data df,facilities f,districts d,counties c WHERE df.facility_code = f.facility_code AND f.district = d.id AND d.county = c.id $lvl_where $level_group_by ORDER BY month,year DESC";
                    // echo "<pre>";print_r($q);exit;
                    $dhis_fcdrr = $this->db->query($q)->result_array();
                    $data['result'] = $dhis_fcdrr;

            break;

            case 'facility_by_commodity':
                $data['status'] = 'present';
                $data['message'] = 'Assumed always to be present as to access this level subcounty data has to have been pulled.';

                $lvl_where = '';
                $lvl_where .= ($facility_code > 0)? "AND df.facility_code = $facility_code":NULL;

                $q = "SELECT c.county,d.district,f.facility_name,df.* $level_count FROM dhis_fcdrr_data df,facilities f,districts d,counties c WHERE df.facility_code = f.facility_code AND f.district = d.id AND d.county = c.id $lvl_where $level_group_by ORDER BY month,year DESC";
                // echo "<pre>";print_r($q);exit;
                $dhis_fcdrr = $this->db->query($q)->result_array();

                foreach ($dhis_fcdrr as $key => $value) {
                    // echo "<pre>";print_r($value);exit;
                    $mfl = $value['facility_code'];
                    $comm_id = $value['commodity_id'];
                    $m = $value['month'];
                    $y = $value['year'];
                    $order_date = $y.'-'.$m.'-01';
                    $yrm = date('Ym',strtotime($order_date));
                    // echo "<pre>";print_r($yr_m);exit;
                    // $ym = $y.$m;
                    $m_ym = $mfl.'_'.$yrm;

                    // echo "<pre>";print_r($m_ym);exit;
                    $dhis_data[$comm_id][$m_ym]['facility_code'] = $value['facility_code'];
                    $dhis_data[$comm_id][$m_ym]['facility_name'] = $value['facility_name'];
                    $dhis_data[$comm_id][$m_ym]['county'] = $value['county'];
                    $dhis_data[$comm_id][$m_ym]['county_id'] = $value['county_id'];
                    $dhis_data[$comm_id][$m_ym]['district'] = $value['district'];
                    $dhis_data[$comm_id][$m_ym]['district_id'] = $value['district_id'];
                    $dhis_data[$comm_id][$m_ym]['commodity_id'] = $value['commodity_id'];
                    $dhis_data[$comm_id][$m_ym]['facility_name'] = $value['facility_name'];
                    $dhis_data[$comm_id][$m_ym]['rtk_check'] = (isset($value['order_id']) && $value['order_id'] > 0)?1:0;

                    $dhis_data[$comm_id][$m_ym]['order_id'] = $value['order_id'];
                    $dhis_data[$comm_id][$m_ym]['order_date'] = $order_date;
                    $dhis_data[$comm_id][$m_ym]['month'] = $value['month'];
                    $dhis_data[$comm_id][$m_ym]['year'] = $value['year'];
                    $dhis_data[$comm_id][$m_ym]['beginning_bal'] = $value['beginning_bal'];
                    $dhis_data[$comm_id][$m_ym]['q_received'] = $value['q_received'];
                    $dhis_data[$comm_id][$m_ym]['positive_adj'] = $value['positive_adj'];
                    $dhis_data[$comm_id][$m_ym]['negative_adj'] = $value['negative_adj'];
                    $dhis_data[$comm_id][$m_ym]['losses'] = $value['losses'];
                    $dhis_data[$comm_id][$m_ym]['q_used'] = $value['q_used'];
                    $dhis_data[$comm_id][$m_ym]['no_of_tests_done'] = $value['no_of_tests_done'];
                    $dhis_data[$comm_id][$m_ym]['closing_stock'] = $value['closing_stock'];
                    $dhis_data[$comm_id][$m_ym]['q_expiring'] = $value['q_expiring'];
                    $dhis_data[$comm_id][$m_ym]['q_requested'] = $value['q_requested'];
                        // array_push($data_, $data);
                }
                // echo "<pre>";print_r($dhis_data);exit;
                $data['result'] = $dhis_data;

            break;
            default:
                # code...
                break;
        }
        
        // echo "<pre>";print_r($data);exit;
        return $data;
    }

    public function get_dhis_fcdrr_data_($county_id = NULL,$district_id = NULL,$facility_code = NULL,$commodity_id = NULL,$quarter = NULL,$year = NULL,$month = NULL)
    {
        // echo "<pre>";print_r($district_id);exit;
        $param_string = $county_id.$district_id.$facility_code.$commodity_id.$quarter.$year.$month;
        $dhis_data = array();
        $dimensions['dx']['screening'] = 'CzrZlb2oGMv';
        $dimensions['dx']['confirmatory'] = 'rTFvDwdftfT';
        $dimensions['pe'][] = 'LAST_12_MONTHS';

        $category_opt_q = "SELECT * FROM dhis_commodity_category_combos;";
        $category_options = $this->db->query($category_opt_q)->result_array(); 
        // echo "<pre>";print_r($category_options);exit;

        foreach ($category_options as $key => $value) {
            $dimensions['co'][] = $value['category_option_dhis_code'];
        }

        // echo "<pre>";print_r($dimensions);exit;
        // $org_units_q = "SELECT * FROM dhis_facilities;";
        $o_u_where = "";
        $o_u_where .= ($district_id > 0)? " AND d.id = $district_id":NULL;
        $o_u_where .= ($county_id > 0)? " AND c.id = $county_id":NULL;

        $org_units_q = "
            SELECT 
                df.*,
                f.district AS district_id,
                d.district,
                d.county AS county_id,
                c.county
            FROM
                dhis_facilities df,
                districts d,
                counties c,
                facilities f
            WHERE
                df.facility_code = f.facility_code
                    AND f.district = d.id
                    AND d.county = c.id $o_u_where;
        ";
        // echo "<pre>";print_r($org_units_q);exit;
        $all_org_units = $this->db->query($org_units_q)->result_array(); 
        // echo "<pre>";print_r($all_org_units);exit;

        $count = 0;
        foreach ($all_org_units as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $org_units[] = $value['dhis_facility_code'];
            
            $count++;
        }

        // echo "<pre>";print_r($dimensions);exit;
        $dimensions_json = json_encode($dimensions);
        // echo "<pre>";print_r($dimensions_json);exit;
        // $org_units = array_chunk($org_units, 100);
        $org_units = array_chunk($org_units, 200);
        // echo "<pre>";print_r($org_units);exit;
        $cache_key = "pull_dhis_fcdrr_data_final_data".$dimensions_json.$param_string.date('Y-m-d');
        $cache_check = $this->memcached_check($cache_key);
        // echo "<pre>";print_r($cache_check);exit;
        if ($cache_check['status'] == "FAILED") {
            /*INNER CACHE CHECK*/
            $cache_key_ = "pull_dhis_fcdrr_data_".$dimensions_json.$param_string.date('Y-m-d');
            // echo "<pre>";print_r($cache_key_);exit;
            $cache_check_ = $this->memcached_check($cache_key_);
            // echo "<pre>";print_r($cache_check);exit;
            if ($cache_check_['status'] == "FAILED") {
                foreach ($org_units as $key => $value) {
                    $dimensions['ou'] = $value;
                    // echo "<pre>";print_r($dimensions);exit;
                    $dhis_data_ = $this->pull_dhis_fcdrr_data_($dimensions);
                    // echo "<pre>";print_r($dhis_data_);exit;
                    array_push($dhis_data, $dhis_data_);
                    // echo "<pre>";print_r($dhis_data);exit;
                }
                // echo "<pre>";print_r($dhis_data);exit;

                $cache_data_['key'] = md5($cache_key_);
                $cache_data_['data'] = json_encode($dhis_data);

                $cache_save_ = $this->memcached_save($cache_data_);
                // echo "CACHE SAVED.";exit;
            }else{
                // echo "<pre>";print_r($cache_check_);exit;
                $dhis_data = json_decode($cache_check_['data'],true);
            }
            /*END OF INNER CACHE CHECK*/
            // echo "<pre>";print_r($dhis_data);exit;
            $final_array = $this->format_dhis_fcdrr_data($dhis_data);

            // echo "<pre>";print_r($final_array);exit;
            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($final_array);

            $cache_save = $this->memcached_save($cache_data);
            // echo "<pre>Save: ";print_r($cache_save);exit;
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $final_array = json_decode($cache_check['data'],TRUE);
        }

        // echo "<pre>";print_r($final_array);exit;
        $save_data = $this->save_dhis_fcdrr_data($final_array,$county_id,$district_id,$facility_code);
        // echo "<pre>";print_r($save_data);exit;
        // echo "<pre>";print_r($district_id);exit;
        // echo "<pre>";print_r($final_array);exit;
        $data['save_data'] = $save_data;
        $data['final_array'] = $final_array;
        return $data;
    }

    public function pull_dhis_fcdrr_data_($dimensions = NULL)
    {
        // echo "<pre>";print_r($dimensions);exit;
        $ou = implode(";", $dimensions['ou']);
        $pe = implode(";", $dimensions['pe']);
        $dx = implode(";", $dimensions['dx']);
        $co = implode(";", $dimensions['co']);
        // echo "<pre>";print_r($pe);exit;
        $data = array();
        $data = array(
            'dx'=>$dx,
            'ou'=>$ou,
            'pe'=>$pe,
            'co'=>$co,
        );

        // echo "<pre>";print_r($data);exit;
        $url_data = '';

        foreach ($data as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $data_ = array(
            'dimension'=>$key.":".$value.';&',
            );
            // $d = 'dimension:'.$key.":".$value.';&';

            $url_data .= http_build_query($data_);
            // $url_data .= $d;
        }
        // echo "<pre>";print_r($url_data);exit;
        $api_url = 'https://hiskenya.org/api/analytics?';
        // $url_data = urldecode($url_data);
        // echo "<pre>";print_r($url_data);exit;
        $api_url = $api_url.$url_data;
        $api_url = urldecode($api_url);
        // echo "<pre>";print_r($api_url);exit;
        $curl = curl_init();
        $username = 'tngugi';
        $password = 'Ngugi1234';

        $headr = array();
        // $headr[] = 'apitoken: $2a$06$BciA87SeFAPc4OT1HNLIYO5LxxWBq/2GlMVMT/K8AviAX1OUhrWsa';

        curl_setopt_array($curl, array(
          CURLOPT_URL => $api_url,
          CURLOPT_RETURNTRANSFER => TRUE,
          // CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 100,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_0,
          CURLOPT_USERPWD, "$username" .":"."$password",
          // CURLOPT_USERPWD, "$username:$password",
          CURLOPT_VERBOSE => 1,
          // CURLOPT_HEADER => true,
          // CURLINFO_HEADER_OUT => true,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => $headr,
          CURLOPT_TIMEOUT => 0
          // CURLOPT_FOLLOWLOCATION => TRUE
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $information = curl_getinfo($curl);

        // echo "<pre>";print_r($information);
        // echo "<pre>";print_r($response);exit;
        curl_close($curl);
        // echo "<pre>";print_r($response);exit;    
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            // echo "<pre>";print_r($response);exit;
            if ($information['http_code'] == '302') {
                echo "<pre>";print_r("<a href=".$api_url." target='blank_'>".$api_url."</a>");echo "</pre>";
                // echo "<pre>";print_r($api_url);echo "</pre>";
                echo "<pre>";print_r($information);exit;
            }

            // echo "<pre>";print_r($dhis_data);exit;
            $dhis_data = json_decode($response, TRUE);
            echo "<pre>";print_r($dhis_data);exit;
        }


        return $dhis_data;
    }

    public function format_dhis_fcdrr_data($dhis_data = NULL)
    {
        // echo "<pre>";print_r($dhis_data);exit;
        $metadata['headers'] = $dhis_data[0]['headers'];
        $metadata['items'] = $dhis_data[0]['metaData']['items'];
        $metadata['dx'] = $dhis_data[0]['metaData']['dimensions']['dx'];
        $metadata['pe'] = $dhis_data[0]['metaData']['dimensions']['pe'];
        $metadata['c_o'] = $dhis_data[0]['metaData']['dimensions']['co'];

        $cat_opt_arr = $org_unit_arr = $data_elem_arr = array();

        $category_opt_q = "SELECT * FROM dhis_commodity_category_combos;";
        $category_options = $this->db->query($category_opt_q)->result_array(); 
        // echo "<pre>";print_r($category_options);exit;

        foreach ($category_options as $key => $value) {
            $code = $value['category_option_dhis_code'];
            $cat_opt_arr[$code]['dhis_code'] = $code;
            $cat_opt_arr[$code]['category_option'] = $value['category_option'];
            $cat_opt_arr[$code]['rtk_column'] = $value['rtk_column'];
        }

        // echo "<pre>";print_r($cat_opt_arr);exit;
        $org_units_q = "
            SELECT 
                df.*,
                c.id AS county_id,
                c.county,
                d.id AS district_id,
                d.district
            FROM
                dhis_facilities df,
                facilities f,
                districts d,
                counties c
            WHERE
                f.facility_code = df.facility_code
                    AND f.district = d.id
                    AND d.county = c.id;";
        $all_org_units = $this->db->query($org_units_q)->result_array(); 
        // echo "<pre>";print_r($all_org_units);exit;

        foreach ($all_org_units as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $code = $value['dhis_facility_code'];
            $org_unit_arr[$code]['dhis_code'] = $code;
            $org_unit_arr[$code]['county_id'] = $value['county_id'];
            $org_unit_arr[$code]['county'] = $value['county'];
            $org_unit_arr[$code]['district_id'] = $value['district_id'];
            $org_unit_arr[$code]['district'] = $value['district'];
            $org_unit_arr[$code]['facility_code'] = $value['facility_code'];
            $org_unit_arr[$code]['facility_name'] = $value['facility_name'];
        }

        $data_elem_q = "SELECT * FROM dhis_data_elements;";
        $all_data_elem = $this->db->query($data_elem_q)->result_array(); 
        // echo "<pre>";print_r($all_data_elem);exit;

        foreach ($all_data_elem as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $code = $value['dhis_code'];
            $data_elem_arr[$code]['dhis_code'] = $code;
            $data_elem_arr[$code]['commodity_id'] = $value['commodity_id'];
        }

        // echo "<pre>";print_r($data_elem_arr);exit;

        $row_data = array();

        // echo "<pre>";print_r($dhis_data);exit;
        foreach ($dhis_data as $keys => $values) {
            // echo "<pre>";print_r($value);exit;
            $rows = $values['rows'];
            // echo "<pre>";print_r($rows);exit;
            foreach ($rows as $key => $value) {
                // echo "<pre>";print_r($value);exit;
                $comm_dhis_code = $value[0];
                $cat_opt_dhis_code = $value[1];
                $org_unit_dhis_code = $value[2];
                $date = $value[3];
                $val = $value[4];

                $month = substr($date, -2);
                $year = substr($date, 0, 4);

                // echo "<pre>";print_r($year);exit;
                // $o_d = $org_unit_dhis_code.'_'.$date;
                $rtk_cat = $cat_opt_arr[$cat_opt_dhis_code]['rtk_column'];
                $mfl = $org_unit_arr[$org_unit_dhis_code]['facility_code'];
                $comm_id = $data_elem_arr[$comm_dhis_code]['commodity_id'];
                // echo "<pre>";print_r($comm_id);exit;
                $o_d = $mfl.'_'.$date;
                // echo "<pre>";print_r($cat);exit;

                $row_data[$comm_id][$o_d]['county_id'] = $org_unit_arr[$org_unit_dhis_code]['county_id'];
                $row_data[$comm_id][$o_d]['county'] = $org_unit_arr[$org_unit_dhis_code]['county'];
                $row_data[$comm_id][$o_d]['district_id'] = $org_unit_arr[$org_unit_dhis_code]['district_id'];
                $row_data[$comm_id][$o_d]['district'] = $org_unit_arr[$org_unit_dhis_code]['district'];
                $row_data[$comm_id][$o_d]['facility_name'] = $org_unit_arr[$org_unit_dhis_code]['facility_name'];
                $row_data[$comm_id][$o_d]['facility_code'] = $mfl;
                $row_data[$comm_id][$o_d]['dhis_code'] = $org_unit_dhis_code;
                $row_data[$comm_id][$o_d][$rtk_cat] = $val;
                $row_data[$comm_id][$o_d]['dhis_category_code'] = $cat_opt_dhis_code;
                $row_data[$comm_id][$o_d]['month'] = $month;
                $row_data[$comm_id][$o_d]['year'] = $year;
            }

        }

        // echo "<pre>";print_r($row_data);exit;

        $cache_key = "order_data".'NULL'.'NULL'.'NULL'.'NULL'.'NULL'."facility".date('Y-m-d');
        // echo "<pre>";print_r($cache_key);exit;
        // echo "<pre>";print_r(md5($cache_key));exit;
        $cache_check = $this->memcached_check($cache_key);
        // echo "<pre>";print_r($cache_check);exit;
        if ($cache_check['status'] == "FAILED") {
            $order_data = $this->get_order_data(NULL,NULL,NULL,NULL,NULL, "facility");
            // echo "<pre>summary_data_s: ";print_r($summary_data_s);exit;

            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($order_data);

            $cache_save = $this->memcached_save($cache_data);
            // echo "<pre>Save: ";print_r($cache_save);exit;
            // echo "CACHE SAVED.";exit;
        }else{
            // echo "<pre>";print_r($cache_check);exit;
            $order_data = json_decode($cache_check['data'],TRUE);
        }
        
        // echo "<pre>";print_r($order_data);exit;

        foreach ($order_data as $key => $value) {
            $mfl = $value['facility_code'];
            $comm_id = $value['commodity_id'];
            $yr_m = date('Ym',strtotime($value['order_date']));
            $m_ym = $mfl.'_'.$yr_m;

            // echo "<pre>";print_r($m_yr);exit;
            $rtk_data[$comm_id][$m_ym]['facility_code'] = $value['facility_code'];
            $rtk_data[$comm_id][$m_ym]['facility_name'] = $value['facility_name'];
            $rtk_data[$comm_id][$m_ym]['county'] = $value['county'];
            $rtk_data[$comm_id][$m_ym]['county_id'] = $value['county_id'];
            $rtk_data[$comm_id][$m_ym]['district'] = $value['district'];
            $rtk_data[$comm_id][$m_ym]['district_id'] = $value['district_id'];
            $rtk_data[$comm_id][$m_ym]['commodity_id'] = $value['commodity_id'];
            $rtk_data[$comm_id][$m_ym]['facility_name'] = $value['facility_name'];

            $rtk_data[$comm_id][$m_ym]['order_id'] = $value['order_id'];
            $rtk_data[$comm_id][$m_ym]['order_date'] = $value['order_date'];
            $rtk_data[$comm_id][$m_ym]['beginning_bal'] = $value['beginning_bal'];
            $rtk_data[$comm_id][$m_ym]['q_received'] = $value['q_received'];
            $rtk_data[$comm_id][$m_ym]['q_used'] = $value['q_used'];
            $rtk_data[$comm_id][$m_ym]['no_of_tests_done'] = $value['no_of_tests_done'];
            $rtk_data[$comm_id][$m_ym]['closing_stock'] = $value['closing_stock'];
            $rtk_data[$comm_id][$m_ym]['q_expiring'] = $value['q_expiring'];
            $rtk_data[$comm_id][$m_ym]['q_requested'] = $value['q_requested'];
                // array_push($data_, $data);
        }
        
        // echo "<pre>";print_r($row_data);exit;

        $final_array = array();

        foreach ($row_data as $commodity_id => $comm_value) {
            // echo "<pre>";print_r($comm_value);exit;
            foreach ($comm_value as $key => $value) {
                $comm_id = $commodity_id;
                $mfl = $value['facility_code'];

                $yr_m = $value['year'].$value['month'];
                $m_ym = $mfl.'_'.$yr_m;


                $final_array[$comm_id][$m_ym]['county_id'] = $value['county_id'];
                $final_array[$comm_id][$m_ym]['county'] = $value['county'];
                $final_array[$comm_id][$m_ym]['district_id'] = $value['district_id'];
                $final_array[$comm_id][$m_ym]['district'] = $value['district'];
                $final_array[$comm_id][$m_ym]['dhis_code'] = $value['dhis_code'];
                $final_array[$comm_id][$m_ym]['facility_code'] = $value['facility_code'];
                $final_array[$comm_id][$m_ym]['facility_name'] = $value['facility_name'];
                $final_array[$comm_id][$m_ym]['closing_stock'] = $value['closing_stock'];
                $final_array[$comm_id][$m_ym]['positive_adj'] = $value['positive_adj'];
                $final_array[$comm_id][$m_ym]['negative_adj'] = $value['negative_adj'];
                $final_array[$comm_id][$m_ym]['losses'] = $value['losses'];
                $final_array[$comm_id][$m_ym]['q_expiring'] = $value['q_expiring'];
                $final_array[$comm_id][$m_ym]['days_out_of_stock'] = $value['days_out_of_stock'];
                $final_array[$comm_id][$m_ym]['month'] = $value['month'];
                $final_array[$comm_id][$m_ym]['year'] = $value['year'];
                $final_array[$comm_id][$m_ym]['q_requested'] = $value['q_requested'];
                $final_array[$comm_id][$m_ym]['q_received'] = $value['q_received'];
                $final_array[$comm_id][$m_ym]['beginning_bal'] = $value['beginning_bal'];
                $final_array[$comm_id][$m_ym]['q_used'] = $value['q_used'];
                $final_array[$comm_id][$m_ym]['no_of_tests_done'] = $value['no_of_tests_done'];

                $final_array[$comm_id][$m_ym]['dhis_data'] = $value;
                $final_array[$comm_id][$m_ym]['rtk_data'] = $rtk_data[$commodity_id][$key];

                // echo "<pre>";print_r($rtk_data[$commodity_id][$key]);exit;
                $rtk_check = (isset($rtk_data[$commodity_id][$key]))?1:0;
                // echo "<pre>";print_r($rtk_check);exit;
                if ($rtk_check > 0) {
                    $final_array[$comm_id][$m_ym]['rtk_check'] = $rtk_check;
                    $final_array[$comm_id][$m_ym]['present'] = 'SUCCESS';
                }else{
                    $final_array[$comm_id][$m_ym]['rtk_check'] = $rtk_check;
                    $final_array[$comm_id][$m_ym]['present'] = 'FAIL';
                }
            }
        }

        // echo "<pre>";print_r($final_array);exit;
        return $final_array;    
    }

    public function save_dhis_fcdrr_data($save_data = NULL,$county_id = NULL,$district_id = NULL,$facility_code = NULL,$commodity_id = NULL,$quarter = NULL,$year = NULL,$month = NULL){
        $county_id = (isset($county_id) && $county_id > 0)? $county_id:NULL;
        $district_id = (isset($district_id) && $district_id > 0)? $district_id:NULL;
        $facility_code = (isset($facility_code) && $facility_code > 0)? $facility_code:NULL;
        $quarter = (isset($quarter) && $quarter > 0)? $quarter:NULL;
        // echo "<pre>";print_r($district_id);exit;
        $present_dhis_fcdrr = $present_data = $fcdrr = $fcdrr_ = array();

        $check_q = "SELECT mfl_month_year FROM dhis_fcdrr_data;";
        $check_data = $this->db->query($check_q);
        
        // echo "<pre>";print_r($check_data);exit;
        if ($check_data->num_rows() > 0) {
            $check_data = $check_data->result_array();
            foreach ($check_data as $key => $value) {
                // echo "<pre>";print_r($value);exit;
                $present_dhis_fcdrr[] = $value['mfl_month_year'];
            }
        }

        // echo "<pre>";print_r($present_dhis_fcdrr);exit;

        // echo "<pre>";print_r($present_dhis_fcdrr);
        // echo "<pre>";print_r($save_data);exit;
        foreach ($save_data as $comm => $comm_data) {
            // echo "<pre>";print_r($comm_data);exit;
            // echo "<pre>";print_r($comm);exit;
            foreach ($comm_data as $key => $value) {
                // echo "<pre>";print_r($value);
                // echo "<pre>";print_r($key);exit;
                $mfl_month_year = $value['facility_code'].'_'.$value['month'].'_'.$value['year'];
                // echo "<pre>";print_r($mfl_month_year);exit;
                $fcdrr_['county_id'] = $value['county_id'];
                $fcdrr_['district_id'] = $value['district_id'];
                $fcdrr_['dhis_facility_code'] = $value['dhis_code'];
                $fcdrr_['commodity_id'] = $comm;
                $fcdrr_['facility_code'] = $value['facility_code'];
                $fcdrr_['closing_stock'] = $value['closing_stock'];
                $fcdrr_['month'] = $value['month'];
                $fcdrr_['year'] = $value['year'];
                $fcdrr_['month_year'] = $value['month'].'_'.$value['year'];
                $fcdrr_['q_requested'] = $value['q_requested'];
                $fcdrr_['q_received'] = $value['q_received'];
                $fcdrr_['beginning_bal'] = $value['beginning_bal'];
                $fcdrr_['q_used'] = $value['q_used'];
                $fcdrr_['losses'] = $value['losses'];
                $fcdrr_['positive_adj'] = $value['positive_adj'];
                $fcdrr_['negative_adj'] = $value['negative_adj'];
                $fcdrr_['q_expiring'] = $value['q_expiring'];
                $fcdrr_['days_out_of_stock'] = $value['days_out_of_stock'];
                $fcdrr_['q_requested'] = $value['q_requested'];
                // $fcdrr_['tests_done'] = $value['no_of_tests_done'];
                $fcdrr_['no_of_tests_done'] = $value['no_of_tests_done'];
                $fcdrr_['mfl_month_year'] = $mfl_month_year;

                if ($value['rtk_check'] > 0) {
                    $fcdrr_['order_id'] = $value['rtk_data']['order_id'];
                }else{
                    $fcdrr_['order_id'] = NULL;
                }

                $fcdrr_['date_retrieved'] = date('Y-m-d');
                // echo "<pre>";print_r($fcdrr_);exit;

                $search_key = array_search($mfl_month_year, $present_dhis_fcdrr);
                // echo "<pre>";print_r($search_key);
                if (isset($search_key) && $search_key !== FALSE) {
                    // echo "FOUND";
                  array_push($present_data, $fcdrr_);
                }else{
                    // echo "NOT FOUND";
                  array_push($fcdrr, $fcdrr_);
                }
                
            }

        }
        
        // echo "<pre>";print_r($present_data);
        // echo "<pre>";print_r($fcdrr);exit;
        // echo "<pre>";print_r($this->db->insert_batch);exit;
        $res = $this->db->insert_batch('dhis_fcdrr_data', $fcdrr);
        $q = $this->db->last_query();
        // echo "<pre>";print_r($q);exit;
        // echo "<pre>";print_r($res);exit;

        if (count($fcdrr) > 0) {
            if ($district_id > 0) {
                $log_level = 'subcounty';
                $log_id = $district_id;
            }

            $log_data['level'] = $log_level;
            $log_data['level_id'] = $log_id;
            $log_data['status'] = 1;
            $log_data['date_retrieved'] = date('Y-m-d');

            $ins = $this->db->insert('dhis_fcdrr_pull_logs',$log_data);
            $data['log_data'] = $log_data;
        }


        $data['status'] = $res;
        $data['new_data'] = $fcdrr;
        $data['present_data'] = $present_data;

        return $data;
    }

    public function pull_dhis_fcdrr_data_by_district($start_point = NULL,$limit = NULL,$county_id = NULL, $district_id = NULL,$facility_code = NULL,$commodity_id = NULL,$quarter = NULL,$year = NULL,$month = NULL)
    {
        $d_where .= (isset($district_id) && $district_id > 0) ? " AND id = $district_id" : NULL;

        $d_where .= (isset($start_point) && $start_point > 0) ? " AND id > $start_point" : NULL;

        $lim = (isset($limit) && $limit > 0) ? " LIMIT $limit" : NULL;


        $districts_q = "SELECT * FROM districts WHERE id > 0 $d_where $lim";
        // echo "<pre>";print_r($districts_q);exit;
        $districts = $this->db->query($districts_q)->result_array();

        foreach ($districts as $key => $value) {
            // echo "<pre>";print_r($value);exit;
            $d_id = $value['id'];

            $pull[] = $this->get_dhis_fcdrr_data(NULL,$d_id);
        }
            echo "<pre>";print_r($pull);exit;
    }

    public function get_dhis_data_analysis($year=NULL,$month=NULL)
    {
        $year = (isset($year) && $year>0)? $year:date('Y');
        $month = (isset($month) && $month>0)? $month:NULL;

        $cache_key = "get_dhis_data_analysis_rtk_data/".$year."/NULL".date('Y-m-d');
        // echo "<pre>";print_r($cache_key);exit;
        // echo "<pre>";print_r(md5($cache_key));exit;
        $cache_check = $this->memcached_check($cache_key);
        // echo "<pre>";print_r($cache_check);
        // exit;
        if ($cache_check['status'] == "FAILED") {

            $order_data = $this->get_order_data(NULL,NULL,NULL,NULL,NULL, "facility",$year);
            // echo "<pre>";print_r($order_data);exit;

            $balances_scr = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 4 , $beg_date, $end_date, $quarter,NULL,$month,$year);
            $balances_conf = $this->get_opening_and_closing_balances($county_id, $district_id, $facility_code, 5 , $beg_date, $end_date, $quarter,NULL,$month,$year);

            // echo "<pre>";print_r($balances_scr);
            // echo "<pre>";print_r($balances_conf);exit;
            // $closing_bal_scr = $summary_data_s['results'][0]['beginning_bal'];
            // $closing_bal_conf = $summary_data_c['results'][0]['beginning_bal'];

            $rtk_data['totals'][4]['closing_balance'] = $balances_scr['closing_balance']['closing_balance'];
            $rtk_data['totals'][5]['closing_balance'] = $balances_conf['closing_balance']['closing_balance'];

            $rtk_data['totals']['closing_balance_order_date'] = $balances_scr['closing_balance']['order_date'];

            $rtk_data['totals'][4]['beginning_balance'] = $balances_scr['beginning_balance']['beginning_balance'];
            $rtk_data['totals'][5]['beginning_balance'] = $balances_conf['beginning_balance']['beginning_balance'];
            $rtk_data['totals']['beginning_balance_order_date'] = $balances_scr['beginning_balance']['order_date'];

            // echo "<pre>";print_r($rtk_data);exit;
            foreach ($order_data as $key => $value) {
                $mfl = $value['facility_code'];
                $comm_id = $value['commodity_id'];
                $yr_m = date('Ym',strtotime($value['order_date']));
                $m = date('m',strtotime($value['order_date']));
                $y = date('Y',strtotime($value['order_date']));
                $m_ym = $mfl.'_'.$yr_m;

                // echo "<pre>";print_r($m);exit;

                // $rtk_data['totals'][$comm_id]['beginning_bal'] += $value['beginning_bal'];
                $rtk_data['totals'][$comm_id]['q_received'] += $value['q_received'];
                $rtk_data['totals'][$comm_id]['q_used'] += $value['q_used'];
                // $rtk_data['totals'][$comm_id]['closing_stock'] += $value['closing_stock'];
                $rtk_data['totals'][$comm_id]['q_requested'] += $value['q_requested'];
                $rtk_data['totals'][$comm_id]['q_expiring'] += $value['q_expiring'];

                $rtk_data[$comm_id][$m_ym]['facility_code'] = $value['facility_code'];
                $rtk_data[$comm_id][$m_ym]['facility_name'] = $value['facility_name'];
                $rtk_data[$comm_id][$m_ym]['county'] = $value['county'];
                $rtk_data[$comm_id][$m_ym]['county_id'] = $value['county_id'];
                $rtk_data[$comm_id][$m_ym]['district'] = $value['district'];
                $rtk_data[$comm_id][$m_ym]['district_id'] = $value['district_id'];
                $rtk_data[$comm_id][$m_ym]['commodity_id'] = $value['commodity_id'];
                $rtk_data[$comm_id][$m_ym]['facility_name'] = $value['facility_name'];

                $rtk_data[$comm_id][$m_ym]['order_id'] = $value['order_id'];
                $rtk_data[$comm_id][$m_ym]['order_date'] = $value['order_date'];
                $rtk_data[$comm_id][$m_ym]['beginning_bal'] = $value['beginning_bal'];
                $rtk_data[$comm_id][$m_ym]['q_received'] = $value['q_received'];
                $rtk_data[$comm_id][$m_ym]['q_used'] = $value['q_used'];
                $rtk_data[$comm_id][$m_ym]['no_of_tests_done'] = $value['no_of_tests_done'];
                $rtk_data[$comm_id][$m_ym]['closing_stock'] = $value['closing_stock'];
                $rtk_data[$comm_id][$m_ym]['q_expiring'] = $value['q_expiring'];
                $rtk_data[$comm_id][$m_ym]['q_requested'] = $value['q_requested'];
                    // array_push($data_, $data);
            }

            $cache_data['key'] = md5($cache_key);
            $cache_data['data'] = json_encode($rtk_data,true);
            // echo "<pre>";print_r($cache_data);exit;
            $cache_save = $this->memcached_save($cache_data);
        }else{
            $rtk_data = json_decode($cache_check['data'],TRUE);
        }
        
        // echo "<pre>";print_r($rtk_data);exit;
        

        // echo "<pre>";print_r($dhis_data);exit;
        $dhis_data_analysis = $this->get_dhis_fcdrr_data($county_id,$district_id,$facility_code,NULL,$commodity_id,$quarter,$year,$month,"facility_by_commodity");
        // echo "<pre>";print_r($dhis_data_analysis);exit;

        $dhis_data_result = $dhis_data_analysis['result'];

        $final_array = array();

        $final_array['totals']['rtk'] = $rtk_data['totals'];

        $dhis_balances_scr = $this->get_opening_and_closing_balances_dhis($county_id, $district_id, $facility_code, 4 , $beg_date, $end_date, $quarter,NULL,$month,$year);
        // echo "<pre>get_opening_and_closing_balances_dhis: ";print_r($dhis_balances_scr);exit;
        $dhis_balances_conf = $this->get_opening_and_closing_balances_dhis($county_id, $district_id, $facility_code, 5 , $beg_date, $end_date, $quarter,NULL,$month,$year);

        // echo "<pre>";print_r($balances_scr);exit;
        // echo "<pre>";print_r($balances_scr);exit;
        // $closing_bal_scr = $summary_data_s['results'][0]['beginning_bal'];
        // $closing_bal_conf = $summary_data_c['results'][0]['beginning_bal'];

        $final_array['totals']['dhis'][4]['closing_balance'] = $dhis_balances_scr['closing_balance']['closing_balance'];
        $final_array['totals']['dhis'][5]['closing_balance'] = $dhis_balances_conf['closing_balance']['closing_balance'];

        $final_array['totals']['closing_balance_order_date'] = $dhis_balances_scr['closing_balance']['order_date'];

        $final_array['totals']['dhis'][4]['beginning_balance'] = $dhis_balances_scr['beginning_balance']['beginning_balance'];
        $final_array['totals']['dhis'][5]['beginning_balance'] = $dhis_balances_conf['beginning_balance']['beginning_balance'];
        $final_array['totals']['beginning_balance_order_date'] = $dhis_balances_scr['beginning_balance']['order_date'];

        foreach ($dhis_data_result as $commodity_id => $comm_value) {
            // echo "<pre>";print_r($comm_value);exit;
            foreach ($comm_value as $key => $value) {
                $comm_id = $commodity_id;
                $mfl = $value['facility_code'];

                $yr_m = $value['year'].$value['month'];
                $m_ym = $mfl.'_'.$yr_m;

                // $final_array['totals']['dhis'][$comm_id]['beginning_bal'] += $value['beginning_bal'];
                $final_array['totals']['dhis'][$comm_id]['q_received'] += $value['q_received'];
                $final_array['totals']['dhis'][$comm_id]['q_used'] += $value['q_used'];
                // $final_array['totals']['dhis'][$comm_id]['closing_stock'] += $value['closing_stock'];
                $final_array['totals']['dhis'][$comm_id]['q_requested'] += $value['q_requested'];
                $final_array['totals']['dhis'][$comm_id]['q_expiring'] += $value['q_expiring'];

                $final_array[$comm_id][$m_ym]['county_id'] = $value['county_id'];
                $final_array[$comm_id][$m_ym]['county'] = $value['county'];
                $final_array[$comm_id][$m_ym]['district_id'] = $value['district_id'];
                $final_array[$comm_id][$m_ym]['district'] = $value['district'];
                $final_array[$comm_id][$m_ym]['facility_code'] = $value['facility_code'];
                $final_array[$comm_id][$m_ym]['facility_name'] = $value['facility_name'];
                $final_array[$comm_id][$m_ym]['closing_stock'] = $value['closing_stock'];
                $final_array[$comm_id][$m_ym]['positive_adj'] = $value['positive_adj'];
                $final_array[$comm_id][$m_ym]['negative_adj'] = $value['negative_adj'];
                $final_array[$comm_id][$m_ym]['losses'] = $value['losses'];
                $final_array[$comm_id][$m_ym]['q_expiring'] = $value['q_expiring'];
                $final_array[$comm_id][$m_ym]['days_out_of_stock'] = $value['days_out_of_stock'];
                $final_array[$comm_id][$m_ym]['month'] = $value['month'];
                $final_array[$comm_id][$m_ym]['year'] = $value['year'];
                $final_array[$comm_id][$m_ym]['q_requested'] = $value['q_requested'];
                $final_array[$comm_id][$m_ym]['q_received'] = $value['q_received'];
                $final_array[$comm_id][$m_ym]['beginning_bal'] = $value['beginning_bal'];
                $final_array[$comm_id][$m_ym]['q_used'] = $value['q_used'];
                $final_array[$comm_id][$m_ym]['no_of_tests_done'] = $value['no_of_tests_done'];

                $final_array[$comm_id][$m_ym]['dhis_data'] = $value;
                $final_array[$comm_id][$m_ym]['rtk_data'] = $rtk_data[$commodity_id][$key];

                // echo "<pre>";print_r($rtk_data[$commodity_id][$key]);exit;
                $rtk_check = (isset($rtk_data[$commodity_id][$key]))?1:0;
                // echo "<pre>";print_r($rtk_check);exit;
                if ($rtk_check > 0) {
                    $final_array[$comm_id][$m_ym]['rtk_check'] = $rtk_check;
                    $final_array[$comm_id][$m_ym]['present'] = 'SUCCESS';
                }else{
                    $final_array[$comm_id][$m_ym]['rtk_check'] = $rtk_check;
                    $final_array[$comm_id][$m_ym]['present'] = 'FAIL';
                }
            }
        }

        // echo "<pre>";print_r($final_array);exit;

        return $final_array;
    }

    public function column_highchart($graph_stuff = NULL)
    {
        // echo "<pre>";print_r($graph_stuff);exit;
        /*SAMPLE graph_stuff
            Array
                (
                    [categories] => 'Screening'
                    [series_data] => Array
                        (
                            [Issued from KEMSA] => 0
                            [Allocated] => 8966896
                            [Received] => 5100911
                        )

                    [div] => utilization_graph_scr
                )
        */
        $categories = (isset($graph_stuff['categories']) && $graph_stuff['categories']!='')? $graph_stuff['categories'] : "'County One', 'County Two'";

            $variable = "
            Highcharts.chart('".$graph_stuff["div"]."', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: 'Source: RTK'
                },
                xAxis: {
                    categories: ['".$categories."'],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Units'
                    }
                },
                 credits: {
                      enabled: false
                  },
                tooltip: {
                    headerFormat: '<span style=\"font-size:10px\">{point.key}</span><table>',
                    pointFormat: '<tr><td style=\"color:{series.color};padding:3px\">{series.name}: </td>' +
                        '<td style=\"padding:3px\"><b>{point.y:,.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },series: [
                
                ";
            foreach ($graph_stuff['series_data'] as $key => $value) {
                // echo "<pre>";print_r($value);exit;
                $val = (isset($value) && $value > 0)? $value:0;
                $variable .= "
                {
                        name: '".$key."',
                        data: [".$val."]
                    },";
            }

            $variable .= "]});";
        // echo $variable_2;exit;   
        return $variable;          
    }

    public function print_post_results()
    {
        $data = $this->input->post();

        echo "<pre>";print_r($data);exit;
    }

    public function dhis_token_request()
    {
        $server="https://play.dhis2.org/dev";
        $secret="1e6db50c-0fee-11e5-98d0-3c15c2c6caf6";

        $url = DHIS_OAUTH2_TEST_CLIENT;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://accounts.google.com/o/oauth2/token");

        curl_setopt($ch, CURLOPT_POST, TRUE);

        $code = $_REQUEST['code'];

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
        'code' => $code,
        'client_id' => $client_id,
        'client_secret' => $client_secret,
        'redirect_uri' => $redirect_uri,
        'grant_type' => 'authorization_code'
        ));

        $data = curl_exec($ch);


    }

    public function receive_auth_token()
    {
        $data = $this->input->post();

        echo "<pre>POST DATA: ";print_r($data);exit;
    }
}//END OF MY CONTROLLER 