#!/bin/sh -e

##COPY LINE BELOW TO CRONTAB FILE. 
##CRONTAB FILE OPENED USING COMMAND: crontab -e
##CHANGE DIRECTORY TO RTK RESPECTIVELY
#0 */4 * * * /usr/share/nginx/html/HCMP_RTK/cron/backup_script.sh


# location=~/`date +%Y%m%d_%H%M%S`.db
# location=/var/www/html/database_backups/rtk_backup_`date +%Y%m%d_%H%M_%S`.sql
location=/var/www/html/database_backups/rtk_backup_`date +%Y%m%d_%H%M`.sql

# mysqldump -u username -p database_to_backup > backup_name.sql
# mysqldump -u root -pakm -A -R --single-transaction rtk > $location
mysqldump -u root -pakm --routines --triggers --set-gtid-purged=OFF rtk > $location
# mysqldump -u root -pkarsan --routines --triggers rtk > $location

gzip $location