#START MOH 730A
#DATA SET:
    "id": Drk2yudsNrm5D,
    "displayName": MOH 730A Central Site/Sub county Store (CDRR)- Rev 2017

#DATA ELEMENTS
    ##METADATA
        {
            "id": T8vIpv842eX,
            "displayName": MOH 730A Abacavir (ABC) 300mg Tabs
        },
        {
            "id": v70T2bzhYBH,
            "displayName": MOH 730A Abacavir/Lamivudine (ABC/3TC) 60mg/30mg FDC Tabs
        },
        {
            "id": kMTppRz9Dy6,
            "displayName": MOH 730A Acyclovir 400mg Tabs
        },
        {
            "id": OzHa7Wm9wWb",
            "displayName": "MOH 730A Amphotericin B 50mg IV Injection"
        },
        {
            "id": "HKNzvgOEXdV",
            "displayName": "MOH 730A Atazanavir/Ritonavir (ATV/r) 300/100mg tabs"
        },
        {
            "id": "zX3RWIpPsU5",
            "displayName": "MOH 730A Cotrimoxazole 960mg Tabs (for Pack of 100 tabs)"
        },
        {
            "id": "anJTqZAje6a",
            "displayName": "MOH 730A Cotrimoxazole 960mg Tabs (for Pack of 500 tabs)"
        },
        {
            "id": "Ptvb1N5gQ54",
            "displayName": "MOH 730A Cotrimoxazole Suspension 240mg/5ml"
        },
        {
            "id": "ZcZwhTRfWkK",
            "displayName": "MOH 730A Dapsone 100mg Tabs (for Pack of 1000 tabs)"
        },
        {
            "id": "g2J7NpW2ZM8",
            "displayName": "MOH 730A Dapsone 100mg Tabs (for Pack of 100 tabs)"
        },
        {
            "id": "onTjG9ODh1k",
            "displayName": "MOH 730A Darunavir (DRV) 150mg Tabs (Pack of 240 tabs)"
        },
        {
            "id": "BDKBvFhIQJf",
            "displayName": "MOH 730A Darunavir (DRV) 600mg tabs"
        },
        {
            "id": "bZplX60YbPf",
            "displayName": "MOH 730A Darunavir (DRV) 75mg Tabs (Pack of 480 tabs)"
        },
        {
            "id": "wxA11VC6wRx",
            "displayName": "MOH 730A Darunavir (DRV) susp 100mg/ml"
        },
        {
            "id": "U3PLl3ULQt8",
            "displayName": "MOH 730A Diflucan 200mg Tabs"
        },
        {
            "id": "piEFmBh6Uql",
            "displayName": "MOH 730A Diflucan IV Infusion 2mg/ml"
        },
        {
            "id": "zbrSNo8bSST",
            "displayName": "MOH 730A Diflucan Suspension 50mg/5ml"
        },
        {
            "id": "UMqyIpzJRCJ",
            "displayName": "MOH 730A Efavirenz (EFV) 200mg Tabs"
        },
        {
            "id": "ugsQqSyyZy2",
            "displayName": "MOH 730A Efavirenz (EFV) 600mg Tabs"
        },
        {
            "id": "wFFhejJhg1B",
            "displayName": "MOH 730A Etravirine (ETV) 100mg tabs"
        },
        {
            "id": "LI1E9Xrvpwr",
            "displayName": "MOH 730A Etravirine (ETV) 200mg tabs"
        },
        {
            "id": "fWTJUSnntn6",
            "displayName": "MOH 730A Etravirine (ETV) 25mg tabs"
        },
        {
            "id": "HkZ39x8BCEK",
            "displayName": "MOH 730A Fluconazole 200mg Tabs"
        },
        {
            "id": "siZ3FslqDHV",
            "displayName": "MOH 730A Isoniazid (H) 100mg tabs"
        },
        {
            "id": "ymWk2uW4f1s",
            "displayName": "MOH 730A Isoniazid (H) 300mg Tabs (for Pack of 100 tabs)"
        },
        {
            "id": "y8vOQhrlRLj",
            "displayName": "MOH 730A Isoniazid (H) 300mg Tabs (for Pack of 672 tabs)"
        },
        {
            "id": "iKalfg97rtZ",
            "displayName": "MOH 730A Lamivudine (3TC) 150mg Tabs"
        },
        {
            "id": "r9fSbflgQgC",
            "displayName": "MOH 730A Lamivudine (3TC) liquid 10mg/ml"
        },
        {
            "id": "VJ9VJ6dxwVD",
            "displayName": "MOH 730A Lopinavir/ritonavir (LPV/r) 200/50mg Tabs"
        },
        {
            "id": "OnFFuii2sYi",
            "displayName": "MOH 730A Lopinavir/ritonavir (LPV/r) liquid 80/20mg/ml"
        },
        {
            "id": "JrfpoCr2vII",
            "displayName": "MOH 730A Nevirapine (NVP) 200mg Tabs"
        },
        {
            "id": "rNmSAnjCmWK",
            "displayName": "MOH 730A Nevirapine (NVP) Susp 10mg/ml"
        },
        {
            "id": "AUsGmP0j5eC",
            "displayName": "MOH 730A Pyridoxine 25mg Tabs"
        },
        {
            "id": "wWKgpcMkHnJ",
            "displayName": "MOH 730A Pyridoxine 50mg Tabs"
        },
        {
            "id": "pCPSqHPI4iN",
            "displayName": "MOH 730A Raltegravir (RAL) 100mg tabs"
        },
        {
            "id": "kwhxIWspmVw",
            "displayName": "MOH 730A Raltegravir (RAL) 25mg tabs"
        },
        {
            "id": "VfUQzXlpKQl",
            "displayName": "MOH 730A Raltegravir (RAL) 400mg tabs"
        },
        {
            "id": "xUayEnX8Blt",
            "displayName": "MOH 730A Ritonavir (RTV) 100mg tabs"
        },
        {
            "id": "NWL4ULZezMy",
            "displayName": "MOH 730A Ritonavir (RTV) liquid 80mg/ml"
        },
        {
            "id": "LK4JkwJDx2c",
            "displayName": "MOH 730A Tenofovir/Lamivudine/Efavirenz (TDF/3TC/EFV) FDC (300/300/600mg) Tabs"
        },
        {
            "id": "aYXQcIen2Wl",
            "displayName": "MOH 730A Tenofovir/Lamivudine (TDF/3TC) FDC (300/300mg) Tabs"
        },
        {
            "id": "u66LdVsKCvV",
            "displayName": "MOH 730A Tenofovir (TDF) 300mg Tabs"
        },
        {
            "id": "ijmmJw97V6g",
            "displayName": "MOH 730A Zidovudine/Lamivudine (AZT/3TC) FDC (300/150mg) Tabs"
        },
        {
            "id": "eb9fVoSbFIr",
            "displayName": "MOH 730A Zidovudine/Lamivudine (AZT/3TC) FDC (60/30mg) Tabs"
        },
        {
            "id": "c4V5Fav5ovZ",
            "displayName": "MOH 730A Zidovudine/Lamivudine/Nevirapine (AZT/3TC/NVP) FDC (300/150/200mg) Tabs"
        },
        {
            "id": "in4iMk8Fjj1",
            "displayName": "MOH 730A Zidovudine/Lamivudine/Nevirapine(AZT/3TC/NVP) FDC (60/30/50mg) tabs"
        },
    ##DX
        T8vIpv842eX;v70T2bzhYBH;kMTppRz9Dy6;OzHa7Wm9wWb;HKNzvgOEXdV;zX3RWIpPsU5;anJTqZAje6a;Ptvb1N5gQ54;ZcZwhTRfWkK;g2J7NpW2ZM8;onTjG9ODh1k;BDKBvFhIQJf;bZplX60YbPf;wxA11VC6wRx;U3PLl3ULQt8;piEFmBh6Uql;zbrSNo8bSST;UMqyIpzJRCJ;ugsQqSyyZy2;wFFhejJhg1B;LI1E9Xrvpwr;fWTJUSnntn6;HkZ39x8BCEK;siZ3FslqDHV;ymWk2uW4f1s;y8vOQhrlRLj;iKalfg97rtZ;r9fSbflgQgC;VJ9VJ6dxwVD;OnFFuii2sYi;JrfpoCr2vII;rNmSAnjCmWK;AUsGmP0j5eC;wWKgpcMkHnJ;pCPSqHPI4iN;kwhxIWspmVw;VfUQzXlpKQl;xUayEnX8Blt;NWL4ULZezMy;LK4JkwJDx2c;aYXQcIen2Wl;u66LdVsKCvV;ijmmJw97V6g;eb9fVoSbFIr;c4V5Fav5ovZ;in4iMk8Fjj1;

#CATEGORY OPTIONS
##METADATA

##CO
    wckQxxQK4hY;zb988DEsTWI;Wo1PowlV2Nu;JGrvMIR9dNW;hC4WAv2wKep;QDGwAPfi7tu;c7ZwRxfFYyF;z80g7okL9TR;ESHbSUOcBfD;IYWzA8BiqBg

    ##URL
        https://hiskenya.org/api/analytics?dimension=dx:T8vIpv842eX;v70T2bzhYBH;kMTppRz9Dy6;OzHa7Wm9wWb;HKNzvgOEXdV;zX3RWIpPsU5;anJTqZAje6a;Ptvb1N5gQ54;ZcZwhTRfWkK;g2J7NpW2ZM8;onTjG9ODh1k;BDKBvFhIQJf;bZplX60YbPf;wxA11VC6wRx;U3PLl3ULQt8;piEFmBh6Uql;zbrSNo8bSST;UMqyIpzJRCJ;ugsQqSyyZy2;wFFhejJhg1B;LI1E9Xrvpwr;fWTJUSnntn6;HkZ39x8BCEK;siZ3FslqDHV;ymWk2uW4f1s;y8vOQhrlRLj;iKalfg97rtZ;r9fSbflgQgC;VJ9VJ6dxwVD;OnFFuii2sYi;JrfpoCr2vII;rNmSAnjCmWK;AUsGmP0j5eC;wWKgpcMkHnJ;pCPSqHPI4iN;kwhxIWspmVw;VfUQzXlpKQl;xUayEnX8Blt;NWL4ULZezMy;LK4JkwJDx2c;aYXQcIen2Wl;u66LdVsKCvV;ijmmJw97V6g;eb9fVoSbFIr;c4V5Fav5ovZ;in4iMk8Fjj1;NQzz85r5JPD;ID6Aso4IXcZ;pZctFqRLi8W;F1W9V1jfIwf;q8sjZIlFs4J;RFWuUKeuciq;MNVBv62ijRX;MZgAjnphgsg;cWzuTozjBUb;VRFyixZiNKq;TW2swzvuk6K;gFNHAyxeLks;fCU7NkQ7egO;owvoPNua6Q1;jV9MuYZNb6l;vLBcMXH3ITC;VGti1UehUwX;wAkXhyTK5ug;EX8fzwmyfI7;I6KeyHpHPaI;Ao4QopHHGEk;LVNETa4jV2u;R3cZo5wqEJO;ARFq4oY3heY;PHm90uko7eq;CIl81MC1CxL;VgX1W96jMow;xOuDDwYj1Yw;jclt56IrD0J;LbpQwKhUFgf;KUVy8JTJHma;Iif81jdgAwp;bfehwWGgiWJ;lduCnun9ZJP;DLtximMysSs;MD5fubfNOMI;nfjDbwayZ4s;PQBxN4inyLo;aSaopRmPuaz;AZZqUosplNX;EBquyc7eibs;QjNUILIPIGL;uM70qGs87WM;AYlM9Gs9D6i;iObZlGvkJDy;AwEvXiS8TXC;nQlkXyft9pb;K4wC1yrizQD;AqK0pDHRkop;cHlP8txIIr4;cv9SwH7B09P;CM4fRYs6wGn;PWd1NaYM55x;yYUBDfyp10Y;CQY0y7k4IQy;IEURUdOXwmV;wYblxwrkbww;Tn9w6C5ilIr;&dimension=pe:LAST_12_MONTHS;&dimension=ou:IG1Gyy3YH3q;OnDEhHt2DlT;VoW67kdzIju;GTBJgWnu1OT;GuFAWSVAJtr;SP1Xq12kl7B;jMASvYveXkK;A9FAQoB1Siq;NbF8FMrzjto;TJ7kmj9PYLF;w4zbe1VwNQD;M3YycknfNBU;Na35MAIiedG;uECirbmK3vj;ke9S2Mz3VEm;LkV5kJMdakx;JnIq6KZ1Itj;R6tG0IzAhpv;ASQn9MEddSK;hLgnGJh9fti;fcY1fkS6bL1;PnwtJbax3ac;FsamkWbjXfz;qfmsA8bsghX;tJVeZDtaAeT;PP253B8hwav;mWWHYZ1tDWQ;bSJkg11sPh7;zjvUyYZscbf;ngqqwzhuvjL;sm3tsYY05ZR;wdClQzCFIJ4;UpSJbkTCmSw;iV6KcqpZpNP;TjbSrT7E52j;KMa5n1FkqQa;o4pSK5PF97Y;OjwX5IrpWkM;VJaFTjbLNoF;BY5hzogAARK;Fme5HfYi8Yz;Tns72Eh3EfT;beSdaPe8i3e;lbpglStu7o6;Ao5e4mVaq7w;wm5jiU5wkdW;hmLJq0fpP3B;geSMiYmY8Go;K6JHRvs7bAu;TV3caTTBc8U;aOVjR4sxcFF;ozud4J6eShX;kHS8L1DtPun;ElWbzzfBiku;oLOy2vk98aO;oQyshRFoeqU;YPtA3hSg5pg;ENlNLYthtQS;bPQKfBFpNfA;rHeKd2QIbD1;bs491lneOuc;hac6Lg2Bqf5;m7VVNB6yflG;XhrPpF0LTsQ;ZxmTHyJ43ap;O1VQBaVQvat;vzRh9ON9ZsM;G8avY4Gzra8;NBZfDf5Rag9;R5ojZThWuJk;M9RAPkG5Qsn;eSc3m5tl4kf;qIuHbZDmYPX;ak0wzh9CthB;LRl9e3LWbrt;Ys64m0FQOFC;jneHRlhDshV;EYS2C6J7ram;wUH8NEbTU1W;Psa3nl6S4SK;upkXXpAU2Ua;sjhSrlzV4w6;l1OmcL5lUDk;Hye6HfITIcg;Ri3LdmazeTy;SKnHLYLo6lK;IUIajcIDxbh;uiQo8k8dlO4;ZyVu64QtPyq;AadLaV4ti7l;Eu4DUBxnPbV;BDWdywDLeVS;t5ezj03HWYj;nrl4EXu8KV0;UqRSRuTq2mg;QaCEAVVfwZE;aTrw18DrKW1;KRC06WZHgOL;aEdFARk1Zsm;VEde2ZaEcYZ;
#END MOH 730A